+function($) {
    (function (d, t) {
        var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
        bh.type = 'text/javascript';
        bh.src = '//www.bugherd.com/sidebarv2.js?apikey=hnhzqvbk86ftsu7ppwqoha';
        s.parentNode.insertBefore(bh, s);
    })(document, 'script');
    
if (!Modernizr.svg) {
    $(".logo img").attr("src", "images/site/logo.png");
}
var viewportHeight = $(window).height();
var alterHeight=viewportHeight-264;

    var windowWidth = window.screen.width < window.outerWidth ?
            window.screen.width : window.outerWidth;
    mobile = windowWidth == 320;
    var csswidth = $(window).width();
    csswidth = csswidth - 360;
    $('.main-dashboard#dashboard .row#dash').css('width', csswidth + 'px');
    $(window).resize(function() {
        csswidth = $(window).width();
        csswidth = csswidth - 360;
        $('.main-dashboard#dashboard .row#dash').css('width', csswidth + 'px');
    });

    function checkForElem(elem) {
        if (elem.length <= 0) {
            return false;
        }
        else {
            return true;
        }
    }
    
    if(alterHeight<573) {
        $('.fullmap').css('height', alterHeight-20+'px');
        $('#map-canvas3').css('height', alterHeight-22+'px');
        $('#fullmap').css('height', alterHeight-22+'px');
        // not using full map using map-canvas
        $('#map-canvas').css('height', alterHeight-22+'px');
        $('#photos').css('height', alterHeight-22+'px');
        $('.preview1 .scroll-pane').css('height', alterHeight-46+'px');
        $('#photos .main-image img').css('height', alterHeight-46+'px');
    }
    var selected_element;
    $('.dropdown').hover(
            function() {

                $(this).find('.dropdown_content').show();

                $(this).find('input').attr('checked', true);

            },
            function() {
            }
    );

    $('.dropdown').mouseleave(
            function() {
                $(this).find('.dropdown_content').hide();

                $(this).find('input').attr('checked', false);
            }
    );
    $('.dropdown').click(function(e) {
        if ($(this).find('.dropdown_content').is(":visible")) {

            $(this).find('.dropdown_content').hide();

            $(this).find('input').attr('checked', false);
        }
        else {

            $(this).find('.dropdown_content').show();

            $(this).find('input').attr('checked', true);

        }

    })
    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ')
                c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0)
                return c.substring(nameEQ.length, c.length);
        }
        return null;
    }


/*
    $.get("read.php", function(data) {
        $('body').prepend(data);

    });
*/
    function setCookie(c_name, value, exdays)
    {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
        document.cookie = c_name + "=" + c_value;
    }



    $('body').on("change", "#sitelist", function() {
        setCookie('selected_element', $(this).val(), 1);
        window.location.href = $(this).val();
    });

/*
    $('.home-bar .normal a.change').click(function(e) {
        e.preventDefault();
        $('.home-bar .normal ').hide();
        $('.home-bar .advanced ').show();
        $('#dashboard .navbar-second.navbar-static-top.home-bar').attr('style', 'height:auto!important');
        $('.navbar-second.navbar-static-top.home-bar .container').css('background', 'none');
        $('.home-bar select, #dashboard select, #dashboard input[type="radio"]').fancyfields("reset");

    });
    $('.home-bar .advanced a.change').click(function(e) {
        e.preventDefault();
        $('.home-bar .advanced ').hide();
        $('.home-bar .normal ').show();
        $('#dashboard .navbar-second.navbar-static-top.home-bar').attr('style', 'height:208px!important');
        $('.navbar-second.navbar-static-top.home-bar .container').not('#dashboard .navbar-second.navbar-static-top.home-bar .container').css('background', 'url(img/home_bg.png) no-repeat bottom right');
        $('.home-bar select, #dashboard select, #dashboard input[type="radio"]').fancyfields("reset");


    });
    */
    $('.dropdown .dropdown_content li').click(function() {
        $(this).parent('.dropdown_content').hide();

        $(this).parent('dropdown').find('input').attr('checked', false);

    });

    if (mobile === true) {

        $('#footer .left.subscribe input[type="text"]').attr('placeholder', 'Subscribe to our newsletter');

    }



    $('body#contact-us').prepend(('<div class="navbar navbar-inverse navbar-fixed-top" style="position:relative;z-index:999999"><div class="container"><p>You need to confim your email by clicking on the link we sent, If you have lost the confirmation link or it has expired, <a href="#">click here</a>.</p></div></div>'))
    if (checkForElem($('#mass-uploader')) === true) {
        $('#mass-uploader').customFileInput();
    }
    $(document).ready(function() {
        if ($('.preview1').length > 0) {
        } else {
            $('.v5 .viewNavigation li').click(function(e) {
                e.preventDefault();
                $('.v5 .viewNavigation li').removeClass('active');
                $(this).addClass('active');
                var el = $(this).find('a').attr('rel');
                $('#' + el).show();
                if (el == 'images') {
                    $('#map-canvas2').hide();
                }
                else {
                    $('#images').hide();
                    initialize2();
                }
                return false;
            });
        }
        /*
        if (checkForElem($('#map-canvas')) === true) {


            var map;
            initialize = function() {
                var mapOptions = {
                    center: new google.maps.LatLng(1.3667, 103.8),
                    zoom: 6,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map(document.getElementById("map-canvas"),
                        mapOptions);
            }
            initialize();
            google.maps.event.addDomListener(window, 'load', initialize);
            google.maps.event.addDomListener(window, "resize", function() {
                var center = map.getCenter();
                google.maps.event.trigger(map, "resize");
                map.setCenter(center);
            });
        }

        if (checkForElem($('#map-canvas2')) === true) {


            var map;
            initialize2 = function() {
                var mapOptions = {
                    center: new google.maps.LatLng(1.3667, 103.8),
                    zoom: 6,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map(document.getElementById("map-canvas2"),
                        mapOptions);
            }
            initialize2();
            google.maps.event.addDomListener(window, 'load', initialize);
            google.maps.event.addDomListener(window, "resize", function() {
                var center = map.getCenter();
                google.maps.event.trigger(map, "resize");
                map.setCenter(center);
            });
        }
        */
    });
    
    $('input').focusin(function(e) {
        $(this).parent().find('label').css('color', '#0066a1');
        $(this).attr('title', '');
        $(this).removeClass('text-warning');
        $(this).popover('hide');
    });
    $('input').focusout(function(e) {
        $(this).parent().find('label').css('color', '#212121');
    });
if($('.selectpicker').length){
    $('.selectpicker').selectpicker({
        style: 'btn-info',
        size: 4
    });
}
    $('.modal-content span.close').click(function(e) {
        $('.modal').modal('hide');
    });

    var calcW = $('.row.calcrow').outerWidth();

    elwidth = Math.round(calcW / 220);
   
    $('.mosaicview:nth-child(' + elwidth + ')').css('margin-right', '0');
    $('.multiselect.dropdown-toggle.btn').click(function() {
        $('.createplan').hide();
        $('.btn-create').css('opacity', '1');
    })
    $('a.reset').click(function(e) {
        e.preventDefault();
        $('.filter-box').find("input[type=text], textarea").val("");
     
        $("#slider-range").ionRangeSlider("update", {
                    min: 0,
                    max: 5000,
                    from: 1,
                    to: 1500,
                    type: 'double',
                    step: 1,
                    prefix: "S$",
                    prettify: true,
                    hasGrid: false
                });
   $('.filter-box').find("select").fancyfields("reset");
    });
    $('.right.first.favourites .multiselect-container.dropdown-menu').find('a, input').click(function(e) {
        return false;
    });
    $('.right.first.favourites .btn-group.mmselect .multiselect-container.dropdown-menu').find('.btn-create').click(function(e) {
        $('.createplan').show();
        $('.btn-create').css('opacity', '0.4');

    });

    $('.btn-create').css('opacity', '1');
    $('.right.first.favourites .multiselect-container.dropdown-menu .createplan').find('button').click(function(e) {
        $('.createplan').hide();
        $('.btn-create').css('opacity', '1');
    });

$('.addcomment').popover();

    $('.addcomment').each(function() {
        $('.addcomment').click(function() {
            $(this).popover();
            $('.addcomment').not(this).popover('hide'); 
        });
    //$('.addcomment').popover();
    });

    $('.rfp .popover p').live('click', function() {
        $('.addcomment').popover('hide');
    });

    $('.rfp #dashboard .col-sm-12.comment.secondary.closed label').click(function() {
        $(this).parent().find('textarea').toggle();
        
    });
    
    
    // photo gallery on listing detail page
    $('.scroll-pane .thumb').click(function(e) {

        e.preventDefault();
        $newurl = $(this).attr('href');
        $('#mainImage').attr('src', $newurl);
        $('#mainImage').parent('a').attr('href', $newurl);
        return false;
    });
    
    $('#home #_submit').click(function(e){
        e.preventDefault();
       $('#sl1').parent('.ffSelectWrapper').addClass('error');
              return false;
    });
    $('.home-bar select, #dashboard .container select, #dashboard input[type="radio"], input[type="radio"], .container select').not('.container select.foot_select, select#mediatypeid').fancyfields();
}(window.jQuery);
