+function($) {
    function checkForElem(elem) {
        if (elem.length <= 0) {
            return false;
        }
        else {
            return true;
        }
    }
    $().ready(function() {
    if (checkForElem($('#bigmap-canvas')) === true) {

        "use strict";
        var marker, i, number, infobox;
        initializeBigMap();
        var Gmap;
        function initializeBigMap() {
            var mapOptions = {
                center: new google.maps.LatLng(1.3667, 103.8),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            Gmap = new google.maps.Map(document.getElementById("bigmap-canvas"),
                    mapOptions);
        }
        google.maps.event.addDomListener(window, 'load', initializeBigMap);
        google.maps.event.addDomListener(window, "resize", function() {
            var center = Gmap.getCenter();
            google.maps.event.trigger(Gmap, "resize");
            Gmap.setCenter(center);
        });

        infobox = new InfoBox({
            content: '<div class="left mosaicview" style="background: url(../images/site/adsample.jpg) no-repeat top left;">                            <a href="#" class="add2fav active"></a>                            <h3>B.T.Road Hoarding<br/><span>BILLBOARD<br/>16” x 20”</span></h3>                            <div class="foot-info">                                <h4>S$3000 / week<br/><span>2200 EUR /week</span></h4>                                 <span class="btn btn-primary counter">1</span>                            </div>                        </div></div>',
            disableAutoPan: false,
            maxWidth: 150,
            pixelOffset: new google.maps.Size(15, -60),
            zIndex: null,
            closeBoxMargin: "0",
            closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
            infoBoxClearance: new google.maps.Size(1, 1)
        });
        var locs = [['1.299015998840332', '103.855316162109370'], ['1.294657000000000', '103.859723000000000'],
            ['1.294311046600342', '103.852691650390600'], ['1.349199056625366', '103.984947204589800'],
            ['1.331245000000000', '103.741868000000000'], ['1.350711622879579', '103.849525451660150'],
            ['1.283949000000000', '103.858845999999970'], ['1.340151190757751', '103.706558227539060'], ['1.300012272776791', '103.845301151275630']];

        for (i = 0; i < locs.length; i++) {

            number = i + 1;
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locs[i][0], locs[i][1]),
                map: Gmap,
                icon: '../images/site/' + number + '.png',
                title: 'See store information!',
                id: number,
                number: number
            });
            google.maps.event.addListener(marker, 'mouseover', function() {
                //   infowindow.open(map1, this);
                console.log(this.number);
                this.setIcon('../images/site/' + this.number + '_on.png');
                this.setZIndex(5000);
                infobox.open(Gmap, this);

            });
            google.maps.event.addListener(marker, 'mouseout', function() {

                this.setIcon('../images/site/' + this.number + '.png');
                this.setZIndex(1000);
                infobox.close(Gmap, this);


            });
        }
        }
    });
    

}(window.jQuery);




                    
