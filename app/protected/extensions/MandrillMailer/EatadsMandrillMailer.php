<?php

/**
 * Include the the PHPMailer class.
 */
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'MandrillApiPhp' . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Mandrill.php');

//$mandrill = new Mandrill('wtWRc4QXlHhoMyK6nzHUqQ');

class EatadsMandrillMailer extends Mandrill {

    public $mandrill;
    public $CharSet='UTF-8';

    private $template_name = null;
    private $emTo;
    private $emFrom = null;
    private $emFromName = 'EatAds Admin';

    private $template_content;
    private $emSubject;
    private $async;
    private $ip_pool;
    private $send_at;
    private $message;
    
    private $emBcc;
    
    
//    public function __construct() {
//        $this->mandrill = new Mandrill(Yii::app()->params['mandrill']['api_key']);
//    }
    
//    public function sendMailTemplate($templateName) {
//        
//        $url = '';
//        $params = [];
//
//        $mandrill->call($url, $params);
//    }
    

        
	/**
	 * Set and configure initial parameters
	 * @param string $view View name
	 * @param array $data Data array
	 * @param string $layout Layout name
	 */
	public function __construct($emView = '', $emFrom = array(),$emTo = array(), $emData=array())
	{
            $this->template_name = $emView;
            $this->emTo = $emTo;
            if ($emFrom) {
                $this->emFrom = $emFrom;
            } else {
                $this->emFrom['email'] = Yii::app()->params['adminEmail'];
                $this->emFrom['name']  = 'Eatads';
            }
            $this->template_content = $emData;
            $this->message['from_email'] = $emFrom['from_email'];
            $this->message['from_name'] = $emFrom['from_name'];
            $this->message['to'] = $emTo;
            //array of struct
	}
        
        public function eatadsSendAvailabilityMailer() {
            $mailContent = EmailTemplate::getMailContentBySlug($this->template_name);
            $this->message['subject'] = $mailContent->subject;
            //call
            $result = $this->mandrill->messages->sendTemplate($this->template_name, $this->template_content, $message, $this->async, $ip_pool, $send_at);
            return $result;
            }
        public function eatadsSend() 
        {

            $mailContent = EmailTemplate::getMailContentBySlug($this->template_name);
            $this->emSubject = $mailContent->subject;
            //$this->emBody = $mailContent->content;
            /*
             * Generate json variables that need to be passed
             * these can be passed directly from the service
             */
            
            
            foreach( $this->template_content as $key => $value ) {
                $this->emBody = str_replace('{{'.$key.'}}', $value, $this->emBody);
                $this->emSubject = str_replace('{{'.$key.'}}', $value, $this->emSubject);
            }
            
            parent::__construct( 'eatadsMail', array('message' => $this->emBody) );
            parent::setFrom($this->emFrom, $this->emFromName);
            parent::setSubject($this->emSubject);
            parent::setTo($this->emTo);
            if(count($this->emBcc)) {
                parent::setBcc($this->emBcc);
            }
            return parent::send();
        }

}
