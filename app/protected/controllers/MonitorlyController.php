<?php

class MonitorlyController extends Controller
{
	
        public function init() {
            Yii::app()->theme = 'landing';
            $this->layout = "//layouts/landing";
        }
        
        public function actionIndex()
	{
            $url = Yii::app()->createUrl('monitorly/landing');
            $this->redirect($url);
	}
        
	public function actionLanding()
	{
		$this->render('landing');
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}