<?php

class SellerController extends Controller {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform actions
                'actions' => array('index'),
                'users' => array('*'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    // public function actionIndex($id) {
    public function actionIndex($userid) {
        
        if (!is_numeric($userid)) {
            // check if userid is not in DB, error 404
            if(!UserCompany::isUserAliasCompany($userid)) {
                throw new CHttpException(404,'Company cannot be found.');
            }
            $companyUrldata = UserCompany::getCompanyDataFromAlias($userid);
        } else {
            // check if userid is not in DB, error 404
            if(!UserCompany::isUserCompany($userid)) {
                throw new CHttpException(404,'Company cannot be found.');
            }
            $companyUrldata = UserCompany::getCompanyUrlData($userid);            
        }

        Yii::app()->openexchanger->exchangeRates;
        // default solrUrl
        $solrParams = array('sort' => '', 'fq' => '');

//        if (!is_numeric($id)) {
//            $companyUrldata = UserCompany::getCompanyDataFromAlias($id);
//        } else {
//            $companyUrldata = UserCompany::getCompanyUrlData($id);
//        }
             
        $companyUserId = $companyUrldata['userid'];
        $companyId = $companyUrldata['id'];

        $companyData = UserCompany::getCompanyDataById($companyId);

        $dropDownFilter = array('1' => 'Most recent', '2' => 'Price', '3' => 'Popular');

        $breadcrumbArr = array(
            'Home' => JoyUtilities::getHomeUrl(),
        );

        // dropdown filter
        $filterId = Yii::app()->request->getQuery('filterId');
        if ($filterId == 2) {
            //$criteria['order'] = 'price DESC';
            $solrParams['sort'] = 'weeklyprice desc ';
        } else if ($filterId == 3) {
            //$criteria['order'] = 't.pscore DESC';
            $solrParams['sort'] = 'pscore desc';
        } else {
            //$criteria['order'] = 'datemodified DESC';
            $solrParams['sort'] = 'datemodified desc';
        }

        // filter user by id
        $solrParams['fq'] .= ' foruserid:' . $companyUserId;

        // solr query 
        $solrQuery = '*:*';

        // get listing from Solr
        $result = Yii::app()->listingSearch->get($solrQuery, 0, 50000, $solrParams);
        $listing = array();
        if (isset($result->response->numFound) && $result->response->numFound) {
            //echo "Results number is ".$result->response->numFound.'<br />';
            foreach ($result->response->docs as $doc) {
                $listing[] = $doc;
            }
        }

        $dataProvider = new CArrayDataProvider($listing, array(
            'pagination' => array(
                'pageSize' => Yii::app()->params['records_per_page'],
            //'params' => array('companyUserId'=>$companyUserId),
            //'route' => JoyUtilities::getCompanyUrl($companyData->id)
            ),
        ));

        // Set page meta keyword and page title
        $meta_keyword = $companyData->name . " " . str_replace(",", "," . $companyData->name . " ", Yii::app()->params['meta_keyword']);
        Yii::app()->clientScript->registerMetaTag($meta_keyword, 'keywords', null, array('id' => 'meta_keywords'), 'meta_keywords');

        $stateList = UserCompany::getOwnerListingState($companyUserId);
        $stateList = strlen($stateList) > 0 ? " in " . $stateList : '';
        $this->pageTitle = $companyData->name . " - Outdoor & OOH media" . $stateList;
        $meta_description = "Visit EatAds.com to find Outdoor & OOH media to match your campaign needs. Find media hoardings, billboards, transit media, bus shelters, airport media and much more. From all over the world.";
        Yii::app()->clientScript->registerMetaTag($meta_description, 'description', null, array('id' => 'meta_description'), 'meta_description');

        // Set page breadcrumb
        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            $companyData->name,
        );

        // favorite star link
        $favLink = false;
        if (Yii::app()->user->isGuest) {
            $favLink = true;
        } else {
            $roleId = Yii::app()->user->roleId;
            if ($roleId == 2 || $roleId == 4) {
                $favLink = true;
            }
        }

        if (isset($_REQUEST['ajax'])) {
            $this->renderPartial('indexGrid', array(
                'dataProvider' => $dataProvider,
                'updateBreadcrumb' => true,
                'listType' => Yii::app()->request->getQuery('lt'),
                'favLink' => $favLink,
            ));
        } else {
            $this->render('index', array(
                'dataProvider' => $dataProvider,
                'dropDownFilter' => $dropDownFilter,
                'listType' => Yii::app()->request->getQuery('lt'),
                'favLink' => $favLink,
                'companyUserId' => $companyUserId,
                'companyData' => $companyData             // for company profile details
            ));
        }
    }

    /* public function actionIndexAjax() {
      // default solrUrl

      $solrParams = array('sort' => '', 'fq' => '');
      $companyAlias = Yii::app()->request->getQuery('id');
      $companyUrldata = UserCompany::getCompanyDataFromAlias($companyAlias);
      $companyUserId = $companyUrldata['userid'];

      // dropdown filter
      $filterId = Yii::app()->request->getQuery('filterId');
      if ($filterId == 2) {
      //$criteria['order'] = 'price DESC';
      $solrParams['sort'] = 'weeklyprice desc ';
      } else if ($filterId == 3) {
      //$criteria['order'] = 't.pscore DESC';
      $solrParams['sort'] = 'pscore desc';
      } else {
      //$criteria['order'] = 'datemodified DESC';
      $solrParams['sort'] = 'datemodified desc';
      }

      // filter user by id
      if(is_numeric($companyUserId)) {
      $solrParams['fq'] = ' foruserid:' . $companyUserId;
      }

      // solr query
      $solrQuery = '*:*';

      // get listing from Solr
      $result = Yii::app()->listingSearch->get($solrQuery, 0, 20, $solrParams);

      $listing = array();
      if (isset($result->response->numFound) && $result->response->numFound) {
      //echo "Results number is ".$result->response->numFound.'<br />';
      foreach ($result->response->docs as $doc) {
      $listing[] = $doc;
      }
      }

      $dataProvider = new CArrayDataProvider($listing, array(
      'pagination' => array(
      'pageSize' => Yii::app()->params['records_per_page']
      ),
      ));
      // favorite star link
      $favLink = false;
      if (Yii::app()->user->isGuest) {
      $favLink = true;
      } else {
      $roleId = Yii::app()->user->roleId;
      if ($roleId == 2 || $roleId == 4) {
      $favLink = true;
      }
      }
      $this->renderPartial('indexGrid', array(
      'dataProvider' => $dataProvider,
      'updateBreadcrumb' => true,
      'listType'=>  Yii::app()->request->getQuery('lt'),
      'favLink'=>$favLink,
      ));
      } */

//    public function actionContact($id)
//    {        
//        $model=new Email;
//        $userMessage = null;
//        // uncomment the following code to enable ajax-based validation
//        
//        if(isset($_POST['ajax']) && $_POST['ajax']==='email-contact-form')
//        {
//            echo CActiveForm::validate($model);
//            Yii::app()->end();
//        }
//                
//        if(isset($_POST['Email']))
//        {
//            $_POST['Email'] = JoyUtilities::cleanInput($_POST['Email']);
//            $model->attributes=$_POST['Email'];
//            if($model->validate())
//            {
//                $model->datecreated = date("Y-m-d H:i:s");
//                $model->byuserid = Yii::app()->user->id;
//                $model->foruserid = $id;
//                $model->type = 1;
//                $model->subject = 'Contact Seller';
//                
//                
//                // get touser details
//                $userModel = User::model()->findByPk($id, array('select'=>'email,subscribe'));
//                
//                if($userModel->subscribe) {
//                    // user subscribed for email ntfn                    
//                    // get fromuser details
//                    $fromUserModel = User::model()->findByPk(Yii::app()->user->id, array('select'=>'fname,lname,phonenumber'));
//                    
//                    $model->datesent = date("Y-m-d H:i:s");
//                    $emailMessage = '<html><body>
//                                    Hi,<br><br>
//                                    '.$model->message.'<br><br>
//                                    <b>Regards,<br>'.$fromUserModel->fname.' '.$fromUserModel->lname.',<br>
//                                    '.$fromUserModel->phonenumber.'<b>
//                                    </body></html>'; //end of message
//                } else {
//                    // user not subscribed for email ntfn
//                    $emailMessage = '<html><body>
//                                    Hi,<br><br>
//                                    You have received a new notification.<br>
//                                    Please confirm your email to see all pending and new notifications.<br><br>
//                                    <b>Regards,<br>EatAds Team<br><b>
//                                    </body></html>'; //end of message
//                }
//                // send email to seller
//                // send reset pwd link email to user                    
//                $to      = $userModel->email;
//                $subject = $model->subject;
//                
//                /*echo '<pre>';
//                CVarDumper::dump($model->attributes);
//                echo '</pre>'; die();*/
//                
//                $headers = 'From: '.Yii::app()->params['adminEmail']. "\r\n" .
//                            'Reply-To: '.Yii::app()->params['adminEmail']. "\r\n" .
//                            'Content-type: text/html; charset=iso-8859-1\r\n'.
//                            'X-Mailer: PHP/' . phpversion();
//
//                if(mail($to, $subject, $emailMessage, $headers)){ 
//                    if($model->save()) {
//                        // record save
//                        $userMessage = 'Email sent successfully.';
//                    } else {
//                        // record save failure
//                        $userMessage = 'Failure, Please try again!';
//                    }
//                } else {
//                    // email failure
//                    $userMessage = 'Failure, Please try again!';
//                }                
//            }
//        }        
//        $this->render('contact', array('model'=>$model, 'userMessage'=>$userMessage));
//    }
    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
}
