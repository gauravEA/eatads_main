<?php

class AvailabilityManagerController extends Controller
{
    
        protected $myvar;
    
        public function init() {
            Yii::app()->theme = 'new';
            $this->layout = "//layouts/newdesign";
            $this->myvar = Yii::app()->request->getQuery('alias');
        }
        
	public function actionIndex()
	{
            $company = new UserCompany();
            $alias = $this->myvar;
            if ($alias) {
                $company = UserCompany::getCompanyDataFromAlias($alias);
//                print_r($company);die();
            }
            //fetch all the listing of the company
            $data = Listing::getListingsForCompany($company['id'], null);
            $this->render('index', array('data' => $data));
	}
        
        
          /**
     * Used to return Vendor details we need to pass the alias as a param eg : live-media is 
     * @param type $alias
     * @return json of the company data 
     * 
     */
    public function actionVendorDetails() {
        $alias =  $_GET['alias'];// Yii::app()->request->getQuery('alias');
        echo json_encode(UserCompany::getCompanyDataFromAlias($alias));
    }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}