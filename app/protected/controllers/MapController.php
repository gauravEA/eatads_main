<?php

class MapController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'area', 'areaAjax', 'indexAjax'),
                'users' => array('*'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        //$startTime = microtime(true);

        $metaKeyword = $pageTitle = '';

        //$geoLatLng = '28.649199,77.226295';
        $geoLatLng = Yii::app()->request->getQuery('geoloc');
        if ($geoLatLng == '' || !is_numeric(Yii::app()->request->getQuery('proximity'))) {
            // $this->redirect(array('/'));
            // $this->redirect(JoyUtilities::getHomeUrl());            
            throw new CHttpException(404, '');
        }

        if (isset($_REQUEST['ajax'])) {
//            $this->forward('indexAjax');
        }
        // used in index.php
        $dropDownFilter = array('1' => 'Most recent', '2' => 'Price', '3' => 'Popular');

        // default solrUrl
        $solrParams = array('fq' => '');

        $breadcrumbArr = array(
            'Home' => JoyUtilities::getHomeUrl(),
        );

        // filter media type 
        $mediaTypeParam = Yii::app()->request->getQuery('mediatypeid');
        $mediaTypeId = null;
        if (!empty($mediaTypeParam) && is_array($mediaTypeId = explode(",", $mediaTypeParam))) {
            //$solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
            $solrParams['fq'] .= '(';
            foreach ($mediaTypeId as $mt) {
                if (is_numeric($mt))     // to remove 'multiselect-all'
                    $solrParams['fq'] .= ' mediatypeid:' . $mt . ' OR';
            }
            $solrParams['fq'] = rtrim($solrParams['fq'], 'OR');
            $solrParams['fq'] .= ')';
        }

        // filter price slider 
        $priceSlider = explode('-', Yii::app()->request->getQuery('priceslider'));
        if (count($priceSlider) > 1) {
            // base on currency selected conv to usd to compare weeklyprice
            $newMinPrice = round(Yii::app()->openexchanger->convertCurrency($priceSlider[0], $this->ipCurrencyCode, 'USD'));
            $newMaxPrice = round(Yii::app()->openexchanger->convertCurrency($priceSlider[1], $this->ipCurrencyCode, 'USD'));
            //$criteria['condition']  .= ' AND weeklyprice between '.$priceSlider[0]. ' AND '. $priceSlider[1];
            $solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
            $solrParams['fq'] .= 'weeklyprice:[' . $newMinPrice . ' TO ' . $newMaxPrice . ']';
        }

        // distance        
        $proximity = is_numeric(Yii::app()->request->getQuery('proximity')) ? (int) Yii::app()->request->getQuery('proximity') : Yii::app()->params['proximity'];
        // lat lng with proximity
        $solrParams['fq'] .= " AND {!geofilt pt=$geoLatLng sfield=geoloc d=$proximity}";

        // solr query 
        $textSearch = Yii::app()->request->getQuery('textsearch');
        if (!empty($textSearch)) {
            $solrQuery = "name:*{$textSearch}* OR description:*{$textSearch}* OR mediatype:*{$textSearch}* OR audiencetag:*{$textSearch}*";
        } else {
            $solrQuery = '*:*';
        }
        // get listing from Solr                
        //$result = Yii::app()->listingSearch->get($solrQuery, 0, 50000, $solrParams);
        //$timeTaken = microtime(true) - $startTime;
        //echo 'b4 curl timeTaken - ' . $timeTaken.'<br />';

        $solrParams['wt'] = 'json';
        //$params['json.nl'] = '';
        $solrParams['fl'] = 'id,lat,lng,ea';
        $solrParams['q'] = $solrQuery;
        $solrParams['start'] = 0;
        $solrParams['rows'] = 50000; //Yii::app()->params['init_markers'];//50000;

        $qp = http_build_query($solrParams, null, '&');

        // >>> curl query
        $ch = curl_init();
        //$url = 'http://localhost:8080/solr/listing/select?'.$qp;            
        $url = Yii::app()->params['solrCurl'] . $qp;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);            // Include header in result? (0 = yes, 1 = no)            
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Should cURL return or print out the data? (true = return, false = print)
        //curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $result = curl_exec($ch);
        curl_close($ch);
        //print_r($result);die();
        // <<< curl query
        //$timeTaken = microtime(true) - $startTime;
        //echo 'curl timeTaken - ' . $timeTaken.'<br />';


        /* $listing = array();
          if (isset($result->response->numFound) && $result->response->numFound) {
          //echo "Results number is ".$result->response->numFound.'<br />';
          foreach ($result->response->docs as $doc) {
          $listing[] = $doc;
          }
          }

          $dataProvider = new ArrayDataProvider($listing, array(
          'totalItemCount' => isset($result->response->numFound) ? $result->response->numFound : 0,
          'pagination' => array(
          'pageSize' => Yii::app()->params['records_per_page'],
          ),
          ));
         */
        // Create Breadcrumb
        $this->breadcrumbs = $breadcrumbArr;

        // Set page meta keyword and page title
        //$meta_keyword = str_replace(",", " ".$metaKeyword.",", Yii::app()->params['meta_keyword'])." ".$metaKeyword;        
        //Yii::app()->clientScript->registerMetaTag($meta_keyword, 'keywords', null, array('id'=>'meta_keywords'), 'meta_keywords');

        if (strlen($pageTitle)) { // if breadcrumb has data then showbreadcrumb otherwise default title
            $this->pageTitle = $pageTitle;
        } else {
            $this->pageTitle = Yii::app()->name;
        }
        // if countryid is not set then, set country lat lng manually
        $geoLatLng = isset($geoLatLng) ? $geoLatLng : Yii::app()->params['map_latlng'];
        $this->render('index', array(
            'dataProvider' => $result,
            'dropDownFilter' => $dropDownFilter,
            'pageTitleText' => '',
            'pageMetaKeywordText' => '',
            'geoLatLng' => $geoLatLng,
            'proximity' => $proximity,
            'pageMetaKeywordText' => Yii::app()->params['meta_keyword'],
            'search_params' => array(
                'mediatypeid' => $mediaTypeId,
                'priceslider' => $priceSlider
            ),
        ));
    }

    public function actionIndexAjax() {
        $metaKeyword = $pageTitle = '';

        // default solrUrl
        $solrParams = array('fq' => '');

        /* $breadcrumbArr = array(
          'Home' => JoyUtilities::getHomeUrl()
          ); */


        // filter media type 
        $mediaTypeParam = Yii::app()->request->getQuery('mediatypeid');
        $mediaTypeId = null;
        if (!empty($mediaTypeParam) && is_array($mediaTypeId = explode(",", $mediaTypeParam))) {
            //$solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
            $solrParams['fq'] .= '(';
            foreach ($mediaTypeId as $mt) {
                if (is_numeric($mt)) {    // to remove 'multiselect-all'
                    $solrParams['fq'] .= ' mediatypeid:' . $mt . ' OR';
                }
            }
            $solrParams['fq'] = rtrim($solrParams['fq'], 'OR');
            $solrParams['fq'] .= ')';
        }

        // filter price slider 
        $priceSlider = explode('-', Yii::app()->request->getQuery('priceslider'));
        if (count($priceSlider) > 1) {
            // base on currency selected conv to usd to compare weeklyprice
            $newMinPrice = round(Yii::app()->openexchanger->convertCurrency($priceSlider[0], $this->ipCurrencyCode, 'USD'));
            $newMaxPrice = round(Yii::app()->openexchanger->convertCurrency($priceSlider[1], $this->ipCurrencyCode, 'USD'));
            //$criteria['condition']  .= ' AND weeklyprice between '.$priceSlider[0]. ' AND '. $priceSlider[1];
            $solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
            $solrParams['fq'] .= 'weeklyprice:[' . $newMinPrice . ' TO ' . $newMaxPrice . ']';
        }

        // proximity
        $proximity = is_numeric(Yii::app()->request->getQuery('proximity')) ? (int) Yii::app()->request->getQuery('proximity') : Yii::app()->params['proximity'];
        // geoloc
        $geoloc = Yii::app()->request->getQuery('geoloc');

        $solrParams['fq'] .= " AND {!geofilt pt=$geoloc sfield=geoloc d=$proximity}";

        // solr query 
        $textSearch = Yii::app()->request->getQuery('textsearch');
        if (!empty($textSearch)) {
            $solrQuery = "name:*{$textSearch}* OR description:*{$textSearch}* OR mediatype:*{$textSearch}* OR audiencetag:*{$textSearch}*";
        } else {
            $solrQuery = '*:*';
        }

        //$solrParams['rows'] = 5;
        // get listing from Solr                
        //$result = Yii::app()->listingSearch->get($solrQuery, 0, 50000, $solrParams);
        // load from 0 if markers already loaded is not in $_GET
        $marker_loaded = (int) Yii::app()->request->getQuery('marker_loaded');
        $marker_loaded = ($marker_loaded > 0) ? $marker_loaded : 0;

        // how many to load - next_toload_count not there then default load count
        $next_toload_count = (int) Yii::app()->request->getQuery('next_toload_count');
        $init_markers = ($next_toload_count > 0) ? $next_toload_count : Yii::app()->params['init_markers'];


        $solrParams['wt'] = 'json';
        //$params['json.nl'] = 'map';
        $solrParams['fl'] = 'id,lat,lng,ea';
        $solrParams['q'] = $solrQuery;
        $solrParams['start'] = 0; // $marker_loaded; //0;
        $solrParams['rows'] = 50000; // $init_markers; //50000;

        $qp = http_build_query($solrParams, null, '&');

        // >>> curl query
        $ch = curl_init();
        $url = Yii::app()->params['solrCurl'] . $qp;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);            // Include header in result? (0 = yes, 1 = no)            
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Should cURL return or print out the data? (true = return, false = print)
        //curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $result = curl_exec($ch);
        curl_close($ch);
        // <<< curl query

        /*
          $listing = array();
          if (isset($result->response->numFound) && $result->response->numFound) {
          //echo "Results number is ".$result->response->numFound.'<br />';
          foreach ($result->response->docs as $doc) {
          $listing[] = $doc;
          }
          }
          $dataProvider = new ArrayDataProvider($listing, array(
          'totalItemCount' => isset($result->response->numFound) ? $result->response->numFound : 0,
          'pagination' => array(
          'pageSize' => Yii::app()->params['records_per_page'],
          ),
          ));
         */
        // Set page meta keyword and page title
        //$pageMetaKeywordText = str_replace(",", " ".$metaKeyword.",", Yii::app()->params['meta_keyword'])." ".$metaKeyword;
        // Create Breadcrumb
        //$this->breadcrumbs= $breadcrumbArr;

        echo $result;
        /* $this->renderPartial('indexGrid', array(
          'dataProvider' => $result,
          'updateBreadcrumb' => true,
          'pageTitleText' => $pageTitle,
          'pageMetaKeywordText' => $pageMetaKeywordText,
          'countryName'=>  'TOCHANGE'
          )); */
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Listing the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Listing::model()->with(array('listingImages', 'mediatype', 'basecurrency', 'city', 'country', 'state', 'foruser' => array('select' => 'fname,lname')))->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Listing $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'listing-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
