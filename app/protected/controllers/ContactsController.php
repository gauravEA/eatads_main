<?php

class ContactsController extends Controller
{
    protected $myvar;
    
    public function init() {
        Yii::app()->theme = 'new';
        $this->layout = "//layouts/newdesign";
        $this->myvar = Yii::app()->request->getQuery('alias');
    }
    
    public function actionIndex() {
        if (isset($_POST['data']) && isset($_POST['companyid'])) {
            Yii::app()->user->setFlash('success', " Contacts Added");
            //print_r($_POST);
            $inserted = $this->uploadContacts($_POST['data'], $_POST['companyid']);
            Yii::app()->user->setFlash('success', $inserted . " Contacts Added Successfully");
        }
        $this->render('index');
    }
    
    
    
    private function uploadContacts($json, $companyid) {
        $inserted = 0;
        $data = json_decode($json);
        if ($data) {
            for ($i =0 ; $i < count($data); $i++) {
                $email = strtolower($data[$i][0]);
                $contact = new UserContacts();
                $contact->email = $email;
                $contact->name = $data[$i][1];
                $contact->company = $data[$i][2];
                $contact->status = 1;
                $contact->vendorid = $companyid;
                $contact->createdDate = date("Y-m-d H:i:s");
                // find user by email
                $user = User::model()->findByAttributes(array('email'=> $email));
                if ($user) {
                    $contact->linkedUserId = $user->id;
                } else {
                    $user = new User;
                    $user->email = $email;
                    $user->active = 0;
                    $user->datecreated = date("Y-m-d H:i:s");
                    $user->save();
                    
                    //default buyer
                    $userRoleModel = new UserRole;
                    $userRoleModel->attributes = array('userid'=>$user->id , 'roleid'=> 2);
                    $userRoleModel->save();
                    
                    $contact->linkedUserId = $user->id;
                }
                $contact->save();
                $inserted ++ ;
            }
            return  $inserted;
        }
    }
    
      public function actionUpdateVendorContacts() {
            //TODO Alias should be fetched from User Logged in 
          $updated = 0;
          if(isset($_POST['data'])) {
                $data = json_decode($_POST['data']);
                
              //  print_r($data);die();
                if ($data) {
                    for ($i=0; $i < count($data); $i++) {
                        //die(print_r($data[$i]));
                        if ($data[$i]->id) {
                            $contactModel = UserContacts::model()->findByPk($data[$i]->id);
                            if ($contactModel) {
                            $contactModel->name = $data[$i]->name;
                            $contactModel->company = $data[$i]->company;

                            if ($data[$i]->deleteFlag == "true") {
                                $contactModel->status = 0;
                            } else {
                                $contactModel->status = 1;
                            }
                            $contactModel->isNewRecord = false;
                            $contactModel->save();
                            $updated ++;  
                            }

                        }
                    }
                }
            }
            Yii::app()->user->setFlash('success', $updated . " Contacts Updated Successfully");
            $this->redirect(Yii::app()->urlManager->createUrl("vendor/" . $_POST['alias'] . "/contacts"));
            
    }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}