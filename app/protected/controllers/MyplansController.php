<?php

class MyplansController extends Controller
{
    
    protected $plan;
    public function init() {
        Yii::app()->theme = 'plans';
        $this->layout = "//layouts/plan_page";
        $this->plan = Yii::app()->request->getQuery('name');
    }
    
    public function actionIndex()
	{
        
            $this->render('index');
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}