<?php

class EmailAnalyticsController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}
        
        public function actionUpdateEmailopenRedirectToUrl() {
        $userid = Yii::app()->request->getQuery('uid');
        $eventid = Yii::app()->request->getQuery('eid');
        $url = Yii::app()->request->getQuery('url');
        Yii::log($userid . " being called for event " . $eventid );
        //echo $userid . " being called for event " . $eventid . " redirecting to url " . $url . " redirect url " . JoyUtilities::getVendorShowcaseUrl($url);//die(); 
        
        $this->redirect(JoyUtilities::getVendorShowcaseUrl($url));
    }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}