<?php

/**
 * Controller only for AJAX calls
 */
class AjaxController extends Controller {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform actions
                'actions' => array('index'),
                'users' => array('@'),
                'roles' => array(JoyUtilities::ROLE_ADMIN),
            ),
            array('allow', // allow all users to perform actions
                'actions' => array('massview', 'managelist'),
                'users' => array('@'),
                'roles' => array(JoyUtilities::ROLE_MEDIA_OWNER, JoyUtilities::ROLE_ADMIN),
            ),
            array('allow', // allow all users to perform actions
                'actions' => array('dynamiccountries', 'dynamicstates', 'dynamiccities', 'subscribe', 'countrylatlng', 'reversegeocode', 'postrequirements',
                    'getmapinfosolr', 'actionPrioritystates', 'prioritystates', 'prioritycities', 'localitySearch', 'getlistingcountlatlng'),
                'users' => array('*'),
            ),
            array('allow', // allow all users to perform actions
                'actions' => array('contactSeller', 'favlistingtoggle'),
                'users' => array('@'),
                'roles' => array(JoyUtilities::ROLE_MEDIA_BUYER, JoyUtilities::ROLE_THIRD_PARTY),
                'expression' => array('WebUser', 'allowActiveUser'),
            ),
            array('allow', // allow all users to perform actions
                'actions' => array('userfavlistids'),
                'users' => array('@'),
                'roles' => array(JoyUtilities::ROLE_MEDIA_BUYER, JoyUtilities::ROLE_THIRD_PARTY),
            ),
            array('allow', // allow all users to perform actions
                'actions' => array('setLocation'),
                'users' => array('@'),
                'roles' => array(JoyUtilities::ROLE_MEDIA_OWNER, JoyUtilities::ROLE_ADMIN),
            ),
            array('allow', // allow all users to perform actions
                'actions' => array('signup', 'getlisting', 'getmarkers', 'vendordetails', 
                    'retriveplan', 'getsitedetails', 'addinexistingplan', 'addplan', 'addfavorite', 
                    'plandetail', 'deleteplanlisting', 'getmediatypes', 'uploadcontacts', 'vendorcontacts', 
                    'updatevendorcontacts','pushavailabilitymailstoqueue', 'massuploadlistingsforvendor', 
                    'sendavailabilitymailer', 'updateemailanalytics', 'updateavailablity',
                    'updateemailopenredirecttourl'),
                'users' => array('*'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }

    /*
     * Post requirement
     */

    public function actionPostrequirements() {
        // ajax validation request for Post Requirements
        if (isset($_POST['Contact'])) {

            $model = new Contact();
            $model->scenario = 'postRequirements';
            $model->setAttributes($_POST['Contact']);
            $valid = $model->validate();
            if ($valid) {
                $model->type = 1;
                $model->datecreated = date("Y-m-d H:i:s");
                $model->save();
                // EMAIL support 
                // mail support
                $date = date('l jS \of F Y h:i:s A');
                // Inserts HTML line breaks before all newlines in a string
                $message = nl2br($model->message, false);

                $mailSupport = new EatadsMailer('post-requirements-support', Yii::app()->params['supportEmail'], array('date' => $date,
                    'name' => $model->name,
                    'phone' => $model->phone,
                    'message' => $message,
                    'email' => $model->email));
                $mailSupport->eatadsSend();

                // mail user
                $mailUser = new EatadsMailer('post-requirements-user', $model->email, array('name' => $model->name));
                $mailUser->eatadsSend();

                // mail chimp
                $MailChimp = new MailChimp(Yii::app()->params['mailChimp']['api_key']);
                $result = $MailChimp->call('lists/subscribe', array(
                    'id' => Yii::app()->params['mailChimp']['id'],
                    'email' => array('email' => $model->email),
                        //'merge_vars'        => array('FNAME'=>'J', 'LNAME'=>'Joy'),
                        //'double_optin'      => false,
                        //'update_existing'   => true,
                        //'replace_interests' => false,
                        //'send_welcome'      => false,
                ));

                // echo CJSON::encode(array('status'=>'success'));
                echo 'success';
                Yii::app()->end();
            } else {
                $error = CActiveForm::validate($model);
                if ($error != '[]')
                    echo $error;
                Yii::app()->end();
            }
            //echo CActiveForm::validate($model);
            //Yii::app()->end();
        }
    }

    public function actionreversegeocode() {
        $lat = Yii::app()->request->getParam('lat');
        $lng = Yii::app()->request->getParam('lng');
        $result = JoyUtilities::reverseGeocode($lat, $lng);
        $data = array(
            'country' => ucwords($result['country']),
            'state' => ucwords($result['state']),
            'city' => ucwords($result['city'])
        );
        echo json_encode($data);
    }

    /*
     * Home page search
     * get listing count in 15 km radius of lat lng
     */

    public function actiongetlistingcountlatlng() {
        $lat = Yii::app()->request->getParam('lat');
        $lng = Yii::app()->request->getParam('lng');
        $proximity = Yii::app()->request->getParam('proximity');

        // default solrUrl
        $distance = (is_numeric($proximity) && $proximity > 0) ? $proximity : Yii::app()->params['proximity'];
        $solrParams = array('fq' => '');

        // filter media type 
        $mediaTypeParam = Yii::app()->request->getQuery('mediatypeid');
        $mediaTypeId = null;
        if (!empty($mediaTypeParam) && is_array($mediaTypeId = explode(",", $mediaTypeParam))) {
            $solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
            $solrParams['fq'] .= '(';
            foreach ($mediaTypeId as $mt) {
                if (is_numeric($mt))
                    $solrParams['fq'] .= ' mediatypeid:' . $mt . ' OR';
            }
            $solrParams['fq'] = rtrim($solrParams['fq'], 'OR');
            $solrParams['fq'] .= ')';
        }

        // filter lat lng with proximity
        $solrParams['fq'] .= " AND {!geofilt pt=$lat,$lng sfield=geoloc d=$distance}";
        $solrQuery = '*:*';
        // get listing from Solr
        $result = Yii::app()->listingSearch->get($solrQuery, 0, 200, $solrParams);
        $listingCount = 0;
        if (isset($result->response->numFound) && $result->response->numFound) {
            $listingCount = $result->response->numFound;
        }
        //$result = array('lat'=>$lat, 'lng'=>$lng);
        echo json_encode($listingCount);
    }

    /*
     * for locality seach autocomplete
     * home page search
     */

    public function actionlocalitySearch() {
        $term = Yii::app()->request->getParam('term');
        //$result = array("ActionScript", "AppleScript", "Asp", "BASIC", "C", "C++", "Clojure", "COBOL", "ColdFusion", "Erlang", "Fortran");


        $term = addcslashes($term, '%_'); // escape LIKE's special characters
        $q = new CDbCriteria(array(
            'select' => 'id, CONCAT(sublocality1, ", ", sublocality2, ", ", sublocality) as locality, CONCAT(geolat, ",", geolng) as geoloc',
            'condition' => "sublocality1 LIKE :term OR sublocality2 LIKE :term OR sublocality1 LIKE :term",
            'params' => array(':term' => "%$term%")
        ));

        $data = LocalityGeoloc::model()->findAll($q);
        $result = array();
        foreach ($data as $dt) {
            $row = array(
                'id' => $dt->id,
                'locality' => trim($dt->locality, ","),
                'geoloc' => $dt->geoloc
            );
            array_push($result, $row);
        }

        echo json_encode($result);
        //$result = CHtml::listData($data, 'id', 'locality');
        //echo json_encode($result);        
    }

    public function actiongetmapinfosolr() {
        Yii::app()->openexchanger->exchangeRates;
        $id = Yii::app()->request->getParam('id');
        // get listing details from solr
        $result = Yii::app()->listingSearch->get("id:$id");
        $data = null;
        if (isset($result->response->numFound) && $result->response->numFound) {
            //echo "Results number is ".$result->response->numFound.'<br />';
            $row = $result->response->docs[0];

            if (!empty($row->filename)) {
                $imagePath = JoyUtilities::getAwsFileUrl('tiny_' . $row->filename, 'listing');
                if (empty($imagePath)) {
                    echo '1st';
                    $imagePath = Yii::app()->baseUrl . '/images/site/img.png';
                }
            } else {
                echo '2nd';
                $imagePath = Yii::app()->baseUrl . '/images/site/img.png';
            }

            $data = '<div style="height:80px;">';
            $data .= CHtml::image($imagePath, '', array());
            $data .= '</div>';

            $data .= '<div>';
            $data .= '<b><a href="' . Yii::app()->urlManager->createUrl('listing/view', array('id' => $row->id)) . '" target="_blank">' . $row->name . '</a></b>';
            $data .= '<br/>';
            $data .= CHtml::encode($row->mediatype);

            $favLink = false;
            if (Yii::app()->user->isGuest) {
                $favLink = true;
            } else {
                $roleId = Yii::app()->user->roleId;
                if ($roleId == 2 || $roleId == 4) {
                    $favLink = true;
                }
            }
            if ($favLink) {
                // check if listing is fav or unfav now
                $favListModal = FavouriteListing::model()->findByAttributes(array('userid' => Yii::app()->user->id, 'listingid' => $id));
                if ($favListModal) {
                    $backUrl = Yii::app()->baseUrl . '/images/site/star_active.png';
                } else {
                    $backUrl = Yii::app()->baseUrl . '/images/site/star.png';
                }
                $data .= '<a href="javascript:void(0);" rel="activationPopup" class="add2fav mapfav" style="background-image: url(' . $backUrl . ')" id="' . $row->id . '"></a>';
                $data .= '<br/>';
            }

            $data .= '<br/>';
            $data .= CHtml::encode(JoyUtilities::formatNumber($row->length, 2) . ' x ' . JoyUtilities::formatNumber($row->width, 2) . ' ' . $row->sizeunit) . "<sup>2</sup>";
            $data .= '</div>';

            $data .= '<div>';
            $data .= '<span class="dyn_ccode">' . CHtml::encode($this->ipCurrencyCode) . '</span> <span class="dyn_price">' . CHtml::encode(JoyUtilities::formatNumber(Yii::app()->openexchanger->convertCurrency($row->price, $row->basecurrency, $this->ipCurrencyCode))) . '</span> / ' . $row->priceduration;
            $data .= '<br/>';
            $data .= '<span><span class="base_ccode">' . CHtml::encode($row->basecurrency) . '</span> <span class="base_price">' . CHtml::encode(JoyUtilities::formatNumber($row->price)) . '</span> / ' . $row->priceduration . '</span>';
            $data .= '</div>';
        } else {
            $data = 'No details found.';
        }
        /* $seconds = 10*60;
          header("Cache-Control: private, max-age=$seconds");
          header("Expires: ".gmdate('r', time()+$seconds)); */
        echo $data;
    }

    /*
     * Get country lat lng detail
     */

    public function actioncountrylatlng() {
        if (isset($_POST['country']) && !empty($_POST['country'])) {
            $countryName = $_POST['country'];
        } else {
            $countryName = Yii::app()->params['map_country'];
        }
        echo $result = Ip2nationCountries::getCountryLatLng($countryName);
    }

    /**
     * Countries list
     */
    public function actionDynamiccountries() {
        $data = Area::getCountryOptions(); // CHtml::listData(Area::model()->findAllByAttributes(array('type'=>'c')), 'id', 'name');
        // echo CHtml::tag('option', array('value'=>''),CHtml::encode('Select state'),true);
        $countries = '[["Select country", ""],';

        foreach ($data as $value => $name) {
            // echo CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
            $countries .= '["' . $name . '", "' . $value . '"],';
        }

        echo $countries = rtrim($countries, ',') . ']';
    }

    /**
     * States list which are in solr
     */
    public function actionPrioritystates() {
        $data = Area::getPriorityStateOptions((int) $_POST['id']);
        $states = '[["Select state", ""],';

        foreach ($data as $value => $name) {
            // echo CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
            $states .= '["' . $name . '", "' . $value . '"],';
        }
        echo $states = rtrim($states, ',') . ']';
    }

    /**
     * Cities list which are in solr
     */
    public function actionPrioritycities() {
        $data = Area::getPriorityCityOptions((int) $_POST['id']);
        $cities = '[["Select city", ""],';

        foreach ($data as $value => $name) {
            // echo CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
            $cities .= '["' . $name . '", "' . $value . '"],';
        }

        echo $cities = rtrim($cities, ',') . ']';
    }

    /**
     * States list
     */
    public function actionDynamicstates() {
        $data = Area::getStateOptions((int) $_POST['id']); //CHtml::listData(Area::model()->findAllByAttributes(array('type'=>'s', 'parentid'=>(int)$_POST['id'])), 'id', 'name');
        // echo CHtml::tag('option', array('value'=>''),CHtml::encode('Select state'),true);
        $states = '[["Select state", ""],';

        foreach ($data as $value => $name) {
            // echo CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
            $states .= '["' . $name . '", "' . $value . '"],';
        }

        echo $states = rtrim($states, ',') . ']';
    }

    /**
     * Cities list
     */
    public function actionDynamiccities() {
        $data = Area::getCityOptions((int) $_POST['id']); //CHtml::listData(Area::model()->findAllByAttributes(array('type'=>'ci', 'parentid'=>(int)$_POST['id'])), 'id', 'name');
        // echo CHtml::tag('option', array('value'=>''),CHtml::encode('Select city'),true);
        $cities = '[["Select city", ""],';
        foreach ($data as $value => $name) {
            // echo CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
            $cities .= '["' . $name . '", "' . $value . '"],';
        }
        echo $cities = rtrim($cities, ',') . ']';
    }

    /*
     * Favourite Listing
     */

    public function actionFavlistingtoggle() {
        $userId = Yii::app()->request->getQuery('id');
        $listingId = Yii::app()->request->getQuery('listingid');
        //$roleId = JoyUtilities::getUserRoleId($userId);
        $roleId = Yii::app()->user->roleId;

        if ($userId == Yii::app()->user->id && ($roleId == 2 || $roleId == 4)) {
            // check if the logged in user id matches with param
            $favListModal = FavouriteListing::model()->findByAttributes(array('userid' => $userId, 'listingid' => $listingId));
            if ($favListModal) {
                // favourite, so remove from favourite
                FavouriteListing::model()->deleteByPk($favListModal->id);
                echo 0;
            } else {
                // not favourite, so save as favourite
                $favList = new FavouriteListing;
                $favList->userid = $userId;
                $favList->listingid = $listingId;
                $favList->datecreated = date("Y-m-d H:i:s");
                $favList->save();
                echo 1;
            }
        }
    }

    /*
     * Return user's favorite listing id
     */

    public function actionUserfavlistids() {
        $userId = Yii::app()->request->getQuery('id');
        //$roleId = JoyUtilities::getUserRoleId($userId);
        $roleId = Yii::app()->user->roleId;
        if ($userId == Yii::app()->user->id && ($roleId == 2 || $roleId == 4)) {
            // check if the logged in user id matches with param
            $favListModal = FavouriteListing::model()->findAllByAttributes(array('userid' => $userId), array('select' => 'listingid'));
            $favListIds = null;
            foreach ($favListModal as $favListing) {
                $favListIds .= $favListing->listingid . ",";
            }
            echo rtrim($favListIds, ",");
        }
    }

    /*
     * Return user's favorite listing id
     */

    public function actionContactSeller() {
        // favorite star link
        $flag = false;
        if (Yii::app()->user->isGuest) {
            $flag = true;
        } else {
            $roleId = Yii::app()->user->roleId;
            if ($roleId == 2 || $roleId == 4) {
                $flag = true;
            }
        }
        $sellerId = Yii::app()->request->getQuery('sellerid');
        $emailMessage = Yii::app()->request->getQuery('message');
        if (strlen($emailMessage) == 0) {
            $flag = false;
            echo 2;
            exit;
        }

        if ($flag) {
            $emailMessage = JoyUtilities::cleanInput($emailMessage);
            $modelEmail = new Email;
            $modelEmail->datecreated = date("Y-m-d H:i:s");
            $modelEmail->byuserid = Yii::app()->user->id;
            $modelEmail->foruserid = $sellerId;
            $modelEmail->type = 1;
            $modelEmail->subject = 'Contact Vendor';
            $emailMessage = nl2br($emailMessage);
            $modelEmail->message = $emailMessage;

            // get touser (seller) details - subscribed or not
            $userModel = User::model()->findByPk($sellerId, array('select' => 'email,subscribe'));
            if ($userModel->subscribe) {
                // user subscribed for email ntfn                    
                // get byuser details
                $fromUserModel = User::model()->findByPk(Yii::app()->user->id, array('select' => 'fname,lname,phonenumber'));
                $mail = new EatadsMailer('contact-seller-sub', $userModel->email, array('emailMessage' => $emailMessage, 'fname' => $fromUserModel->fname, 'lname' => $fromUserModel->lname, 'phoneNumber' => $fromUserModel->phonenumber));
            } else {
                // user not subscribed for email ntfn
                $mail = new EatadsMailer('contact-seller-unsub', $userModel->email);
            }
            // send email to seller
            // send reset pwd link email to user
            $modelEmail->datesent = date("Y-m-d H:i:s");
            $mail->eatadsSend();
            $userMessage = 'Email sent successfully.';
            echo 1;

            /* if (mail($to, $subject, $emailMessage, $headers)) {
              if ($modelEmail->save()) {
              // record save
              $userMessage = 'Email sent successfully.';
              echo 1;
              } else {
              // record save failure
              $userMessage = 'Failure, Please try again!';
              echo 0;
              }
              } else {
              // email failure
              $userMessage = 'Failure, Please try again!';
              echo 0;
              } */
        } else {
            echo 0;
        }
    }

    public function actionSetLocation() {
        $country = strtolower(Yii::app()->request->getQuery('country'));
        $countryShort = substr(strtoupper(Yii::app()->request->getQuery('country_short')), 0, 2);
        $state = strtolower(Yii::app()->request->getQuery('state'));
        $city = strtolower(Yii::app()->request->getQuery('city'));

        $validCountry = $validState = $validCity = false;
        // check if country exists        
        if ($country != '' && $country != null) {
            $countryId = Area::checkAreaExists($country, 'c', null, $countryShort);
        }
        // check if state exists
        if (is_numeric($countryId) && $state != '' && $state != null) {
            $stateId = Area::checkAreaExists($state, 's', $countryId);
        }
        // check if city exists
        if (is_numeric($stateId) && $city != '' && $city != null) {
            $cityId = Area::checkAreaExists($city, 'ci', $stateId);
        }
        $area = array('c' => array(ucwords($country), $countryId),
            's' => array(ucwords($state), $stateId),
            'ci' => array(ucwords($city), $cityId));
        //header('Content-type: application/json');
        echo json_encode($area);
    }

    /**
     * 
     */
    public function actionSubscribe() {
        $email = strtolower(Yii::app()->request->getQuery('email'));
        $message = null;
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // valid email id
            $MailChimp = new MailChimp(Yii::app()->params['mailChimp']['api_key']);
            $result = $MailChimp->call('lists/subscribe', array(
                'id' => Yii::app()->params['mailChimp']['id'],
                'email' => array('email' => $email),
                    //'merge_vars'        => array('FNAME'=>'J', 'LNAME'=>'Joy'),
                    //'double_optin'      => false,
                    //'update_existing'   => true,
                    //'replace_interests' => false,
                    //'send_welcome'      => false,
            ));

            if (isset($result['name'])) {
                // a generic error message, if name is set
                $message = 'An error has occurred, please try again.';

                if ($result['name'] == 'List_NotSubscribed') {
                    // in case of update and email is not subscribed
                    $message = $email . ' is not subscribed.';
                } else if ($result['name'] == 'List_AlreadySubscribed') {
                    // in case of email already subscribed
                    $message = $email . ' is already subscribed.';
                }
            } else {
                // otherwise success message
                $message = 'Your email has been successfully subscribed.';
            }
        } else {
            // invalid email id
            $message = 'Email address is not valid.';
        }
        echo $message;
    }

    public function actionMassview() {
        $userId = Yii::app()->user->id;

        // add the header line to specify that the content type is JSON
        header("Content-type: application/json");

        // determine the request type                
        $requestType = CHttpRequest::getRequestType();


        // GET (ALL) LIST DRAFT
        if ($requestType == "GET") {
            $listing = Yii::app()->db->createCommand("CALL getListingDraft({$userId}, 1);")->setFetchMode(PDO::FETCH_OBJ)->queryAll();
            $arr = array();
            foreach ($listing as $draft) {
                $arr[] = $draft;
            }
            echo $data = "{\"data\":" . json_encode($arr) . "}";
        }

        // UPDATE LIST DRAFT
        else if ($requestType == "POST") {
            $request_vars = $this->cleanInputArray($_POST);
            $objMassUpload = new ListingDraft();
            $objMassUpload->setScenario('createListing');
            $objMassUpload->attributes = $request_vars;

            $hasError = 0;
            if (!$objMassUpload->validate()) {
                $hasError = 1;
            }

            // get lat long condition from admin/ListingController
            // match lat lng and country/state/city                        
            if (!empty($objMassUpload->geolat) && !empty($objMassUpload->geolng)) {
                $locationInfoArr = JoyUtilities::reverseGeocode($objMassUpload->geolat, $objMassUpload->geolng);
                unset($locationInfoArr['countryCode']);
                $dropdownLocationArr = array(
                    'country' => strtolower($objMassUpload->country),
                    'state' => strtolower($objMassUpload->state),
                    'city' => strtolower($objMassUpload->city)
                );
                $locationInfoArr = array_map('strtolower', $locationInfoArr);
                $misMatchCount = count(array_diff($locationInfoArr, $dropdownLocationArr));
                if ($misMatchCount) {
                    $objMassUpload->addError('geolng', 'Selected locations and the Latitude Longitude provided are different. Please enter valid location details.');
                    $hasError = 1;
                }
            }

            if (!$hasError) {
                $objMassUpload->datemodified = date("Y-m-d H:i:s");
                $objMassUpload->isNewRecord = false;
                $objMassUpload->save();
                echo "success";
            } else {
                //header("HTTP/1.1 500 Internal Server Error");
                $rowNumber = $errorCell = $errorStr = '';
                $errorCounter = 1;
                $modelArr = $objMassUpload->getErrors();

                foreach ($modelArr as $key => $val) {
                    //echo $key; 
                    //print_r($val[0]);
                    //$errorCell = array_search($key, $this->massExcelFields);
                    $errorStr .= $errorCounter . " - " . $objMassUpload->getAttributeLabel($key) . " - " . $val[0] . "<br />";
                    $errorCounter++;
                }
                echo 'Row ' . $request_vars['sn'] . ' has ' . --$errorCounter . ' errors:<br />';
                echo $errorStr;
            }
        }

        // CREATE (1) LIST DRAFT
        else if ($requestType == "PUT") {
            $request_vars = Array();
            parse_str(file_get_contents('php://input'), $request_vars);
            $request_vars = $this->cleanInputArray($request_vars);

            $objMassUpload = new ListingDraft();
            $objMassUpload->setScenario('createListing');

            $objMassUpload->attributes = $request_vars;
            $hasError = 0;
            if (!$objMassUpload->validate()) {
                $hasError = 1;
            }
            // match lat lng and country/state/city                        
            if (!empty($objMassUpload->geolat) && !empty($objMassUpload->geolng)) {
                $locationInfoArr = JoyUtilities::reverseGeocode($objMassUpload->geolat, $objMassUpload->geolng);
                unset($locationInfoArr['countryCode']);
                $dropdownLocationArr = array(
                    'country' => strtolower($objMassUpload->country),
                    'state' => strtolower($objMassUpload->state),
                    'city' => strtolower($objMassUpload->city)
                );
                $locationInfoArr = array_map('strtolower', $locationInfoArr);
                $misMatchCount = count(array_diff($locationInfoArr, $dropdownLocationArr));
                if ($misMatchCount) {
                    $objMassUpload->addError('geolng', 'Selected locations and the Latitude Longitude provided are different. Please enter valid location details.');
                    $hasError = 1;
                }
            }

            if (!$hasError) {    // no error proceed
                $objMassUpload->userid = Yii::app()->user->id;
                $objMassUpload->status = 1;
                $objMassUpload->datecreated = date("Y-m-d H:i:s");
                $objMassUpload->datemodified = date("Y-m-d H:i:s");
                $objMassUpload->save();
                echo "success";
            } else {
                //header("HTTP/1.1 500 Internal Server Error");
                $rowNumber = $errorCell = $errorStr = '';
                $errorCounter = 1;
                $modelArr = $objMassUpload->getErrors();

                foreach ($modelArr as $key => $val) {
                    //echo $key; 
                    //print_r($val[0]);
                    //$errorCell = array_search($key, $this->massExcelFields);
                    $errorStr .= $errorCounter . " - " . $objMassUpload->getAttributeLabel($key) . " - " . $val[0] . "<br />";
                    $errorCounter++;
                }
                echo 'New row ' . ' has ' . --$errorCounter . ' errors:<br />';
                echo $errorStr;
            }

            /* $sql = "INSERT INTO EmployeeTerritories (EmployeeID, TerritoryID) VALUES (" . $employeeID . "," . $territoryId . ")";
              $rs = mysql_query($sql);
              if ($rs) {
              echo true;
              } else {
              header("HTTP/1.1 500 Internal Server Error");
              echo $validationFields;
              } */
        }

        // DELETE (1) LIST DRAFT
        else if ($requestType == "DELETE") {
            $request_vars = Array();
            parse_str(file_get_contents('php://input'), $request_vars);

            $id = $request_vars["id"];
            $rs = Yii::app()->db->createCommand("DELETE FROM `ListingDraft` WHERE id = {$id}")->query();
            if ($rs) {
                echo "success";
            } else {
                //header("HTTP/1.1 500 Internal Server Error");
                echo "Could not delete record.";
            }
        }
    }

    public function audTagStructure($audTagId, $audTagName) {
        $audTagIdArr = array_filter(explode(",", $audTagId));
        $audTagNameArr = array_filter(explode(",", $audTagName));
        if (count($audTagIdArr) == count($audTagNameArr)) {
            $audienceTagArray = array();
            for ($i = 0; $i < count($audTagIdArr); $i++) {
                $audienceTagArray[$i]['text'] = $audTagNameArr[$i];
                $audienceTagArray[$i]['value'] = $audTagIdArr[$i];
            }
            return $audienceTagArray;
        } else {
            return null;
        }
    }

    public function actionManagelist() {
        $userId = Yii::app()->user->id;

        // add the header line to specify that the content type is JSON
        header("Content-type: application/json");

        // determine the request type                
        $requestType = CHttpRequest::getRequestType();


        // GET (ALL) LIST
        $i = 1;
        if ($requestType == "GET") {
            $listing = Yii::app()->db->createCommand("CALL getListingActive({$userId});")->setFetchMode(PDO::FETCH_OBJ)->queryAll();
            $arr = array();
            foreach ($listing as $draft) {
                // create json structure for audiencetag
                $audienceTagArr = $this->audTagStructure($draft->audiencetag, $draft->audiencetagname);
                unset($draft->audiencetag);
                unset($draft->audiencetagname);
                $draft->audiencetag = $audienceTagArr;
                $draft->sn = $i++;
                $arr[] = $draft;
            }
            echo $data = "{\"data\":" . json_encode($arr) . "}";
        }


        // UPDATE LIST
        else if ($requestType == "POST") {
            // cleanInputArray can't clean aud. tags array
            $audienceTagArr = array();
            if (isset($_POST['audiencetag'])) {
                $audienceTagArr = $_POST['audiencetag'];
                unset($_POST['audiencetag']);
            }
            $request_vars = $this->cleanInputArray($_POST);
            $objListing = new Listing();
            $objListing->setScenario('updateListing');

            $objListing->attributes = $request_vars;
            $objListing->byuserid = Yii::app()->user->id;
            $objListing->foruserid = Yii::app()->user->id;
            $hasError = false;

            if (!$objListing->validate()) {
                $hasError = true;
            }

            // match lat lng and country/state/city                        
            if (!empty($objListing->geolat) && !empty($objListing->geolng)) {
                $locationInfoArr = JoyUtilities::reverseGeocode($objListing->geolat, $objListing->geolng);
                unset($locationInfoArr['countryCode']);
                $dropdownLocationArr = array(
                    'country' => strtolower(Area::model()->getAreaName($objListing->countryid)),
                    'state' => strtolower(Area::model()->getAreaName($objListing->stateid)),
                    'city' => strtolower(Area::model()->getAreaName($objListing->cityid))
                );
                $locationInfoArr = array_map('strtolower', $locationInfoArr);
                $misMatchCount = count(array_diff($locationInfoArr, $dropdownLocationArr));
                if ($misMatchCount) {
                    $objListing->addError('geolng', 'Selected locations and the Latitude Longitude provided are different. Please enter valid location details.');
                    $hasError = true;
                }
            }

            $audienceTagsCount = count($audienceTagArr);
            if ($audienceTagsCount > 3 || $audienceTagsCount < 1) {
                $hasError = true;
            }

            if (!$hasError) {
                $objListing->id = $_POST['id'];

                // save weeklyprice in USD, conversion from basecurrency to USD
                $objListing->weeklyprice = Listing::calculateWeeklyPrice($objListing->price, $objListing->pricedurationid, $objListing->basecurrencyid);
                $objListing->solr = 0;
                $objListing->geolat = empty($objListing->geolat) ? NULL : $objListing->geolat;
                $objListing->geolng = empty($objListing->geolng) ? NULL : $objListing->geolng;
                $objListing->length = number_format((float) $objListing->length, 3, '.', '');
                $objListing->width = number_format((float) $objListing->width, 3, '.', '');

                $objListing->datemodified = date("Y-m-d H:i:s");
                $objListing->isNewRecord = false;
                $objListing->save();

                // update audience tags to listing
                $listAudTagModel = new ListingAudienceTag;
                $listAudTagModel->deleteAllByAttributes(array('listingid' => $_POST['id']));
                foreach ($audienceTagArr as $audIds) {
                    $listAudTagModel = new ListingAudienceTag;
                    $listAudTagModel->listingid = $_POST['id'];
                    $listAudTagModel->audiencetagid = $audIds['value'];
                    $listAudTagModel->save();
                }
                Listing::updateSolr($_POST['id']);
                echo "success";
            } else {
                //header("HTTP/1.1 500 Internal Server Error");
                $rowNumber = $errorCell = $errorStr = '';
                $errorCounter = 1;
                $modelArr = $objListing->getErrors();

                foreach ($modelArr as $key => $val) {
                    //echo $key; 
                    //print_r($val[0]);
                    //$errorCell = array_search($key, $this->massExcelFields);
                    $errorStr .= $errorCounter . " - " . $objListing->getAttributeLabel($key) . " - " . $val[0] . "<br />";
                    $errorCounter++;
                }
                if ($audienceTagsCount > 3 || $audienceTagsCount < 1) {
                    $errorStr .= $errorCounter . " - Audience tag - Please choose min 1 and max 3 options.<br />";
                    $errorCounter++;
                }
                echo 'Row ' . $_POST['sn'] . ' has ' . --$errorCounter . ' errors:<br />';
                echo $errorStr;
            }
        }

        // CREATE (1) LIST
        else if ($requestType == "PUT") {
            $request_vars = Array();
            parse_str(file_get_contents('php://input'), $request_vars);
            // cleanInputArray can't clean aud. tags array
            $audienceTagArr = $request_vars['audiencetag'];
            unset($request_vars['audiencetag']);
            $request_vars = $this->cleanInputArray($request_vars);


            $objListing = new Listing();
            $objListing->setScenario('createListing');
            $objListing->attributes = $request_vars;
            $hasError = false;

            if (!$objListing->validate()) {
                $hasError = true;
            }
            // match lat lng and country/state/city                        
            if (!empty($objListing->geolat) && !empty($objListing->geolng)) {
                $locationInfoArr = JoyUtilities::reverseGeocode($objListing->geolat, $objListing->geolng);
                unset($locationInfoArr['countryCode']);
                $dropdownLocationArr = array(
                    'country' => strtolower(Area::model()->getAreaName($objListing->countryid)),
                    'state' => strtolower(Area::model()->getAreaName($objListing->stateid)),
                    'city' => strtolower(Area::model()->getAreaName($objListing->cityid))
                );
                $locationInfoArr = array_map('strtolower', $locationInfoArr);
                $misMatchCount = count(array_diff($locationInfoArr, $dropdownLocationArr));
                if ($misMatchCount) {
                    $objListing->addError('geolng', 'Selected locations and the Latitude Longitude provided are different. Please enter valid location details.');
                    $hasError = true;
                }
            }

            $audienceTagsCount = count($audienceTagArr);
            if ($audienceTagsCount > 3 || $audienceTagsCount < 1) {
                $hasError = true;
            }

            if (!$hasError) {
                $objListing->byuserid = Yii::app()->user->id;
                $objListing->foruserid = Yii::app()->user->id;
                // save weeklyprice in USD, conversion from basecurrency to USD
                $objListing->weeklyprice = Listing::calculateWeeklyPrice($objListing->price, $objListing->pricedurationid, $objListing->basecurrencyid);
                $objListing->status = 1;
                $objListing->datecreated = date("Y-m-d H:i:s");
                $objListing->datemodified = date("Y-m-d H:i:s");
                $objListing->save();
                $listingId = $objListing->getPrimaryKey();
                // add audience tags to listing
                foreach ($audienceTagArr as $audIds) {
                    $listAudTagModel = new ListingAudienceTag;
                    $listAudTagModel->listingid = $listingId;
                    $listAudTagModel->audiencetagid = $audIds['value'];
                    $listAudTagModel->save();
                }
                // send email to owner on success
                $userInfo = User::getUserAttributeById(Yii::app()->user->id, 'fname, lname, email');
                $mail = new EatadsMailer('list-add-owner', $userInfo->email, array('listName' => $request_vars['name']));
                $mail->eatadsSend();
                // send email to admin on success                    
                $mail = new EatadsMailer('list-add-admin', Yii::app()->params['adminEmail'], array('listName' => $request_vars['name'], 'fname' => $userInfo->fname, 'lname' => $userInfo->lname));
                $mail->eatadsSend();
                Listing::updateSolr($listingId);
                echo "success";
            } else {
                //header("HTTP/1.1 500 Internal Server Error");
                $rowNumber = $errorCell = $errorStr = '';
                $errorCounter = 1;
                $modelArr = $objListing->getErrors();

                foreach ($modelArr as $key => $val) {
                    //echo $key; 
                    //print_r($val[0]);
                    //$errorCell = array_search($key, $this->massExcelFields);
                    $errorStr .= $errorCounter . " - " . $objListing->getAttributeLabel($key) . " - " . $val[0] . "<br />";
                    $errorCounter++;
                }
                if ($audienceTagsCount > 3 || $audienceTagsCount < 1) {
                    $errorStr .= $errorCounter . " - Audience tag - Please choose min 1 and max 3 options.<br />";
                    $errorCounter++;
                }
                echo 'New row ' . ' has ' . --$errorCounter . ' errors:<br />';
                echo $errorStr;
            }

            /* $sql = "INSERT INTO EmployeeTerritories (EmployeeID, TerritoryID) VALUES (" . $employeeID . "," . $territoryId . ")";
              $rs = mysql_query($sql);
              if ($rs) {
              echo true;
              } else {
              header("HTTP/1.1 500 Internal Server Error");
              echo $validationFields;
              } */
        }

        // DELETE (1) LIST
        /* else if ($requestType == "DELETE") {     
          $request_vars = Array();
          parse_str(file_get_contents('php://input'), $request_vars);

          $id = $request_vars["id"];
          $rs = Yii::app()->db->createCommand("DELETE FROM `ListingDraft` WHERE id = {$id}")->query();
          if ($rs) {
          echo "success";
          } else {
          //header("HTTP/1.1 500 Internal Server Error");
          echo "Could not delete record.";
          }
          } */
    }

    private function cleanInputArray($toCleanArray) {
        $cleanArray = array();
        foreach ($toCleanArray as $key => $val) {
            $cleanArray[$key] = trim($val);
        }
        return $cleanArray;
    }

    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */

    /**
     * Used to return Vendor details we need to pass the alias as a param eg : live-media is 
     * @param type $alias
     * @return json of the company data 
     * 
     */
    public function actionVendorDetails() {
        $alias = $_POST['alias']; // Yii::app()->request->getQuery('alias');
        echo json_encode(UserCompany::getCompanyDataFromAlias($alias));
    }

    public function actionGetListing() {

        $metaKeyword = $pageTitle = '';
        // default solrUrl
        $solrParams = array('fq' => '');
        //companyId
        $companyid = $_POST['companyid'];

        // filter media type 

        $mediaTypeParam = '';
        if (!empty($_POST['mediatypeid'])) {
            $mediaTypeParam = $_POST['mediatypeid'];
            $mediaTypeId = null;
            if (!empty($mediaTypeParam) && is_array($mediaTypeId = explode(",", $mediaTypeParam))) {
                //$solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
                $solrParams['fq'] .= '(';
                foreach ($mediaTypeId as $mt) {
                    if (is_numeric($mt))     // to remove 'multiselect-all'
                        $solrParams['fq'] .= ' mediatypeid:' . $mt . ' OR';
                }
                $solrParams['fq'] = rtrim($solrParams['fq'], 'OR');
                $solrParams['fq'] .= ')';
            }
        }



        //companyid
        $solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
        $solrParams['fq'] .= ' companyid:' . $companyid;

        //lightingid
        $lightTypeParam = '';
        if (!empty($_POST['lightingid'])) {
            $lightTypeParam = $_POST['lightingid'];
            $lightTypeId = null;
            if (!empty($lightTypeParam) && is_array($lightTypeId = explode(",", $lightTypeParam))) {
                //$solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
                $solrParams['fq'] .= ' AND (';
                foreach ($lightTypeId as $mt) {
                    if (is_numeric($mt))     // to remove 'multiselect-all'
                        $solrParams['fq'] .= ' lightingid:' . $mt . ' OR';
                }
                $solrParams['fq'] = rtrim($solrParams['fq'], 'OR');
                $solrParams['fq'] .= ')';
            }
        }


        // filter price slider 
        $priceSlider = '';
        if (!empty($_POST['priceslider'])) {
            $priceSlider = explode(':', $_POST['priceslider']);
            if (count($priceSlider) > 1) {
                // base on currency selected conv to usd to compare weeklyprice
                $newMinPrice = round(Yii::app()->openexchanger->convertCurrency($priceSlider[0], $this->ipCurrencyCode, 'INR'));
                $newMaxPrice = round(Yii::app()->openexchanger->convertCurrency($priceSlider[1], $this->ipCurrencyCode, 'INR'));
                //$criteria['condition']  .= ' AND weeklyprice between '.$priceSlider[0]. ' AND '. $priceSlider[1];
                $solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
                $solrParams['fq'] .= 'weeklyprice:[' . $newMinPrice . ' TO ' . $newMaxPrice . ']';
                //print_r($solrParams);die();
            }
        }


        // proximity
        $proximity = is_numeric(Yii::app()->request->getQuery('proximity')) ? (int) Yii::app()->request->getQuery('proximity') : Yii::app()->params['proximity'];
        // geoloc
        if (!empty($_POST['Lat']) && !empty($_POST['Lng'])) {
            $geoloc = $_POST['Lat'] . ',' . $_POST['Lng'];
            if (!empty($geoloc)) {
                $solrParams['fq'] .= " AND {!geofilt pt=$geoloc sfield=geoloc d=$proximity}";
            }
        }



//Sorting
        if (!empty($_POST['sort'])) {
            $filter = '';
            if ($_POST['sort'] === 'Price') {
                $filter = 'weeklyprice asc';
            } else if ($_POST['sort'] === 'Popularity') {
                $filter = 'pscore desc';
            } else if ($_POST['sort'] === 'Most Recent') {
                $filter = 'datemodified desc';
            }
            $solrParams['sort'] = $filter;
        }

        // solr query 
        $textSearch = '';
        $solrQuery = '';
        if (!empty($_POST['textsearch'])) {
            $textSearch = $_POST['textsearch'];
            if (!empty($textSearch)) {
                $solrQuery = "name:*{$textSearch}* OR description:*{$textSearch}* OR mediatype:*{$textSearch}* OR audiencetag:*{$textSearch}*";
            } else {
                $solrQuery = '*:*';
            }
        } else {
            $solrQuery = '*:*';
        }


        //$solrParams['rows'] = 5;
        // get listing from Solr                
        //$result = Yii::app()->listingSearch->get($solrQuery, 0, 50000, $solrParams);
        // load from 0 if markers already loaded is not in $_GET
        $marker_loaded = (int) Yii::app()->request->getQuery('marker_loaded');
        $marker_loaded = ($marker_loaded > 0) ? $marker_loaded : 0;

        // how many to load - next_toload_count not there then default load count
        $next_toload_count = (int) Yii::app()->request->getQuery('next_toload_count');
        $init_markers = ($next_toload_count > 0) ? $next_toload_count : Yii::app()->params['init_markers'];


        $solrParams['wt'] = 'json';
        //$params['json.nl'] = 'map';
        //$solrParams['fl'] = 'id,lat,lng,ea';
        $solrParams['q'] = $solrQuery;
        $solrParams['start'] = 0; // $marker_loaded; //0;
        $solrParams['rows'] = 50; // $init_markers; //50000;

        $qp = http_build_query($solrParams, null, '&');

        // >>> curl query
        $ch = curl_init();
        $url = Yii::app()->params['solrCurl'] . $qp;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);            // Include header in result? (0 = yes, 1 = no)            
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Should cURL return or print out the data? (true = return, false = print)
        //curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $result = curl_exec($ch);
        curl_close($ch);
        $res = json_decode($result);
        $finalresult = array();
        $data = array();
        foreach ($res->response->docs as $doc) {
            $singleDocs = array();
            $doc->thumbnail = JoyUtilities::getAwsFileUrl('small_' . $doc->filename, 'listing');
            $doc->type = $doc->mediatype;

            if (!empty($_POST['userid'])) {
                $favListModal = FavouriteListing::model()->findByAttributes(array('userid' => $_POST['userid'], 'listingid' => '' . $doc->id));
                if ($favListModal) {
                    $doc->is_favByUser = 1;
                }
            }

            $singleDocs = (array) $doc;
            array_push($data, $singleDocs);
        }

        $finalresult['SiteListing'] = $data;
        echo json_encode($data);
    }

    public function actiongetmarkers() {
        $metaKeyword = $pageTitle = '';
        // default solrUrl
        $solrParams = array('fq' => '');
        //companyId
        $companyid = $_POST['companyid'];

        // filter media type 

        $mediaTypeParam = '';
        if (!empty($_POST['mediatypeid'])) {
            $mediaTypeParam = $_POST['mediatypeid'];
            $mediaTypeId = null;
            if (!empty($mediaTypeParam) && is_array($mediaTypeId = explode(",", $mediaTypeParam))) {
                //$solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
                $solrParams['fq'] .= '(';
                foreach ($mediaTypeId as $mt) {
                    if (is_numeric($mt))     // to remove 'multiselect-all'
                        $solrParams['fq'] .= ' mediatypeid:' . $mt . ' OR';
                }
                $solrParams['fq'] = rtrim($solrParams['fq'], 'OR');
                $solrParams['fq'] .= ')';
            }
        }



        //companyid
        $solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
        $solrParams['fq'] .= ' companyid:' . $companyid;

        //lightingid
        $lightTypeParam = '';
        if (!empty($_POST['lightingid'])) {
            $lightTypeParam = $_POST['lightingid'];
            $lightTypeId = null;
            if (!empty($lightTypeParam) && is_array($lightTypeId = explode(",", $lightTypeParam))) {
                //$solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
                $solrParams['fq'] .= ' AND (';
                foreach ($lightTypeId as $mt) {
                    if (is_numeric($mt))     // to remove 'multiselect-all'
                        $solrParams['fq'] .= ' lightingid:' . $mt . ' OR';
                }
                $solrParams['fq'] = rtrim($solrParams['fq'], 'OR');
                $solrParams['fq'] .= ')';
            }
        }


        // filter price slider 
        $priceSlider = '';
        if (!empty($_POST['priceslider'])) {
            $priceSlider = explode('-', $_POST['priceslider']);
            if (count($priceSlider) > 1) {
                // base on currency selected conv to usd to compare weeklyprice
                $newMinPrice = round(Yii::app()->openexchanger->convertCurrency($priceSlider[0], $this->ipCurrencyCode, 'USD'));
                $newMaxPrice = round(Yii::app()->openexchanger->convertCurrency($priceSlider[1], $this->ipCurrencyCode, 'USD'));
                //$criteria['condition']  .= ' AND weeklyprice between '.$priceSlider[0]. ' AND '. $priceSlider[1];
                $solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
                $solrParams['fq'] .= 'weeklyprice:[' . $newMinPrice . ' TO ' . $newMaxPrice . ']';
            }
        }


        // proximity
        $proximity = is_numeric(Yii::app()->request->getQuery('proximity')) ? (int) Yii::app()->request->getQuery('proximity') : Yii::app()->params['proximity'];

        // geoloc
        if (!empty($_POST['Lat']) && !empty($_POST['Lng'])) {
            $geoloc = $_POST['Lat'] . ',' . $_POST['Lng'];
            if (!empty($geoloc)) {
                $solrParams['fq'] .= " AND {!geofilt pt=$geoloc sfield=geoloc d=$proximity}";
            }
        }


//Sorting
        if (!empty($_POST['sort'])) {
            $filter = '';
            if ($_POST['sort'] === 'Price') {
                $filter = 'weeklyprice asc';
            } else if ($_POST['sort'] === 'Popularity') {
                $filter = 'pscore desc';
            } else if ($_POST['sort'] === 'Most Recent') {
                $filter = 'datemodified desc';
            }
            $solrParams['sort'] = $filter;
        }

        // solr query 
        $textSearch = '';
        $solrQuery = '';
        if (!empty($_POST['textsearch'])) {
            $textSearch = $_POST['textsearch'];
            if (!empty($textSearch)) {
                $solrQuery = "name:*{$textSearch}* OR description:*{$textSearch}* OR mediatype:*{$textSearch}* OR audiencetag:*{$textSearch}*";
            } else {
                $solrQuery = '*:*';
            }
        } else {
            $solrQuery = '*:*';
        }


        //$solrParams['rows'] = 5;
        // get listing from Solr                
        //$result = Yii::app()->listingSearch->get($solrQuery, 0, 50000, $solrParams);
        // load from 0 if markers already loaded is not in $_GET
        $marker_loaded = (int) Yii::app()->request->getQuery('marker_loaded');
        $marker_loaded = ($marker_loaded > 0) ? $marker_loaded : 0;

        // how many to load - next_toload_count not there then default load count
        $next_toload_count = (int) Yii::app()->request->getQuery('next_toload_count');
        $init_markers = ($next_toload_count > 0) ? $next_toload_count : Yii::app()->params['init_markers'];


        $solrParams['wt'] = 'json';
        //$params['json.nl'] = 'map';
        $solrParams['fl'] = 'id,lat,lng,ea';
        $solrParams['q'] = $solrQuery;
        $solrParams['start'] = 0; // $marker_loaded; //0;
        $solrParams['rows'] = 50; // $init_markers; //50000;

        $qp = http_build_query($solrParams, null, '&');

        // >>> curl query
        $ch = curl_init();
        $url = Yii::app()->params['solrCurl'] . $qp;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);            // Include header in result? (0 = yes, 1 = no)            
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Should cURL return or print out the data? (true = return, false = print)
        //curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $result = curl_exec($ch);
        curl_close($ch);
        $res = json_decode($result);
        $markerlist = array();
        $markerlist['Markerslist'] = $res->response->docs;
        echo json_encode($res->response->docs);
    }

    public function actiongetsitedetails() {
        $id = $_POST['siteid'];
        $userid = '';
        if (!empty($_POST['userid'])) {
            $userid = $_POST['userid'];
        }
        //echo $id;die();
        echo Listing::getListingDetails($id, $userid);
    }

    public function actionaddfavorite() {
        $listingId = $_POST['siteid'];
        $userId = $_POST['userid'];

        if (!empty($listingId) && !empty($userId)) {
            // check if the logged in user id matches with param
            $favListModal = FavouriteListing::model()->findByAttributes(array('userid' => $userId, 'listingid' => $listingId));
            if ($favListModal) {
                // favourite, so remove from favourite
                FavouriteListing::model()->deleteByPk($favListModal->id);
                echo 0;
            } else {
                // not favourite, so save as favourite
                $favList = new FavouriteListing;
                $favList->userid = $userId;
                $favList->listingid = $listingId;
                $favList->datecreated = date("Y-m-d H:i:s");
                $favList->save();
                echo 1;
            }
        }
    }

    public function actionRetrivePlan() {
        $userid = $_POST['Userid'];
        $result = array();
        $result['PlanList'] = Plan::getPlansForUser($userid);
        echo json_encode(Plan::getPlansForUser($userid));
    }

    public function actionDeletePlanListing() {
        $planid = $_POST['planid'];
        $listingid = $_POST['listingid'];
        $planListing = PlanListing::model()->findByAttributes(array('planid' => $planid, 'listingid' => $listingid));
        if ($planListing) {
            PlanListing::model()->deleteByPk($planListing->id);
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionAddplan() {

        $userId = $_POST['userid'];
        $name = $_POST['planname'];
        $listingId = $_POST['listingid'];

        if ($userId && $name && $listingId) {
            $planModel = new Plan();
            $planModel->userid = $userId;
            $planModel->name = $name;
            $planModel->status = 1;
            $planModel->datecreated = date('Y-m-d H:i:s');
            //$planModel->datemodified = date('Y-m-d H:i:s');
            $planModel->save();
            echo $planId = $planModel->getPrimaryKey();


            $planListingModel = new PlanListing();
            $planListingModel->planid = $planId;
            $planListingModel->listingid = $listingId;
            $planListingModel->status = 1;
            $planListingModel->datecreated = date('Y-m-d H:i:s');
            $planListingModel->datemodified = date('Y-m-d H:i:s');
            $planListingModel->save();
            echo "Listing added to new Plan";
        }
    }

    public function actionAddInExistingPlan() {

        $userId = $_POST['userid'];
        $planId = $_POST['planid'];
        $listingId = $_POST['listingid'];

        $response = PlanListing::isPlanListingExist($planId, $listingId);
        if ($response) {
            echo "Listing already existing on the plan";
            exit;
        }

        if (!$response && $userId && $planId && $listingId) {
            $planListingModel = new PlanListing();
            $planListingModel->planid = $planId;
            $planListingModel->listingid = $listingId;
            $planListingModel->status = 1;
            $planListingModel->datecreated = date('Y-m-d H:i:s');
            $planListingModel->datemodified = date('Y-m-d H:i:s');
            $planListingModel->save();

            // Show report in admin updating plan modify date
            $planModel = Plan::model()->findByPk($planId);
            $planModel->datemodified = date('Y-m-d H:i:s');
            $planModel->save();
            echo 'Listing added to plan';
        }
    }

    public function actionPlanDetail() {
        $userId = $_POST['userid'];
        $name = $_POST['plan'];
        echo json_encode(Plan::getPlan($userId, $name));
    }

    public function actiongetmediatypes() {
        $companyid = $_POST['companyid'];
        echo json_encode(MediaType::getPriorityMediaTypeForCompany(0, $companyid));
    }

    public function actionSignup() {
        $email = $_POST['email'];
        $phonenumber = $_POST['phone'];
        $product = $_POST['pid'];

        if (!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $user = User::model()->findByAttributes(array('email' => $email));
            if ($user) {
                $userproduct = Userproduct::model()->findByAttributes(array('userid' => $user->id, 'productid' => $product));
                if ($userproduct) {
                    //error
                    echo 'user exists in product';
                } else {

                    $userId = $user->primaryKey;
                    //user product entry
                    $userProduct = new Userproduct;
                    //die($pro);
                    $userProduct->userid = $userId;
                    $userProduct->productid = $product;
                    $userProduct->save();

                    //user role add
//                $user->password = $ph->HashPassword($user->password);
                    // as discussed on 25-02-0214 3:30 pm IST (Pooja Ghulam)
                    // save user registration
                    //               $user->save(false);    // false for not validating again   
                    // Save to User Role table
                    $userRoleModel = new UserRole;
                    $userRoleModel->attributes = array('userid' => $userId, 'roleid' => 3);
                    $userRoleModel->save();

                    $linkModel = new Link;
                    // generate the hash
                    $hash = sha1(uniqid());
                    $linkModel->attributes = array('userid' => $userId, 'hash' => $hash, 'type' => 1, 'datecreated' => date('Y-m-d H:i:s'));
                    if ($linkModel->save()) {

                        // show success message
                        $userMessage = 'Please check your email to activate your account.';
                        // $successModel = 1;
                        // sign up flow changed, login user 
                        //$identity=new LoginForm();
                        //$identity->loginWithoutPassword($model->email);
                        // send reset pwd link email to user
                        $resetLink = Yii::app()->createAbsoluteUrl('account/activateuser', array('code' => $hash));
                        $mail = new EatadsMailer('sign-up', $user->email, array('resetLink' => $resetLink), array('sales@eatads.com'));
                        $mail->eatadsSend();
                        //$mail = new EatadsMailer('sign-up-admin', Yii::app()->params['adminEmail'], array('userType' => JoyUtilities::getUserRole($userId), 
                        //                                                                                    'fname' => $user->email, 
                        //                  
                    }
                    echo 1;
                }
            } else {
                $user = new User;
                $user->email = $email;
                $user->phonenumber = $phonenumber;
                $user->active = 0;
                $user->datecreated = date("Y-m-d H:i:s");

                $user->save();

                $userId = $user->primaryKey;
                //user product entry
                $userProduct = new Userproduct;
                //die($pro);
                $userProduct->userid = $userId;
                $userProduct->productid = $product;
                $userProduct->save();

                //user role add
//                $user->password = $ph->HashPassword($user->password);
                // as discussed on 25-02-0214 3:30 pm IST (Pooja Ghulam)
                // save user registration
                //               $user->save(false);    // false for not validating again   
                // Save to User Role table
                $userRoleModel = new UserRole;
                $userRoleModel->attributes = array('userid' => $userId, 'roleid' => 3);
                $userRoleModel->save();

                $linkModel = new Link;
                // generate the hash
                $hash = sha1(uniqid());
                $linkModel->attributes = array('userid' => $userId, 'hash' => $hash, 'type' => 1, 'datecreated' => date('Y-m-d H:i:s'));
                if ($linkModel->save()) {

                    // show success message
                    $userMessage = 'Please check your email to activate your account.';
                    // $successModel = 1;
                    // sign up flow changed, login user 
                    //$identity=new LoginForm();
                    //$identity->loginWithoutPassword($model->email);
                    // send reset pwd link email to user
                    $resetLink = Yii::app()->createAbsoluteUrl('account/activateuser', array('code' => $hash));
                    $mail = new EatadsMailer('sign-up', $user->email, array('resetLink' => $resetLink), array('sales@eatads.com'));
                    $mail->eatadsSend();
                    //$mail = new EatadsMailer('sign-up-admin', Yii::app()->params['adminEmail'], array('userType' => JoyUtilities::getUserRole($userId), 
                    //                                                                                    'fname' => $user->email, 
                    //                                                                                   'lname' => ''));
                    $mail->eatadsSend();
                }
                //activation mail
                //
                echo 1;
            }
        } else {
            //error message
            echo 0;
        }
    }

    function actionVendorContacts() {
        $companyid = $_POST['companyid'];
        $contacts = UserContacts::model()->getVendorContacts($companyid);
        echo json_encode($contacts);
    }

    public function actionMassUploadListingsForVendor() {
        $useremail = $_POST['useremail'];
        $listingData = json_decode($_POST['data']);

        //  $result = array();
        //print_r(json_encode($data));die();
        //find out the company id too of the userid
//        if ($data) {
//            /*
//             * fetch the geocode and set the value in case if it exists
//             */
//            for ($i =0 ; $i < count($data); $i ++) {
//                
//            }
//        }
        //upload the data

        $lightingArr = Listing::getLighting();
        $sizeunitArr = Listing::getSizeUnit();
        $pricedurationArr = Listing::getPriceDuration();

        for ($i = 0; $i < count($listingData); $i++) {
            $data = $listingData[$i];
            if ($data[0]) {


//                    print_r($data[0]);die();
                $listingObj = new Listing();
                // 0 element : MediaType
                $mediaTypeId = MediaType::isMediaTypeExist($data[0]);
                $listingObj->mediatypeid = $mediaTypeId;


                $countryId = Area::isCountryExist($data[1]);
                $stateId = Area::isStateExist($countryId, $data[2]);
                $cityId = Area::isCityExist($stateId, $data[3]);

                $listingObj->countryid = $countryId;
                $listingObj->stateid = $stateId;
                $listingObj->cityid = $cityId;

                $listingObj->locality = $data[4];

                $listingObj->geolat = $data[5];
                $listingObj->geolng = $data[6];

                $listingObj->name = $data[7];
                $listingObj->length = $data[8];
                $listingObj->width = $data[9];
                $sizeunitId = array_search(ucfirst(strtolower($data[10])), $sizeunitArr);

                // Fetch id of media type and base currency
                $baseCurrencyId = LookupBaseCurrency::isBaseCurrencyExist($data[11]);
                $listingObj->basecurrencyid = $baseCurrencyId;
                $listingObj->otherdata = $data[12];

                // Set Fixed array element
                $lightingId = array_search(ucfirst(strtolower($data[13])), $lightingArr);
                $listingObj->lightingid = $lightingId;

                $listingObj->description = $data[14];
                $listingObj->reach = $data[15];

                //audience tag 16

                $listingObj->price = $data[17];
                $pricedurationId = array_search(ucfirst(strtolower($data[18])), $pricedurationArr);

                $listingObj->sizeunitid = $sizeunitId;
                $listingObj->pricedurationid = $pricedurationId;

                $listingObj->byuserid = Yii::app()->user->id;
                $listingObj->foruserid = User::model()->findByAttributes(array('email' => $useremail))->primaryKey;

                //to be cehcked
                $listingObj->accurate_geoloc = 1;

                $listingObj->weeklyprice = Listing::calculateWeeklyPrice($data[17], $pricedurationId, $baseCurrencyId);


                $listingObj->datecreated = date("Y-m-d H:i:s");
                $listingObj->datemodified = date("Y-m-d H:i:s");

                $listingObj->save();
                $listingId = $listingObj->getPrimaryKey();

                // Insert Audiance tag
                $tagArr = explode(",", $data[16]);
                if (count($tagArr)) {
                    foreach ($tagArr as $tag) {
                        $tagId = AudienceTag::getTagIdByName($tag);
                        $listAudTagModel = new ListingAudienceTag;
                        $listAudTagModel->listingid = $listingId;
                        $listAudTagModel->audiencetagid = $tagId;
                        $listAudTagModel->save();
                    }
                }

                //$rs = Yii::app()->db->createCommand("DELETE FROM `ListingDraft` WHERE id = {$data->id}")->query();
                // add Listing default image
                $listingImageModel = new ListingImage;
                $listingImageModel->listingid = $listingId;
                $listingImageModel->filename = 'default_listing.png';
                //$listingImageModel->filename = '1395226617_1283896814.png';
                $listingImageModel->save();

                // add to solr
                Listing::updateSolr($listingId);
//                    $listingDraftModel = ListingDraft::model()->findByPk($data->id); 
//                    $listingDraftModel->status = 0;
//                    ListingDraft::model()->updateByPk($data->id, $listingDraftModel->attributes);
            }
        }
    }

    private function validateMassUploadRow($arr = array()) {
        
    }

    public function actionSendAvailabilityMailer() {
        $vendorid = Yii::app()->request->getQuery('cid');
        $contacts = array();
        //1. Fetch all active Contacts for this vendor
        if ($vendorid != 0) {
            $contacts = UserContacts::model()->findAllByAttributes(array('vendorid' => $vendorid, 'status' => 1));
        } else if ($vendorid == 0) {
            // find all companies whose availbiliyt is true and then send mails
            // $contacts = UserContacts::model()->findAllByAttributes(array('status' => 1));
        }

//        print_r(json_encode($contacts[0]->attributes));die();
        //2. Create email event
        $eventid = $this->createEmailEvent($vendorid, 'vendor-availability-mailer');

        //2. Send the list to SQS
        //3. call the mail trigger service

        $company = UserCompany::model()->findByPk($vendorid);
        $this->sendAvailbilityMails($contacts, $company, $eventid);
    }

    private function sendAvailbilityMails($contacts = array(), $company, $eventid) {
        foreach (array_chunk($contacts, 5) as $batch) {
            //$messages=array();
            foreach ($batch as $i => $message) {
                //print_r($message->attributes);
                //print_r($company->attributes);
                //print_r($eventid);
                $companyname = $company['name'];
                $emailtracker = Yii::app()->createAbsoluteUrl('ajax/UpdateEmailAnalytics', array('uid' => $message['linkedUserId'], 'eid' => $eventid));
                $url = 'vendor/' . $company['alias'] . '/';
                $vendorshowcaselink = Yii::app()->createAbsoluteUrl('ajax/UpdateEmailopenRedirectToUrl/', array('uid' => $message['linkedUserId'], 'eid' => $eventid, 'url' => $url));
                $mail = new EatadsMailer('vendor-availability-mailer', $message['email'], array('companyname' => $companyname, 'emailtracker' => $emailtracker, 'vendorshowcaselink' => $vendorshowcaselink, 'username' => $message['name']), array('gaurav@eatads.com'));
                $mail->eatadsSend();
                //print_r($mail);
            }
        }
    }

    public function actionUpdateEmailAnalytics() {
        $userid = Yii::app()->request->getQuery('uid');
        $eventid = Yii::app()->request->getQuery('eid');
        Yii::log($userid . " being called for event " . $eventid);
        /*
         * Now here we goona check if the uid with eid exists if yes then update the emailed opened
         */
        $userEmailLog = EmailEventLog::model()->findAllByAttributes(array('userid' => $userid, 'eventid' => $eventid));
        if ($userEmailLog) {
            $totalOpens = $userEmailLog[0]['totalopens'] + 1;
            EmailEventLog::model()->updateByPk($userEmailLog[0]['id'], array('totalopens' => $totalOpens, 'modifieddate' => date("Y-m-d H:i:s")));
        } else {
            $emailLog = new EmailEventLog();
            $emailLog->userid = $userid;
            $emailLog->eventid = $eventid;
            $emailLog->createddate = date("Y-m-d H:i:s");
            $emailLog->modifieddate = date("Y-m-d H:i:s");
            $emailLog->openedFlag = 1;
            $emailLog->clickedFlag = 0;
            $emailLog->totalopens = 1;
            $emailLog->totalclicks = 0;
            $emailLog->save();
            //print_r($emailLog->getErrors());
        }
    }

    public function actionUpdateEmailopenRedirectToUrl() {
        $userid = Yii::app()->request->getQuery('uid');
        $eventid = Yii::app()->request->getQuery('eid');
        $url = Yii::app()->request->getQuery('url');
        Yii::log($userid . " being called for event " . $eventid);

        /*
         * Now here we goona check if the uid with eid exists if yes then update the emailed opened
         * then we gonna check if url not been opened then check the opened too and update the email clicked
         * 
         */
        $userEmailLog = EmailEventLog::model()->findAllByAttributes(array('userid' => $userid, 'eventid' => $eventid));
        if ($userEmailLog) {
            $totalOpens = $userEmailLog[0]['totalclicks'] + 1;
            EmailEventLog::model()->updateByPk($userEmailLog[0]['id'], array('clickedFlag' => 1, 'totalclicks' => $totalOpens, 'modifieddate' => date("Y-m-d H:i:s")));
        } else {
            $emailLog = new EmailEventLog();
            $emailLog->userid = $userid;
            $emailLog->eventid = $eventid;
            $emailLog->createddate = date("Y-m-d H:i:s");
            $emailLog->modifieddate = date("Y-m-d H:i:s");
            $emailLog->openedFlag = 1;
            $emailLog->clickedFlag = 1;
            $emailLog->totalopens = 1;
            $emailLog->totalclicks = 1;
            $emailLog->save();
            //print_r($emailLog->getErrors());
        }
        $this->redirect(JoyUtilities::getVendorShowcaseUrl($url));
    }

//    public function actionPushAvailabilityMailsToQueue($companyid) {
//        $sqs = new EatadSqs();
//        $contacts = UserContacts::model()->getVendorContacts($companyid);
//        $sqs->generateQueueMessage($contacts);
//    }

    public function actionUpdateAvailablity() {
        $listingid = Yii::app()->request->getQuery('listingid');
        $availabilitydate = Yii::app()->request->getQuery('availabilitydate');
        if ($listingid && $availabilitydate) {
            $date = new DateTime($availabilitydate);
            print_r(Listing::model()->updateByPk($listingid, array('availabilitydate' => $date->format("Y-m-d H:i:s"))));
        }
    }

    private function createEmailEvent($cid, $templateName) {
        //vendor-availability-mailer
        $emailEvent = new EmailEvent();
        $emailEvent->name = '';
        $emailEvent->emailtemplate = $templateName;
        $emailEvent->createddate = date("Y-m-d H:i:s");

        if (Yii::app()->user) {
            $emailEvent->createdby = Yii::app()->user->id;
        } else {
            $emailEvent->createdby = null;
        }
        $emailEvent->emailtype = 0; //this is a bulk mailer
        $emailEvent->companyid = $cid;
        $emailEvent->save();
        //print_r($emailEvent['id']);die();
        return $emailEvent['id'];
    }

}
