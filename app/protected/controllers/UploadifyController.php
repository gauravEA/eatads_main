<?php

class UploadifyController extends Controller {
    
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform actions
                'actions' => array('checkExist', 'index', 'deleteFile', 'deletePermanentFile', 'uploadFive'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    public $uploadDir;
    public $uploadDirPathToShowListing;
    
    public function init() {
        $this->uploadDir = Yii::app()->params['fileUpload']['path'].'listing/';
        $this->uploadDirPathToShowListing = Yii::app()->params['fileUpload']['tempAbsPath'].'listing/';
    }
    
    public function actionIndex() {
        $this->render('index');
    }

    public function actionCheckExist() {
        $imagePath = $this->uploadDir; // Relative to the root and should match the upload folder in the uploader script

        if (file_exists($imagePath . $_POST['filename'])) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionUploadFive() {
        // Set the uplaod directory
        $uploadDir = $this->uploadDir;

        // Set the allowed file extensions
        $fileTypes = array('jpg', 'jpeg', 'gif', 'png'); // Allowed file extensions

//        echo $_POST['imageCount'];
//        exit;
        $verifyToken = md5('unique_salt' . $_POST['timestamp']);

        if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $uploadDir = $uploadDir;
            $targetFile = $uploadDir . $_FILES['Filedata']['name'];

            list($width, $height, $type, $attr) = getimagesize($tempFile);
            if( ($width < 212 || $height < 159) || ($width > 1280 || $height > 1024) ) {
                echo 'size error';
            } else {
            
                // Validate the filetype
                $fileParts = pathinfo($_FILES['Filedata']['name']);
                if (in_array(strtolower($fileParts['extension']), $fileTypes)) {

                    // Save the file
                    move_uploaded_file($tempFile, $targetFile);

                    //echo 1;
                    echo $this->uploadDirPathToShowListing.$_FILES['Filedata']['name'];
                } else {

                    // The file type wasn't allowed
                    echo 'Invalid file type.';
                }
            }
        }
    }
    
    public function actionDeleteFile() {
        $fileName = $_POST['filename'];        
        if(@unlink($this->uploadDir.basename($fileName))){
            echo 1;
        } else {
            echo 0;
        }
        
    }

    public function actionDeletePermanentFile() {
        $fileName = Yii::app()->request->getPost('filename');
        $listingId = Yii::app()->request->getPost('listingid');
        $imageId = Yii::app()->request->getPost('imageid');
        $imageCount = ListingImage::getListingImageCount($listingId);

        // at least one image is required
        if($imageCount == 1) {
            echo "error";
        }else {
            Listing::updateSolrStatusByListingId($listingId);            
            $result = ListingImage::deleteListingImage($imageId, $listingId, $fileName);            
            if($result && $fileName != 'default_listing.png') {
                JoyUtilities::deleteAwsListingFile($fileName);
            }
            Listing::updateSolr($listingId);
        }
    }
}