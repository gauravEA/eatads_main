<?php

class StaticPagesController extends Controller {

    public $cacheTime = 86400;
    
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform actions
                'actions' => array('maintenance', 'aboutUs', 'career', 'contactUs', 'faq', 'help', 'press', 'sitemap', 'termsAndCondition', 'demo', 'airport'),
                'users' => array('*'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }    
    public function actionDemo()
    {
        $this->render('demo');
    }
    public function actionTermsAndCondition() {
        $this->pageTitle = Yii::app()->name . ' - Conditions';
        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            'Terms & Condition',
        );
        $this->render('terms_and_condition');
    }

    public function actionAboutUs() {
        $this->pageTitle = Yii::app()->name . ' - About Us';
        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            'About Us',
        );
        $this->render('about_us');
    }

    public function actionContactUs() {
        $this->pageTitle = Yii::app()->name . ' - Contact Us';
        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            'Contact Us',
        );
        
        $model = new Contact;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'contact-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['Contact'])) {
            $_POST['Contact'] = JoyUtilities::cleanInput($_POST['Contact']);
            $model->attributes = $_POST['Contact'];
            //
            // validate user input and redirect to the previous page if valid
            if ($model->validate())
            {   $model->datecreated = date("Y-m-d H:i:s");                
                $model->save();
                
                // Inserts HTML line breaks before all newlines in a string
                $message = nl2br($model->message, false);
                
                // Email to support@eatads.com
                // send email to owner on success
                
                $mail = new EatadsMailer('contact-us-admin', Yii::app()->params['supportEmail'], array('emailMessage'=>$message, 'name'=>$model->name, 'email'=>$model->email));
                $mail->eatadsSend();
                
                Yii::app()->user->setFlash('success', "Thank You. We will contact you very soon.");
                $model->unsetAttributes();
            }
        }
        $this->render('contact_us', array('model' => $model));
    }

    public function actionFaq() {
        $this->pageTitle = Yii::app()->name . ' - FAQ';
        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            'FAQ',
        );
        $this->render('faq');
    }

    public function actionHelp() {
        $this->pageTitle = Yii::app()->name . ' - Help';
        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            'Help',
        );
        $this->render('help');
    }

    public function actionSitemap() {
        $this->pageTitle = Yii::app()->name . ' - Site Map';
        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            'Sitemap',
        );
        
        // get the country city list based on listing count        
        $countryCityList = Area::getCountryCitySitemap();
        $this->render('sitemap', array('countryCityList'=>$countryCityList));
    }

    public function actionCareer() {
        $this->pageTitle = Yii::app()->name . ' - Career';
        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            'Career',
        );
        $this->render('career');
    }

    public function actionPress() {
        $this->pageTitle = Yii::app()->name . ' - Press';
        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            'Press',
        );
        $this->render('press');
    }
    
    /**
     * This is the action to handle site donw time.
     */
    
    public function actionMaintenance() {
        $this->render('maintenance');
    }
    
    public function actionAirport() {
        $this->render('airport');
    }
}