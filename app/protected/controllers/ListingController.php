<?php

class ListingController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index', 'view', 'area', 'areaAjax', 'indexAjax'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        
        // check if listing is not in solr, error 404
        if(!Listing::isSolrListing($id)) {
            throw new CHttpException(404,'Listing cannot be found.');
        }
        
        $countryName = Yii::app()->request->getQuery('country');
        $stateName = Yii::app()->request->getQuery('state');
        $cityName = Yii::app()->request->getQuery('city');
        $listingName = Yii::app()->request->getQuery('listingname');
        
        
        $listingUrlData = JoyUtilities::getListingUrl($id);
        $listingUrl = $listingUrlData['url'];
        $listingData = $listingUrlData['listingData'];
        
        if(!$countryName || !$stateName || !$cityName || !$listingName || $listingName != $listingData['listingName']) {
            $this->redirect($listingUrl);
        }
        
        Yii::app()->openexchanger->exchangeRates;        
        //Yii::app()->openexchanger->currency;
        
        // Calculate the popularity for View Details Page
        Listing::setPopularityScore($id, 'ViewDetail');
       
        
        $model = $this->loadModel($id);

        $userCompanyModel = UserCompany::model()->findByAttributes(array('userid'=>$model->foruserid), array('select'=>'name, id, logo')); 
        $companyName = $companyId = $companyLogo = '';
        if($userCompanyModel) {            
            $companyName = $userCompanyModel->name;
            $companyId = $userCompanyModel->id;
            $companyLogo = $userCompanyModel->logo;            
        }
        
        // For Meta Keywords
        $audienceTagArr = array();
        $audienceTags = ListingAudienceTag::getListingAudienceTag($id);        
        foreach($audienceTags as $audienceTag){
            $audienceTagArr[] = $audienceTag->audiencetag->name;            
        }
        
        // Set page meta keyword and page title
        $meta_keyword = str_replace(",", " ".$model->city->name.",", Yii::app()->params['meta_keyword']) ." ".$model->city->name;                
        Yii::app()->clientScript->registerMetaTag($meta_keyword .",". implode(",",$audienceTagArr) .",". $companyName, 'keywords', null, array('id'=>'meta_keywords'), 'meta_keywords');
        $this->pageTitle = $model->name." ".$model->mediatype->name ." ". $model->city->name ." ";
        
        // Page Breadcrumb
        $this->breadcrumbs=array(
            'Home' => JoyUtilities::getHomeUrl(),
            $model->country->name => Yii::app()->UrlManager->createUrl('listing/index', array('countryid' => $model->countryid) ),            
            $model->state->name => Yii::app()->UrlManager->createUrl('listing/index', array('countryid' => $model->countryid, 'stateid' => $model->stateid) ),
            $model->city->name." " => Yii::app()->UrlManager->createUrl('listing/index', array('countryid' => $model->countryid, 'stateid' => $model->stateid, 'cityid' => $model->cityid) ),
            $model->name,
        );

        $roleId = '';
        if(!Yii::app()->user->isGuest) {
            $roleId = Yii::app()->user->roleId;
        }
        //echo '<pre>';
        //print_r($model);
        
        $planData = Plan::getPlanWithListCount(Yii::app()->user->id);       
        $this->render('view', array(
            'model' => $model,
            'companyName' => $companyName,
            'companyId' => $companyId,
            'userId' => $model->foruserid,
            'roleId' => $roleId,
            'companyLogo' => $companyLogo,
            'planData' => $planData
        ));                
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        Yii::app()->openexchanger->exchangeRates;        
        if(Yii::app()->request->pathInfo == 's/mosaicview') {
            $listType = 'm';
        } else {
            // $listType = Yii::app()->request->getQuery('lt')? Yii::app()->request->getQuery('lt'): '';
            $listType = Yii::app()->request->getQuery('type')? Yii::app()->request->getQuery('type'): '';
        }
        //$metaKeyword = '';
        $pageTitle = '';
        $geoLatLng = Yii::app()->request->getQuery('geoloc');        
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $upperLimit = Yii::app()->params['records_per_page'];
        $lowerLimit = ($page  - 1) * $upperLimit;
        
        if(isset($_REQUEST['ajax'])){
            $this->forward('indexAjax');
        }
        // used in index.php
        $dropDownFilter = array('1' => 'Most recent', '2' => 'Price', '3' => 'Popular');

        // default solrUrl
        $solrParams = array('sort' => 'datemodified desc', 'fq' => '');
        
        $breadcrumbArr = array(
            'Home' => JoyUtilities::getHomeUrl(),
        );
        
        // dropdown filter
        $filterId = Yii::app()->request->getQuery('filterid');
        if ($filterId == 2) {
            //$criteria['order'] = 'price DESC';
            $solrParams['sort'] = 'weeklyprice desc ';
        } else if ($filterId == 3) {
            //$criteria['order'] = 't.pscore DESC';
            $solrParams['sort'] = 'pscore desc';
        } else {
            //$criteria['order'] = 'datemodified DESC';
            $filterId = 1;
            $solrParams['sort'] = 'datemodified desc';
        }

        // filter price slider 
        $priceSlider = explode('-', Yii::app()->request->getQuery('priceslider'));
        if (count($priceSlider) > 1) {            
            // base on currency selected conv to usd to compare weeklyprice
            $newMinPrice = round(Yii::app()->openexchanger->convertCurrency($priceSlider[0], $this->ipCurrencyCode, 'USD'));
            $newMaxPrice = round(Yii::app()->openexchanger->convertCurrency($priceSlider[1], $this->ipCurrencyCode, 'USD'));
            //$criteria['condition']  .= ' AND weeklyprice between '.$priceSlider[0]. ' AND '. $priceSlider[1];
            $solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
            $solrParams['fq'] .= 'weeklyprice:[' . $newMinPrice . ' TO ' . $newMaxPrice . ']';
        }
        
        // filter media type 
        $mediaTypeParam = Yii::app()->request->getQuery('mediatypeid');       
        $mediaTypeId = null;
        if (!empty($mediaTypeParam) && is_array($mediaTypeId = explode(",", $mediaTypeParam))) {
            $solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
            $solrParams['fq'] .= '(';
            foreach($mediaTypeId as $mt) {
                $solrParams['fq'] .= ' mediatypeid:' . $mt . ' OR';
            }
            $solrParams['fq'] = rtrim($solrParams['fq'], 'OR');
            $solrParams['fq'] .= ')';
        }
        
        // distance        
        $proximity = is_numeric(Yii::app()->request->getQuery('proximity')) ? (int)Yii::app()->request->getQuery('proximity') : Yii::app()->params['proximity'];
        // lat lng with proximity
        $solrParams['fq'] .= " AND {!geofilt pt=$geoLatLng sfield=geoloc d=$proximity}";
        
        // solr query 
        $textSearch = Yii::app()->request->getQuery('textsearch');
        if (!empty($textSearch)) {
            $solrQuery = "name:*{$textSearch}* OR description:*{$textSearch}* OR mediatype:*{$textSearch}* OR audiencetag:*{$textSearch}*";
        } else {
            $solrQuery = '*:*';
        }
        // get listing from Solr
        if($listType =='p') {
            $result = Yii::app()->listingSearch->get($solrQuery, 0, 50000, $solrParams);
        } else {
            $result = Yii::app()->listingSearch->get($solrQuery, $lowerLimit, $upperLimit, $solrParams);
        }
        $listing = array();
        if (isset($result->response->numFound) && $result->response->numFound) {
            //echo "Results number is ".$result->response->numFound.'<br />';
            foreach ($result->response->docs as $doc) {
                $listing[] = $doc;
            }
        }
        
        $dataProvider = new ArrayDataProvider($listing, array(
                    'totalItemCount' => isset($result->response->numFound) ? $result->response->numFound : 0,
                    'pagination' => array(
                        'pageSize' => Yii::app()->params['records_per_page'],
                    ),
                ));

        // http://www.yiiframework.com/wiki/232/using-filters-with-cgridview-and-carraydataprovider/#c5501

        /* $dataProvider=new CActiveDataProvider('Listing', array(
          'criteria' => $criteria,
          'pagination' => array(
          'pageSize' => Yii::app()->params['records_per_page'],
          ),
          )); */
        
        // favorite star link
        $favLink = false;
        if (Yii::app()->user->isGuest) {
            $favLink = true;
        } else {
            $roleId = Yii::app()->user->roleId;
            if ($roleId == 2 || $roleId == 4) {
                $favLink = true;
            }
        }
        
        // Create Breadcrumb
        $this->breadcrumbs= $breadcrumbArr;

        // Set page meta keyword and page title
        //$meta_keyword = str_replace(",", " ".$metaKeyword.",", Yii::app()->params['meta_keyword'])." ".$metaKeyword;
        //Yii::app()->clientScript->registerMetaTag($meta_keyword, 'keywords', null, array('id'=>'meta_keywords'), 'meta_keywords');
        
        if(strlen($pageTitle)) { // if breadcrumb has data then showbreadcrumb otherwise default title
            $this->pageTitle = $pageTitle;
        } else {
            $this->pageTitle = Yii::app()->name;
        }
        // if countryid is not set then, set country lat lng manually
        $geoLatLng = isset($geoLatLng) ? $geoLatLng: Yii::app()->params['map_latlng'];
        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'dropDownFilter' => $dropDownFilter,
            //'updateBreadcrumb' => '',
            'pageTitleText' => '',
            'pageMetaKeywordText' => Yii::app()->params['meta_keyword'],
            'listType' => $listType,
            'favLink' => $favLink,
            //'countryName'=>  'TOCHANGE',
            'geoLatLng'=>$geoLatLng,
            'search_params' => array(                
                'mediatypeid'=>$mediaTypeId,
                'priceslider'=>$priceSlider,                
                'filterid'=>$filterId
            ),
        ));
    }
    
    public function actionIndexAjax() {
        Yii::app()->openexchanger->exchangeRates;
        // $listType = Yii::app()->request->getQuery('lt')? Yii::app()->request->getQuery('lt'): '';
        $listType = Yii::app()->request->getQuery('type')? Yii::app()->request->getQuery('type'): '';
        //$metaKeyword = '';
        $pageTitle = '';
        $geoLatLng = Yii::app()->request->getQuery('geoloc');
        // default solrUrl
        $solrParams = array('sort' => 'datemodified desc', 'fq' => '');

        $breadcrumbArr = array(
            'Home' => JoyUtilities::getHomeUrl()            
        );

        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $upperLimit = Yii::app()->params['records_per_page'];
        $lowerLimit = ($page  - 1) * $upperLimit;

        
        // dropdown filter
        $filterId = Yii::app()->request->getQuery('filterid');
        if ($filterId == 2) {
            //$criteria['order'] = 'price DESC';
            $solrParams['sort'] = 'weeklyprice desc ';
        } else if ($filterId == 3) {
            //$criteria['order'] = 't.pscore DESC';
            $solrParams['sort'] = 'pscore desc';
        } else {
            //$criteria['order'] = 'datemodified DESC';
            $solrParams['sort'] = 'datemodified desc';
        }

        // filter price slider 
        $priceSlider = explode('-', Yii::app()->request->getQuery('priceslider'));
        if (count($priceSlider) > 1) {            
            // base on currency selected conv to usd to compare weeklyprice
            $newMinPrice = round(Yii::app()->openexchanger->convertCurrency($priceSlider[0], $this->ipCurrencyCode, 'USD'));            
            $newMaxPrice = round(Yii::app()->openexchanger->convertCurrency($priceSlider[1], $this->ipCurrencyCode, 'USD'));
            //$criteria['condition']  .= ' AND weeklyprice between '.$priceSlider[0]. ' AND '. $priceSlider[1];
            $solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
            $solrParams['fq'] .= 'weeklyprice:[' . $newMinPrice . ' TO ' . $newMaxPrice . ']';            
        }
        /*
        // filter country
        if (is_numeric($countryId = Yii::app()->request->getQuery('countryid'))) {
            //$criteria['condition']  .= " AND t.countryid = $countryId";
            $solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
            $solrParams['fq'] .= ' countryid:' . $countryId;
            
            $countryName = Area::getAreaName($countryId);
            $metaKeyword = $countryName;
            $pageTitle = $countryName;
            $breadcrumbArr[$countryName] = Yii::app()->UrlManager->createUrl('listing/index', array('countryid' => $countryId, 'filterid'=>$filterId, 'lt'=>$listType));
        }*/
        
        // filter media type 
        $mediaTypeParam = Yii::app()->request->getQuery('mediatypeid');       
        $mediaTypeId = null;
        if (!empty($mediaTypeParam) && is_array($mediaTypeId = explode(",", $mediaTypeParam))) {
            $solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
            $solrParams['fq'] .= '(';
            foreach($mediaTypeId as $mt) {
                $solrParams['fq'] .= ' mediatypeid:' . $mt . ' OR';
            }
            $solrParams['fq'] = rtrim($solrParams['fq'], 'OR');
            $solrParams['fq'] .= ')';
        }
        /*
        // filter sizeunit
        if (is_numeric($sizeUnitId = Yii::app()->request->getQuery('sizeunitid'))) {
            $solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
            $solrParams['fq'] .= ' sizeunitid:' . $sizeUnitId;
        }
        */
        
        // distance        
        $proximity = is_numeric(Yii::app()->request->getQuery('proximity')) ? (int)Yii::app()->request->getQuery('proximity') : Yii::app()->params['proximity'];
        // lat lng with proximity
        $solrParams['fq'] .= " AND {!geofilt pt=$geoLatLng sfield=geoloc d=$proximity}";
        
        // solr query 
        $textSearch = Yii::app()->request->getQuery('textsearch');
        if (!empty($textSearch)) {
            $solrQuery = "name:*{$textSearch}* OR description:*{$textSearch}* OR mediatype:*{$textSearch}* OR audiencetag:*{$textSearch}*";
        } else {
            $solrQuery = '*:*';
        }        
        
        // get listing from Solr        
        $result = Yii::app()->listingSearch->get($solrQuery, $lowerLimit, $upperLimit, $solrParams);
        
        $listing = array();
        if (isset($result->response->numFound) && $result->response->numFound) {
            //echo "Results number is ".$result->response->numFound.'<br />';
            foreach ($result->response->docs as $doc) {
                $listing[] = $doc;
            }
        }  
        $dataProvider = new ArrayDataProvider($listing, array(
                    'totalItemCount' => isset($result->response->numFound) ? $result->response->numFound : 0,
                    'pagination' => array(
                        'pageSize' => Yii::app()->params['records_per_page'],
                    ),
                ));
        // favorite star link
        $favLink = false;
        if (Yii::app()->user->isGuest) {
            $favLink = true;
        } else {
            $roleId = Yii::app()->user->roleId;
            if ($roleId == 2 || $roleId == 4) {
                $favLink = true;
            }
        }
        
        // Set page meta keyword and page title
        //$pageMetaKeywordText = str_replace(",", " ".$metaKeyword.",", Yii::app()->params['meta_keyword'])." ".$metaKeyword;
        
        // Create Breadcrumb
        $this->breadcrumbs= $breadcrumbArr;
                
        $this->renderPartial('indexGrid', array(
            'dataProvider' => $dataProvider,
            //'updateBreadcrumb' => true,
            'pageTitleText' => $pageTitle,
            //'pageMetaKeywordText' => $pageMetaKeywordText,
            'listType'=>  $listType,
            'favLink'=>$favLink,
            'countryName'=>  'TOCHANGE'
        ));
    }
    

    public function actionArea() {
        $countryName = '';
        
        $countryId = Yii::app()->request->getQuery('countryid');
        if(!is_numeric($countryId)) {
            $countryId = Area::getAreaIdByAlias($countryId);
        }
        
        $data = Area::getListingCountInCountryStateCity($countryId);
        foreach($data as $key => $value) {
            $countryName = $value['country'];
            //$arr[$value['countryid']]['Name'] = $value['country'];
            $arr[$value['countryid']][$value['stateid']]['name'] = $value['state'];
            $arr[$value['countryid']][$value['stateid']][$value['cityid']] = $value;
        }

        $this->render( 'area_new', array('countryStateCityArr' => $arr, 'countryName' => $countryName) );
    }
    
    
    /**
     *  Backup of old area action
     */
/*    public function actionArea() {
        $countryId = $updateBreadcrumb = 0;
        $countryName = '';

        $countryId = Yii::app()->request->getQuery('countryid');
        
        if(!is_numeric($countryId)) {
            $countryId = Area::getAreaIdByShortCode($countryId);
        }

//        $universityId = Yii::app()->request->getParam('universityId');
//        $uniUrldata = TblUniversityMaster::getUniIdFromHash($universityId);

        // If direct land on this page
        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            'Listings',
        );
        $criteria = array('condition' => 't.type="c"', 'order' => 'name');

        // filter country (from home page or other pages)
        if ($countryId) {
            $countryName = Area::model()->getCountryName($countryId);

            // if tamper country id
            if (!$countryName) {
                Yii::app()->user->setFlash('success', "Country not found.");
                $this->redirect(Yii::app()->UrlManager->createUrl('listing/area'));
            }

            $this->breadcrumbs = array(
                'Home' => JoyUtilities::getHomeUrl(),
                'Listings' => Yii::app()->UrlManager->createUrl('listing/area'),
                $countryName
            );
            $criteria['condition'] = "t.type='ci' AND t.parentid IN (SELECT id FROM `Area` WHERE type='s' AND parentid ={$countryId})";
            $criteria['order'] = 'name';
        }

        $dataProvider = new CActiveDataProvider('Area', array(
                    'criteria' => $criteria,
                    'pagination' => array(
                        'pageSize' => Yii::app()->params['city_per_page'],
                    ),
                ));

        $this->render('area', array(
            'dataProvider' => $dataProvider,
            'countryName' => $countryName,
            'countryId' => $countryId,
            'updateBreadcrumb' => $updateBreadcrumb,    
        ));
    } */

    public function actionAreaAjax() {

        $countryId = Yii::app()->request->getQuery('countryid');

        if ($countryId) {
            $countryName = Area::model()->getCountryName($countryId);
            $criteria = new CDbCriteria;
            $criteria->addCondition("t.type='ci'");
            $criteria->addCondition("t.parentid IN (SELECT id FROM `Area` WHERE type='s' AND parentid ={$countryId})");
            $criteria->order = 'name';

            $this->breadcrumbs = array(
                'Home' => JoyUtilities::getHomeUrl(),
                'Listings' => Yii::app()->UrlManager->createUrl('listing/area'),
                $countryName
            );
        }

        $dataProvider = new CActiveDataProvider('Area', array(
                    'criteria' => $criteria,
                    'pagination' => array(
                        'pageSize' => Yii::app()->params['city_per_page'],
                    ),
                ));




        $this->renderPartial('_areaGrid', array(
            'dataProvider' => $dataProvider,
            'countryName' => $countryName,
            'countryId' => $countryId,
            'updateBreadcrumb' => true,
        ));
    }

//    public function actionArea()
//    {        
//        $criteria = array('condition' => 't.type="c"', 'order'=>'name');
//        
//        // filter country
//        if(is_numeric($countryId = Yii::app()->request->getQuery('countryid'))) {
//            $criteria['condition'] = "t.type='ci' AND t.parentid IN (SELECT id FROM `Area` WHERE type='s' AND parentid ={$countryId})";
//        }
//        // filter city
//        if(is_numeric($cityId = Yii::app()->request->getQuery('cityid'))) {
//            echo $cityId;
//        }
//        // filter country (from home page or other pages)
//        if(is_numeric($cId = Yii::app()->request->getQuery('cid'))) {
//            $criteria['condition'] = "t.type='ci' AND t.parentid IN (SELECT id FROM `Area` WHERE type='s' AND parentid ={$cId})";
//        }
//        
//        $dataProvider=new CActiveDataProvider('Area', array(
//            'criteria' => $criteria,
//            'pagination' => array(
//                'pageSize' => Yii::app()->params['city_per_page'],
//            ),            
//        ));
//        
//        if(is_numeric($countryId)) { 
//            // render partial view for cities
//            $this->renderPartial('area',array(
//                'dataProvider'=>$dataProvider,
//                'countryName'=>Area::model()->getAreaName($countryId),
//                'countryid'=>$countryId,                
//            ));
//        } else if(is_numeric($cId)) {
//            // render view for cities (from home page or other pages)
//            $this->render('area',array(
//                'dataProvider'=>$dataProvider,
//                'countryName'=>Area::model()->getAreaName($cId),
//                'countryid'=>$cId,
//                'fullView'=>true,
//            ));
//        } else {
//            // render full view for countries
//            $this->render('area',array(
//                'dataProvider'=>$dataProvider,
//                'fullView'=>true,
//                //'dropDownFilter'=>$dropDownFilter, 
//                //'listType'=>  Yii::app()->request->getQuery('lt'),
//            ));
//        }
//    }

    /**
     * Manages all models.
     */
//    public function actionAdmin() {
//        $model = new Listing('search');
//        $model->unsetAttributes();  // clear any default values
//        if (isset($_GET['Listing']))
//            $model->attributes = $_GET['Listing'];
//
//        $this->render('admin', array(
//            'model' => $model,
//        ));
//    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Listing the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Listing::model()->with(array('listingImages', 'mediatype', 'basecurrency', 'city', 'country', 'state', 'foruser' => array('select' => 'fname,lname')))->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Listing $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'listing-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
