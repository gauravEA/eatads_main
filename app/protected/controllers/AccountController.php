<?php

class AccountController extends Controller {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform actions
                'actions' => array('index', 'login', 'activateuser', 'signup', 'reset', 'forgot', 'logout', 'emailsuccess'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform action
                'actions' => array('userCompany', 'resendActivationLink'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }    
    
    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $this->redirect(Yii::app()->controller->createUrl('account/login'));
    }

    public function actionResendActivationLink() {

        $this->breadcrumbs=array(
                'Home' => JoyUtilities::getHomeUrl(),
                'Resend Activation Link',
        );

        $linkStatus = 0;
        
        if(!JoyUtilities::isUserActiveFromSession()) {
            
            $userId = Yii::app()->user->id;
            $userModel = User::model()->findByPk($userId);
            $userEmail = $userModel->email;

            $linkModel = new Link;
            // generate the hash
            $hash = sha1(uniqid());
            $linkModel->attributes = array('userid'=>$userId, 'hash'=> $hash, 'type'=> 1, 'datecreated'=>date('Y-m-d H:i:s'));                
            if($linkModel->save()) {

                // show success message
                $linkStatus =  'Please check your email to activate your account.';

                // send reset pwd link email to user
                $resetLink = Yii::app()->createAbsoluteUrl('account/activateuser',array('code'=>$hash));
                $mail = new EatadsMailer('activate-account', $userEmail, array('resetLink'=>$resetLink));
                $mail->eatadsSend();
            } else {
                // show error message
                $linkStatus =  "Could not send email to user.";
            }
        } else {
            $linkStatus =  "Your email is already activated.";
        }
        
        $this->render('sendactivationlink', array('linkStatus' => $linkStatus));
    }
    
    /**
     * Displays the login page
     */
    public function actionLogin() {
        
        //check if logged in redirect to home
        if(!Yii::app()->user->isGuest) {
            $this->redirect(Yii::app()->user->returnUrl);
        }
        $returnUrlParam = Yii::app()->request->getQuery('rurl');
        $ha = Yii::app()->getModule('hybridauth')->getHybridAuth();
        $handle = $ha->getAdapter('Google');
        
        //echo '<pre>';var_dump(Yii::app()->user->id);echo '</pre>';
        //Yii::app()->user->username = $handle->userProfile->email;
        
        //Yii::app()->user->login($handle->getUserProfile(), 3600*24);
        //echo '<pre>';var_dump(Yii::app()->user); echo '</pre>';        
        //$facebook->setUserStatus('Hi');
        
        $model = new LoginForm;
        $model->setscenario('signin');   // set scenario for rules validation
        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $_POST['LoginForm'] = JoyUtilities::cleanInput($_POST['LoginForm']);
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
            {
                if(!empty($returnUrlParam)) {                    
                    $this->redirect($returnUrlParam);
                } else {
                    JoyUtilities::redirectUser(Yii::app()->user->id);
                    $this->redirect(Yii::app()->user->returnUrl);
                }
                
            }
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    public function actionReset() {
        $model = new ResetForm();
        // if it's ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'reset-pwd') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['ResetForm'])) {
            $model->attributes = $_POST['ResetForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate()) {
                // find the userid from link table
                $linkModel = Link::model()->find('hash=:hash AND type=:type', array(':hash' => $model->hiddenHash, ':type' => 0));

                // update the password for the user
                $userModel = User::model()->findByPk($linkModel->userid);
                // CHANGE THE PASSWORD HASHING METHOD
                $ph = new PasswordHash(Yii::app()->params['phpass']['iteration_count_log2'], Yii::app()->params['phpass']['portable_hashes']);
                $userModel->password = $ph->HashPassword($model->password);               
                $userModel->save();
                Yii::app()->user->setFlash('success', "Your password has been reset successfully.");
                $this->redirect(array('account/login'));  // redirect user to login
            } else {
                $this->render('reset', array('invalidLink' => 1));
            }
        }

        // get the reset hash
        $hash = Yii::app()->request->getQuery('code');

        // match the hash with db and check if valid
        $linkModel = Link::model()->find('hash=:hash AND type=:type AND expired=:expired', array(':hash' => $hash, ':type' => 0, ':expired' => 0));

        if ($linkModel != null) {
            // check if link has not expired
            $timeDiff = (time() - strtotime($linkModel->datecreated)) / 3600;
            $linkModel->expired = 1;    // expire the link
            $linkModel->save();         // save the record
            // check link expiration time set in config
            $timeDiff < Yii::app()->params['linkexpiry']['forgot'] ? $this->render('reset', array('model' => $model, 'hiddenHashValue' => $hash)) // display the reset pwd form
                            : $this->render('reset', array('invalidLink' => 1));  // show link expired window  
        } else {
            // show link expired window              
            $this->render('reset', array('invalidLink' => 1));
        }
    }

    public function actionSignup() {        
        //check if logged in redirect to home
        if(!Yii::app()->user->isGuest) {
            $this->redirect(Yii::app()->user->returnUrl);
        }
        
        $model = new User;
        $model->setScenario('createProfile');
        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-signup-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        $userAccountTypeModel = 1;
        $successModel = 0;
        $accountTypeErrorMsg = '';
        $hiddenAccountType = '';
        // collect user input data
        if (isset($_POST['User'])) {
            $_POST['User'] = JoyUtilities::cleanInput($_POST['User']);
            $model->attributes = $_POST['User'];            
            if(empty($_POST['User']['hiddenAccountType'])) {
                // User account type not selected
                $userAccountTypeModel = 1;
                $accountTypeErrorMsg = "Please select the account type to complete Sign up.";                
            } else {
                $userAccountTypeModel = 0;
            }
            
            if (!$userAccountTypeModel && $model->validate()) {                
                // create user model to save record
                //$userModel = new User;
                $model->attributes = $_POST['User'];
                    
                // encrypt password                
                $ph = new PasswordHash(Yii::app()->params['phpass']['iteration_count_log2'], Yii::app()->params['phpass']['portable_hashes']);
                
                $model->password = $ph->HashPassword($model->password);
                // as discussed on 25-02-0214 3:30 pm IST (Pooja Ghulam)
                $model->active = 0;
                $model->datecreated = date("Y-m-d H:i:s");                                
                // save user registration
                $model->save(false);    // false for not validating again                
                $userId = $model->primaryKey;
                // Save to User Role table
                $userRoleModel = new UserRole;
                $userRoleModel->attributes = array('userid'=>$userId, 'roleid'=> (int)$_POST['User']['hiddenAccountType']);
                $userRoleModel->save();
                $linkModel = new Link;
                // generate the hash
                $hash = sha1(uniqid());
                $linkModel->attributes = array('userid'=>$userId, 'hash'=> $hash, 'type'=> 1, 'datecreated'=>date('Y-m-d H:i:s'));                
                if($linkModel->save()) {
                    
                    // subscribe to newsletter
                    $MailChimp = new MailChimp(Yii::app()->params['mailChimp']['api_key']);
                    if($model->subscribe == 1) {
                        $result = $MailChimp->call('lists/subscribe', array(
                                                'id' => Yii::app()->params['mailChimp']['id'],
                                                'email' => array('email'=>$model->email)                                             
                                            ));
                    } else {
                        $result = $MailChimp->call('lists/unsubscribe', array(
                                                'id' => Yii::app()->params['mailChimp']['id'],
                                                'email' => array('email'=>$model->email)                                                
                                            ));
                    }
                    
                     // show success message
                    $userMessage =  'Please check your email to activate your account.';
                    $successModel = 1;
                    // sign up flow changed, login user 
                    $identity=new LoginForm();
                    $identity->loginWithoutPassword($model->email);

                    // send reset pwd link email to user
                    $resetLink = Yii::app()->createAbsoluteUrl('account/activateuser',array('code'=>$hash));
                    $mail = new EatadsMailer('sign-up', $model->email, array('resetLink'=>$resetLink));
                    $mail->eatadsSend();
                    $mail = new EatadsMailer('sign-up-admin', Yii::app()->params['adminEmail'], array('userType' => JoyUtilities::getUserRole($userId), 
                                                                                                        'fname' => $_POST['User']['fname'], 
                                                                                                        'lname' => $_POST['User']['lname']));
                    $mail->eatadsSend();
                   
                    
                    /*if(mail($to, $subject, $message, $headers)){ 
                        // show success message
                        $userMessage =  'Please check your email to activate your account.';
                        $successModel = 1;
                        // sign up flow changed, login user 
                        $identity=new LoginForm();
                        $identity->loginWithoutPassword($model->email);
                        
                    } else {
                        // show error message
                        $userMessage =  'Could not send email to user.';
                    } */                   
                } else {                    
                    // show error message
                    $userMessage =  'Could not send email to user.';
                }
                // redirect to the previous page if valid
                // $this->redirect(Yii::app()->user->returnUrl);
                $model->unsetAttributes();
            }
        } else {            
            $accountType = Yii::app()->request->getQuery('type');
            if(strtolower($accountType) == 'vendor') {
                $userAccountTypeModel = 0; 
                $hiddenAccountType = 3;
            } else if(strtolower($accountType) == 'buyer') {
                $userAccountTypeModel = 0;
                $hiddenAccountType = 2;
            }
        }        
        // display the signup form        
//        $this->render('signup', array('model' => $model, 'userAccountTypeModel'=>$userAccountTypeModel, 'accountTypeErrorMsg'=>$accountTypeErrorMsg, 
//            'hiddenAccountType'=>isset($_POST['User']['hiddenAccountType'])?$_POST['User']['hiddenAccountType']:'', 'successModel'=>$successModel));
        $this->render('signup', array('model' => $model, 'userAccountTypeModel'=>$userAccountTypeModel, 'accountTypeErrorMsg'=>$accountTypeErrorMsg, 
            'hiddenAccountType'=>isset($_POST['User']['hiddenAccountType'])?$_POST['User']['hiddenAccountType']:$hiddenAccountType, 'successModel'=>$successModel));
    }
    
    public function actionEmailsuccess() {
        // $email = strtolower(Yii::app()->request->getQuery('email'));        
        if(empty($_POST)) {
            $this->redirect(Yii::app()->homeUrl);            
        }        
        $email = $_POST['subscribe'];
        $message = null;
        if(isset($email)) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // valid email id
            $MailChimp = new MailChimp(Yii::app()->params['mailChimp']['api_key']);
            $result = $MailChimp->call('lists/subscribe', array(
                'id' => Yii::app()->params['mailChimp']['id'],
                'email' => array('email' => $email),
                //'merge_vars'        => array('FNAME'=>'J', 'LNAME'=>'Joy'),
                //'double_optin'      => false,
                //'update_existing'   => true,
                //'replace_interests' => false,
                //'send_welcome'      => false,
            ));

                if (isset($result['name'])) {
                    // a generic error message, if name is set
                    $message = 'An error has occurred, please try again.';

                    if ($result['name'] == 'List_NotSubscribed') {
                        // in case of update and email is not subscribed
                        $message = $email . ' is not subscribed.';
                    } else if ($result['name'] == 'List_AlreadySubscribed') {
                        // in case of email already subscribed
                        $message = $email . ' is already subscribed.';
                    }
                } else {
                    // otherwise success message
                    $message = 'Your email has been successfully subscribed.';
                }           
            } else {
                // invalid email id
                $message = 'Email address is not valid.';
            }
        }
        // echo $message;
        $this->render('emailsuccess', array('message'=>$message));
    }

    public function actionForgot() {
        if(!Yii::app()->user->isGuest) {
            $this->redirect(Yii::app()->user->returnUrl);
        }
        $model = new LoginForm;
        $model->setscenario('forgot');  // set scenario for rules validation
        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // CVarDumper::dump($_POST);
            // validate user input and redirect to the previous page if valid
            if ($model->validate()) {
                // get the userid from the entered email
                $userModel = User::model()->find(array('condition'=>'email=:email', 'params'=>array(':email'=>$model->email), 'select'=>'id'));

                $userId = $userModel->id;
                // generate the hash
                $hash = sha1(uniqid());
                // generate reset link
                $linkModel = new Link();
                $linkModel->attributes = array('userid'=>$userId, 'hash'=>$hash, 'type'=>0, 'datecreated'=>date('Y-m-d H:i:s'));
                if($linkModel->save()) {
                    // send reset pwd link email to user    
                    $resetLink = Yii::app()->createAbsoluteUrl('account/reset',array('code'=>$hash));
                    $mail = new EatadsMailer('forgot-pwd', $model->email, array('resetLink'=>$resetLink));
                    $mail->eatadsSend();
                    $model->unsetAttributes();
                    // show success message
                    Yii::app()->user->setFlash('success', "Please check your inbox to reset the password.");

                    /*if(@mail($to, $subject, $message, $headers)){ 
                        // show success message
                        Yii::app()->user->setFlash('success', "Please check your inbox to reset the password.");
                    } else {
                        // show error message
                        Yii::app()->user->setFlash('success', "Could not send email to user.");
                    } */                   
                } else {                    
                    // show error message
                    Yii::app()->user->setFlash('success', "Could not send email to user.");
                }
            }
		}
		// display the forgot pwd form
		$this->render('forgot', array('model'=>$model));
	}


    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
    
    /**
    * Company form
    */
    public function actionUserCompany() {
        
        if(!Yii::app()->user->isGuest) {

            // If user company record is already created then redirect on edit profile page.
            $companyData = UserCompany::getCompanyDataByUserId(Yii::app()->user->id);
            if($companyData){
                $this->redirect(Yii::app()->urlManager->createUrl('user/profile/edit'));
            }

            $model = new UserCompany;
            $model->setScenario('createProfile');

            // if it's ajax validation request
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-usercompany-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            // collect user input data
            if (isset($_POST['UserCompany'])) {
                $_POST['UserCompany'] = JoyUtilities::cleanInput($_POST['UserCompany']);
                $model->attributes = $_POST['UserCompany'];
                $model->logo = CUploadedFile::getInstance($model, 'logo');

                $model->validate();
                $hasError = count($model->getErrors());
                
                if($model->logo && !$hasError) {               
                    list($width, $height) = getimagesize($model->logo->tempName);
                    if( $width < 100 || $height < 100 ){
                        $model->addError('logo', 'Logo must be minimum of 100x100 and maximum of 500x500');
                        $hasError = 1;
                    }
                    if( $width > 500 || $height > 500 ){
                        $model->addError('logo', 'Logo must be minimum of 100x100 and maximum of 500x500');
                        $hasError = 1;
                    }
                }
                
                // if there is file type error then unset logo attribute value
                if(count($model->getErrors('logo'))){
                    $model->logo = '';
                }

                // validate user input and redirect to the previous page if valid
                if ( !$hasError ) {
                    //$userCompanyModel = new UserCompany;
                    //$model->attributes = $_POST['UserCompany'];
                    $model->alias = UserCompany::companyNameAlias(JoyUtilities::createAlias($_POST['UserCompany']['name']));
                    $model->userid = Yii::app()->user->id;
                    
                    //$model->logo = CUploadedFile::getInstance($model, 'logo');
                    if($model->logo) { // when user upload his logo
                        $ext = pathinfo($model->logo->name, PATHINFO_EXTENSION);
                        $newFileName = time()."_".Yii::app()->user->id.'.' . $ext;
                        $uploadFilePath = Yii::app()->params['fileUpload']['path'] . 'companylogo/'.$newFileName;
                        $model->logo->saveAs($uploadFilePath);

                        // Create Thumb
                        $imageThumb = new EasyImage($uploadFilePath);
                        $imageThumb->resize(100, 100);
                        $imageThumb->save($uploadFilePath);

                        // Upload on Amazon S3
                        $s3Obj = new EatadsS3();
                        $s3Obj->uploadFile($uploadFilePath, 'companylogo/' . $newFileName);
                        @unlink($uploadFilePath);
                        $model->logo = $newFileName;
                    } else { // if user is not uploading new image then keep previous logo image
                        $model->logo = NULL;
                    }
                    
                    $model->save();

                    // change user active to 1
                    $userModel = User::model()->findByPk($model->userid);
                    $userModel->active = 0;
                    $userModel->save();

                    // redirect to the previous page if valid
                    JoyUtilities::redirectUser(Yii::app()->user->id);
                    $this->redirect(Yii::app()->user->returnUrl);
                }
            }

            //print_r($model->hasErrors()); exit;
            $countryList = $stateList = $cityList = array();
            $countryList = Area::model()->getCountryOptions();
            
            $countryId = $model->countryid;
            if($countryId) {
                $stateList = Area::model()->getStateOptions($countryId);
            }

            $stateId = $model->stateid;
            if($stateId) {
                $cityList = Area::model()->getCityOptions($stateId);
            }
            
            $this->render('usercompany', array(
                        'model' => $model, 
                        'userRoleType'=>Yii::app()->user->roleId,
                        'countryList' => $countryList,
                        'stateList' => $stateList,
                        'cityList' => $cityList,
                    ));
        } else {
            $this->redirect(Yii::app()->homeUrl);
        }
    }

    public function actionActivateuser() {

        // get the user company hash
       $hash = Yii::app()->request->getQuery('code');
 
        // match the hash with db and check if valid
        $linkModel = Link::model()->find('hash=:hash AND type=:type AND expired=:expired', array(':hash' => $hash, ':type' => 1, ':expired' => 0));

        if ($linkModel != null) {
            // check if link has not expired
            $timeDiff = (time() - strtotime($linkModel->datecreated)) / 3600;
            $linkModel->expired = 1;    // expire the link
            $linkModel->save();         // save the record
            // check link expiration time set in config
            
            if($timeDiff < Yii::app()->params['linkexpiry']['signup']) {
                // Fetch user details
                // change to get only email
                $userRow = User::model()->findByPk($linkModel->userid, array('select'=>'email'));
                // auto log in
                if($userRow) {
                    
                    //Activate User
                    User::model()->updateByPk($linkModel->userid, array('active' => 1, 'dateactivated' => date("Y-m-d H:i:s")));
                    
                    $identity=new LoginForm();
                    $identity->loginWithoutPassword($userRow->email);
                    
                    Yii::app()->user->setFlash('success', "Your email has been successfully verified.");
                    // After Login Redirect on Dashboard
                    $this->redirect(JoyUtilities::getDashboardUrl());
                } else {
                    // hash is valid but user dose not exist
                    $this->render('activateuser', array('message' => "User does not exist."));
                }
            } else {
                // hash is valid time has been expired
                $this->render('activateuser', array('message' => "Invalid Link or Link has been expired."));
            }  
        } else {            
            // show link expired / wrong hash code window
            $this->render('activateuser', array('message' => "Invalid Link or Link has been expired."));
         }
     }
}