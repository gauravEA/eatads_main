<?php

class AdminScriptsController extends Controller {

    public $cacheTime = 86400;    
    
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform actions
                'actions' => array('mapOldDb','mapOldDbNew', 'index', 'areaAlias', 'updateWeekly', 'updateListingAudiencetag', 
                    'mapCompanyState', 'deleteFromSolr', 'mapNoImageListings', 'localityGeoloc', 'finalStep', 'mapOldListingLatLong'),
                'users' => array('@'),
                'roles' => array(JoyUtilities::ROLE_ADMIN),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }    

    public $uploadDir;
    public $uploadDirPathToShowListing;
    public $ignoreArr = array('.', '..');
    
    public function init() {
        $this->uploadDir = Yii::app()->params['fileUpload']['path'].'oldlisting/';
        $this->uploadDirPathToShowListing = Yii::app()->params['fileUpload']['tempAbsPath'].'listing/';
    }
    public function actionDeleteFromSolr() {
        $listingIds = array('');
        Yii::app()->listingSearch->deleteMany($listingIds);
    }
    
    public function actionLocalityGeoloc()
    {
        $listingModel = Listing::model()->findAllByAttributes(array('locality_geoloc_status'=>0));    // where status is 0
        $locationInfoArray = array();
        foreach($listingModel as $listing) {            
            if($listing->accurate_geoloc) {
                //echo 'accurate';
                // reverse geocode from lat lng (248)                
                $locationInfoArr = JoyUtilities::reverseGeocode($listing->geolat, $listing->geolng);
                
            } else {
                //echo 'not accurate';
                // geocode from city + locality (213)
                $address = Area::model()->getAreaName($listing->cityid);
                $address.= strlen($listing->locality) ? ",".$listing->locality : '';                
                $locationInfoArr = JoyUtilities::geocode($address);
            }            
            // ignore update if sublocality already exists
            if(!count($locationInfoArr)) {
                // mark status 2
                $listingObj = Listing::model()->findByPk($listing->id);                
                $listingObj->locality_geoloc_status = 2;
                $listingObj->save();
            } else {
                $result = LocalityGeoloc::model()->findByAttributes(array('sublocality'=>$locationInfoArr['sublocality']), array('select'=>'id'));

                if($result) {
                    // set the id
                    $localityGeolocId = $result->id;
                } else {
                    echo '<pre>';print_r($locationInfoArr);
                    $objListingGeoloc = new LocalityGeoloc;
                    $objListingGeoloc->sublocality1 = $locationInfoArr['sublocality1'];
                    $objListingGeoloc->sublocality2 = $locationInfoArr['sublocality2'];
                    $objListingGeoloc->sublocality = $locationInfoArr['sublocality'];
                    /*$objListingGeoloc->city = $locationInfoArr['city'];
                    $objListingGeoloc->state = $locationInfoArr['state'];
                    $objListingGeoloc->country = $locationInfoArr['country'];*/
                    $objListingGeoloc->geolat = $locationInfoArr['lat'];
                    $objListingGeoloc->geolng = $locationInfoArr['lng'];
                    
                    $objListingGeoloc->save();                    
                    $localityGeolocId = $objListingGeoloc->primaryKey;                    
                }                
                // update the LocalityGeoloc id to listings table
                // and set temp status                
                $listingObj = Listing::model()->findByPk($listing->id);
                $listingObj->locality_geoloc_id = $localityGeolocId;
                $listingObj->locality_geoloc_status = 1;
                $listingObj->save();
            }
        }
    }
    
    public function setLocationArray($location) {
        $country = strtolower($location['country']);
        $countryShort = substr(strtoupper($location['countryCode']), 0, 2);
        $state = strtolower($location['state']);
        $city = strtolower($location['city']);


        // check if country exists        
        if ($country != '' && $country != null) {
            $countryId = Area::checkAreaExists($country, 'c', null, $countryShort);
        }
        // check if state exists
        if (is_numeric($countryId) && $state != '' && $state != null) {
            $stateId = Area::checkAreaExists($state, 's', $countryId);
        }
        // check if city exists
        if (is_numeric($stateId) && $city != '' && $city != null) {
            $cityId = Area::checkAreaExists($city, 'ci', $stateId);
        }
        $area = array('c' => $countryId,
            's' => $stateId,
            'ci' => $cityId);
        return $area;
    }

    private function geocode($address)
	{
        $protocol = Yii::app()->params['protocol'];
		$geoCodeURL = $protocol.'maps.googleapis.com/maps/api/geocode/json?address='.$address.'&sensor=false';

		$data = json_decode(file_get_contents($geoCodeURL)); 
        $country = $state = $city = '';

    	foreach($data->results[0]->address_components as $address_component){			
		    if(in_array('country', $address_component->types)){		    	
		        $country = $address_component->long_name;
                $countryCode = $address_component->short_name;		        
		    } else if(in_array('route', $address_component->types)) {		    	
		        $address = $address_component->long_name;                		        
		    } else if(in_array('administrative_area_level_1', $address_component->types)) {
                $state = $address_component->long_name;                
            } else if(in_array('locality', $address_component->types)) {
                $city = $address_component->long_name;                
            } else if(in_array('administrative_area_level_2', $address_component->types) && strlen($city)==0) {
                $city = $address_component->long_name;                
            }		
		}
        $arrayResult = array();
        if($country!='') {
            // IGNORE

        if($state=='') {
           $state = $country;
        }
        if($city=='') {
            $city = $state;
        }
        $location = array(
            'country' => $country,
            'countryCode' => $countryCode,
            'state' => $state,
            'city' => $city
        );
        $arrayResult = $this->setLocationArray($location);
                return $arrayResult;
         } else {
                return $arrayResult;
        }
    }

    public function actionMapCompanyState() {

        $criteria = new CDbCriteria();
        $criteria->select = 'id, backup_country, backup_city, area_status';
        $criteria->condition = "area_status = :area_status AND backup_country != :backup_country AND backup_city != :backup_city";
        $criteria->params = array(':area_status' => 0, ':backup_country'=>'', ':backup_city'=>'');
        $data = UserCompany::model()->findAll($criteria);
        echo '<pre>';
        foreach($data as $company) {
            echo $id = $company->id;
            $address = urlencode($company->backup_country.','.$company->backup_city);
                echo $address .' => ' ;
                die();
            $newAddress = $this->geocode($address);
                $ucObj = UserCompany::model()->findByPk($id);
                if(count($newAddress) == 3) {
                        $ucObj->area_status = 1;
                        $ucObj->countryid = $newAddress['c'];
                        $ucObj->stateid  = $newAddress['s'];
                        $ucObj->cityid = $newAddress['ci'];
                } else {
                        $ucObj->area_status = 2;
                }
                $ucObj->save();
                echo "<script>location.reload();</script>";
                exit();
        }

   }

    
    public function actionIndex() {

        // Set the uplaod directory
        $uploadFilePath = $this->uploadDir;
        
        $fileTypes = array('jpg', 'jpeg', 'gif', 'png'); 
        
        if ($handle = opendir($uploadFilePath)) {
            $i = 0;
            /* This is the correct way to loop over the directory. */
            while (false !== ($uploadedFileName = readdir($handle))) {
                
                $findme   = 'filenameproblem_';
                $pos = strpos($uploadedFileName, $findme);
                if ($pos !== false) {echo ('t');
                    continue;
                }
                $findme   = 'sizeerror_';
                $pos = strpos($uploadedFileName, $findme);
                if ($pos !== false) {echo ('b');
                    continue;
                }
                $findme   = 'typeerror_';
                $pos = strpos($uploadedFileName, $findme);
                if ($pos !== false) { echo ('a');
                    continue;
                }
                $findme   = 'notfoundindb_';
                $pos = strpos($uploadedFileName, $findme);
                if ($pos !== false) {echo ('e');
                    continue;
                }
                $findme   = '#_';
                $pos = strpos($uploadedFileName, $findme);
                if ($pos !== false) { echo('d');
                    continue; 
                }                

                if(!in_array($uploadedFileName, $this->ignoreArr)) { // do not read . and .. files
                    //echo $uploadedFileName; exit();
                    $imageStatus = '';
                    $originalFileWithPath = $uploadFilePath . $uploadedFileName;
                    try {
                        $data = ListingImage::isImageExist($uploadedFileName);
                    } catch (Exception $e) {
                        //rename($originalFileWithPath, $uploadFilePath."filenameproblem_".$uploadedFileName); 
                        copy($originalFileWithPath, $uploadFilePath."../filenameproblem/".$uploadedFileName);
                        @unlink($originalFileWithPath);
                        continue;
                    }
                    if(count($data)) { // if file exist
                        foreach($data as $key => $value)
                        {
                            $listingImageModel = ListingImage::model()->findByPk($value->id);

                            // Check size error
                            list($width, $height, $type, $attr) = getimagesize($uploadFilePath.$uploadedFileName);

                            $ext = pathinfo($uploadedFileName, PATHINFO_EXTENSION);

                            if( ($width < 212 || $height < 159) || ($width > 1280 || $height > 1024) ) {
                                //$listingImageModel->new_status = 2; // size error
                                //$listingImageModel->save();
                                
                                $command = Yii::app()->db->createCommand()->update('ListingImage', array(
                                                                                        'new_status'=>2,
                                                                                    ), 'id=:id', array(':id'=>$value->id));
                                
                                $imageStatus = "sizeerror";
                            } elseif (!in_array(strtolower($ext), $fileTypes)) {
                                //$listingImageModel->new_status = 3; // type error
                                //$listingImageModel->save();
                                
                                $command = Yii::app()->db->createCommand()->update('ListingImage', array(
                                                                                        'new_status'=>3,
                                                                                    ), 'id=:id', array(':id'=>$value->id));
                                
                                $imageStatus = "typeerror";
                            } else {

                                // send them to aws s3
                                $s3Obj = new EatadsS3();
                                //$ext = pathinfo($uploadedFileName, PATHINFO_EXTENSION);
                                $newFileName = time() . "_" . mt_rand() .  '.' . $ext;
                                //exit;

                                
//                                $imageThumb = new EasyImage($originalFileWithPath);
//
//                                $imageThumb->resize(1280, 1024);
//                                $newFileThumbName = $uploadFilePath . $newFileName;
//                                $imageThumb->save($newFileThumbName);
//                                $s3Obj->uploadFile($newFileThumbName, 'listing/' . $newFileName);
//                                @unlink($newFileThumbName);

                                $imageThumb = new EasyImage($originalFileWithPath);

                                $newFileThumbName = $uploadFilePath . $newFileName;
                                
                                copy($originalFileWithPath, $newFileThumbName);
                                $s3Obj->uploadFile($newFileThumbName, 'listing/' . $newFileName);
                                @unlink($newFileThumbName);
                                
                                $imageThumb->resize(487, 310);
                                $newFileThumbName = $uploadFilePath . 'big_' . $newFileName;
                                $imageThumb->save($newFileThumbName);
                                $s3Obj->uploadFile($newFileThumbName, 'listing/big_' . $newFileName);
                                @unlink($newFileThumbName);

                                $imageThumb->resize(212, 160);
                                $newFileThumbName = $uploadFilePath . 'small_' . $newFileName;
                                $imageThumb->save($newFileThumbName);
                                $s3Obj->uploadFile($newFileThumbName, 'listing/small_' . $newFileName);
                                @unlink($newFileThumbName);

                                $imageThumb->resize(102, 74);
                                $newFileThumbName = $uploadFilePath . 'tiny_' . $newFileName;
                                $imageThumb->save($newFileThumbName);
                                $s3Obj->uploadFile($newFileThumbName, 'listing/tiny_' . $newFileName);
                                @unlink($newFileThumbName);
                                //@unlink($originalFileWithPath);

                                //$listingImageModel->new_filename = $newFileName;
                                //$listingImageModel->new_status = 1;

                                //$listingImageModel->save();
                                
                                $command = Yii::app()->db->createCommand()->update('ListingImage', array(
                                                                                        'new_filename'=>$newFileName,
                                                                                        'new_status'=>1,
                                                                                    ), 'id=:id', array(':id'=>$value->id));
                                
                                //$imageStatus .= "#". $value->listingid ."#_";
                                $imageStatus .= "#";
                            }
                        }
                        // rename image with status
                        //rename($originalFileWithPath, $uploadFilePath.$imageStatus.$uploadedFileName);
                        
                        if($imageStatus == "sizeerror") {
                            copy($originalFileWithPath, $uploadFilePath."../sizeerror/".$uploadedFileName);
                        } elseif($imageStatus == "typeerror") {
                            copy($originalFileWithPath, $uploadFilePath."../typeerror/".$uploadedFileName);
                        } else {
                            copy($originalFileWithPath, $uploadFilePath."../listingdone/".$imageStatus.$uploadedFileName);
                        }
                        @unlink($originalFileWithPath);
                        


                    } else { // if file dose not exist for any listing
                       //rename($originalFileWithPath, $uploadFilePath."notfoundindb_".$uploadedFileName); 
                       copy($originalFileWithPath, $uploadFilePath."../notfoundindb/".$uploadedFileName);
                       unlink($originalFileWithPath);
                    }
                    
                }
                $i++;
                echo $i;
                if($i == 10) {
                    echo "<script>location.reload();</script>";
                    exit;
                }
            }
            closedir($handle);
        }
    }
    
    public function actionAreaAlias() {
        $data = Area::model()->findAll();
        $i = 0;
        foreach($data as $key => $area){
            $areaModel = Area::model()->findByPk($area->id);
            $areaModel->alias = JoyUtilities::createAlias($areaModel->name);
            $areaModel->save();
            $i++;
        }
        echo "No of alias created - ";
        echo $i;
    }
    
    public function actionMapNoImageListings() {
        $command = Yii::app()->db->createCommand('SELECT l.id FROM `Listing` as l left join ListingImage as li on l.id = li.listingid where li.listingid is null')->queryAll();
        $i = 0;
        foreach($command as $key => $listingId){
            $listingImageModel = new ListingImage;
            $listingImageModel->listingid = $listingId['id'];
            $listingImageModel->filename = 'default_listing.png';
            $listingImageModel->status = 1;
            $listingImageModel->save();
            $i++;
            if($i == 30) {
                echo "<script>location.reload();</script>"; 
                exit();
            }
        }
        echo "No of alias created - ";
        echo $i;
    }
    
    
    public function actionUpdateWeekly()
    {
        $criteria = new CDbCriteria();
        $criteria->select = 'id, price, pricedurationid, basecurrencyid';
        $criteria->condition = "wpstatus = :wpstatus";
        $criteria->params = array(':wpstatus' => 0);
        
        $data = Listing::model()->findAll($criteria);
        $i = 0;
        foreach($data as $listing) {
            
            $weeklyPrice = Listing::calculateWeeklyPrice($listing->price, $listing->pricedurationid, $listing->basecurrencyid);
            $listingId = $listing->id;            
            $listingModel = Listing::model()->findByPk($listingId); 
            $listingModel->weeklyprice = $weeklyPrice;
            $listingModel->wpstatus = 1;
            $listingModel->save();            
            $i++;
            if($i == 30) {
                echo "<script>location.reload();</script>"; 
                exit();
            }
        }
        echo '1';
        // ALTER TABLE `Listing` ADD `wpstatus` TINYINT(1) NOT NULL DEFAULT '0' AFTER `weeklyprice`;
        // SELECT COUNT(*) FROM Listing WHERE wpstatus=0
    }
    
    public function actionSendPasswordMail() {
        $sendMail = 0;
        
        if($sendMail) {
            $users = User::getResetPasswordMail();
            foreach($users as $key => $user) {
                echo "<pre>";
                print_r($user->email);
                
                $userId = $user->id;
                // generate the hash
                $hash = sha1(uniqid());
                // generate reset link
                $linkModel = new Link();
                $linkModel->attributes = array('userid'=>$userId, 'hash'=>$hash, 'type'=>0, 'datecreated'=>date('Y-m-d H:i:s'));
                if($linkModel->save()) {
                    // send reset pwd link email to user    
                    $resetLink = Yii::app()->createAbsoluteUrl('account/reset',array('code'=>$hash));
                    $mail = new EatadsMailer('reset-password', $user->email, array('resetLink'=>$resetLink));
                    $mail->eatadsSend();
                    
                    // update reset password status 1 - send mail, 2 - some error in sending mail
                    $userModel = User::model()->findByPk($user->id);
                    $userModel->resetpassword = 1;
                    $userModel->save();
                    
                } else {                    
                    $userModel = User::model()->findByPk($user->id);
                    $userModel->resetpassword = 2;
                    $userModel->save();
                }
                
            }
            //print_r($data);
            
        } else {
            
        }
        
    } 
    
    
    public function actionFinalStep() {
        
        $command = Yii::app()->db->createCommand();
        
        /* ******  FavListing ******* */
        // After updating new listing id and user id move this table data to favlisting table
        $FavListingDetails = Yii::app()->db->createCommand('SELECT * FROM `FavouriteListing_copy_newmap`')->queryAll();
        foreach($FavListingDetails as $key => $FavListingDetail) {
            $command->insert('FavouriteListing', array(
                'userid' => $FavListingDetail["userid"], 
                'listingid' => $FavListingDetail["listingid"], 
                'datecreated' => $FavListingDetail["datecreated"], 
            ));
        }
        
        /* ******  Plan ******* */
        // After updating new user id move this table data to plan table
        $planDetails = Yii::app()->db->createCommand('SELECT * FROM `Plan_copy_newmap`')->queryAll();
        foreach($planDetails as $key => $planDetail) {
            $oldPlanId = $planDetail["id"];
            $command->insert('Plan', array(
                    'userid' => $planDetail["userid"], 
                    'name' => $planDetail["name"], 
                    'status' => $planDetail["status"], 
                    'datecreated' => $planDetail["datecreated"], 
                    'datemodified' => $planDetail["datemodified"], 
            ));
            $newPlanId = Yii::app()->db->getLastInsertId();
            
            /* ******  planlisting ******* */
            // Map table with new planid
            Yii::app()->db->createCommand("UPDATE PlanListing_copy_newmap SET planid = $newPlanId WHERE planid = $oldPlanId")->execute();

            /* ******  rfplog ******* */
            Yii::app()->db->createCommand("UPDATE RfpLog_copy SET planid = $newPlanId WHERE planid = $oldPlanId")->execute();

        }
        
        /* ******  planlisting ******* */
        // After updating new plan id move this table data to planlisting table
        $planListingDetails = Yii::app()->db->createCommand('SELECT * FROM `PlanListing_copy_newmap`')->queryAll();
        foreach($planListingDetails as $key => $planListingDetail) {
            $command->insert('PlanListing', array(
                'planid' => $planListingDetail["planid"], 
                'listingid' => $planListingDetail["listingid"], 
                'status' => $planListingDetail["status"], 
                'datecreated' => $planListingDetail["datecreated"], 
                'datemodified' => $planListingDetail["datemodified"], 
            ));
        }

        /* ******  RfpLog ******* */
        // After updating new plan id move this table data to planlisting table
        $rfpLogDetails = Yii::app()->db->createCommand('SELECT * FROM `RfpLog_copy`')->queryAll();
        foreach($rfpLogDetails as $key => $rfpLogDetail) {
            $command->insert('RfpLog', array(
                'planid' => $rfpLogDetail["planid"], 
                'datecreated' => $rfpLogDetail["datecreated"], 
            ));
        }
    }
    
    public function actionMapOldDb() {
        
        set_time_limit(60*60);
        
        
        $userDetails = Yii::app()->db->createCommand('SELECT * FROM `User_copy` WHERE mig_status = 0 ORDER BY id LIMIT 1')->queryAll();
        foreach($userDetails as $id => $userDetail) {            
            $oldUserId = $userDetail['id'];
            echo "User Id - ". $oldUserId ."<br>";
            $userEmailDetail = Yii::app()->db->createCommand('SELECT count(id) AS count FROM User WHERE email = "'.$userDetail["email"].'"')->queryRow();
            
            if($userEmailDetail['count']) {
                // Update record in user Table and use this new user id for further migration
                $command = Yii::app()->db->createCommand();
                $command->update('User', array(
                        'fname' => $userDetail["fname"], 
                        'lname' => $userDetail["lname"], 
                        'email' => $userDetail["email"], 
                        'username' => $userDetail["username"], 
                        'password' => $userDetail["password"], 
                        'phonenumber' => $userDetail["phonenumber"], 
                        'active' => $userDetail["active"], 
                        'status' => $userDetail["status"], 
                        'subscribe' => $userDetail["subscribe"], 
                        'lastlogin' => $userDetail["lastlogin"], 
                        'datecreated' => $userDetail["datecreated"], 
                        'datemodified' => $userDetail["datemodified"], 
                        'dateactivated' => $userDetail["dateactivated"], 
                ), 'email=:email', array(':email'=>$userDetail["email"]));
                $newUserId = $oldUserId;
                echo "New User Id - ". $newUserId ."<br>";
                
                /* ******  UserCompany ******* */

                // Insert usercompany according to newly generated userid
                $userCompanyDetail = array();
                $userCompanyDetail = Yii::app()->db->createCommand('SELECT * FROM `UserCompany_copy` WHERE userid = '.$oldUserId)->queryRow();
                
                $userCompanyNewDetail = array();
                $userCompanyNewDetail = Yii::app()->db->createCommand('SELECT * FROM `UserCompany` WHERE userid = '.$oldUserId)->queryRow();

                // Update only when user has insert his details
                
                   if($userCompanyDetail && count($userCompanyDetail) && $userCompanyNewDetail && count($userCompanyNewDetail)) {
                    $command->update('UserCompany', array(
                        'userid' => $newUserId, 
                        'name' => $userCompanyDetail["name"], 
                        'alias' => $userCompanyDetail["alias"], 
                        'description' => $userCompanyDetail["description"], 
                        'logo' => $userCompanyDetail["logo"], 
                        'websiteurl' => $userCompanyDetail["websiteurl"], 
                        'phonenumber' => $userCompanyDetail["phonenumber"], 
                        'countryid' => $userCompanyDetail["countryid"], 
                        'stateid' => $userCompanyDetail["stateid"], 
                        'cityid' => $userCompanyDetail["cityid"], 
                        'area_status' => $userCompanyDetail["area_status"], 
                        'address1' => $userCompanyDetail["address1"], 
                        'address2' => $userCompanyDetail["address2"], 
                        'postalcode' => $userCompanyDetail["postalcode"], 
                        'facebookprofile' => $userCompanyDetail["facebookprofile"], 
                        'twitterhandle' => $userCompanyDetail["twitterhandle"], 
                        'linkedinprofile' => $userCompanyDetail["linkedinprofile"], 
                        'googleplusprofile' => $userCompanyDetail["googleplusprofile"], 
                        'backup_country' => $userCompanyDetail["backup_country"], 
                        'backup_city' => $userCompanyDetail["backup_city"]
                    ), 'userid=:userid', array(':userid'=>$oldUserId));
                } else if($userCompanyDetail && count($userCompanyDetail)) {
                    $command->insert('UserCompany', array(
                        'userid' => $newUserId, 
                        'name' => $userCompanyDetail["name"], 
                        'alias' => $userCompanyDetail["alias"], 
                        'description' => $userCompanyDetail["description"], 
                        'logo' => $userCompanyDetail["logo"], 
                        'websiteurl' => $userCompanyDetail["websiteurl"], 
                        'phonenumber' => $userCompanyDetail["phonenumber"], 
                        'countryid' => $userCompanyDetail["countryid"], 
                        'stateid' => $userCompanyDetail["stateid"], 
                        'cityid' => $userCompanyDetail["cityid"], 
                        'area_status' => $userCompanyDetail["area_status"], 
                        'address1' => $userCompanyDetail["address1"], 
                        'address2' => $userCompanyDetail["address2"], 
                        'postalcode' => $userCompanyDetail["postalcode"], 
                        'facebookprofile' => $userCompanyDetail["facebookprofile"], 
                        'twitterhandle' => $userCompanyDetail["twitterhandle"], 
                        'linkedinprofile' => $userCompanyDetail["linkedinprofile"], 
                        'googleplusprofile' => $userCompanyDetail["googleplusprofile"], 
                        'backup_country' => $userCompanyDetail["backup_country"], 
                        'backup_city' => $userCompanyDetail["backup_city"]
                    ));
                }
            } else {
                // Insert record in user Table and use this new user id for further migration
                $command = Yii::app()->db->createCommand();
                $command->insert('User', array(
                        'fname' => $userDetail["fname"], 
                        'lname' => $userDetail["lname"], 
                        'email' => $userDetail["email"], 
                        'username' => $userDetail["username"], 
                        'password' => $userDetail["password"], 
                        'phonenumber' => $userDetail["phonenumber"], 
                        'active' => $userDetail["active"], 
                        'status' => $userDetail["status"], 
                        'subscribe' => $userDetail["subscribe"], 
                        'lastlogin' => $userDetail["lastlogin"], 
                        'datecreated' => $userDetail["datecreated"], 
                        'datemodified' => $userDetail["datemodified"], 
                        'dateactivated' => $userDetail["dateactivated"], 
                ));
                $newUserId = Yii::app()->db->getLastInsertId();
                echo "New User Id - ". $newUserId ."<br>";
                
                /* ******  UserRole ******* */
            
                // Insert userrole according to newly generated userid
                $userRoleDetail = Yii::app()->db->createCommand('SELECT * FROM `UserRole_copy` WHERE userid = '.$oldUserId)->queryRow();
                $command->insert('UserRole', array(
                        'userid' => $newUserId, 
                        'roleid' => $userRoleDetail["roleid"]
                ));
                
                /* ******  UserCompany ******* */

                // Insert usercompany according to newly generated userid
                $userCompanyDetail = array();
                $userCompanyDetail = Yii::app()->db->createCommand('SELECT * FROM `UserCompany_copy` WHERE userid = '.$oldUserId)->queryRow();

                // Insert only when user has insert his details
                if($userCompanyDetail && count($userCompanyDetail)) {
                    $command->insert('UserCompany', array(
                        'userid' => $newUserId, 
                        'name' => $userCompanyDetail["name"], 
                        'alias' => $userCompanyDetail["alias"], 
                        'description' => $userCompanyDetail["description"], 
                        'logo' => $userCompanyDetail["logo"], 
                        'websiteurl' => $userCompanyDetail["websiteurl"], 
                        'phonenumber' => $userCompanyDetail["phonenumber"], 
                        'countryid' => $userCompanyDetail["countryid"], 
                        'stateid' => $userCompanyDetail["stateid"], 
                        'cityid' => $userCompanyDetail["cityid"], 
                        'area_status' => $userCompanyDetail["area_status"], 
                        'address1' => $userCompanyDetail["address1"], 
                        'address2' => $userCompanyDetail["address2"], 
                        'postalcode' => $userCompanyDetail["postalcode"], 
                        'facebookprofile' => $userCompanyDetail["facebookprofile"], 
                        'twitterhandle' => $userCompanyDetail["twitterhandle"], 
                        'linkedinprofile' => $userCompanyDetail["linkedinprofile"], 
                        'googleplusprofile' => $userCompanyDetail["googleplusprofile"], 
                        'backup_country' => $userCompanyDetail["backup_country"], 
                        'backup_city' => $userCompanyDetail["backup_city"]
                    ));
                }
            }
            
            
            /* ******  FavListing ******* */
            // Map table with new userid
            Yii::app()->db->createCommand("UPDATE FavouriteListing_copy_newmap SET userid = $newUserId WHERE userid = $oldUserId")->execute();

            /* ******  Plan ******* */
            Yii::app()->db->createCommand("UPDATE Plan_copy_newmap SET userid = $newUserId WHERE userid = $oldUserId")->execute();

            /* ******  Listing ******* */
            
            // Insert Listing according to newly generated userid
            $listingDetails = Yii::app()->db->createCommand('SELECT * FROM `Listing_copy` WHERE foruserid = '.$oldUserId)->queryAll();
            $listingCounter = 0;
            foreach($listingDetails as $key => $listingDetail) {
                
                $oldListingId = $listingDetail['id'];
                echo "Old Listing Id - ". $oldListingId ."<br>";
                
                if($listingDetail["byuserid"] == $listingDetail["foruserid"]) { // If user has created his listing
                    $newByUserId = $newForUserId = $newUserId;
                    
                } else { // if admin created on behalf of user
                    $newByUserId = 1; // after this we update this id with admin id
                    $newForUserId = $newUserId;
                }
                
                // Insert only when user has listing
                $command->insert('Listing', array(
                    'byuserid' => $newByUserId, 
                    'foruserid' => $newForUserId, 
                    'name' => $listingDetail["name"], 
                    'length' => $listingDetail["length"], 
                    'width' => $listingDetail["width"], 
                    'sizeunitid' => $listingDetail["sizeunitid"], 
                    'basecurrencyid' => $listingDetail["basecurrencyid"], 
                    'price' => $listingDetail["price"], 
                    'weeklyprice' => $listingDetail["weeklyprice"], 
                    'wpstatus' => $listingDetail["wpstatus"], 
                    'pricedurationid' => $listingDetail["pricedurationid"], 
                    'otherdata' => $listingDetail["otherdata"], 
                    'countryid' => $listingDetail["countryid"], 
                    'stateid' => $listingDetail["stateid"], 
                    'cityid' => $listingDetail["cityid"], 
                    'locality' => $listingDetail["locality"], 
                    'geolat' => $listingDetail["geolat"], 
                    'geolng' => $listingDetail["geolng"], 
                    'zoomlevel' => $listingDetail["zoomlevel"], 
                    'lightingid' => $listingDetail["lightingid"], 
                    'mediatypeid' => $listingDetail["mediatypeid"], 
                    'description' => $listingDetail["description"], 
                    'status' => $listingDetail["status"], 
                    'pscore' => $listingDetail["pscore"], 
                    'reach' => $listingDetail["reach"], 
                    'solr' => $listingDetail["solr"], 
                    'datecreated' => $listingDetail["datecreated"], 
                    'datemodified' => $listingDetail["datemodified"], 
                    'tag1' => $listingDetail["tag1"], 
                    'tag2' => $listingDetail["tag2"], 
                    'tag3' => $listingDetail["tag3"], 
                    'availability_start' => $listingDetail["availability_start"], 
                    'availability_end' => $listingDetail["availability_end"], 
                    'min_period_num' => $listingDetail["min_period_num"], 
                    'min_period_unit' => $listingDetail["min_period_unit"], 
                    'contact_name' => $listingDetail["contact_name"], 
                    'contact_phone' => $listingDetail["contact_phone"], 
                    'contact_email' => $listingDetail["contact_email"], 
                    'rate_per_unit' => $listingDetail["rate_per_unit"], 
                    'search_keywords' => $listingDetail["search_keywords"], 
                    'area_code' => $listingDetail["area_code"], 
                    'postal' => $listingDetail["postal"], 
                    'loop_length' => $listingDetail["loop_length"], 
                    'daily_spots' => $listingDetail["daily_spots"], 
                    'operating_hours' => $listingDetail["operating_hours"], 
                    'screen_size' => $listingDetail["screen_size"], 
                    'dwell_time' => $listingDetail["dwell_time"], 
                    'other_type' => $listingDetail["other_type"], 
                    'temp_status' => $listingDetail["temp_status"]
                ));
                $newListingId = Yii::app()->db->getLastInsertId();
                echo "New Listing Id - ". $newListingId ."<br>";
                
                
                /* ******  ListingImages ******* */
            
                // Insert ListingImages according to newly generated ListingId
                $listingImageDetails = Yii::app()->db->createCommand('SELECT * FROM `ListingImage_copy1` WHERE listingid = '.$oldListingId)->queryAll();
                foreach($listingImageDetails as $key => $listingImageDetail) {
                    $command->insert('ListingImage', array(
                        'listingid' => $newListingId, 
                        'filename_old' => $listingImageDetail["filename_old"], 
                        'status' => $listingImageDetail["status"], 
                        'filename' => $listingImageDetail["filename"], 
                        'caption' => $listingImageDetail["caption"], 
                        'new_status' => $listingImageDetail["new_status"]
                    ));
                }

                /* ******  ListingAudianceTags ******* */
            
                // Insert ListingAudianceTags according to newly generated ListingId
                $listingAudienceTagDetails = Yii::app()->db->createCommand('SELECT * FROM `ListingAudienceTag_copy` WHERE listingid = '.$oldListingId)->queryAll();
                foreach($listingAudienceTagDetails as $key => $listingAudienceTagDetail) {
                    $command->insert('ListingAudienceTag', array(
                        'listingid' => $newListingId, 
                        'audiencetagid' => $listingAudienceTagDetail["audiencetagid"], 
                    ));
                }

                /* ******  FavListing ******* */
                // Map table with new listingid
                Yii::app()->db->createCommand("UPDATE FavouriteListing_copy_newmap SET listingid = $newListingId WHERE listingid = $oldListingId")->execute();
                
                /* ******  PlanListing ******* */
                Yii::app()->db->createCommand("UPDATE PlanListing_copy_newmap SET listingid = $newListingId WHERE listingid = $oldListingId")->execute();
                
                echo "<br><br>";
                $listingCounter++;
            }
            
            Yii::app()->db->createCommand("UPDATE User_copy SET mig_status = 1 WHERE id = $oldUserId")->execute();
            echo $listingCounter;            
        }        
    }
    
    
    public function actionMapOldListingLatLong() {
        
        set_time_limit(60*60);
        
        $objPHPExcel = Yii::app()->excel;
        $objReader = $objPHPExcel->readCsv();
        $objPHPExcel = $objReader->load('/home/user/Music/legacy_data.csv');
        $locationArr = array();
        // for($i = 2; $i <= 654; $i++) {
        $command = Yii::app()->db->createCommand();
        for($i = 2; $i <= 324; $i++) {
            $cellNumber = 'A'.$i;
            $cellObj = $objPHPExcel->getActiveSheet()->getCell($cellNumber);
            $listingID = $cellObj->getValue();
            
            $cellNumber = 'AI'.$i;
            $cellObjNew = $objPHPExcel->getActiveSheet()->getCell($cellNumber);
            $addressORLatLong = $cellObjNew->getValue();            
            $firstCharacter = substr($addressORLatLong, 0, 1);
            if($firstCharacter == '(') {
                $latLong = ltrim($addressORLatLong, '(');
                $latLong = rtrim($latLong, ')');
                $latLong = str_replace(' ','',$latLong); 
                $latLongArray = explode(",",$latLong);
                $locationInfoArr = JoyUtilities::reverseGeocode($latLongArray[0], $latLongArray[1]);                
                if(!isset($locationInfoArr['country']) || empty($locationInfoArr['country']) 
                    || !isset($locationInfoArr['state']) || empty($locationInfoArr['state'])
                    || !isset($locationInfoArr['city']) || empty($locationInfoArr['city'])) {                    
                    Yii::app()->db->createCommand("UPDATE Listing SET status = 0, new_mig_status = 2 WHERE id = $listingID")->execute();
                } else {
                    $locationArr = Area::model()->setLocationArray($locationInfoArr);   // to update/add location in db
                    $countryId = $locationArr['c'];
                    $stateId = $locationArr['s'];
                    $cityId = $locationArr['ci'];
                    
                    $command->update('Listing', array(
                        'countryid' => $countryId, 
                        'stateid' => $stateId, 
                        'cityid' => $cityId, 
                        'geolat' => number_format($locationInfoArr['lat'], 6), 
                        'geolng' => number_format($locationInfoArr['lng'], 6), 
                        'accurate_geoloc' => 1, 
                        'status' => 1, 
                        'new_mig_status' => 1
                    ), 'id=:id', array(':id'=>$listingID));
                }
                
            } elseif($firstCharacter != '') {
                // reverse geocode to get lat long, from locality and city                
                $address = $addressORLatLong;
                $locationInfoArr = JoyUtilities::geocode($address);
                if(!isset($locationInfoArr['country']) || empty($locationInfoArr['country']) 
                    || !isset($locationInfoArr['state']) || empty($locationInfoArr['state'])
                    || !isset($locationInfoArr['city']) || empty($locationInfoArr['city'])
                    || !isset($locationInfoArr['lat']) || empty($locationInfoArr['lat'])
                    || !isset($locationInfoArr['lng']) || empty($locationInfoArr['lng'])) {
                    
                    $sql = "UPDATE Listing SET status = 0, new_mig_status = 2 WHERE id = $listingID" ;
                    Yii::app()->db->createCommand($sql)->execute();                      
                } else {                    
                    $locationArr = Area::model()->setLocationArray($locationInfoArr);   // to update/add location in db
                    $countryId = $locationArr['c'];
                    $stateId = $locationArr['s'];
                    $cityId = $locationArr['ci'];
                    
                    $command->update('Listing', array(
                        'countryid' => $countryId, 
                        'stateid' => $stateId, 
                        'cityid' => $cityId, 
                        'geolat' => number_format($locationInfoArr['lat'], 6), 
                        'geolng' => number_format($locationInfoArr['lng'], 6), 
                        'accurate_geoloc' => 0, 
                        'status' => 1, 
                        'new_mig_status' => 1
                    ), 'id=:id', array(':id'=>$listingID));

                }                
                
            } else {                
                Yii::app()->db->createCommand("UPDATE Listing SET new_mig_status = 2 WHERE id = $listingID")->execute();
            }            
        }
    }
   
}