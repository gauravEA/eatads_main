<?php

class SiteController extends Controller {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform actions
                'actions' => array('index', 'error', 'login', 'logout'),
                'users' => array('*'),
            ),
            array('deny', // deny this action for all users
                'actions' => array('contact'),
                'users' => array('*'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        
        $roleId = isset(Yii::app()->user->roleId) ? Yii::app()->user->roleId : '';
        if ($roleId == 2) { //Media Buyer
            $this->pageTitle =  "EatAds.com - Media Buyer's Dashboard";
        } elseif ($roleId == 3) { // Media vendor
            $this->pageTitle =  "EatAds.com - Media Owner's Dashboard";
        } elseif ($roleId == 4) { //Third Party
            $this->pageTitle =  "EatAds.com - Service Provider's Dashboard";
        } else {
            $this->pageTitle = Yii::app()->name;
        }
         
        //$model = Area::model()->findAll(array('condition' => 'type="c"', 'order' => 'name', 'limit' => Yii::app()->params['countries_home']));
        /*
            SELECT a.id, a.name, IFNULL(count(l.id),0) as count
            FROM `Area` a
            LEFT JOIN Listing l on l.countryid = a.id
            WHERE a.type='c'
            GROUP BY a.id
            ORDER BY count DESC,name
         */
        $model = Area::getPriorityCountryList(Yii::app()->params['countries_home'], true);
//        $criteria = new CDbCriteria;
//        $criteria->select = 't.id, t.name, IFNULL(count(l.id),0) as count';
//        $criteria->join = 'LEFT JOIN Listing l on l.countryid = t.id';
//        $criteria->condition = "t.type = 'c'";
//        $criteria->group = 't.id';        
//        //$criteria->params = array(":type" => "c");
//        $criteria->order = 'count DESC, name';
//        $criteria->limit = Yii::app()->params['countries_home'];
//        $model = Area::model()->findAll($criteria);
        
        // Post requirement model
        $contactModel = new Contact;
        $contactModel->scenario = 'postRequirements';
        
        $this->render('index', array(
            'model' => $model,
            'contactModel' => $contactModel,
            'ipCurrencyCode' => $this->ipCurrencyCode
        ));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }
    }
        
    /**
     * Displays the contact page
     */
    public function actionContact() {
        
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

}