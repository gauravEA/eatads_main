<?php

class VendorController extends Controller {

    protected $myvar;
    protected $company;

    public function init() {
        Yii::app()->theme = 'vendor';
        $this->layout = "//layouts/vendor_page";
        $this->myvar = Yii::app()->request->getQuery('alias');
    }

    
    public function actionIndex() {
        $this->render('index');
    }
    
    public function actionContacts() {
        $this->redirect(Yii::app()->urlManager->createUrl('contacts/index'));
        $this->render('');
    }
    
    public function actionAvailability() {
        $this->redirect(Yii::app()->urlManager->createUrl('availbilitymanager/index'));
    }
    
    
    public function actionUpdateEmailopenRedirectToUrl() {
        $userid = Yii::app()->request->getQuery('uid');
        $eventid = Yii::app()->request->getQuery('eid');
        $url = Yii::app()->request->getQuery('url');
        Yii::log($userid . " being called for event " . $eventid );
        echo $userid . " being called for event " . $eventid . " redirecting to url " . $url;//die(); 
        $this->redirect('/'.$url);
    }

}
