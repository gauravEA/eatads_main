<?php

class VendorAjaxController extends Controller {
    
    
        /**
     * Used to return Vendor details we need to pass the alias as a param eg : live-media is 
     * @param type $alias
     * @return json of the company data 
     * 
     */
    public function actionVendorDetails() {
        $alias =  $_GET['alias'];// Yii::app()->request->getQuery('alias');
        echo json_encode(UserCompany::getCompanyDataFromAlias($alias));
    }
    
    public function actionGetListings() {
            
        $metaKeyword = $pageTitle = '';
        // default solrUrl
        $solrParams = array('fq' => '');
        //companyId
        $companyid=$_GET['companyid'];
        
        // filter media type 
        
        $mediaTypeParam = '';
        if (!empty($_POST['mediatypeid'])) {
            $mediaTypeParam = $_POST['mediatypeid'];
            $mediaTypeId = null;
            if (!empty($mediaTypeParam) && is_array($mediaTypeId = explode(",", $mediaTypeParam))) {
                //$solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
                 $solrParams['fq'] .= '(';
                foreach ($mediaTypeId as $mt) {
                    if (is_numeric($mt))     // to remove 'multiselect-all'
                        $solrParams['fq'] .= ' mediatypeid:' . $mt . ' OR';
                }
                $solrParams['fq'] = rtrim($solrParams['fq'], 'OR');
                $solrParams['fq'] .= ')';
            }
        }
        
        

        //companyid
        $solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
        $solrParams['fq'] .= ' companyid:' . $companyid;
        
        //lightingid
        $lightTypeParam = '';
        if (!empty($_POST['lightingid'])) {
            $lightTypeParam = $_POST['lightingid'];
            $lightTypeId = null;
            if (!empty($lightTypeParam) && is_array($lightTypeId = explode(",", $lightTypeParam))) {
                //$solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
                 $solrParams['fq'] .= ' AND (';
                foreach ($lightTypeId as $mt) {
                    if (is_numeric($mt))     // to remove 'multiselect-all'
                        $solrParams['fq'] .= ' lightingid:' . $mt . ' OR';
                }
                $solrParams['fq'] = rtrim($solrParams['fq'], 'OR');
                $solrParams['fq'] .= ')';
            }
        }
        
        
        // filter price slider 
        $priceSlider = '';
        if (!empty($_POST['priceslider'])) {
            $priceSlider = explode(':', $_POST['priceslider']);
            if ( count($priceSlider) > 1) {
                // base on currency selected conv to usd to compare weeklyprice
                $newMinPrice = round(Yii::app()->openexchanger->convertCurrency($priceSlider[0], $this->ipCurrencyCode, 'INR'));
                $newMaxPrice = round(Yii::app()->openexchanger->convertCurrency($priceSlider[1], $this->ipCurrencyCode, 'INR'));
                //$criteria['condition']  .= ' AND weeklyprice between '.$priceSlider[0]. ' AND '. $priceSlider[1];
                $solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
                $solrParams['fq'] .= 'weeklyprice:[' . $newMinPrice . ' TO ' . $newMaxPrice . ']';
                //print_r($solrParams);die();
            }            
        }


        // proximity
        $proximity = is_numeric(Yii::app()->request->getQuery('proximity')) ? (int) Yii::app()->request->getQuery('proximity') : Yii::app()->params['proximity'];
        // geoloc
//        $geoloc = $_POST['geoloc'];
//        if (!empty($geoloc)) {
//                    $solrParams['fq'] .= " AND {!geofilt pt=$geoloc sfield=geoloc d=$proximity}";
//        }
        

//Sorting
        if(!empty($_POST['sort'])) {
            $filter = '';
            if ($_POST['sort'] === 'Price') {
                $filter = 'weeklyprice asc';
            } else if ($_POST['sort'] === 'Popularity') {
                $filter = 'pscore desc';
            } else if ($_POST['sort'] === 'Most Recent') {
                $filter = 'datemodified desc';
            }  
            $solrParams['sort'] = $filter;
        }
        
        // solr query 
        $textSearch = '';
        $solrQuery = '';
        if (!empty($_POST['textsearch'])) {
            $textSearch = $_POST['textsearch'];
            if (!empty($textSearch)) {
                $solrQuery = "name:*{$textSearch}* OR description:*{$textSearch}* OR mediatype:*{$textSearch}* OR audiencetag:*{$textSearch}*";
            } else {
                $solrQuery = '*:*';
            }
        } else {
                $solrQuery = '*:*';
            }


        //$solrParams['rows'] = 5;
        // get listing from Solr                
        //$result = Yii::app()->listingSearch->get($solrQuery, 0, 50000, $solrParams);
        // load from 0 if markers already loaded is not in $_GET
        $marker_loaded = (int) Yii::app()->request->getQuery('marker_loaded');
        $marker_loaded = ($marker_loaded > 0) ? $marker_loaded : 0;

        // how many to load - next_toload_count not there then default load count
        $next_toload_count = (int) Yii::app()->request->getQuery('next_toload_count');
        $init_markers = ($next_toload_count > 0) ? $next_toload_count : Yii::app()->params['init_markers'];


        $solrParams['wt'] = 'json';
        //$params['json.nl'] = 'map';
        //$solrParams['fl'] = 'id,lat,lng,ea';
        $solrParams['q'] = $solrQuery;
        
        $start = 0;
        if (!empty($_POST['start'])) {
            $start = $_POST['start'];
        }
                
        $solrParams['start'] = $start; // $marker_loaded; //0;
        $solrParams['rows'] = 50; // $init_markers; //50000;

        $qp = http_build_query($solrParams, null, '&');

        // >>> curl query
        $ch = curl_init();
        $url = Yii::app()->params['solrCurl'] . $qp;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);            // Include header in result? (0 = yes, 1 = no)            
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Should cURL return or print out the data? (true = return, false = print)
        //curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $result = curl_exec($ch);
        curl_close($ch);
        $res = json_decode($result);
        $finalresult = array();
        $data = array();
        foreach ($res->response->docs as $doc) {
            $singleDocs = array();
            $doc->thumbnail = JoyUtilities::getAwsFileUrl('small_'.$doc->filename, 'listing');
            $doc->type = $doc->mediatype;
            
            if (!empty($_POST['userid'])) {
                 $favListModal = FavouriteListing::model()->findByAttributes(array('userid' => $_POST['userid'], 'listingid' => ''.$doc->id));
                if ($favListModal) {
                    $doc->is_favByUser = 1;
                }
            }
            
            $singleDocs = (array)$doc;
            array_push($data, $singleDocs);
        }

        $finalresult['SiteListing'] = $data;
        echo json_encode($finalresult);
    }
    
    
    public function actiongetmarkers() {
        $metaKeyword = $pageTitle = '';
        // default solrUrl
        $solrParams = array('fq' => '');
        //companyId
        $companyid=$_POST['companyid'];
        
        // filter media type 
        
        $mediaTypeParam = '';
        if (!empty($_POST['mediatypeid'])) {
            $mediaTypeParam = $_POST['mediatypeid'];
            $mediaTypeId = null;
            if (!empty($mediaTypeParam) && is_array($mediaTypeId = explode(",", $mediaTypeParam))) {
                //$solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
                 $solrParams['fq'] .= '(';
                foreach ($mediaTypeId as $mt) {
                    if (is_numeric($mt))     // to remove 'multiselect-all'
                        $solrParams['fq'] .= ' mediatypeid:' . $mt . ' OR';
                }
                $solrParams['fq'] = rtrim($solrParams['fq'], 'OR');
                $solrParams['fq'] .= ')';
            }
        }
        
        

        //companyid
        $solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
        $solrParams['fq'] .= ' companyid:' . $companyid;
        
        //lightingid
        $lightTypeParam = '';
        if (!empty($_POST['lightingid'])) {
            $lightTypeParam = $_POST['lightingid'];
            $lightTypeId = null;
            if (!empty($lightTypeParam) && is_array($lightTypeId = explode(",", $lightTypeParam))) {
                //$solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
                 $solrParams['fq'] .= ' AND (';
                foreach ($lightTypeId as $mt) {
                    if (is_numeric($mt))     // to remove 'multiselect-all'
                        $solrParams['fq'] .= ' lightingid:' . $mt . ' OR';
                }
                $solrParams['fq'] = rtrim($solrParams['fq'], 'OR');
                $solrParams['fq'] .= ')';
            }
        }
        
        
        // filter price slider 
        $priceSlider = '';
        if (!empty($_POST['priceslider'])) {
            $priceSlider = explode('-', $_POST['priceslider']);
            if ( count($priceSlider) > 1) {
                // base on currency selected conv to usd to compare weeklyprice
                $newMinPrice = round(Yii::app()->openexchanger->convertCurrency($priceSlider[0], $this->ipCurrencyCode, 'USD'));
                $newMaxPrice = round(Yii::app()->openexchanger->convertCurrency($priceSlider[1], $this->ipCurrencyCode, 'USD'));
                //$criteria['condition']  .= ' AND weeklyprice between '.$priceSlider[0]. ' AND '. $priceSlider[1];
                $solrParams['fq'] .= (!empty($solrParams['fq'])) ? ' AND ' : '';
                $solrParams['fq'] .= 'weeklyprice:[' . $newMinPrice . ' TO ' . $newMaxPrice . ']';
            }            
        }


        // proximity
        $proximity = is_numeric(Yii::app()->request->getQuery('proximity')) ? (int) Yii::app()->request->getQuery('proximity') : Yii::app()->params['proximity'];
        // geoloc
//        $geoloc = $_POST['geoloc'];
//        if (!empty($geoloc)) {
//                    $solrParams['fq'] .= " AND {!geofilt pt=$geoloc sfield=geoloc d=$proximity}";
//        }
        

//Sorting
        if(!empty($_POST['sort'])) {
            $filter = '';
            if ($_POST['sort'] === 'Price') {
                $filter = 'weeklyprice asc';
            } else if ($_POST['sort'] === 'Popularity') {
                $filter = 'pscore desc';
            } else if ($_POST['sort'] === 'Most Recent') {
                $filter = 'datemodified desc';
            }  
            $solrParams['sort'] = $filter;
        }
        
        // solr query 
        $textSearch = '';
        $solrQuery = '';
        if (!empty($_POST['textsearch'])) {
            $textSearch = $_POST['textsearch'];
            if (!empty($textSearch)) {
                $solrQuery = "name:*{$textSearch}* OR description:*{$textSearch}* OR mediatype:*{$textSearch}* OR audiencetag:*{$textSearch}*";
            } else {
                $solrQuery = '*:*';
            }
        } else {
                $solrQuery = '*:*';
            }


        //$solrParams['rows'] = 5;
        // get listing from Solr                
        //$result = Yii::app()->listingSearch->get($solrQuery, 0, 50000, $solrParams);
        // load from 0 if markers already loaded is not in $_GET
        $marker_loaded = (int) Yii::app()->request->getQuery('marker_loaded');
        $marker_loaded = ($marker_loaded > 0) ? $marker_loaded : 0;

        // how many to load - next_toload_count not there then default load count
        $next_toload_count = (int) Yii::app()->request->getQuery('next_toload_count');
        $init_markers = ($next_toload_count > 0) ? $next_toload_count : Yii::app()->params['init_markers'];


        $solrParams['wt'] = 'json';
        //$params['json.nl'] = 'map';
        $solrParams['fl'] = 'id,lat,lng,ea';
        $solrParams['q'] = $solrQuery;
        $solrParams['start'] = 0; // $marker_loaded; //0;
        $solrParams['rows'] = 50; // $init_markers; //50000;

        $qp = http_build_query($solrParams, null, '&');

        // >>> curl query
        $ch = curl_init();
        $url = Yii::app()->params['solrCurl'] . $qp;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);            // Include header in result? (0 = yes, 1 = no)            
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Should cURL return or print out the data? (true = return, false = print)
        //curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $result = curl_exec($ch);
        curl_close($ch);
        $res = json_decode($result);
        $markerlist = array();
        $markerlist['Markerslist'] = $res->response->docs;
        echo json_encode($markerlist);
    }
    
    
    public function actiongetsitedetails() {
        $id=$_POST['siteid'];
        $userid ='';
        if (!empty($_POST['userid'])) {
            $userid = $_POST['userid'];
        }
        //echo $id;die();
        echo Listing::getListingDetails($id, $userid);
    }
    
    public function actionaddfavorite() {
        $listingId=$_POST['siteid'];
        $userId=$_POST['userid'];

        if (!empty($listingId) && !empty($userId)) {
            // check if the logged in user id matches with param
            $favListModal = FavouriteListing::model()->findByAttributes(array('userid' => $userId, 'listingid' => $listingId));
            if ($favListModal) {
                // favourite, so remove from favourite
                FavouriteListing::model()->deleteByPk($favListModal->id);
                echo 0;
            } else {
                // not favourite, so save as favourite
                $favList = new FavouriteListing;
                $favList->userid = $userId;
                $favList->listingid = $listingId;
                $favList->datecreated = date("Y-m-d H:i:s");
                $favList->save();
                echo 1;
            }
        }
    }
    
    public function actionRetrivePlan() {
        $userid = $_POST['Userid'];
        $result = array();
        $result['PlanList'] = Plan::getPlansForUser($userid);
        echo json_encode($result);
    }
    
    public function actionDeletePlanListing() {
        $planid = $_POST['planid'];
        $listingid = $_POST['listingid'];
        $planListing = PlanListing::model()->findByAttributes(array('planid' => $planid, 'listingid' => $listingid));
        if ($planListing) {
            PlanListing::model()->deleteByPk($planListing->id);
            echo 1;
        } else {
            echo 0;
        }
    }
    
    public function actionAddplan() {

        $userId = $_POST['userid'];
        $name = $_POST['planname'];
        $listingId = $_POST['listingid'];
        
        if($userId && $name && $listingId) {
            $planModel = new Plan();
            $planModel->userid = $userId;
            $planModel->name = $name;
            $planModel->status = 1;
            $planModel->datecreated = date('Y-m-d H:i:s');
            //$planModel->datemodified = date('Y-m-d H:i:s');
            $planModel->save();
            echo $planId = $planModel->getPrimaryKey();


            $planListingModel = new PlanListing();
            $planListingModel->planid = $planId;
            $planListingModel->listingid = $listingId;
            $planListingModel->status = 1;
            $planListingModel->datecreated = date('Y-m-d H:i:s');
            $planListingModel->datemodified = date('Y-m-d H:i:s');
            $planListingModel->save();
            echo "Listing added to new Plan";
        }
        
    }
    
    public function actionAddInExistingPlan() {
         
         $userId = $_POST['userid'];
        $planId = $_POST['planid'];
        $listingId = $_POST['listingid'];

        $response = PlanListing::isPlanListingExist($planId, $listingId);
        if($response) {
            echo "Listing already existing on the plan";
            exit;
        }
        
        if(!$response && $userId && $planId && $listingId) {
            $planListingModel = new PlanListing();
            $planListingModel->planid = $planId;
            $planListingModel->listingid = $listingId;
            $planListingModel->status = 1;
            $planListingModel->datecreated = date('Y-m-d H:i:s');
            $planListingModel->datemodified = date('Y-m-d H:i:s');
            $planListingModel->save();
            
            // Show report in admin updating plan modify date
            $planModel = Plan::model()->findByPk($planId);
            $planModel->datemodified = date('Y-m-d H:i:s');
            $planModel->save();
            echo 'Listing added to plan';
        }
    }
    
    public function actionPlanDetail() {
        $userId = $_POST['userid'];
        $name = $_POST['plan'];
        echo json_encode(Plan::getPlan($userId, $name));
    }
}
