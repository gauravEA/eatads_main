<?php

class VendorInsightsController extends Controller
{
	protected $myvar;
    
        public function init() {
            Yii::app()->theme = 'new';
            $this->layout = "//layouts/newdesign";
            $this->myvar = Yii::app()->request->getQuery('alias');
        }
        
	public function actionIndex()
	{
            $company = new UserCompany();
            $alias = $this->myvar;
            if ($alias) {
                $company = UserCompany::getCompanyDataFromAlias($alias);
//                print_r($company);die();
            }
            $result = array();
            //fetch all the listing of the company
            $events = EmailEvent::model()->findAllByAttributes(array("companyid" => $company['id']));
            if ($events) {
                for ($i = 0; $i < count($events); $i ++) {
                    $sql = "Select u.email as useremail,eel.modifieddate as lastclicked,eel.totalopens as openedcount, eel.totalclicks as clickcount from  EmailEventLog eel inner join User u on u.id = eel.userid where eventid = " . $events[$i]['id'] ;
                    $emailLogs = Yii::app()->db->createCommand($sql)->queryAll();
                    //$emailLog = EmailEventLog::model()->findBySql($sql);
                    //print_r($emailLog);//die();

                    $event = array(
                        'id' => $events[$i]['id'],
                        'createddate' => $events[$i]['createddate'],
                        'name' => $company['name'] . " Availability Mailer " . $events[$i]['createddate'],
                        'emailanalytics' => $emailLogs
                    );
                    array_push($result, $event);
                }
            }

            $this->render('index', array("data" => $result));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}