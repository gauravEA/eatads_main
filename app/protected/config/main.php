<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

$protocol = 'http://';     // https:// or http://

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'EatAds.com - Simplying OOH',
    // preloading 'log' component
    'preload' => array('log', 'UnderConstruction'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.PasswordHash',
        'application.components.JOY.*',
        'ext.easyimage.EasyImage',
        'application.modules.hybridauth.controllers.*', // for hybridauth
        'application.extensions.solr.*',    // for apache solr        
        'ext.YiiMailer.EatadsMailer', // Wrapper of YiiMailer
        'ext.PHPExcel.*'
    ),
    'modules' => array(
        'admin',
        'user',
        // uncomment the following to enable the Gii tool
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'eatads',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
        
        'hybridauth' => array(
            'baseUrl' => $protocol. $_SERVER['SERVER_NAME'] . '/eatads/app/index.php/hybridauth', 
            'withYiiUser' => false, // Set to true if using yii-user
            "providers" => array ( 
                
                "Google" => array ( 
                    "enabled" => true,
                    "keys"    => array ( "id" => "334477999151.apps.googleusercontent.com", "secret" => "zHsLJJUoRd2bOfQclq2yizOp" ),
                    "scope"   => "https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile"
                ),
                "LinkedIn" => array ( 
                    "enabled" => true,
                    "keys"    => array ( "key" => "zyqkhkvb72rj", "secret" => "nje0hffj1TUGmCo8" ),
                    "scope"   => "r_basicprofile+r_emailaddress"
                ),
                /*                 
                "Yahoo" => array ( 
                    "enabled" => false 
                ),
 
                "Facebook" => array ( // 'id' is your facebook application id
                    "enabled" => false,
                    "keys" => array ( "id" => "", "secret" => "" ),
                    "scope" => "email, user_about_me, user_birthday, user_hometown" // optional
                ),

                "Twitter" => array ( 
                    "enabled" => false,
                    "keys"    => array ( "key" => "", "secret" => "" ) 
                )*/
            ),
        ),
    ),
    // application components
    'components' => array(
        /*'UnderConstruction' => array(
            'class' => 'application.components.UnderConstruction',
            'allowedIPs'=>array('127.0.0.1'), //whatever IPs you want to allow
            'locked'=>true,//this is the on off switch
            'redirectURL'=>'http://www.yiiframework.com',//put in your desired redirect page.           
        ),*/

        'excel'=>array(
	          'class'=>'application.extensions.PHPExcel',
        ),
        'openexchanger' => array(
            'class' => 'Openexchanger',
            'appId' => '1861e1d93d584f38b768262ad774cd89', //'3660c1096358452eb625cde00a4ccdab', //'d295d6446d4242b8b239b747554734a7',
            'cache' => true,
        ),
        'user' => array(
            'class' => 'WebUser',
            // default login url
            'loginUrl'=>array('account/login'),
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        'listingSearch' => array(
        	        	'class' => 'CSolrComponent',
        	'host' => 'eatadssolr@172.20.17.79',
        	'port' => 8080,
        	'indexPath' => '/solr/listing'
        ),
        
        /*'clientScript' => array(
            'coreScriptPosition' => CClientScript::POS_END
        ),*/
        
        // easy image extension
        'easyImage' => array(
            'class' => 'application.extensions.easyimage.EasyImage',
            //'driver' => 'GD',
            //'quality' => 100,
            //'cachePath' => '/assets/easyimage/',
            //'cacheTime' => 2592000,
            //'retinaSupport' => false,
        ),

        'cache' => array(
            'class' => 'system.caching.CDummyCache',
        ),
        
//        'cache' => array(
//            'class' => 'system.caching.CMemCache',
//            'useMemcached' => false,
//            'servers' => array(
//                array(
//                    'host' => 'localhost',
//                    'port' => '11211',
//                    'persistent' => true,
//                ),	
//            ),
//        ),
        
        // uncomment the following to enable URLs in path-format
        /*
        'urlManager' => array(
            'showScriptName' => FALSE,
            'urlFormat' => 'path',
            'rules' => array(
                'about-us' => 'staticPages/aboutUs',
                'terms-conditions' => 'staticPages/termsAndCondition',
                'contact-us' => 'staticPages/contactUs',
                'faq' => 'staticPages/faq',
                'help' => 'staticPages/help',
                'site-map' => 'staticPages/sitemap',
                'career' => 'staticPages/career',
                'press' => 'staticPages/press',
                'demo' => 'staticPages/demo',
                'maintenance' => 'staticPages/maintenance',

                '<usertype:media-owner|media-buyer|media-service-providers|admin>' => 'site/index',
                
                'listings/<countryid:[\w\-]+>' => 'listing/area',
                
                'listing/<id:\d+>/<country:[\w\-]+>/<state:[\w\-]+>/<city:[\w\-]+>/<listingname:[\w\-]+>' => 'listing/view',
                'listing/<id:\d+>' => 'listing/view',
                
                'company/<id:[\w\-]+>' => 'seller/index',                
                '<keyword:[\w\-]+>/dashboard' => 'user/dashboard/index',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ), */
        'urlManager' => array(
            'showScriptName' => FALSE,
            'urlFormat' => 'path',
            'rules' => array(
                'about-us' => 'staticPages/aboutUs',
                'terms' => 'staticPages/termsAndCondition',
                'terms-conditions' => 'staticPages/termsAndCondition',                
                'contact-us' => 'staticPages/contactUs',
                'faq' => 'staticPages/faq',
                'help' => 'staticPages/help',
                'site-map' => 'staticPages/sitemap',
                'career' => 'staticPages/career',
                'press' => 'staticPages/press',
                'demo' => 'staticPages/demo',
                'maintenance' => 'staticPages/maintenance',
                'signup' => 'account/signup',
                'login' => 'account/login',
                'logout' => 'account/logout',
                'forgot' => 'account/forgot',
                'email-success' => 'account/emailsuccess',
                'airport' => 'staticPages/airport',
                'signup/<type:[\w\-]+>' => 'account/signup',

                '<usertype:admin>' => 'site/index',
                
                'listings/<countryid:[\w\-]+>' => 'listing/area',
                
                'l/<id:\w+>/<country:[\w\-]+>/<state:[\w\-]+>/<city:[\w\-]+>/<listingname:[\w\-]+>' => 'listing/view',
                'l/<id:\w+>' => 'listing/view',
                
                'dashboard/[v|b|s]/<id:[\w\-]+>' => 'user/dashboard/index',                
                'dashboard/[v|b|s]/<id:[\w\-]+>/media-plans' => 'user/mediaplans',
                'dashboard/[v|b|s]/<id:[\w\-]+>/favourites' => 'user/favourites',
                'dashboard/[v|b|s]/<id:[\w\-]+>/profile' => 'user/profile/edit',
                'dashboard/[v|b|s]/<id:[\w\-]+>/manage-media' => 'user/listing/manage',
                'dashboard/[v|b|s]/<id:[\w\-]+>/add' => 'user/listing/create',                
                'dashboard/[v|b|s]/<id:[\w\-]+>/<planid:[\w\-]+>/rfp-create' => 'user/mediaplans/sendrfp',
                
                'v/<userid:[\w\-]+>' => 'seller/index',
                
                'company/<userid:[\w\-]+>' => 'seller/index',
                
                's/mapview' => 'map/index',
                's/listingview' => 'listing/index',
                's/mosaicview' => 'listing/index',
                'vendor/<alias:[\w\-]+>' => 'vendor/index',
                'vendor/<alias:[\w\-]+>/contacts' => 'contacts/index',
                'vendor/<alias:[\w\-]+>/availability' => 'availabilityManager/index',
                'vendor/<alias:[\w\-]+>/insights' => 'vendorInsights/index',
                'vendor/UpdateEmailopenRedirectToUrl' => 'vendor/UpdateEmailopenRedirectToUrl',
                
                'myplans/<name:[\w\-]+>' => 'myplans/index',
                'vendorprofile/<alias:[\w\-]+>' => 'vendorprofile/index',
                'media-hub'=>'mediahub/index',
                'media-hub/landing' => 'mediahub/landing',
                'monitorly'=>'monitorly/index',
                'monitorly/landing' => 'monitorly/landing',
                
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        /*
          'db'=>array(
          'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
          ),
         */
        // uncomment the following to use a MySQL database
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=eatads',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',
            // enabled profile params for YII-DEBUG-TOOLBAR
            //'enableProfiling' => true,
            //'enableParamLogging' => true,
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'trace, info,debug,error, warning',
                    'logFile'=>'test.log',
                    //'categories'=>'my_category.*'
                    // YII-DEBUG-TOOLBAR >>>
                    //'class' => 'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
                    //'ipFilters' => array('127.0.0.1','192.168.1.215'),
                    // <<< YII-DEBUG-TOOLBAR
                ),
            // uncomment the following to show log messages on web pages
            
//              array(
//              'class'=>'CWebLogRoute',
//              ),
             
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        'init_markers' => 1000,
        'load_markers' => 3000,
        'solrCurl'=>  'http://eataddsolr.eatads.com:8080/solr/listing/select?', //$protocol.'localhost:8080/solr/listing/select?',
        // this is used in contact page
        'adminEmail' => 'eatads.kellton@gmail.com',
        // used in rfps
        'bccEmail' => array(
            'joy8436@gmail.com'
        ),
        'supportEmail' => 'joy8436@gmail.com',
        'linkexpiry' => array(
            'signup' => 48,
            'forgot' => 24
        ),
        'fileUpload' => array(
            'size'=>1024*1024*5,
            'count'=>10,
            'path'=>  $_SERVER['DOCUMENT_ROOT'] .  '/eatads/app/uploads/',        
            'tempAbsPath'=>  'http://localhost/eatads/app/uploads/',
        ),
        'protocol' => $protocol,    // goto top
        'proximity' => 15,          // map proximity radius
        'records_per_page'=>10,      // for pagination
        'jrid_recorder_per_page'=>50, // for jgrid pagination
        'city_per_page'=>200,       // for country/city page
        'countries_home'=>10,       // home page country list
        'countries_per_slide' =>10, // home page countries per slide in slider
        'meta_keyword' => "outdoor advertising,outdoor media,billboard,billboard advertising,underground advertising,airport advertising,cinema advertising,advertising,media,traditional advertising",
        'meta_description' => 'Browse a range of outdoor advertising, billboard, taxi, underground media and more. EatAds.com makes buying advertising simple.',
        
        'map_latlng' => '20,77',    // default latlng of India
        'map_country'=> 'India',    // default country name for map
        
        'awss3' => array(           // amazon s3 details
            's3Bucket'=>'eatads-media',
            'accessKey'=>'AKIAIW62GKSH4I5LIEXQ',
            'secretKey'=>'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'
        ),
        
        'awssqs' => array(           // amazon s3 details
            'region'=>'ap-southeast-1',
            'accessKey'=>'AKIAJ6MBDOHMUIOD4BEA',
            'secretKey'=>'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',
            'queue' => 'https://sqs.ap-southeast-1.amazonaws.com/135156983991/eatads_availability_mailer'
        ),
        'mandrill' => array(
            'api_key' => 'wtWRc4QXlHhoMyK6nzHUqQ'
        ),
        'mailChimp' => array(       // mail chimp api key and id
            'api_key' => '76d05ba87b150c382677a19da6f4be91-us3',
            'id' => 'c3657884d3'
        ),
        'mailChimpBrief' => array(       // mail chimp api key and id for brief
            'api_key' => '76d05ba87b150c382677a19da6f4be91-us3',
            'id' => 'c3657884d3'
        ),
       
        // for gmail api
        'gmapApiKey' => 'AIzaSyD9ycb1xXwLT6Wh5HrRb1YbUcBCw7_UHic', //'AIzaSyCtfa3XFporc1yBA7Z16T_FmhMfxNJ6WcQ',
        // for password protection extension
        'phpass' => array(
            'iteration_count_log2' => 8,
            'portable_hashes' => false,
        ),
    ),
);