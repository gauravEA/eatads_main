<?php 
	error_reporting(E_ALL);
	ini_set('display_errors', 1);

// Update Listing
// 273, 330, 2153, 2161, 2170 - (28.283300,76.149999)        
//-- 8767
//SELECT id, title, country_code, state_code, city_code, mapping, new_countryid, new_stateid, new_cityid, new_status  FROM `eatads_listing` WHERE new_status = 1;

//-- 274
//SELECT *  FROM `eatads_listing` WHERE new_status != 1;        
        
	// create db connection
	mysql_connect('localhost', 'root', '') or die("Couldn't create connection.");
	
	// select db
	mysql_select_db('eatadslivemapping') or die("Couldn't fetch database.");
	
	
	function cleanMapping($mapping)
	{
		$mapping = str_replace("(", "", $mapping);
		$mapping = str_replace(")", "", $mapping);
		$map = explode(",", $mapping);
		return $map;
	}

	
	$sql = "SELECT `id`, `title`, `country_code`, `state_code`, `city_code`, `mapping`
				FROM `eatads_listing`
				WHERE `mapping` IS NOT NULL 
                                AND `mapping`  != '(latitude, longitude)'
                                AND `mapping` != '(34.0836581, 74.79736809999997)'
                                AND `mapping` != '(33.97980872872457, 74.761962890625)'
                                AND `mapping` != '(27.0869009, 93.60860989999992)'
				AND `new_status` = 0
				LIMIT 1";
	$result = mysql_query($sql);
	
	echo '<table style="border:1;">';
	while($rows = mysql_fetch_object($result)) {
		$latLng = cleanMapping($rows->mapping);
		echo '<tr class="record">';		
		echo '<td class="recordid" >'.$rows->id.'</td>';
		echo '<td id="lat'.$rows->id.'" >'.$latLng[0].'</td>';
		echo '<td id="lng'.$rows->id.'" >'.$latLng[1].'</td>';
		echo '<td id="country'.$rows->id.'" ></td>';
		echo '<td id="state'.$rows->id.'" ></td>';
		echo '<td id="city'.$rows->id.'" ></td>';
		echo '</tr>';	
	}
	echo '</table>';
	
?>
<style>
table, th, td
{
	border: 1px solid black;
}
</style>
<script src="<?php echo Yii::app()->params['protocol']; ?>code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCtfa3XFporc1yBA7Z16T_FmhMfxNJ6WcQ&sensor=true"></script>
<script type="text/javascript">
	var counter =0;	
	$(function(){
		$('tr.record').each(function(){
			var record_id = $(this).children('td.recordid').html();
			//console.log(record_id);
			var geo_lat = $(this).children('td#lat'+record_id).html(); 
			var geo_lng = $(this).children('td#lng'+record_id).html();
			//console.log(geo_lat + ' '  + geo_lng);
			setTimeout(function(){
				getLatLongDetail(geo_lat.trim(), geo_lng.trim(), record_id);
			},2000);						
			
		});
	});
	
    function getLatLongDetail(lat, lng, record_id) {
		 
    	var myLatLng = new google.maps.LatLng(lat, lng);      
        var geocoder = new google.maps.Geocoder(); 
        geocoder.geocode({ 'latLng': myLatLng },
          function (results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                  if (results[0]) {
                      var address = "", city = "", state = "", zip = "", country = "", country_short="";

                      for (var i = 0; i < results[0].address_components.length; i++) {
                          var addr = results[0].address_components[i];
                          //console.log(addr);
                          // check if this entry in address_components has a type of country
                          if (addr.types[0] == 'country') {
                              country = addr.long_name;
                              country_short = addr.short_name;
                          } else if (addr.types[0] == 'street_address') { // address 1 
                              address = address + addr.long_name;
                          } else if (addr.types[0] == 'establishment') {
                              address = address + addr.long_name;
                          } else if (addr.types[0] == 'route') { // address 2
                              address = address + addr.long_name;
                          } else if (addr.types[0] == 'postal_code') {      // Zip
                              zip = addr.short_name;
                          } else if (addr.types[0] == 'administrative_area_level_1') {      // State
                              state = addr.long_name;
                          } else if (addr.types[0] == 'locality') {      // City
                              city = addr.long_name; 
                          } else if (addr.types[0] == 'administrative_area_level_2' && city.length===0) {      // City
                              city = addr.long_name; 
                          }
                      }
                      //console.log('country='+country+' state='+state+' city='+city);                      
                        $.ajax({                
                              type: "GET",
                              url: "old_db_listing_area_ajax.php", //url to call.
                              cache: true,
                              async:false,
                              data: {                                
                                  country: country,
                                  country_short: country_short,
                                  state: state,
                                  city: city,
                                  id: record_id,
                                  geo_lat: lat,
                                  geo_lng: lng
                              },
                              success: function (data) {
                                    counter++; 
                                  var obj = jQuery.parseJSON(data);
                                  //console.log(obj.c);
                                  //console.log(obj.s);
                                  //console.log(obj.ci);
                                    if(obj.status==2) {
                                        // country state or city empty
                                        // update record new_status = 2
                                        $('#country'+record_id).html('Address not valid.');
                                    } else if(obj.status==0) {
                                        // id null
                                        $('#country'+record_id).html('Id not valid.');
                                    } else {
                                        $('#country'+record_id).html(country  + " (" +obj.c[1] + ")");
                                        $('#state'+record_id).html(state + " (" +obj.s[1]+ ")");
                                        $('#city'+record_id).html(city + " (" +obj.ci[1]+ ")");
                                    }	
                                    console.log(counter);
//                                    if(counter==5) {
                                            window.location.reload(true);
//                                    }								  										
                              }
                        });						                                     
                  }
              }
          });
    }    
</script>




