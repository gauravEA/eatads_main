<?php

mysql_connect("localhost", "root", "");
mysql_select_db("eatadslive");

$countryStr = "Afghanistan|AF|Albania|AL|Algeria|DZ|American Samoa|AS|Andorra|AD|Angola|AO|Anguilla|AI|Antigua and Barbuda|AG|Argentina|AR|Armenia|AM|Aruba|AW|Australia|AU|Austria|AT|Azerbaijan|AZ|Bahamas|BS|Bahrain|BH|Bangladesh|BD|Barbados|BB|Belarus|BY|Belgium|BE|Belize|BZ|Benin|BJ|Bermuda|BM|Bhutan|BT|Bolivia|BO|Bosnia and Herzegovina|BA|Botswana|BW|Bouvet Island|BV|Brazil|BR|British Indian Ocean Territory|IO|Brunei Darussalam|BN|Bulgaria|BG|Burkina Faso|BF|Burundi|BI|Cambodia|KH|Cameroon|CM|Canada|CA|Cape Verde|CV|Cayman Islands|KY|Central African Republic|CF|Chad|TD|Chile|CL|China|CN|Christmas Island|CX|Cocos (Keeling) Islands|CC|Colombia|CO|Comoros|KM|Congo|CG|Cook Islands|CK|Costa Rica|CR|Croatia|HR|Cuba|CU|Cyprus|CY|Czech Republic|CZ|Denmark|DK|Djibouti|DJ|Dominican Republic|DO|Ecuador|EC|Egypt|EG|El Salvador|SV|Equatorial Guinea|GQ|Eritrea|ER|Estonia|EE|Ethiopia|ET|Falkland Islands (Malvinas)|FK|Faroe Islands|FO|Fiji|FJ|Finland|FI|France|FR|French Guiana|GF|French Polynesia|PF|French Southern Territories|TF|Gabon|GA|Gambia|GM|Georgia|GE|Germany|DE|Ghana|GH|Gibraltar|GI|Greece|GR|Greenland|GL|Grenada|GD|Guadeloupe|GP|Guam|GU|Guatemala|GT|Guinea|GN|Guyana|GY|Haiti|HT|Heard and McDonald Islands|HM|Holy See (Vatican City State)|VA|Honduras|HN|Hong Kong, China|HK|Hungary|HU|Iceland|IS|India|IN|Indonesia|ID|Iran (Islamic Republic Of)|IR|Iraq|IQ|Ireland|IE|Israel|IL|Italy|IT|Jamaica|JM|Japan|JP|Jordan|JO|Kazakhstan|KZ|Kenya|KE|Kiribati|KI|Korea, Democratic People's Republic Of|KP|Korea, Republic of|KR|Kuwait|KW|Kyrgyzstan|KG|Lao People's Democratic Republic|LA|Latvia|LV|Lebanon|LB|Lesotho|LS|Liberia|LR|Libyan Arab Jamahiriya|LY|Liechtenstein|LI|Lithuania|LT|Luxembourg|LU|Macao|MO|Macedonia, the Former Yugoslav Republic Of|MK|Madagascar|MG|Malawi|MW|Malaysia|MY|Maldives|MV|Mali|ML|Malta|MT|Marshall Islands|MH|Martinique|MQ|Mauritania|MR|Mauritius|MU|Mayotte|YT|Mexico|MX|Micronesia, Federated States Of|FM|Moldova|MD|Monaco|MC|Mongolia|MN|Montenegro|ME|Montserrat|MS|Morocco|MA|Mozambique|MZ|Myanmar|MM|Namibia|NA|Nauru|NR|Nepal|NP|Netherlands|NL|New Caledonia|NC|New Zealand|NZ|Nicaragua|NI|Niger|NE|Nigeria|NG|Niue|NU|Norfolk Island|NF|Northern Mariana Islands|MP|Norway|NO|Oman|OM|Pakistan|PK|Palau|PW|Panama|PA|Papua New Guinea|PG|Paraguay|PY|Peru|PE|Philippines|PH|Pitcairn|PN|Poland|PL|Portugal|PT|Puerto Rico|PR|Qatar|QA|Romania|RO|Russian Federation|RU|Rwanda|RW|Saint Helena|SH|Saint Kitts And Nevis|KN|Saint Lucia|LC|Saint Pierre And Miquelon|PM|Saint Vincent And The Grenedines|VC|Samoa|WS|San Marino|SM|Sao Tome and Principe|ST|Saudi Arabia|SA|Senegal|SN|Serbia|RS|Seychelles|SC|Sierra Leone|SL|Singapore|SG|Slovenia|SI|Solomon Islands|SB|Somalia|SO|South Africa|ZA|Spain|ES|Sri Lanka|LK|Sudan|SD|Suriname|SR|Svalbard And Jan Mayen|SJ|Swaziland|SZ|Sweden|SE|Switzerland|CH|Syrian Arab Republic|SY|Tajikistan|TJ|Tanzania, United Republic of|TZ|Thailand|TH|Timor-Leste|TL|Tokelau|TK|Tonga|TO|Trinidad and Tobago|TT|Tunisia|TN|Turkey|TR|Turkmenistan|TM|Turks and Caicos Islands|TC|Tuvalu|TV|Uganda|UG|Ukraine|UA|United Arab Emirates|AE|United Kingdom|GB|United States|US|Uruguay|UY|Uzbekistan|UZ|Vanuatu|VU|Vietnam|VN|Wallis and Futuna|WF|Western Sahara|EH|Yemen|YE|Zambia|ZM|Zimbabwe|ZW";
$countryArr = explode("|", $countryStr);

$ausState = "Australian Capital Territory|11|New South Wales|12|Northern Territory|13|Queensland|14|South Australia|15|Tasmania|16|Victoria|17|Western Australia|18";
$ausStateArr = explode("|", $ausState);

$indiaState = "Andaman and Nicobar Islands|11|Andhra Pradesh|12|Arunachal Pradesh|13|Assam|14|Bihar|15|Chhattisgarh|16|Delhi NCR|17|Goa|18|Gujarat|19|Haryana|20|Himachal Pradesh|21|Jammu and Kashmir|22|Jharkhand|23|Karnataka|24|Kerala|25|Madhya Pradesh|26|Maharashtra|27|Manipur|28|Meghalaya|29|Mizoram|30|Nagaland|31|Orissa|32|Punjab|33|Rajasthan|34|Sikkim|35|Tamil Nadu|36|Tripura|37|Uttar Pradesh|38|Uttarakhand|39|West Bengal|40";
$indiaStateArr = explode("|", $indiaState);

$malState = "Johor|11|Kedah|12|Kelantan|13|Kuala Lumpur|14|Labuan|15|Malacca|16|Negeri Sembilan|17|Pahang|18|Penang|19|Perak|20|Perlis|21|Putrajaya|22|Sabah|23|Sarawak|24|Selangor|25|Terengganu|26";
$malStateArr = explode("|", $malState);

$phyState = "Autonomous Region in Muslim Mindanao|11|Bicol Region|12|Cagayan Valley|13|CALABARZON|14|Caraga Region|15|Central Luzon|16|Central Visayas|17|Cordillera Administrative Region|18|Davao Region|19|Eastern Visayas|20|Ilocos Region|21|MIMAROPA|22|National Capital Region|23|Northern Mindanao|24|SOCCSKSARGEN|25|Western Visayas|26|Zamboanga Peninsula|27";
$phyStateArr = explode("|", $phyState);

$sql = "select id, country_code, countryname, state_code from eatads_listing where (state_code > 1 AND state_code is not null) AND statename is null";
$result = mysql_query($sql);



$counter = 0;
while($row = mysql_fetch_array($result)) {
    if($row['country_code'] == "AU") {
        if( $match = array_search($row['state_code'], $ausStateArr) ) {
            $newSql = "UPDATE eatads_listing set statename = '". $ausStateArr[$match-1] ."' WHERE id = '". $row['id'] ."'";
            mysql_query($newSql);
            $counter++;
        }
    }else if($row['country_code'] == "IN") {
        if( $match = array_search($row['state_code'], $indiaStateArr) ) {
            $newSql = "UPDATE eatads_listing set statename = '". $indiaStateArr[$match-1] ."' WHERE id = '". $row['id'] ."'";
            mysql_query($newSql);
            $counter++;
        }
    }else if($row['country_code'] == "MY") {
        if( $match = array_search($row['state_code'], $malStateArr) ) {
            $newSql = "UPDATE eatads_listing set statename = '". $malStateArr[$match-1] ."' WHERE id = '". $row['id'] ."'";
            mysql_query($newSql);
            $counter++;
        }
    }else if($row['country_code'] == "PH") {
        if( $match = array_search($row['state_code'], $phyStateArr) ) {
            $newSql = "UPDATE eatads_listing set statename = '". $phyStateArr[$match-1] ."' WHERE id = '". $row['id'] ."'";
            mysql_query($newSql);
            $counter++;
        }
    }
    
}
echo $counter;
?>

<script type="text/javascript">
var ALL_CURRENCIES = "Afghanistan Afghani--AFN|AFN|Albania Lek--ALL|ALL|Algeria Dinar--DZD|DZD|Angola Kwanza--AOA|AOA|Argentina Peso--ARS|ARS|Armenia Dram--AMD|AMD|Aruba Guilder--AWG|AWG|Australia Dollar--AUD|AUD|Azerbaijan New Manat--AZN|AZN|Bahamas Dollar--BSD|BSD|Bahrain Dinar--BHD|BHD|Bangladesh Taka--BDT|BDT|Barbados Dollar--BBD|BBD|Belarus Ruble--BYR|BYR|Belize Dollar--BZD|BZD|Bermuda Dollar--BMD|BMD|Bhutan Ngultrum--BTN|BTN|Bolivia Boliviano--BOB|BOB|Bosnia and Herzegovina Convertible Marka--BAM|BAM|Botswana Pula--BWP|BWP|Brazil Real--BRL|BRL|Brunei Darussalam Dollar--BND|BND|Bulgaria Lev--BGN|BGN|Burundi Franc--BIF|BIF|Cambodia Riel--KHR|KHR|Canada Dollar--CAD|CAD|Cape Verde Escudo--CVE|CVE|Cayman Islands Dollar--KYD|KYD|Chile Peso--CLP|CLP|China Yuan Renminbi--CNY|CNY|Colombia Peso--COP|COP|Communaut� Financi�re Africaine (BCEAO) Franc--XOF|XOF|Communaut� Financi�re Africaine (BEAC) CFA Franc BEAC--XAF|XAF|Comoros Franc--KMF|KMF|Comptoirs Fran�ais du Pacifique (CFP) Franc--XPF|XPF|Congo/Kinshasa Franc--CDF|CDF|Costa Rica Colon--CRC|CRC|Croatia Kuna--HRK|HRK|Cuba Convertible Peso--CUC|CUC|Cuba Peso--CUP|CUP|Czech Republic Koruna--CZK|CZK|Denmark Krone--DKK|DKK|Djibouti Franc--DJF|DJF|Dominican Republic Peso--DOP|DOP|East Caribbean Dollar--XCD|XCD|Egypt Pound--EGP|EGP|El Salvador Colon--SVC|SVC|Eritrea Nakfa--ERN|ERN|Ethiopia Birr--ETB|ETB|Euro Member Countries--EUR|EUR|Falkland Islands (Malvinas) Pound--FKP|FKP|Fiji Dollar--FJD|FJD|Gambia Dalasi--GMD|GMD|Georgia Lari--GEL|GEL|Ghana Cedi--GHS|GHS|Gibraltar Pound--GIP|GIP|Guatemala Quetzal--GTQ|GTQ|Guernsey Pound--GGP|GGP|Guinea Franc--GNF|GNF|Guyana Dollar--GYD|GYD|Haiti Gourde--HTG|HTG|Honduras Lempira--HNL|HNL|Hong Kong Dollar--HKD|HKD|Hungary Forint--HUF|HUF|Iceland Krona--ISK|ISK|India Rupee--INR|INR|Indonesia Rupiah--IDR|IDR|Iran Rial--IRR|IRR|Iraq Dinar--IQD|IQD|Isle of Man Pound--IMP|IMP|Israel Shekel--ILS|ILS|Jamaica Dollar--JMD|JMD|Japan Yen--JPY|JPY|Jersey Pound--JEP|JEP|Jordan Dinar--JOD|JOD|Kazakhstan Tenge--KZT|KZT|Kenya Shilling--KES|KES|Korea (North) Won--KPW|KPW|Korea (South) Won--KRW|KRW|Kuwait Dinar--KWD|KWD|Kyrgyzstan Som--KGS|KGS|Laos Kip--LAK|LAK|Latvia Lat--LVL|LVL|Lebanon Pound--LBP|LBP|Lesotho Loti--LSL|LSL|Liberia Dollar--LRD|LRD|Libya Dinar--LYD|LYD|Lithuania Litas--LTL|LTL|Macau Pataca--MOP|MOP|Macedonia Denar--MKD|MKD|Madagascar Ariary--MGA|MGA|Malawi Kwacha--MWK|MWK|Malaysia Ringgit--MYR|MYR|Maldives (Maldive Islands) Rufiyaa--MVR|MVR|Mauritania Ouguiya--MRO|MRO|Mauritius Rupee--MUR|MUR|Mexico Peso--MXN|MXN|Moldova Leu--MDL|MDL|Mongolia Tughrik--MNT|MNT|Morocco Dirham--MAD|MAD|Mozambique Metical--MZN|MZN|Myanmar (Burma) Kyat--MMK|MMK|Namibia Dollar--NAD|NAD|Nepal Rupee--NPR|NPR|Netherlands Antilles Guilder--ANG|ANG|New Zealand Dollar--NZD|NZD|Nicaragua Cordoba--NIO|NIO|Nigeria Naira--NGN|NGN|Norway Krone--NOK|NOK|Oman Rial--OMR|OMR|Pakistan Rupee--PKR|PKR|Panama Balboa--PAB|PAB|Papua New Guinea Kina--PGK|PGK|Paraguay Guarani--PYG|PYG|Peru Nuevo Sol--PEN|PEN|Philippines Peso--PHP|PHP|Poland Zloty--PLN|PLN|Qatar Riyal--QAR|QAR|Romania New Leu--RON|RON|Russia Ruble--RUB|RUB|Rwanda Franc--RWF|RWF|Saint Helena Pound--SHP|SHP|Samoa Tala--WST|WST|Saudi Arabia Riyal--SAR|SAR|Seborga Luigino--SPL|SPL|Serbia Dinar--RSD|RSD|Seychelles Rupee--SCR|SCR|Sierra Leone Leone--SLL|SLL|Singapore Dollar--SGD|SGD|Solomon Islands Dollar--SBD|SBD|Somalia Shilling--SOS|SOS|South Africa Rand--ZAR|ZAR|Sri Lanka Rupee--LKR|LKR|Sudan Pound--SDG|SDG|Suriname Dollar--SRD|SRD|Swaziland Lilangeni--SZL|SZL|Sweden Krona--SEK|SEK|Switzerland Franc--CHF|CHF|Syria Pound--SYP|SYP|S�o Tom� and Pr�ncipe Dobra--STD|STD|Taiwan New Dollar--TWD|TWD|Tajikistan Somoni--TJS|TJS|Tanzania Shilling--TZS|TZS|Thailand Baht--THB|THB|Tonga Pa'anga--TOP|TOP|Trinidad and Tobago Dollar--TTD|TTD|Tunisia Dinar--TND|TND|Turkey Lira--TRY|TRY|Turkmenistan Manat--TMT|TMT|Tuvalu Dollar--TVD|TVD|Uganda Shilling--UGX|UGX|Ukraine Hryvna--UAH|UAH|United Arab Emirates Dirham--AED|AED|United Kingdom Pound--GBP|GBP|United States Dollar--USD|USD|Uruguay Peso--UYU|UYU|Uzbekistan Som--UZS|UZS|Vanuatu Vatu--VUV|VUV|Venezuela Bolivar--VEF|VEF|Vietnam Dong--VND|VND|Yemen Rial--YER|YER|Zambia Kwacha--ZMW|ZMW|Zimbabwe Dollar--ZWD|ZWD";

var ALL_COUNTRIES = "Afghanistan|AF|Albania|AL|Algeria|DZ|American Samoa|AS|Andorra|AD|Angola|AO|Anguilla|AI|Antigua and Barbuda|AG|Argentina|AR|Armenia|AM|Aruba|AW|Australia|AU|Austria|AT|Azerbaijan|AZ|Bahamas|BS|Bahrain|BH|Bangladesh|BD|Barbados|BB|Belarus|BY|Belgium|BE|Belize|BZ|Benin|BJ|Bermuda|BM|Bhutan|BT|Bolivia|BO|Bosnia and Herzegovina|BA|Botswana|BW|Bouvet Island|BV|Brazil|BR|British Indian Ocean Territory|IO|Brunei Darussalam|BN|Bulgaria|BG|Burkina Faso|BF|Burundi|BI|Cambodia|KH|Cameroon|CM|Canada|CA|Cape Verde|CV|Cayman Islands|KY|Central African Republic|CF|Chad|TD|Chile|CL|China|CN|Christmas Island|CX|Cocos (Keeling) Islands|CC|Colombia|CO|Comoros|KM|Congo|CG|Cook Islands|CK|Costa Rica|CR|Croatia|HR|Cuba|CU|Cyprus|CY|Czech Republic|CZ|Denmark|DK|Djibouti|DJ|Dominican Republic|DO|Ecuador|EC|Egypt|EG|El Salvador|SV|Equatorial Guinea|GQ|Eritrea|ER|Estonia|EE|Ethiopia|ET|Falkland Islands (Malvinas)|FK|Faroe Islands|FO|Fiji|FJ|Finland|FI|France|FR|French Guiana|GF|French Polynesia|PF|French Southern Territories|TF|Gabon|GA|Gambia|GM|Georgia|GE|Germany|DE|Ghana|GH|Gibraltar|GI|Greece|GR|Greenland|GL|Grenada|GD|Guadeloupe|GP|Guam|GU|Guatemala|GT|Guinea|GN|Guyana|GY|Haiti|HT|Heard and McDonald Islands|HM|Holy See (Vatican City State)|VA|Honduras|HN|Hong Kong, China|HK|Hungary|HU|Iceland|IS|India|IN|Indonesia|ID|Iran (Islamic Republic Of)|IR|Iraq|IQ|Ireland|IE|Israel|IL|Italy|IT|Jamaica|JM|Japan|JP|Jordan|JO|Kazakhstan|KZ|Kenya|KE|Kiribati|KI|Korea, Democratic People's Republic Of|KP|Korea, Republic of|KR|Kuwait|KW|Kyrgyzstan|KG|Lao People's Democratic Republic|LA|Latvia|LV|Lebanon|LB|Lesotho|LS|Liberia|LR|Libyan Arab Jamahiriya|LY|Liechtenstein|LI|Lithuania|LT|Luxembourg|LU|Macao|MO|Macedonia, the Former Yugoslav Republic Of|MK|Madagascar|MG|Malawi|MW|Malaysia|MY|Maldives|MV|Mali|ML|Malta|MT|Marshall Islands|MH|Martinique|MQ|Mauritania|MR|Mauritius|MU|Mayotte|YT|Mexico|MX|Micronesia, Federated States Of|FM|Moldova|MD|Monaco|MC|Mongolia|MN|Montenegro|ME|Montserrat|MS|Morocco|MA|Mozambique|MZ|Myanmar|MM|Namibia|NA|Nauru|NR|Nepal|NP|Netherlands|NL|New Caledonia|NC|New Zealand|NZ|Nicaragua|NI|Niger|NE|Nigeria|NG|Niue|NU|Norfolk Island|NF|Northern Mariana Islands|MP|Norway|NO|Oman|OM|Pakistan|PK|Palau|PW|Panama|PA|Papua New Guinea|PG|Paraguay|PY|Peru|PE|Philippines|PH|Pitcairn|PN|Poland|PL|Portugal|PT|Puerto Rico|PR|Qatar|QA|Romania|RO|Russian Federation|RU|Rwanda|RW|Saint Helena|SH|Saint Kitts And Nevis|KN|Saint Lucia|LC|Saint Pierre And Miquelon|PM|Saint Vincent And The Grenedines|VC|Samoa|WS|San Marino|SM|Sao Tome and Principe|ST|Saudi Arabia|SA|Senegal|SN|Serbia|RS|Seychelles|SC|Sierra Leone|SL|Singapore|SG|Slovenia|SI|Solomon Islands|SB|Somalia|SO|South Africa|ZA|Spain|ES|Sri Lanka|LK|Sudan|SD|Suriname|SR|Svalbard And Jan Mayen|SJ|Swaziland|SZ|Sweden|SE|Switzerland|CH|Syrian Arab Republic|SY|Tajikistan|TJ|Tanzania, United Republic of|TZ|Thailand|TH|Timor-Leste|TL|Tokelau|TK|Tonga|TO|Trinidad and Tobago|TT|Tunisia|TN|Turkey|TR|Turkmenistan|TM|Turks and Caicos Islands|TC|Tuvalu|TV|Uganda|UG|Ukraine|UA|United Arab Emirates|AE|United Kingdom|GB|United States|US|Uruguay|UY|Uzbekistan|UZ|Vanuatu|VU|Vietnam|VN|Wallis and Futuna|WF|Western Sahara|EH|Yemen|YE|Zambia|ZM|Zimbabwe|ZW";

var ALL_STATES_DEFAULT = "All-state|1";

var ALL_STATES = new Array();

ALL_STATES["AU"] = "Australian Capital Territory|11|New South Wales|12|Northern Territory|13|Queensland|14|South Australia|15|Tasmania|16|Victoria|17|Western Australia|18";

ALL_STATES["IN"] = "Andaman and Nicobar Islands|11|Andhra Pradesh|12|Arunachal Pradesh|13|Assam|14|Bihar|15|Chhattisgarh|16|Delhi NCR|17|Goa|18|Gujarat|19|Haryana|20|Himachal Pradesh|21|Jammu and Kashmir|22|Jharkhand|23|Karnataka|24|Kerala|25|Madhya Pradesh|26|Maharashtra|27|Manipur|28|Meghalaya|29|Mizoram|30|Nagaland|31|Orissa|32|Punjab|33|Rajasthan|34|Sikkim|35|Tamil Nadu|36|Tripura|37|Uttar Pradesh|38|Uttarakhand|39|West Bengal|40";

ALL_STATES["MY"] = "Johor|11|Kedah|12|Kelantan|13|Kuala Lumpur|14|Labuan|15|Malacca|16|Negeri Sembilan|17|Pahang|18|Penang|19|Perak|20|Perlis|21|Putrajaya|22|Sabah|23|Sarawak|24|Selangor|25|Terengganu|26";

ALL_STATES["PH"] = "Autonomous Region in Muslim Mindanao|11|Bicol Region|12|Cagayan Valley|13|CALABARZON|14|Caraga Region|15|Central Luzon|16|Central Visayas|17|Cordillera Administrative Region|18|Davao Region|19|Eastern Visayas|20|Ilocos Region|21|MIMAROPA|22|National Capital Region|23|Northern Mindanao|24|SOCCSKSARGEN|25|Western Visayas|26|Zamboanga Peninsula|27";

var ALL_CITIES = new Array();

ALL_CITIES["AF-1"] = "Kabul|20";

ALL_CITIES["AL-1"] = "Tirana|4";

ALL_CITIES["DZ-1"] = "Algiers|21";

ALL_CITIES["AS-1"] = "Pago Pago|99";

ALL_CITIES["AD-1"] = "Andorra la Vella|78";

ALL_CITIES["AO-1"] = "Luanda|2";

ALL_CITIES["AI-1"] = "The Valley|99";

ALL_CITIES["AG-1"] = "St. John's|99";

ALL_CITIES["AR-1"] = "Buenos Aires|11";

ALL_CITIES["AM-1"] = "Yerevan|1";

ALL_CITIES["AW-1"] = "Capital|99";

ALL_CITIES["AU-11"] = "Canberra|101";

ALL_CITIES["AU-12"] = "Blue Mountains|101|Central Coast|102|Central West|103|Far North Coast|104|Islands|105|Lower North Coast|106|Mid North Coast|107|New England|108|North West|109|Outback|110|Snowy Mountains|111|South Coast|112|South West|113|Sydney|114";

ALL_CITIES["AU-13"] = "Darwin|101|Middle|102|Red Centre|103|Top End|104";

ALL_CITIES["AU-14"] = "Brisbane|101|Capricorn Coast|102|Darling Downs|103|Far North Queensland|104|Fraser Coast|105|Gold Coast|106|Islands|107|North Coast|108|Outback|109|Sunshine Coast|110|Whitsunday Coast|111";

ALL_CITIES["AU-15"] = "Adelaide|101|Adelaide Hills|102|Barossa Valley|103|Eyre Peninsula|104|Far South East|105|Fleurieu Peninsula|106|Flinders Ranges|107|Kangaroo Island|108|Mid North|109|Outback|110|Yorke Peninsula|111";

ALL_CITIES["AU-16"] = "Central Tasmania|101|East Coast|102|Hobart|103|Islands|104|North Coast|105|South Coast|106|West Coast|107";

ALL_CITIES["AU-17"] = "Far South East|101|Great Dividing Range|102|Great Ocean Road|103|Melbourne|104|Mid West|105|North|106|North West|107|South East|108|South West|109|West|110";

ALL_CITIES["AU-18"] = "Central West|101|Coral Coast|102|Islands|103|Kimberley|104|Outback|105|Perth|106|Pilbara|107|South West|108";

ALL_CITIES["AT-1"] = "Vienna|1";

ALL_CITIES["AZ-1"] = "Baku|12";

ALL_CITIES["BS-1"] = "Nassau|242";

ALL_CITIES["BH-1"] = "Manama|99";

ALL_CITIES["BD-1"] = "Alamdanga|7622|Ashugonj|8528|Avaynagar (Noapara)|4222|Badargonj|5222|Bagerhat|468|Banaripara|4332|Bandarban|361|Banshkhali|3037|Barguna|448|Barisal|431|Bauphal|4422|Bazitpur|9423|Bhairab|9424|Bhandaria|4623|Bheramara|7022|Bhola|491|Bogra|51|Brahmanbaria|851|Chandpur|841|Chandraghona (Kaptai)|3529|Chaudagram|8034|Chhatak|8723|Chittagong|31|Chuadanga|761|Comilla|81|Companigon (Bashurhat)|3223|Cox�s Bazar|341|Daudkandi|8023|Daulatpur|7023|Dhaka|2|Dinajpur|531|Faridpur|631|Feni|331|Fulbari|5327|Gaibandha|541|Galachipa|4424|Gazipur|681|Ghatail|9225|Goalanda|6423|Gopalgonj|423|Hajigonj|8424|Hatiya (Oshkhali)|3224|Hobigonj|831|Ishwardi|732|Jamalpur|981|Jessore|421|Jhalokathhi|498|Jhenaidah|451|Jhikargacha|4225|Joypurhat|571|Kalaroa|4724|Kanaighat|8233|Khagrachhari|371|Khepupara|4425|Khulna|41|Kishoregonj|941|Kurigram|581|Kustia|71|Laksham (Daulatgonj)|8032|Lakshmipur|381|Lalmonirhat|591|Madaripur|661|Magura|488|Manikgonj|651|Mathabdi|6257|Meherpur|791|Mithamain|9435|Mohanganj|9524|Moheshkhali|3424|Mongla|402|Mongla New Port|4662|Moulavibazar|861|Muktagacha|9028|Muladi|4326|Munshigonj|691|Mymensingh|91|Nabinagar|8525|Nagarpur|9233|Naogaon|741|Narail|481|Narsingdi|628|Natore|771|Nawabgonj (Chapai)|781|Netrokona|951|Nilphamari|551|Noakhali|321|Pabna|731|Paikgacha|4027|Palash (Ghorashal)|6254|Panchagar|568|Parbatipur|5334|Patuakhali|441|Perojpur|461|Plulpur|9033|Potiya|3035|Rajbari|641|Rajshahi|721|Ramgoti (Char Alexander)|3823|Rangamati|351|Rangpur|521|Saidpur|5526|Sarsha (Benapol)|4228|Satkania|3036|Satkhira|471|Shariatpur|601|Sherpur|931|Shibganj|5033|Sirajgonj|751|Sitakundu (Barabkundu)|3028|Srimangol|8626|Sunamgonj|871|Syedpur|552|Sylhet|821|Taherpur|8732|Tangail|921|Thakurgaon|561|Ukhia|3427";

ALL_CITIES["BB-1"] = "Bridgetown|99";

ALL_CITIES["BY-1"] = "Minsk|17";

ALL_CITIES["BE-1"] = "Brussels|2";

ALL_CITIES["BZ-1"] = "Belmopan|2";

ALL_CITIES["BJ-1"] = "Porto-Novo|20";

ALL_CITIES["BM-1"] = "Hamilton|99";

ALL_CITIES["BT-1"] = "Thimphu|2";

ALL_CITIES["BO-1"] = "Sucre |4";

ALL_CITIES["BA-1"] = "Sarajevo|57";

ALL_CITIES["BW-1"] = "Gaborone|31";

ALL_CITIES["BR-1"] = "Bras�lia|61";

ALL_CITIES["IO-1"] = "Capital|99";

ALL_CITIES["BN-1"] = "Brunei |99";

ALL_CITIES["BG-1"] = "Sofia|2";

ALL_CITIES["BF-1"] = "Ouagadougou|1";

ALL_CITIES["BI-1"] = "Bujumbura|99";

ALL_CITIES["KH-1"] = "Phnom Penh|23";

ALL_CITIES["CM-1"] = "Yaound�|99";

ALL_CITIES["CA-1"] = "Calgary|03|Edmonton|80|Gatineau|19|Halifax|02|London|19|Mississauga|05|Montreal|14|Ottawa|13|Quebec City|18|Saskatoon|06|Sault Ste. Marie|05|Specialized Telecom Services|00|St. John|06|St. John�s|09|Thunber Bay|07|Toronto|16|Vancouver|36|Victoria|50|Winnipeg|04|Yellowknife|67";

ALL_CITIES["CV-1"] = "Praia|99";

ALL_CITIES["KY-1"] = "George Town|99";

ALL_CITIES["CF-1"] = "Bangui|99";

ALL_CITIES["TD-1"] = "N'Djamena|51";

ALL_CITIES["CL-1"] = "Santiago |2";

ALL_CITIES["CN-1"] = "Akesu � Xinjiang|997|Ankang � Shaanxi|915|Anqing � Anhui|556|Anqiu � Shandong|5467|Anshun � Guizhou|853|Antu � Jilin|4434|Anxi � Gansu|9472|Anyang � Henan|372|Atushi � Xinjiang|9081|Baicheng � Jilin|436|Baise � Guangxi|776|Bama � Guangxi|7808|Baodin � Hebei|312|Baoji � Shaanxi|917|Baotou � Nei Mongol|472|Bazhong � Sichuan|8279|Beihai � Guangxi|779|Beijing � Beijing|10|Beipiao � Liaoning|421|Bengbu � Anhui|552|Benxi � Liaoning|414|Bole � Xinjiang|9093|Cangwu � Guangxi|774|Chang-an � Shaanxi|29|Changbai � Jilin|4458|Changchun � Jilin|431|Changde � Hunan|736|Changdu � Xizang (Tibet)|895|Changfeng � Anhui|551|Changji � Xinjiang|994|Changle � Fujian|591|Changsha � Hunan|731|Changshu � Jiangsu|512|Changye � Shanxi|355|Changzhou � Jiangsu|519|Chaozhou � Guangdong|7681|Chengde � Hebei|314|Chengdu � Sichuan|28|Chifeng � Nei Mongol|476|Chongqing � Sichuan|23|Chuxiong � Yunnan|878|Cixi � Zhejiang|574|Conghua � Guangdong|20|Da-tong � Qinghai|971|Dali � Yunnan|872|Dalian � Liaoning|411|Dandong � Liaoning|415|Danyang � Jiangsu|5211|Daqing � Heilongjiang|459|Datong � Shanxi|352|Dezhou � Shandong|534|Dinxi � Gansu|932|Dinyuan � Anhui|5604|Dongfang � Hainan|8003|Dongwan � Guangdong|7620|Dongyang � Zhejiang|579|Dujiangyan � Sichuan|8236|Dunhua � Jilin|4435|Dunhuang � Gansu|9473|Erlianhaote � Nei Mongol|4813|Erlunchun � Nei Mongol|4803|Ezhou � Hubei|711|Fanyu � Guangdong|20|Fenghua � Zhejiang|574|Fenghuang � Hunan|7480|Fengzhen � Nei Mongol|4841|Fenjie � Sichuan|8286|Fenqiu � Henan|3836|Fenyang � Shanxi|3602|Foshan � Guangdong|757|Fu-an � Fujian|593|Fu-zhou � Jiangxi|794|Fumin � Yunnan|871|Fuopin � Shaanxi|9263|Fushun � Liaoning|413|Fuxin � Liaoning|418|Fuzhou � Fujian|591|Ganzhou � Jiangxi|797|Gao-an � Jiangxi|7056|Gaoyou � Jiangsu|5247|Gejiu � Yunnan|873|Germu � Qinghai|979|Guang-an � Sichuan|8269|Guangling � Shanxi|3627|Guangning � Guangdong|758|Guangzhou � Guangdong|20|Guilin � Guangxi|773|Guiyang � Guizhou|851|Gulang � Gansu|9453|Guliu � Xinjiang|9097|Gutian � Fujian|593|Guyang � Neimengu|4823|Guyuan � Ningxia|954|Haifeng � Guangdong|7647|Haikou � Hainan|898|Hailar � Nei Mongol|470|Hailun � Heilongjiang|4652|Haimen � Jiangsu|5231|Haiyan � Zhejiang|573|Hami � Xinjiang|9022|Handan � Hebei|310|Hangzhou � Zhejiang|571|Hanning � Zhejiang|573|Hanyang � Hubei|7212|Hanzhong � Shaanxi|916|Harbin � Heilongjiang|451|Hechi � Guangxi|778|Hefei � Anhui|551|Hegang � Heilongjiang|4618|Heishan � Liaoning|416|Helan � Ningxia|951|Hengyang � Hunan|734|Henshan � Hunan|7446|Heshuo � Xinjiang|996|Hetian � Xinjiang|9032|Heze � Shandong|530|Hohhot � Nei Mongol|471|Houma � Shanxi|3651|Huachi � Gansu|9447|Huai-an � Jiangsu|517|Huaihua � Hunan|7402|Huaiyuan � Anhui|552|Huanglin � Shaanxi|911|Huangmei � Hubei|7238|Huangshanshi � Anhui|559|Huangshi � Hubei|714|Huangyan � Zhejiang|576|Huaxian � Guangdong|20|Huayin � Shaanxi|9231|Hui-an � Fujian|595|Huizhou � Guangdong|752|Hunchun � Jilin|4437|Hunjiang � Jilin|439|Huocheng � Xinjiang|9091|Huzhou � Zhejiang|572|Ji-an � Jiangxi|796|Ji-lin � Jilin|432|Ji-ning � Nei Mongol|474|Jiamusi � Heilongjiang|454|Jiangdu � Jiangsu|5244|Jiangjin � Sichuan|8221|Jiangling � Hubei|716|Jiangmen � Guangdong|750|Jiangyin � Jiangsu|5217|Jiangyou � Sichuan|8244|JianOu � Fujian|599|Jiaozhou � Shandong|5422|Jiaozuo � Henan|391|Jiaxin � Zhejiang|573|Jiayuguan � Gansu|9477|Jinan � Shandong|531|Jingangshan � Jiangxi|7060|Jingdezhen � Jiangxi|798|Jinhua � Zhejiang|579|Jining � Shandong|537|Jinmen � Hubei|724|Jinsha � Guizhou|8676|Jinxi � Liaoning|429|Jinxian � Jiangxi|791|Jinyuan � Gansu|9467|Jinzhou � Liaoning|416|Jiujiang � Jiangxi|792|Jiuquan � Gansu|937|Jixi � Heilongjiang|4617|Jiyang � Shandong|5448|Jiyuan � Henan|391|Julu � Hebei|3276|Juye � Shandong|5401|Kaifeng � Henan|378|Kaihua � Zhejiang|570|Kaili � Guizhou|855|Kaiyuan � Liaoning|410|Kangdin � Sichuan|836|Kashi � Xinjiang|998|Kelamayi � Xinjiang|990|Kuitun � Xinjiang|992|Kunming � Yunnan|871|Kunshan � Jiangsu|512|Kurle � Xinjiang|996|Lai-yang � Shandong|5427|Laiyang � Hunan|7442|Lankao � Henan|3881|Lanxi � Zhejiang|579|Lanzhou � Gansu|931|Leshan � Sichuan|833|Lhasa � Xizang (Tibet)|891|Liancheng � Fujian|597|Liangcheng � Nei Mongol|4848|Lianyungang � Jiangsu|518|Liaoyang � Liaoning|419|Liaoyuan � Jilin|437|Lijiangshi � Yunnan|8891|Lijin � Shandong|5430|Lin-an � Zhejiang|571|Linfen � Shanxi|357|Linhai � Zhejiang|576|Lintong � Shaanxi|29|Linxi � Neimengu|4868|Linyi � Shandong|539|Linzhi � Xizang (Tibet)|894|Liujiang � Guangxi|772|Liuyang � Hunan|731|Liuzhou � Guangxi|772|Longquan � Zhejiang|5885|Longxi � Gansu|9421|Luntai � Xinjiang|996|Luoyang � Henan|379|Lushanshi � Jiangxi|792|Ma-anshan � Anhui|555|Machen � Hubei|7232|Mangkang � Xizang|8054|Manzhouli � Nei Mongol|4812|Maomin � Guangdong|7683|Meishan � Sichuan|8425|Meizhou � Guangdong|753|Mianyang � Sichuan|816|Minhou � Fujian|591|Miquan � Xinjiang|9041|Mohe � Heilongjiang|457|Mudanjiang � Heilongjiang|467|Nanchang � Jiangxi|791|Nanfeng � Jiangxi|7048|Nanjing � Jiangsu|25|Nanning � Guangxi|771|Nantong � Jiangsu|513|Nanyang � Henan|377|Nenjiang � Heilongjiang|4665|Ningbo � Zhejiang|574|Ningcheng � Nei Mongol|4862|Ningguo � Anhui|5639|Ninghua � Fujian|598|Nnchong � Sichuan|817|Panzhihua � Sichuan|812|Peilin � Sichuan|810|Penglai � Shandong|5457|Pindinshan � Henan|375|Ping-xiang � Jiangxi|799|Pingliang � Gansu|933|Pingxiang � Guangxi|7815|Pinle � Guangxi|7847|Putian � Fujian|594|Qidong � Jiangsu|5232|Qin-an � Gansu|938|Qingdao � Shandong|532|Qinhuangdao � Hebei|335|Qiongshan � Hainan|898|Qiqihar � Heilongjiang|452|Qitai � Xinjiang|994|Quanzhou � Fujian|595|Qufu � Shandong|5473|Qujiang � Guangdong|751|Renqiu � Hebei|3420|Rikeze � Xizang (Tibet)|892|Rongxian � Guangxi|775|Ruijin � Jiangxi|7077|Ruili � Yunnan|8885|Runan � Henan|3013|Sanmen � Zhejiang|576|Sanmenxia � Henan|3891|Sanya � Hainan|898|Shangdu � Nei Mongol|4843|Shanggao � Jiangxi|7051|Shanghai � Shanghai|21|Shangqiu � Henan|370|Shangrao � Jiangxi|793|Shangzhou � Shaanxi|914|Shantou � Guangdong|754|Shaoguan � Guangdong|751|Shaoshan � Hunan|732|Shaoxing � Zhejiang|575|Shaoyang � Hunan|739|Shenmu � Shaanxi|9229|Shenyang � Liaoning|24|Shenzhen � Guangdong|755|Shihezi � Xinjiang|993|Shijiazhuang � Hebei|311|Shimen � Hunan|7462|Shimian � Sichuan|8437|Shishi � Fujian|595|Shiyan � Hubei|719|Shuangcheng � Heilongjiang|4615|Siping � Jilin|434|Sishui � Shandong|5483|Songyuan � Jilin|438|Suifenhe � Heilongjiang|4638|Suzhou � Jiangsu|512|Tacheng � Xinjiang|9003|Tai-an � Shandong|538|Tai-hu � Anhui|556|Taicang � Jiangsu|512|Taiyuan � Shanxi|351|Taizhou � Jiangsu|5241|Tangshan � Hebei|315|Tianjin � Tianjin|22|Tianshui � Gansu|938|Tiantai � Zhejiang|576|Tongchuan � Shaanxi|919|Tongguan � Shaanxi|9233|Tonghua � Jilin|435|Tongliao � Nei Mongol|475|Tongling � Anhui|5612|Tongren � Guizhou|856|Tumen � Jilin|4436|Tuquan � Neimengu|4858|Turufan � Xinjiang|995|Urumqi � Xinjiang|991|Wanxian � Sichuan|819|Weifang � Shandong|536|Weili � Xinjiang|996|Weinan � Shaanxi|913|Wenshan � Yunnan|876|Wenzhou � Zhejiang|577|Wuchuan � Nei Mongol|4817|Wudu � Gansu|939|Wuhai � Nei Mongol|473|Wuhan � Hubei|27|Wuhu � Anhui|553|Wulanhaote � Nei Mongol|4814|Wuwei � Gansu|935|Wuxi � Jiangsu|510|Wuxian � Jiangsu|512|Wuxue � Hubei|7239|Wuyishan � Fujian|599|Wuyuan � Neimengu|4881|Xian � Shaanxi|29|Xi-feng � Gansu|934|Xiamen � Fujian|592|Xiangfan � Hubei|710|Xiangtan � Hunan|732|Xianju � Zhejiang|576|Xianyang � Shaanxi|910|Xichang � Sichuan|834|Xifeng � Guizhou|8638|Xin-tai � Shandong|5482|Xingguo � Jiangxi|7073|Xinhui � Guangdong|750|Xining � Qinghai|971|Xinmin � Liaoning|24|Xintai � Hebei|319|Xinxiang � Henan|373|Xinyi � Guizhou|859|Xinyu � Jiangxi|790|Xinyuan � Xinjiang|9096|Xuanhua � Hebei|313|Xuchang � Henan|374|Xuejiawan � Nei Mongol|477|Xuzhou � Jiangsu|516|Ya-an � Sichuan|835|Yan-an � Shaanxi|911|Yancheng � Jiangsu|515|Yangpu � Hainan|890|Yangquan � Shanxi|353|Yangzhou � Jiangsu|514|Yanji � Jilin|433|Yanjin � Henan|3832|Yantai � Shandong|535|Yi-chun � Jiangxi|795|Yi-yang � Hunan|737|Yibin � Sichuan|831|Yichun � Heilongjiang|4652|Yimin � Nei Mongol|4890|Yinchuan � Ningxia|951|Yingkou � Liaoning|417|Yingtan � Jiangxi|7032|Yining � Xinjiang|999|Yishui � Shandong|5492|Yiwu � Zhejiang|579|Yixin � Jiangsu|5218|Yiyang � Jiangxi|7038|Yongchuan � Sichuan|814|Yongfu � Guangxi|7848|Yongxiu � Jiangxi|792|Yu-shu � Jilin|4415|Yuanmou � Yunnan|8869|Yueyang � Hunan|730|Yuhang � Zhejiang|571|Yulin � Shaanxi|912|Yumen � Gansu|9471|Yushu � Qinghai|976|Yuxi � Yunnan|877|Yuyao � Zhejiang|574|Yuzhong � Gansu|9440|Zhangjiagang � Jiangsu|512|Zhangjiakou � Hebei|313|Zhangshu � Jiangxi|7052|Zhangzhou � Fujian|596|Zhanjiang � Guangdong|759|Zhengzhou � Henan|371|Zhenjiang � Jiangsu|511|Zhicheng � Hubei|27|Zhijiang � Hunan|7420|Zhongshan � Guangdong|760|Zhuhai � Guangdong|756|Zhumadian � Henan|3011|Zhunger � Nei Mongol|4873|Zhuzhou � Hunan|733|Zibo � Shandong|533|Zigong � Sichuan|813|Zigui � Hubei|7278|Ziyuan � Guangxi|7852|Zunhua � Hebei|315|Zunyi � Guizhou|852";

ALL_CITIES["CX-1"] = "The Settlement|99";

ALL_CITIES["CC-1"] = "West Island|99";

ALL_CITIES["CO-1"] = "Bogot�|1";

ALL_CITIES["KM-1"] = "Moroni|99";

ALL_CITIES["CG-1"] = "Kinshasa|99";

ALL_CITIES["CK-1"] = "Avarua|99";

ALL_CITIES["CR-1"] = "San Jos�|99";

ALL_CITIES["HR-1"] = "Zagreb|1";

ALL_CITIES["CU-1"] = "Havana|7";

ALL_CITIES["CY-1"] = "Nicosia|22";

ALL_CITIES["CZ-1"] = "Prague|3";

ALL_CITIES["DK-1"] = "Copenhagen|99";

ALL_CITIES["DJ-1"] = "Djibouti|99";

ALL_CITIES["DO-1"] = "Santo Domingo|99";

ALL_CITIES["EC-1"] = "Quito|2";

ALL_CITIES["EG-1"] = "Cairo|2";

ALL_CITIES["SV-1"] = "San Salvador|2";

ALL_CITIES["GQ-1"] = "Malabo|7";

ALL_CITIES["ER-1"] = "Asmara|99";

ALL_CITIES["EE-1"] = "Tallinn|60";

ALL_CITIES["ET-1"] = "Addis Ababa|1";

ALL_CITIES["FK-1"] = "Stanley|99";

ALL_CITIES["FO-1"] = "Capital|99";

ALL_CITIES["FJ-1"] = "Suva|32";

ALL_CITIES["FI-1"] = "Helsinki|9";

ALL_CITIES["FR-1"] = "Paris|1";

ALL_CITIES["PF-1"] = "Papeete|99";

ALL_CITIES["GA-1"] = "Libreville|99";

ALL_CITIES["GM-1"] = "Capital|2";

ALL_CITIES["GE-1"] = "Tbilisi|32";

ALL_CITIES["DE-1"] = "Berlin|30";

ALL_CITIES["GH-1"] = "Accra|21";

ALL_CITIES["GI-1"] = "Capital|4";

ALL_CITIES["GR-1"] = "Aegina|2297|Aeginio|2353|Aegio|2691|Afidnes|2295|Ag. Kirikos|2275|Ag. Nikolaos|2841|Ag. Sotira|2293|Ag. Varvara|2894|Agia|2494|Agiasos|2252|Agrinio|2641|Aitoliko|2632|Akrata|2696|Alexandria|2333|Alexandroupoli|2551|Aliartos|2268|Aliveri|2223|Almyros|2422|Amaliada|2622|Amari|2833|Amfikleia|2234|Amfilochia|2642|Amfissa|2265|Amynteo|2386|Anc. Olympia|2624|Andritsena|2626|Andros|2282|Archagelos|2244|Argos|2751|Argostoli|2671|Aridea|2384|Arkalochori|2891|Arnea|2372|Arta|2681|Asprovalta|2397|Astros|2755|Atalanti|2233|Athens|21|Chalandritsa|2694|Chalkiopoulo|2647|Chania|2821|Chios|2271|Chlkidona|2391|Chrysoupoli|2591|Corfu|2661|Delvinaki|2657|Didymoticho|2553|Distomo|2267|Domokos|2232|Domvrena|2264|Drama|2521|Echinos|2544|Edessa|2381|Elassona|2493|Eleftheroupoli|2592|Eretria|2229|Farkadona|2433|Farsala|2491|Feres|2555|Filiates|2664|Florina|2385|Fyties|2646|Gargalianoi|2763|Giannitsa|2382|Grevena|2462|Gytheio|2733|Halkida|2221|Herakleia|2325|Iasmos|2534|Ierapetra|2842|Ierissos|2377|Igoumenitsa|2665|Iraklion|281|K. Nevrokopi|2523|Kala Nera|2423|Kalabaka|2432|Kalamata|2721|Kalavrita|2692|Kalentzi|2659|Kalianoi|2747|Kalimnos|2243|Kallikrateia|2399|Kallisti|2535|Kalloni|2253|Kam.Vourla|2235|Kanalaki|2684|Kantanos|2823|Kardamila|2272|Karditsa|2441|Karies|2653|Karpathos|2245|Karpenisi|2237|Karystos|2224|Kassandreia|2374|Kastoria|2467|Kastri|2792|Katerini|2351|Kavala|251|Kea|2288|Kiato|2742|Kilkis|2341|Kissamos|2822|Kolympari|2824|Komotini|2531|Konitsa|2655|Kopanaki|2765|Korinthos|2741|Koroni|2725|Kos|2242|Kozani|2461|Kranidi|2754|Krestena|2625|Kymi|2222|Kyparissia|2761|Kyprinos|2556|Kythira|2736|Lagadas|2394|Lagadikia|2393|Lagonisi|2291|Lamia|2231|Larissa|241|Lavrion|2292|Lechena|2623|Lefkada|2645|Lefkimi Corfu|2662|Leonidio|2757|Leros|2247|Levidi|2796|Lidoriki|2266|Limenari|2593|Limin Chersonisou|2897|Livadeia|2261|Loutra Aidipsou|2226|Loutraki|2744|Lygourgio|2753|Makrakomi|2236|Makrychori|2495|Mantoudi|2227|Markopoulo|2299|Mataraga|2635|Megalopoli|2791|Megara|2296|Meligalas|2724|Messini|2722|Messologi|2631|Mikonos|2289|Milos|2287|Mirina|2254|Mitilini|2251|Moires|2892|Molaoi|2732|Moudania|2373|Mouzaki|2445|Nafpaktos|2634|Nafplion|2752|Naousa|2332|Naxos|2285|Neapoli S|2734|Nemea|2746|Nigrita|2322|Nikitas|2375|Orestiada|2552|Palamas|2444|Paramythia|2666|Paranesti|2524|Paros|2284|Patra|261|Perama|2834|Peramos|2594|Perdika|2654|Perea|2392|Philippiada|2683|Plaka|2352|Polikastro|2343|Polygyros|2371|Poros|2298|Preveza|2682|Prosotsani|2522|Psachna|2228|Ptolemaida|2463|Pyli|2434|Pylos|2723|Pyrgos|2621|Pyrgos (Creta)|2893|Rafina|2294|Rethymno|2831|Rhodes|2241|Rodopoli|2327|Salakos|2246|Sami|2674|Samos|2273|Sapes|2532|Serres|2321|Servia|2464|Siatista|2465|Sidirokastro|2323|Siros|2281|Sitia|2843|Skala|2735|Skiathos|2427|Skopelos|2424|Skripero Corfu|2663|Sochos|2395|Sofades|2443|Soufli|2554|Sparti|2731|Spili|2832|Stavroupoli|2542|Stilida|2238|Stratoni|2376|Tampouria|2656|Thermo|2644|Thessaloniki|231|Thira|2286|Thiva|2262|Tinos|2283|Trikala|2431|Tripoli| 271|Tropea|2797|Tyrnavos|2492|Tzermiades|2844|Vamos|2825|Vasilika|2396|Velestino|2425|Veroia|2331|Viannos|2895|Vilia|2263|Vitina|2795|Volissos|2274|Volos|2428|Vonitsa|2643|Voulgareli|2685|Xanthi|2541|Xylagani|2533|Xylokastro|2743|Zagora|2426|Zakynthos|2695|Zichni|2324|Zitsa|2658";

ALL_CITIES["GL-1"] = "Capital|3";

ALL_CITIES["GD-1"] = "St. George's|414";

ALL_CITIES["GP-1"] = "Capital|99";

ALL_CITIES["GU-1"] = "Hagatna|99";

ALL_CITIES["GT-1"] = "Guatemala City|99";

ALL_CITIES["GN-1"] = "Conakry|99";

ALL_CITIES["GY-1"] = "Georgetown|99";

ALL_CITIES["HT-1"] = "Port-au-Prince|99";

ALL_CITIES["HM-1"] = "Capital|99";

ALL_CITIES["VA-1"] = "Vatican City|99";

ALL_CITIES["HN-1"] = "Tegucigalpa|2";

ALL_CITIES["HK-1"] = "Hong Kong|2";

ALL_CITIES["HU-1"] = "Budapest|1";

ALL_CITIES["IS-1"] = "Reykjav�k|5";

ALL_CITIES["IN-11"] = "Port Blair|101";

ALL_CITIES["IN-12"] = "Adilabad|101|Adoni|102|Amadalavalasa|103|Amalapuram|104|Anakapalle|105|Anantapur|106|Badepalle|107|Banganapalle|108|Bapatla|109|Bellampalle|110|Bethamcherla|111|Bhadrachalam|112|Bhainsa|113|Bheemunipatnam|114|Bhimavaram|115|Bhongir|116|Bobbili|117|Bodhan|118|Chilakaluripet|119|Chirala|120|Chittoor|121|Cuddapah|122|Devarakonda|123|Dharmavaram|124|Eluru|125|Farooqnagar|126|Gadwal|127|Gooty|128|Gudivada|129|Gudur|130|Guntakal|131|Guntur|132|Hanuman Junction|133|Hindupur|134|Hyderabad|135|Ichchapuram|136|Jaggaiahpet|137|Jagtial|138|Jammalamadugu|139|Jangaon|140|Kadapa|141|Kadiri|142|Kagaznagar|143|Kakinada|144|Kalyandurg|145|Kamareddy|146|Kandukur|147|Karimnagar|148|Kavali|149|Khammam|150|Koratla|151|Kothagudem|152|Kothapeta|153|Kovvur|154|Kurnool|155|Kyathampalle|156|Macherla|157|Machilipatnam|158|Madanapalle|159|Mahbubnagar|160|Mancherial|161|Mandamarri|162|Mandapeta|163|Manuguru|164|Markapur|165|Medak|166|Miryalaguda|167|Mogalthur|168|Nagari|169|Nagarkurnool|170|Nandyal|171|Narasapur|172|Narasaraopet|173|Narayanpet|174|Narsipatnam|175|Nellore|176|Nidadavole|177|Nirmal|178|Nizamabad|179|Nuzvid|180|Ongole|181|Palacole|182|Palasa Kasibugga|183|Palwancha|184|Parvathipuram|185|Pedana|186|Peddapuram|187|Pithapuram|188|Pondur|189|Ponnur|190|Proddatur|191|Punganur|192|Puttur|193|Rajahmundry|194|Rajam|195|Ramachandrapuram|196|Ramagundam|197|Rayachoti|198|Rayadurg|199|Renigunta|200|Repalle|201|Sadasivpet|202|Salur|203|Samalkot|204|Sangareddy|205|Sattenapalle|206|Siddipet|207|Singapur|208|Sircilla|209|Srikakulam|210|Srikalahasti|211|Suryapet|212|Tadepalligudem|213|Tadpatri|214|Tandur|215|Tanuku|216|Tenali|217|Tirupati|218|Tuni|219|Uravakonda|220|Venkatagiri|221|Vicarabad|222|Vijayawada|223|Vinukonda|224|Visakhapatnam|225|Vizianagaram|226|Wanaparthy|227|Warangal|228|Yellandu|229|Yemmiganur|230|Yerraguntla|231|Zahirabad|232";

ALL_CITIES["IN-13"] = "Along|101|Bomdila|102|Itanagar|103|Naharlagun|104|Pasighat|105";

ALL_CITIES["IN-14"] = "Abhayapuri|101|Amguri|102|Anandnagaar|103|Barpeta|104|Barpeta Road|105|Bilasipara|106|Bongaigaon|107|Dhekiajuli|108|Dhubri|109|Dibrugarh|110|Digboi|111|Diphu|112|Dispur|113|Gauripur|114|Goalpara|115|Golaghat|116|Guwahati|117|Haflong|118|Hailakandi|119|Hojai|120|Jorhat|121|Karimganj|122|Kokrajhar|123|Lanka|124|Lumding|125|Mangaldoi|126|Mankachar|127|Margherita|128|Mariani|129|Marigaon|130|Nagaon|131|Nalbari|132|North Lakhimpur|133|Rangia|134|Sibsagar|135|Silapathar|136|Silchar|137|Tezpur|138|Tinsukia|139";

ALL_CITIES["IN-15"] = "Amarpur|101|Araria|102|Areraj|103|Arrah|104|Asarganj|105|Aurangabad|106|Bagaha|107|Bahadurganj|108|Bairgania|109|Bakhtiarpur|110|Banka|111|Banmankhi Bazar|112|Barahiya|113|Barauli|114|Barbigha|115|Barh|116|Begusarai|117|Behea|118|Bettiah|119|Bhabua|120|Bhagalpur|121|Bihar Sharif|122|Bikramganj|123|Bodh Gaya|124|Buxar|125|Chandan Bara|126|Chanpatia|127|Chhapra|128|Colgong|129|Dalsinghsarai|130|Darbhanga|131|Daudnagar|132|Dehri-on-Sone|133|Dhaka|134|Dighwara|135|Dumraon|136|Fatwah|137|Forbesganj|138|Gaya|139|Gogri Jamalpur|140|Gopalganj|141|Hajipur|142|Hilsa|143|Hisua|144|Islampur|145|Jagdispur|146|Jamalpur|147|Jamui|148|Jehanabad|149|Jhajha|150|Jhanjharpur|151|Jogabani|152|Kanti|153|Katihar|154|Khagaria|155|Kharagpur|156|Kishanganj|157|Lakhisarai|158|Lalganj|159|Madhepura|160|Madhubani|161|Maharajganj|162|Mahnar Bazar|163|Makhdumpur|164|Maner|165|Manihari|166|Marhaura|167|Masaurhi|168|Mirganj|169|Mokameh|170|Motihari|171|Motipur|172|Munger|173|Murliganj|174|Muzaffarpur|175|Narkatiaganj|176|Naugachhia|177|Nawada|178|Nokha|179|Patna|180|Piro|181|Purnia|182|Rafiganj|183|Rajgir|184|Ramnagar|185|Raxaul Bazar|186|Revelganj|187|Rosera|188|Saharsa|189|Samastipur|190|Sasaram|191|Sheikhpura|192|Sheohar|193|Sherghati|194|Silao|195|Sitamarhi|196|Siwan|197|Sonepur|198|Sugauli|199|Sultanganj|200|Supaul|201|Warisaliganj|202";

ALL_CITIES["IN-16"] = "Ahiwara|101|Akaltara|102|Ambagarh Chowki|103|Ambikapur|104|Arang|105|Bade Bacheli|106|Balod|107|Baloda Bazar|108|Bemetra|109|Bhatapara|110|Bilaspur|111|Birgaon|112|Champa|113|Chirmiri|114|Dalli-Rajhara|115|Dhamtari|116|Dipka|117|Dongargarh|118|Durg-Bhilai Nagar|119|Gobranawapara|120|Jagdalpur|121|Janjgir|122|Jashpurnagar|123|Kanker|124|Kawardha|125|Kondagaon|126|Korba|127|Mahasamund|128|Mahendragarh|129|Mungeli|130|Naila Janjgir|131|Raigarh|132|Raipur|133|Rajnandgaon|134|Sakti|135|Tilda Newra|136";

ALL_CITIES["IN-17"] = "Asola|101|Delhi|102";

ALL_CITIES["IN-18"] = "Margao|101|Marmagao|102|Panaji|103";

ALL_CITIES["IN-19"] = "Ahmedabad|180|Bharuch|101|Bhavnagar|102|Bhuj|103|Cambay|104|Dahod|105|Deesa|106|Dholka|107|Gandhinagar|108|Godhra|109|Himatnagar|110|Idar|111|Jamnagar|112|Junagadh|113|Kadi|114|Kalavad|115|Kalol|116|Kapadvanj|117|Karjan|118|Keshod|119|Khambhalia|120|Khambhat|121|Kheda|122|Khedbrahma|123|Kheralu|124|Kodinar|125|Lathi|126|Limbdi|127|Lunawada|128|Mahesana|129|Mahuva|130|Manavadar|131|Mandvi|132|Mangrol|133|Mansa|134|Mehmedabad|135|Modasa|136|Morvi|137|Nadiad|138|Navsari|139|Padra|140|Palanpur|141|Palitana|142|Pardi|143|Patan|144|Petlad|145|Porbandar|146|Radhanpur|147|Rajkot|148|Rajpipla|149|Rajula|150|Ranavav|151|Rapar|152|Salaya|153|Sanand|154|Savarkundla|155|Sidhpur|156|Sihor|157|Songadh|158|Surat|159|Talaja|160|Thangadh|161|Tharad|162|Umbergaon|163|Umreth|164|Una|165|Unjha|166|Upleta|167|Vadnagar|168|Vadodara|169|Valsad|170|Vapi|172|Veraval|173|Vijapur|174|Viramgam|175|Visnagar|176|Vyara|177|Wadhwan|178|Wankaner|179";

ALL_CITIES["IN-20"] = "Ateli|101|Babiyal|102|Bahadurgarh|103|Barwala|104|Bhiwani|105|Charkhi Dadri|106|Cheeka|107|Ellenabad 2|108|Faridabad|109|Fatehabad|110|Ganaur|111|Gharaunda|112|Gohana|113|Gurgaon|114|Haibat(Yamuna Nagar)|115|Hansi|116|Hisar|117|Hodal|118|Jhajjar|119|Jind|120|Kaithal|121|Kalan Wali|122|Kalka|123|Karnal|124|Ladwa|125|Mahendragarh|126|Mandi Dabwali|127|Narnaul|128|Narwana|129|Palwal|130|Panchkula|131|Panipat|132|Pehowa|133|Pinjore|134|Rania|135|Ratia|136|Rewari|137|Rohtak|138|Safidon|139|Samalkha|140|Shahbad|141|Sirsa|142|Sohna|143|Sonipat|144|Taraori|145|Thanesar|146|Tohana|147|Yamunanagar|148";

ALL_CITIES["IN-21"] = "Dalhousie|101|Dharamsala|102|Hamirpur|103|Mandi|104|Nahan|105|Shimla|106|Solan|107|Sundarnagar|108|Achabbal|109|Akhnoor|110|Anantnag|111|Arnia|112|Awantipora|113|Bandipore|114|Baramula|115|Kathua|116|Leh|117|Punch|118|Rajauri|119|Sopore|120|Srinagar|121|Udhampur|122";

ALL_CITIES["IN-23"] = "Chaibasa|101|Chakradharpur|102|Chandrapura|103|Chatra|104|Chirkunda|105|Churi|106|Daltonganj|107|Deoghar|108|Dhanbad|109|Dumka|110|Garhwa|111|Ghatshila|112|Giridih|113|Godda|114|Gomoh|115|Gumia|116|Gumla|117|Hazaribag|118|Hussainabad|119|Jamshedpur|120|Jamtara|121|Jhumri Tilaiya|122|Khunti|123|Lohardaga|124|Madhupur|125|Mihijam|126|Musabani|127|Pakaur|128|Patratu|129|Phusro|130|Ramngarh|131|Ranchi|132|Sahibganj|133|Saunda|134|Simdega|135|Tenu Dam-cum- Kathhara|136";

ALL_CITIES["IN-24"] = "Bagalkote|179|Bangalore|180|Belgaum|181|Bellary|182|Bidar|183|Bijapur|184|Chamrajnagar|101|Chikkaballapur|102|Chikmagalur|175|Chintamani|103|Chitradurga|104|Davanagere|176|Dharwad|177|Gadag|178|Gulbarga|105|Gundlupet|106|Hassan|107|Hospet|108|Hubli|109|Karkala|110|Karwar|111|Kolar|112|Kota|113|Lakshmeshwar|114|Lingsugur|115|Maddur|116|Madhugiri|117|Madikeri|118|Magadi|119|Mahalingpur|120|Malavalli|121|Malur|122|Mandya|123|Mangalore|124|Manvi|125|Mudalgi|126|Mudbidri|127|Muddebihal|128|Mudhol|129|Mulbagal|130|Mundargi|131|Mysore|132|Nanjangud|133|Pavagada|134|Puttur|135|Rabkavi Banhatti|136|Raichur|137|Ramanagaram|138|Ramdurg|139|Ranibennur|140|Robertson Pet|141|Ron|142|Sadalgi|143|Sagar|144|Sakleshpur|145|Sandur|146|Sankeshwar|147|Saundatti-Yellamma|148|Savanur|149|Sedam|150|Shahabad|151|Shahpur|152|Shiggaon|153|Shikapur|154|Shimoga|155|Shorapur|156|Shrirangapattana|157|Sidlaghatta|158|Sindgi|159|Sindhnur|160|Sira|161|Sirsi|162|Siruguppa|163|Srinivaspur|164|Talikota|165|Tarikere|166|Tekkalakota|167|Terdal|168|Tiptur|169|Tumkur|170|Udupi|171|Vijayapura|172|Wadi|173|Yadgir|174";

ALL_CITIES["IN-25"] = "Adoor|101|Akathiyoor|102|Alappuzha|103|Ancharakandy|104|Aroor|105|Ashtamichira|106|Attingal|107|Avinissery|108|Chalakudy|109|Changanassery|110|Chendamangalam|111|Chengannur|112|Cherthala|113|Cheruthazham|114|Chittur-Thathamangalam|115|Chockli|116|Erattupetta|117|Guruvayoor|118|Irinjalakuda|119|Kadirur|120|Kalliasseri|121|Kalpetta|122|Kanhangad|123|Kanjikkuzhi|124|Kannur|125|Kasaragod|126|Kayamkulam|127|Kochi|128|Kodungallur|129|Kollam|130|Koothuparamba|131|Kothamangalam|132|Kottayam|133|Kozhikode|134|Kunnamkulam|135|Malappuram|136|Mattannur|137|Mavelikkara|138|Mavoor|139|Muvattupuzha|140|Nedumangad|141|Neyyattinkara|142|Ottappalam|143|Palai|144|Palakkad|145|Panniyannur|146|Pappinisseri|147|Paravoor|148|Pathanamthitta|149|Payyannur|150|Peringathur|151|Perinthalmanna|152|Perumbavoor|153|Ponnani|154|Punalur|155|Quilandy|156|Shoranur|157|Taliparamba|158|Thiruvalla|159|Thiruvananthapuram|160|Thodupuzha|161|Thrissur|162|Tirur|163|Vadakara|164|Vaikom|165|Varkala|166";

ALL_CITIES["IN-26"] = "Ashok Nagar|101|Balaghat|102|Betul|103|Bhopal|104|Burhanpur|105|Chhatarpur|106|Dabra|107|Datia|108|Dewas|109|Dhar|110|Fatehabad|111|Gwalior|112|Indore|113|Itarsi|114|Jabalpur|115|Katni|116|Kotma|117|Lahar|118|Lundi|119|Maharajpur|120|Mahidpur|121|Maihar|122|Malajkhand|123|Manasa|124|Manawar|125|Mandideep|126|Mandla|127|Mandsaur|128|Mauganj|129|Mhow Cantonment|130|Mhowgaon|131|Morena|132|Multai|133|Murwara|134|Nagda|135|Nainpur|136|Narsinghgarh|138|Neemuch|139|Nepanagar|140|Niwari|141|Nowgong|142|Nowrozabad|143|Pachore|144|Pali|145|Panagar|146|Pandhurna|147|Panna|148|Pasan|149|Pipariya|150|Pithampur|151|Porsa|152|Prithvipur|153|Raghogarh-Vijaypur|154|Rahatgarh|155|Raisen|156|Rajgarh|157|Ratlam|158|Rau|159|Rehli|160|Rewa|161|Sabalgarh|162|Sagar|163|Sanawad|164|Sarangpur|165|Sarni|166|Satna|167|Sausar|168|Sehore|169|Sendhwa|170|Seoni|171|Seoni-Malwa|172|Shahdol|173|Shajapur|174|Shamgarh|175|Sheopur|176|Shivpuri|177|Shujalpur|178|Sidhi|179|Sihora|180|Singrauli|181|Sironj|182|Sohagpur|183|Tarana|184|Tikamgarh|185|Ujhani|186|Ujjain|187|Umaria|188|Vidisha|189|Wara Seoni|190";

ALL_CITIES["IN-27"] = "Kolhapur|101|Ahmednagar|102|Akola|103|Amravati|104|Aurangabad|105|Baramati|106|Chalisgaon|107|Chinchani|108|Devgarh|109|Dhule|110|Dombivli|111|Durgapur|112|Ichalkaranji|113|Jalna|114|Kalyan|115|Latur|116|Loha|117|Lonar|118|Lonavla|119|Mahad|120|Mahuli|121|Malegaon|122|Malkapur|123|Manchar|124|Mangalvedhe|125|Mangrulpir|126|Manjlegaon|127|Manmad|128|Manwath|129|Mehkar|130|Mhaswad|131|Miraj|132|Morshi|133|Mukhed|134|Mul|135|Mumbai|136|Murtijapur|137|Nagpur|138|Nalasopara|139|Nanded-Waghala|140|Nandgaon|141|Nandura|142|Nandurbar|143|Narkhed|144|Nashik|145|Navi Mumbai|146|Nawapur|147|Nilanga|148|Osmanabad|149|Ozar|150|Pachora|151|Paithan|152|Palghar|153|Pandharkaoda|154|Pandharpur|155|Panvel|156|Parbhani|157|Parli|158|Parola|159|Partur|160|Pathardi|161|Pathri|162|Patur|163|Pauni|164|Pen|165|Phaltan|166|Pulgaon|167|Pune|168|Purna|169|Pusad|170|Rahuri|171|Rajura|172|Ramtek|173|Ratnagiri|174|Raver|175|Risod|176|Sailu|177|Sangamner|178|Sangli|179|Sangole|180|Sasvad|181|Satana|182|Satara|183|Savner|184|Sawantwadi|185|Shahade|186|Shegaon|187|Shendurjana|188|Shirdi|189|Shirpur-Warwade|190|Shirur|191|Shrigonda|192|Shrirampur|193|Sillod|194|Sindhudurg|214|Sinnar|195|Solapur|196|Soyagaon|197|Talegaon Dabhade|198|Talode|199|Tasgaon|200|Tirora|201|Tuljapur|202|Tumsar|203|Uran|204|Uran Islampur|205|Wadgaon Road|206|Wai|207|Wani|208|Wardha|209|Warora|210|Warud|211|Washim|212|Yevla|213";

ALL_CITIES["IN-28"] = "Imphal|101|Kakching|102|Lilong|103|Mayang Imphal|104|Thoubal|105";

ALL_CITIES["IN-29"] = "Jowai|101|Nongstoin|102|Shillong|103|Tura|104";

ALL_CITIES["IN-30"] = "Aizawl|101|Champhai|102|Lunglei|103|Saiha|104";

ALL_CITIES["IN-31"] = "Wokha|101|Zunheboto|102";

ALL_CITIES["IN-32"] = "Ahmedgarh|101|Amritsar|102|Barnala|103|Batala|104|Bathinda|105|Bhagha Purana|106|Budhlada|107|Chandigarh|108|Dasua|109|Dhuri|110|Dinanagar|111|Faridkot|112|Fazilka|113|Firozpur|114|Firozpur Cantt.|115|Giddarbaha|116|Gobindgarh|117|Gurdaspur|118|Hoshiarpur|119|Jagraon|120|Jaitu|121|Jalalabad|122|Jalandhar|123|Jalandhar Cantt.|124|Jandiala|125|Kapurthala|126|Karoran|127|Kartarpur|128|Khanna|129|Kharar|130|Kot Kapura|131|Kurali|132|Longowal|133|Ludhiana|134|Malerkotla|135|Malout|136|Mansa|137|Maur|138|Moga|139|Mohali|140|Morinda|141|Mukerian|142|Muktsar|143|Nabha|144|Nakodar|145|Nangal|146|Nawanshahr|147|Pathankot|148|Patiala|149|Patran|150|Patti|151|Phagwara|152|Phillaur|153|Qadian|154|Raikot|155|Rajpura|156|Rampura Phul|157|Rupnagar|158|Samana|159|Sangrur|160|Sirhind Fatehgarh Sahib|161|Sujanpur|162|Sunam|163|Talwara|164|Tarn Taran|165|Urmar Tanda|166|Zira|167|Zirakpur|168";

ALL_CITIES["IN-34"] = "Ajmer|101|Alwar|102|Bandikui|103|Baran|104|Barmer|105|Bikaner|106|Fatehpur|107|Jaipur|108|Jaisalmer|109|Jodhpur|110|Kota|111|Lachhmangarh|112|Ladnu|113|Lakheri|114|Lalsot|115|Losal|116|Makrana|117|Malpura|118|Mandalgarh|119|Mandawa|120|Mangrol|121|Merta City|122|Mount Abu|123|Nadbai|124|Nagar|125|Nagaur|126|Nargund|127|Nasirabad|128|Nathdwara|129|Navalgund|130|Nawalgarh|131|Neem-Ka-Thana|132|Nelamangala|133|Nimbahera|134|Nipani|135|Niwai|136|Nohar|137|Nokha|138|Pali|139|Phalodi|140|Phulera|141|Pilani|142|Pilibanga|143|Pindwara|144|Pipar City|145|Prantij|146|Pratapgarh|147|Raisinghnagar|148|Rajakhera|149|Rajaldesar|150|Rajgarh (Alwar)|151|Rajgarh (Churu|152|Rajsamand|153|Ramganj Mandi|154|Ramngarh|155|Ratangarh|156|Rawatbhata|157|Rawatsar|158|Reengus|159|Sadri|160|Sadulshahar|161|Sagwara|162|Sambhar|163|Sanchore|164|Sangaria|165|Sardarshahar|166|Sawai Madhopur|167|Shahpura|169|Sheoganj|170|Sikar|171|Sirohi|172|Sojat|173|Sri Madhopur|174|Sujangarh|175|Sumerpur|176|Suratgarh|177|Taranagar|178|Todabhim|179|Todaraisingh|180|Tonk|181|Udaipur|182|Udaipurwati|183|Vijainagar|184";

ALL_CITIES["IN-35"] = "Gangtok|101";

ALL_CITIES["IN-36"] = "Arakkonam|101|Arcot|102|Aruppukkottai|103|Bhavani|104|Chengalpattu|105|Chennai|226|Chinna salem|107|Coimbatore|227|Coonoor|109|Cuddalore|110|Dharmapuri|111|Dindigul|112|Erode|113|Gudalur|116|Kanchipuram|117|Karaikudi|118|Karungal|119|Karur|120|Kollankodu|121|Lalgudi|122|Madurai|123|Nagapattinam|124|Nagercoil|125|Namagiripettai|126|Namakkal|127|Nandivaram-Guduvancheri|128|Nanjikottai|129|Natham|130|Nellikuppam|131|Neyveli|132|Oddanchatram|133|P.N.Patti|134|Pacode|135|Padmanabhapuram|136|Palani|137|Palladam|138|Pallapatti|139|Pallikonda|140|Panagudi|141|Panruti|142|Paramakudi|143|Parangipettai|144|Pattukkottai|145|Perambalur|146|Peravurani|147|Periyakulam|148|Periyasemur|149|Pernampattu|150|Pollachi|151|Polur|152|Ponneri|153|Pudukkottai|154|Pudupattinam|155|Puliyankudi|156|Punjaipugalur|157|Rajapalayam|158|Ramanathapuram|159|Rameshwaram|160|Rasipuram|161|Salem|162|Sankarankoil|163|Sankari|164|Sathyamangalam|165|Sattur|166|Shenkottai|167|Sholavandan|168|Sholingur|169|Sirkali|170|Sivaganga|171|Sivagiri|172|Sivakasi|173|Srivilliputhur|174|Surandai|175|Suriyampalayam|176|Tenkasi|177|Thammampatti|178|Thanjavur|179|Tharamangalam|180|Tharangambadi|181|Theni Allinagaram|182|Thirumangalam|183|Thirunindravur|184|Thiruparappu|185|Thirupuvanam|186|Thiruthuraipoondi|187|Thiruvallur|188|Thiruvarur|189|Thoothukudi|190|Thuraiyur|191|Tindivanam|192|Tiruchendur|193|Tiruchengode|194|Tiruchirappalli|195|Tirukalukundram|196|Tirukkoyilur|197|Tirunelveli|198|Tirupathur|200|Tiruppur|201|Tiruttani|202|Tiruvannamalai|203|Tiruvethipuram|204|Tittakudi|205|Udhagamandalam|206|Udumalaipettai|207|Unnamalaikadai|208|Usilampatti|209|Uthamapalayam|210|Uthiramerur|211|Vadakkuvalliyur|212|Vadalur|213|Vadipatti|214|Valparai|215|Vandavasi|216|Vaniyambadi|217|Vedaranyam|218|Vellakoil|219|Vellore|220|Vikramasingapuram|221|Viluppuram|222|Virudhachalam|223|Virudhunagar|224|Viswanatham|225";

ALL_CITIES["IN-37"] = "Agartala|101|Badharghat|102|Dharmanagar|103|Indranagar|104|Jogendranagar|105|Kailasahar|106|Khowai|107|Pratapgarh|108|Udaipur|109";

ALL_CITIES["IN-38"] = "Achhnera|101|Adari|102|Agra|103|Aligarh|104|Allahabad|105|Amroha|106|Azamgarh|107|Bahraich|108|Ballia|109|Balrampur|110|Banda|111|Bareilly|112|Chandausi|113|Dadri|114|Deoria|115|Etawah|116|Fatehabad|117|Fatehpur|119|Greater Noida|120|Hamirpur|121|Hardoi|122|Jajmau|123|Jaunpur|124|Jhansi|125|Kalpi|126|Kanpur|127|Kota|128|Laharpur|129|Lakhimpur|130|Lal Gopalganj Nindaura|131|Lalganj|132|Lalitpur|133|Lar|134|Loni|135|Lucknow|136|Mathura|137|Meerut|138|Modinagar|139|Muradnagar|140|Nagina|141|Najibabad|142|Nakur|143|Nanpara|144|Naraura|145|Naugawan Sadat|146|Nautanwa|147|Nawabganj|148|Nehtaur|149|NOIDA|150|Noorpur|151|Obra|152|Orai|153|Padrauna|154|Palia Kalan|155|Parasi|156|Phulpur|157|Pihani|158|Pilibhit|159|Pilkhuwa|160|Powayan|161|Pukhrayan|162|Puranpur|163|Purquazi|164|Purwa|165|Rae Bareli|166|Rampur|167|Rampur Maniharan|168|Rasra|169|Rath|170|Renukoot|171|Reoti|172|Robertsganj|173|Rudauli|174|Rudrapur|175|Sadabad|176|Safipur|177|Saharanpur|178|Sahaspur|179|Sahaswan|180|Sahawar|181|Sahjanwa|182|Sambhal|183|Samdhan|184|Samthar|185|Sandi|186|Sandila|187|Sardhana|188|Seohara|189|Shahganj|190|Shahjahanpur|191|Shamli|192|Sherkot|193|Shikohabad|194|Shishgarh|195|Siana|196|Sikanderpur|197|Sikandra Rao|198|Sikandrabad|199|Sirsaganj|200|Sirsi|201|Sitapur|202|Soron|203|Suar|204|Sultanpur|205|Sumerpur|206|Tanda|208|Tetri Bazar|209|Thakurdwara|210|Thana Bhawan|211|Tilhar|212|Tirwaganj|213|Tulsipur|214|Tundla|215|Unnao|216|Utraula|217|Varanasi|218|Vrindavan|219|Warhapur|220|Zaidpur|221|Zamania|222";

ALL_CITIES["IN-39"] = "Almora|101|Bazpur|102|Chamba|103|Dehradun|104|Haldwani|105|Haridwar|106|Jaspur|107|Kashipur|108|kichha|109|Kotdwara|110|Manglaur|111|Mussoorie|112|Nagla|113|Nainital|114|Pauri|115|Pithoragarh|116|Ramnagar|117|Rishikesh|118|Roorkee|119|Rudrapur|120|Sitarganj|121|Tehri|122";

ALL_CITIES["IN-40"] = "Calcutta|101|Alipurduar|102|Arambagh|103|Asansol|104|Baharampur|105|Bally|106|Balurghat|107|Bankura|108|Barakar|109|Barasat|110|Bardhaman|111|Bidhan Nagar|112|Chinsura|113|Contai|114|Cooch Behar|115|Darjeeling|116|Durgapur|117|Haldia|118|Howrah|119|Islampur|120|Jhargram|121|Kharagpur|122|Kolkata|123|Mainaguri|124|Mal|125|Mathabhanga|126|Medinipur|127|Memari|128|Monoharpur|129|Murshidabad|130|Nabadwip|131|Naihati|132|Panchla|133|Pandua|134|Paschim Punropara|135|Purulia|136|Raghunathpur|137|Raiganj|138|Rampurhat|139|Ranaghat|140|Sainthia|141|Santipur|142|Siliguri|143|Sonamukhi|144|Srirampore|145|Suri|146|Taki|147|Tamluk|148|Tarakeswar|149";

ALL_CITIES["ID-1"] = "Bali|22|Jakarta|21|Other|999";

ALL_CITIES["IR-1"] = "Tehran|21";

ALL_CITIES["IQ-1"] = "Baghdad|1";

ALL_CITIES["IE-1"] = "Dublin|1";

ALL_CITIES["IL-1"] = "Jerusalem|2|Tel-Aviv|3";

ALL_CITIES["IT-1"] = "Rome|6";

ALL_CITIES["JM-1"] = "Kingston|99";

ALL_CITIES["JP-1"] = "Tokyo|3";

ALL_CITIES["JO-1"] = "Amman|6";

ALL_CITIES["KZ-1"] = "Astana|99";

ALL_CITIES["KE-1"] = "Nairobi|20";

ALL_CITIES["KI-1"] = "South Tarawa|99";

ALL_CITIES["KP-1"] = "Pyongyang|2";

ALL_CITIES["KR-1"] = "Seoul|2";

ALL_CITIES["KW-1"] = "Kuwait City|99";

ALL_CITIES["KG-1"] = "Bishkek|312";

ALL_CITIES["LA-1"] = "Vientiane|21";

ALL_CITIES["LV-1"] = "Riga|7";

ALL_CITIES["LB-1"] = "Beirut|1";

ALL_CITIES["LS-1"] = "Maseru|99";

ALL_CITIES["LR-1"] = "Monrovia|99";

ALL_CITIES["LY-1"] = "Tripoli|21";

ALL_CITIES["LI-1"] = "Vaduz|2";

ALL_CITIES["LT-1"] = "Vilnius|521";

ALL_CITIES["LU-1"] = "Luxembourg City|99";

ALL_CITIES["MO-1"] = "Macao|99";

ALL_CITIES["MK-1"] = "Skopje|2";

ALL_CITIES["MG-1"] = "Antananarivo|22";

ALL_CITIES["MW-1"] = "Lilongwe|99";

ALL_CITIES["MY-11"] = "Batu Pahat|101|Johor Bahru|102|Kluang|103|Kota Tinggi|104|Mersing|105|Muar|106|Pontian|107|Segamat|108|Kulaijaya|109|Ledang|110";

ALL_CITIES["MY-12"] = "Baling|101|Bandar Baharu|102|Kota Setar|103|Kuala Muda|104|Kubang Pasu|105|Kulim|106|Pulau Langkawi|107|Padang Terap|108|Pendang|109|Pokok Sena|110|Sik|111|Yan|112";

ALL_CITIES["MY-13"] = "Bachok|101|Gua Musang|102|Jeli|103|Kota Baharu|104|Kuala Krai|105|Machang|106|Pasir Mas|107|Pasir Puteh|108|Tanah Merah|109|Tumpat|110";

ALL_CITIES["MY-14"] = "Kuala Lumpur|101";

ALL_CITIES["MY-15"] = "Labuan|101";

ALL_CITIES["MY-16"] = "Alor Gajah|101|Central Malacca|102|Jasin|103";

ALL_CITIES["MY-17"] = "Jelebu|101|Jempol|102|Kuala Pilah|103|Port Dickson|104|Rembau|105|Seremban|106|Tampin|107";

ALL_CITIES["MY-18"] = "Bentong|101|Bera|102|Cameron Highlands|103|Jerantut|104|Kuantan|105|Lipis|106|Maran|107|Pekan|108|Raub|109|Rompin|110|Temerloh|111";

ALL_CITIES["MY-19"] = "Penang Island|101|Timur Laut|102|Barat Daya|103|Seberang Perai (Mainland)|104|Seberang Perai Utara|105|Seberang Perai Tengah|106|Seberang Perai Selatan|107";

ALL_CITIES["MY-20"] = "Batang Padang|101|Hilir Perak|102|Hulu Perak|103|Kerian|104|Kinta|105|Kuala Kangsar|106|Larut Matang|107|Selama|108|Manjung|109|Perak Tengah|110";

ALL_CITIES["MY-21"] = "Arau|101|Kangar|102|Padang Besar|103";

ALL_CITIES["MY-22"] = "Putrajaya|101";

ALL_CITIES["MY-23"] = "Beaufort|101|Keningau|102|Kuala Penyu|103|Nabawan|104|Sipitang|105|Tambunan|106|Tenom|107|Kota Marudu|108|Kudat|109|Pitas|110|Beluran|111|Kinabatangan|112|Sandakan|113|Tongod|114|Kunak|115|Lahad Datu|116|Semporna|117|Tawau|118|Kota Belud|119|Kota Kinabalu|120|Papar|121|Penampang|122|Putatan|123|Ranau|124|Tuaran|125";

ALL_CITIES["MY-24"] = "Betong|101|Saratok|102|Bintulu|103|Tatau|104|Belaga|105|Kapit|106|Song|107|Bau|108|Kuching|109|Lundu|110|Lawas|111|Limbang|112|Marudi|113|Miri|114|Dalat|115|Daro|116|Matu|117|Mukah|118|Asajaya|119|Samarahan|120|Serian|121|Simunjan|122|Julau|123|Meradong|124|Sarikei|125|Kanowit|126|Sibu|127|Lubok Antu|128|Sri Aman|129";

ALL_CITIES["MY-25"] = "Gombak|101|Hulu Langat|102|Hulu Selangor|103|Klang|104|Kuala Langat|105|Kuala Selangor|106|Petaling|107|Sabak Bernam|108|Sepang|109";

ALL_CITIES["MY-26"] = "Besut|101|Dungun|102|Hulu Terengganu|103|Kemaman|104|Kuala Terengganu|105|Marang|106|Setiu|107";

ALL_CITIES["MV-1"] = "Mal�|31";

ALL_CITIES["ML-1"] = "Bamako|99";

ALL_CITIES["MT-1"] = "Valletta|99";

ALL_CITIES["MH-1"] = "Majuro|247";

ALL_CITIES["MR-1"] = "Nouakchott|99";

ALL_CITIES["MU-1"] = "Port Louis|99";

ALL_CITIES["YT-1"] = "Capital|99";

ALL_CITIES["MX-1"] = "Mexico City|55";

ALL_CITIES["FM-1"] = "Palikir|99";

ALL_CITIES["MD-1"] = "Chi?in?u|22";

ALL_CITIES["MC-1"] = "Monaco|9";

ALL_CITIES["MN-1"] = "Ulan Bator|11";

ALL_CITIES["ME-1"] = "Podgorica|81";

ALL_CITIES["MS-1"] = "Plymouth |99";

ALL_CITIES["MA-1"] = "Rabat|37";

ALL_CITIES["MZ-1"] = "Maputo|21";

ALL_CITIES["MM-1"] = "Yangon|1";

ALL_CITIES["NA-1"] = "Windhoek|61";

ALL_CITIES["NR-1"] = "Yaren District|99";

ALL_CITIES["NP-1"] = "Bhaktapur|1|Bharatput|56|Biratnagar|21|Bjojpur|29|Dhangadi|91|Dhankuta|26|Dolakha|49|Gorkha|64|Jhapa|23|Kathmandu|1|Nepalgunj|81|Phidim|24|Pokhara|41|Sarlahi|46";

ALL_CITIES["NL-1"] = "Amsterdam|20";

ALL_CITIES["NC-1"] = "Capital|99";

ALL_CITIES["NZ-1"] = "Wellington|4";

ALL_CITIES["NI-1"] = "Managua|2";

ALL_CITIES["NE-1"] = "Niamey|99";

ALL_CITIES["NG-1"] = "Lagos|1";

ALL_CITIES["NU-1"] = "Alofi|1";

ALL_CITIES["NF-1"] = "Kingston|99";

ALL_CITIES["MP-1"] = "Saipan|99";

ALL_CITIES["NO-1"] = "Oslo|2";

ALL_CITIES["OM-1"] = "Muscat|24";

ALL_CITIES["PK-1"] = "Islamabad|51";

ALL_CITIES["PW-1"] = "Ngerulmud|99";

ALL_CITIES["PA-1"] = "Panama City|2";

ALL_CITIES["PG-1"] = "Port Moresby|30";

ALL_CITIES["PY-1"] = "Asunci�n|21";

ALL_CITIES["PE-1"] = "Lima|1";

ALL_CITIES["PH-11"] = "Basilan (excluding Isabela City)|101|Lanao del Sur|102|Maguindanao|103|Sulu|104|Tawi-Tawi|105";

ALL_CITIES["PH-12"] = "Albay|101|Camarines Norte|102|Camarines Sur|103|Catanduanes|104|Masbate|105|Sorsogon|106";

ALL_CITIES["PH-13"] = "Batanes|101|Cagayan|102|Isabela|103|Nueva Vizcaya|104|Quirino|105|Santiago";

ALL_CITIES["PH-14"] = "Batangas|101|Cavite|102|Laguna|103|Lucena|104|Quezon|105|Rizal|106";

ALL_CITIES["PH-15"] = "Agusan del Norte|101|Agusan del Sur|102|Butuan|103|Dinagat Islands|104|Surigao del Norte|105|Surigao del Sur|106";

ALL_CITIES["PH-16"] = "Angeles|101|Aurora|102|Bataan|103|Bulacan|104|Nueva Ecija|105|Olongapo|106|Pampanga|107|Tarlac|108|Zambales|109";

ALL_CITIES["PH-17"] = "Bohol|101|Cebu|102|Cebu City|103|Lapu-Lapu|104|Mandaue|105|Negros Oriental|106|Siquijor|107";

ALL_CITIES["PH-18"] = "Abra|101|Apayao|102|Baguio|103|Benguet|104|Ifugao|105|Kalinga|106|Mountain Province|107";

ALL_CITIES["PH-19"] = "Compostela Valley|101|Davao City|102|Davao del Norte|103|Davao del Sur|104|Davao Oriental|105";

ALL_CITIES["PH-20"] = "Biliran|101|Eastern Samar|102|Leyte|103|Northern Samar|104|Ormoc|105|Samar|106|Southern Leyte|107|Tacloban|108";

ALL_CITIES["PH-21"] = "Dagupan|101|Ilocos Norte|102|Ilocos Sur|103|La Union|104|Pangasinan|105";

ALL_CITIES["PH-22"] = "Marinduque|101|Occidental Mindoro|102|Oriental Mindoro|103|Palawan|104|Puerto Princesa|105|Romblon|106";

ALL_CITIES["PH-23"] = "Caloocan|101|Las Pi�as|102|Makati|103|Malabon|104|Mandaluyong|105|Manila|106|Marikina|107|Muntinlupa|108|Navotas|109|Para�aque|110|Pasay|111|Pasig|112|Pateros|113|Quezon City|114|San Juan|115|Taguig|116|Valenzuela|117";

ALL_CITIES["PH-24"] = "Bukidnon|101|Cagayan de Oro|102|Camiguin|103|Iligan|104|Lanao del Norte|105|Misamis Occidental|106|Misamis Oriental|107";

ALL_CITIES["PH-25"] = "Cotabato|101|Cotabato City|102|General Santos|103|Sarangani|104|South Cotabato|105|Sultan Kudarat|106";

ALL_CITIES["PH-26"] = "Aklan|101|Antique|102|Bacolod[4]|103|Capiz|104|Guimaras|105|Iloilo|106|Iloilo City|107|Negros Occidental|108";

ALL_CITIES["PH-27"] = "Isabela City|101|Zamboanga City|102|Zamboanga del Norte|103|Zamboanga del Sur|104|Zamboanga Sibugay|105";

ALL_CITIES["PN-1"] = "Adamstown|99";

ALL_CITIES["PL-1"] = "Warsaw|2";

ALL_CITIES["PT-1"] = "Lisbon|21";

ALL_CITIES["PR-1"] = "Puerto Rico|787";

ALL_CITIES["QA-1"] = "Doha|99";

ALL_CITIES["RO-1"] = "Bucharest|21";

ALL_CITIES["RU-1"] = "Moscow|495";

ALL_CITIES["RW-1"] = "Kigali|99";

ALL_CITIES["SH-1"] = "Jamestown|99";

ALL_CITIES["KN-1"] = "Basseterre|99";

ALL_CITIES["LC-1"] = "Castries|99";

ALL_CITIES["PM-1"] = "Saint-Pierre|99";

ALL_CITIES["VC-1"] = "Kingstown|99";

ALL_CITIES["WS-1"] = "Apia|99";

ALL_CITIES["SM-1"] = "San Marino|6";

ALL_CITIES["ST-1"] = "Sao Tome|99";

ALL_CITIES["SA-1"] = "Riyadh|1";

ALL_CITIES["SN-1"] = "Dakar|8";

ALL_CITIES["RS-1"] = "Belgrade|11";

ALL_CITIES["SC-1"] = "Victoria|99";

ALL_CITIES["SL-1"] = "Freetown|22";

ALL_CITIES["SI-1"] = "Ljubljana|1";

ALL_CITIES["SB-1"] = "Honiara|74";

ALL_CITIES["SO-1"] = "Mogadishu|61";

ALL_CITIES["ZA-1"] = "Pretoria|12";

ALL_CITIES["ES-1"] = "Madrid|91";

ALL_CITIES["LK-1"] = "Colombo|99";

ALL_CITIES["SD-1"] = "Khartoum|183";

ALL_CITIES["SR-1"] = "Paramaribo|4";

ALL_CITIES["SJ-1"] = "Longyearbyen|99";

ALL_CITIES["SZ-1"] = "Mbabane|404";

ALL_CITIES["SE-1"] = "Stockholm|8";

ALL_CITIES["CH-1"] = "Bern|31";

ALL_CITIES["SY-1"] = "Damascus|11";

ALL_CITIES["TJ-1"] = "Dushanbe|372";

ALL_CITIES["TZ-1"] = "Dar Es Salaam|22";

ALL_CITIES["TH-1"] = "Bangkok|2|Burirum|44|Chanthaburi|39|Chiang Mai|53|Chiang Rai|54|Chon Buri|38|Kanchanaburi|34|Lampang|54|Nakhon Ratchasima|44|Nakhon Sawan|56|Namphaengphet|55|Nong Khai|42|Pattani|73|Pattaya|38|Phitsanulok|55|Phuket|76|Ratchaburi|32|Saraburi|36|Songkhla|74|Surat Thani|77|Tak|55|Ubon Ratchathani|45";

ALL_CITIES["TL-1"] = "Dili|99";

ALL_CITIES["TK-1"] = "Nukunonu|99";

ALL_CITIES["TO-1"] = "Nuku?alofa|20";

ALL_CITIES["TT-1"] = "Port of Spain|99";

ALL_CITIES["TN-1"] = "Tunis|1";

ALL_CITIES["TR-1"] = "Ankara|312";

ALL_CITIES["TM-1"] = "Ashgabat|12";

ALL_CITIES["TC-1"] = "Cockburn Town|99";

ALL_CITIES["TV-1"] = "Funafuti|99";

ALL_CITIES["UG-1"] = "Kampala|41";

ALL_CITIES["UA-1"] = "Kiev|99";

ALL_CITIES["AE-1"] = "Abu Dhabi|2";

ALL_CITIES["GB-1"] = "Aberdeen|1224|Abingdon|1235|Aboyne|1339|Aldershot|1252|Alford (Lincs)|1507|Alloa|1259|Alton|1420|Ammanford|1269|Andover|1264|Annan|1461|Arbroath|1241|Ardrossan|1294|Arrochar|1301|Ascot|1276|Ashbourne|1335|Ashburton|1364|Ashford|1233|Axminster|1297|Aylesbury|1296|Ayr|1292|Banbury|1295|Banchory|1330|Banff|1261|Bangor (N. Wales)|1248|Barmouth|1341|Barnsley|1226|Barnstable|1271|Barrow-in-Furness|1229|Barry|1446|Basingstoke|1246|Bath|1225|Bathgate|1506|Bedford|1234|Bellingham|1434|Berwick-on-Tweed|1289|Bewdley|1299|Bideford|1237|Birmingham|121|Bishop Auckland|1388|Bishops Stortford|1279|Blackburn|1254|Blackpool|1253|Blairgowrie|1250|Blandford|1258|Bodmin|1208|Bolton|1204|Boroughbridge|1423|Boston|1205|Bournemouth|1202|Bourton-on-the-Water|1451|Bracknell|1344|Bradford|1274|Braintree|1376|Brechin|1356|Brentwood|1277|Bridgwater|1278|Bridlington|1262|Bridport|1308|Brighton|1273|Bristol| 1275|Broadford|1471|Brooke|1508|Buckingham|1280|Bude|1288|BudleighSalterton|1395|Burley|1425|Burnley|1282|Burntwood|1543|Burton-on-Trent|1283|Bury-St-Edmunds|1284|Buxton|1298|Caernarvon|1286|Cambridge|1223|Canterbury|1227|Cardiff|29|Cardigan|1239|Carlisle|1228|Carmarthen|1267|Castle Douglas|1556|Cerne Abbas|1300|Chard|1460|Chelmsford|1245|Cheltenham|1242|Chester|1244|Chesterfield|1246|Chichester|1243|Chippenham|1249|Cirencester|1285|Clacton-on-Sea|1255|Clitheroe|1200|Clynderwen|1437|Coalville|1530|Coatbridge|1236|Colchester|1206|Colwyn Bay|1492|Congleton|1477|Consett|1207|Coppull|1257|Corwen|1490|Coventry|24|Craigellachie|1340|Crediton|1363|Crewe|1270|Cromer|1263|Cumnock|1290|Darlington|1325|Derby|1332|Dereham|1362|Devizes|1380|Dingwall|1349|Diss|1379|Docking|1485|Doddington|1354|Doncaster|1302|Dorchester|1305|Dover|1304|Downham Market|1366|Driffield|1377|Dulverton|1398|Dumbarton|1389|Dumfries|1387|Dunbar|1368|Dundee|1382|Dunfermline|1383|Dunkeld|1350|Dunoon|1369|Duns|1361|Durham| 191|Dursley|1453|Easingwold|1347|East Grinstead|1342|East Kilbride|1357|Eastbourne|1323|Edinbane|1470|Edinburgh|131|Elgin|1343|Ellon|1358|Ely|1353|Esher|1372|Exeter|1392|Fakenham|1328|Falkirk|1324|Falmouth|1326|Fareham|1489|Faringdon|1367|Fishguard|1348|Folkestone|1303|Fordoun|1561|Forfar|1307|Forres|1309|Fort Augustus|1320|Fort William|1397|Fortrose|1381|Fraserburgh|1346|Frome|1373|Gainsborough|1427|Gairloch|1445|Girvan|1465|Glasgow|141|Glastonbury|1458|Glenurquhart|1456|Glossop|1457|Gloucester|1452|Golspie|1408|Goole|1405|Grantham|1476|Grantown-on-Spey|1479|Gravesend|1474|Great Dunmow|1371|Great Shefford|1488|Great Yarmouth|1493|Greenock|1475|Grimsby|1472|Guildford|1483|Guisborough|1287|Halifax|1422|Harthill|1501|Hartlepool|1429|Haslemere|1428|Hastings|1424|Hathersage|1433|Haverhill|1440|Hawick|1450|Hay-on-Wye|1497|Haywards Heath|1444|Heathfield|1435|Helensburgh|1436|Helmsdale|1431|Helmsley|1439|Hereford|1432|High Wycombe|1494|Hinckley|1455|Hitchin|1462|Holbeach|1406|Holsworthy|1409|Holyhead|1407|Honington|1400|Honiton|1404|Horsham|1403|Huddersfield|1484|Hull|1482|Huntingdon|1480|Huntly|1466|Insch|1464|Inveraray|1499|Inverness|1463|Inverurie|1467|Ipswich|1473|Johnstone|1505|Keighley|1535|Keith|1542|Kelso|1573|Kendal|1539|Kettering|1538|Killearn|1360|Killin|1567|Killingholme|1469|Kilmarnock|1563|Kings Lynn|1553|Kingsbridge|1548|Kington|1544|Kingussie|1540|Kinross|1577|Kirkcudbright|1557|Kirriemuir|1575|Knebworth|1438|Knighton|1547|Knutsford|1565|Ladybank|1337|Laggan|1528|Lairg|1549|Lampeter|1570|Lanark|1555|Lancaster|1524|Lapworth|1564|Lauder|1578|Launceston|1566|Leeds|113|Leicester|116|Leighton Buzzard|1525|Leominster|1568|Lincoln|1522|Liverpool|151|Llanarth|1545|Llandeilo|1558|Llandovery|1550|Llandyssul|1559|Llanelli|1554|Lochcarron|1520|Lochgilphead|1546|Lochinver|1571|Lockerbie|1576|London|20|Looe|1503|Loughborough|1509|Lowestoft|1502|Manchester|161|Market Weighton|1430|Markyate|1442|Martin|1526|Mold|1352|Moscow|1560|Nettlebed|1491|Newdigate|1306|Northern Ireland|28|Nottingham|115|Oakham|1572|Pakenham|1359|Peat Inn|1334|Pontypool|1495|Pontypridd|1443|Port Ellen|1496|Portree|1478|Portsmouth|23|Rangeworthy|1454|Reading|118|Redditch|1527|Redruth|1209|Sheffield|114|Shirenewton|1291|Shottisham|1394|Sleaford|1529|Stanford-le-Hope|1275|Stonehaven|1569|Stourbridge|1562|Stowmarket|1449|Swanley|1322|Warboys|1487|Weedon|1327";

ALL_CITIES["UY-1"] = "Montevideo|2";

ALL_CITIES["UZ-1"] = "Tashkent|71";

ALL_CITIES["VU-1"] = "Port Vila|22";

ALL_CITIES["VN-1"] = "An Giang|77|Ba Ria|64|Bac Giang|40|Bac Kan|281|Bac Lieu|781|Bac Ninh|241|Ben Tre|62|Binh Dinh|56|Binh Duong|65|Binh Phuoc|651|Binh Thuan|62|Ca Mau|780|Can Tho|71|Cao Bang|26|Cuu Long|70|Da Nang|51|Dak Lak|50|Dong Nai|61|Dong Thap|67|Gia Lai|59|Ha Giang|19|Ha Nam|351|Ha Tay|34|Ha Tinh|39|Hai Duong|320|Hai Phong|31|Hanoi|4|Ho Chi Minh City|8|Hoa Binh|18|Hung Yen|321|Khanh Hoa|58|Kien Giang|77|Kon Tum|60|Lai Chau|23|Lang Son|25|Lao Cai|20|Long An|72|Long Xuyen|76|Minh Hai|78|Nam Dinh|350|Nghe An|38|Nin Thuan|68|Ninh Binh|30|Phu Tho|210|Phu Yen|57|Quang Binh|52|Quang Nam|510|Quang Ngai|55|Quang Tri|53|Quant Ninh|33|Soc Trang|79|Son La|22|Tay Ninh|66|Thai Binh|36|Thai Nguyen|280|Thanh Hoa|37|Thua Thien Hue|54|Thuan Lam Dong|63|Tien Giang|73|Tra Vinh|74|Tuyen Quang|27|Vinh Long|70|Vinh Phu|211|Vung Tau|64|Yen Bai|29";

ALL_CITIES["WF-1"] = "MataUtu|99";

ALL_CITIES["EH-1"] = "Capial|99";

ALL_CITIES["YE-1"] = "Sanaa|1";

ALL_CITIES["ZM-1"] = "Lusaka|1";

ALL_CITIES["ZW-1"] = "Harare|4";

var ALL_AREAS_DEFAULT = "East|11|South|12|West|13|North|14|Central|15";

var ALL_AREAS = new Array();

ALL_AREAS["IN-17-102"] = "Alwar|31|Chanakya Puri|32|Civil Lines|33|Connaught Place|34|Daryaganj|35|Defence Colony|36|Delhi Cantonment|37|Dwarka|38|Faridabad |39|Gandhi Nagar|40|Ghaziabad|41|Greater Noida|42|Gurgaon|43|Hauz Khas|44|Kalkaji|45|Kanjhawala|46|Karol Bagh|47|Kotwali|48|Model Town|49|Najafgarh|50|Narela|51|Noida|52|Pahar Ganj|53|Panipat |54|Parliament Street|55|Patel Nagar|56|Preet Vihar|57|Punjabi Bagh|58|Rajouri Garden|59|Sadar Bazaar|60|Saket|61|Saraswati Vihar|62|Seelampur|63|Seema Puri|64|Shahdara|65|Sonipat |66|Vasant Vihar|67|Vivek Vihar|68|Other|69";

ALL_AREAS["IN-40-123"] = "Alipore Sadar|31|Alipurduar|32|Arambagh|33|Asansol|34|Balurghat|35|Bangaon|36|Barasat Sadar|37|Bardhaman Sadar North|38|Bardhaman Sadar South|39|Barhampur|40|Barrackpore|41|Baruipur|42|Basirhat|43|Bidhannagar|44|Bolpur|45|Canning|46|Chanchal|47|Chandannagore|48|Chinsurah|49|Contai|50|Cooch Behar Sadar|51|Darjeeling Sadar|52|Diamond Harbour|53|Dinhata|54|Domkol|55|Durgapur|56|Egra|57|Gangarampur|58|Haldia|59|Howrah Sadar|60|Islampur|61|Jalpaiguri Sadar|62|Jangipur|63|Kakdwip|64|Kalimpong|65|Kalna|66|Kalyani|67|Kandi|68|Katwa|69|Krishnanagar Sadar|70|Kurseong|71|Lalbag|72|Malbazar|73|Malda Sadar|74|Mathabhanga|75|Mekhliganj|76|Purulia Sadar East|77|Purulia Sadar West|78|Raghunathpur|79|Raiganj|80|Rampurhat|81|Ranaghat|82|Siliguri|83|Srirampore|84|Suri Sadar|85|Tamluk|86|Tehatta|87|Tufanganj|88|Uluberia|89|Other|90";

ALL_AREAS["IN-19-180"] = "Ambawadi |31|Asarwa|32|Ashram Road|33|Astodia|34|Bapu Nagar|35|Bopal|36|C.G. Road|37|Dani Limbada|38|Dariapur|39|Ellis Bridge|40|Ghatlodia|41|Gita Mandir Road|42|Gulbai Tekra|43|Jamalpur|44|Kadia|45|Kalupur|46|Kankaria|47|Khokra Mehmedabad|48|Lal Darwaza|49|Maninagar|50|Meghani Nagar|51|Mem Nagar|52|Mithakhali Six Roads|53|Naranpura|54|Naroda|55|Narol|56|Navrangpura|57|Odhav|58|Paldi|59|Prahlad Nagar|60|Raipur|61|Ranip|62|S.G. Road|63|Sabarmati|64|Sanand|65|Sarangpur Darwaza |66|Saraspur|67|Satellite Area|68|Shahibaug|69|Shahpur|70|University Area|71|Vadaj|72|Vasana|73|Vastrapur|74|Other|75";

ALL_AREAS["IN-12-135"] = "Hyderabad|31|Mushirabad|32|Prakasham Nagar |33|Punjagutta|34|Afzal Gunj|35|Dilsukhnagar|36|Keshavagiri |37|Lal Darwaza |38|NTR Nagar |39|Umda Bazar |40|Amberpet |41|Habsiguda |42|Jamia Osmania|43|Tarnaka |44";

ALL_AREAS["IN-27-136"] = "Andheri |31|Bandra |32|Borivali|33|Chembur|34|Colaba|35|Dadar|36|Dahisar|37|Fort Area|38|Ghatkopar |39|Goregaon|40|Juhu|41|Kalbadevi|42|Kalyan|43|Kurla |44|Malad|45|Mulund |46|Nariman Point|47|Navi Mumbai |48|Parel|49|Powai |50|Raigarh|51|Tardeo|52|Thane|53|Worli |54|Other|55";

</script>

<script type="text/javascript" src="/js/eatads/eatads.js"></script>
<script type="text/javascript">

    function onchange_countries(opt)
    {
        console.log("Boo 501: onchange_countries called..");
        if (opt.selectedIndex > 0)
        {
//            var targetAddress = geoXML3.nodeValue(opt[opt.selectedIndex]);
////            var targetAddress = "hangzhou, china";
//            updateMap(targetAddress);
            load_states("0");
            init_cities();
            init_areas();
        }
        else
        {
            alert("Please pick a country");
        }
    }

    function onchange_states(opt)
    {
        if (opt.selectedIndex > 0)
        {
            if(opt.value > 0)
            {
                if(opt.value > 10)
                {
                    //real states
//                    var targetAddress = geoXML3.nodeValue(opt[opt.selectedIndex]);
//                    updateMap(targetAddress);
                }

                load_cities("0");
                init_areas();
            }
            else
            {
                init_cities();
                init_areas();
            }
        }
        else
        {
            alert("Please pick a state");
        }
    }


    function onchange_cities(opt)
    {
        console.log("Boo 601: onchange_cities called..");
        if (opt.selectedIndex > 0)
        {
            if(opt.value > 0)
            {
//                var targetAddress = geoXML3.nodeValue(opt[opt.selectedIndex]);
//                updateMap(targetAddress);
                load_areas("0");
            }
            else
            {
                init_areas();
            }
        }
        else
        {
            alert("Please pick a city");
        }
    }

    function onchange_areas(opt)
    {
        console.log("Boo 701: onchange_areas called..");

    }
</script>