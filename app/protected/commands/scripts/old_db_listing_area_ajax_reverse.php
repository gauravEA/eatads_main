<?php 
	mysql_connect('localhost', 'root', '') or die("Couldn't create connection."); 	// create db connection
	mysql_select_db('eatadslive_24_march_2014') or die("Couldn't fetch database.");			// select db

    function createAlias($str) {
        $str = trim($str);
        $junkChar = array("'", "&", ",", "$", "+", ",", "/", ":", "=", "?", "@", ".", "(", ")", "[", "]", "*", "&nbsp;", "&amp;", "%A0");
        //$str = str_replace($junkChar, "", $str); // replace special character
        $str = str_replace($junkChar, "", html_entity_decode($str)); // replace special character
        $str = str_replace(" ", "-", $str); // replace space with hyphen
        $str = str_replace("--", "-", $str); // replace double hyphen with single hyphen      
        return strtolower($str);
    }

	function updateArea($areaName, $areaType, $parentId=null, $shortCode=null)
	{
		// check if area with type exists
		$query = mysql_query("SELECT `id` FROM `Area` WHERE name='".str_replace("'", "''", $areaName)."' AND type='$areaType'");
		$result = mysql_num_rows($query);
		if($result) {
			// if exists, then get id
			$result = mysql_fetch_row($query);
			return $result[0];			
		} else {
			// if not then add record and get id
			$areaName = ucwords(strtolower(trim($areaName)));
			$areaType = strtolower(trim($areaType));
			
                        $alias = createAlias($areaName);
                        
			// insert new record
			if($parentId==null) {
				// only happens in case of Country
				// get the autoincrement id
				$result = mysql_query("SHOW TABLE STATUS LIKE 'Area'");
				$row = mysql_fetch_array($result);
				$parentId = $row['Auto_increment'];
				$query = "INSERT INTO `Area` (`id`, `name`, `alias`, `type`, `short_code`, `parentid`) 
					VALUES ('$parentId', '".str_replace("'", "''", $areaName)."', '$alias', '$areaType', '$shortCode', '$parentId')";
			} else {
				$query = "INSERT INTO `Area` (`name`, `alias`, `type`, `short_code`, `parentid`) 
					VALUES ('".str_replace("'", "''", $areaName)."', '$alias', '$areaType', '$shortCode', '$parentId')";
			}			
			mysql_query($query);
			return mysql_insert_id();
		}
		
		// new_countryid, new_stateid, new_cityid, new_status
		// 0-default, 1-update, 2-not available
		
	
	
	}
	
	if(isset($_GET['id'])) {
	
            $id = $_GET['id'];
            $country = strtolower($_GET['country']);
            $state = strtolower($_GET['state']);
            $city = strtolower($_GET['city']);
            $countryCode = substr(strtoupper($_GET['country_short']), 0, 2);
            $lat = $_GET['geo_lat'];
            $lng = $_GET['geo_lng'];

            // use these variable to update same city and location
            $gcity = $_GET['gcity'];
            $glocation = $_GET['glocation'];
            $gcountry = $_GET['gcountry'];
            
            
            if (empty($state)) {
                $state = $country;
            } elseif (empty($city)) {
                $city = $state;
            }

	
		// header('Content-type: application/json');
		if(is_numeric($id)) {
                        // if any value is missing in country, state and city
			if( empty($country) || empty($state) || empty($city) ) {
				// update record area_status = 2
				mysql_query("UPDATE `eatads_listing` SET area_status=2 WHERE id='$id'");
				echo json_encode(array('status'=>2));
				
			} else {
				// check if country exists
				$countryId = updateArea($country, 'c', null, $countryCode);
				// check if state exists
				$stateId = updateArea($state, 's', $countryId);
				// check if city exists
				$cityId = updateArea($city, 'ci', $stateId);
								
				
				// update the country, state and city id for the record
				$query = "UPDATE `eatads_listing` SET `area_status`=1, new_lat='$lat', new_lng='$lng', new_countryid='$countryId', new_stateid='$stateId', new_cityid='$cityId'
							WHERE id='$id'";
				mysql_query($query);
                                
                                // Moved code approx start
                                $lagLngSql = "SELECT new_countryid, new_stateid, new_cityid, new_lat, new_lng  FROM eatads_listing WHERE countryname='$gcountry' AND cityname='$gcity' AND location='$glocation' AND area_status=1 LIMIT 1";
                                $lngLatquery = mysql_query($lagLngSql);
                                
                                // if this lan lat is already updated then update all record with this lat lng in database and skip checking process.
                                $rowLatLng = mysql_num_rows($lngLatquery);
                                
                                if($rowLatLng) {
                                    $resultLatLng = mysql_fetch_array($lngLatquery);
                                    $countryIdLatLng = $resultLatLng['new_countryid'];
                                    $stateIdLatLng = $resultLatLng['new_stateid'];
                                    $cityIdLatLng = $resultLatLng['new_cityid'];
                                    $newLat = $resultLatLng['new_lat'];
                                    $newLng = $resultLatLng['new_lng'];

                                    $lagLngSql = "UPDATE `eatads_listing` SET new_lat = $newLat, new_lng = $newLng, new_countryid = $countryIdLatLng, new_stateid = $stateIdLatLng, new_cityid = $cityIdLatLng, area_status = 1 WHERE countryname='$gcountry' AND cityname='$gcity' AND location='$glocation' AND area_status=0";
                                    $lngLatquery = mysql_query($lagLngSql);
                                }
                                
                                // Moved code approx end
                                
				$area = array('c' => array(ucwords($country), $countryId),
						's' => array(ucwords($state), $stateId),
						'ci' => array(ucwords($city), $cityId));
				
				echo json_encode($area);
                            
			}
		} else {			
			// id null
			echo json_encode(array('status'=>0));
		}		
	
	}
	
	// SELECT * FROM eatads_listing WHERE new_status =1 OR new_status =2
	// update eatads_listing SET new_status=0, new_countryid=0, new_stateid=0, new_cityid=0
	
	
	