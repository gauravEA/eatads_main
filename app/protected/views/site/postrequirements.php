 <!-- POST REQUIREMENTS MODAL -->
    <?php
        $form=$this->beginWidget('CActiveForm', array(
                'id'=>'post-req-form',
                //'enableAjaxValidation'=> true,
                //'action'=> Yii::app()->urlManager->createUrl('ajax/postrequirements'),
                'enableClientValidation'=>true,//false,//true,
                'clientOptions'=>array(
                    'validateOnSubmit'=>true, 
                    'validateOnType'=>false,
                    //'afterValidate'=>'js:$.yii.fix.ajaxSubmit.afterValidate',
                ),
        ));        
    ?>
    
        <div class="modal fade left-aligned req" id="RegisterModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-body">
                        <h2 class="modal-title">Tell us your Campaign requirements</h2>
                        <ul>
                            <li>Please provide as much detail as possible so that we may source relevant assistance from our media partners. When done click Submit. </li>
                        </ul>
                        <div class="bodytalk">
                            <div class="row">
                                <div class="col-sm-6 ">
                                    <label class="top">Your Name </label> 
                                    <!--<input type="text" class="form-control" id="username" name="username" />-->
                                    <?php echo $form->textField($contactModel, 'name', array('class' => 'form-control')); ?>
                                    <?php echo $form->error($contactModel, 'name', array('class' => 'errormessage')); ?>
                                </div>
                                <div class="col-sm-6 ">
                                    <label class="top">Your Email</label> 
                                    <!--<input type="text" class="form-control" id="email" name="email" />-->
                                    <?php echo $form->textField($contactModel, 'email', array('class' => 'form-control')); ?>
                                    <?php echo $form->error($contactModel, 'email', array('class' => 'errormessage')); ?>
                                </div>
                                <div class="col-sm-6 ">
                                    <label class="top">Your Phone</label> 
                                    <!--<input type="text" class="form-control" id="phone" name="phone" />-->
                                    <?php echo $form->textField($contactModel, 'phone', array('class' => 'form-control')); ?>
                                    <?php echo $form->error($contactModel, 'phone', array('class' => 'errormessage')); ?>
                                </div>
                                <div class="col-sm-12 clearfix">
                                    <label class="top">Your campaign objectives & media requirements</label> 
                                    <!--<textarea class="form-control col-sm-8" placeholder=""></textarea>-->
                                    <?php echo $form->textArea($contactModel, 'message', array( 'class' => 'form-control col-sm-8')); ?>
                                    <?php echo $form->error($contactModel, 'message', array('class' => 'errormessage')); ?>
                                </div>
                                <!--<button class="btn btn-lg btn-success">Submit</button>-->
                                <img src="<?php echo Yii::app()->baseUrl.'/images/site/ajax-loader.gif'; ?>" alt="ajax_loader" id="pr_ajax_loader" style="display:none;"/>
                                <?php echo CHtml::ajaxSubmitButton('Submit', 
                                            CHtml::normalizeUrl(array('ajax/postrequirements')),
                                            array(
                                                'beforeSend'=>'function(){                                                    
                                                    $("#pr_ajax_loader").show();
                                                    //$("#update_info").replaceWith("<p id=\"update_info\">sending...</p>");
                                                }',
                                                'success'=>'function(data){                                                    
                                                    $("#pr_ajax_loader").hide();                                                   
                                                    // if(data.status=="success"){
                                                    if(data=="success") {                                                        
                                                        $("#req_submit").append("form submitted successfully.");
                                                        // $("#user-form")[0].reset();
                                                        $("#pr_ajax_loader").hide();
                                                        $("#RegisterModal").modal("hide");
                                                        $("#callback_post_req").modal("show");
                                                        $("#Contact_name").val("");
                                                        $("#Contact_email").val("");
                                                        $("#Contact_phone").val("");
                                                        $("#Contact_message").val("");
                                                    } else {
                                                        $.each(JSON.parse(data), function(key, val) {                                                            
                                                            $("#"+key+"_em_").text(val);
                                                            $("#"+key+"_em_").show();
                                                        });                                                        
                                                        $("#pr_ajax_loader").hide();
                                                        $("#RegisterModal").modal("show");
                                                        $("#callback_post_req").modal("hide");                                                        
                                                    }
                                                }',
                                            ),
                                            array('id'=>'req_submit', 'class' => 'btn btn-lg btn-success big')); ?>                                
                            </div>
                        </div>
                        <h6>For queries please email <a href="mailto:support@eatads.com">support@eatads.com</a>.</h6>

                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
        <div class="modal left-aligned" id="callback_post_req" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-body">
                        <h4 class="modal-title">Thank you for your submission. We will get back to you shortly.</h4>

                        <button class="btn btn-lg btn-success" id="pr_success_ok">OK</button>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
    <?php $this->endWidget(); ?>
    <script type="text/javascript">
        $(function(){
            $('.btn-cta').click(function() {
                console.log('btn-cta click');
                $('#RegisterModal').modal('show');
            });
            $('#pr_success_ok').click(function(e) {
                e.preventDefault();
                $('#callback_post_req').modal('hide');
            });
            
        });
    </script>
    
    <!-- POST REQUIREMENTS MODAL -->  
   