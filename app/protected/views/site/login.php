<?php
/* @var $this AccountController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<h1>Login</h1>

<p>Please fill out the following form with your login credentials:</p>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Login with your existing account.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>	
	</div>

	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Login'); ?>
	</div>      
    
    <div class="row">
        <?php echo CHtml::link('Forgotten Password',array('account/forgot'),array('class'=>'btn_signup'));?>
    </div>
    <br />
    <p class="note">Or create a new one. It's free.</p>
    <div class="row">
        <?php echo CHtml::link('Forgotten Password',array('account/forgot'),array('class'=>'btn_signup'));?>
    </div>
    <h3>hi</h3>
    <?php echo CHtml::link('Google', array('account/login?provider=google'), array('class'=>'btn_social')); ?>
    <?php echo CHtml::link('LinkedIn', array('account/login?provider=linkedin'), array('class'=>'btn_social')); ?>
        
<?php $this->endWidget(); ?>
</div><!-- form -->