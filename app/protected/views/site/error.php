<?php
/* @var $this SiteController */
/* @var $error array */
if($code == "404") {
    $this->pageTitle = Yii::app()->name . ' - Page not found';
    $this->breadcrumbs = array(
        'Home' => JoyUtilities::getHomeUrl(),
        'Page not found',
    );
} else {
   $this->pageTitle = Yii::app()->name . ' - Error';
    $this->breadcrumbs = array(
        'Home' => JoyUtilities::getHomeUrl(),
        'Error',
    ); 
}
?>

<div class="navbar navbar-second navbar-static-top home-bar ">
    <div class="container <?php if($code == "403" && $message == "Activate your account - Your activation link has been sent on mail. Please check your mail.") { ?> noBg <?php } ?>">
        <div class="left normal errorcode">
            <h1>
                <?php
                    if($code == "404") {
                        // echo CHtml::encode($message);
                        echo CHtml::encode('The page you are looking for could not be found.');
                    } else {
                        // echo CHtml::encode($message);
                        echo CHtml::encode('The page you are looking for could not be found.');
                    }
                ?>
            </h1>
            <p>
                <?php if(!Yii::app()->user->isGuest) { ?>
                    <a href="<?php echo JoyUtilities::getDashboardUrl(); ?>">Dashboard</a><br/>
                <?php } else { ?>
                    <a href="<?php echo JoyUtilities::getHomeUrl(); ?>">Home Page</a><br/>
                <?php } ?>
            </p>
        </div>
    </div>
</div>
<div class="container main-body">
    <div class="row">
        <?php //echo CHtml::encode($message); ?>
    </div>
</div>
