<script>
$(document).ready(function() {
     console.log('we are ready');
     $('#homepage-overlay').modal('show');
});
</script>

<div>
     <div class="modal fade" id="homepage-overlay">
       <div class="modal-dialog">
         <div class="modal-content">
           <div class="modal-header">
             <h4 class="modal-title">
             	<img width="140" src="<?php echo Yii::app()->getBaseUrl(); ?>/images/site/logo.svg" alt="logo">
				<span>Simplifying OOH</span>
             </h4>
           </div>
           <div class="modal-body">
               <div class="modal-body-wrapper">
				  <div class="main-msg-wrapper">
	                  <p class="main-msg-big">Welcome to EatAds. We’re on a roll!</p>
					  <p class="main-msg-small">Our beta phase is nearing completion, and we’re busy preparing for the launch of two great products for the OOH media industry (available globally).</p> 
					  <p class="main-msg-small">We’re securing pre-launch partnerships now.</p>
			  	</div>
                  <p class="secondary-msg">Specialist OOH vendors and agencies, please  <a href="mailto:sales@eatads.com">contact us</a> for advanced details. </p>
                  <p class="other-msg">For now, <span><a href="#" data-dismiss="modal">continue to view our beta site. <span class="glyphicon glyphicon-chevron-right"</span></span></a></p>
               </div>
           </div>
         </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->
</div>
<div class="navbar navbar-second navbar-static-top home-bar ">

    <div class="container ">
        <?php //$this->widget('CreateAccountButton'); ?>
        <?php $this->widget('AdvanceSearch', array('renderBackground' => true, 'ipCurrencyCode' => $ipCurrencyCode)); ?>       
    </div>
</div>
<div class="container main-body" id="home">
    <?php echo $this->renderPartial('postrequirements', array('contactModel'=>$contactModel)); ?>
    <div class="row">
        <div class="col-sm-3 right">
            <h2 id="browse_country">Browse Countries</h2>
            <!-- Slider code -->
                <!--<span id="slider_counter" style="display:none;">1</span>
                <a id="prev_link" href="#browse_country" style="display:none;" ><< </a> <a id="next_link" href="#browse_country">>></a><br />-->
            <div class="slider_container">
                <?php echo $this->renderPartial('country', array('model' => $model, 'count' => 0, 'i' => 1, 'countries_per_slide' => Yii::app()->params['countries_per_slide'])); ?>
            </div>
        </div>
        <div class="col-sm-9 left">
            <h2>About Us</h2> 
            <p>EatAds is the leading platform for Outdoor & Out-of-Home advertising space. Our platform & mobile solutions help you manage your OOH sites, simplify & grow your business.</p>
            <p>Grow your business with the latest web & mobile technologies for information sharing & better decision-making. Whether it is preparing and presenting your inventory information in our simplified map-view, or managing your availability via our partner software providers, EatAds will help you increase your reach amongst media buyers.</p>
            <p>We take no commission and it's easy for vendors to join. Media vendors <a href="mailto:support@eatads.com">contact us</a> today.</p><br>
            <p><a href="<?php echo Yii::app()->urlManager->createUrl('staticPages/aboutUs'); ?>">Learn more</a></p>
        </div>

    </div>   
</div>

<script type="text/javascript">
    /*$(function(){              
    
     // to slide to prev group of countries
     $('#prev_link').click(function(){
     curr_counter = parseInt(get_curr_counter());
    
     $('.slide'+curr_counter).fadeOut( "slow", function() {
     prev_counter = curr_counter-1;
     $('#slider_counter').html(prev_counter);
     $('.slide'+prev_counter).fadeIn("slow");
     $('#next_link').show();                        
     });
     prev_prev_counter = curr_counter-2;
     if(!$('.slide'+(prev_prev_counter))[0]) {
     $('#prev_link').fadeOut("slow");                   
     }
     });
    
     // to slide to next group of countries
     $('#next_link').click(function(){
     curr_counter = parseInt(get_curr_counter());
    
     $('.slide'+curr_counter).fadeOut( "slow", function() {
     // Animation complete
     next_counter = curr_counter+1;                                      
     $('.slide'+next_counter).fadeIn("slow");
     $('#slider_counter').html(next_counter);
     $('#prev_link').show();
    
     });
     next_next_counter = curr_counter+2;               
     if(!$('.slide'+(next_next_counter))[0]) {                          
     $('#next_link').hide("slow");                        
     }
     // $('.slide'+curr_counter).animate({opacity:'0.0'}, 1000, function(){
     // $(this).hide();                         
     // });
    
     });
     });
     function get_curr_counter()
     {
     curr_counter = $('#slider_counter').html();              
     return curr_counter;
     }*/
</script>