<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Monitorly</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
<!--    <?php // if ($this->beginCache('aboutUs', array('duration' => $this->cacheTime))) { ?>    -->
        <?php
            $theme = Yii::app()->theme;
            $cs = Yii::app()->clientScript;
            
            //echo $theme->getBaseUrl() . 'sdfdffsdfsdsd';
            $cs->registerCssFile( $theme->getBaseUrl() . '/css/bootstrap.min.css' );
            $cs->registerCssFile( $theme->getBaseUrl() . '/css/bootstrap-theme.min.css' );
            $cs->registerCssFile( $theme->getBaseUrl() . '/css/main_monitorly.css' );
            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js', CClientScript::POS_BEGIN);
            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/main_monitorly.js', CClientScript::POS_BEGIN);
            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/vendor/bootstrap.min.js', CClientScript::POS_BEGIN);
//            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/vendor/jquery-1.11.0.min.js', CClientScript::POS_BEGIN);
        ?>    
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Delius' rel='stylesheet' type='text/css'>
        
        
        <!-- Google Tag Manager - Initiate DataLayer variable -->
<script>
dataLayer = [];

$(function() {
   $('#btn-signup').click(function() {
        //console.log('sdfsdfsdfsdf');
        //    e.preventDefault();
        
    $.ajax({
            type: 'POST',
             url: '<?php echo Yii::app()->urlManager->createUrl('ajax/signup/'); ?>',
            data:{'email': $('#email').val(),
                  'phone': $('#code').val() + '-' + $('#phonenumber').val(),
                   'pid':2 },
         success:function(data){
             console.log(data + " data being passed");
                if (data == 1) {
                    $('.error-message').hide();
                    $('.signup-form').hide().delay(7000).fadeIn("slow");
                    $('#thank-you').show().delay(5000).fadeOut("slow");
                    //$('#thank-you').style.display='inline-block';
                    //console.log($('#email').val());
                    $('#email').val('');
                    $('#phonenumber').val('');
                    
//                    $('.signup-form').show().delay('500');
                    
                    
//                    $('#thank-you').fadeOut(5000).done(function(){
//                        alert("Now all '.hotel_photo_select are hidden'");
//                    });
//                    $.when($('#thank-you').fadeOut(500))
//                               .done(function() {
//                        alert("Now all '.hotel_photo_select are hidden'");
//                $('.signup-form').show();
//                    });
//                    $('.signup-form').replaceWith($('.thank-you'));
  //                  $('#thank-you').show();
                    
//                    $('.signup-form').style.display='none';
                } if (data == 0) { 
                    $('.error-message').text('Invalid Email');
                    $('.error-message').show().delay(4000).fadeOut("slow");
                }                 
                 else if (data != 0 && data != 1) {
                    $('.error-message').show().delay(4000).fadeOut("slow");
                }     
            },
            error: function(data) { // if error occured
                  alert("Error occured.please try again");
                  alert(data);
             }
           });
}); 
});
</script>  
    </head>
    
    
    
    <body>
        
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#"><img src="<?php echo Yii::app()->theme->getBaseUrl(); ?>/img/monitorly/logo.png" class="eatads-logo"></a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Login</a></li>
        </ul>
      </div>
    </div>

    <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="container" id="big-banner">
        <div class="hero-lines">
            <h1>Everything You Need to Monitor Outdoor. 
With Peace of Mind.</h1>
            <p>With Monitorily your field staff can collect time critical data and photos of media, you can plan and manage assignments of your field staff, and your clients can directly see proof of media without you taking the trouble of creating ppts. All that with automated analytics on top.
</p>
        </div>
        <div class="signup-form form-inline">
<!--            <form class="form-inline" role="form">-->
                <div class="form-group">
                  <input type="text" id="email" placeholder="cheryl@company.com" class="form-control">
                </div>
                <div class="form-group">
                    <input type="text" id="code" placeholder="+91" class="form-control country-code" value="+91">
                </div>
                <div class="form-group">
                  <input id="phonenumber" type="tel" placeholder="optional" class="form-control" maxlength="10">
                </div>
                <button type="submit" class="btn btn-custom" id="btn-signup">Sign Up. It's Free.</button>
<!--            </form>-->
        </div>
          <div class="thank-you" id="thank-you" style="display: none;">
            <span><img src="<?php echo Yii::app()->theme->getBaseUrl(); ?>/img/check.png">&nbsp; Thank you for signing-up with us. Please check your mailbox to activate your account.</span>
        </div>
        <div class="error-message" style="display: none;">Email already exist with us.</div>
      </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row promises">
        <div class="col-md-3 col-sm-3 divider-vertical">
          <h2>Monitoring Made Easy</h2>
          <img src="<?php echo Yii::app()->theme->getBaseUrl(); ?>/img/monitorly/promise-icon-1.png">
          <p>Monitoring sites has never been easier with our simple to use Android App and Web dashboard. No more cumbersome digital cameras.</p>
        </div>
        <div class="col-md-3 col-sm-3 divider-vertical">
          <h2>User Assignment</h2>
          <img src="<?php echo Yii::app()->theme->getBaseUrl(); ?>/img/monitorly/promise-icon-2.png">
          <p>Organize your field staff to get only the sites they need to monitor with our intuitive User Assignment feature on the Web Dashboard. No more confusion on who is responsible for which site.</p>
       </div>
        <div class="col-md-3 col-sm-3 divider-vertical">
          <h2>Instant Alerts</h2>
          <img src="<?php echo Yii::app()->theme->getBaseUrl(); ?>/img/monitorly/promise-icon-3.png">
          <p>Get instant alerts via Email and SMS when problems are reported. Instant alerts now alerts the relevant party when sites have problems so that they can be rectified swiftly.</p>
        </div>
        <div class="col-md-3 col-sm-3">
          <h2>Reporting</h2>
          <img src="<?php echo Yii::app()->theme->getBaseUrl(); ?>/img/monitorly/promise-icon-4.png">
          <p> With a click of a button generate reports of your field staff activity and identify constant problem sites.</p>
        </div>
      </div>
      <div class="container">
        <div class="row text-center"><h2>With Monitorly monitoring is as easy as</h2></div>
        <div class="row" id="process">
           <div class="col-md-3">
            <div class="process-step">   
                <div class="process-step-rectangle"> 
                    <h2>Create</h2>
                    <p>Campaign and Users</p>
                </div>
                <div class="process-step-arrow">
                </div>
            </div>
           </div> 
           <div class="col-md-3">
            <div class="process-step">   
                <div class="process-step-rectangle"> 
                    <h2>Assign</h2>
                    <p>Zones and Sites to Field
Staff</p>
                </div>
                <div class="process-step-arrow">
                </div>
            </div>
           </div> 
           <div class="col-md-3">
            <div class="process-step">   
                <div class="process-step-rectangle"> 
                    <h2>Collect</h2>
                    <p>Images and Data</p>
                </div>
                <div class="process-step-arrow">
                </div>
            </div>
           </div> 
           <div class="col-md-3">
            <div class="process-step">   
                <div class="process-step-rectangle"> 
                    <h2>Report</h2>
                    <p>Performance and Analysis</p>
                </div>
                <div class="process-step-arrow">
                </div>
            </div>
           </div> 
        </div>  
      <div class="row landing-footer">
          <footer>
            <p>&copy; 2014 EatAds.com - All Rights Reserved Company 2014
                <span class="footer-links pull-right">
                    <a href="https://www.eatads.com/about-us">About</a>
                    <a href="https://www.eatads.com/contact-us">Contact</a>
                    <a href="https://www.facebook.com/EatAds?ref=br_tf"><img src="<?php echo Yii::app()->theme->getBaseUrl(); ?>/img/monitorly/facebook.png"></a>
                    <a href="#"><img src="<?php echo Yii::app()->theme->getBaseUrl(); ?>/img/monitorly/linkedin.png"></a>
                </span>
            </p>
          </footer>
      </div>
    </div> <!-- /container -->        
    </body>
    </html>