    <div class="navbar navbar-second navbar-static-top ">
        <div class="container">
            <div class="left">
                <h1>FAQ</h1>
            </div>
            <?php $this->widget('CreateAccountButton'); ?>
        </div>
    </div>
<?php if ($this->beginCache('faq', array('duration' => $this->cacheTime))) { ?>
    <div class="container main-body">
        <div class="row">
            <div class="col-sm-12 left">

                <h3>Is EatAds free?</h3>
                <p>Yes. It is free for media sellers to sign up, create listings and generate leads. It is also free for media buyers to search, browse and make an offer for a media placement.</p>
                <h3>I'm a seller. How do I list my media, so that I can generate leads?</h3>
                <p>After you have created your Media Owner Account, make sure you are logged-in and follow the online guide accessible from your Dashboard.</p>
                <h3>I'm a buyer. How do I buy an advertising placement?</h3>
                <p>Create a Media Buyer Account, find the listing you like, then make an offer. The Media Owner will then contact you either directly or via your EatAds account. Alternatively you can contact the Media Owner direct using the contact details provided on the listing. After you have agreed the deal with the Media Owner the final order and payment is done directly (i.e. not via EatAds).</p>
                <h3>What media types do you have?</h3>
                <p>EatAds supports multiple media types in the Outdoor and OOH categories. If your media type is not shown please contact us.</p>
                <h3>More questions?</h3>
                <p>Contact us if you have further queries, <a href="mailto:support@eatads.com">support@eatads.com</a>. We always love hearing from you.</p>
            </div>

        </div>
    </div>
    <?php $this->endCache();
} ?>
<script>
    $(function() {
        $("#currencyDropDown").hide();
    });
</script>