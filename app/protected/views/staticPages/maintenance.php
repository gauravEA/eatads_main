<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle = Yii::app()->name . ' - Error';
$this->breadcrumbs = array(
    'Home',
    'Maintenance',
);
?>

<div class="navbar navbar-second navbar-static-top home-bar ">
    <div class="container">
        <div class="left normal errorcode">
            <h1>
                We are upgrading our platform at the moment to provide you a better service.
            </h1>
        </div>
    </div>
</div>
<div class="container main-body">
    <div class="row">
        <?php //echo CHtml::encode($message); ?>
    </div>
</div>
