    <div class="navbar navbar-second navbar-static-top ">
        <div class="container">
            <div class="left">
                <h1>Contact Us</h1>
            </div>
            <?php $this->widget('CreateAccountButton'); ?>
        </div>
    </div>
<?php if ($this->beginCache('contactUs', array('duration' => $this->cacheTime))) { ?>    
    <div class="container main-body" id="contact-us">
        <div class="row">
            <div class="col-sm-9 left">
                <h2>Write a comment</h2>
                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'contact-form',
                        'enableClientValidation'=>true,
                        'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                        ),
                        'htmlOptions' => array('class' => 'form-signup' ),
                    )); ?>
                    <div class="col-sm-6 clearfix">
                        <label class="top">Your Name </label> 
                        <?php echo $form->textField($model,'name', array('class'=>'form-control')); ?>                            
                        <?php echo $form->error($model,'name', array('class'=>'errormessage')); ?>
                    </div><div class="clear clearfix"></div>
                    <div class="col-sm-6 clearfix">
                        <label class="top">Your Email</label> 
                        <?php echo $form->textField($model,'email', array('class'=>'form-control')); ?>                            
                        <?php echo $form->error($model,'email', array('class'=>'errormessage')); ?>
                    </div><div class="clear clearfix"></div>
                    <div class="col-sm-12 clearfix">
                        <label class="top">Your Message</label><div class="clear clearfix"></div> 
                        <?php echo $form->textArea($model,'message', array('class'=>'form-control col-sm-8')); ?>                            
                        <?php echo $form->error($model,'message', array('class'=>'errormessage')); ?>
                    </div>
                    <input type="submit" id="_submit" class="btn btn-lg btn-success big" value="Send Your Message"/>
                <?php $this->endWidget(); ?>
            </div>
            <?php $this->renderPartial('_right');?>
        </div>
    </div>
    <?php $this->endCache();
} ?>

<script>
    $(function() {
        $("#currencyDropDown").hide();
    });
</script>