<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/airportDemo/bootstrap.min.css'); ?>
<!--        <link rel="stylesheet" href="css/airportDemo/bootstrap.min.css">-->
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/airportDemo/bootstrap-theme.min.css'); ?>
<!--        <link rel="stylesheet" href="css/airportDemo/bootstrap-theme.min.css">-->
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/airportDemo/main.css'); ?>
<!--        <link rel="stylesheet" href="css/airportDemo/main.css">-->
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/airportDemo/jcarousel.basic.css'); ?>
<!--        <link rel="stylesheet" type="text/css" href="css/airportDemo/jcarousel.basic.css">-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--        <script src="js/airportDemo/libs/bootstrap/bootstrap.min.js"></script>-->
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/airportDemo/bootstrap/bootstrap.min.js', CClientScript::POS_BEGIN); ?>
<!--        <script src="js/airportDemo/main.js"></script>-->
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/airportDemo/main.js', CClientScript::POS_BEGIN); ?>
<!--        <script src="js/airportDemo/jquery-1.9.0/jquery.min.js"></script>-->
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/airportDemo/jquery-1.9.0/jquery.min.js', CClientScript::POS_BEGIN); ?>
<!--        <script src="js/airportDemo/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>-->
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/airportDemo/modernizr/modernizr-2.6.2-respond-1.1.0.min.js', CClientScript::POS_BEGIN); ?>
        
        
<!--        <script type="text/javascript" src="js/airportDemo/dust/dust-full-2.2.0.js"></script>-->
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/airportDemo/dust/dust-full-2.2.0.js', CClientScript::POS_BEGIN); ?>
<!--        <script type="text/javascript" src="js/airportDemo/dust/dust-helpers-1.1.1.js"></script>-->
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/airportDemo/dust/dust-helpers-1.1.1.js', CClientScript::POS_BEGIN); ?>
<!--        <script type="text/javascript" src="js/airportDemo/template/vendor_listing.tl"></script>-->
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/airportDemo/template/vendor_listing.tl', CClientScript::POS_BEGIN); ?>
    
        <script>
          var favItems = [];
          var addedItems = [];
            var currSlideIdx = 0;
           function generateCarousalImages() {
            $.getJSON('js/airportDemo/data.json', function(data){
                 //$.each(data.items, function(idx) {
                var thisD = data.items[currSlideIdx];
                var html = '<li class="licol" id="liid_'+ currSlideIdx+'"><div style="position: relative;left: 0;right: 0;" id="img_'+currSlideIdx+'"> <img style="width:600px;height:400px; position: relative;top: 0;left: 0;"  src="' + thisD.src + '" alt="' + thisD.title + '">'
                        +'</div></li>';
                for (var i=0; i< thisD.sites.length; i++) {
                    var relTop = parseInt(thisD.sites[i].top) + 24;
                    var relLeft = parseInt(thisD.sites[i].left) + 30;
                    html += "<img src='/eatads/app/images/airportDemo/marker_airport.png' class='overlayImage1' style='position: absolute;top:" + thisD.sites[i].top +"px; left:" + thisD.sites[i].left +"px' onclick='showDetails(1,"+i+");'><img class='overlayImage1' style='display:none;position: absolute;top:" + relTop +"px; left:" + relLeft +"px;' id='img1_" + i + "' src='" + thisD.sites[i].img +"'> ";
                }
                $('#ulcol').html(html);
                dust.render("vendor_listings", thisD, function(err, out) {
                //console.log("this is test out from ajax11111 : " + out +"err "+ err);    
                    $('#vendorListing').html(out);
                });            
            });
           } 
            
           function forwardKeyNextItem() {
            currSlideIdx ++ ;   
            generateCarousalImages();
           } 
            
           function backwardKeyNextItem() {
            currSlideIdx -- ;   
            generateCarousalImages();
           } 
     
    $( document ).ready(function() {
        generateCarousalImages();
     });
    
    function showDetails(idx,i) {
        console.log("idx : " + idx + " i : " + i);
        $('#img'+ idx+'_'+ i ).toggle();
        $('#site_'+ idx+'_'+ i).toggleClass('selectedSite');

    }
    
    function setfav(id) {
         var num = parseInt($('#favaddedNum').html());
        if ($('#fav_'+id).hasClass('glyphicon-star-empty')) {
            $('#fav_'+id).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
            num = num +1;
        } else if ($('#fav_'+id).hasClass('glyphicon-star')) {
            $('#fav_'+id).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
            num = num -1;
        }
        $('#favaddedNum').html(num);
    }
    
    function add(id) {
         var num = parseInt($('#sitesaddedNum').html());
        if ($('#add_'+id).hasClass('glyphicon-plus')) {
            $('#add_'+id).removeClass('glyphicon-plus').addClass('glyphicon-minus');
            num = num +1;
        } else if ($('#add_'+id).hasClass('glyphicon-minus')) {
            $('#add_'+id).removeClass('glyphicon-minus').addClass('glyphicon-plus');
            num = num -1;
        }
        $('#sitesaddedNum').html(num);
    }
            
       </script>     
       <style>
           .wrapper {
/*    max-width: 620px;*/
    padding: 0 58px 43px 23px;
    margin: auto;
}
           .imageWrapper {
  position: relative;
}
.overlayImage1 {
  position: absolute;
}

.selectedSite {
    background-color: bisque;
}
       </style>
    </head>

    <body>
        
    <!-- header -->    
<!--    <nav class="navbar navbar-default custom-nav" role="navigation">
  <div class="container-fluid" id="top-header">
     Brand and toggle get grouped for better mobile display 
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
 
      <a class="navbar-brand" href="#">Graphisads</a>
    </div>

     Collect the nav links, forms, and other content for toggling 
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="custom-active"><a href="#">Inventory</a></li>
        <li><a href="#">About</a></li>
        <li><button type="submit" class="btn btn-default primary-btn">Contact</button></li>
            <li class="divider"></li>
          </ul>
       
          
      <ul class="nav navbar-nav navbar-right ">
        <li>
           <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
            </form> 
        </li>
          
         /My Plans   
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Plans<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a> </li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </li>
          
         User Account  
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Stephanie<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a> </li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </li>
        <li><img src="img/user.png" width="60px" height="60px"></li>
          
      </ul>
    </div> /.navbar-collapse 
  </div> /.container-fluid 
</nav>-->
        
    <!-- /filter bar -->     
<!--    <nav class="navbar navbar-default custom-navbar" role="navigation">
  <div class="container-fluid" id="filter-bar">
     Brand and toggle get grouped for better mobile display 
    <div class="navbar-header">
    </div>
  </div> /.container-fluid 
</nav>-->
    
    <!-- /content -->
    <div class="row" id="content">

<div class="col-md-8 wrapper ">
    
<div class="jcarousel-wrapper">
                <div class="jcarousel">
                    <ul id="ulcol">
                    </ul>
                </div>
    <a href="#" class="jcarousel-control-prev" onclick="backwardKeyNextItem();">&lsaquo;</a>
                <a href="#" class="jcarousel-control-next" onclick="forwardKeyNextItem();"  >&rsaquo;</a>
                
                <p class="jcarousel-pagination">
                </p>
                <div id="pid">
                </div>    
            </div>
        </div>    
    <div class="col-md-4 " id="sitelist">
            
          <!-- sort -->
          <div>
<span>
                <div class="btn-group">
                    <button type="button" class="btn btn-default">Sites Added <span id="sitesaddedNum">0</span> </button>
                    <button type="button" class="btn btn-default">Favorites <span id="favaddedNum">0</span></button>
                </div>
            </span>              
              
              </div>
            
           <!-- listing --> 
           <div class="list-group" id="vendorListing">
               
               <!-- start of list item -->
               
                  <!-- list item -->
                  <a href="#" class="list-group-item">
                    <div class="pull-right">
                        <button class="glyphicon glyphicon-plus small-btn"></button>
                        <button class="glyphicon glyphicon-star small-btn"></button>
                    </div>
                      
                    <div class="pull-left">
                        <img class="media-object" src="" alt="" height="60px" width="90px">   </div>   
  
                    <h4 class="list-group-item-heading">Vasant Vihar</h4>
                    <p class="list-group-item-text">Non-lit Billboard &nbsp; &nbsp; 2.5x4.5 ft2</p>
                    <p class="list-group-item-text price">INR 5000 <span class="per-month">per mo</span></p>
                   </a>
                    <!-- end of list item -->
                  
</div>
<!--          <img class="overlayImage" src="image/aa.jpg" >-->
        </div>
        
        <!-- /sitelist -->
        
            
                
          </div>
             
        


    </body>
</html>
