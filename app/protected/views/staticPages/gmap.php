<script>
    // global marker
    var marker;
    var map;
    function initialize() {
      // Enable the visual refresh
      google.maps.visualRefresh = true;
      
        myLatLng = new google.maps.LatLng(1.3, 103.8);
      
      var mapOptions = {
        zoom: 6,
        center: myLatLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        streetViewControl: true,
        panControl: false,
        mapTypeControl: false
      }
      map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
    }      
    function loadScript()
    {
      var script = document.createElement("script");
      script.type = "text/javascript";
      script.src = "https://maps.googleapis.com/maps/api/js?key=<?php echo Yii::app()->params['gmapApiKey']; ?>&sensor=false&callback=initialize";
      // &language=ja for language change
      // &region=GB for regional settings
      document.body.appendChild(script);
    }
    window.onload = loadScript;
</script>