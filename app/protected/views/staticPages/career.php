    <div class="navbar navbar-second navbar-static-top ">
        <div class="container">
            <div class="left">
                <h1>Career</h1>
            </div>
            <?php $this->widget('CreateAccountButton'); ?>
        </div>
    </div>
<?php if ($this->beginCache('career', array('duration' => $this->cacheTime))) { ?>
    <div class="container main-body">


        <div class="row">
            <div class="col-sm-12 top-gap">
                <p class="career">EatAds is not currently hiring at the moment. However, we intend to grow rapidly, so keep an eye on this space.

                </p>
            </div>
        </div>
    </div>
    <!-- /container -->
        <?php $this->endCache();
    } ?>

