
            <div class="navbar navbar-second navbar-static-top ">
                <div class="container">
                    <div class="left">
                        <h1>Terms & Conditions</h1>
                    </div>
                    <?php $this->widget('CreateAccountButton'); ?>
                </div>
            </div>
<?php if ($this->beginCache('terms', array('duration' => $this->cacheTime))) { ?>
            <div class="container main-body">
                <div class="row">
                    <div class="col-sm-9 left">
                        <div id="about-text">
                            <p>This website is operated by EatAds Pte Ltd (“EatAds”). The use of the EatAds website including all information, tools and services available from this site, constitutes your agreement and acceptance to these Terms and Conditions. Before you may become a member of EatAds, you must read and accept all of the terms and conditions. By accepting these Terms and Conditions, you agree that this will apply whenever you use the EatAds website and services.</p><br>
                            <h3>ACCURACY, COMPLETENESS AND TIMELINESS OF INFORMATION ON THIS SITE</h3>
                            <p>EatAds is not responsible if information made available on this site is not accurate, complete or current. Any reliance on the material on this site is at your own risk. This site may contain certain historical information. Historical information is not necessarily current and is provided for your reference only. We reserve the right to modify the contents of this site at any time, but we have no obligation to update any information on the site. You agree that it is your responsibility to monitor changes to the site.</p><br>
                            <h3>APPROPRIATE USE OF THE EATADS.COM WEBSITE</h3>
                            <p>When using EatAds.com, you will not:</p><br>
                            <ul class="normal">
                                <li>Post content or items which are inappropriate with regards to the nature and context of our site,</li>
                                <li>Violate any laws or third party rights,</li>
                                <li>Use our sites or services if you are not able to form legally binding contracts, are under the age of 18, or are temporarily or indefinitely suspended from our site,</li>
                                <li>Manipulate the price of any item or interfere with other user's listings,</li>
                                <li>Circumvent or manipulate our fee structure, the billing process, or fees owed to EatAds,</li>
                                <li>Post false, inaccurate, misleading, defamatory, or libelous content (including personal information),</li>
                                <li>Transfer your EatAds account and User ID to another party without our consent,</li>
                                <li>Distribute or post spam, false information or promote pyramid schemes,</li>
                                <li>Distribute viruses or any other technologies that may harm EatAds, or the interests or property of EatAds users,</li>
                                <li>Copy, modify, or distribute content from the sites and EatAds' copyrights and trademarks; or harvest or otherwise collect information about users, including email addresses, without their consent; and</li>
                                <li>Use contact information provided on the site for unauthorized purposes, including marketing.</li><br>
                            </ul>
                            <h3>ABUSE OF EATADS WEBSITE</h3>
                            <p>You may not use any hardware or software intended to damage or interfere with the proper working of the site or to surreptitiously intercept any system, data or personal information from the site. You agree not to interrupt or attempt to interrupt the operation of the site in any way.</p><br>
                            <p>Without limiting other remedies, we may limit, suspend, or terminate our service and user accounts, prohibit access to our site and their content, delay or remove hosted content, and take technical and legal steps to keep users off the site if we think that they are creating problems or possible legal liabilities, infringing the intellectual property rights of third parties, or acting inconsistently with the letter or spirit of our policies. Additionally, we may, in appropriate circumstances and at our discretion, suspend or terminate accounts of users who may be repeat infringers of intellectual property rights of third parties. We also reserve the right to cancel unconfirmed accounts or accounts that have been inactive for a long time.</p><br>
                            <h3>USE OF MATERIALS ON THE SITE</h3>
                            <p>Content on this site including, without limitation, text, design, graphics, logos, icons, images, audio clips, downloads, interfaces, code and software, as well as the selection and arrangement thereof, is the exclusive property of and owned by EatAds, its licensors or its content providers and is protected by copyright, trademark and other applicable laws. You may access, copy, download and print the material contained on the site for your personal and non-commercial use, provided you do not modify or delete any copyright, trademark or other proprietary notice that appears on the material you access, copy, download or print. Any other use of content on the site, including but not limited to the modification, distribution, transmission, performance, broadcast, publication, uploading, licensing, reverse engineering, transfer or sale of, or the creation of derivative works from, any material, information, software, products or services obtained from the site, or use of the site for purposes competitive to EatAds, is expressly prohibited. You agree to abide by all additional restrictions displayed on the site as it may be updated from time to time.</p><br>
                            <p>When you give us content, you grant us a non-exclusive, worldwide, perpetual, irrevocable, royalty-free, sublicensable (through multiple tiers) right to exercise any and all copyright, publicity, and database rights (but no other rights) you have in the content, in any media known now or in the future.</p><br>
                            <p>While we try to offer reliable data, we cannot promise that the listings will always be accurate and up-to-date. You agree that you will not hold our content providers or us responsible for inaccuracies in their listings.</p><br>
                            <h3>LIMITATION OF LIABILITY</h3>
                            <p>You will not hold EatAds responsible for other users' content or items they list. You acknowledge that we are not a traditional auctioneer. Instead, the sites are a venue to allow anyone to offer, sell, and buy at anytime, from anywhere, in a variety of pricing formats such as fixed price formats and auction-style formats. We are not involved in the actual transaction between buyers and sellers. We have no control over and do not guarantee the quality, safety or legality of items advertised, the truth or accuracy of users’ content or listings, the ability of sellers to sell items, the ability of buyers to pay for items, or that a buyer or seller will actually complete a transaction.</p><br>
                            <p>We cannot guarantee continuous or secure access to our services, and operation of the site may be interfered with by numerous factors outside of our control. Accordingly, to the extent legally permitted, we exclude all implied warranties, terms and conditions. We are not liable for any loss of money, goodwill, or reputation, or any special, indirect, or consequential damages arising out of your use of our site and services.</p><br>
                            <h3>CONDUCT ON THE SITE</h3>
                            <p>Some features that may be available on this site require registration. By registering at and in consideration of your use of the site you agree to provide true, accurate, current and complete information about yourself.</p><br>
                            <p>Some features on this site require use of a password. You are responsible for protecting your password. You agree that you will be responsible for any and all statements made, and acts or omissions that occur, through the use of your password. If you have any reason to believe or become aware of any loss, theft or unauthorized use of your password, notify EatAds immediately. EatAds may assume that any communications EatAds receives under your password have been made by you unless EatAds receives notice otherwise.</p><br>
                            <p>You or third parties acting on your behalf are not allowed to frame this site or use our proprietary marks as meta tags, without our written consent. You may not use frames or utilize framing techniques or technology to enclose any content included on the site without EatAds' express written consent. Further, you may not utilize any site content in any meta tags or any other "hidden text" techniques or technologies without EatAds's express written consent.</p><br>
                            <h3>PRIVACY</h3>
                            <p>We treat data as an asset that must be protected and use lots of tools (encryption, passwords, physical security, etc.) to protect your personal information against unauthorized access and disclosure. However, as you probably know, third parties may unlawfully intercept or access transmissions or private communications, and other users may abuse or misuse your personal information that they collect from the site. Therefore, although we work very hard to protect your privacy, we do not promise, and you should not expect, that your personal information or private communications will always remain private.</p><br>
                            <ul class="normal">
                                <li>We may collect and store the following personal information:</li>
                                <li>Email address, physical contact information, and (depending on the service used) sometimes financial information, such as credit card or bank account numbers.</li>
                                <li>Transactional information based on your activities on the sites (such as bidding, buying, selling, item and content you generate or that relates to your account).</li>
                                <li>Correspondence through our sites, and correspondence sent to us.</li>
                                <li>Other information from your interaction with our sites, services, content and</li>
                                <li>Advertising, including computer and connection information, statistics on page views, traffic to and from the sites, ad data, IP address and standard web log information.</li>
                                <li>Additional information we ask you to submit to authenticate yourself or if we believe you are violating site policies.</li>
                                <li>Information from other companies, such as demographic and navigation data; and other supplemental information from third parties.</li><br>
                            </ul>
                            <p>Our primary purpose in collecting personal information is to provide you with a safe, smooth, efficient, and customized experience. You agree that we may use your personal information to:</p><br>
                            <ul class="normal">
                                <li>Provide the services and customer support you request.</li>
                                <li>Resolve disputes, collect fees, and troubleshoot problems.</li>
                                <li>Prevent potentially prohibited or illegal activities, and enforce our Terms and Conditions.</li>
                                <li>Customize, measure and improve our services, content and advertising.</li>
                                <li>Tell you about our services and those of our corporate family, targeted marketing, service updates, and promotional offers, based on your communication preferences. and</li>
                                <li>Compare information for accuracy, and verify it with third parties.</li><br>
                            </ul>
                            <p>We don't sell or rent your personal information to third parties for their marketing purposes without your explicit consent. We may combine your information with information we collect from other companies and use it to improve and personalize our services, content and advertising.</p><br>
                            <h3>Our Disclosure of Your Information:</h3>
                            <p>We may disclose personal information to respond to legal requirements, enforce our policies, respond to claims that a listing or other content violates the rights of others, or protect anyone's rights, property, or safety. Such information will be disclosed in accordance with applicable laws and regulations.</p><br>
                            <h3>BUYER PROTECTION</h3>
                            <p>Buyers and sellers share the responsibility for making sure purchases facilitated by EatAds are exciting, rewarding and hassle-free. We strongly encourage buyers to work with sellers before opening a claim relating to a purchase. Buyers and sellers agree to follow the requirements of the applicable buyer protection policies and rules applicable in the country where transaction has taken place.</p><br>
                            <p>If you list your items for sale internationally and/or otherwise offer to make your products available to buyers in other countries, you will be subject to the policies and rules in that country.</p><br>
                            <h3>INDEMNIFICATION</h3>
                            <p>You agree to indemnify, defend and hold harmless EatAds and its affiliates and their officers, directors, employees, contractors, agents, licensors, service providers, subcontractors and suppliers from and against any and all losses, liabilities, expenses, damages and costs, including reasonable attorneys' fees and court costs, arising or resulting from your use of the site and any violation of these Terms of Use. If you cause a technical disruption of the site or the systems transmitting the site to you or others, you agree to be responsible for any and all losses, liabilities, expenses, damages and costs, including reasonable attorneys' fees and court costs, arising or resulting from that disruption.</p><br>
                            <h3>JURISDICTION AND APPLICABLE LAW</h3>
                            <p>This site has been designed to comply with the laws of the Singapore. If any material on this site, or your use of the site, is contrary to the laws of the place where you are when you access it, the site is not intended for you, and we ask you not to use the site. You are responsible for informing yourself of the laws of your jurisdiction and complying with them.</p><br>
                            <h3>CHANGES TO THESE TERMS OF USE</h3>
                            <p>EatAds reserves the right, at its sole discretion, to change, modify, add or remove any portion of these Terms and Conditions, in whole or in part, at any time, by posting revised terms on the site. It is your responsibility to check periodically for any changes we make to the Terms and Conditions. Your continued use of the site after any changes to the Terms and Conditions means you accept the changes.</p><br>
                            <h3>SEVERABILITY</h3>
                            <p>If any provision of this agreement is unlawful, void or unenforceable, the remaining provisions of the agreement will remain in place.</p><br>


                        </div>
                    </div>

                </div>
            </div>

<?php $this->endCache(); } ?>
<script>
    $(function() {
        $("#currencyDropDown").hide();
    });
</script>