    <div class="navbar navbar-second navbar-static-top ">
        <div class="container">
            <div class="left">
                <h1>Press</h1>
            </div>
            <?php $this->widget('CreateAccountButton'); ?>
        </div>
    </div>
<?php if ($this->beginCache('press', array('duration' => $this->cacheTime))) { ?>
    <div class="container main-body">
        <div class="row">
            <div class="col-sm-9 left press">
                <div class="col-md-8">
                    <div class="left">
                        <img src="<?php echo Yii::app()->baseUrl; ?>/images/site/placeholder.png" alt="placeholder" />
                    </div>
                    <div class="left">
                        <p>29 January 2013</p>
                        <h3>EatAds Coverage in India</h3>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="left">
                        <img src="<?php echo Yii::app()->baseUrl; ?>/images/site/placeholder.png" alt="placeholder" />
                    </div>
                    <div class="left">
                        <p>29 January 2013</p>
                        <h3>EatAds Coverage in India</h3>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="left">
                        <img src="<?php echo Yii::app()->baseUrl; ?>/images/site/placeholder.png" alt="placeholder" />
                    </div>
                    <div class="left">
                        <p>29 January 2013</p>
                        <h3>EatAds Coverage in India</h3>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 right press">
                <div class="blue-box left">
                    <h3> Press Kit</h3>
                    <a href="#" class="btn btn-primary">Download</a>

                </div>

            </div>
        </div>
    </div>
<?php $this->endCache(); } ?>
