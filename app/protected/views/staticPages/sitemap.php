<div class="navbar navbar-second navbar-static-top ">
    <div class="container">
        <div class="left">
            <h1>Sitemap</h1>
        </div>
        <?php $this->widget('CreateAccountButton'); ?>
    </div>
</div>
<?php  //if ($this->beginCache('Sitemap', array('duration' => $this->cacheTime))) { ?>

<div class="container main-body">
    <div class="content_header">

    </div>

    <div class="row col-sm-12 left sitemap">
        <div class="row">
            <ul>
                <li><a href="<?php echo Yii::app()->urlManager->createUrl('staticPages/termsAndCondition'); ?>">Terms & Conditions </a></li>
                <li><a href="<?php echo Yii::app()->urlManager->createUrl('staticPages/aboutUs'); ?>">About Us</a></li>
                <li><a href="<?php echo Yii::app()->urlManager->createUrl('staticPages/contactUs'); ?>">Contact Us</a></li>
                <li><a href="<?php echo Yii::app()->urlManager->createUrl('staticPages/faq'); ?>">FAQ</a></li>                
            </ul>
        </div>
        <div class="row">
            <?php
                foreach($countryCityList as $cclist) { 
                    $countryId = $cclist['id'];
                    ?>
                    <ul>
                        <b><li><?php echo CHtml::link(CHtml::encode($cclist['country']), 'javascript:void(0);', array('class'=>'csc_geo')); ?></li></b>
                        <li>
                            <ul>
                                <?php foreach($cclist['cities'] as $cities) { ?>                            
                                    <li><?php echo CHtml::link(CHtml::encode($cities['name']), 'javascript:void(0);', array('class'=>'csc_geo c_'.ucfirst($cities['name']))); ?></li>
                                <?php } ?>
                            </ul>
                        </li>
                    </ul>
            <?php } ?>
        </div>
        <?php $mediaTypes = ''; 
            foreach(MediaType::getPriorityMediaType() as $key => $value) {
                $mediaTypes .=  $key.",";
            }
            $mediaTypes = rtrim($mediaTypes,',');
        ?>
        <input type="hidden" id="geocomplete" />
        <input type="hidden" id="geo_latlng" />        
        <input type="hidden" id="mediatypeids" value="<?php echo $mediaTypes; ?>"/>
        <input type="hidden" id="vp" value=""/>
    </div>
</div>
<?php //$this->endCache(); }  ?>
<?php   
    $protocol = Yii::app()->params['protocol'];
    Yii::app()->clientScript->registerScriptFile($protocol.'maps.googleapis.com/maps/api/js?sensor=false&libraries=places', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.geocomplete.js', CClientScript::POS_END);  
?>
<script>
    function calc_proximity()
    {
        // r = radius of the earth in statute miles
        var r = 3963.0;
        // Convert lat or lng from decimal degrees into radians (divide by 57.2958)
        var geolatlng = $('#geo_latlng').val();
        geolatlng = geolatlng.split(",");
        var lat1 = parseFloat(geolatlng[0]) / 57.2958;
        var lon1 = parseFloat(geolatlng[1]) / 57.2958;
        var ne = $('#vp').val();
        ne = ne.split(",");        
        var lat2 = ne[0] / 57.2958;
        var lon2 = ne[1] / 57.2958;
        // distance = circle radius from center to Northeast corner of bounds
        proximity = r * Math.acos(Math.sin(lat1) * Math.sin(lat2) +
                Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1));
        proximity = Math.round(proximity * 1000) / 1000;
        console.log(proximity);
        return proximity;        
    }
    $(function() {
        $("#currencyDropDown").hide();
        
        $('.csc_geo').click(function(e){
            e.preventDefault();
            /*var search = $(this).text() + ', ';
            search += $(this).parent().siblings('h3').children('a').text();
            var country = $('#country').val();
            if(country.length) {
                search += ", " + country;
            }*/
            var search = $(this).text() + ", "; 
            search += $(this).closest('ul').parent().parent().children('b').children('li').find('a').text();
            //console.log(search);
            $("#geocomplete").val(search).trigger("geocode");
            return false;
        });
        
        $("#geocomplete").geocomplete()
        .bind("geocode:result", function(event, result){
            var lat = parseFloat(result.geometry.location.lat()).toFixed(6);
            var lng = parseFloat(result.geometry.location.lng()).toFixed(6);
            var location = lat + "," + lng;
            //console.log(result.geometry.viewport);
            $('#geo_latlng').val(location);
            var vp = result.geometry.viewport.getNorthEast();
            vp = parseFloat(vp.lat()).toFixed(6) + "," + parseFloat(vp.lng()).toFixed(6);
            $('#vp').val(vp);
            //$('#viewport').val(result.geometry.viewport);
            var vp = result.geometry.viewport;
            if(vp) {
                // create link and redirect
                var geoloc = $('#geo_latlng').val();
                var mediatypeids = $('#mediatypeids').val();
                var mt = $('#geocomplete').val();
                mt = mt.split(",");
                var proximity = calc_proximity();
                // var url = '<?php // echo Yii::app()->urlManager->createUrl("/map/index?"); ?>mediatypeid='+mediatypeids+'&geoloc='+geoloc;
                var url = '<?php echo Yii::app()->urlManager->createUrl("/s/mapview?"); ?>mediatypeid='+mediatypeids+'&geoloc='+geoloc + '&proximity=' + proximity + '&mt=' + mt[0];
                //console.log(url);
                window.location.href = url;
            } else {
                console.log('no bounds');
                alert('Please try some other location.');
            }
        })
        .bind("geocode:error", function(event, status){
          //$.log("ERROR: " + status);
        })
        .bind("geocode:multiple", function(event, results){
          //$.log("Multiple: " + results.length + " results found");
        });
    });
</script>