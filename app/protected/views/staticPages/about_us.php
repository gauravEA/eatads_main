<div class="navbar navbar-second navbar-static-top ">
    <div class="container">
        <div class="left">
            <h1>About Us</h1>
        </div>
        <?php $this->widget('CreateAccountButton'); ?>
    </div>
</div>  
<?php if ($this->beginCache('aboutUs', array('duration' => $this->cacheTime))) { ?>
    <div class="container main-body about">
        <div class="row">
            <div class="col-sm-9 left">
                <h2>Executive Management</h2>
                <div class="col-xs-3 ">
                    <img src="<?php echo Yii::app()->baseUrl; ?>/images/site/dhruv.png" alt="dhruv" />
                    <h3>Dhruv Sahgal<a href="http://sg.linkedin.com/in/dhruvsahgal" target="_blank" class="in"></a></h3>
                    <p>Co- Founder, Business Ops</p>
                    <p class="italic">Engineer by degree, started career in banking in the USA, Hong Kong and Singapore. He is a problem solver who likes to use technology to create a more efficient world. This is Dhruv's 2nd venture.</p>
                </div>
                <div class="col-xs-3">
                    <img src="<?php echo Yii::app()->baseUrl; ?>/images/site/nigel.png" alt="nigel" />
                    <h3>Nigel Hembrow<a href="http://www.linkedin.com/in/nigelhembrow" target="_blank" class="in"></a></h3>
                    <p>Co- Founder,<br/>Product & Market</p>
                    <p class="italic">With a background in property development, social enterprise and digital ventures; Nigel loves building ventures and products that help transform the way people live and work, and create value shared widely. This is Nigel's third startup.</p>
                </div>
                <div class="col-xs-3">
                    <img src="<?php echo Yii::app()->baseUrl; ?>/images/site/rahul.png" alt="rahul" />
                    <h3>Rahul Chidgopkar<a href="http://in.linkedin.com/in/rahulchidgopkar" target="_blank" class="in"></a></h3>
                    <p>Head of Sales</p>
                    <p class="italic">Rahul is an IIM-graduate with an engineering background. Having run his own startup and held senior positions at others, he brings a unique blend of management skills, technical ability and startup experience to the EatAds team.</p>
                </div><div class="clear"></div>
                <h2>Advisory Board</h2>                        
                <div class="col-xs-3 ">
                    <img src="<?php echo Yii::app()->baseUrl; ?>/images/site/ron.png" alt="ron" />
                    <h3>Ron Graham<a href="http://sg.linkedin.com/pub/ron-graham/9/710/79b" target="_blank" class="in"></a></h3>
                    <p class="italic">Ron brings over 30 years of Outdoor media experience to EatAds and has worked both the buy-side and sell-side of OOH media. Since 2009, Ron has acted as an independent consultant throughout Asia-Pacific and is actively involved in new developments and in particular new technology applications for out of home media, including digital and mobile</p>
                </div>
                <div class="col-xs-3 ">
                    <img src="<?php echo Yii::app()->baseUrl; ?>/images/site/jonathan.png" alt="jonathan" />
                    <h3>Jonathan Bill<a href="http://in.linkedin.com/pub/jonathan-bill/0/235/372" target="_blank" class="in"></a></h3>
                    <p class="italic">Jonathan is a mobile internet expert, having spent a decade leading Vodafone's mobile internet operations and strategy in developing markets including India, the Middle East, Africa and Eastern Europe. Prior to Vodafone he was Commercial Director at internet ad serving company Real Media and was Business Development Director for Reuters Media.</p>
                </div>
                <div class="col-xs-3 ">
                    <img src="<?php echo Yii::app()->baseUrl; ?>/images/site/john.png" alt="john" />
                    <h3>John Fearon<a href="http://sg.linkedin.com/pub/john-fearon/13/826/668" target="_blank" class="in"></a></h3>
                    <p class="italic">John is a serial entrepreneur and brings with him 10 years of Digital marketing experience. As Chairman of Gilcrux Holdings, John has the unique knack of building great teams around great ideas.</p>
                </div>
                <div class="clear"></div>
                <div class="col-xs-3 ">
                    <img src="<?php echo Yii::app()->baseUrl; ?>/images/site/sangeet.png" alt="sangeet" />
                    <h3>Sangeet Paul Choudary<a href="http://sg.linkedin.com/in/sangeetpaul" target="_blank" class="in"></a></h3>
                    <p class="italic">Apart from being the man behind Platformed.info, Sangeet brings Strong experience in incubation, investments and innovation in the Internet, Mobile and New Media Industries. Sangeet has Led platform strategy for multiple startups. Consulted leading multi-billion dollar high growth startups as well as traditional telco and media companies on network effects and platform strategies.</p>
                </div>
                <div class="col-xs-3 ">
                    <img src="<?php echo Yii::app()->baseUrl; ?>/images/site/charif.png" alt="charif" />
                    <h3>Charif El Ansari<a href="http://sg.linkedin.com/pub/charif-el-ansari/1/65a/351" target="_blank" class="in"></a></h3>
                    <p class="italic">Charif brings over 10 years of digital and software expertise to the EatAds team. His last role was the head of business development for Google in Southeast Asia and He currently is the CEO of the high growth startup, Dropmysite.</p>
                </div>
                <div class="clear"></div>
                <h2>About Us</h2>
                <p>At EatAds we believe that life should be simple and easy. Particularly when it comes to business.</p>
                <p>Our platform & mobile solutions help you manage your OOH sites, simplify & grow your business. EatAds is the leading platform for Outdoor & Out-of-Home advertising space.</p>
                <p>Our service provide you the latest web & mobile technologies for information sharing & better decision-making. And it’s not complicated. Whether it is preparing and presenting your inventory information in our simplified map-view, or managing your availability via our partner software providers, EatAds will help you increase your reach amongst media buyers.</p>                        
                <p>We take no commission and it's easy for vendors to join. Media vendors  <a href="mailto:support@eatads.com">contact us</a> today.</p>
                <p>The Outdoor & Out-of-Home media (billboards, transit media, bus shelters, airport media etc.) industry is a large but disorganized industry globally. It has been very late in adopting similar web tools that have brought great efficiency to almost all other industries.</p>
                <p>We are a Singapore company, with Asia focus, and a heavy presence in the India market. The Founders are all experienced entrepreneurs, backed by some of the most experienced digital investors in India and Singapore.</p>                        
            </div>
            <?php $this->renderPartial('_right'); ?>
        </div>
    </div>
    <?php $this->endCache();
}
?>
<script>
    $(function() {
        $("#currencyDropDown").hide();
    });
</script>