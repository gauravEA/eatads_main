<div class="col-sm-3 right">
    <div class="blue-box left ">
        <p><span>
            India Office</span><br/>
             177, Adharshila, <br>First Floor, Gulmohar Park Road, Gautam Nagar, Delhi – 110049</p>

    </div>
    <div class="map-box left" id="map-canvas01"></div>
    
    <div class="blue-box left last">
    <p><span>Singapore Office</span><br/>
            Block 71, Ayer Rajah Crescent<br/>
            #01-12 Singapore 139951.</p>

    </div>
    <div class="map-box left" id="map-canvas02"></div>
    <?php $this->widget('GoogleMap', array(
                            'onlyMap'=>true,                             
                            'mapFieldData' => array(
                                '0' => array(
                                        'fieldid' => 'map-canvas01',
                                        'lat' => 28.557808,
                                        'lng' => 77.210869,
                                        'showMarker' => 1,
                                        'zoomLevel'=>10
                                    ),
                                '1' => array(
                                        'fieldid' => 'map-canvas02',
                                        'lat' => 1.3,
                                        'lng' => 103.8,
                                        'showMarker' => 1,
                                        'zoomLevel'=>12
                                    )
                            ) 
                        )); ?>
</div>