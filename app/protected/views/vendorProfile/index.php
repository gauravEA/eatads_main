<!DOCTYPE html5>
<html ng-app="siteListing">
	
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Graphisads</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        
        
          <?php
            $theme = Yii::app()->theme;
            $cs = Yii::app()->clientScript;
            $cs->registerCssFile( $theme->getBaseUrl() . '/css/bootstrap.min.css' );
            $cs->registerCssFile( $theme->getBaseUrl() . '/css/bootstrap-theme.min.css' );
            $cs->registerCssFile( $theme->getBaseUrl() . '/css/main.css' );
            $cs->registerCssFile( $theme->getBaseUrl() . '/css/datepicker.css' );
       	 	$cs->registerCssFile( $theme->getBaseUrl() . '/css/slider.css' );
          $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/jquery-1.11.0.min.js');
          $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/bootstrap.js');
       	 $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js');
        

        
         
        ?>
       <?php Yii::app()->clientScript->registerScriptFile("https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=drawing", CClientScript::POS_BEGIN); ?>
        <?php
            $theme = Yii::app()->theme;
            $cs = Yii::app()->clientScript;
            

            $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/angular.js', CClientScript::POS_BEGIN);
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/custom/map.js', CClientScript::POS_BEGIN); 
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/custom/main.js', CClientScript::POS_BEGIN);
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/custom/bootstrap-datepicker.js', CClientScript::POS_BEGIN);
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/custom/bootstrap-slider.js', CClientScript::POS_BEGIN);
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/custom/app.js', CClientScript::POS_BEGIN);
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/ng-infinite-scroll.min.js', CClientScript::POS_BEGIN);
            
        ?>
        
        

        
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        
        

        <script>
            $(document).ready(function () {
                
            });
       </script>     
        
    </head>

    <body>
        <div style="display: none" id="basePath"><?php echo Yii::app()->getBaseUrl() ?></div>
        <!-- contact-modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header modal-header-custom">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h2 class="modal-title semi-bold" id="myModalLabel">Write to Graphisads</h2>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6"><span class="glyphicon glyphicon-envelope">&nbsp;</span><span class="semi-bold">Email: </span><span>contact@graphisads.com</span></div>
            <div class="col-md-6">
                <div class="pull-right">
                    <span class="glyphicon glyphicon-earphone">&nbsp;</span><span class="semi-bold">Phone: </span><span>+91 011-238445</span>
                </div>
            </div>
        </div>
        <br>
        <textarea class="form-control" rows="4"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary2" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary-custom1">Send</button>
      </div>
    </div>
  </div>
</div>

        
    <!-- header -->
        
    
    <nav class="navbar navbar-default navbar-static-top" role="navigation" id="top-header">
  <div class="container-fluid">
    <div class="row">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
 
      <a class="navbar-brand" href="#">
        <h6><span>Powered by </span><span style="color: #C70B27">Eat</span><span style="color:#17568A">Ads</span></h6>
        <div><img class="vendor-logo" src="images/vendorProfileDemo/vendor-logo.png"></div>
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active-custom"><a href="#">Inventory</a></li>
        <!--<li><a href="#">About</a></li>-->
        <li><button class="btn btn-primary-custom1" data-toggle="modal" data-target="#myModal">Contact</button></li>
            <li class="divider"></li>
          </ul>
        </li>
      </ul>
          
      <ul class="nav navbar-nav navbar-right ">
        <li>
           <form class="navbar-form navbar-left" role="search">
            </form> 
        </li>
          
        <!-- /My Plans -->  
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Plans<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#">Louis Vuitton Outdoor Delhi</a></li>
            <li><a href="#">Vodafone Mumbai</a></li>
            <li><a href="#">Skoda Launch Chennai</a> </li>
          </ul>
        </li>
          
        <!-- User Account --> 
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gaurav<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#">Logout</a> </li>
          </ul>
        </li>
        <li><img src="images/vendorProfileDemo/user.png" width="45px" height="50px" margin-top="10px"></li>
          
      </ul>
    </div><!-- /.navbar-collapse -->
    </div>      
  </div><!-- /.container-fluid -->
</nav>

<!-- filter bar -->
      <nav class="navbar navbar-default" role="navigation" id="filter-bar">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
          
        <!-- Availability -->  
        <li class="dropdown">
          <a href="#" class="dropdown-toggle dropdown-custom1" data-toggle="dropdown">Availability<b class="caret"></b></a>
          <ul class="dropdown-menu dropdown-menu-custom">
            <li>
                <div class="dropdown-menu-container">
                    <h4>Select a Date:</h4>
                    <br>
                    <div class="form-group row" id="datepicker">
                        <div class="col-xs-8">
                            <div class="input-group date" id="dp3" data-date="21-06-2014" data-date-format="dd-mm-yyyy">
                            <input class="form-control" type="text" value="21-06-2014">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                                                                  </div>
            </li>
          </ul>
        </li>
          
        <!-- Media Types -->  
        <li class="dropdown keep-open">
          <a href="#" class="dropdown-toggle dropdown-custom1" data-toggle="dropdown">Media Types<b class="caret"></b></a>
          <ul class="dropdown-menu dropdown-menu-custom">
            <li>
                <a href="#">
                    <div class="checkbox">
                        <label>
      <input type="checkbox">Billboards
                        </label>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="checkbox">
                        <label>
      <input type="checkbox">Lamp iers
                        </label>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="checkbox">
                        <label>Digital Screens
      <input type="checkbox">
                        </label>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="checkbox">
                        <label>
      <input type="checkbox">Bus Shelter
                        </label>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="checkbox">
                        <label>
      <input type="checkbox">Shopping Center Ad
                        </label>
                    </div>
                </a>
            </li>
            
          </ul>
        </li>
          
        <!-- Point of Interest -->  
        <li class="dropdown" id="poi">
          <a href="#" class="dropdown-toggle dropdown-custom1" data-toggle="dropdown">Point of Interest<b class="caret"></b></a>
          <ul class="dropdown-menu dropdown-menu-custom">
            <li>
                <a href="#">
                    <div class="checkbox">
                        <label>
      <input type="checkbox">School
                        </label>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="checkbox">
                        <label>
      <input type="checkbox">Cafe
                        </label>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="checkbox">
                        <label>
      <input type="checkbox">Hospital
                        </label>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="checkbox">
                        <label>
      <input type="checkbox">Cinema
                        </label>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="checkbox">
                        <label>
      <input type="checkbox">IT Parks
                        </label>
                    </div>
                </a>
            </li>
          </ul>
        </li>
          
        <!-- Pricing -->  
        <li class="dropdown">
          <a href="#" class="dropdown-toggle dropdown-custom1" data-toggle="dropdown">Pricing<b class="caret"></b></a>
          <ul class="dropdown-menu dropdown-menu-custom">
            <li>
                <div class="dropdown-menu-container">
                    <div class="slider-container">
                        INR 5000&nbsp;<input id="price-slider" type="text" class="span2" value="" data-slider-min="5000" data-slider-max="500000" data-slider-step="5000" data-slider-value="[5000,100000]"/>&nbsp;INR 500000
                    </div>
                </div>
            </li>
          </ul>
        </li>
          
        <!-- Lighting -->  
        <li class="dropdown">
          <a href="#" class="dropdown-toggle dropdown-custom1" data-toggle="dropdown">Lighting<b class="caret"></b></a>
          <ul class="dropdown-menu dropdown-menu-custom">
            <li>
                <a href="#">
                    <div class="checkbox">
                        <label>
      <input type="checkbox">Non-lit
                        </label>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="checkbox">
                        <label>
      <input type="checkbox">Front-lit
                        </label>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="checkbox">
                        <label>
      <input type="checkbox">Back-lit
                        </label>
                    </div>
                </a>
            </li>
          </ul>
        </li>
          
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
        
    
    <!-- /content -->
    <div class="row" id="content">
        
        <!-- /map -->
        <div class="col-md-8" id="map">
            <div id="clearoption" style="display:none" ><img src="https://demo.eatads.com/images/vendorProfileDemo/x.gif">Clear Slection</div>
			
			<nav id="social-sidebar">

				<ul>

					<li>
						<a href="#" class="entypo-drawing">
							<span>Drawing Tool</span>
						</a>
					</li>

					<li>
						<a href="#" class="entypo-audience" >
							<span>Audience View</span>
						</a>
					</li>

					<li>
						<a href="#" class="entypo-visulization" >
							<span>Visulization</span>
						</a>
					</li>

				</ul>

			</nav>
			
            <div id="map_canvas"></div>
            
            <!-- detailed-view -->
            <div id="detailed-view" ng-show="visible" ng-controller="DetailController">
              <div class="detailed-view-container">
                <h2 class="semi-bold">{{pulldata.Name}}<span class="glyphicon glyphicon-remove pull-right" ng-click="visible=false"></span></h2>
                
                <!-- carousel -->
                <div class="carousel slide" id="detailed-view-carousel" data-interval="2000" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#detailed-view-carousel" data-slide-to="0" class="active"></li>
    <li data-target="#detailed-view-carousel" data-slide-to="1"></li>
    <li data-target="#detailed-view-carousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="img/slide1.png" alt="...">
    </div>
    <div class="item">
      <img src="img/slide1.png" alt="...">
    </div>
    <div class="item">
      <img src="img/slide1.png" alt="...">
    </div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#detailed-view-carousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#detailed-view-carousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>
            <!-- Price and add to plan -->
            <div class="row">
                <div class="col-md-6"><h2 class="semi-bold">{{pulldata.Price}}</h2></div>
                <div class="col-md-2"><div class="glyphicon glyphicon-star-empty star-empty-custom"></div>
                </div>
                <div class="col-md-4">
                    <div class="pull-right">
                        <button class="btn btn-primary-custom2"><span class="glyphicon glyphicon-plus"></span>&nbsp; Add to Plan
                        </button>
                    </div>
                </div>
            </div>
            
            <!-- table -->          
            <table class="table table-custom">
                <tr>
                    <td class="glyphicon glyphicon-picture glyphicon-custom"></td>
                    <td><h4 class="semi-bold">Media Type</h4></td>
                    <td><h4>{{pulldata.MediaType}}</h4></td>
                </tr>
                <tr>
                    <td></td>
                    <td><h4 class="semi-bold">Lighting</h4></td>
                    <td><h4>{{pulldata.Lighting}}</h4></td>
                </tr>
                <tr>
                    <td></td>
                    <td><h4 class="semi-bold">Width (ft)</h4></td>
                    <td><h4>{{pulldata.Width}} ft</h4></td>
                </tr>
                <tr>
                    <td></td>
                    <td><h4 class="semi-bold">Height (ft)</h4></td>
                    <td><h4>{{pulldata.Height}} ft</h4></td>
                </tr>
                <tr>
                    <td class="glyphicon glyphicon-picture glyphicon-custom"></td>
                    <td><h4 class="semi-bold">Availability</h4></td>
                    <td><h4>Contact Vendor</h4></td>
                </tr>
                <tr>
                    <td class="glyphicon glyphicon-picture glyphicon-custom"></td>
                    <td><h4 class="semi-bold">Address</h4></td>
                    <td><h4> {{pulldata.Address}}</h4></td>
                </tr>
                <tr>
                    <td class="glyphicon glyphicon-picture glyphicon-custom"></td>
                    <td><h4 class="semi-bold">Lat, Long</h4></td>
                    <td><h4>{{pulldata.Lat}}, {{pulldata.Lng}}</h4></td>
                </tr>
            </table>
    
              </div>
            </div>
        </div>
        
        <!-- /sitelist -->
        <div class="col-md-4" id="site-list-container" ng-controller = "SiteController as site">
 			
          <!-- sort -->
          <div id="sort">
            <span>Sort&nbsp;&nbsp;
                <div class="btn-group">
                    <button type="button" ng-click="order('Price',false)" class="btn btn-sm btn-secondary">Price</button>
                    <button type="button" ng-click="order('id',false)" class="btn btn-sm btn-secondary">Popularity</button>
             <!--        <button type="button" ng-click="order('name',false)" class="btn btn-sm btn-secondary">Most Recent</button> -->
				</div>
						  <input type="text" id="search" class="search-control" placeholder="List Search" ng-model="search" >
						  
             </span>
           </div>
            
           <!-- listing --> 
            
           <!-- hidden popover -->
            
           <div class="popover-content-custom">   
                <ul class="popover-mediaList">
                    <li><h4>media plan 1 Lorem Ipsum</h4></li>
                    <li><h4>media plan 2</h4></li>
                    <li><h4>media plan 3 Lorem</h4></li>
                    <li><h4><a class="primary-link"><span class="glyphicon glyphicon-plus"></span> Create New Plan</a></h4></li>
                </ul>
          </div>  
          <div class="popover-title-custom"><h4 class="semi-bold">Select a Plan</h4>
          </div> 
            
           <!-- start of list item -->
           <div class="list-group" id="site-list" infinite-scroll="GetMore()" infinite-scroll-distance="3" >


               <!-- angular test List -->
 				   <div data-ng-repeat="banner in product | filter:search" ng-animate="'animate'" >
 	                <a href="#"  ng-click="toggle()" class="list-group-item site-list-item">
	                    <div class="pull-right">
	                        <button class="glyphicon glyphicon-plus small-btn plus" data-container="body" data-toggle="popover" data-placement="left" data-content="">
	                        </button>
	                        <button class="glyphicon glyphicon-star-empty small-btn star"></button>
	                    </div>
                      
	                    <div class="pull-left">
	                        <img class="media-object" ng-src="{{banner.thumbnail}}" alt="" height="60px" width="90px">   
	                    </div>   
  
	                    <h4 class="list-group-item-heading">{{banner.name}}</h4>
	                    <p class="list-group-item-text">{{banner.type}} &nbsp; &nbsp; {{banner.width}} x {{banner.height}} ft2</p>
	                    <p class="list-group-item-text price">INR {{banner.price}} <span class="per-month">per mo</span></p>
	                </a>
 				</div>
 
 <div ng-show="busy">Loading data...</div>
               <!-- closed angular test List -->

               <!-- list item 
                <a href="#" class="list-group-item site-list-item">
                    <div class="pull-right">
                        <button class="glyphicon glyphicon-plus small-btn plus" data-container="body" data-toggle="popover" data-placement="left" data-content="">
                        </button>
                        <button class="glyphicon glyphicon-star-empty small-btn star"></button>
                    </div>
                      
                    <div class="pull-left">
                        <img class="media-object" src="img/billboard.png" alt="" height="60px" width="90px">   
                    </div>   
  
                    <h4 class="list-group-item-heading">Site Name Lorem Ipsum</h4>
                    <p class="list-group-item-text">Non-lit Billboard &nbsp; &nbsp; 2.5x4.5 ft2</p>
                    <p class="list-group-item-text price">INR 5000 <span class="per-month">per mo</span></p>
                </a>

				   <a href="#" class="list-group-item site-list-item">
                    <div class="pull-right">
                        <button class="glyphicon glyphicon-plus small-btn plus" data-container="body" data-toggle="popover" data-placement="left" data-content="">
                        </button>
                        <button class="glyphicon glyphicon-star-empty small-btn star"></button>
                    </div>
                      
                    <div class="pull-left">
                        <img class="media-object" src="img/billboard.png" alt="" height="60px" width="90px">   
                    </div>   
  
                    <h4 class="list-group-item-heading">Site Name Lorem Ipsum</h4>
                    <p class="list-group-item-text">Non-lit Billboard &nbsp; &nbsp; 2.5x4.5 ft2</p>
                    <p class="list-group-item-text price">INR 5000 <span class="per-month">per mo</span></p>
                </a>
               
               <a href="#" class="list-group-item site-list-item">
                    <div class="pull-right">
                        <button class="glyphicon glyphicon-plus small-btn plus" data-container="body" data-toggle="popover" data-placement="left" data-content="">
                        </button>
                        <button class="glyphicon glyphicon-star-empty small-btn star"></button>
                    </div>
                      
                    <div class="pull-left">
                        <img class="media-object" src="img/billboard.png" alt="" height="60px" width="90px">   
                    </div>   
  
                    <h4 class="list-group-item-heading">Site Name Lorem Ipsum</h4>
                    <p class="list-group-item-text">Non-lit Billboard &nbsp; &nbsp; 2.5x4.5 ft2</p>
                    <p class="list-group-item-text price">INR 5000 <span class="per-month">per mo</span></p>
                </a>
               
               <a href="#" class="list-group-item site-list-item">
                    <div class="pull-right">
                        <button class="glyphicon glyphicon-plus small-btn plus" data-container="body" data-toggle="popover" data-placement="left" data-content="">
                        </button>
                        <button class="glyphicon glyphicon-star-empty small-btn star"></button>
                    </div>
                      
                    <div class="pull-left">
                        <img class="media-object" src="img/billboard.png" alt="" height="60px" width="90px">   
                    </div>   
  
                    <h4 class="list-group-item-heading">Site Name Lorem Ipsum</h4>
                    <p class="list-group-item-text">Non-lit Billboard &nbsp; &nbsp; 2.5x4.5 ft2</p>
                    <p class="list-group-item-text price">INR 5000 <span class="per-month">per mo</span></p>
                </a>
               
               <a href="#" class="list-group-item site-list-item">
                    <div class="pull-right">
                        <button class="glyphicon glyphicon-plus small-btn plus" data-container="body" data-toggle="popover" data-placement="left" data-content="">
                        </button>
                        <button class="glyphicon glyphicon-star-empty small-btn star"></button>
                    </div>
                      
                    <div class="pull-left">
                        <img class="media-object" src="img/billboard.png" alt="" height="60px" width="90px">   
                    </div>   
  
                    <h4 class="list-group-item-heading">Site Name Lorem Ipsum</h4>
                    <p class="list-group-item-text">Non-lit Billboard &nbsp; &nbsp; 2.5x4.5 ft2</p>
                    <p class="list-group-item-text price">INR 5000 <span class="per-month">per mo</span></p>
                </a>
               
               <a href="#" class="list-group-item site-list-item">
                    <div class="pull-right">
                        <button class="glyphicon glyphicon-plus small-btn plus" data-container="body" data-toggle="popover" data-placement="left" data-content="">
                        </button>
                        <button class="glyphicon glyphicon-star-empty small-btn star"></button>
                    </div>
                      
                    <div class="pull-left">
                        <img class="media-object" src="img/billboard.png" alt="" height="60px" width="90px">   
                    </div>   
  
                    <h4 class="list-group-item-heading">Site Name Lorem Ipsum</h4>
                    <p class="list-group-item-text">Non-lit Billboard &nbsp; &nbsp; 2.5x4.5 ft2</p>
                    <p class="list-group-item-text price">INR 5000 <span class="per-month">per mo</span></p>
                </a>
               
               <a href="#" class="list-group-item site-list-item">
                    <div class="pull-right">
                        <button class="glyphicon glyphicon-plus small-btn plus" data-container="body" data-toggle="popover" data-placement="left" data-content="">
                        </button>
                        <button class="glyphicon glyphicon-star-empty small-btn star"></button>
                    </div>
                      
                    <div class="pull-left">
                        <img class="media-object" src="img/billboard.png" alt="" height="60px" width="90px">   
                    </div>   
  
                    <h4 class="list-group-item-heading">Site Name Lorem Ipsum</h4>
                    <p class="list-group-item-text">Non-lit Billboard &nbsp; &nbsp; 2.5x4.5 ft2</p>
                    <p class="list-group-item-text price">INR 5000 <span class="per-month">per mo</span></p>
                </a>
               
               <a href="#" class="list-group-item site-list-item">
                    <div class="pull-right">
                        <button class="glyphicon glyphicon-plus small-btn plus" data-container="body" data-toggle="popover" data-placement="left" data-content="">
                        </button>
                        <button class="glyphicon glyphicon-star-empty small-btn star"></button>
                    </div>
                      
                    <div class="pull-left">
                        <img class="media-object" src="img/billboard.png" alt="" height="60px" width="90px">   
                    </div>   
  
                    <h4 class="list-group-item-heading">Site Name Lorem Ipsum</h4>
                    <p class="list-group-item-text">Non-lit Billboard &nbsp; &nbsp; 2.5x4.5 ft2</p>
                    <p class="list-group-item-text price">INR 5000 <span class="per-month">per mo</span></p>
                </a>
               
               <a href="#" class="list-group-item site-list-item">
                    <div class="pull-right">
                        <button class="glyphicon glyphicon-plus small-btn plus" data-container="body" data-toggle="popover" data-placement="left" data-content="">
                        </button>
                        <button class="glyphicon glyphicon-star-empty small-btn star"></button>
                    </div>
                      
                    <div class="pull-left">
                        <img class="media-object" src="img/billboard.png" alt="" height="60px" width="90px">   
                    </div>   
  
                    <h4 class="list-group-item-heading">Site Name Lorem Ipsum</h4>
                    <p class="list-group-item-text">Non-lit Billboard &nbsp; &nbsp; 2.5x4.5 ft2</p>
                    <p class="list-group-item-text price">INR 5000 <span class="per-month">per mo</span></p>
                </a>
               
               
                      -->
                  
</div>
            
                
          </div>
       </div>
      </div>     
 

    </body>
</html>
