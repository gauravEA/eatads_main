<div class="navbar navbar-second navbar-static-top ">
    <div class="container">
        <div class="left">
            <h1>Upload File</h1>
        </div>
        <?php $this->widget('CreateAccountButton'); ?>
    </div>
</div>
<div class="container main-body">
    <div class="row">
        <div class="col-sm-9 left">
            <div class="clear"></div>
            <h2>Upload File</h2>
            <p>
                <form method="post" enctype="multipart/form-data">
                    <div id="queue"></div>
                    <input id="file_upload" name="file_upload" type="file" multiple="true">
<!--                    <a style="position: relative; top: 8px;" href="javascript:$('#file_upload').uploadifive('upload')">Upload Files</a>-->
                </form>    
<!--                <input type="text" id='uploadify_filenames' name="uploadify_filenames" />-->
            </p>
        </div>
    </div>
</div>

<!-- /container -->

<?php $this->widget('Uploadify', array('fileFieldId' => 'file_upload', 'imageType' => 'listing') ); ?>