<style>
    .site_marked_fav {
        background-color: #F58707;
        color: white;
        border: none;
    }
     .selectedSite {
                background-color: bisque;
            }
            
            #loading-image {
	background-color: #333;
	width: 100%;
	height: 100%;
	position: fixed;
	top: 0px;
	right: 0px;
	z-index: 9999;
	-moz-border-radius: 10px;
	-webkit-border-radius: 10px;
	border-radius: 10px; /* future proofing */
	-khtml-border-radius: 10px;
        opacity: 0.7;
}
</style>
<script>
    var icons, map, centerLatLng, legend, circle, mcOptions;
    var userid = 418;
    var companyid;
    var listingIdForPlan =0;
        var centerPointArr = [];
        var allMarkers = [];
        
     var listings = '';   
     var zoomLevel;
     var new_boundary;
    function fetchListings() {
        var chk = [];
        
       // console.log("this is the price slider " + $('.tooltip-inner').text());
        
        $("input:checkbox[name=type]:checked").each(function()
        {
           chk.push($(this).val());
        });
        
        var light = [];
        $("input:checkbox[name=lighting]:checked").each(function()
        {
           light.push($(this).val());
        });
        var filter = $("#filter_group .active").data("value");
        console.log('filter :' + filter + ' light : ' + light + ' mt: ' + chk );
        
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->urlManager->createUrl('ajax/getlisting/'); ?>',
            data:{'companyid': companyid,
                   'mediatypeid' : chk.join(","),
                   'lightingid': light.join(","),
                   'sort':filter,
                   'userid' : userid
//                   ,
//                   'priceslider':$('.tooltip-inner').text()
                    },
         success:function(data){
             //console.log("data " + data);
                var currList = JSON.parse(data);
                listings += JSON.parse(data);
                var len = JSON.parse(data);
                for (var i =0 ; i < len.length; i++) {
                    listings += len[i];
         //   console.log(listings);
                }    
                dust.render("listings", JSON.parse(data), function(err, out) {
               // console.log("this is test out from ajax11111 : " + out +"err "+ err);    
                $('#site-list').html(out);
                
//                $('#site-list  ').jscroll({
//                    loadingHtml: '<img src="loading.gif" alt="Loading" /> Loading...',
//                    padding: 20,
//                    nextSelector: 'a.jscroll-next:last',
//                    contentSelector: 'li'
//                });

//listings.SiteListing.push(currList);
             //   console.log(listings + " listings length" + JSON.parse(data).SiteListing.length );
                });
                  //$('#site-list  ').jscroll();
                  
                  $('#site-list').scroll(function()
{
    if($('#site-list').scrollTop() == $(document).height() - $('#site-list').height())
    {
        //$('div#loadmoreajaxloader').show();
        console.log('this is is tst');
        $.ajax({
        url: "loadmore.php",
        success: function(html)
        {
            if(html)
            {
                $("#postswrapper").append(html);
                $('div#loadmoreajaxloader').hide();
            }else
            {
                $('div#loadmoreajaxloader').html('<center>No more posts to show.</center>');
            }
        }
        });
    }
});
                $(".plus").popover({
                    html : true, 
                    content: function() {
                      return $(".popover-content-custom").html();
                    },
                    title: function() {
                      return $(".popover-title-custom").html();
                    }
                });
                
            },
            error: function(data) { // if error occured
                  alert("Error occured.please try again");
                  alert(data);
             }
           });
           
           

           
    }
    
    $(document).ready(function() {
        jQuery('#loading-image').show();
        vendorDetails();
        showPlans();

    });
  
  function getmarkers(lat, lng) {
      var chk = [];
        
       // console.log("this is the price slider " + $('.tooltip-inner').text());
        
        $("input:checkbox[name=type]:checked").each(function()
        {
           chk.push($(this).val());
        });
        
        var light = [];
        $("input:checkbox[name=lighting]:checked").each(function()
        {
           light.push($(this).val());
        });
        var filter = $("#filter_group .active").data("value");
                 $.ajax({
            type: 'POST',
             url: '<?php echo Yii::app()->urlManager->createUrl('ajax/getmarkers/'); ?>',
            data:{'companyid': companyid,
                   'mediatypeid' : chk.join(","),
                   'lightingid': light.join(","),
                   'sort':filter,
                   'Lat': lat,
                   'Lng': lng
                    },
         success:function(data){
             //console.log("data " + data);
             //jQuery('#loading-image').hide();
        var lat = [];
        var lng = [];
        var arr = JSON.parse(data);
        for (var i =0;i < arr.length; i++) {
            lat.push(arr[i].lat);
            lng.push(arr[i].lng);
        }
        $('#dp').html(data);
         
        new_boundary = new google.maps.LatLngBounds();
       map = new google.maps.Map(document.getElementById('map_canvas'));
       // map.setZoom(7);      // This will trigger a zoom_changed on the map
        
               
        map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
        if (lat === null && lng === null) {
                   console.log("max long " + Math.max.apply(Math, lat) + " max long: " + Math.max.apply(Math, lng))
        var generallat = (Math.max.apply(Math, lat) + Math.min.apply(Math, lat))/2;
        var generallng = (Math.max.apply(Math, lng) + Math.min.apply(Math, lng))/2;
        $('#geo_latlng').text(generallat + ',' + generallng);
        map.setCenter(new google.maps.LatLng(generallat, generallng));
        } else {
            map.setCenter(new google.maps.LatLng(lat, lng));
        }    
 
        
        
        

        
        

        
        setMarker();
        
                
        for(index in allMarkers){
            position = allMarkers[index].position;
            new_boundary.extend(position);
          }

          map.fitBounds(new_boundary);
          jQuery('#loading-image').fadeOut();
            },
            error: function(data) { // if error occured
                  alert("Error occured.please try again");
                  alert(data);
             }
           }); 
    }   
  
  function vendorDetails() {
   $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->urlManager->createUrl('ajax/vendordetails'); ?>',
            data:{
                  'alias' : $('#alias').text()  },
         success:function(data){
             var json = JSON.parse(data);
             console.log("adasdasdaa" + json.Companylogo);
             companyid = json.id;
             document.title = json.Vendorname;
             $('#image').html('<img class="vendor-logo" src="' + json.Companylogo + '">');
             $('#vendorName').text(json.Vendorname);
             $('#vendorphone').text(json.Contact.Phonenumber);
            console.log("vendorname" + $('#image').html());
            fetchListings();
            //showMediaTypes();
            getmarkers(null,null);
            },
            error: function(data) { // if error occured
                  alert("Error occured.please try again");
                  alert(data);
             }
           });

           
   }
   
  
   
  $( document ).on( "click", ".plus", function() {
        $( this ).popover( "toggle" );      
        return false;
  });
  $( document ).on( "click", ".checkboxMedia", function() {
     fetchListings();
  });
                
    function showMediaTypes() {
        $.ajax({
            type: 'POST',
             url: '<?php echo Yii::app()->urlManager->createUrl('ajax/getmediatypes'); ?>',
            data:{
                  'companyid' : companyid  },
         success:function(data){
               dust.render("mediatypes", JSON.parse(data), function(err, out) {
                    $('#mediatypes').html(out);
                });
                
            },
            error: function(data) { // if error occured
                  alert("Error occured.please try again");
                  alert(data);
             }
           });
    }    
    
    function showPlans() {
    $.ajax({
            type: 'POST',
             url: '<?php echo Yii::app()->urlManager->createUrl('ajax/retriveplan'); ?>',
            data:{
                  'Userid' : userid  },
         success:function(data){
                dust.render("myplans", JSON.parse(data), function(err, out) {
                    $('#myplans_id').html(out);
                });
                dust.render("popover_plans", JSON.parse(data), function(err, out) {
                    $('#popover_plans').html(out);
                });
                
            },
            error: function(data) { // if error occured
                  alert("Error occured.please try again");
                  alert(data);
             }
           });
    }
    
    function showDetails(id) {

            $('.list-group-item').removeClass('selectedSite');
            $('#modalImg_' + id).addClass('selectedSite');
      $.ajax({
            type: 'POST',
             url: '<?php echo Yii::app()->urlManager->createUrl('ajax/getsitedetails/'); ?>',
            data:{'siteid': id,
                  'userid':userid  },
         success:function(data){
                dust.render("site_detail_view", JSON.parse(data), function(err, out) {
                    $('#detailed-view').html(out);
                    $("#detailed-view").show(); 
                });
        selectedLat = JSON.parse(data).Lat;
        selectedLng = JSON.parse(data).Lng;
        getmarkers(selectedLat, selectedLng);
//        map = new google.maps.Map(document.getElementById('map_canvas'));
//        map.setZoom(8);      // This will trigger a zoom_changed on the map
//        map.setCenter(new google.maps.LatLng(JSON.parse(data).Lat, JSON.parse(data).Lng));
//        map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
//        setMarker();
//        offsetCenter(map.getCenter(),-200,-100);
            },
            error: function(data) { // if error occured
                  alert("Error occured.please try again");
                  alert(data);
             }
           });
    }
    
    function offsetCenter(latlng,offsetx,offsety) {

// latlng is the apparent centre-point
// offsetx is the distance you want that point to move to the right, in pixels
// offsety is the distance you want that point to move upwards, in pixels
// offset can be negative
// offsetx and offsety are both optional

var scale = Math.pow(2, map.getZoom());
var nw = new google.maps.LatLng(
    map.getBounds().getNorthEast().lat(),
    map.getBounds().getSouthWest().lng()
);

var worldCoordinateCenter = map.getProjection().fromLatLngToPoint(latlng);
var pixelOffset = new google.maps.Point((offsetx/scale) || 0,(offsety/scale) ||0)

var worldCoordinateNewCenter = new google.maps.Point(
    worldCoordinateCenter.x - pixelOffset.x,
    worldCoordinateCenter.y + pixelOffset.y
);

var newCenter = map.getProjection().fromPointToLatLng(worldCoordinateNewCenter);

map.setCenter(newCenter);

}


    function addtoPlan(planid) {
        $.ajax({
            type: 'POST',
             url: '<?php echo Yii::app()->urlManager->createUrl('ajax/addinexistingplan/'); ?>',
            data:{'listingid': listingIdForPlan,
                  'userid':userid,
                   'planid':planid },
         success:function(data){
                alert(data);
            },
            error: function(data) { // if error occured
                  alert("Error occured.please try again");
                  alert(data);
             }
           });
    }
        
     
    function addNewPlan(e,planname) {
    if (e.keyCode == 13) {
        $.ajax({
            type: 'POST',
             url: '<?php echo Yii::app()->urlManager->createUrl('ajax/addplan/'); ?>',
            data:{'listingid': listingIdForPlan,
                  'userid':userid,
                   'planname':planname },
         success:function(data){
                alert(data);
                showPlans();
            },
            error: function(data) { // if error occured
                  alert("Error occured.please try again");
                  alert(data);
             }
           });
    }
        
    }
    
    function setlisting(id) {
    listingIdForPlan = id;
    }
    
    function redirect(name) {
     window.location.replace("<?php echo Yii::app()->urlManager->createUrl('myplans/'); ?>" + '/' + name);
    }
    function setfav(id) {
    
                //var num = parseInt($('#favaddedNum').html());
                if ($('#fav_' + id).hasClass('site_marked_fav')) {
                    $('#fav_' + id).removeClass('site_marked_fav');
                } else  {
                    $('#fav_' + id).addClass('site_marked_fav');
                }
                
        $.ajax({
            type: 'POST',
             url: '<?php echo Yii::app()->urlManager->createUrl('ajax/addfavorite/'); ?>',
            data:{'siteid': id,
                'userid': userid},
         success:function(data){
                //alert(data);
            },
            error: function(data) { // if error occured
                  alert("Error occured.please try again");
                  alert(data);
             }
           });
        
                
            }
    
    
    /*
    * Map functions begin

     * @returns {undefined}     */
    
function initialize() {
        var map_canvas = document.getElementById('map_canvas');
        var map_options = {
          center: new google.maps.LatLng(22,77),
          zoom: 4,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
            icons = {
                exact: {
                    name: 'Exact location',
//                    //icon: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/exact.png',
//                    icon: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/exact_new.png',
                    icon: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/markerred2.png',
                    legend: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/exact_legend.png'
                },
                approx: {
                    name: 'Approximate location',
                    //icon: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/approx.png',
                    //icon: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/approx_new.png',
                    icon: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/markerblue_new.png',
                    legend: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/exact_legend.png'
                }
            };   
        
//        legend = document.getElementById('legend');                      
//            for (var key in icons) {
//                var type = icons[key];
//                var name = type.name;
//                var icon = type.icon;
//                var div = document.createElement('div');
//                div.innerHTML = '<img src="' + icon + '"> ' + name;
//                legend.appendChild(div);
//            }
        
        map = new google.maps.Map(map_canvas, map_options)
      }
      google.maps.event.addDomListener(window, 'load', initialize);
        
        function clearOverlays() {
            //console.log('clear overlays called');
            //console.log(allMarkers);
            if(allMarkers.length) {
                for (var i = 0; i < allMarkers.length; i++) {
                    allMarkers[i].setMap(null);
                }
                allMarkers.length = 0;
            }
        }
            function setMarker() {
            //console.log('setmarker called');
            console.time("setmarker");
            clearOverlays();
            /*if (typeof markerClusterer === 'object') {  
                // console.log('clear markers called');
                markerClusterer.clearMarkers();
            }*/
        
            //clearClusterMarkers();
            // draw circle 
            var geo_latlng = $('#geo_latlng').val();
            var geo_latlng_split = geo_latlng.split(",");
            //console.log(typeof google);
            //console.log(typeof google.maps);           
            myLatLng = new google.maps.LatLng(geo_latlng_split[0], geo_latlng_split[1]);
            console.log("##1 draw circle");
            //drawCircle(myLatLng);
            
            // get markers from div "dp", and converto array              
            mapData = JSON.parse($('#dp').html());
            mapLoc = mapData;            
            var type;
            
            for(var key in mapLoc) {
            //console.log(key);    
                                
                if(mapLoc[key].lat != '0.000000' && mapLoc[key].lng != '0.000000') {
                    
                    var myLanLat = new google.maps.LatLng(mapLoc[key].lat, mapLoc[key].lng);
                    if(mapLoc[key].ea==1) {                    
                        type = icons['exact']['icon'];                        
                    } else {                        
                        type = icons['approx']['icon'];                        
                    }
                    //console.log(mapLoc[key].id +" - " +mapLoc[key].ea);
                    
                    // Draw marker
                    var marker = new google.maps.Marker({
                        position: myLanLat,
                        icon: type,
                        map: map
                    });
                    marker.setValues({id: mapLoc[key].id});
                    
                    allMarkers.push(marker);
                    // info window
                    var content = ''; // $('#map_content_' + counterVar).html();
                    
                    google.maps.event.addListener(marker, 'click', (function(marker, content) {                        
                        return function() {
                            marker.setIcon('<?php echo Yii::app()->getBaseUrl(); ?>/images/site/approx.png');
                            showDetails(marker.id);
                        };
                    })(marker, content));
                    
                }                
            }

            console.timeEnd("setmarker");
        }
    
    
    
//    $('#site-list').infinitescroll({
//  // other options
//  dataType: 'json',
//  appendCallback: false
//}, function(json, opts) {
//  // Get current page
//  var page = opts.state.currPage;
//  // Do something with JSON data, create DOM elements, etc ..
//  
//  console.log("thisis the first page of  teh : :: " + page);
//});



//$('#site-list').scroll(function()
//{
//    if($('#site-list').scrollTop() == $(document).height() - $(window).height())
//    {
//        //$('div#loadmoreajaxloader').show();
//        console.log('this is text');
//        $.ajax({
//        url: "loadmore.php",
//        success: function(html)
//        {
//            if(html)
//            {
//                $("#postswrapper").append(html);
//                $('div#loadmoreajaxloader').hide();
//            }else
//            {
//                $('div#loadmoreajaxloader').html('<center>No more posts to show.</center>');
//            }
//        }
//        });
//    }
//});
    
</script>

<div id="alias" style="display: none"><?php echo $this->myvar ?> </div>
<div id="geo_latlng" style="display: none"></div>
<!-- contact-modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header modal-header-custom">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h2 class="modal-title semi-bold" id="myModalLabel">Write to <span id="vendorName"></span> </h2>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6"><span class="glyphicon glyphicon-envelope">&nbsp;</span><span class="semi-bold">Email: </span><span id="vendorEmail">sales@eatads.com</span></div>
            <div class="col-md-6">
                <div class="pull-right">
                    <span class="glyphicon glyphicon-earphone">&nbsp;</span><span class="semi-bold">Phone: </span><span id="vendorphone">+91 011-238445</span>
                </div>
            </div>
        </div>
        <br>
        <textarea class="form-control" rows="4"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary2" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary-custom1">Send</button>
      </div>
    </div>
  </div>
</div>



<div id="loading-image">
    <img style="display: block;margin-left: 400px;margin-top: 200px;" src='<?php echo Yii::app()->getBaseUrl(); ?>/images/site/override.gif' alt="Loading..." />
</div>

    <!-- /content -->
      <div class="row" id="content">
        
          <div style="display: none" id="geo_latlng"><?php echo Yii::app()->params['map_latlng']; ?></div>
        <!-- /map -->
        <div class="col-md-8 col-sm-8" id="map">
            <div id="map_canvas"></div>
            <div id="legend"></div>
            
            <!-- detailed-view -->
            <div id="detailed-view">
             
            </div>
        </div>
        <div id="dp" style="display: none"></div>
        <!-- /sitelist -->
        <div class="col-md-4 col-sm-4" id="site-list-container">
            
            <!-- sort -->
            <div id="sort">
              <span>Sort&nbsp;&nbsp;
                  <div class="btn-group" id="filter_group">
                      <button type="button" class="btn btn-sm btn-secondary" data-value="Price">Price</button>
                      <button type="button" class="btn btn-sm btn-secondary" data-value="Popularity">Popularity</button>
                      <button type="button" class="btn btn-sm btn-secondary" data-value="Most Recent">Most Recent</button>
                  </div>
               </span>
             </div>
           <!-- hidden popover -->
            
                <div class="popover-content-custom">   
                    <ul class="popover-mediaList">
                        <span id="popover_plans"> 
                        </span>
                        
                        <li><h4>
                                <a class="primary-link" ><span class="glyphicon glyphicon-plus"></span> Create New Plan</a>
                                <input type="text" class="addnewplan" id="planNameid" onkeypress="addNewPlan(event, $(this).val());">    
                            </h4></li>
                    </ul>
                </div>  
                <div class="popover-title-custom"><h4 class="semi-bold">Select a Plan</h4>
                </div> 
            
           <!-- start of list item -->
           <div class="list-group" id="site-list" style="overflow-y: scroll;height: 100%">
            </div>
            
                
          </div>
       </div>
  