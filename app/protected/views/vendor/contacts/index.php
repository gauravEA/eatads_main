<?php
    $theme = Yii::app()->theme;
    $cs = Yii::app()->clientScript;
    $cs->registerCssFile( $theme->getBaseUrl() . '/css/jquery.handsontable.full.css' );
    $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/jquery.handsontable.full.js',CClientScript::POS_END);
?>
<script>
    
   var maxed = false
  , resizeTimeout
  , availableWidth
  , availableHeight
  , $window = $(window);

    



var data = [
              ['Email', 'Name', 'Company']        
            ];
            
            var prefilleddata = [
              ['Email', 'Name', 'Company', 'Delete Flag']        
            ];
            var $container = $("#uploadContacts");
$(document).ready(function() {
        vendorDetails();
    });
 var companyid;   

 function vendorContacts() {
    $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->urlManager->createUrl('ajax/vendorcontacts'); ?>',
            data:{
                  'companyid' : companyid  },
         success:function(data1){
             var json = JSON.parse(data1);
             console.log("adasdasdaa" + json);
            for (var i =0 ; i < json.length; i++) {
                var temp = [];
                temp[0] = json[i].email;
                temp[1] = json[i].name;
                temp[2] = json[i].company;
                temp[3] = false;
                prefilleddata.push(temp);
            }    
            console.log(JSON.stringify(prefilleddata) + " fdsfsd s ");
            
             $('#availableContacts').handsontable({
              data: json,
              colHeaders: ['Email', 'Name', 'Company', 'Delete Flag'],
              rowHeaders: true,
              colWidths: [100, 100, 100],
              manualColumnResize: true,
              manualRowResize: true,
              minSpareRows: 1,
              columns: [{
                      data : "email"
              },{
                  data: "name"
              },{
                  data: "company"
              },{
                  data: "deleteFlag",
                  type: "checkbox",
                  name: "id"
              }],
              cells: function (row, col, prop) {
                  var cellProperties = {};
                  //console.log(data.length + " length ");
                  var flag= false;
                  for ( var i=1; i < prefilleddata.length; i++) {
                      if (prefilleddata[i][0]) {
                          flag = true;
                          break;  
                       }
                  }    
                  if (flag) {
                      if (col === 0) {
                        cellProperties.readOnly = true; //make cell read-only if it is first row or the text reads 'readOnly'
                      }
                  }    
                  return cellProperties;
              }   
            });
            },
            error: function(data) { // if error occured
                  alert("Error occured.please try again");
                  alert(data);
             }
           });
 }
function vendorDetails() {
   $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->urlManager->createUrl('ajax/vendordetails'); ?>',
            data:{
                  'alias' : $('#alias').text()  },
         success:function(data){
             var json = JSON.parse(data);
             //console.log("adasdasdaa" + json.Companylogo);
             companyid = json.id;
             document.title = json.Vendorname;
             $('#image').html('<img class="vendor-logo" src="' + json.Companylogo + '">');
             $('#vendorName').text(json.Vendorname);
             $('#vendorphone').text(json.Contact.Phonenumber);
            console.log("vendorname" + $('#image').html());
            vendorContacts();
            },
            error: function(data) { // if error occured
                  alert("Error occured.please try again");
                  alert(data);
             }
           });

           
   }
   
   
    
</script>
<div id="alias" style="display: none"><?php echo $this->myvar ?> </div>
<div class="row">
        <div class="col-md-12">
            <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                Also <a>Upload Your Contacts</a> before you can share availability with them.
            </div>
        </div>
    </div>
    <div class="container-fluid" id="availability-list">
        <div class="row">
            <div class="col-md-12">
                <h3 class="pull-left list-heading">Availability List</h3>
                <form class="pull-right">
                    <label>
                        <input type="checkbox">&nbsp;Send automatically at 11 AM local time&nbsp;&nbsp;
                    </label>
                    <button class="btn btn-primary" id="send-availability">Send Availibility to Contacts</button>
                </form>
            </div>
        </div>
        <div class="row" id="availability-list-table">
            <div class="col-md-12">
                <div class="table-container" id="uploadContacts">
                   
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid table-action">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    <a href="#" class="disabled">Cancel&nbsp;</a>
                    <button type="button" class="btn btn-primary" name="update" >Save</button>
                </div>
            </div>
        </div>
     </div>
</body>
    

<script>



             var $container = $("#uploadContacts");
            var handsontable = $container.data('handsontable');
            $('#uploadContacts').handsontable({
              data: data,
              colHeaders: true,
              rowHeaders: true,
              colWidths: [100, 100, 100],
              manualColumnResize: true,
              manualRowResize: true,
              minSpareRows: 10

            });
            $('#uploadContacts table').addClass('table');
            
            var errorEntires= [];
            var validEntries = [];
            var cnt =1;
            var arr = [];

            $('body').on('click', 'button[name=update]', function () {
                var dump = $(this).data('dump');
                var $container = $(dump);
               //  console.log('data of ' + dump, $container.handsontable('getData'));
               arr = $container.handsontable('getData');
               console.log(JSON.stringify(arr));
                $.ajax({
                        type: 'POST',
                         url: '<?php echo Yii::app()->urlManager->createUrl('ajax/updatevendorcontacts/'); ?>',
                        data:{
                              'data':JSON.stringify(arr)
                                },
                     success:function(data){
                            alert(data);
                            //handsontable.loadData(data);
                        },
                        error: function(data) { // if error occured
                              alert("Error occured.please try again");
                              alert(data);
                         }
                       });
           });  

            $('body').on('click', 'button[name=push]', function () {
             $.ajax({
                        type: 'GET',
                         url: '<?php echo Yii::app()->urlManager->createUrl('ajax/SendAvailabilityMailer/'); ?>',
                        data:{'vid': companyid
                                },
                     success:function(data){
                            alert(data);
                            //handsontable.loadData(data);
                        },
                        error: function(data) { // if error occured
                              alert("Error occured.please try again");
                              alert(data);
                         }
                       });
                
            });
            
            
            $('body').on('click', 'button[name=dump]', function () {
                var dump = $(this).data('dump');
                var $container = $(dump);
               //  console.log('data of ' + dump, $container.handsontable('getData'));
               arr = $container.handsontable('getData');
              // console.log(JSON.stringify(arr));     
               for (var i = 1; i < arr.length; i++) {
                   cnt ++;
                   if (arr[i][0]) {
                       //check email
                       var test = validateEmail(arr[i][0]);
                       if(test) {
                         validEntries.push(arr[i]);
                       } else {
                         errorEntires.push(arr[i]);
                         data.push(arr[i]);
                       }
                   }
               }
               uploadContacts();
               console.log("valid " + JSON.stringify(validEntries) + " invalid " + errorEntires);
            });
            
            function uploadContacts() {
                if (arr.length === cnt) {
                   // cnt = 1;
                    $.ajax({
                        type: 'POST',
                         url: '<?php echo Yii::app()->urlManager->createUrl('ajax/uploadcontacts/'); ?>',
                        data:{'companyid': companyid,
                              'data':JSON.stringify(validEntries)
                                },
                     success:function(data){
                            alert(data);
                            //handsontable.loadData(data);
                        },
                        error: function(data) { // if error occured
                              alert("Error occured.please try again");
                              alert(data);
                         }
                       });
                }
                 //handsontable.loadData(data);
            }
            
            function validateEmail(email) { 
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            } 
</script>