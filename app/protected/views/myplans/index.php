<style>
    .site_marked_fav {
        background-color: #F58707;
        color: white;
        border: none;
    }
    
</style>
<script>
    
    var icons, map, centerLatLng, legend, circle, mcOptions;
    var userid = 418;
    var companyid;
    var listingIdForPlan =0;
        var centerPointArr = [];
        var allMarkers = [];
        
    function  fetchPlanListings() {
        $.ajax({
            type: 'POST',
             url: '<?php echo Yii::app()->urlManager->createUrl('ajax/plandetail/'); ?>',
            data:{
                   'userid' : userid,
                   'plan' : $('#plan').text()
            },
         success:function(data){
             //console.log("data " + data);
             
             var json = JSON.parse(data);
                dust.render("listings", json, function(err, out) {
               // console.log("this is test out from ajax11111 : " + out +"err "+ err);    
                $('#site-list').html(out);
                });
                
                 $('#dp').html(data);
                 $('#planid').text(json.id);
                 $('#selectedplan').html(' ' + json.name + '<b class="caret"></b>');
                 $('#totalsites').text(' ' +json.TotalSites);
                 $('#totalPrice').text(' ' +json.TotalPrice);
                 
                 dust.render("myplans", JSON.parse(data), function(err, out) {
                    $('#myplans_id').html(out);
                });
                setMarker();
                
            },
            error: function(data) { // if error occured
                  alert("Error occured.please try again");
                  alert(data);
             }
           });
    }   
        
        
      function redirect(name) {
     window.location.replace("<?php echo Yii::app()->urlManager->createUrl('myplans/'); ?>" + '/' + name);
    }  

    
    $(document).ready(function() {

        fetchPlanListings();
    });
  


    
    function showDetails(id) {
      $.ajax({
            type: 'POST',
             url: '<?php echo Yii::app()->urlManager->createUrl('ajax/getsitedetails/'); ?>',
            data:{'siteid': id,
                  'userid':userid  },
         success:function(data){
                dust.render("site_detail_view", JSON.parse(data), function(err, out) {
                    $('#detailed-view').html(out);
                    $("#detailed-view").show(); 
                });
            },
            error: function(data) { // if error occured
                  alert("Error occured.please try again");
                  alert(data);
             }
           });
    }
    function removeFromPlan(planid) {
        $.ajax({
            type: 'POST',
             url: '<?php echo Yii::app()->urlManager->createUrl('ajax/deleteplanlisting/'); ?>',
            data:{'listingid': listingIdForPlan,
                   'planid':planid },
         success:function(data){
                if (data == '1') {
                alert('Listing deleted successfully');    
                fetchPlanListings();
                }    
                
            },
            error: function(data) { // if error occured
                  alert("Error occured.please try again");
                  alert(data);
             }
           });
    }
        
     
    
    
    function setlisting(id) {
    listingIdForPlan = id;
    removeFromPlan($('#planid').text());
    }

    
    
    /*
    * Map functions begin

     * @returns {undefined}     */
    
function initialize() {
        var map_canvas = document.getElementById('map_canvas');
        var map_options = {
          center: new google.maps.LatLng(20,77),
          zoom: 4,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
            icons = {
                exact: {
                    name: 'Exact location',
                    //icon: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/exact.png',
                    //icon: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/exact_new.png',
                    icon: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/markerred2.png',
                    //legend: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/exact_legend.png'
                },
                approx: {
                    name: 'Approximate location',
                    //icon: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/approx.png',
                    //icon: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/approx_new.png',
                    icon: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/markerblue2.png',
                    //legend: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/approx_legend.png'
                }
            };   
        
       
        
        map = new google.maps.Map(map_canvas, map_options)
      }
      google.maps.event.addDomListener(window, 'load', initialize);
        
        function clearOverlays() {
            //console.log('clear overlays called');
            //console.log(allMarkers);
            if(allMarkers.length) {
                for (var i = 0; i < allMarkers.length; i++) {
                    allMarkers[i].setMap(null);
                }
                allMarkers.length = 0;
            }
        }
            function setMarker() {
            //console.log('setmarker called');
            console.time("setmarker");
            clearOverlays();
            /*if (typeof markerClusterer === 'object') {  
                // console.log('clear markers called');
                markerClusterer.clearMarkers();
            }*/
        
            //clearClusterMarkers();
            // draw circle 
            var geo_latlng = $('#geo_latlng').val();
            var geo_latlng_split = geo_latlng.split(",");
            //console.log(typeof google);
            //console.log(typeof google.maps);           
            myLatLng = new google.maps.LatLng(geo_latlng_split[0], geo_latlng_split[1]);
            console.log("##1 draw circle");
            //drawCircle(myLatLng);
            
            // get markers from div "dp", and converto array              
            mapData = JSON.parse($('#dp').html());
            mapLoc = mapData.Markerslist;            
//            console.log('setMarker() markers found - ' + mapData.response.numFound);
            //console.log('setMarker() markers loaded - ' + mapData.responseHeader.params.rows);
            //$('#result_count').html(mapData.response.numFound);            
            
         //   marker_count = mapData.response.numFound;            
            
            //console.time('marker render time');
            var type;
            var infowindow = new google.maps.InfoWindow();            
            for(var key in mapLoc) {
            //console.log(key);    
                                
                if(mapLoc[key].lat != '0.000000' && mapLoc[key].lng != '0.000000') {
                    
                    var myLanLat = new google.maps.LatLng(mapLoc[key].lat, mapLoc[key].lng);
                    if(mapLoc[key].ea==1) {                    
                        type = icons['exact']['icon'];                        
                    } else {                        
                        type = icons['approx']['icon'];                        
                    }
                    //console.log(mapLoc[key].id +" - " +mapLoc[key].ea);
                    
                    // Draw marker
                    var marker = new google.maps.Marker({
                        position: myLanLat,
                        icon: type,
                        map: map
                    });
                    marker.setValues({id: mapLoc[key].id});
                    
                    allMarkers.push(marker);
                    // info window
                    var content = ''; // $('#map_content_' + counterVar).html();
                    google.maps.event.addListener(marker, 'click', (function(marker, content, infowindow) {                        
                        return function() {
                            
                            showDetails(marker.id);
                            //infowindow.setContent(content);
//                            infowindow.open(map, marker);                            
//                            $.ajax({
//                                type: "GET",
//                                url: "<?php echo Yii::app()->urlManager->createUrl('ajax/getmapinfosolr'); ?>",                                
//                                data: {
//                                    id: marker.id
//                                },
//                                beforeSend: function() {
//                                    infowindow.setContent('<img src="<?php echo Yii::app()->getBaseUrl(); ?>/images/site/ajax-loader.gif" />');
//                                },
//                                success:function (data) {
//                                    infowindow.close();//hide the infowindow
//                                    infowindow.setContent(data);
//                                    infowindow.open(map, marker);
//                                }
//                            });
                        };
                    })(marker, content, infowindow));
                    //counterVar++;                    
                }                
            }
            //console.timeEnd('marker render time');
            //map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);                        
            //markerClusterer.addMarkers(allMarkers);
                        
            //var mc = new MarkerClusterer(map);            
            //fetchBreadCrumb();
            
            //$('#div_overlay').hide();
            //console.log('div_overlay hide');
            console.timeEnd("setmarker");
        }
    
    
</script>

<div id="plan" style="display: none"><?php echo $this->plan ?> </div>
<div id="planid" style="display: none"></div>



    <!-- /content -->
      <div class="row" id="content">
        
          <div style="display: none" id="geo_latlng"><?php echo Yii::app()->params['map_latlng']; ?></div>
        <!-- /map -->
        <div class="col-md-8 col-sm-8" id="map">
            <div id="map_canvas"></div>
            <div id="legend"></div>
            
            <!-- detailed-view -->
            <div id="detailed-view">
             
            </div>
        </div>
        <div id="dp" style="display: none"></div>
        <!-- /sitelist -->
        <div class="col-md-4 col-sm-4" id="site-list-container">
            
            <!-- sort -->
            <div id="sort">
              <span>Sort&nbsp;&nbsp;
                  <div class="btn-group" id="filter_group">
                      <button type="button" class="btn btn-sm btn-secondary" data-value="Price">Price</button>
                      <button type="button" class="btn btn-sm btn-secondary" data-value="Popularity">Popularity</button>
                      <button type="button" class="btn btn-sm btn-secondary" data-value="Most Recent">Most Recent</button>
                  </div>
               </span>
             </div>
            
           <!-- start of list item -->
           <div class="list-group" id="site-list">
            </div>
            
                
          </div>
       </div>
  