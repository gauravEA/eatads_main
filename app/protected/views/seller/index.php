<div class="navbar navbar-second navbar-static-top ">
    <div class="container">
        <div class="left">
            <h1><?php echo $companyData->name; ?></h1>
        </div>
        <?php $this->widget('CreateAccountButton'); ?>
    </div>
</div>
<div class="container main-body views">
    <div class="content_header">
        <?php 
            $logoPath = JoyUtilities::getAwsFileUrl($companyData->logo, 'companylogo'); 
            echo strlen($logoPath) ? '<img src="'. $logoPath .'" alt="logo" />' : '';
        ?>
        
        <!--<img src="<?php echo Yii::app()->baseUrl ; ?>/images/site/dino.png"/>-->
        <!--<img src="<?php // echo Yii::app()->baseUrl ; ?>/images/site/gold.png"/>
        <img src="<?php // echo Yii::app()->baseUrl ; ?>/images/site/verified.png"/>-->
        
        <!-- AddThis Button BEGIN -->
        <!-- Change icons from https://www.addthis.com/get/sharing?flag=registered&lb=1 -->
        <div style="float:right;" class="addthis_toolbox addthis_default_style addthis_32x32_style">
            <!--<a class="addthis_button_pinterest_share"></a>-->
            <a class="addthis_button_google_plusone_share"><img src="<?php echo Yii::app()->baseUrl?>/images/site/google+-32.png" alt="google_plus" /></a>
            <a class="addthis_button_linkedin"><img src="<?php echo Yii::app()->baseUrl?>/images/site/linkedin-32.png" alt="linkedin" /></a>
            <a class="addthis_button_facebook"><img src="<?php echo Yii::app()->baseUrl?>/images/site/facebook-32.png" alt="facebook" /></a>
            <a class="addthis_button_twitter"><img src="<?php echo Yii::app()->baseUrl?>/images/site/twitter-32.png" alt="twitter" /></a>
            <a class="addthis_button_email"><img src="<?php echo Yii::app()->baseUrl?>/images/site/email-32.png" alt="email" /></a>
            <!--<a class="addthis_button_compact"></a>-->
            <!--<a class="addthis_counter addthis_bubble_style"></a>-->
        </div>
        <script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52a56e4a5b6d8a9d"></script>
        <!-- AddThis Button END -->
        
    </div>
    
    <div class="row mosaic left company">

        <h2>About Us</h2>
        <p><?php echo $companyData->description; ?></p>
        
        
        <?php if($dataProvider->totalItemCount) { ?>
        <div class="viewNavigation">
            <ul>
                <?php 
                    $listClass = '';
                    $mosClass = '';
                    $mapClass = '';
                    if($listType=='') {
                        $listClass = 'class="active"';
                    } else if ($listType=='m') {
                        $mosClass = 'class="active"';
                    } else if($listType=='p') {
                        $mapClass = 'class="active"';
                    } 
                    echo '<li '.$listClass.'>'.CHtml::link("List view",  '#', array('class'=>'list updatelisting', 'id'=>'listlink')).'</li>';
                    echo '<li '.$mosClass.'>'.CHtml::link("Mosaic view",  '#', array('class'=>'mosaic updatelisting', 'id'=>'moslink')).'</li>';
                    //echo '<li '.$mapClass.'>'.CHtml::link("Map view", '#', array('class'=>'map-view updatelisting', 'id'=>'maplink')).'</li>';
                    //echo '<li '.$mapClass.'>'.CHtml::link("Map view",  Yii::app()->urlManager->createUrl('listing/index', array_merge($_GET, array('lt'=>'p'))), array('class'=>'map-view')).'</li>';
                ?>
            </ul>
        </div>
        <div class="hr"></div>
        <div class="col-sm-9 left">
            <div class="order_by">
                <!-- DROP DOWN FILTER - AJAX FILTER FOR LISTING -->
                <div class="form">
                    <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'list-filter-dropdown',
                            'enableAjaxValidation'=>true,
                        ));
                        echo CHtml::dropDownList('filterId', '1', $dropDownFilter);
                        $this->endWidget();
                    ?>
                </div>
            </div>
<!--            <ul class="paging">

                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">Last page</a></li>
            </ul>
            <div class="numrows right">
                <p>Showing 1-12 of 78</p>
            </div>-->
            <div class="row">
                <div class="clear"></div>
                <?php $this->renderPartial('indexGrid', array('dataProvider'=>$dataProvider, 'listType'=>$listType, 'favLink'=>$favLink)); ?>
                <div class="left mapview" id="bigmap-canvas" style="display:none;"></div>
            </div>
<!--            <div class="col-sm-12 top-gap">
                <ul class="paging ">

                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">Last page</a></li>
                </ul>
                <div class="numrows right">
                    <p>Showing 1-12 of 78</p>
                </div>
            </div>-->
        </div>
        <?php } ?>
        
        
    </div>
    <div class="right blue-right-box">
        <div class="col-sm-12 clearfix">
            <h3><?php echo $companyData->name; ?></h3>
            <p>

                <?php echo strlen($companyData->address1) ? $companyData->address1.',<br />' : ''; ?>
                <?php echo strlen($companyData->address2) ? $companyData->address2.',<br />' : ''; ?>
                <?php echo strlen($companyData->city->name) ? $companyData->city->name.',' : ''; ?>
                <?php echo strlen($companyData->state->name) ? $companyData->state->name.'<br />' : ''; ?>
                <?php 
                    $flagImagePath = Yii::app()->baseUrl."/images/site/flags/".$companyData->country->short_code.".png";
                    $flagPath = "images/site/flags/".$companyData->country->short_code.".png";
                    if($companyData->country->short_code && file_exists($flagPath)) { ?>
                    <img src="<?php echo $flagImagePath; ?>" alt="flag" />
                <?php } ?>
                <?php echo strlen($companyData->country->name) ? $companyData->country->name : ''; ?>
                <?php if( strlen($companyData->country->name) && strlen($companyData->postalcode) ) { echo " - "; } ?>
                <?php echo strlen($companyData->postalcode) ? $companyData->postalcode : ''; ?>
            </p>
            
            
            <!-- CONTACT SELLER BUTTON -->
            <?php   // link is available only to Media Owner & Third Party
                if(!Yii::app()->user->isGuest) {
                    $roleId = Yii::app()->user->roleId;
                    if($roleId==2 || $roleId==4){   // media buyer or third party
                        echo CHtml::link(JoyUtilities::chopString("Contact ".$companyData->name, 17), '#', array('class'=>'btn btn-lg btn-success', 'rel' => 'activationPopup', 'title'=>"Contact ".$companyData->name, 'onclick'=>'email_model();'));
                    } /* else {
                        echo CHtml::link('Contact Seller', '', array('class'=>'btn btn-primary', 'title'=>'Please login as Media Buyer/Third party Owner to contact seller')); 
                    }*/
                } else {
                    echo CHtml::link(JoyUtilities::chopString("Contact ".$companyData->name, 17), 'javascript:void(0);', array('class'=>'btn btn-lg btn-success', 'onclick' => "$('#FavouriteModal').modal('show');", 'title'=>'Please login as Media Buyer/Third party Owner to contact seller')); 
                }
            ?>
            
<!--            <a href="#" class="btn btn-lg btn-success" title="<?php echo "Contact ".$companyData->name; ?>"><?php echo JoyUtilities::chopString("Contact ".$companyData->name, 17); ?></a>-->

        </div>
    </div>
</div>

<!-- Favourite / Contact Seller Guest Login model -->
<div class="modal fade left-aligned" id="FavouriteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <span class="close"></span>
            <div class="modal-body">
                <h4 class="modal-title">Login required as Media Buyer or Third Party!</h4>
                <p>Please <a href="<?php echo Yii::app()->urlManager->createUrl('account/login', array('rurl'=>Yii::app()->request->url)); ?>">click here</a> to login.</p>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
<div id="dp" style="display:none;"></div>
<input type="hidden" id="userfavlistids" value="" />
<script>
        function loadScript()
        {
          var script = document.createElement("script");
          script.type = "text/javascript";
          script.src = "https://maps.googleapis.com/maps/api/js?key=<?php echo Yii::app()->params['gmapApiKey']; ?>&sensor=true&callback=initialize";
          // &language=ja for language change
          // &region=GB for regional settings
          document.body.appendChild(script);
        }        
        window.onload = loadScript; 
        function initialize()     
        {
            // map for list and mosaic view
            // Enable the visual refresh
            //google.maps.visualRefresh = true;
            var myLatLng;
                        
            myLatLng = new google.maps.LatLng(10, -10);

            // get markers from div "dp", and converto array
            var mapLocation = JSON.parse("[" + $('#dp').html() + "]");
            //console.log(mapLocation);
            var mapOptions = {
              zoom: 1,
              center: myLatLng,
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              streetViewControl: false,
              panControl: false,
              mapTypeControl: false
            };
                                    
            map = new google.maps.Map(document.getElementById("bigmap-canvas"), mapOptions);
            setMarker(map, mapLocation, true);  // info window true            

            function setMarker(map, mapLoc, info) {
                  var counterVar = $('.counter').html();
                  var infowindow = new google.maps.InfoWindow();
                  for (i = 0; i < mapLoc.length; i++) {
                      var loca = mapLoc[i];
                      
                      //console.log(loca);
                      var myLanLat = new google.maps.LatLng(loca[0], loca[1]);
                      var marker = new google.maps.Marker({
                          position: myLanLat,
                          icon:'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld='+ (parseInt(counterVar)) +'|0066A1|FFFFFF',
                          shadow: 'https://chart.googleapis.com/chart?chst=d_map_pin_shadow',
                          map: map
                          //tittle: loca[0],
                          //zIndex: loca[3]
                      });
                      if(info) {
                            var content = $('#map_content_'+counterVar).val();
                            google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
                                  return function() {
                                      infowindow.setContent(content);
                                      infowindow.open(map,marker);
                                  };
                            })(marker,content,infowindow)); 
                      }
                      
                      counterVar++;
                  }
              }              
        }
        // get markers from hidden geo field in _view & _mview
        function getMarkers() {            
            var sum = '';
            $('.listing_geoloc').each(function(){            
                sum += this.value + ',';
                //console.log ('sum ' + sum);
            });
            sum = sum.substring(0, sum.length - 1);
            //sum += ']';
            //$('#dp').replaceWith("<?php //foreach($dataProvider->data as $data) { echo $data->geolat . ", "; } ?>");
            $('#dp').html(sum);
            
            initialize();         
        }
    function mark_favorite(){    
        //console.log('mark fav');
        $('a.add2fav').each(function(){

            var input_list_array = $("#userfavlistids").val();
            var list_array = input_list_array.split(",");
            var listing_id = $(this).attr("id");                
            if(list_array.indexOf(listing_id) >= 0) {
                $(this).css('background-image','url(<?php echo Yii::app()->getBaseUrl(); ?>/images/site/star_active.png)');
            }
        }); 
    }
  
</script>


    
    <!-- Email Modal -->
    <div class="modal fade left-aligned" id="MessageSend" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <span class="close"></span>
                <div class="modal-body" id="message_modal_body">
                    <?php /*$form=$this->beginWidget('CActiveForm', array(
                        'id'=>'email-contact-form',
                        'enableAjaxValidation'=>false,
                    )); */ ?>
                        <h4 class="modal-title">Send a message to <?php echo $companyData->name; ?>!</h4>
                        <div class="col-sm-12 clearfix">
                            <?php echo CHtml::textArea('Email_message','', array('rows'=>10, 'cols'=>50, 'class'=>'form-control col-sm-8')); ?>                            
                            
                        </div>                    
                        <?php echo CHtml::button('Send your message', array('class'=>'btn btn-lg btn-success', 'id'=>'email_submit', 'disabled'=>'disabled')); ?>
                    <?php //$this->endWidget(); ?>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
    <script>        
        $("#Email_message").bind("keyup", function(event, ui) {
            
            if( !$.trim($(this).val()) ) {
                $('#email_submit').attr("disabled", "disabled");
            } else {                
                $('#email_submit').removeAttr("disabled");
            }
           
        });
        
        function email_model() {            
            $('#Email_message').val('');
            $('#MessageSend').modal('show');
        } 
    </script>
    
    <!-- Email Success Modal -->
    <div class="modal fade left-aligned" id="ThanksModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <span class="close"></span>
                <div class="modal-body" id="ThanksModalBody">                    
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
    
     <!-- Favourite Guest Login model -->
    <div class="modal fade left-aligned" id="FavouriteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <span class="close"></span>
                <div class="modal-body">
                    <h4 class="modal-title">Login required as Media Buyer or Third Party!</h4>
                    <p>Please <a href="<?php echo Yii::app()->urlManager->createUrl('account/login', array('rurl'=>Yii::app()->request->url)); ?>">click here</a> to login.</p>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
     
     <script>
        $(function(){
            
            $('#email_submit').click(function(){
               //console.log('email submit') ;
               $.ajax({
                    type: "GET",
                    url: "<?php echo Yii::app()->urlManager->createUrl('ajax/contactseller'); ?>",
                    data: {
                        sellerid: "<?php echo $companyUserId; ?>",
                        message: $('#Email_message').val()
                    },
                    success: function(data) {
                        $('#message_modal_body').html('Email has been successfuly sent to the Seller.');
                        $('#MessageSend').modal('hide');
                        if(data==1) {
                            msg = '<h4 class="modal-title">Thank you!</h4><p>Email sent successfully.</p>';
                        } else if(data==2) {
                            msg = '<h4 class="modal-title">Oops!</h4><p></p>Please provide a valid message.';
                        }else {
                            msg = '<h4 class="modal-title">Oops!</h4><p></p>Failure, Please try again!';
                        }
                        $('#ThanksModalBody').html(msg);
                        $('#ThanksModal').modal('show');
                        
                    }
                }); 
            });
            
            // if user logged in, get his fav listing, mark them as fav
            if(!<?php echo Yii::app()->user->isGuest ? true:0; ?>) {                
                $.ajax({
                    type: "GET",
                    url: "<?php echo Yii::app()->urlManager->createUrl('ajax/userfavlistids'); ?>",
                    data: {
                        id: "<?php echo Yii::app()->user->id; ?>"                        
                    },
                    success: function(data) {
                        var list_array = data.split(",");
                        $("#userfavlistids").val(list_array);                        
                        mark_favorite();
                    }
                }); 
            }

            $('#filterId').fancyfields("bind","onSelectChange", function (input,text,val){     
                $.fn.yiiListView.update('listViewId', {
                        data: $('#filterId').serialize(),
                        complete: function(){ mark_favorite(); }
                    }
                );
            });  
            
            // add favorite star
            // $('a.add2fav').live('click', function(){
            $(document).on('click', 'a.add2fav', function(){                  
                //console.log('clicked');
                var clicked_fav_link = $(this);
                listing_id = clicked_fav_link.attr("id");
                // console.log( listing_id );
                
                if(!<?php echo Yii::app()->user->isGuest ? true:0; ?>) {
                    
                    // if user logged in (Media buyer & Third party)
                    // console.log('user');                    
                    $.ajax({
                        type: "GET",
                        url: "<?php echo Yii::app()->urlManager->createUrl('ajax/favlistingtoggle'); ?>",
                        data: {
                            id: "<?php echo Yii::app()->user->id; ?>",
                            listingid: listing_id
                        },
                        success: function(data) {
                            var input_list_array = $("#userfavlistids").val();
                            var list_array = input_list_array.split(",");
                            var index = list_array.indexOf(listing_id);                 
                            // check if returned true
                            if(data==1) {
                                // fav added show golden star
                                clicked_fav_link.css('background-image','url(<?php echo Yii::app()->getBaseUrl(); ?>/images/site/star_active.png)');
                                if(index < 0) {                                    
                                    $("#userfavlistids").val( $("#userfavlistids").val() +","+listing_id);
                                }
                            } else if(data==0) {
                                // fav added show silver star
                                clicked_fav_link.css('background-image','url(<?php echo Yii::app()->getBaseUrl(); ?>/images/site/star.png)');
                                if(index >= 0) {
                                    list_array.splice(index,1);                                    
                                    $("#userfavlistids").val( list_array );
                                }
                                
                            }
                        }
                    });
                } else {
                    // if not logged in then show login modal
                    // console.log('guest');
                    // show login message modal
                    $('#FavouriteModal').modal('show');                    
                }
            });
            
             
        });
    </script>

<?php

    $btnFilter = '$(".updatelisting").click(function(){
                            filterid =  $( "select#filterId option:selected").val();
                            var listtype;
                            if( $(this).is("a") ) {     // for the target link                            
                                a_attr = $(this).attr("id");                                
                                if(a_attr=="moslink") {
                                    listtype ="m";                                    
                                } else if(a_attr=="maplink") {
                                    listtype = "p";
                                } else {
                                    listtype = "";
                                }
                            } 
                            companyUserId = '. $companyUserId.';
                            $.fn.yiiListView.update("listViewId", {
                                    url: "'.Yii::app()->request->url.'?&lt="+listtype+"&filterId="+filterid,
                                    data: $(this).serialize(),
                                    complete: function(){ 
                                        mark_favorite();
                                        $(".updatelisting").parent("li").removeClass("active");                                        
                                        if( listtype=="m" ) {                                            
                                            $("#bigmap-canvas").hide();
                                            $("#moslink").parent("li").addClass("active");
                                        } else if(listtype=="p") {                                             
                                            $("#bigmap-canvas").show();
                                            getMarkers();
                                            $("#maplink").parent("li").addClass("active");
                                        } else {
                                            $("#bigmap-canvas").hide();
                                            $("#listlink").parent("li").addClass("active");
                                        }
                                    }
                                }
                            );
                            return false;
                        });';
            Yii::app()->clientScript->registerScript('listFilter', $btnFilter, CClientScript::POS_END);