<?php                    
    if($listType=='m') { $lt = '/listing/_mview'; } else if($listType=='p') { $lt = '/listing/_mpview'; } else { $lt = '/listing/_view'; }
    $this->widget('zii.widgets.CListView', array(
        'dataProvider'=>$dataProvider,
        'itemView'=> $lt,    // refers to the partial view named '_post'
        //lets tell the pager to use our own css file
        'pager' => array('cssFile' => Yii::app()->baseUrl . '/css/listViewStyle/listViewPager.css'),
        // 'pagerCssClass' => 'paging',
        'summaryText'=>'Showing {start} - {end} of {count}',
        'pager' => Array(
                    // 'cssFile' => Yii::app()->baseUrl . '/css/listViewStyle/listViewPager.css',
                    'header' => '',
                    'prevPageLabel' => 'Previous',
                    'nextPageLabel' => 'Next',
                    'firstPageLabel'=>'First',
                    'lastPageLabel'=>'Last Page'
        ),
        'emptyText' => '<div align="center">No Records Found.</div>',
        'template'=>'<div>{pager}{summary}</div><div style="clear:both;"></div>{items}<div id="downPager"><div style="clear:both;">{pager}{summary}</div></div>',
        'id'=>'listViewId',
        'viewData' => array('favLink'=>$favLink),
        'sortableAttributes'=>array(
            //'name',
        ),
        //'enablePagination'=> false,
        'afterAjaxUpdate'=>"function(id, data) {  
            mark_favorite();
        }",
    ));
