<!-- FETCHES ONLY SINGLE COMPANY DETAILS -->
    <h4><?php echo $company->name; ?></h4>
    <?php echo $company->address1.','; ?><br />
    <?php echo $company->address2.','; ?><br />
    <?php echo Area::getAreaName($company->countryid) ? Area::getAreaName($company->countryid).', ' : ''; ?>
    <?php echo Area::getAreaName($company->stateid) ? Area::getAreaName($company->stateid).', ' : ''; ?>
    <?php echo Area::getAreaName($company->cityid) ? Area::getAreaName($company->cityid).', ' : ''; ?><br />
    <?php echo $company->postalcode; ?><br /><br />
    
    
    <?php 
        if($company->googleplusprofile) {
            echo "<a href='". $company->googleplusprofile ."' rel='nofollow'>Google+</a>"; 
        }
    ?>
    
    <br />
    <?php 
        if($company->linkedinprofile) {
            echo "<a href='". $company->linkedinprofile ."' rel='nofollow'>LinkedIn</a>"; 
        }
    ?>

    <br />
    <?php 
        if($company->facebookprofile) {
            echo "<a href='". $company->facebookprofile ."' rel='nofollow'>Facebook</a>"; 
        }
    ?>

    <br />
    <?php 
        if($company->twitterhandle) {
            echo "<a href='https://twitter.com/". $company->twitterhandle ."' rel='nofollow'>Twitter</a>"; 
        }
    ?>
    <br /><br />
    
    <?php echo CHtml::link($company->websiteurl, array($company->websiteurl)); ?><br /><br />
    
    
<!-- CONTACT SELLER BUTTON -->
    <?php   // link is available only to Media Owner & Third Party            
            if(!Yii::app()->user->isGuest) {
                $roleId = Yii::app()->user->roleId;
                if($roleId==2 || $roleId==4){
                    echo CHtml::link('Contact Seller', Yii::app()->createUrl('seller/contact', array('id'=>$company->userid)), array('title'=>'Email to seller'));
                } else {
                    echo CHtml::link('Contact Seller', '', array('title'=>'Please login as Media Buyer/Third party Owner to contact seller')); 
                }
            } else {
                echo CHtml::link('Contact Seller', '', array('title'=>'Please login as Media Buyer/Third party Owner to contact seller')); 
            } ?>
    <br /><br />

    <!-- AddThis Button BEGIN -->
        <!-- Change icons from https://www.addthis.com/get/sharing?flag=registered&lb=1 -->
        <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
            <a class="addthis_button_pinterest_share"></a>
            <a class="addthis_button_google_plusone_share"></a>
            <a class="addthis_button_linkedin"></a>
            <a class="addthis_button_facebook"></a>
            <a class="addthis_button_twitter"></a>
            <a class="addthis_button_email"></a>
            <a class="addthis_button_compact"></a>
            <!--<a class="addthis_counter addthis_bubble_style"></a>-->
        </div>
        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52a56e4a5b6d8a9d"></script>
    <!-- AddThis Button END -->

    <h3>About Us</h3>
    <?php echo $company->description; ?><br /><br />

    <h3><?php echo $company->name; ?>'s Listing</h3>

