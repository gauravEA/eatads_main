<?php
/* @var $this ListingController */
/* @var $dataProvider CActiveDataProvider */
/*
    1. map markers 
    2. proximity 

*/
?>
<?php // Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/geocomplete.css'); ?>
<!--<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>-->

<script>
    $('#body').addClass('views');
</script>

<div class="navbar navbar-second navbar-static-top ">
    <div class="container">
        <div class="left">
            <h1>Results - <span id="result_count"></span></h1>
        </div>
        <?php $this->widget('CreateAccountButton'); ?>
    </div>
</div>
<div class="container main-body NoBottomPadding">
    <div class="viewNavigation">
        <ul>
            <?php            
            // echo '<li>' . CHtml::link("List view", Yii::app()->urlManager->createUrl('listing/index').'?lt=&filterid=1', array('class' => 'list', 'id' => 'listlink')) . '</li>';
            // echo '<li>' . CHtml::link("Mosaic view", Yii::app()->urlManager->createUrl('listing/index').'?lt=m&filterid=1', array('class' => 'mosaic', 'id' => 'moslink')) . '</li>';
            echo '<li class="active">' . CHtml::link("Map view", 'javascript:void(0);', array('class' => 'map-view updatelisting', 'id' => 'maplink')) . '</li>';
            echo '<li>' . CHtml::link("List view", 'javascript:void(0);', array('class' => 'list', 'id' => 'listlink')) . '</li>';
            echo '<li>' . CHtml::link("Mosaic view", 'javascript:void(0);', array('class' => 'mosaic', 'id' => 'moslink')) . '</li>';
            ?>

        </ul>
    </div>
</div>
<style>
    div#holder {
        position: relative;
    }    
    div.overlay {
        position: absolute;
        top: 18px;
        left: 0px;
        width: 743px;
        height: 600px;          
        opacity: 0.8;
        z-index: 99;
    }
    div.overlay.standard { background: #fff url("<?php echo Yii::app()->getBaseUrl().'/images/site/eatadspreload.gif'; ?>") no-repeat 50% 50%; }    
</style>
<div class="container main-body markers">
    <div class="hr"></div>
    <div class="row">
        <div class="col-sm-9 left">
            <div class="row">
                <!-- View comes here -->
                <div class="overlay standard" style="display:none;" id="div_overlay">&nbsp</div>
                <div class="left mapview" id="bigmap-canvas" style="height:600px;width:100%;"></div>
                <div id="legend"></div>
            </div>
        </div>
        <div class="right filter-box">            
            <div class="col-sm-12 clearfix">                    
                <?php echo CHtml::textField('textsearch', isset($_GET['textsearch'])?$_GET['textsearch']:'', array('placeholder' => 'search in the results', 'class' => 'form-control')) ?>
            </div>
            <div class="col-sm-12 clearfix">                    
                <?php echo CHtml::textField('geocomplete_filter', '', array('placeholder' => 'Type in an address', 'class' => 'form-control geoac')) ?>
            </div>
            <?php /*<div class="col-sm-6 clearfix">
                <?php echo CHtml::dropDownList('countryid', ($search_params['countryid'] > 0) ? $search_params['countryid'] : '', Area::model()->getPriorityCountryOptions(), array('empty' => 'Select country', 'disabled'=>'disabled')); ?>
            </div>
            <div class="col-sm-6 clearfix" >
                <?php echo CHtml::dropDownList('stateid', ($search_params['stateid'] > 0) ? $search_params['stateid'] : '', ($search_params['countryid'] > 0) ? Area::model()->getStateOptions($search_params['countryid']) : array(), array('empty' => 'Select state', 'disabled'=>'disabled')); ?>
            </div>
            <div class="col-sm-6 clearfix">
                <?php echo CHtml::dropDownList('cityid', ($search_params['cityid'] > 0) ? $search_params['cityid'] : '', ($search_params['stateid'] > 0) ? Area::model()->getCityOptions($search_params['stateid']) : array(), array('empty' => 'Select city', 'disabled'=>'disabled')); ?>
            </div> */ ?>

            <div class="col-sm-12 clearfix mtype">
                <?php echo CHtml::dropDownList('mediatypeid', 
                                    ($search_params['mediatypeid'] > 0) ? $search_params['mediatypeid'] : '',
                                    MediaType::getPriorityMediaType(),
                                    array('multiple'=>'multiple', 'data-title'=>'Media Type')); ?>
                <?php // echo CHtml::checkboxlist('mediatypeid', $search_params['mediatypeid'], CHtml::listData(MediaType::model()->findAll(), 'id', 'name')); ?>                
            </div>
            
            
            <!-- HTML CODE FOR FILTERING -->
            <div class="col-sm-12 nooverflow"><input type="text" id="slider-range" /></div>

            <div class="col-sm-12 clearfix">                
                <?php echo CHtml::button('Filter', array('class' => 'btn btn-success btn-sm updatelisting', 'id' => 'btn_filter', 'name' => 'btn_filter')); ?>
            </div>
        </div>
        <!-- content container will end in layout -->
    </div>  

    <!-- Favourite Guest Login model -->
    <div class="modal fade left-aligned" id="FavouriteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <span class="close"></span>
                <div class="modal-body">
                    <h4 class="modal-title">Login required as Media Buyer or Third Party!</h4>
                    <p>Please <a href="<?php echo Yii::app()->urlManager->createUrl('account/login', array('rurl' => Yii::app()->request->url)); ?>">click here</a> to login.</p>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
    <input type="hidden" value="" id="marker_loaded" value="" />
    <input type="hidden" id="geo_latlng" value="<?php echo $geoLatLng; ?>" />
    <input type="hidden" id="proximity" value="<?php echo $proximity; ?>" />
    <input type="hidden" id="userfavlistids" value="" />
    <input type="hidden" id="pageMetaKeywordText" value="<?php echo $pageMetaKeywordText; ?>" />
    <div class="details">
        <input type="hidden" data-geo="country" id="bc_country" value="" />
        <input type="hidden" data-geo="administrative_area_level_1" id="bc_state" value="" />
        <input type="hidden" data-geo="locality" id="bc_city" value="" />
    </div>    
</div>

    <!-- DROP DOWN JS, AJAX, VIEW RENDER CODE - not in use fancy fields -->    
    <!-- LOAD YII JQUERY & JQUERY CSS FILES FOR SLIDER -->
    
    <?php // Yii::app()->clientScript->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css'); ?>
    <?php // Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    <?php // Yii::app()->clientScript->registerCoreScript('jquery.ui');  ?>
    
    <script type="text/javascript" src = "https://maps.googleapis.com/maps/api/js?v=3.exp&key=<?php echo Yii::app()->params['gmapApiKey']; ?>&sensor=false&libraries=places"></script>
    <script type="text/javascript" src = "<?php echo Yii::app()->baseUrl . '/js/jquery.geocomplete.js'; ?>"></script>
    
    <!-- LOAD MAP MARKERS -->    
    <script type="text/javascript">
        
        document.onclick = function(evt) {
            var evt=window.event || evt; // window.event for IE
            if (!evt.target) evt.target=evt.srcElement; // extend target property for IE
                console.log(evt.target);            
        }
        
        var icons, map, centerLatLng, legend, circle, mcOptions;
        var centerPointArr = [];
        var allMarkers = [];
        /*var clustererMaxZoom = 11;
        var clusterer = null;
        var clusterStyles = [];
        var gridSize = 80;
        var clusterStyle = 0;*/
        //var markerClusterer;
        //var total_marker_toload, marker_loaded, next_toload_count;
            
        function initialize()
        {
            //console.time('map init time');
            console.log('init called');
            //$('#div_overlay').show();
                       
            var geo_latlng = $('#geo_latlng').val();
            var geo_latlng_split = geo_latlng.split(",");
            centerLatLng = new google.maps.LatLng(geo_latlng_split[0], geo_latlng_split[1]);
            
            var styles = [{
                stylers: [
                    { saturation: -60 }
                ]
            }];            
            
            var mapOptions = {
                zoom: 11,    //11   max should be 4/5
                center: centerLatLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                streetViewControl: false,
                panControl: false,
                mapTypeControl: false,
                styles: styles
            };            
            
            icons = {
                exact: {
                    name: 'Exact location',
                    //icon: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/exact.png',
                    //icon: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/exact_new.png',
                    icon: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/markerred2.png',
                    //legend: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/exact_legend.png'
                },
                approx: {
                    name: 'Approximate location',
                    //icon: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/approx.png',
                    //icon: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/approx_new.png',
                    icon: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/markerblue2.png',
                    //legend: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/approx_legend.png'
                }
            };            
            
            map = new google.maps.Map(document.getElementById("bigmap-canvas"), mapOptions);
            //setMarker();
            //setTimeout(function(){ setMarker(); },5000);
            
            // Limit the zoom level
            /*google.maps.event.addListener(map, 'zoom_changed', function() {
                console.log('init zoomchanged - '+ map.getZoom());                
                //$('#div_overlay').show();
                //console.log('div_overlay show');
                centerLatLng = map.getCenter();
                var lat = centerLatLng.lat();
                var lng = centerLatLng.lng();
                centerPointArr[0] = lat = lat.toFixed(6);
                centerPointArr[1] = lng = lng.toFixed(6);
                $('#geo_latlng').val(lat + ',' + lng);
                $('#geocomplete_filter').val('');
                
                console.log('##1 setCircleProximity');
                setCircleProximity();
                func_updatelisting($(this));                
            });*/
            
            google.maps.event.addListener(map, 'dragend', function() {
                //console.log('init dragend');
                //$('#div_overlay').show();
                //console.log('div_overlay show');
                centerLatLng = map.getCenter();
                var lat = centerLatLng.lat();
                var lng = centerLatLng.lng();
                centerPointArr[0] = lat = lat.toFixed(6);
                centerPointArr[1] = lng = lng.toFixed(6);
                $('#geo_latlng').val(lat + ',' + lng);
                $('#geocomplete_filter').val('');
                
                console.log('##2 setCircleProximity');
                setCircleProximity();
                //console.log('drag Zoom - ' + map.getZoom());                
                func_updatelisting($(this));
            });

            var $map = $('#bigmap-canvas');
            function wheelEvent(event) {
                //console.log('init mouse wheel event');
                //console.log(event.detail);
                if(event.wheelDelta == -120 || event.detail >0) {
                    // for chrome OR firefox
                    setTimeout(function(){ 
                        //$('#div_overlay').show();
                        //console.log('div_overlay show');
                        centerLatLng = map.getCenter();
                        var lat = centerLatLng.lat();
                        var lng = centerLatLng.lng();
                        centerPointArr[0] = lat = lat.toFixed(6);
                        centerPointArr[1] = lng = lng.toFixed(6);
                        $('#geo_latlng').val(lat + ',' + lng);

                        setCircleProximity();
                        //console.log('initialize() zoom - ' + map.getZoom());
                        drawCircle(map.getCenter());    // redraw circle
                        func_updatelisting($(this));    // update listings on map
                        //console.log('initialize() map center - ' + centerLatLng);                        
                        //map.setCenter(center);
                    },50);
                } else {
                    console.log('zoom in');
                }
            }
            $map[0].addEventListener('mousewheel', wheelEvent, true );
            $map[0].addEventListener('DOMMouseScroll', wheelEvent, true );
            
            legend = document.getElementById('legend');                      
            for (var key in icons) {
                var type = icons[key];
                var name = type.name;
                var icon = type.icon;
                var div = document.createElement('div');
                div.innerHTML = '<img src="' + icon + '"> ' + name;
                legend.appendChild(div);
            }
            // map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);
            
            
            // ENABLE THIS IF zoom_changed IS REMOVED
            //console.log("##0 draw circle");            
            drawCircle(map.getCenter());
            map.fitBounds(circle.getBounds());
            
            //console.timeEnd('map init time');
            /*setTimeout(function(){
                $('#div_overlay').hide();
            },5500);*/
            //checkUrl();
        }        
        initialize();
        /*function get_next_toload_count()
        {
            total_marker_toload = parseInt($('#result_count').html());
            marker_loaded = parseInt($('#marker_loaded').val());
            next_toload_count = <?php //echo Yii::app()->params['load_markers']; ?>;
            
            if(total_marker_toload - marker_loaded <= next_toload_count) {
                next_toload_count = total_marker_toload - marker_loaded;
            }
            
            console.log('marker_loaded - ' + marker_loaded);
            console.log('total_marker_toload - ' + total_marker_toload);
            console.log('next_toload_count - ' + next_toload_count);
            
            //marker_loaded = marker_loaded + next_toload_count;
            //$('#marker_loaded').val(marker_loaded);
            //console.log('after marker_loaded = ' + marker_loaded);
            
            //console.log('get next toload count - ' + next_toload_count);
            return next_toload_count;
        }
        function lazy_load_markers()
        {
            console.log('lazy load markers - '+marker_loaded+'/'+next_toload_count);
            
            if(get_next_toload_count()) {
                var mediatypeid = '';
                $(':checkbox:checked').each(function(i){
                     mediatypeid += $(this).val() +',';
                });
                mediatypeid = mediatypeid.replace(/^,|,$/g,'');             
                var price_val_array = $("#slider-range").val().split(";");
                var price_val = price_val_array[0] + "-" + price_val_array[1];
                //console.log(price_val + " " + countryid+" "+ stateid + " "+ cityid + " "+ mediatypeid);            
                //var distance = $("#distance").val();            
                var geoloc = $("#geo_latlng").val();
                var textsearch = $("#textsearch").val();
                $('#proximity').val(4500);
                
                $.ajax({
                    url: "<?php //echo Yii::app()->urlManager->createUrl('map/indexajax'); ?>",
                    data: {
                        mediatypeid: mediatypeid,
                        priceslider: price_val,
                        textsearch: textsearch,
                        geoloc: geoloc,
                        proximity: $('#proximity').val(),
                        marker_loaded: marker_loaded,
                        next_toload_count: next_toload_count
                    },
                    async: false,
                    success: function(data) {
                        //$(".updatelisting").parent("li").removeClass("active");
                        $(".updatelisting").removeAttr("disabled");

                        $('#dp').html(data);

                        //console.log(data);
                        //console.log();
                        //marker_loaded = marker_loaded + next_toload_count;
                        //console.log('after ajax - ' + marker_loaded + ' / '+ next_toload_count +' / ' + total_marker_toload);
                        //$('#marker_loaded').val(marker_loaded);

                        setMarker();
                        if(get_next_toload_count()) {                        
                            setTimeout(function(){ 
                                //console.log('lazy_load_markers');
                                lazy_load_markers(); 
                            }, 200);
                        } else {
                            $('#div_overlay').hide();
                            //console.log('no markers to load');
                        }                    
                    }
                }); 
            } 
        }*/
        function getUrlParameter(param)
        {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++)
            {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == param)
                {                   
                    return sParameterName[1];   // otherwise                    
                }
            }
            return false;   // if param donot exists
        }
        function getlistingcount()
        {
            console.log('get listing count');
            var mapData = JSON.parse($('#dp').html());
            var listing_count = mapData.response.numFound;
            //console.log(listing_count);
            if (listing_count) {
                $('#result_count').html(listing_count);
            } else {
                $('#result_count').html(0);
            }
        }
        
        var ajax_request;
        function func_updatelisting(thisvar) {
            console.log('func_updatelisting');
            var url_params, isviewlink;
            
            //var mediatypeid = ($("#mediatypeid").val() > 0) ? $("#mediatypeid").val() : "";
            var mediatypeid = '';
            $(':checkbox:checked').each(function(i){
                 mediatypeid += $(this).val() +',';
            });
            mediatypeid = mediatypeid.replace(/^,|,$/g,'');
            var price_val_array = $("#slider-range").val().split(";");
            var price_val = price_val_array[0] + "-" + price_val_array[1];                        
            var geoloc = $("#geo_latlng").val();
            var textsearch = $("#textsearch").val();
            var proximity = $("#proximity").val();
            
            url_params = "?mediatypeid=" + mediatypeid + "&priceslider=" + price_val + '&textsearch='+ textsearch + "&geoloc=" + geoloc+"&proximity="+proximity;
            //console.log(url_params);
            if(typeof ajax_request !== 'undefined') {
                ajax_request.abort();
                //console.log('ajax aborted');
            }
            ajax_request = $.ajax({
                url: "<?php echo Yii::app()->urlManager->createUrl('map/indexajax'); ?>",
                data: {                    
                    mediatypeid: mediatypeid,
                    priceslider: price_val,
                    textsearch: textsearch,
                    geoloc: geoloc,
                    proximity: proximity
                },
                async: false,
                beforeSend: function() {                    
                    //console.log('before');                                        
                    $(".updatelisting").attr("disabled", true);                    
                    $('.updatelisting').click(function () {return false;});
                },
                success: function(data) {                    
                    //$(".updatelisting").parent("li").removeClass("active");
                    $(".updatelisting").removeAttr("disabled");
                   
                    $('#dp').html(data);
                    // console.log(data);
                    // setCircleProximity();
                    console.log("##2 set marker");
                    setMarker();                    
                    //getlistingcount();
                    
                    var stateObj = {geoloc: geoloc};
                    history.pushState(stateObj, null, url_params);
                }
            });            
            return false;
        }

        function load_geocomplete() 
        {
            $("#geocomplete_filter").geocomplete()
            .bind("geocode:result", function(event, result){
                $('#textsearch').val('');
                $('#geomsg').html("Result: " + result.formatted_address);
                var lat = centerPointArr[0] = parseFloat(result.geometry.location.lat()).toFixed(6);
                var lng = centerPointArr[1] = parseFloat(result.geometry.location.lng()).toFixed(6);
                var location = lat + "," + lng;
                //console.log('location - ' + location);
                
                $('#geo_latlng').val(location);
                //$('#viewport').val(result.geometry.viewport);
                var vp = result.geometry.viewport;
                if(vp) {
                    //map = new google.maps.Map(document.getElementById("bigmap-canvas"));
                    map.fitBounds(result.geometry.viewport);
                    // calculate radius and fill it
                    console.log('##3 setCircleProximity');
                    setCircleProximity();                                        
                    
                    // DUPLICATE HITS ON GEOCOMPLETE                    
                    //setCircleProximity();
                    //drawCircle(map.getCenter());
                    func_updatelisting($(this));
                    
                } else {
                    //console.log('no bounds');
                    alert('Please try some other location.');
                }
            })
            .bind("geocode:error", function(event, status){
                $('#geomsg').html("ERROR: " + status);
            })
            .bind("geocode:multiple", function(event, results){
                $('#geomsg').html("Multiple: " + results.length + " results found");
            });
        }
               
        function setCircleProximity()
        {
            console.log('set proximity called');
            // get the markers and set the viewport
            // to set the zoom level, and also need to show clustering
            bounds = map.getBounds();
            //console.log('Center2 - ' + map.getCenter());
            //center = bounds.getCenter();
            //console.log('Center3 - ' + center);

            //var center_latlng = $('#geo_latlng').val();
            //center_latlng = center_latlng.split(",");
            //console.log('setCircleProximity() ' + center_latlng[0] + ',' + center_latlng[1]);
            // console.log('setCircleProximity() ' + centerPointArr[0] + ',' + centerPointArr[1]);
            
            ne = bounds.getNorthEast();

            // r = radius of the earth in statute miles
            var r = 3963.0;

            // Convert lat or lng from decimal degrees into radians (divide by 57.2958)
            var lat1 = centerPointArr[0] / 57.2958;
            var lon1 = centerPointArr[1] / 57.2958;
            var lat2 = ne.lat() / 57.2958;
            var lon2 = ne.lng() / 57.2958;
            
            // distance = circle radius from center to Northeast corner of bounds
            proximity = r * Math.acos(Math.sin(lat1) * Math.sin(lat2) + 
            Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1));
            proximity = Math.round(proximity * 1000) / 1000;
            $('#proximity').val(proximity);
            console.log('setCircleProximity() proximity - ' + proximity);
        }
        function drawCircle(centerLatLng)
        {
            console.log('draw circle called');
            if(circle) {
                circle.setMap(null);
            }
            var proximity = $('#proximity').val();
            if(proximity > 0) {
                proximity = proximity * 1000;
            } else {
                proximity = 15000;
            }
            //console.log("drawCircle() proximity - " + proximity);
            var circleOptions = {
                //strokeColor: '#00CCFF',
                //strokeOpacity: 0.8,
                strokeOpacity: 0.0,
                //strokeWeight: 1,
                //fillColor: '#FFFFFF',
                //fillOpacity: 0.20,
                fillOpacity: 0.0,
                map: map,
                center: centerLatLng,
                radius: proximity
            };
            // Add the circle for this city to the map.
            circle = new google.maps.Circle(circleOptions);
            map.panTo(centerLatLng);            
        }
        
        function clearOverlays() {
            //console.log('clear overlays called');
            //console.log(allMarkers);
            if(allMarkers.length) {
                for (var i = 0; i < allMarkers.length; i++) {
                    allMarkers[i].setMap(null);
                }
                allMarkers.length = 0;
            }
        }
        
        function setMarker() {
            //console.log('setmarker called');
            console.time("setmarker");
            clearOverlays();
            /*if (typeof markerClusterer === 'object') {  
                // console.log('clear markers called');
                markerClusterer.clearMarkers();
            }*/
        
            //clearClusterMarkers();
            // draw circle 
            var geo_latlng = $('#geo_latlng').val();
            var geo_latlng_split = geo_latlng.split(",");
            //console.log(typeof google);
            //console.log(typeof google.maps);           
            myLatLng = new google.maps.LatLng(geo_latlng_split[0], geo_latlng_split[1]);
            console.log("##1 draw circle");
            drawCircle(myLatLng);
            
            // get markers from div "dp", and converto array              
            mapData = JSON.parse($('#dp').html());
            mapLoc = mapData.response.docs;            
            console.log('setMarker() markers found - ' + mapData.response.numFound);
            //console.log('setMarker() markers loaded - ' + mapData.responseHeader.params.rows);
            $('#result_count').html(mapData.response.numFound);            
            
            marker_count = mapData.response.numFound;            
            
            //console.time('marker render time');
            var type;
            var infowindow = new google.maps.InfoWindow();            
            for(var key in mapLoc) {
            //console.log(key);    
                                
                if(mapLoc[key].lat != '0.000000' && mapLoc[key].lng != '0.000000') {
                    
                    var myLanLat = new google.maps.LatLng(mapLoc[key].lat, mapLoc[key].lng);
                    if(mapLoc[key].ea==1) {                    
                        type = icons['exact']['icon'];                        
                    } else {                        
                        type = icons['approx']['icon'];                        
                    }
                    console.log(mapLoc[key].id +" - " +mapLoc[key].ea);
                    
                    // Draw marker
                    var marker = new google.maps.Marker({
                        position: myLanLat,
                        icon: type,
                        map: map
                    });
                    marker.setValues({id: mapLoc[key].id});
                    
                    allMarkers.push(marker);
                    // info window
                    var content = ''; // $('#map_content_' + counterVar).html();
                    google.maps.event.addListener(marker, 'click', (function(marker, content, infowindow) {                        
                        return function() {
                            //infowindow.setContent(content);
                            infowindow.open(map, marker);                            
                            $.ajax({
                                type: "GET",
                                url: "<?php echo Yii::app()->urlManager->createUrl('ajax/getmapinfosolr'); ?>",                                
                                data: {
                                    id: marker.id
                                },
                                beforeSend: function() {
                                    infowindow.setContent('<img src="<?php echo Yii::app()->getBaseUrl(); ?>/images/site/ajax-loader.gif" />');
                                },
                                success:function (data) {
                                    infowindow.close();//hide the infowindow
                                    infowindow.setContent(data);
                                    infowindow.open(map, marker);
                                }
                            });
                        };
                    })(marker, content, infowindow));
                    //counterVar++;                    
                }                
            }
            //console.timeEnd('marker render time');
            //map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);                        
            //markerClusterer.addMarkers(allMarkers);
                        
            //var mc = new MarkerClusterer(map);            
            //fetchBreadCrumb();
            
            //$('#div_overlay').hide();
            //console.log('div_overlay hide');
            console.timeEnd("setmarker");
        }
    </script>
    <!-- WHOLE VIEW IS RENDERED & CUSTOMIZED HERE -->
        <div id="dp" style="display:none;"><?php echo $dataProvider; ?></div>
    <?php 
    $minMaxListPriceUsd = Yii::app()->cache->get('min_max_listprice');
    
    if(!$minMaxListPriceUsd) {
        $cmObj = new CachedMethods(); 
        $minMaxListPriceUsd = $cmObj->getMinMaxListPrice();
    }
    
    $minMaxListPriceNew = array(
        'min' => round(Yii::app()->openexchanger->convertCurrency($minMaxListPriceUsd['min'], 'USD', $this->ipCurrencyCode)),
        'max' => round(Yii::app()->openexchanger->convertCurrency($minMaxListPriceUsd['max'], 'USD', $this->ipCurrencyCode))
    );
    $fromValue = (isset($search_params['priceslider'][0]) && $search_params['priceslider'][0] > 0) ? $search_params['priceslider'][0] : $minMaxListPriceNew['min'];
    $toValue = (isset($search_params['priceslider'][1]) && $search_params['priceslider'][1] > 0) ? $search_params['priceslider'][1] : $minMaxListPriceNew['max'];
?>    
    <script type="text/javascript">        
        $(document).on('click', 'a.bcb_geo', function(){
            var click_id = $(this).attr('id');                        
            var search;
            if(click_id=='bcb_country') {
                search = $(this).text();
            } else if(click_id=='bcb_state') {
                search = $(this).text() + ', ' + $('#bcb_country').text();
            } else {
                search = $(this).text() + ', ' + $('#bcb_state').text() + ', ' + $('#bcb_country').text();
            }            
            $("#geocomplete_filter").val(search).trigger("geocode");
            return false;
        });
        $(function() {
            
            /*$("div[title=\"Zoom out\"] img").click(function(){
               console.log('zoom out + clicked') ;
            });
            
            $("div[title=\"Zoom in\"] img").click(function(){
               console.log('zoom in - clicked') ;
            });*/
            
            $('#bigmap-canvas').on("click", 'a.add2fav', function (e) {                
                //$('a[rel="activationPopup"]').on('click', function(e){    
                favFlag = $(this).attr('rel');
                if(favFlag == "activationPopup") {            
                    e.preventDefault();
                    $('#inactiveUser').modal({ keyboard: false, backdrop : false });                        
                    return false;
                } else {                    
                     return;
                }
            });
            
            // add favorite star
            // $('a.add2fav').live('click', function() {
            $(document).on('click', 'a.add2fav', function(){
                //console.log('clicked');
                var clicked_fav_link = $(this);
                var listing_id = clicked_fav_link.attr("id");
                // console.log( listing_id );                
                if (!<?php echo Yii::app()->user->isGuest ? true : 0; ?>) {

                    // if user logged in (Media buyer & Third party)
                    // console.log('user');                    
                    $.ajax({
                        type: "GET",
                        url: "<?php echo Yii::app()->urlManager->createUrl('ajax/favlistingtoggle'); ?>",
                        async: false,
                        data: {
                            id: "<?php echo Yii::app()->user->id; ?>",
                            listingid: listing_id
                        },
                        success: function(data) {
                            //var input_list_array = $("#userfavlistids").val();
                            //var list_array = input_list_array.split(",");
                            //var index = list_array.indexOf(listing_id);
                            // check if returned true
                            if (data == 1) {
                                // fav added show golden star
                                clicked_fav_link.css('background-image', 'url(<?php echo Yii::app()->getBaseUrl(); ?>/images/site/star_active.png)');
                                /*if (index < 0) {
                                    $("#userfavlistids").val($("#userfavlistids").val() + "," + listing_id);
                                }*/
                            } else if (data == 0) {
                                // fav added show silver star
                                clicked_fav_link.css('background-image', 'url(<?php echo Yii::app()->getBaseUrl(); ?>/images/site/star.png)');
                                /*if (index >= 0) {
                                    list_array.splice(index, 1);
                                    $("#userfavlistids").val(list_array);
                                }*/
                            }
                            //setTimeout(function(){ initialize(); },3000);   // for map view fav star                                   
                        }
                    });
                } else {
                    // if not logged in then show login modal
                    // console.log('guest');
                    // show login message modal
                    $('#FavouriteModal').modal('show');
                }
            });           
            
            $("#slider-range").ionRangeSlider({
                min: <?php echo $minMaxListPriceNew['min']; ?>,
                max: <?php echo $minMaxListPriceNew['max']; ?>,
                from: <?php echo $fromValue; ?>,
                to: <?php echo $toValue; ?>,
                type: "double",
                step: 1,
                prefix: get_sel_cur() + ' ',
                prettify: true,
                hasGrid: false
            });
          
            $('#mediatypeid').multiselect({
                includeSelectAllOption: true,
                enableFiltering: false,
                maxHeight: 250
            });
                            
            $("#listlink").click(function(){
                var mediatypeid = getUrlParameter('mediatypeid') ? getUrlParameter('mediatypeid') : '';
                var priceslider = getUrlParameter('priceslider') ? getUrlParameter('priceslider') : '';
                var filterid = getUrlParameter('filterid') ? getUrlParameter('filterid') : '';                
                var textsearch = getUrlParameter('textsearch') ? getUrlParameter('textsearch') : '';                
                //url = '<?php echo Yii::app()->urlManager->createUrl("listing/index?"); ?>mediatypeid='+ mediatypeid +'&priceslider='+ priceslider +'&filter='+ filterid +"&textsearch=" + textsearch +'&geoloc='+getUrlParameter('geoloc')+'&proximity='+getUrlParameter('proximity')+'&lt=';
                url = '<?php echo Yii::app()->urlManager->createUrl("s/listingview?"); ?>mediatypeid='+ mediatypeid +'&priceslider='+ priceslider +'&filter='+ filterid +"&textsearch=" + textsearch +'&geoloc='+getUrlParameter('geoloc')+'&proximity='+getUrlParameter('proximity')+'&type=';
                location.href = url;                            
            });
            $("#moslink").click(function(){
                var mediatypeid = getUrlParameter('mediatypeid') ? getUrlParameter('mediatypeid') : '';
                var priceslider = getUrlParameter('priceslider') ? getUrlParameter('priceslider') : '';
                var filterid = getUrlParameter('filterid') ? getUrlParameter('filterid') : '';
                var textsearch = getUrlParameter('textsearch') ? getUrlParameter('textsearch') : '';
                //url = '<?php echo Yii::app()->urlManager->createUrl("listing/index?"); ?>mediatypeid='+ mediatypeid +'&priceslider='+ priceslider +'&filter='+ filterid +"&textsearch=" + textsearch +'&geoloc='+getUrlParameter('geoloc')+'&proximity='+getUrlParameter('proximity')+'&lt=m';
                url = '<?php echo Yii::app()->urlManager->createUrl("s/mosaicview?"); ?>mediatypeid='+ mediatypeid +'&priceslider='+ priceslider +'&filter='+ filterid +"&textsearch=" + textsearch +'&geoloc='+getUrlParameter('geoloc')+'&proximity='+getUrlParameter('proximity')+'&type=m';
                location.href = url;
            });
            
            //mcOptions = { gridSize: gridSize, maxZoom: 14, imagePath: 'https://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclustererplus/images/m' };
            //markerClusterer = new MarkerClusterer(map,[], mcOptions);
            
            // meta keyword update on either from home page or this page search
            var mt = getUrlParameter('mt');
            var mt_this = $('#geocomplete_filter').val();
            mt_this = mt_this.split(",");
            if(mt_this[0].length) {
                //console.log('this page geo - ' + mt_this[0]);
                var meta = $('#pageMetaKeywordText').val();
                meta = meta.split(",").join(" "+mt_this[0]+",");
                //console.log(meta);                
                $("#meta_keywords").attr("content", meta);
            } else if(mt.length) {
                mt = decodeURIComponent(mt);
                var meta = $('#pageMetaKeywordText').val();
                meta = meta.split(",").join(" "+mt+",");
                //console.log(meta);                
                $("#meta_keywords").attr("content", meta);
            }
            
            setTimeout(function(){
                console.log("##1 set marker");                
                setMarker();                
                load_geocomplete();
                /*if(get_next_toload_count()) {
                    lazy_load_markers();
                }*/
            }, 2000);
            
            
        });
        
    </script>

    <?php
    // change Function(Event, ui): Function that gets called on slide stop, but only if the slider position has changed.
    // stop Function(Event, ui): Function that gets called when the user stops sliding.
    // ref - http://stackoverflow.com/questions/11498061/yii-update-clistview-by-dropdownlist    
    // MEDIA TYPE - JS, AJAX    
    // for ajax timeout - http://www.yiiframework.com/wiki/185/clistview-ajax-filtering/
    
    $btnFilter = '$(".updatelisting").click(function(){
                            var clicked_this = $(this);
                            if( clicked_this.is("a")) { 
                                func_updatelisting( clicked_this );                                
                            } else {
                                // make another ajax request to get country lat lng
                                var cur_country = $( "select#countryid option:selected").html();
                                $.ajax({                
                                    type: "POST",
                                    url: "' . Yii::app()->createAbsoluteUrl('ajax/countrylatlng') . '", //url to call.
                                    cache: true,
                                    data: { country: cur_country },
                                    success: function (data) {                                        
                                        //$("#geo_latlng").val(data);
                                        func_updatelisting( clicked_this );
                                        //getCountryName();
                                    }
                                });
                            }                            
                        });';
    Yii::app()->clientScript->registerScript('listFilter', $btnFilter, CClientScript::POS_END);
    
    /* This was working
    Yii::app()->clientScript->registerScriptFile('https://maps.googleapis.com/maps/api/js?v=3.exp&key='.Yii::app()->params['gmapApiKey'].'&libraries=places&sensor=false&callback=initialize', CClientScript::POS_BEGIN);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.geocomplete.js', CClientScript::POS_END);  */

    //Yii::app()->clientScript->registerScriptFile('https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&sensor=false&callback=initialize', CClientScript::POS_BEGIN);
    //Yii::app()->clientScript->registerScriptFile('http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places', CClientScript::POS_END);
        
    //Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/markerclusterer_compiled.js', CClientScript::POS_END);
?>
<?php /*
    <script type="text/javascript">
        function loadScript()
        {
            console.time('script load time');
            var script = document.createElement("script");
            script.type = "text/javascript";
            //script.src = "https://maps.googleapis.com/maps/api/js?key=<?php echo Yii::app()->params['gmapApiKey']; ?>&sensor=true&callback=initialize";
            script.src = "https://maps.googleapis.com/maps/api/js?v=3.exp&key=<?php echo Yii::app()->params['gmapApiKey']; ?>&sensor=false&callback=initialize&libraries=places";
            // &language=ja for language change
            // &region=GB for regional settings
            document.body.appendChild(script);
            
            var script2 = document.createElement("script");
            script2.type = "text/javascript";
            script2.src = "<?php echo Yii::app()->baseUrl . '/js/jquery.geocomplete.js'; ?>";
            document.body.appendChild(script2);
            //load_geocomplete(); console.log('test');
            console.timeEnd('script load time');
            setTimeout(function(){load_geocomplete();},1500);            
        }
 
        window.onload = loadScript; 
        function generateClusterStyles()
        {
            //console.log('generate cluster styles');
            var newType, styleSet;
            styleSet = [];
            newType = {
                textColor: 'white',
                url: defaultClusterMarkerUrl,
                width: 45,
                height: 45
             };
             styleSet.push(newType);
             clusterStyles.push(styleSet);
        }
        function clearClusterMarkers()
        {
            //console.log('clusterer markers');
            if (clusterer !== null) {
                clusterer.clearMarkers();
                clusterer = null;
            }
            if (clusterer === null) {
                //console.log('Clear clusterer marker');
                //console.log(map);
                clusterer = new MarkerClusterer(map, [], {
                    maxZoom: clustererMaxZoom,
                    averageCenter: true,
                    minimumClusterSize: 1,
                    // zoomOnClick: false,
                    styles: clusterStyles[clusterStyle],
                    gridSize: gridSize
                });
            }
            clusterer.clearMarkers();
            for (i = 0; i < allMarkers.length; i++) {                
                clusterer.addMarker(allMarkers[i], true);                
            }
            //clusterer.setMap(null);
            //clusterer.setMap(map);
            //clusterer.redraw();
            //clusterer.repaint();
        }
        function getCountryName()
        {
            var sel_country = $("select#countryid option:selected");
            if (sel_country.val() === '') {
                $('#result_country').html("All Countries");
            } else {
                $('#result_country').html(sel_country.html());
            }
        }
        function fetchBreadCrumb() {
            console.log('fetch breadcrumb');            
            var c = map.getCenter();
            var lat = c.lat();
            lat = lat.toFixed(6);
            var lng = c.lng();
            lng = lng.toFixed(6);
            $.ajax({
                url: "<?php echo Yii::app()->urlManager->createUrl('ajax/reversegeocode'); ?>",
                data: {                    
                    lat: lat,
                    lng: lng
                },
                async: false,
                beforeSend: function() {
                    if(lat && lng) {
                        return true;
                    } else {
                        return false;
                    }
                },
                success: function(data) {
                    obj = JSON.parse(data);                    
                    $('#bc_country').val(obj.country);
                    $('#bc_state').val(obj.state);
                    $('#bc_city').val(obj.city);
                    updateBreadCrumb();
                }
            });            
        }
        function updateBreadCrumb() {
            //console.log('update breadcrumb');
            var country = $('#bc_country').val();
            var state = $('#bc_state').val();
            var city = $('#bc_city').val();
            var bcb = '<ul class="breadcrumb left col-sm-9">';
            bcb += '<li><a href="<?php echo JoyUtilities::getHomeUrl(); ?>">Home</a></li>';
            if(country!='') {
                bcb += '<li class="separator"></li>';
                bcb += '<li><a href="javascript:void(0);" id="bcb_country" class="bcb_geo">'+country+'</a></li>';
            } 
            if(state!='') {
                bcb += '<li class="separator"></li>';
                bcb += '<li><a href="javascript:void(0);" id="bcb_state" class="bcb_geo">'+state+'</a></li>';
            }
            if(city!='') {
                bcb += '<li class="separator"></li>';
                bcb += '<li><a href="javascript:void(0);" id="bcb_city" class="bcb_geo">'+city+'</a></li>';
            }
            bcb += '</ul>';
            
            $('.navbar-collapse').html(bcb);            
            //console.log(breadcrumb);
            document.title = (country === null) ? 'EatAds.com - Simplifying OOH' : country + " " + state + " " + city;
            
            
            // meta keyword update on either from home page or this page search
            var mt = getUrlParameter('mt');
            var mt_this = $('#geocomplete_filter').val();
            mt_this = mt_this.split(",");
            if(mt_this[0].length) {
                console.log('this page geo - ' + mt_this[0]);
                var meta = $('#pageMetaKeywordText').val();
                meta = meta.split(",").join(" "+mt_this[0]+",");
                console.log(meta);                
                $("#meta_keywords").attr("content", meta);
            } else if(mt.length) {
                mt = decodeURIComponent(mt);
                var meta = $('#pageMetaKeywordText').val();
                meta = meta.split(",").join(" "+mt+",");
                console.log(meta);                
                $("#meta_keywords").attr("content", meta);
            }
            
        }
    </script>
    */
