<!DOCTYPE html>


<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6 en" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml" dir="ltr"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7 en" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml" dir="ltr"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8 en" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml" dir="ltr"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9 en" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml" dir="ltr"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js notie en zz_en" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml" dir="ltr"> <!--<![endif]-->

    <head>
        
        <!--<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />-->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="">
        <meta name="language" content="en" />
        <?php // meta keyword and description is setting from controller ?>
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/bootstrap-multiselect.css'); ?>
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/fonts.css'); ?>
        
        <!-- Bootstrap core CSS -->
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/bootstrap.css'); ?>
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/ion.rangeSlider.css'); ?>
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/ion.rangeSlider.skinNice.css'); ?>        
        <!-- Bootstrap theme -->
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/bootstrap-theme.css'); ?>
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/fancyfields.css'); ?>        
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/listViewStyle/listViewPager.css'); ?>
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/custom.css'); ?>

        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/jquery.fancybox.css?v=2.1.5'); ?>
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/jquery.jscrollpane.css'); ?> 
        
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/modernizr.js'); ?>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="<?php echo Yii::app()->baseUrl; ?>/js/html5shiv.js"></script>
          <script src="<?php echo Yii::app()->baseUrl; ?>/js/respond.min.js"></script>
        <![endif]-->
        <!--<script id="redline_js" type="text/javascript">var redline = {}; redline.project_id = 76336252;var b,d;b=document.createElement("script");b.type="text/javascript";b.async=!0;b.src=("https:"===document.location.protocol?"https://data":"http://www")+'.redline.cc/assets/button.js';d=document.getElementsByTagName("script")[0];d.parentNode.insertBefore(b,d);</script>-->
    </head>

    <body id="body">
        <?php $errorCodeArray = Yii::app()->errorHandler->error;
            if($errorCodeArray['code']=='404') {
                ?>
                    <script>
                        dataLayer = [{
                         'Error': '404'
                         }];
                    </script>
        <?php }  ?>
        
        <!-- Google Tag Manager -->         
            <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-W6FQS7"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-W6FQS7');</script>
        <!-- End Google Tag Manager -->
        
        
        <div id="fb-root"></div>
            <?php 
                /*$ratesArray = Yii::app()->cache->get('rates');
                echo '<pre>';
                $ratesArray = Yii::app()->openexchanger->getOpenExchnagerRate();                
                die('');*/
            ?>
            <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        
            // Exchange currency rates 
            <?php 
                $cmObj = new CachedMethods(); 
                $currSymb = Yii::app()->cache->get('curr_symbols');
                if(!$currSymb) {
                    $currSymb = $cmObj->getCurrencySymbols();
                }
                
                $listingMinMaxUsd = Yii::app()->cache->get('min_max_listprice');
                if(!$listingMinMaxUsd) {
                    $listingMinMaxUsd = $cmObj->getMinMaxListPrice();
                }
                
            ?>
            var min_max_listprice_usd = <?php echo json_encode($listingMinMaxUsd);  ?>;
            var all_curr_symbols = <?php echo json_encode($currSymb); ?>;
            <?php                
                    $ratesArray = Yii::app()->openexchanger->getOpenExchnagerRate();                 
            ?>
            var all_currency = <?php echo json_encode($ratesArray->rates); ?>;            
            //console.log('curr - ' + all_currency['INR']);
            function get_sel_cur()
            {
                return $('#selected_currency_sym').html();
            }
            function get_curr_sym(curr)
            {
                return all_curr_symbols[curr];
            }
            function OE_curr_conv(native_curr, new_curr, value)
            {
                var native_curr_rate = all_currency[native_curr];
                var new_curr_rate = all_currency[new_curr];                
                var new_value = Math.round(((1 / native_curr_rate ) * new_curr_rate ) * value);                
                return new_value;
            }            
        </script>
        
        
        <?php //This div is for error message from ajax ?>
        <div class="navbar navbar-inverse navbar-fixed-top" style="position:relative; z-index:999999; display:none;">
            <div class="container">
                <p id="errorContainer"></p>
            </div>
        </div>
        
        <?php if (Yii::app()->user->hasFlash('success')) { ?>
            <div class="navbar navbar-inverse navbar-fixed-top" style="position:relative; z-index:999999">
                <div class="container">
                    <p><?php echo Yii::app()->user->getFlash('success'); ?></p>
                </div>
            </div>
        <?php } ?>

        <?php if (Yii::app()->user->hasFlash('hybridauth-error')) { ?>
            <div class="navbar navbar-inverse navbar-fixed-top" style="position:relative; z-index:999999">
                <div class="container">
                    <p><?php echo Yii::app()->user->getFlash('hybridauth-error'); ?></p>
                </div>
            </div>
        <?php } ?>

        <div class="wrapper">
            <div class="container page-header">                
                <a class="logo" href="<?php echo JoyUtilities::getHomeUrl(); //Yii::app()->getHomeUrl(); ?>"><img width="140" src="<?php echo Yii::app()->baseUrl ; ?>/images/site/logo.svg" alt="logo"></a>
                          
                <p class="slogan">Simplifying OOH</p>
                <div class="head-menu">
                   
                    <?php 
                        if(isset($_COOKIE['IpCurrency'])) {                            
                            $ipCurrency = $_COOKIE['IpCurrency'];
                            $ipCurrency = explode('#', $ipCurrency);
                        } else {                            
                            $ipCurrency[0] = $this->ipCurrencyId;           // default USD 20
                            $ipCurrency[1] = $this->ipCurrencyCode;        // default USD 20
                        }
                    ?>
                    
                    <div class="left" id="currencyDropDown">Currency
                            <div class="dropdown" id="dropdown1">
                                <input type="checkbox" id="drop1" />
                                <label for="drop1" class="dropdown_button">
                                    <span id ='selected_currency_sym' ><?php //echo $ipCurrency[2]; ?></span>
                                    <span id='selected_currency_id' style='display:none;'><?php echo $ipCurrency[0]; ?></span>
                                    <span id='selected_currency'><?php echo $ipCurrency[1]; ?></span>
                                    <span class="arrow"></span>
                                </label>
                                <ul class="dropdown_content" id='currency_dd_ul'>
                                    <li>
                                        <ul>
                                    <?php
                                        // CACHE QUERY
                                        $currencyListArray = LookupBaseCurrency::getBaseCurrencyList();
                                        $cntCurrency = 1;
                                        //echo CHtml::dropDownList('ck_currid', $this->ipCurrencyId, $currencyList, array('empty' => 'Select currency'));
                                        foreach($currencyListArray as $currencyList) {
                                            
                                            if($ipCurrency[0] != $currencyList->id) {
                                    ?>
                                    <li role="presentation" class='currency_dd_li_class'><strong>
                                            <span class='curr_li_sym'><?php // echo $currencyList->currency_symbol; ?></span> <?php //echo $currencyList->id; ?></strong>
                                            <span id='list_curr_<?php echo $currencyList->id; ?>'><?php echo $currencyList->currency_code; ?></span>                                            
                                        </li>
                                        <?php if($cntCurrency % 8 == 0) { print '</ul><ul>'; } $cntCurrency++;?>
                                    <?php }} ?>
                                        </ul>
                                  </li>
                                </ul>
                            </div>
                        </div>
                    <script>
                        // $('#selected_currency_sym').html(get_curr_sym($('#selected_currency').html()));
                        $('.currency_dd_li_class').live("click", function(){
                            var sel_curr_sym = $(this).find('span:nth-child(1)').html();                            
                            var sel_curr = $(this).find('span:nth-child(2)').html();
                            var sel_curr_id = $(this).find('span:nth-child(2)').attr('id');                             
                            sel_curr_id = parseInt(sel_curr_id.replace("list_curr_", ""));                            
                            //console.log('sel - ' + sel_curr_sym + ' ' + sel_curr + ' ' + sel_curr_id);
                            var prev_curr = $('#selected_currency').html();
                            var prev_curr_id = parseInt($('#selected_currency_id').html());
                            var prev_curr_sym = $('#selected_currency_sym').html();
                            //console.log('prev - ' + prev_curr_sym + ' ' + prev_curr + ' ' + prev_curr_id);                            
                            // replace top selected curr
                            $('#selected_currency').html(sel_curr);
                            $('#selected_currency_id').html(sel_curr_id);
                            // $('#selected_currency_sym').html(sel_curr_sym);                            
                            $(this).remove();
                            var check_id;
                            $('.currency_dd_li_class').each(function(){
                                check_id = $(this).find('span:nth-child(2)').attr('id');
                                check_id = parseInt(check_id.replace("list_curr_", ""))
                                if(check_id >= prev_curr_id) {
                                    return false;
                                }
                            });
                            if(check_id==prev_curr_id) {
                                // item already exists, won't happen 
                            } else {                                
                                var cur_rep_html = '<li role="presentation" class="currency_dd_li_class"><strong><span class="curr_li_sym">'+prev_curr_sym+'</span></strong> '+ ' <span id="list_curr_'+prev_curr_id+'">'+prev_curr+'</span></li>';                               
                                //console.log(cur_rep_html);
                                $('span#list_curr_'+check_id).parent().before(cur_rep_html);
                                var d = new Date();
                                d.setTime(d.getTime()+(30 * 24 * 60 * 60 * 1000));
                                var expires = d.toGMTString();
                                var cookie_val = sel_curr_id +'#'+sel_curr;                                
                                document.cookie='IpCurrency='+cookie_val+';expires='+expires+'; path=/';
                                
                                // change currency code
                                $('.dyn_ccode').each(function(){
                                    $(this).html(sel_curr);
                                });
                                // change currency values
                                $('.dyn_price').each(function(){
                                    var base_price;
                                    var base_ccode = $(this).siblings('span').children('span.base_ccode').html();
                                    var base_price_html = $(this).siblings('span').children('span.base_price').html();                                
                                    base_price = parseInt( base_price_html.replace(',', '') ); // remove comma
                                    var curr_price = parseInt( $(this).html() );
                                    // taking the base values to get new values
                                    var new_price = OE_curr_conv(base_ccode, sel_curr, base_price);
                                    // internationalize price with comma
                                    new_price = new_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                    //console.log(base_ccode + ' => ' + sel_curr + ' | ' + base_price + ' => ' + new_price)
                                    
                                    $(this).html(new_price);                                    
                                });
                                // change slider price if on page
                                var min_price, max_price, from_price, to_price;                                 
                                if( $('#adv_search_slider').length > 0 ) {
                                    min_price = OE_curr_conv('USD', sel_curr, min_max_listprice_usd.min);
                                    max_price = OE_curr_conv('USD', sel_curr, min_max_listprice_usd.max);
                                    
                                    $("#slider-range").ionRangeSlider("update", {
                                        min: min_price,      // change min value
                                        max: max_price,      // change max value
                                        from: min_price,                       // change default FROM setting
                                        to: max_price,                         // change default TO setting
                                        step: 1,                         // change slider step
                                        prefix: sel_curr_sym + ' '
                                    });
                                }   
                                else if($('#slider-range').length > 0) {                                    
                                    
                                    min_price = OE_curr_conv('USD', sel_curr, min_max_listprice_usd.min);
                                    max_price = OE_curr_conv('USD', sel_curr, min_max_listprice_usd.max);
                                    
                                    //console.log('min - ' + min_price);
                                    //console.log('max - ' + max_price);
                                    // check if price slider exists in url
                                    // if yes fetch convert update slider and url
                                    var url_params = '?'+window.location.search.substring(1);
                                    var price_slider = getUrlParameter('priceslider');
                                    //console.log(price_slider);
                                    if ( price_slider == false || price_slider == true) { 
                                        // true if its blank
                                        from_price = min_price;
                                        to_price = max_price;
                                        url_params = url_params.replace("&priceslider=", "&priceslider="+from_price+'-'+to_price);
                                    } else {                                        
                                        var price_min_max = price_slider.split("-");
                                        from_price = OE_curr_conv(prev_curr, sel_curr, price_min_max[0]);
                                        to_price = OE_curr_conv(prev_curr, sel_curr, price_min_max[1]);
                                        url_params = url_params.replace("&priceslider="+price_slider, "&priceslider="+from_price+'-'+to_price);
                                    }
                                    //console.log(url_params);
                                    //console.log('from - ' + from_price);
                                    //console.log('to - ' + to_price);
                                    // price slider hidden fields value
                                    $("#slider-range").ionRangeSlider("update", {
                                        min: min_price,      // change min value
                                        max: max_price,      // change max value
                                        from: from_price,                       // change default FROM setting
                                        to: to_price,                         // change default TO setting
                                        step: 1,                         // change slider step
                                        prefix: sel_curr_sym + ' '
                                    });
                                    var stateObj = {price: price_slider};
                                    history.pushState(stateObj, null, url_params);        
                                } else if( $('.list_detail_curr').length > 0) {
                                    var base_price_ld = $('#base_price_ld').html();
                                    base_price_ld = base_price_ld.replace(',', '');
                                    var new_dyn_price_ld = OE_curr_conv($('#base_ccode_id').html(), sel_curr, parseInt(base_price_ld));
                                    new_dyn_price_ld = new_dyn_price_ld.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                    $('.dyn_ccode_ld').each(function(){
                                        $(this).html(sel_curr);
                                    });
                                    $('.dyn_price_ld').each(function(){
                                        $(this).html(new_dyn_price_ld);
                                    });
                                    
                                }                                
                            }                                                        
                            
                        });
                    </script>
                        <?php if(Yii::app()->user->isGuest) { // Guest User ?>                       
                                <div class="left">  
                                    <button onclick="location.href='<?php echo Yii::app()->urlManager->createUrl('account/login'); ?>'" class="btn btn-primary">Login</button>
                                </div>                                
                        <?php } else { // Logged in User ?>
                                <div class="left loggedin">
                                    <span class="h4">
                                        Welcome <?php echo Yii::app()->user->name;?> <a href="<?php echo JoyUtilities::getDashboardUrl(); ?>">(Dashboard)</a> <?php //echo ' ('. JoyUtilities::getUserRole(Yii::app()->user->id).')'; ?>
                                        <br><a href="<?php echo Yii::app()->urlManager->createUrl('account/logout'); ?>">Logout</a>
                                    </span>
                                </div>
                        <?php } ?>
                </div>
            </div> <!-- container page-header ends -->

            <?php /*<div id="mainmenu">
                <?php 
                    // to get user label shown in logout, got from UserIdentity
                    $userLabel = null;
                    if(!Yii::app()->user->isGuest) {                
                        $userLabel = Yii::app()->user->name;
                    }
                    $this->widget('zii.widgets.CMenu',array(
                    'items'=>array(
                        array('label'=>'Home', 'url'=>array('/site/index')),
                        array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
                        array('label'=>'Contact', 'url'=>array('/site/contact')),
                        array('label'=>'Login', 'url'=>array('/account/login'), 'visible'=>Yii::app()->user->isGuest),
                        array('label'=>'Logout ('.$userLabel.')', 'url'=>array('/account/logout'), 'visible'=>!Yii::app()->user->isGuest)
                    ),
                )); ?>
            </div><!-- mainmenu -->*/?>
            
            
            <!-- Fixed navbar -->            
            <?php if(isset($this->breadcrumbs) && count($this->breadcrumbs)):?>
                <div class="navbar navbar-inverse navbar-static-top ">
                    <div class="container">
                        <div class="navbar-collapse ">
                                <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                                    'separator' => '<li class="separator"></li>',
                                    'tagName' => 'ul',
                                    'homeLink' => false,
                                    'links'=>$this->breadcrumbs,
                                    'inactiveLinkTemplate' => '<li>{label}</li>',
                                    'activeLinkTemplate' => '<li><a href="{url}">{label}</a></li>',
                                    'htmlOptions' => array('class' => 'breadcrumb left col-sm-9'),
                                )); ?><!-- breadcrumbs -->                                
                        </div><!--/.nav-collapse -->
                    </div>
                </div>
            <?php endif?>
                
            <!-- Navbar second and main content -->

                <?php echo $content; ?>            

           
            <div id="footer">
                <div class="navbar navbar-second navbar-static-bootom " id="footer_first">
                    <div class="container">
                        <div class="left social_plugins">

                            <div style="float:left; padding-right:5px;">
                                <div class="fb-like" data-href="http://www.facebook.com/EatAds" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
                            </div>
                            
                            <div style="float:left;">
                                <script src="//platform.linkedin.com/in.js" type="text/javascript">
                                    lang: en_US
                                </script>
                                <script type="IN/FollowCompany" data-id="2128549" data-counter="right"></script>
                            </div>
                            
                            <div style="float:left;">
    <!--                                 Place this tag in your head or just before your close body tag. -->
                                <script type="text/javascript" src="https://apis.google.com/js/platform.js"></script>

    <!--                                 Place this tag where you want the +1 button to render. -->
                                <div class="g-plusone" data-align="left"></div>
                            </div>
                        </div>
                        
                        <!-- <div class="left subscribe">                        
                            <label>Get our latest updates</label>                            
                            <input type="text" class="form-control" id="subscribe"  placeholder="Email address" name="subscribe" />                            
                            <img src="<?php // echo Yii::app()->baseUrl.'/images/site/ajax_loader.gif'; ?>" alt="ajax_loader" id="subscribe_loader" style="display:none;"/>
                            <button type="button" class="btn btn-primary" id="subscribe_ok">Submit</button>                            
                        </div> -->                        
                        <div class="subscribe">                        
                            <label>Get our latest updates</label>
                            <form id="subcriber-form" action="<?php echo Yii::app()->urlManager->createUrl("account/emailsuccess"); ?>" method="post">
                                <input type="text" class="form-control" id="subscribe"  placeholder="Email address" name="subscribe" />                                
                                <button type="button" class="btn btn-primary" id="subscribe_submit">Submit</button>
                            </form>
                        </div>
                        <div class="right social">
                            <a href="https://www.linkedin.com/company/eatads" target="_blank" class="linkedin"></a>
                            <a href="https://www.facebook.com/EatAds" target="_blank" class="fb"></a>
                            <a href="https://twitter.com/eatads" target="_blank" class="twitter"></a>
                        </div>
                    </div>
                </div><!-- navbar navbar-second navbar-static-bootom ends -->
                <div class="navbar navbar-inverse navbar-static-bottom ">
                    <div class="container">
                        <div class="left">&copy; <?php echo date('Y'); ?> EatAds.com  - All Rights Reserved</div>
                        <div class="right">
                            <ul class="foot_normal">
                                <li><a href="<?php echo Yii::app()->urlManager->createUrl('staticPages/termsAndCondition'); ?>">Terms & Conditions</a></li>
                                <li><a href="<?php echo Yii::app()->urlManager->createUrl('staticPages/aboutUs'); ?>">About Us</a></li>
                                <li><a href="<?php echo Yii::app()->urlManager->createUrl('staticPages/contactUs'); ?>">Contact Us</a></li>
                                <li><a href="<?php echo Yii::app()->urlManager->createUrl('staticPages/faq'); ?>">FAQ</a></li>
<!--                                <li><a href="<?php echo Yii::app()->urlManager->createUrl('staticPages/help'); ?>">Help</a></li>-->
                                <li><a href="<?php echo Yii::app()->urlManager->createUrl('staticPages/sitemap'); ?>">Sitemap</a></li>
<!--                                <li><a href="<?php echo Yii::app()->urlManager->createUrl('staticPages/career'); ?>">Career</a></li>-->
<!--                                <li><a href="<?php echo Yii::app()->urlManager->createUrl('staticPages/press'); ?>">Press</a></li>-->
                                <li><a href="http://blog.eatads.com/" target="_blank">OutdoorInsider (our Blog)</a></li>
                            </ul>
                            <select class="foot_select">
                                <option>Quicklinks</option>
                                <option value="">Terms & Conditions</option>
                                <option value="">About Us</option>
                                <option value="">Contact Us</option>
                                <option value="">FAQ</option>
                                <option value="">Help</option>
                                <option value="">Sitemap</option>
                                <option value="">Career</option>
                                <option value="">Press</option>
                            </select>
                        </div> <!-- right ends -->
                    </div> <!-- container ends -->
                </div> <!-- navbar navbar-inverse navbar-static-bottom ends -->
            </div> <!-- footer ends -->

        </div><!-- wrapper ends -->
                   
  
        <!-- Bootstrap core JavaScript
         ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <?php /*<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.10.2.custom.min.js"></script>*/?>
        <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
        <?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
        
        
        <!-- Fancy fields js & css files -->
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/fancyfields-1.2.min.js', CClientScript::POS_END); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/ion.rangeSlider.min.js', CClientScript::POS_END); ?>

        <!-- Fancy fields add-on for custom scroll bar -->
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/fancyfields.csb.min.js', CClientScript::POS_END); ?>
        
        <?php //Yii::app()->clientScript->registerScriptFile("http://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.min.js", CClientScript::POS_END); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap.min.js', CClientScript::POS_END); ?>
        <?php //Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/holder.js', CClientScript::POS_END); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/popover.js', CClientScript::POS_END); ?>
        
        <?php // Yii::app()->clientScript->registerScriptFile("http://maps.google.com/maps/api/js?sensor=false&language=en", CClientScript::POS_END); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/tooltip.js', CClientScript::POS_END); ?>

        
        <?php 
        if(!Yii::app()->user->isGuest) { // user is logged in
            if(!JoyUtilities::isUserActiveFromSession()) { // logged in Users only ?>
            <div class="modal fade left-aligned" id="inactiveUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <span class="close"></span>
                        <div class="modal-body">
                            <h4 class="modal-title">Activate your account!</h4>
                            <p>Your activation link has been sent on mail. Please check and activate your account to get complete access of your dashboard. <br><br>If you did not receive the email, please <a href="<?php echo Yii::app()->urlManager->createUrl('account/resendActivationLink'); ?>">click here</a> to resend the activation email.</p>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </div>
            <script>                
                //$('a').live("click", function (e) {
                $('a').on('click', function(e){
                //$('a[rel="activationPopup"]').on('click', function(e){    
                    favFlag = $(this).attr('rel');
                    if( 
                        $(this).text().toLowerCase() == "media plans"
                        || $(this).text().toLowerCase() == "favorites"
                        || favFlag == "activationPopup"                        
                        || $(this).text().toLowerCase() == "add listing" 
                        || $(this).text().toLowerCase() == "mass upload"
                        || $(this).text().toLowerCase() == "listings manager"
                        || $(this).text().toLowerCase() == "listing manager"
                    ) {                        
                        e.preventDefault();
                        $('#inactiveUser').modal({ keyboard: false, backdrop : false });                        
                        return false;
                    } else {                       
                         return;
                    }
                });                
            </script>
        <?php } 
        
            }?>

        <!-- UserVoice JavaScript SDK (only needed once on a page) -->
        <script>(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/7UgEx3dPiHHEPCtt1EtCWg.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})()</script>
        <!-- A tab to launch the Classic Widget -->
        <script>
            UserVoice = window.UserVoice || [];
            UserVoice.push
            ([                
                'set',
                {
                    smartvote_enabled: false,
                    post_idea_enabled: false                    
                }
            ]);
            UserVoice.push
            ([
                'showTab',
                'classic_widget',
                {
                    mode: 'contact',
                    primary_color: '#cc6d00',
                    link_color: '#007dbf',
                    forum_id: 201898,
                    tab_label: 'Feedback & Support',
                    tab_color: '#cc6d00',
                    tab_position: 'middle-right',
                    tab_inverted: false
                }
            ]);
        </script>   
        <script>        
            $('#subscribe_submit').click(function(){
                var sub_email = $('#subscribe').val();                
                var pattern = new RegExp(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/);
                if(!sub_email) {
                    alert('Please enter Email.');
                }else if(pattern.test(sub_email)) {
                    $( "#subcriber-form" ).submit();
                } else {
                    alert('Email address is not valid.');
                }                
            });

        </script>        
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.validate.min.js', CClientScript::POS_END); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap-select.min.js', CClientScript::POS_END); ?>
        <?php //Yii::app()->clientScript->registerScriptFile("http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js"); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/scripts.js', CClientScript::POS_END); ?>        
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END); ?>
    </body>
</html>