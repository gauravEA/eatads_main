<?php
    $theme = Yii::app()->theme;
    $cs = Yii::app()->clientScript;
    $cs->registerCssFile( $theme->getBaseUrl() . '/css/jquery.handsontable.full.css' );
    $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/jquery.handsontable.full.js',CClientScript::POS_END);
?>
<link rel="stylesheet" media="screen" href="http://handsontable.com/lib/jquery-ui/css/ui-bootstrap/jquery-ui.custom.css">
<script src="http://handsontable.com/lib/jquery-ui/js/jquery-ui.custom.min.js"></script>
<script>
    $('body').on('click', 'button[name=send-availability]', function () {
         $.ajax({
                type: 'GET',
                url: '<?php echo Yii::app()->urlManager->createUrl('ajax/sendavailabilitymailer'); ?>',
                data:{
                      'cid' : companyid
                  },
             success:function(data){
                 console.log("adasdasdaa" + data);
                },
                error: function(data) { // if error occured
                      alert("Error occured.please try again");
                      alert(data);
                 }
               });
    });
       $('#send-availability').click(function(){
   console.log("asdasda");
//            event.preventDefault();
             $.ajax({
                type: 'GET',
                url: '<?php echo Yii::app()->urlManager->createUrl('ajax/sendavailabilitymailer'); ?>',
                data:{
                      'cid' : companyid
                  },
             success:function(data){
                 console.log("adasdasdaa" + data);
                },
                error: function(data) { // if error occured
                      alert("Error occured.please try again");
                      alert(data);
                 }
               });
       });     
var data = [
              ['', '', '']        
            ];
      var listings ;      
    $(document).ready(function() {
        $('.navbartoggle').removeClass('active');
        $('#availabilitymanagerlink').addClass('active');
        vendorDetails();
        listings = <?php echo json_encode($data);?>;
    });
 var companyid;   



function vendorDetails() {
   $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->urlManager->createUrl('ajax/vendordetails'); ?>',
            data:{
                  'alias' : $('#alias').text()  },
         success:function(data){
             var json = JSON.parse(data);
             //console.log("adasdasdaa" + json.Companylogo);
             companyid = json.id;
             document.title = json.Vendorname;
             $('#image').html('<img class="vendor-logo" src="' + json.Companylogo + '">');
             $('#vendorName').text(json.Vendorname);
             $('#vendorphone').text(json.Contact.Phonenumber);
            console.log("vendorname" + $('#image').html());
            if (json.autoAvailabilityMailerFlag == 1) {
                 $('#autotrigger').attr('checked', 'checked');
            }    

            },
            error: function(data) { // if error occured
                  alert("Error occured.please try again");
                  alert(data);
             }
           });

           
   }
   

    
</script>    
<div id="alias" style="display: none"><?php echo $this->myvar ?> </div>
<!--<div class="row">
        <div class="col-md-12">
            <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                Also <a>Upload Your Contacts</a> before you can share availability with them.
            </div>
        </div>
    </div>-->
    <div class="container-fluid" id="availability-list">
        <div class="row">
            <div class="col-md-12">
                <h3 class="pull-left list-heading">Availability List</h3>
                <div class="pull-right">
                    <label>
                        <input type="checkbox" id="autotrigger">&nbsp;Send automatically at 11 AM local time&nbsp;&nbsp;
                    </label>
                    <button class="btn btn-primary" id="send-availability" name="send-availability">Send Availability to Contacts</button>
                </div>
            </div>
        </div>
        <div class="row" id="availability-list-table">
            <div class="col-md-12">
                <div class="table-container" id="listings">
                   
                </div>
            </div>
        </div>
    </div>
<!--    <div class="container-fluid table-action">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    <a href="#" class="disabled">Cancel&nbsp;</a>
                    <button type="button" class="btn btn-primary" name="update" >Save</button>
                </div>
            </div>
        </div>
     </div>-->
</body>
<script>

$('#listings').handsontable({
              data: <?php echo json_encode($data);?>,
              minCols: 11,
              //colHeaders: ['MEDIA TYPE','COUNTRY','STATE','CITY','LOCALITY','NAME','LENGTH','WIDTH','SIZE UNIT','PRICE', 'AVAILABILITY DATE'],
      colHeaders:true,
//rowHeaders: true,
              colWidths: [{}, {}, {},{},50,{},{},{},{},{},{}],
              manualColumnResize: true,
              manualRowResize: true,
              stretchH: 'all',
              contextMenu: true,
              maxCols: 100,
              afterChange: function (changes, source) {
                  if (changes) {
                  for (var i = changes.length - 1; i >= 0; i--) {
                      console.log(changes[i][0] + " i ");
                      if(changes[i][1] === 'availabilitydate') {
                          var listing = listings[changes[i][0]];
                          console.log(listing + "listing");
                          //start ajaxcall to update
                           $.ajax({
                                type: 'GET',
                                url: '<?php echo Yii::app()->urlManager->createUrl('ajax/updateavailablity'); ?>',
                                data:{
                                      'listingid' : listing['id'],
                                      'availabilitydate' : changes[i][3]      },
                             success:function(data){
                                 console.log("adasdasdaa" + data);
                             },
                             error: function(data) { // if error occured
                                      alert("Error occured.please try again");
                                      alert(data);
                                 }
                           });
                      }    
                  }
                } 
              },    
              columns: [
            {
                 data : 'mediatype',
                type: 'text'
            }, {
                data : 'country',
            type: 'text'
        }, {
            data : 'state',
            type: 'text'
        }, {
            data : 'city',
            type: 'text'
        }, {
            data : 'locality',
            type: 'text'
        }, {
            data : 'name',
            type : 'text'
        }, {
            data : 'length',
            type: 'numeric'
        }, {
            data : 'width',
            type: 'numeric'
        },{
            data : 'sizeunit',
            type: 'text'
        },{
            data : 'price',
            type: 'numeric'
        },{
            data : 'availabilitydate',
            type: 'date',
            dateFormat: 'dd MM yy'
        } ]
            });
       
</script>
    