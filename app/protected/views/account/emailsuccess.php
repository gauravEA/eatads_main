<div class="modal fade" id="AccountSelectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p>
                        <?php echo $message; ?>
                        <button class="btn btn-lg btn-success" onclick="location.href='<?php echo Yii::app()->urlManager->createUrl('/'); ?>'">Back</button>
                    </p>
                    
                </div>
            </div><!-- /.modal-dialog -->
        </div>
</div>
<script type="text/javascript">
$(function() {
            $('#AccountSelectModal').modal( { keyboard: false, backdrop : false } );        
        });
</script>
