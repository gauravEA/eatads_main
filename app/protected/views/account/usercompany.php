<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */

$this->breadcrumbs=array(
    'Home' => JoyUtilities::getHomeUrl(),
    'Sign Up',
);
?>
<script>    
    $('body').removeAttr('id');
</script>
<!-- Navbar second -->
<div class="navbar navbar-second navbar-static-top ">
    <div class="container">
        <div class="left">
            <h1>Sign Up</h1>
        </div>
        <div class="right">
            <ul> 
                <li class="done"><span>1</span><br/>Account type</li>
                <li class="done"><span>2</span><br/>Basic data</li>
                <li class="third active"><span>3</span><br/>Company data</li>
            </ul>
        </div>
    </div>
</div>
<div class="container main-body">    
    <h2>Fill out the details of your company profile page</h2>
    <div class="row">
        <div class="col-sm-9">                
            <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'user-usercompany-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // See class documentation of CActiveForm for details on this,
                    // you need to use the performAjaxValidation()-method described there.
                    'enableAjaxValidation'=>false,
                        'enableClientValidation'=>true,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>true,
                    ),
                    'htmlOptions' => array('enctype'=>'multipart/form-data'),
                )); ?>

            <div class="col-sm-6 clearfix">
                <label class="top">Company name </label>                 
                <?php echo $form->textField($model,'name', array('class'=>'form-control')); ?>
                <?php echo $form->error($model,'name', array('class'=>'errormessage')); ?>
            </div><div class="clear clearfix"></div>
            <div class="col-sm-12 clearfix">
                <label class="top">A few lines about your company</label>                 
                <?php echo $form->textArea($model,'description', array('class'=>'form-control col-sm-8')); ?>
                <?php echo $form->error($model,'description', array('class'=>'errormessage')); ?>
            </div>
            <div class="col-sm-6 clearfix">
                <label class="top">Website url </label>                 
                <?php echo $form->textField($model,'websiteurl', array('class'=>'form-control', 'placeholder' => "http://www.example.com")); ?>
                <?php echo $form->error($model,'websiteurl', array('class'=>'errormessage')); ?>
            </div>
            <div class="col-sm-6">
                <label class="top">Phone number </label>                 
                <?php echo $form->textField($model,'phonenumber', array('class'=>'form-control')); ?>
                <?php echo $form->error($model,'phonenumber', array('class'=>'errormessage')); ?>
            </div>
            <div class="hr clearfix"></div>
            <div class="col-sm-6 clearfix">
                <label class="top">Country </label>
                <?php echo $form->dropDownList($model,'countryid', $countryList, array('empty'=>'Select country')); ?>
                <?php echo $form->error($model,'countryid', array('class'=>'errormessage')); ?>
            </div>
            <div class="col-sm-6 clearfix">
                <label class="top">State </label>                 
                <?php // echo $form->dropDownList($model,'stateid', array(0 => 'Select state'),
                        echo $form->dropDownList($model,'stateid', $stateList, array('empty' => 'Select state') ); ?>
                <?php echo $form->error($model,'stateid', array('class'=>'errormessage')); ?>
            </div><div class="clear clearfix"></div>
            <div class="col-sm-6 clearfix">
                <label class="top">City </label>                 
                <?php echo $form->dropDownList($model,'cityid', $cityList, array('empty'=>'Select city') ); ?>
                <?php echo $form->error($model,'cityid', array('class'=>'errormessage')); ?>
            </div><div class="clear clearfix"></div>
            <div class="col-sm-6">
                <label class="top">Address </label>                 
                <?php echo $form->textField($model,'address1', array('class'=>'form-control')); ?>
                <?php echo $form->error($model,'address1', array('class'=>'errormessage')); ?>
            </div><div class="clear clearfix"></div>
            <div class="col-sm-6">
                <label class="top"> </label>                 
                <?php echo $form->textField($model,'address2', array('class'=>'form-control')); ?>
                <?php echo $form->error($model,'address2', array('class'=>'errormessage')); ?>
            </div><div class="clear clearfix"></div>
            <div class="col-sm-6">
                <label class="top">Zip </label>                 
                <?php echo $form->textField($model,'postalcode', array('class'=>'form-control')); ?>
                <?php echo $form->error($model,'postalcode', array('class'=>'errormessage')); ?>
            </div><div class="clear clearfix"></div>
            <div class="hr clearfix"></div>

            <div class="col-sm-6 clearfix">
                <label class="top">Facebook profile</label>                 
                <?php echo $form->textField($model,'facebookprofile', array('class'=>'form-control', 'placeholder' => "http://www.facebook.com")); ?>
                <?php echo $form->error($model,'facebookprofile', array('class'=>'errormessage')); ?>
            </div>
            <div class="col-sm-6 clearfix">
                <label class="top">Twitter handle</label>                 
                <?php echo $form->textField($model,'twitterhandle', array('class'=>'form-control')); ?>
                <?php echo $form->error($model,'twitterhandle', array('class'=>'errormessage')); ?>
            </div>
            <div class="col-sm-6 clearfix">
                <label class="top">Linkedin profile</label>                 
                <?php echo $form->textField($model,'linkedinprofile', array('class'=>'form-control', 'placeholder' => "http://www.linkedin.com")); ?>
                <?php echo $form->error($model,'linkedinprofile', array('class'=>'errormessage')); ?>
            </div>
            <div class="col-sm-6 clearfix">
                <label class="top">Google+ profile</label>                 
                <?php echo $form->textField($model,'googleplusprofile', array('class'=>'form-control', 'placeholder' => "http://plus.google.com")); ?>
                <?php echo $form->error($model,'googleplusprofile', array('class'=>'errormessage')); ?>
            </div>
            <div class="hr clearfix"></div>
            
            <div class="col-sm-6 clearfix">
                <label class="top">Logo</label> 
                <?php echo $form->fileField($model, 'logo', array('class'=>'')); ?>
                <?php echo $form->error($model, 'logo', array('class'=>'errormessage')); ?>
            </div>

            <div class="col-sm-6 ">
                <?php if($model->logo == '') { ?> 
                    <div style="float:left; padding-right:5px;">
                        No logo uploaded.
                    </div>
                <?php } else { ?>
                    <div style="float:left; padding-right:5px;">
                        <img src="<?php echo JoyUtilities::getAwsFileUrl($model->logo, 'companylogo'); ?>" alt="companylogo"/>
                        <div class="deleteLogo" id="<?php echo $model->id; ?>" rel="<?php echo $model->logo; ?>" style="cursor:pointer;">X</div>
                    </div>
                <?php } ?>
            </div>

            
            <div class="col-sm-12">                
                <?php echo CHtml::submitButton('Sign Up', array('class'=>'btn btn-lg btn-success btn-clear')); ?> 
                <?php switch ($userRoleType) { 
                        case 2: $userRole = 'Media Buyer'; break;
                        case 3: $userRole = 'Media Owner'; break;
                        case 4: $userRole = 'Third Party'; break;
                } ?>
                By clicking Sign Up as <?php echo $userRole; ?>, you agree to EatAds's <a href="<?php echo Yii::app()->urlManager->createUrl('staticPages/termsAndCondition'); ?>" target="_blank">Terms & Conditions</a>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
    
<!-- content container will end in layout -->
</div>
<script>
    
    
    $( '.deleteLogo' ).click(function(){
        var fileName = $( this ).attr('rel');
        var cId = $( this ).attr('id');
        var currItem = $( this );

        $.ajax({
            type: 'POST',
            url:  '<?php echo Yii::app()->urlManager->createUrl('user/profile/deleteLogo')?>',
            data: { filename: fileName, cid: cId}
        }) .done(function( msg ) {
            if(msg == 1) {
                currItem.parent().html('No logo uploaded.');
            }
            // 1 for success 0 otherwise
        });
    });
    
    $(function(){
        $('#UserCompany_countryid').fancyfields("bind","onSelectChange", function (input,text,val){
            //alert("Current input - ID : "+input.attr("id") + " , selected text : " + text + " , value : " + val);
            $.ajax({                
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl('ajax/dynamicstates'); ?>", //url to call.
                cache: true,
                data: {id: val},
                success: function (data) {                                        
                    $('#UserCompany_stateid').setOptions(eval(data));                    
                },
            });                
        }); //- See more at: http://www.jqfancyfields.com/examples-docs/#sthash.qwn8FgiY.dpuf
        $('#UserCompany_stateid').fancyfields("bind","onSelectChange", function (input,text,val){
            //alert("Current input - ID : "+input.attr("id") + " , selected text : " + text + " , value : " + val);
            $.ajax({                
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl('ajax/dynamiccities'); ?>", //url to call.
                cache: true,
                data: {id: val},
                success: function (data) {                                        
                    $('#UserCompany_cityid').setOptions(eval(data));                    
                },
            });                
        });
    });
</script>