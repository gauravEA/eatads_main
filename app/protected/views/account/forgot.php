<?php
/* @var $this AccountController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Forgot Password';
$this->breadcrumbs=array(
        'Home' => JoyUtilities::getHomeUrl(),
	'Forgot Password',
);
?>

<!-- Navbar second -->
<div class="navbar navbar-second navbar-static-top ">
    <div class="container">
        <div class="left">
            <h1>Forgot Password?</h1>
        </div>
    </div>
</div>

<div class="container main-body top-gap">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',     // to change id
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>
    
	<div class="row">
        <div class="col-sm-9">
            <div class="col-sm-6 clearfix">
                <label class="top ">Email</label> 
                <?php echo $form->textField($model,'email', array('class'=>'form-control')); ?>                            
                <?php echo $form->error($model,'email', array('class'=>'errormessage')); ?>
                <?php //echo $userMessage ? '<br/>'.$userMessage : null; ?>
            </div>
            <div class="col-sm-12">

            </div>
            <?php echo CHtml::submitButton('Recover', array('class'=>'btn btn-lg btn-warning', 'id'=>'_submit')); ?>
        </div>
	</div>

    <?php $this->endWidget(); ?>
<!-- content container will end in layout -->
