<?php
/* @var $this AccountController */

$this->breadcrumbs=array(
        'Home' => JoyUtilities::getHomeUrl(),
	'Reset Password',
);
?>

<!-- Navbar second -->
<div class="navbar navbar-second navbar-static-top ">
    <div class="container">
        <div class="left">
            <h1>Choose a new password</h1>
        </div>

    </div>
</div>
<div class="container main-body top-gap">
       
    <?php if(isset($invalidLink)) { echo '<h3>Invalid link, please '.CHtml::link('click here', array('account/forgot')).' to resend password reset instructions. </h3>'; } else { ?>

    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'reset-pwd',
            'enableClientValidation'=>true,
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
            ),
        )); ?>
        
    <div class="row">
        <div class="col-sm-9">
            <div class="col-sm-6 clearfix">
                <label class="top big">Your new password </label>                         
                <?php echo $form->passwordField($model,'password', array('class'=>'form-control', 'value'=>'', 'autocomplete'=>'off')); ?>
                <?php echo $form->error($model, 'password', array('class'=>'errormessage')); ?>
            </div>
            <div class="clear clearfix"></div>
            <div class="col-sm-6 clearfix">
                <label class="top">Confirm Password</label>                         
                <?php echo $form->passwordField($model,'confirmPassword', array('class'=>'form-control', 'value'=>'', 'autocomplete'=>'off')); ?>
                <?php echo $form->error($model,'confirmPassword', array('class'=>'errormessage')); ?>	
                <?php echo $form->hiddenField($model,'hiddenHash', array('value'=>$hiddenHashValue)); ?>
            </div>

            <div class="col-sm-12"></div>

            <?php echo CHtml::submitButton('Reset Password', array('class'=>'btn btn-lg btn-success full')); ?>            
        </div>
    </div>
    
    <?php $this->endWidget(); } ?>

<!-- content container will end in layout -->