<?php
/* @var $this AccountController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
    'Home' => JoyUtilities::getHomeUrl(),
    'Login',
);
?>

<script>
    $('#body').addClass('signup');
</script>

<!-- Navbar second -->
<div class="navbar navbar-second navbar-static-top ">
    <div class="container">
        <div class="left">
            <h1>Login with existing account.</h1>
        </div>
        <div class="right">
            <h2>Create your free account.</h2>
        </div>
    </div>
</div>

<div class="container main-body top-gap">
    
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-8 left">
                <div class="login left">
                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'login-form',
                        'enableClientValidation'=>true,
                        'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                        ),
                    )); ?>
                        <div class="col-sm-6 clearfix">
                            <label class="top ">Email</label>
                            <?php echo $form->textField($model,'email', array('class'=>'form-control')); // 'id'=>'new_pw'?>                            
                            <?php echo $form->error($model,'email', array('class'=>'errormessage')); ?>
                        </div><div class="clear clearfix"></div>
                        
                        <div class="col-sm-6 clearfix">
                            <label class="top">Password </label>
                            <?php echo $form->passwordField($model,'password', array('class'=>'form-control')); // 'id'=>'new_pw_confirm'?>
                            <?php echo $form->error($model,'password', array('class'=>'errormessage')); ?>
                        </div>

                        <div class="col-sm-12">
                            <div class="squaredFour">                               
                                <?php echo $form->checkBox($model,'rememberMe', array('id'=>'squaredFour' )); ?>
                                <label for="squaredFour"></label>
                                <?php echo $form->error($model,'rememberMe', array('class'=>'errormessage')); ?>
                            </div>Remember me
                        </div>
                                                
                        <?php echo CHtml::submitButton('Login', array('class'=>'btn btn-lg btn-warning', 'id'=>'_submit')); ?>
                        <?php echo CHtml::link('Forgotten Password?', array('account/forgot'), array('id'=>'login_text'));?>
                <?php $this->endWidget(); ?>
                </div>
            
                <?php /* <div class="login right">
                    <label class="top big big">Or login with your</label>
                    <!--<a href="#" class="modal-sprite sprite-google_plus"></a>
                    <a href="#" class="modal-sprite sprite-linkedin"></a>-->
                    <?php $this->widget('application.modules.hybridauth.widgets.renderProviders'); ?>
                </div> */ ?>
            </div>
            <div class="col-sm-4 right">                
                <button onclick="location.href='<?php echo Yii::app()->urlManager->createUrl('account/signup'); ?>'" class="btn btn-lg btn-success">Sign Up</button>                    
            </div>
        </div>        
    </div>
<!-- content container will end in layout -->