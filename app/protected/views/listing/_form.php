<?php
/* @var $this ListingController */
/* @var $model Listing */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'listing-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'byuserid'); ?>
		<?php echo $form->textField($model,'byuserid'); ?>
		<?php echo $form->error($model,'byuserid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'foruserid'); ?>
		<?php echo $form->textField($model,'foruserid'); ?>
		<?php echo $form->error($model,'foruserid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'length'); ?>
		<?php echo $form->textField($model,'length',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'length'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'width'); ?>
		<?php echo $form->textField($model,'width',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'width'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sizeunit'); ?>
		<?php echo $form->textField($model,'sizeunit'); ?>
		<?php echo $form->error($model,'sizeunit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'basecurrency'); ?>
		<?php echo $form->textField($model,'basecurrency',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'basecurrency'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'price'); ?>
		<?php echo $form->textField($model,'price',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'priceduration'); ?>
		<?php echo $form->textField($model,'priceduration'); ?>
		<?php echo $form->error($model,'priceduration'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'otherdata'); ?>
		<?php echo $form->textField($model,'otherdata',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'otherdata'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'countryid'); ?>
		<?php echo $form->textField($model,'countryid'); ?>
		<?php echo $form->error($model,'countryid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stateid'); ?>
		<?php echo $form->textField($model,'stateid'); ?>
		<?php echo $form->error($model,'stateid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cityid'); ?>
		<?php echo $form->textField($model,'cityid'); ?>
		<?php echo $form->error($model,'cityid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'street'); ?>
		<?php echo $form->textField($model,'street',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'street'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'geolat'); ?>
		<?php echo $form->textField($model,'geolat',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($model,'geolat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'geolng'); ?>
		<?php echo $form->textField($model,'geolng',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($model,'geolng'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'zoomlevel'); ?>
		<?php echo $form->textField($model,'zoomlevel'); ?>
		<?php echo $form->error($model,'zoomlevel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lighting'); ?>
		<?php echo $form->textField($model,'lighting'); ?>
		<?php echo $form->error($model,'lighting'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mediatypeid'); ?>
		<?php echo $form->textField($model,'mediatypeid'); ?>
		<?php echo $form->error($model,'mediatypeid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datecreated'); ?>
		<?php echo $form->textField($model,'datecreated'); ?>
		<?php echo $form->error($model,'datecreated'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datemodified'); ?>
		<?php echo $form->textField($model,'datemodified'); ?>
		<?php echo $form->error($model,'datemodified'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->