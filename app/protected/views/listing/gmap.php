<?php
/* @var $this ListingController */
/* @var $model Listing */
?>

<script>
    
    
    // global marker
    var marker;
    var map;
//    function moveMarker(lat,lng) {      
//        var newLatLng = new google.maps.LatLng(lat, lng); 
//        marker.setPosition(newLatLng);
//        map.panTo(marker.position);      
//    } 
    function initialize() {
      // Enable the visual refresh
      google.maps.visualRefresh = true;
      var myLatLng;
      var lat = parseFloat($('#Listing_geolat').val());
      var lng = parseFloat($('#Listing_geolng').val());
      if($.isNumeric(lat) && $.isNumeric(lng)) {      
          myLatLng = new google.maps.LatLng(lat, lng);
      } else {
          myLatLng = new google.maps.LatLng(22.84, 78.75);
      }
      
      var mapOptions = {
        zoom: 3,
        center: myLatLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        streetViewControl: false,
        panControl: false,
        mapTypeControl: false
      }
      map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

      //var point=new GLatLng(49.461,-2.58);
//      marker = new google.maps.Marker({
//        //position: point,
//        position: myLatLng,
//        map: map,
//        //title: 'Click to zoom',
//        draggable: true,
//      });

//      google.maps.event.addListener(map, 'center_changed', function() {
//        // 3 seconds after the center of the map has changed, pan back to the marker.
//        window.setTimeout(function() {
//          map.panTo(marker.getPosition());
//        }, 3000);
//      });

      /*google.maps.event.addListener(marker, 'click', function() {
        map.setZoom(8);
        map.setCenter(marker.getPosition());
      });*/

//      google.maps.event.addListener(marker, 'drag', function() {  // or use dragend
//        var curLatLng = marker.getPosition();
//        document.getElementById('Listing_geolat').value = curLatLng.lat().toFixed(6);
//        document.getElementById('Listing_geolng').value = curLatLng.lng().toFixed(6);            
//      });
//      google.maps.event.addListener(marker, 'dragend', function() {  // or use dragend
//        map.panTo(marker.getPosition());            
//      });

    }      
    function loadScript()
    {
      var script = document.createElement("script");
      script.type = "text/javascript";
      script.src = "https://maps.googleapis.com/maps/api/js?key=<?php echo Yii::app()->params['gmapApiKey']; ?>&sensor=false&callback=initialize";
      // &language=ja for language change
      // &region=GB for regional settings
      document.body.appendChild(script);
    }
    window.onload = loadScript;
</script>