<?php
/* @var $this ListingController */
/* @var $dataProvider CActiveDataProvider */
?>
<script type="text/javascript">
    $('#body').addClass('views');
</script>

<div class="navbar navbar-second navbar-static-top ">
    <div class="container">
        <div class="left">
            <!--<h1>Results in <span id="result_country"><?php //echo ($countryName != '') ? $countryName : 'All Countries'; ?></span> - <span id="result_count"><?php echo $dataProvider->totalItemCount; ?></span></h1>-->
            <h1>Results</span> - <span id="result_count"><?php echo $dataProvider->totalItemCount; ?></span></h1>
        </div>
        <?php $this->widget('CreateAccountButton'); ?>
    </div>
</div>
<div class="container main-body">
    <div class="viewNavigation">
        <ul>
            <?php
            $listClass = '';
            $mosClass = '';
            $mapClass = '';
            if ($listType == '') {
                $listClass = 'class="active"';
            } else if ($listType == 'm') {
                $mosClass = 'class="active"';
            } else if ($listType == 'p') {
                $mapClass = 'class="active"';
            }
            echo '<li ' . $mapClass . '>' . CHtml::link("Map view", 'javascript:void(0);', array('class' => 'map-view', 'id' => 'maplink')) . '</li>';
            echo '<li ' . $listClass . '>' . CHtml::link("List view", 'javascript:void(0);', array('class' => 'list updatelisting', 'id' => 'listlink')) . '</li>';
            echo '<li ' . $mosClass . '>' . CHtml::link("Mosaic view", 'javascript:void(0);', array('class' => 'mosaic updatelisting', 'id' => 'moslink')) . '</li>';
            // echo '<li ' . $mapClass . '>' . CHtml::link("Map view", Yii::app()->urlManager->createUrl('map').'?countryid=1&stateid=11&state=Delhi&cityid=515&length=&width=&reach=&sizeunitid=&lightingid=&mediatypeid=1', array('class' => 'map-view', 'id' => 'maplink')) . '</li>';
            // echo '<li ' .$mapClass.'>'.CHtml::link("Map view",  Yii::app()->urlManager->createUrl('listing/index', array_merge($_GET, array('lt'=>'p'))), array('class'=>'map-view')).'</li>';
            // echo '<li ' . $mapClass . '>' . CHtml::link("Map view", Yii::app()->urlManager->createUrl('map').'?countryid='.$_GET['countryid'].'&stateid='.$_GET['stateid'].'&cityid='.$_GET['cityid'].'&length='.$_GET['length'].'&width='.$_GET['width'].'&reach='.$_GET['reach'].'&sizeunitid='.$_GET['sizeunitid'].'&lightingid='.$_GET['lightingid'].'&mediatypeid='.$_GET['mediatypeid'].'&priceslider='.$_GET['priceslider'], array('class' => 'map-view', 'id' => 'maplink')) . '</li>';            
            ?>

        </ul>
    </div>    
    <div class="hr"></div>
    <div class="row">
        <div class="col-sm-9 left">
            <div class="order_by">
                <!-- DROP DOWN FILTER - AJAX FILTER FOR LISTING -->
                <div class="form">
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'list-filter-dropdown',
                        'enableAjaxValidation' => true,
                    ));
                    echo CHtml::dropDownList('filterid', $search_params['filterid'], $dropDownFilter);
                    $this->endWidget();
                    ?>
                </div>
            </div>         
            <div class="hr"></div>
            <div class="row">
                <!-- View comes here -->
                <!-- WHOLE VIEW IS RENDERED & CUSTOMIZED HERE -->
                <?php
                $this->renderPartial('indexGrid', array(
                    'dataProvider' => $dataProvider,
                    'listType' => $listType,
                    'favLink' => $favLink,
                    //'countryName' => $countryName,
                    //'updateBreadcrumb' => $updateBreadcrumb,
                    'pageTitleText' => $pageTitleText,
                    //'pageMetaKeywordText' => $pageMetaKeywordText
                    )
                );
                ?>
                                

            </div>
        </div>
        <div class="right filter-box">
            <div class="col-sm-12 clearfix">
                <div class="map-box" id="map-canvas"></div>
                <?php // echo CHtml::dropDownList('countryid', '', Area::model()->getCountryOptions(), array('prompt'=>'Select country'));  ?>
            </div>

            <div class="col-sm-12 clearfix">                    
                <?php echo CHtml::textField('textsearch', isset($_GET['textsearch'])?$_GET['textsearch']:'', array('placeholder' => 'search in the results', 'class' => 'form-control')) ?>
            </div>
            <div class="col-sm-12 clearfix">                    
                <?php echo CHtml::textField('geocomplete_filter', '', array('placeholder' => 'Type in an address', 'class' => 'form-control geoac')) ?>
            </div>
            <?php /*
            <div class="col-sm-6 clearfix">
                <?php echo CHtml::dropDownList('countryid', ($search_params['countryid'] > 0) ? $search_params['countryid'] : '', Area::model()->getPriorityCountryOptions(), array('empty' => 'Select country')); ?>
            </div>
            <div class="col-sm-6 clearfix" >
                <?php echo CHtml::dropDownList('stateid', ($search_params['stateid'] > 0) ? $search_params['stateid'] : '', ($search_params['countryid'] > 0) ? Area::model()->getStateOptions($search_params['countryid']) : array(), array('empty' => 'Select state')); ?>
            </div>
            <div class="col-sm-6 clearfix">
                <?php echo CHtml::dropDownList('cityid', ($search_params['cityid'] > 0) ? $search_params['cityid'] : '', ($search_params['stateid'] > 0) ? Area::model()->getCityOptions($search_params['stateid']) : array(), array('empty' => 'Select city')); ?>
            </div>
            */ ?>
            <div class="col-sm-12 clearfix mtype">
                <?php echo CHtml::dropDownList('mediatypeid', 
                                    ($search_params['mediatypeid'] > 0) ? $search_params['mediatypeid'] : '', 
                                    MediaType::getPriorityMediaType(),
                                    array('multiple'=>'multiple', 'data-title'=>'Media Type')); ?>
                <?php // echo CHtml::checkboxlist('mediatypeid', $search_params['mediatypeid'], CHtml::listData(MediaType::model()->findAll(), 'id', 'name')); ?>                
            </div>
            
            
            <!-- HTML CODE FOR FILTERING -->
            <div class="col-sm-12 nooverflow"><input type="text" id="slider-range" /></div>

            <div class="col-sm-12 clearfix">                
                <?php echo CHtml::button('Filter', array('class' => 'btn btn-success btn-sm updatelisting', 'id' => 'btn_filter', 'name' => 'btn_filter')); ?>
            </div>
        </div>
        <!-- content container will end in layout -->
    </div>  

    <!-- Favourite Guest Login model -->
    <div class="modal fade left-aligned" id="FavouriteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <span class="close"></span>
                <div class="modal-body">
                    <h4 class="modal-title">Login required as Media Buyer or Third Party!</h4>
                    <p>Please <a href="<?php echo Yii::app()->urlManager->createUrl('account/login', array('rurl' => Yii::app()->request->url)); ?>">click here</a> to login.</p>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
    
    <input type="hidden" id="filterprevval" value="<?php echo $search_params['filterid']; ?>" />    
    <input type="hidden" id="listtype" value="<?php echo $listType; ?>" />
    <input type="hidden" id="geo_latlng" value="<?php echo $geoLatLng; ?>" />
    <input type="hidden" id="userfavlistids" value="" />
    <input type="hidden" id="pageMeta" value="<?php echo $pageMetaKeywordText; ?>" /> 
    <div class="details">
        <input type="hidden" data-geo="country" id="bc_country" value="" />
        <input type="hidden" data-geo="administrative_area_level_1" id="bc_state" value="" />
        <input type="hidden" data-geo="locality" id="bc_city" value="" />
    </div>
     

    <!-- LOAD YII JQUERY & JQUERY CSS FILES FOR SLIDER -->
    <?php // Yii::app()->clientScript->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css'); ?>
    <?php // Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    <?php // Yii::app()->clientScript->registerCoreScript('jquery.ui');  ?>

    <!-- LOAD MAP MARKERS -->
    <div id="dp" style="display:none;"></div>
    <script type="text/javascript">
        function getUrlParameter(param)
        {
            //console.log('get url param');
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++)
            {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == param)
                {
                    if (sParameterName[1] == '') {
                        return true;    // in case of &lt= 
                    } else {
                        return sParameterName[1];   // otherwise
                    }
                }
            }
            return false;   // if param donot exists
        }
        function getlistingcount()
        {
            console.log('get listing count');
            var listing_count = $('#total_listing_count').val();
            if (listing_count) {
                $('#result_count').html(listing_count);
            } else {
                $('#result_count').html(0);
            }

        }
        function getCountryName()
        {
            //console.log('get country name');
            var sel_country = $("select#countryid option:selected");
            if (sel_country.val() === '') {
                $('#result_country').html("All Countries");
            } else {                
                $('#result_country').html(sel_country.html());
            }
        }
        function fetchBreadCrumb() {
            console.log('fetch breadcrumb');            
            var c = map.getCenter();
            var lat = c.lat();
            lat = lat.toFixed(6);
            var lng = c.lng();
            lng = lng.toFixed(6);
            $.ajax({
                url: "<?php echo Yii::app()->urlManager->createUrl('ajax/reversegeocode'); ?>",
                data: {                    
                    lat: lat,
                    lng: lng
                },
                async: false,
                beforeSend: function() {
                    if(lat && lng) {
                        return true;
                    } else {
                        return false;
                    }
                },
                success: function(data) {
                    obj = JSON.parse(data);                    
                    $('#bc_country').val(obj.country);
                    $('#bc_state').val(obj.state);
                    $('#bc_city').val(obj.city);
                    updateBreadCrumb();
                }
            });            
        }
        function updateBreadCrumb() {
            console.log('update breadcrumb');
            var country = $('#bc_country').val();
            var state = $('#bc_state').val();
            var city = $('#bc_city').val();
            var bcb = '<ul class="breadcrumb left col-sm-9">';
            bcb += '<li><a href="<?php echo JoyUtilities::getHomeUrl(); ?>">Home</a></li>';
            if(country!='') {
                bcb += '<li class="separator"></li>';
                bcb += '<li><a href="javascript:void(0);" id="bcb_country" class="bcb_geo">'+country+'</a></li>';
            } 
            if(state!='') {
                bcb += '<li class="separator"></li>';
                bcb += '<li><a href="javascript:void(0);" id="bcb_state" class="bcb_geo">'+state+'</a></li>';
            }
            if(city!='') {
                bcb += '<li class="separator"></li>';
                bcb += '<li><a href="javascript:void(0);" id="bcb_city" class="bcb_geo">'+city+'</a></li>';
            }
            bcb += '</ul>';
            
            
            if (bcb) {
                $('.navbar-collapse').html(bcb);
            }
            /*var breadcrumb = $('#breadCrumbText').val();
            if (breadcrumb) {
                $('.navbar-collapse').html(breadcrumb);
            }*/
            document.title = (country === null) ? 'EatAds.com - Simplifying OOH' : country + " " + state + " " + city;
            
            // meta keyword update on this page search            
            var mt_this = $('#geocomplete_filter').val();
            mt_this = mt_this.split(",");            
            if(mt_this[0].length) {                
                var meta = $('#pageMeta').val();
                meta = meta.split(",").join(" "+mt_this[0]+",");                
                $("#meta_keywords").attr("content", meta);
            }            
        }
        function func_updatelisting(thisvar) {
            //console.log('update listing');
            //var countryid = ($("#countryid").val() > 0) ? $("#countryid").val() : "";
            //var stateid = ($("#stateid").val() > 0) ? $("#stateid").val() : "";
            //var cityid = ($("#cityid").val() > 0) ? $("#cityid").val() : "";
            //var mediatypeid = ($("#mediatypeid").val() > 0) ? $("#mediatypeid").val() : "";
            //var length = $("#length").val();
            //var width = $("#width").val();
            //var sizeunitid = $("#sizeunitid").val();
            //var lightingid = $("#lightingid").val();
            //var reach = $("#reach").val();
            var listtype, url_params, isviewlink;
            var textsearch = $("#textsearch").val();
            var mediatypeid = '';
            $(':checkbox:checked').each(function(i){
                 mediatypeid += $(this).val() +',';
            });
            mediatypeid = mediatypeid.replace(/^,|,$/g,'');
            var price_val_array = $("#slider-range").val().split(";");
            var price_val = price_val_array[0] + "-" + price_val_array[1];
            //console.log(price_val + " " + countryid+" "+ stateid + " "+ cityid + " "+ mediatypeid);
            var filterid = $("select#filterid option:selected").val();
            var geoloc = $("#geo_latlng").val();
            
            if (thisvar.attr('id')=='btn_filter') {     // in case of view changes                
                //listtype = $("#listtype").val();
                url_params = "?mediatypeid=" + mediatypeid + "&priceslider=" + price_val + "&filterid=" + filterid +"&textsearch=" + textsearch +'&geoloc='+getUrlParameter('geoloc');
            } else if(thisvar.attr('id')=='geocomplete_filter'){    // if geo_complete is clicked                
                //listtype = $("#listtype").val();
                url_params = "?mediatypeid=" + mediatypeid + "&priceslider=" + price_val + "&filterid=" + filterid +"&textsearch=" + textsearch +'&geoloc='+geoloc;
            }else {    // if filter button used, retain li class                
                var prevlisttype = $("#listtype").val();
                var a_attr = thisvar.attr("id");
                if (a_attr == "moslink") {
                    $("#listtype").val("m");
                    listtype = "m";                    
                } else if (a_attr == "maplink") {
                    $("#listtype").val("p");
                    listtype = "p";
                } else {
                    $("#listtype").val("");
                    listtype = "";                    
                }
                // url params change only views
                var sPageURL = window.location.search.substring(1);
//                if (getUrlParameter('lt') == false) {
//                    url_params = "?" + sPageURL + "&lt=" + listtype;
//                } else {
//                    sPageURL = sPageURL.replace("&lt=" + prevlisttype, "&lt=" + listtype);
//                    url_params = "?" + sPageURL;
//                }
                if (getUrlParameter('type') == false) {
                    url_params = "?" + sPageURL + "&type=" + listtype;
                } else {
                    sPageURL = sPageURL.replace("&type=" + prevlisttype, "&type=" + listtype);
                    url_params = "?" + sPageURL;
                }
                // check filterid param
                if (getUrlParameter('filterid') == false) {
                    url_params = url_params + "&filterid=" + filterid;
                }
            }

            $.fn.yiiListView.update("listViewId", {
                url: "<?php echo Yii::app()->urlManager->createUrl('listing/indexajax'); ?>" + url_params,
                data: thisvar.serialize(),
                async: false,
                beforeSend: function() {
                    //console.log('before');
                    $(".updatelisting").attr("disabled", true);                    
                    $('.updatelisting').click(function () {return false;});
                },
                complete: function() {
                    //console.log('complete');
                    $(".updatelisting").parent("li").removeClass("active");
                    var listtype = $("#listtype").val();
                    if (listtype == "m") {
                        $("#moslink").parent("li").addClass("active");
                        $("#legend").hide();                        
                        // url_params = 's/mosaicview' + url_params;
                        url_params = 'mosaicview' + url_params;
                    } else if (listtype == "p") {
                        $("#maplink").parent("li").addClass("active");
                        $("#legend").show();
                    } else {
                        $("#listlink").parent("li").addClass("active");
                        $("#legend").hide();                        
                        // url_params = 's/listingview' + url_params;
                        url_params = 'listingview' + url_params;
                    }
                    $(".updatelisting").removeAttr("disabled");                     
                                        
                    var stateObj = {geoloc: geoloc};
                    history.pushState(stateObj, null, url_params);
                }
            });
            return false;
        }

        // read from hidden input, and mark listing fav
        function mark_favorite() {
            console.log('mark fav');
            $('a.add2fav').each(function() {

                var input_list_array = $("#userfavlistids").val();
                var list_array = input_list_array.split(",");
                var listing_id = $(this).attr("id");
                if (list_array.indexOf(listing_id) >= 0) {
                    $(this).css('background-image', 'url(<?php echo Yii::app()->getBaseUrl(); ?>/images/site/star_active.png)');
                }
            });
        }
        function load_geocomplete()
        {
            $("#geocomplete_filter").geocomplete({details: ".details", detailsAttribute: "data-geo"})
            .bind("geocode:result", function(event, result){
                $('#textsearch').val('');
                $('#geomsg').html("Result: " + result.formatted_address);
                //console.log('geocomplete');
                var lat = parseFloat(result.geometry.location.lat()).toFixed(6);
                var lng = parseFloat(result.geometry.location.lng()).toFixed(6);
                var location = lat + "," + lng;
                $('#geo_latlng').val(location);
                //$('#viewport').val(result.geometry.viewport);
                var vp = result.geometry.viewport;
                if(vp) {
                    //var map = new google.maps.Map(document.getElementById("bigmap-canvas"));
                    //map.fitBounds(result.geometry.viewport);
                    google.maps.event.addListener(map, 'dragend', function() {
                        mapViewMapDrag(map);
                    });                    
                    func_updatelisting($(this));
                } else {
                    //console.log('no bounds');
                    alert('Please try some other location.');
                }
            })
            .bind("geocode:error", function(event, status){
                $('#geomsg').html("ERROR: " + status);
            })
            .bind("geocode:multiple", function(event, results){
                $('#geomsg').html("Multiple: " + results.length + " results found");
            });
        }
        function loadScript()
        {
            var script = document.createElement("script");
            script.type = "text/javascript";
            //script.src = "https://maps.googleapis.com/maps/api/js?key=<?php echo Yii::app()->params['gmapApiKey']; ?>&sensor=true&callback=initialize";
            script.src = "https://maps.googleapis.com/maps/api/js?v=3.exp&key=<?php echo Yii::app()->params['gmapApiKey']; ?>&sensor=false&callback=initialize&libraries=places";
            //
            // &language=ja for language change
            // &region=GB for regional settings
            document.body.appendChild(script);
            
            var script2 = document.createElement("script");
            script2.type = "text/javascript";
            script2.src = "<?php echo Yii::app()->baseUrl . '/js/jquery.geocomplete.js'; ?>";
            document.body.appendChild(script2);
            //load_geocomplete(); console.log('test');
            setTimeout(function(){load_geocomplete(); },1000);
        }
        window.onload = loadScript;

        function initialize()
        {
            //console.log('init');
            // map for list and mosaic view
            // Enable the visual refresh
            //google.maps.visualRefresh = true;
            var myLatLng;
            var geo_latlng = $('#geo_latlng').val();
            var geo_latlng_split = geo_latlng.split(",");
            myLatLng = new google.maps.LatLng(geo_latlng_split[0], geo_latlng_split[1]);

            // get markers from div "dp", and converto array
            var mapLocation = JSON.parse("[" + $('#dp').html() + "]");
            //console.log(mapLocation);
            var mapOptions = {
                zoom: 3,
                center: myLatLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                streetViewControl: false,
                panControl: false,
                mapTypeControl: false
            };

            var icons = {
                exact: {
                    name: 'Exact location',
                    icon: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/markerred2.png'
                },
                approx: {                    
                    name: 'Approximate location',
                    icon: '<?php echo Yii::app()->getBaseUrl(); ?>/images/site/markerblue2.png'
                }
            };
                        
            $("#downPager").show();
            $("#map-canvas").show();
            map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
            setMarker(map, mapLocation, false); // info window false
            //mark_favorite();
            
            /*
            // Styled map
            var styles = [
                {
                  stylers: [
                    { saturation: -60 }
                  ]
                }
              ];
            var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});
            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId('map_style');
            var legend = document.getElementById('legend');
            legend.innerHTML = '';            
            for (var key in icons) {
                var type = icons[key];
                var name = type.name;
                var icon = type.icon;
                var div = document.createElement('div');
                div.innerHTML = '<img src="' + icon + '"> ' + name;
                legend.appendChild(div);
            }*/
            
            function setMarker(map, mapLoc, info) {
                console.log('set marker');
                var counterVar = $('.counter').html();                
                var icon;
                var infowindow = new google.maps.InfoWindow();
                if(counterVar && counterVar.length>2)
                    icon='d_map_pin_letter_withshadow';
				else
                    icon='d_map_pin_letter';
                var mc = new MarkerClusterer(map);
                var type;
                //console.log(mapLoc);
                for (i = 0; i < mapLoc.length; i++) {
                    /*if(i==5) {
                        break;
                    }*/
                    var loca = mapLoc[i];
                    if(loca[0] != '0.000000' && loca[1]!= '0.000000') {
                                        
                        var myLanLat = new google.maps.LatLng(loca[0], loca[1]);
                        // |66CCFF|FFFFFF - blue
                        var marker_color = '';
                        if(loca[2]==1) {
                            marker_color = '|FF0000|FFFFFF';    // red - accurate
                            type = icons['exact']['icon'];
                            drawPolyCircle = true;
                        } else {
                            marker_color = '|00FF00|FFFFFF';    // green - approx
                            type = icons['approx']['icon'];
                            drawPolyCircle = false;
                        }
                        
                        // Draw marker
                        var marker = new google.maps.Marker({
                            position: myLanLat,
                            icon: type,
                            /*   shadow: 'https://chart.googleapis.com/chart?chst=d_map_pin_shadow',*/
                            map: map
                                    //tittle: loca[0],
                                    //zIndex: loca[3]
                        });
                        if (info) {
                            var content = $('#map_content_' + counterVar).html();
                            google.maps.event.addListener(marker, 'click', (function(marker, content, infowindow) {
                                return function() {
                                    infowindow.setContent(content);
                                    infowindow.open(map, marker);
                                };
                            })(marker, content, infowindow));
                        }
                        counterVar++;
                    }                    
                }
                // map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);
            }
            fetchBreadCrumb();
        }

        // get markers from hidden geo field in _view & _mview
        function getMarkers() {
            console.log('get marker');
            var sum = '';
            $('.listing_geoloc').each(function() {
                sum += this.value + ',';
                //console.log ('sum ' + sum);
            });
            sum = sum.substring(0, sum.length - 1);
            //sum += ']';
            //$('#dp').replaceWith("<?php //foreach($dataProvider->data as $data) { echo $data->geolat . ", "; }   ?>");
            $('#dp').html(sum);
            if (typeof google === 'object' && typeof google.maps === 'object') {                
                initialize();
            }            
            mark_favorite();
        }
        getMarkers();
      
    </script>
    
    <?php 
    $minMaxListPriceUsd = Yii::app()->cache->get('min_max_listprice');
    if(!$minMaxListPriceUsd) {
        $cmObj = new CachedMethods(); 
        $minMaxListPriceUsd = $cmObj->getMinMaxListPrice();
    }
    
    $minMaxListPriceNew = array(
        'min' => round(Yii::app()->openexchanger->convertCurrency($minMaxListPriceUsd['min'], 'USD', $this->ipCurrencyCode)),
        'max' => round(Yii::app()->openexchanger->convertCurrency($minMaxListPriceUsd['max'], 'USD', $this->ipCurrencyCode))
    );
    
    $fromValue = (isset($search_params['priceslider'][0]) && $search_params['priceslider'][0] > 0) ? $search_params['priceslider'][0] : $minMaxListPriceNew['min'];
    $toValue = (isset($search_params['priceslider'][1]) && $search_params['priceslider'][1] > 0) ? $search_params['priceslider'][1] : $minMaxListPriceNew['max'];
?>
    <script type="text/javascript">
        $(document).on('click', 'a.bcb_geo', function(){
            var click_id = $(this).attr('id');
            var search;
            if(click_id=='bcb_country') {
                search = $(this).text();
            } else if(click_id=='bcb_state') {
                search = $(this).text() + ', ' + $('#bcb_country').text();
            } else {
                search = $(this).text() + ', ' + $('#bcb_state').text() + ', ' + $('#bcb_country').text();
            }            
            $("#geocomplete_filter").val(search).trigger("geocode");
            return false;
        });
        $(function() {
            /* $("a.bcb_geo").live("click",function(){
                console.log('a clicked');
                $("#geocomplete_filter").val($(this).text()).trigger("geocode");
                return false;
            }); */
            
            $("#slider-range").ionRangeSlider({
                min: <?php echo $minMaxListPriceNew['min']; ?>,
                max: <?php echo $minMaxListPriceNew['max']; ?>,
                from: <?php echo $fromValue; ?>,
                to: <?php echo $toValue; ?>,
                type: "double",
                step: 1,
                prefix: get_sel_cur() + ' ',
                prettify: true,
                hasGrid: false
            });
            $('#mediatypeid').multiselect({
                includeSelectAllOption: true,
                enableFiltering: false,
                maxHeight: 250
            });
            
            $('#filterid').fancyfields("bind", "onSelectChange", function(input, text, val) {
                $.fn.yiiListView.update('listViewId', {
                    data: $('#filterid').serialize(),
                    complete: function() {
                        getMarkers();
                        mark_favorite();

                        var filterid = $('#filterid').val();
                        var url_params;
                        var sPageURL = window.location.search.substring(1);
                        if (getUrlParameter('filterid') == false) {
                            url_params = "?" + sPageURL + "&filterid=" + filterid;
                        } else {
                            var prevval = $('#filterprevval').val(); // to get prevval and replace url string                                    
                            sPageURL = sPageURL.replace("&filterid=" + prevval, "&filterid=" + filterid);
                            url_params = "?" + sPageURL;
                        }
                        $('#filterprevval').val(filterid);
                        var stateObj = {filterid: filterid};
                        history.pushState(stateObj, null, url_params);
                    }
                });
            });

            // add favorite star
            // $('a.add2fav').live('click', function() {
            $(document).on('click', 'a.add2fav', function(){
                //console.log('clicked');
                var clicked_fav_link = $(this);
                var listing_id = clicked_fav_link.attr("id");
                // console.log( listing_id );

                if (!<?php echo Yii::app()->user->isGuest ? true : 0; ?>) {

                    // if user logged in (Media buyer & Third party)
                    // console.log('user');                    
                    $.ajax({
                        type: "GET",
                        url: "<?php echo Yii::app()->urlManager->createUrl('ajax/favlistingtoggle'); ?>",
                        async: false,
                        data: {
                            id: "<?php echo Yii::app()->user->id; ?>",
                            listingid: listing_id
                        },
                        success: function(data) {
                            var input_list_array = $("#userfavlistids").val();
                            var list_array = input_list_array.split(",");
                            var index = list_array.indexOf(listing_id);
                            // check if returned true
                            if (data == 1) {
                                // fav added show golden star
                                clicked_fav_link.css('background-image', 'url(<?php echo Yii::app()->getBaseUrl(); ?>/images/site/star_active.png)');
                                if (index < 0) {
                                    $("#userfavlistids").val($("#userfavlistids").val() + "," + listing_id);
                                }
                            } else if (data == 0) {
                                // fav added show silver star
                                clicked_fav_link.css('background-image', 'url(<?php echo Yii::app()->getBaseUrl(); ?>/images/site/star.png)');
                                if (index >= 0) {
                                    list_array.splice(index, 1);
                                    $("#userfavlistids").val(list_array);
                                }
                            }                            
                        }
                    });
                } else {
                    // if not logged in then show login modal
                    // console.log('guest');
                    // show login message modal
                    $('#FavouriteModal').modal('show');
                }
            });
            
            // if user logged in, get his fav listing, mark them as fav
            if (!<?php echo Yii::app()->user->isGuest ? true : 0; ?>) {
                $.ajax({
                    type: "GET",
                    url: "<?php echo Yii::app()->urlManager->createUrl('ajax/userfavlistids'); ?>",
                    data: {
                        id: "<?php echo Yii::app()->user->id; ?>"
                    },
                    success: function(data) {
                        var list_array = data.split(",");
                        $("#userfavlistids").val(list_array);
                        mark_favorite();
                    }
                });
            }            
        });
                
        $("#maplink").click(function(){
            var mediatypeid = getUrlParameter('mediatypeid') ? getUrlParameter('mediatypeid') : '';
            var priceslider = (getUrlParameter('priceslider')==true || getUrlParameter('priceslider')=='') ? '' : getUrlParameter('priceslider');
            var filterid = getUrlParameter('filterid') ? getUrlParameter('filterid') : '';
            var textsearch = (getUrlParameter('textsearch')==true || getUrlParameter('textsearch')=='') ? '' : getUrlParameter('textsearch');
            // url = '<?php // echo Yii::app()->urlManager->createUrl("map/index?"); ?>mediatypeid='+ mediatypeid +'&priceslider='+ priceslider +'&filter='+ filterid +'&textsearch='+ textsearch +'&geoloc='+getUrlParameter('geoloc')+'&proximity='+getUrlParameter('proximity');
            url = '<?php echo Yii::app()->urlManager->createUrl("s/mapview?"); ?>mediatypeid='+ mediatypeid +'&priceslider='+ priceslider +'&filter='+ filterid +'&textsearch='+ textsearch +'&geoloc='+getUrlParameter('geoloc')+'&proximity='+getUrlParameter('proximity');
            location.href = url;
        });
    </script>

    <?php
    // change Function(Event, ui): Function that gets called on slide stop, but only if the slider position has changed.
    // stop Function(Event, ui): Function that gets called when the user stops sliding.
    // ref - http://stackoverflow.com/questions/11498061/yii-update-clistview-by-dropdownlist
    
    // MEDIA TYPE - JS, AJAX    
    // for ajax timeout - http://www.yiiframework.com/wiki/185/clistview-ajax-filtering/
    $btnFilter = '$(".updatelisting").click(function(){
                            var clicked_this = $(this);
                            if( clicked_this.is("a")) { 
                                func_updatelisting( clicked_this );                                
                            } else {
                                // make another ajax request to get country lat lng
                                /*var cur_country = $( "select#countryid option:selected").html();
                                $.ajax({                
                                    type: "POST",
                                    url: "' . Yii::app()->createAbsoluteUrl('ajax/countrylatlng') . '", //url to call.
                                    cache: true,
                                    data: { country: cur_country },
                                    success: function (data) {                                        
                                        $("#geo_latlng").val(data);
                                        func_updatelisting( clicked_this );
                                        //getCountryName();
                                    }
                                });*/                                
                                func_updatelisting( clicked_this );
                                //getCountryName();
                            }
                            
                        });';
    Yii::app()->clientScript->registerScript('listFilter', $btnFilter, CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/markerclusterer_compiled.js', CClientScript::POS_END);   