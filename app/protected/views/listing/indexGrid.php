<?php                    
    if($listType=='m') { 
        $lt = '_mview';
        $template = '<div>{pager}{summary}</div><div style="clear:both;"></div>{items}<div id="downPager"><div style="clear:both;">{pager}{summary}</div></div>';
    } else { 
        $lt = '_view';
        $template = '<div>{pager}{summary}</div><div style="clear:both;"></div>{items}<div id="downPager"><div style="clear:both;">{pager}{summary}</div></div>';
    } 
    /*$finalString = '';
    if($updateBreadcrumb) { 
        $finalString = $this->widget('zii.widgets.CBreadcrumbs', array(
                'separator' => '<li class="separator"></li>',
                'tagName' => 'ul',
                'homeLink' => false,
                'links'=>$this->breadcrumbs,
                'inactiveLinkTemplate' => '<li>{label}</li>',
                'activeLinkTemplate' => '<li><a href="{url}">{label}</a></li>',
                'htmlOptions' => array('class' => 'breadcrumb left col-sm-9'),
            ),true);

        $finalString = preg_replace('/>\s<+/', '><', $finalString);
    }*/
?>

<?php
    // only list and mosaic view
    // for map view check indexGridMap
    $this->widget('zii.widgets.CListView', array(
        'dataProvider'=>$dataProvider,
        'itemView'=> $lt,    // refers to the partial view named '_post'
        //lets tell the pager to use our own css file
        'pager' => array('cssFile' => Yii::app()->baseUrl . '/css/listViewStyle/listViewPager.css'),
        // 'pagerCssClass' => 'paging',
        'summaryText'=>'Showing {start} - {end} of {count}',
        'pager' => Array(
                    // 'cssFile' => Yii::app()->baseUrl . '/css/listViewStyle/listViewPager.css',
                    'header' => '',
                    'prevPageLabel' => 'Previous',
                    'nextPageLabel' => 'Next',
                    'firstPageLabel'=>'First',
                    'lastPageLabel'=>'Last Page'
        ),
        'emptyText' => '<div align="center">No Records Found.</div><input type="hidden" id="pageTitleText" value="'. htmlspecialchars($pageTitleText) .'" />',
        'template'=> $template,
        'id'=>'listViewId',
        'viewData' => array(
                'favLink'=>$favLink, 
                'totalCount'=>$dataProvider->totalItemCount, 
                //'countryName'=>$countryName, 
                //'breadCrumbText' => $finalString, 
                'pageTitleText' => $pageTitleText, 
                //'pageMetaKeywordText' => $pageMetaKeywordText
            ),
        'sortableAttributes'=>array(
            //'name',
        ),
        //'enablePagination'=> false,
        'afterAjaxUpdate'=>"function(id, data) {            
            getMarkers();
            getlistingcount();            
            fetchBreadCrumb();
        }",
    ));
    // afterAjaxUpdate acts as a calling method for static functions (after ajax call), dynamic values 
    // passed in the functions don't work
    // getlistingcount() function is defined in index.php
    // it uses hidden field with id total_listing_count in _view and _mview