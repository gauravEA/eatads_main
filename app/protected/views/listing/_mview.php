<?php
/* @var $this ListingController */
/* @var $data Listing */
// To calculate listing count
$currentListing = ($widget->dataProvider->getPagination()->pageSize * $widget->dataProvider->getPagination()->getCurrentPage()+1) + $index++;
?>

    <?php if($index==1) { ?>
        <input type="hidden" id="total_listing_count" value="<?php echo isset($totalCount) ? $totalCount : 0; ?>" />
        <input type="hidden" id="breadCrumbText" value='<?php echo isset($breadCrumbText) ? $breadCrumbText : ""; ?>' />
        <input type="hidden" id="pageTitleText" value="<?php echo isset($pageTitleText) ? htmlspecialchars($pageTitleText) : Yii::app()->name; ?>" />
        <input type="hidden" id="pageMetaKeywordText" value="<?php echo isset($pageMetaKeywordText) ? htmlspecialchars($pageMetaKeywordText) : ''; ?>" />        
    <?php } ?>

    <?php 
        if(!empty($data->filename)) {            
            $imagePath = JoyUtilities::getAwsFileUrl('small_'.$data->filename, 'listing');
            if(empty($imagePath)) {
                $imagePath = Yii::app()->baseUrl. '/images/site/img.png';
            }
        } else {
            $imagePath = Yii::app()->baseUrl. '/images/site/img.png';
        }  
    ?>
    
    <div class="left mosaicview" style="background: url(<?php echo $imagePath; ?>) no-repeat top left;">
        <?php if($favLink) { ?>
            <a href="javascript:void(0);" rel="activationPopup" class="add2fav" id="<?php echo $data->id; ?>"></a>
        <?php } ?>
        
        
        <h3><a href='<?php echo Yii::app()->urlManager->createUrl('listing/view', array('id'=>$data->id)); ?>'><?php echo $data->name; ?></a><br/>
            <span>  
                <?php echo CHtml::encode($data->mediatype); ?><br/>
                <?php echo CHtml::encode(JoyUtilities::formatNumber($data->length,2).' x '.JoyUtilities::formatNumber($data->width,2). ' ' . $data->sizeunit); ?><sup>2</sup>
            </span>
        </h3>

        <div class="foot-info">
            <h4><?php echo '<span class="dyn_ccode">' .CHtml::encode($this->ipCurrencyCode).'</span> <span class="dyn_price">'. CHtml::encode(JoyUtilities::formatNumber(Yii::app()->openexchanger->convertCurrency($data->price, $data->basecurrency, $this->ipCurrencyCode))).'</span> / '. $data->priceduration; ?><br/>
                <span><?php echo '<span class="base_ccode">'.CHtml::encode($data->basecurrency).'</span> <span class="base_price">'. CHtml::encode(JoyUtilities::formatNumber($data->price)).'</span> / '.$data->priceduration; ?></span></h4>
            <span class="btn btn-primary counter"><?php echo $currentListing; ?></span>
            <?php 
            $accurateGeolocation = $data->ea ? 1: 0 ;
            echo CHtml::hiddenField("getlatlng", '['.$data->lat.','.$data->lng.','.$accurateGeolocation.']', array('class'=>'listing_geoloc')); ?>
        </div>
    </div>
