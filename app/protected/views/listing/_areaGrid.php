<div class="content_header">
    <h2>
        <!-- display Country/City -->
        <?php if ($countryName) {
            echo 'Listings In ' . $countryName . '';
        } else {
            echo 'Listings By Country';
        } ?>
    </h2>
</div>
<?php $_SESSION['listChar'] = 'abcd'; ?>
<div class="row col-sm-12 left country_listing"> 

    <!-- WHOLE VIEW IS RENDERED & CUSTOMIZED HERE -->
    <?php
    //if(is_numeric(isset($countryid))) { $link = '_country?countryid='.$countryid; } else { $link = '_country'; }
    $this->widget('zii.widgets.CListView', array(
        'id' => 'areaViewId',
        'dataProvider' => $dataProvider,
        'itemView' => '_country', // refers to the partial view named '_post'
        'viewData' => array(// own variables, for citi redirect
            'countryId' => isset($countryId) ? $countryId : 0
        ),
        'template' => "<div>
                            {items}
                        </div>",
        //'enablePagination'=> false,
        'afterAjaxUpdate' => "function(id, data) {
                                    abbrAll();
                                }",
    ));
    ?>
</div>
<?php if($updateBreadcrumb) { 
    $finalString = $this->widget('zii.widgets.CBreadcrumbs', array(
            'separator' => '<li class="separator"></li>',
            'tagName' => 'ul',
            'homeLink' => false,
            'links'=>$this->breadcrumbs,
            'inactiveLinkTemplate' => '<li>{label}</li>',
            'activeLinkTemplate' => '<li><a href="{url}">{label}</a></li>',
            'htmlOptions' => array('class' => 'breadcrumb left col-sm-9'),
        ),true);

    $finalString = preg_replace('/>\s<+/', '><', $finalString);
    ?>

<script type="text/javascript">
    updateBread();
    function updateBread() {
        
        //parentHTML = parentHTML.replace( new RegExp( "\>[\n\t ]+\<" , "g" ) , "><" ); 
        $('.navbar-collapse').html('<?php echo $finalString; ?>');
    }
</script>
<?php } ?>
