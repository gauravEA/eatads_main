<?php
/* @var $this ListingController */
/* @var $model Listing */

/*
  $this->menu=array(
  array('label'=>'List Listing', 'url'=>array('index')),
  array('label'=>'Create Listing', 'url'=>array('create')),
  array('label'=>'Update Listing', 'url'=>array('update', 'id'=>$model->id)),
  array('label'=>'Delete Listing', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
  array('label'=>'Manage Listing', 'url'=>array('admin')),
  ); */
?>

<script type='text/javascript'>
    $('body').attr('class', 'listingpreview');
    $('body').attr('id', '');
    $('body .wrapper').attr('id', 'detailed');
    $('body .wrapper').attr('class', 'views preview1');</script>

<div class="container main-body nopadding v5 v4">
    <div class="left">
        <div class="left">
            <?php
            if (!empty($companyLogo)) {
                $imagePath = JoyUtilities::getAwsFileUrl($companyLogo, 'companylogo');
                if (empty($imagePath)) {
                    $imagePath = Yii::app()->baseUrl . '/images/site/no_logo.png';
                }
            } else {
                $imagePath = Yii::app()->baseUrl . '/images/site/no_logo.png';
            }
            ?>
            <img src="<?php echo $imagePath; ?>" alt="companylogo" class="company" width="63" height="62" />
        </div> 
        <div class="left"><h2><?php echo $model->name; ?></h2>
            <div class="subline">
                <?php echo CHtml::link($companyName, JoyUtilities::getCompanyUrl($userId), array('class' => 'left')); ?>                
                <?php
                // link is available only to Media Owner & Third Party -- JoyUtilities::chopString("Contact ".$companyName, 17)
                if (!Yii::app()->user->isGuest) {
                    if ($roleId == 2 || $roleId == 4) {   // media buyer or third party
                        echo CHtml::link('Contact Vendor', 'javascript:void(0);', array('class' => 'btn btn-primary left', 'rel' => 'activationPopup',  'title' => "Contact " . $companyName, 'onclick' => 'email_model();'));
                    } /* else {
                      echo CHtml::link('Contact Vendor', '', array('class'=>'btn btn-primary', 'title'=>'Please login as Media Buyer/Third party Owner to contact vendor'));
                      } */
                } else {
                    echo CHtml::link('Contact Vendor', 'javascript:void(0);', array('class' => 'btn btn-primary left', 'onclick' => "$('#FavouriteModal').modal('show');", 'title' => 'Please login as Media Buyer/Third party Owner to contact vendor'));
                }
                ?>
            </div>      
        </div>
        <div class="clear"></div>
        <div class="left">
            <div class="viewNavigation">
                <ul>
                    <li  class="" id="map_li_id"><a href="javascript:void(0);" rel="map-canvas">Map</a></li>
                    <?php /* <li  class="" id="street_li_id"><a href="javascript:void(0);" rel="map-canvas3">Street View</a></li> */ ?>
                    <li  class="" id="photo_li_id"><a href="javascript:void(0);" rel="photos">Photos</a></li>
                </ul>
            </div>
        </div>

    </div>
    <div class="right first favourites">
        <?php
        // SAVE TO FAVORITES
        if (Yii::app()->user->isGuest) {
            echo '<a href="javascript:void(0);" id="favourite_link" style="width: 29px;height: 28px;float:right;margin:29px 0 0 20px;right:auto;"><img src="' . Yii::app()->baseUrl . '/images/site/star_big_active.png' . '" alt="star_big_active" /></a>';
        } else if ($roleId == 2 || $roleId == 4) { // media buyer or third party
            if (FavouriteListing::model()->findByAttributes(array('userid' => Yii::app()->user->id, 'listingid' => $model->id))) {
                // rel tag is use to show user activate popup when user is not activated.
                echo '<a href="javascript:void(0);" rel="activationPopup" id="favourite_link" style="width: 29px;height: 28px;float:right;margin:29px 0 0 20px;right:auto;"><img src="' . Yii::app()->baseUrl . '/images/site/star_big.png' . '" alt="star_big" /></a>';
            } else {
                //echo '<a href="#" id="favourite_link" class="btn btn-favourites">Save to favorites</a>';
                echo '<a href="javascript:void(0);" rel="activationPopup" id="favourite_link" style="width: 29px;height: 28px;float:right;margin:29px 0 0 20px;right:auto;"><img src="' . Yii::app()->baseUrl . '/images/site/star_big_active.png' . '" alt="star_big_active" /></a>';
            }
        }
        ?>

        <?php 
            $flagImagePath = Yii::app()->baseUrl . "/images/site/flags/" . $model->country->short_code . ".png";
            $flagPath = "images/site/flags/" . $model->country->short_code. ".png";            
        ?>
        <h4 <?php if ($model->country->short_code && file_exists($flagPath)) { ?>style="background:url(<?php echo $flagImagePath; ?>) no-repeat top left;" <?php } ?>>
            <?php echo $model->country->name . ",<br />" . $model->state->name . ", " . $model->city->name; ?>								
        </h4>
        <div class="clear"></div> 

        <?php
        // link is available only to Media Buyer
        if (!Yii::app()->user->isGuest) {
            if ($roleId == 2) {   // media buyer
                //echo CHtml::link('Contact Vendor', 'javascript:void(0);', array('class' => 'btn btn-primary left', 'title' => "Contact " . $companyName, 'onclick' => 'email_model();'));
                ?>
                <div class="btn-group mmselect">
                    <button type="button" class="multiselect dropdown-toggle btn" data-toggle="dropdown" ><span>Add to plan</span></button>
                    <ul class="multiselect-container dropdown-menu" style="max-height: 245px; ">
                        <li>    
                            <ul>
                                <li><a href="#" rel="activationPopup" class="btn btn-create btn-success"><span>+</span>Create a new plan</a></li>
                                <li class="createplan"><div class="text-search"><input type="text" placeholder="enter a name" title="" id="planName" data-original-title=""><button class="btn btn-search" id="addPlanBtn">Add</button></div></li>
                            </ul>
                        </li>
                        <li style=" max-height:160px;overflow-y: auto; overflow-x: hidden; padding-right:5px;width: 196px;">
                            <ul class="sub">
                                <?php foreach ($planData as $key => $value) { ?>
                                    <li><a href="javascript:void(0);" id="<?php echo $value->id; ?>" rel="activationPopup" onClick="addPlan(<?php echo $value->id; ?>);"><?php echo $value->name; ?></a>  <span id="<?php echo $value->id; ?>_count"><?php echo $value->listcount; ?></span></li>
        <?php } ?>
                            </ul>
                        </li>
                    </ul>
                </div>
                <?php
            }
        } else {
            //echo CHtml::link('Contact Vendor', 'javascript:void(0);', array('class' => 'btn btn-primary left', 'onclick' => "$('#FavouriteModal').modal('show');", 'title' => 'Please login as Media Buyer/Third party Owner to contact vendor'));
            ?>
            <div class="btn-group mmselect">
                <button type="button" title = "Please login as Media Buyer to add plan" onclick = "$('#AddtoplanModal').modal('show');" class="multiselect dropdown-toggle btn" data-toggle="dropdown" ><span>Add to plan</span></button>
            </div>
            <?php
        }
        ?>

        <div class="socialMedia">
            <button class="btn" id="pop" data-toggle="popover" data-container="body" data-toggle="popover"data-html="true" data-placement="left"  data-class="rfp" 
                    data-content='
                    <!-- AddThis Button BEGIN -->
                    <!-- Change icons from https://www.addthis.com/get/sharing?flag=registered&lb=1 -->
                    <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                    <a class="addthis_button_pinterest_share"><img src="<?php echo Yii::app()->baseUrl ?>/images/site/pinterest-32.png" alt="pinterest" /></a>
                    <a class="addthis_button_google_plusone_share"><img src="<?php echo Yii::app()->baseUrl ?>/images/site/google+-32.png" alt="google_plus" /></a>
                    <a class="addthis_button_linkedin"><img src="<?php echo Yii::app()->baseUrl ?>/images/site/linkedin-32.png" alt="linkedin" /></a>
                    <a class="addthis_button_facebook"><img src="<?php echo Yii::app()->baseUrl ?>/images/site/facebook-32.png" alt="facebook" /></a>
                    <a class="addthis_button_twitter"><img src="<?php echo Yii::app()->baseUrl ?>/images/site/twitter-32.png" alt="twitter" /></a>
                    <a class="addthis_button_email"><img src="<?php echo Yii::app()->baseUrl ?>/images/site/email-32.png" alt="email" /></a>

                    <!--<a class="addthis_button_compact"></a>-->
                    <!--<a class="addthis_counter addthis_bubble_style"></a>-->
                    </div>
                    <script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
                    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52a56e4a5b6d8a9d&time="<?php echo time(); ?>></script>
                    <!-- AddThis Button END -->'
                    data-original-title="">
                <img src="<?php echo Yii::app()->baseUrl . '/images/site/share.png'; ?>" alt="share" />
            </button>
        </div>

    </div>
</div>

<div class="fullmap">
    <!--<div id="fullmap"></div>-->
    <div id="map-canvas" style="height:572px; width:992px;margin:0px; padding:0px;"></div>
    <?php
    if ($model->geolat && $model->geolng) {
        $this->widget('GoogleMap', array(
                                        'onlyMap' => true, 
                                        'mapFieldData' => array(
                                            '0' => array(
                                                'fieldid' => 'map-canvas',
                                                'lat' => $model->geolat,
                                                'lng' => $model->geolng,
                                                'showMarker' => 1
                                            )
                                        )));
    }
    /* $imagePath = 'http://maps.google.com/maps/api/staticmap?center='.$model->geolat.','.$model->geolng.'&zoom=13&markers='.$model->geolat.','.$model->geolng.'&size=500x300&sensor=true';
      echo CHtml::image($imagePath, '', array('height'=>300, 'width'=>436)); */
    ?>
    <div id="map-canvas3"></div>
    <div class="inlay">
        <h3><?php
            $priceDuration = Listing::getPriceDuration($model->pricedurationid);
            $convertedPrice = CHtml::encode(JoyUtilities::formatNumber(Yii::app()->openexchanger->convertCurrency($model->price, $model->basecurrency->currency_code, $this->ipCurrencyCode)));
            echo '<span class="dyn_ccode_ld">' . $this->ipCurrencyCode . '</span> <span class="dyn_price_ld">' . $convertedPrice . '</span> / ' . $priceDuration;
            ?><br/><span>
                <?php echo CHtml::encode(JoyUtilities::formatNumber($model->length, 2) . ' x ' . JoyUtilities::formatNumber($model->width, 2) . ' ' . Listing::getSizeUnit($model->sizeunitid)); ?><sup>2</sup><br/>
<?php echo Listing::getLighting($model->lightingid); ?></span></h3>
    </div>
    <div id="photos">
        <div class="container">
            <div class="main-image">
                <?php
                // MAIN IMAGE 1
                if (count($model->listingImages)) {
                    $firstBigImage = JoyUtilities::getAwsFileUrl('big_' . $model->listingImages[0]->filename, 'listing');
                    $imageTitle = $model->name;
                    $imageAlt = strlen($model->listingImages[0]->caption) ? $model->listingImages[0]->caption : $imageTitle;
                    ?>
                <a href="<?php echo $firstBigImage; ?>" class="fancybox" data-fancybox-group="gallery" data-gallery="multiimages"><img src="<?php echo $firstBigImage; ?>" id="mainImage" alt="<?php echo $imageAlt; ?>" title="<?php echo $imageTitle; ?>" /></a> <br/>
<?php } ?>

                <h3 class="imgview">view images</h3>
            </div>
            <div class="list">
                <div class="scroll-pane">
                    <?php
                    // THUMBNAILS                              
                    foreach ($model->listingImages as $liImage) {
                        if (!empty($liImage->filename)) {
                            $imagePathBig = JoyUtilities::getAwsFileUrl('big_' . $liImage->filename, 'listing');
                            $imagePath = JoyUtilities::getAwsFileUrl('tiny_' . $liImage->filename, 'listing');
                            if (empty($imagePath)) {
                                $imagePath = Yii::app()->baseUrl . '/images/site/img.png';
                            }
                        } else {
                            $imagePath = Yii::app()->baseUrl . '/images/site/img.png';
                        }
                        $imageText = $model->name;
                        $imageAlt = strlen($liImage->caption) ? $liImage->caption : $imageTitle;
                        echo '<a href="' . $imagePathBig . '" class="thumb"><img src="' . $imagePath . '" alt="'.$imageAlt.'" title="'.$imageTitle.'" /></a>';
                    }
                    ?> 
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-body container">
    <div class="row">
        <div class="left listing">

<?php if (count($model->listingImages)) { ?>
                <div class="images">
                    <div class="list v1">
                        <?php
                        $i = 1;
                        $imagePathBig = '';
                        foreach ($model->listingImages as $liImage) {
                            if (!empty($liImage->filename)) {
                                $imagePath = JoyUtilities::getAwsFileUrl('tiny_' . $liImage->filename, 'listing');
                                $imagePathBig = JoyUtilities::getAwsFileUrl('big_' . $liImage->filename, 'listing');
                                if (empty($imagePath)) {
                                    $imagePath = Yii::app()->baseUrl . '/images/site/img.png';
                                }
                            } else {
                                $imagePath = Yii::app()->baseUrl . '/images/site/img.png';
                            }
                            $imageText = $model->name;
                            $imageAlt = strlen($liImage->caption) ? $liImage->caption : $imageTitle;
                            //echo '<li><img src="'. $imagePath .'" alt="'. $model->name .'" title="Caption comes here" data-description="Description comes here." /></li>';
                            echo '<a href="' . $imagePathBig . '" class="thumb fancybox" data-fancybox-group="gallery1" data-gallery="multiimages" rel="' . $imagePath . '"><img src="' . $imagePath . '" alt="'.$imageAlt.'" title="'.$imageTitle.'" alt="" ></a>';
                            if ($i == 3)
                                break;
                            $i++;
                        }
                        ?>                    
                    </div>
                </div>
<?php } ?>

            <p><?php echo $model->description; ?></p>
        </div>
        <div class="right listing">
            <table>
                <tr><th>Media Type</th>
                    <td><?php echo $model->mediatype->name; ?>
                    </td>
                </tr>

                <tr>
                    <th>Illumination Type
                    </th><td><?php echo Listing::getLighting($model->lightingid); ?></td>
                </tr>
                <tr>
                    <th>Width <?php echo '(' . Listing::getSizeUnit($model->sizeunitid) . ')'; ?>
                    </th><td><?php echo CHtml::encode(JoyUtilities::formatNumber($model->width, 2)); ?></td>
                </tr>
                <tr>
                    <th>Height <?php echo '(' . Listing::getSizeUnit($model->sizeunitid) . ')'; ?>
                    </th><td><?php echo CHtml::encode(JoyUtilities::formatNumber($model->length, 2)); ?></td>
                </tr>
                <tr>
                    <th>Display Charge (<?php echo $priceDuration; ?>)</th>
                    <td class="list_detail_curr"> 
<?php echo '<span class="dyn_ccode_ld">' . $this->ipCurrencyCode . '</span> <span class="dyn_price_ld">' . $convertedPrice . '</span> '; ?>
                    </td>
                </tr>
                <tr>
                    <th>Display Charge (<?php echo $priceDuration; ?>)</th>
                    <td class="list_detail_curr"> 
<?php echo '<span id="base_ccode_id">' . $model->basecurrency->currency_code . '</span> <span id="base_price_ld">' . CHtml::encode(JoyUtilities::formatNumber($model->price)) . '</span>'; ?>
                    </td>
                </tr>


            </table>
            <br/>
            <p>
                <?php
                echo strlen($model->locality) ? $model->locality . ', ' : '';
                echo $model->city->name . ', ' . $model->state->name . ', ' . $model->country->name;
                ?>
            </p>
        </div>
    </div>
    <!-- Email Modal -->
    <div class="modal fade left-aligned" id="MessageSend" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <span class="close"></span>
                <div class="modal-body">
                    <?php /* $form=$this->beginWidget('CActiveForm', array(
                      'id'=>'email-contact-form',
                      'enableAjaxValidation'=>false,
                      )); */ ?>
                    <span class="modal-title h4">Send a message to <?php echo $companyName; ?>!</span>
                    <div class="col-sm-12 clearfix">
<?php echo CHtml::textArea('Email_message', '', array('rows' => 10, 'cols' => 50, 'class' => 'form-control col-sm-8')); ?>                            

                    </div>                    
                    <?php echo CHtml::button('Send your message', array('class' => 'btn btn-lg btn-success', 'id' => 'email_submit', 'disabled' => 'disabled')); ?>
<?php //$this->endWidget();     ?>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
    <!-- Email Success Modal -->
    <div class="modal fade left-aligned" id="ThanksModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <span class="close"></span>
                <div class="modal-body" id="ThanksModalBody">                    
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>

    <!-- Favourite / Contact Vendor Guest Login model -->
    <div class="modal fade left-aligned" id="FavouriteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <span class="close"></span>
                <div class="modal-body">
                    <span class="modal-title h4">Login required as Media Buyer or Third Party!</span>
                    <p>Please <a href="<?php echo Yii::app()->urlManager->createUrl('account/login', array('rurl' => Yii::app()->request->url)); ?>">click here</a> to login.</p>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>

    <!-- Add to plan Guest Login model -->
    <div class="modal fade left-aligned" id="AddtoplanModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <span class="close"></span>
                <div class="modal-body">
                    <span class="modal-title h4">Login required as Media Buyer!</span>
                    <p>Please <a href="<?php echo Yii::app()->urlManager->createUrl('account/login', array('rurl' => Yii::app()->request->url)); ?>">click here</a> to login.</p>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>

</div>

<script type="text/javascript">
    // validation condition
    var st_vw_avail = false;
    var geo_lat = '<?php echo $model->geolat; ?>';
    var geo_lng = '<?php echo $model->geolng; ?>';
    var image_count = '<?php echo count($model->listingImages); ?>';

    if ((geo_lat == 0 && geo_lng == 0) || (geo_lat == '' || geo_lat == '')) {
        if (image_count == 0) {
            $('.viewNavigation, .fullmap').hide();
        } else {
            $('.preview1 .viewNavigation li').removeClass('active');
            $('#photo_li_id').addClass('active');
            $('#map_li_id').find('a').remove();
            $('#street_li_id').find('a').remove();
            $('#map_li_id').html('Map');
            $('#street_li_id').html('Street View');
            $('#map-canvas').hide();
            $('#map-canvas3').hide();
            $('.inlay').hide();
            $('#photos').show();
        }
    } else {
        $('.preview1 .viewNavigation li').removeClass('active');
        $('#map_li_id').addClass('active');
        if (image_count == 0) {
            $('#photo_li_id').removeAttr('class');
            $('#photo_li_id').children('a').remove();
            $('#photo_li_id').html('Photos');
            // find('a').removeAttr('rel');
        }
    }

    function checkStreetViewAvailable()
    {
        // check panorma view
        var center = new google.maps.LatLng(geo_lat, geo_lng);
        var streetViewService = new google.maps.StreetViewService();
        var maxDistanceFromCenter = 50; //meters        
        streetViewService.getPanoramaByLocation(center, maxDistanceFromCenter, function(streetViewPanoramaData, status) {
            if (status === google.maps.StreetViewStatus.OK) {
                st_vw_avail = true;
                return true;
            } else {
                st_vw_avail = false;
                $('#street_li_id').removeAttr('class');
                $('#street_li_id').children('a').remove();
                $('#street_li_id').html('Street View');
                return false;
            }
        });
    }

    function showAddressMap() {
        var geo_position = new google.maps.LatLng(geo_lat, geo_lng);
        var panoramaOptions = {
            position: geo_position,
            pov: {
                heading: 165,
                pitch: 0
            },
            zoom: 1
        };
        var myPano = new google.maps.StreetViewPanorama(
                document.getElementById('map-canvas3'),
                panoramaOptions);
        myPano.setVisible(true);
    }

    $("#Email_message").bind("keyup", function(event, ui) {

        if (!$.trim($(this).val())) {
            $('#email_submit').attr("disabled", "disabled");
        } else {
            $('#email_submit').removeAttr("disabled");
        }

    });
    function email_model() {
        $('#Email_message').val('');
        $('#MessageSend').modal('show');
    }
    $("#Email_message").bind("keyup", function(event, ui) {

        if (!$.trim($(this).val())) {
            $('#email_submit').attr("disabled", "disabled");
        } else {
            $('#email_submit').removeAttr("disabled");
        }

    });
    $('#favourite_link').click(function() {
        // $('#FavouriteModal').modal('show');
        // check if loggedin else redirect
        if (!<?php echo Yii::app()->user->isGuest ? true : 0; ?>) {
            $.ajax({
                type: "GET",
                url: "<?php echo Yii::app()->urlManager->createUrl('ajax/favlistingtoggle'); ?>",
                data: {
                    id: "<?php echo Yii::app()->user->id; ?>",
                    listingid: <?php echo $model->id; ?>
                },
                success: function(data) {
                    // check if returned true
                    var img_html;
                    if (data == 1) {
                        img_html = '<img src="<?php echo Yii::app()->baseUrl; ?>/images/site/star_big.png" alt="star_big" />';
                        $("#favourite_link").html(img_html);
                    } else if (data == 0) {
                        img_html = '<img src="<?php echo Yii::app()->baseUrl; ?>/images/site/star_big_active.png" alt="star_big_active" />';
                        $("#favourite_link").html(img_html);
                    }
                }
            });
        } else {
            $('#FavouriteModal').modal('show');
        }
    });
    $(function() {

        $('.socialMedia').click(function() {
//            var add_this_html =
//            '<div class="addthis_toolbox addthis_default_style addthis_32x32_style">'+
//            '<a class="addthis_button_pinterest_share"><img src="<?php echo Yii::app()->baseUrl ?>/images/site/pinterest-32.png" /></a>'+
//            '<a class="addthis_button_google_plusone_share"><img src="<?php echo Yii::app()->baseUrl ?>/images/site/google+-32.png" /></a>'+        
//            '<a class="addthis_button_linkedin"><img src="<?php echo Yii::app()->baseUrl ?>/images/site/linkedin-32.png" /></a>'+
//            '<a class="addthis_button_facebook"><img src="<?php echo Yii::app()->baseUrl ?>/images/site/facebook-32.png" /></a>'+
//            '<a class="addthis_button_twitter"><img src="<?php echo Yii::app()->baseUrl ?>/images/site/twitter-32.png" /></a>'+
//            '<a class="addthis_button_email"><img src="<?php echo Yii::app()->baseUrl ?>/images/site/email-32.png" /></a>'+            
//            '<a class="addthis_counter addthis_pill_style">'+
//            '</div>';
//
//            //$("#loadhere").html(add_this_html);
//            
//            $('.socialMedia').find('.btn').attr('data-content', add_this_html);
            $.getScript('http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-52a56e4a5b6d8a9d&time=<?php echo time(); ?>',
                    function() {
                        addthis.init(); //callback function for script loading
                    });
        });

        $('.preview1 .viewNavigation li').click(function(e) {
            if (geo_lat != '' && geo_lng != '') {
                e.preventDefault();
                var el = $(this).find('a').attr('rel');
                $('#' + el).show();
                if (el == 'map-canvas') {
                    $('#photos').hide();
                    $('#map-canvas').show();
                    $('#map-canvas3').hide();
                    $('.preview1 .viewNavigation li').removeClass('active');
                    $(this).addClass('active');
                    $('.inlay').show();
                    initialize();
                }
                else if (el == 'photos') {
                    if (image_count > 0) {
                        $('#photos').show();
                        $('#map-canvas').hide();
                        $('#map-canvas3').hide();
                        $('.preview1 .viewNavigation li').removeClass('active');
                        $(this).addClass('active');
                        $('.inlay').hide();
                        // initialize();
                    }
                }
                else if (el == 'map-canvas3') {
                    if (st_vw_avail) {
                        $('#photos').hide();
                        $('#map-canvas').hide();
                        $('#map-canvas3').show();
                        $('.inlay').show();
                        $('.preview1 .viewNavigation li').removeClass('active');
                        $(this).addClass('active');
                        showAddressMap();
                    } else {
                        //alert('Street view not available for this location.');                        
                        $('#photos').hide();
                        $('#map-canvas').show();
                        $('#map-canvas3').hide();
                        $('.inlay').show();
                        initialize();

                    }
                }
                return false;
            }
        });
        setTimeout(function() {
            if (geo_lat != '' && geo_lng != '') {
                checkStreetViewAvailable();
            }
        }, 3000);

//        $('#photos').show();
        $('.scroll-pane').jScrollPane();
//        $('#photos').hide();
        // delegate calls to data-fancybox-group="gallery" data-gallery="multiimages"
        $('.fancybox').fancybox();
        //  $('.socialMedia .btn').popover();
        // for ADD THIS script
        $('[data-toggle="popover"]').popover(function(e) {

        });


        $('#email_submit').click(function() {
            //console.log('email submit') ;
            $.ajax({
                type: "GET",
                url: "<?php echo Yii::app()->urlManager->createUrl('ajax/contactseller'); ?>",
                data: {
                    sellerid: "<?php echo $model->foruserid; ?>",
                    message: $('#Email_message').val()
                },
                success: function(data) {
                    $('#message_modal_body').html('Email has been successfuly sent to the Vendor.');
                    $('#MessageSend').modal('hide');
                    if (data == 1) {
                        msg = '<span class="modal-title h4">Thank you!</span><p>Email sent successfully.</p>';
                    } else if (data == 2) {
                        msg = '<span class="modal-title h4">Oops!</span><p>Please provide a valid message.</p>';
                    } else {
                        msg = '<span class="modal-title h4">Oops!</span><p>Failure, Please try again!</p>';
                    }
                    $('#ThanksModalBody').html(msg);
                    $('#ThanksModal').modal('show');

                }
            });
        });

        $('#addPlanBtn').click(function() {
            var planName = $.trim($('#planName').val());
            if (planName) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Yii::app()->urlManager->createUrl('user/mediaplans/addplan'); ?>",
                    data: {
                        planName: planName,
                        listingId: <?php echo $model->id; ?>
                    },
                    success: function(data) {
                        $('#planName').val('');

                        var str = '<li><a href="javascript:void(0);" id="' + data + '" onClick="addPlan(' + data + ');">' + planName + '</a><span id="' + data + '_count">1</span></li>';
                        $("ul.sub").prepend(str);
                        $('#errorContainer').html('Plan added successfully');
                        $('#errorContainer').parent().parent().show();
                    }
                });
            }
        });
    });

    function addPlan(pid) {
        //$(this).preventDefault();
        var planname = $('#' + pid).text();
        var listingid = <?php echo $model->id; ?>;
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->urlManager->createUrl('user/mediaplans/addInExistingPlan'); ?>",
            data: {
                planId: pid,
                listingId: listingid
            },
            success: function(data) {
                if (data == "error") {
                    $('#errorContainer').html('This listing is already in ' + planname);
                    $('#errorContainer').parent().parent().show();
                } else {
                    var num = parseInt($('#' + pid + '_count').text());
                    $('#' + pid + '_count').html(++num);
                    $('#errorContainer').html('Listing added successfully in ' + planname);
                    $('#errorContainer').parent().parent().show();
                }
            }
        });
    }

</script>
<?php
// CSS commented due to conflict and add directly in main layout
//Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/jquery.jscrollpane.css', CClientScript::POS_HEAD); 
?>
<?php //Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/jquery.fancybox.css?v=2.1.5', CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.jscrollpane.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.fancybox.js?v=2.1.5', CClientScript::POS_END); ?>
<style>
    #footer {
        width:100%;
        height: 95px;
        position:inherit !important;
        bottom:0;
        left:0;
    }
</style>
