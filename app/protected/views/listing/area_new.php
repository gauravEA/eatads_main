<?php
/* @var $this ListingController */
/* @var $model Listing */

?>
<style>
    .optionWidth { width:165px; float:left; }
</style>
<script>
    $('#body').addClass('views');
</script>
        <div class="navbar navbar-second navbar-static-top ">
            <div class="container">
                <div class="left">
                    <h1>Listings</h1>
                </div>
                <?php $this->widget('CreateAccountButton'); ?>
            </div>
        </div>

            <div id="updateView" class="container main-body">
                <div class="content_header">
                    <h2>
                        <!-- display Country/City -->
                        <?php if ($countryName) {
                            echo 'Listings In ' . $countryName . '';
                        } else {
                            echo 'Listings By Country';
                        } ?>
                    </h2>
                </div>
                <?php $_SESSION['listChar'] = 'abcd'; ?>
                <div class="row col-sm-12 left country_listing"> 
                    <?php
                        foreach($countryStateCityArr as $countryId => $stateArr) {
                            foreach($stateArr as $stateId => $cityArr) {
                                
                                echo "<div class='row'>";
                                echo "<h3><a class='csc_geo' href='javascript:void(0);'>". $cityArr['name'] ."</a></h3>";
                                foreach($cityArr as $cityid => $data) {
                                    if($cityid == "name") { continue; }
                                    echo "<div class='optionWidth'>";
                                        echo "<a class='csc_geo' href='javascript:void(0);'>".$data['city']."</a>";
                                    echo "</div>";
                                }
                                echo "</div>";
                            }
                        }
                    
                    ?>
                    
                </div>
                <?php $mediaTypes = ''; 
                        foreach(MediaType::getPriorityMediaType() as $key => $value) {
                            $mediaTypes .=  $key.",";
                        }
                        $mediaTypes = rtrim($mediaTypes,',');
                ?>
                <input type="hidden" id="geocomplete" />
                <input type="hidden" id="geo_latlng" />
                <input type="hidden" id="country" value="<?php echo isset($countryName) ? $countryName: ''; ?>"/>
                <input type="hidden" id="mediatypeids" value="<?php echo $mediaTypes; ?>"/>
                <input type="hidden" id="vp" value=""/>
            </div>
<?php
    $protocol = Yii::app()->params['protocol'];
    Yii::app()->clientScript->registerScriptFile($protocol.'maps.googleapis.com/maps/api/js?sensor=false&libraries=places', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.geocomplete.js', CClientScript::POS_END);  
?>
<script type="text/javascript">
    function calc_proximity()
    {
        // r = radius of the earth in statute miles
        var r = 3963.0;
        // Convert lat or lng from decimal degrees into radians (divide by 57.2958)
        var geolatlng = $('#geo_latlng').val();
        geolatlng = geolatlng.split(",");
        var lat1 = parseFloat(geolatlng[0]) / 57.2958;
        var lon1 = parseFloat(geolatlng[1]) / 57.2958;
        var ne = $('#vp').val();
        ne = ne.split(",");        
        var lat2 = ne[0] / 57.2958;
        var lon2 = ne[1] / 57.2958;
        // distance = circle radius from center to Northeast corner of bounds
        proximity = r * Math.acos(Math.sin(lat1) * Math.sin(lat2) +
                Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1));
        proximity = Math.round(proximity * 1000) / 1000;
        console.log(proximity);
        return proximity;        
    }
    $(function() {
        $('.csc_geo').click(function(e){
            e.preventDefault();
            var search = $(this).text() + ', ';
            search += $(this).parent().siblings('h3').children('a').text();
            var country = $('#country').val();
            if(country.length) {
                search += ", " + country;
            }
            //  console.log(search);
            $("#geocomplete").val(search).trigger("geocode");
            return false;
        });
    
    $("#geocomplete").geocomplete()
        .bind("geocode:result", function(event, result){
            var lat = parseFloat(result.geometry.location.lat()).toFixed(6);
            var lng = parseFloat(result.geometry.location.lng()).toFixed(6);
            var location = lat + "," + lng;
            //console.log(result.geometry.viewport);
            $('#geo_latlng').val(location);
            var vp = result.geometry.viewport.getNorthEast();
            vp = parseFloat(vp.lat()).toFixed(6) + "," + parseFloat(vp.lng()).toFixed(6);
            $('#vp').val(vp);
            if(vp) {
                // create link and redirect
                var geoloc = $('#geo_latlng').val();
                var mediatypeids = $('#mediatypeids').val();
                var mt = $('#geocomplete').val();
                mt = mt.split(",");
                var proximity = calc_proximity();
                // var url = '<?php // echo Yii::app()->urlManager->createUrl("/map/index?"); ?>mediatypeid='+mediatypeids+'&geoloc='+geoloc;
                var url = '<?php echo Yii::app()->urlManager->createUrl("/s/mapview?"); ?>mediatypeid='+mediatypeids+'&geoloc='+geoloc + '&proximity=' + proximity + '&mt=' + mt[0];
                //console.log(url);
                window.location.href = url;
            } else {
                console.log('no bounds');
                alert('Please try some other location.');
            }
        })
        .bind("geocode:error", function(event, status){
          //$.log("ERROR: " + status);
        })
        .bind("geocode:multiple", function(event, results){
          //$.log("Multiple: " + results.length + " results found");
        });
    });
    
</script>
<!-- content container will end in layout -->    
