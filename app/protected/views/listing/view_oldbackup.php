<?php
/* @var $this ListingController */
/* @var $model Listing */

/*
$this->menu=array(
	array('label'=>'List Listing', 'url'=>array('index')),
	array('label'=>'Create Listing', 'url'=>array('create')),
	array('label'=>'Update Listing', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Listing', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Listing', 'url'=>array('admin')),
);*/
?>

<script>
    $('#body').attr('id', 'detailed');
</script>

<div class="navbar navbar-second navbar-static-top ">
    <div class="container">
        <div class="left">
            <h1><?php echo $model->name; ?></h1>
        </div>
<!--        <div class="right favourites">
            <select id="Favorites">
                <option>Favorites</option>
            </select>
        </div>-->
    </div>
</div>
<div class="container main-body">
    <div class="row">
        <div class="left listing">
            <h2><?php echo $model->country->name.", ".$model->state->name.", ".$model->city->name; ?></h2>
            
            
            <?php if($companyId) { ?>
            <div class="subline">
                <div class="left">                    
                    <br />
                    <?php
                        echo CHtml::link($companyName, JoyUtilities::getCompanyUrl($companyId));
                    ?>
                    <!-- CONTACT SELLER BUTTON -->
                    
                    <?php   // link is available only to Media Owner & Third Party -- JoyUtilities::chopString("Contact ".$companyName, 17)
                            if(!Yii::app()->user->isGuest) {
                                if($roleId==2 || $roleId==4){   // media buyer or third party
                                    echo CHtml::link('Contact Seller', 'javascript:void(0);', array('class'=>'btn btn-primary', 'title'=>"Contact ".$companyName, 'onclick'=>'email_model();'));
                                } /* else {
                                    echo CHtml::link('Contact Seller', '', array('class'=>'btn btn-primary', 'title'=>'Please login as Media Buyer/Third party Owner to contact seller')); 
                                }*/
                            } else {
                                    echo CHtml::link('Contact Seller', 'javascript:void(0);', array('class'=>'btn btn-primary', 'onclick' => "$('#FavouriteModal').modal('show');", 'title'=>'Please login as Media Buyer/Third party Owner to contact seller')); 
                            } ?>
                </div>
            </div>
            <?php } ?>
            
            
            <p><?php echo CHtml::encode($model->description); ?></p>
            <table>
                <tr>
                    <th>Size:</th><td><?php echo CHtml::encode(JoyUtilities::formatNumber($model->length,2) .' x '. JoyUtilities::formatNumber($model->width,2) .' '.  Listing::getSizeUnit($model->sizeunitid)); ?><sup>2</sup></td>
                </tr>
                <tr>
                    <th>Lighting:</th><td><?php echo " ".Listing::getLighting($model->lightingid); ?></td>
                </tr>
            </table>
            <?php $priceDuration = Listing::getPriceDuration($model->pricedurationid); ?>
            <h3 class="price"><?php echo $this->ipCurrencyCode ." ". CHtml::encode(JoyUtilities::formatNumber(Yii::app()->openexchanger->convertCurrency($model->price, $model->basecurrency->currency_code, $this->ipCurrencyCode)) .' / '. $priceDuration); ?></h3>
            <h5 class="price"><?php echo $model->basecurrency->currency_code ." ". CHtml::encode(JoyUtilities::formatNumber($model->price) .' / '. $priceDuration); ?></h5>
        </div>
        <div class="right">
            
            <?php // SAVE TO FAVORITES
                if(Yii::app()->user->isGuest) { 
                    echo '<a href="#" id="favourite_link" class="btn btn-favourites">Save to favorites</a>';
                } else if($roleId == 2 || $roleId == 4) { // media buyer or third party
                    if(FavouriteListing::model()->findByAttributes(array('userid'=>Yii::app()->user->id, 'listingid'=>$model->id))) {
                        echo '<a href="#" id="favourite_link" class="btn btn-favourites">Remove favorites</a>';
                    } else {
                        echo '<a href="#" id="favourite_link" class="btn btn-favourites">Save to favorites</a>';
                    }
                } ?>
            
            <div class="clear"></div>
            <?php if( count($model->listingImages) ) { ?>
                <div class="images">

                    <!-- Second, add the Timer and Easing plugins -->
                    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/gallery.jquery.timers-1.2.js'; ?>"></script>
                    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/gallery.jquery.easing.1.3.js'; ?>"></script>

                    <!-- Third, add the GalleryView Javascript and CSS files -->
                    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/gallery.jquery.galleryview-3.0-dev.js'; ?>"></script>
                    <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->baseUrl.'/css/gallery.jquery.galleryview-3.0-dev.css'; ?>" />

                <ul id="myGallery">
                    <?php 
                        foreach($model->listingImages as $liImage) {

                            if (!empty($liImage->filename)) {
                                $imagePath = JoyUtilities::getAwsFileUrl('big_'.$liImage->filename, 'listing');
                                if(empty($imagePath)) {
                                    $imagePath = Yii::app()->baseUrl. '/images/site/img.png';
                                }
                            } else {
                                $imagePath = Yii::app()->baseUrl . '/images/site/img.png';
                            }     
                            echo '<li><img src="'. $imagePath .'" alt="'. $model->name .'" title="Caption comes here" data-description="Description comes here." /></li>';
                            //echo CHtml::image($imagePath, '', array('height'=>300, 'width'=>436, "title" => $model->name));
                        }    
                    ?>
        </ul>
                    <script type="text/javascript">
        $(function(){
            $('#myGallery').galleryView({
            panel_width: 440,
            panel_height: 300,
            //frame_width: 70,
            frame_height: 40,
                    panel_scale : 'fit',
                    enable_overlays : true,
                    show_filmstrip_nav : false
                    //  filmstrip_position : 'bottom'
        });
        });
    </script>


                </div>
            <?php } ?>
            <div id="map-canvas" style="height:300px; width:436px;"></div>
            <?php 
                $this->widget('GoogleMap', array('latitude'=>$model->geolat,'longitude'=>$model->geolng, 'onlyMap'=>true, 'showMarker'=>true));
                /*$imagePath = 'http://maps.google.com/maps/api/staticmap?center='.$model->geolat.','.$model->geolng.'&zoom=13&markers='.$model->geolat.','.$model->geolng.'&size=500x300&sensor=true';
                echo CHtml::image($imagePath, '', array('height'=>300, 'width'=>436)); */
            ?>
            
        </div>
    </div> 
</div>    

    
    <!-- AddThis Button BEGIN -->
        <!-- Change icons from https://www.addthis.com/get/sharing?flag=registered&lb=1 -->
        <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
            <a class="addthis_button_pinterest_share"><img src="<?php echo Yii::app()->baseUrl?>/images/site/pinterest-32.png" /></a>
            <a class="addthis_button_google_plusone_share"><img src="<?php echo Yii::app()->baseUrl?>/images/site/google+-32.png" /></a>
            <a class="addthis_button_linkedin"><img src="<?php echo Yii::app()->baseUrl?>/images/site/linkedin-32.png" /></a>
            <a class="addthis_button_facebook"><img src="<?php echo Yii::app()->baseUrl?>/images/site/facebook-32.png" /></a>
            <a class="addthis_button_twitter"><img src="<?php echo Yii::app()->baseUrl?>/images/site/twitter-32.png" /></a>
            <a class="addthis_button_email"><img src="<?php echo Yii::app()->baseUrl?>/images/site/email-32.png" /></a>

            <!--<a class="addthis_button_compact"></a>-->
            <!--<a class="addthis_counter addthis_bubble_style"></a>-->
        </div>
        <script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52a56e4a5b6d8a9d"></script>
    <!-- AddThis Button END -->
    
    <!-- Email Modal -->
    <div class="modal fade left-aligned" id="MessageSend" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <span class="close"></span>
                <div class="modal-body">
                    <?php /*$form=$this->beginWidget('CActiveForm', array(
                        'id'=>'email-contact-form',
                        'enableAjaxValidation'=>false,
                    ));*/ ?>
                        <h4 class="modal-title">Send a message to <?php echo $companyName; ?>!</h4>
                        <div class="col-sm-12 clearfix">
                            <?php echo CHtml::textArea('Email_message','', array('rows'=>10, 'cols'=>50, 'class'=>'form-control col-sm-8')); ?>                            
                            
                        </div>                    
                        <?php echo CHtml::button('Send your message', array('class'=>'btn btn-lg btn-success', 'id'=>'email_submit', 'disabled'=>'disabled')); ?>
                    <?php //$this->endWidget(); ?>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
    <script>        
        $("#Email_message").bind("keyup", function(event, ui) {
            
            if( !$.trim($(this).val()) ) {
                $('#email_submit').attr("disabled", "disabled");
            } else {                
                $('#email_submit').removeAttr("disabled");
            }
           
        });
    </script>
    <!-- Email Success Modal -->
    <div class="modal fade left-aligned" id="ThanksModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <span class="close"></span>
                <div class="modal-body" id="ThanksModalBody">                    
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
     <!-- Favourite / Contact Seller Guest Login model -->
        <div class="modal fade left-aligned" id="FavouriteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <span class="close"></span>
                    <div class="modal-body">
                        <h4 class="modal-title">Login required as Media Buyer or Third Party!</h4>
                        <p>Please <a href="<?php echo Yii::app()->urlManager->createUrl('account/login', array('rurl'=>Yii::app()->request->url)); ?>">click here</a> to login.</p>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
    <script type="text/javascript">
        function email_model() { 
            $('#Email_message').val('');
            $('#MessageSend').modal('show');
        }        
        $(function(){            
            
            $('#email_submit').click(function(){
               //console.log('email submit') ;
               $.ajax({
                    type: "GET",
                    url: "<?php echo Yii::app()->urlManager->createUrl('ajax/contactseller'); ?>",
                    data: {
                        sellerid: "<?php echo $model->foruserid; ?>",
                        message: $('#Email_message').val()
                    },
                    success: function(data) {
                        $('#message_modal_body').html('Email has been successfuly sent to the Seller.');
                        $('#MessageSend').modal('hide');
                        if(data==1) {
                            msg = '<h4 class="modal-title">Thank you!</h4><p>Email sent successfully.</p>';
                        } else if(data==2) {
                            msg = '<h4 class="modal-title">Oops!</h4><p></p>Please provide a valid message.';
                        }else {
                            msg = '<h4 class="modal-title">Oops!</h4><p></p>Failure, Please try again!';
                        }
                        $('#ThanksModalBody').html(msg);
                        $('#ThanksModal').modal('show');
                        
                    }
                }); 
            });
            
            $('#favourite_link').click(function(){
                // $('#FavouriteModal').modal('show');
                // check if loggedin else redirect
                if(!<?php echo Yii::app()->user->isGuest ? true:0; ?>) {
                    $.ajax({
                        type: "GET",
                        url: "<?php echo Yii::app()->urlManager->createUrl('ajax/favlistingtoggle'); ?>",
                        data: {
                            id: "<?php echo Yii::app()->user->id; ?>",
                            listingid: <?php echo $model->id; ?>
                        },
                        success: function(data) {
                            // check if returned true
                            if(data==1) {
                                $("#favourite_link").html('Remove favourites');
                            } else if(data==0) {
                                $("#favourite_link").html('Save to favorites');
                            }
                        }
                    });
                } else {
                    $('#FavouriteModal').modal('show');
                }
            });
        });
    </script>

<!-- content container will end in layout -->