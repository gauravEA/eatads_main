<?php
/* @var $this ListingController */
/* @var $model Listing */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'byuserid'); ?>
		<?php echo $form->textField($model,'byuserid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'foruserid'); ?>
		<?php echo $form->textField($model,'foruserid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'length'); ?>
		<?php echo $form->textField($model,'length',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'width'); ?>
		<?php echo $form->textField($model,'width',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sizeunit'); ?>
		<?php echo $form->textField($model,'sizeunit'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'basecurrency'); ?>
		<?php echo $form->textField($model,'basecurrency',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'price'); ?>
		<?php echo $form->textField($model,'price',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'priceduration'); ?>
		<?php echo $form->textField($model,'priceduration'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'otherdata'); ?>
		<?php echo $form->textField($model,'otherdata',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'countryid'); ?>
		<?php echo $form->textField($model,'countryid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stateid'); ?>
		<?php echo $form->textField($model,'stateid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cityid'); ?>
		<?php echo $form->textField($model,'cityid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'street'); ?>
		<?php echo $form->textField($model,'street',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'geolat'); ?>
		<?php echo $form->textField($model,'geolat',array('size'=>9,'maxlength'=>9)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'geolng'); ?>
		<?php echo $form->textField($model,'geolng',array('size'=>9,'maxlength'=>9)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'zoomlevel'); ?>
		<?php echo $form->textField($model,'zoomlevel'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lighting'); ?>
		<?php echo $form->textField($model,'lighting'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mediatypeid'); ?>
		<?php echo $form->textField($model,'mediatypeid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datecreated'); ?>
		<?php echo $form->textField($model,'datecreated'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datemodified'); ?>
		<?php echo $form->textField($model,'datemodified'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->