<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>EatAds Mediahub</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
    <?php if ($this->beginCache('aboutUs', array('duration' => $this->cacheTime))) { ?>    
        <?php
            $theme = Yii::app()->theme;
            $cs = Yii::app()->clientScript;
            
            //echo $theme->getBaseUrl() . 'sdfdffsdfsdsd';
            $cs->registerCssFile( $theme->getBaseUrl() . '/css/bootstrap.min.css' );
            $cs->registerCssFile( $theme->getBaseUrl() . '/css/bootstrap-theme.min.css' );
            $cs->registerCssFile( $theme->getBaseUrl() . '/css/main.css' );
            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js', CClientScript::POS_BEGIN);
            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/main.js', CClientScript::POS_BEGIN);
            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/vendor/bootstrap.min.js', CClientScript::POS_BEGIN);
//            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/vendor/jquery-1.11.0.min.js', CClientScript::POS_BEGIN);
        ?>    
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Delius' rel='stylesheet' type='text/css'>
        
        
        <!-- Google Tag Manager - Initiate DataLayer variable -->
<script>
dataLayer = [];

$(function() {
   $('#btn-signup').click(function() {
console.log('sdfsdfsdfsdf');
        //    e.preventDefault();
        
    $.ajax({
            type: 'POST',
             url: '<?php echo Yii::app()->urlManager->createUrl('ajax/signup/'); ?>',
            data:{'email': $('#email').val(),
                  'phone': $('#code').val() + '-' + $('#phonenumber').val(),
                   'pid':1 },
         success:function(data){
             alert(data);
                if (data == 1) {
                    $('.error-message').hide();
                    $('.signup-form').hide().delay(7000).fadeIn("slow");
                    $('#thank-you').show().delay(5000).fadeOut("slow");
                    //$('#thank-you').style.display='inline-block';
                    //console.log($('#email').val());
                    $('#email').val('');
                    $('#phonenumber').val('');
                    
//                    $('.signup-form').show().delay('500');
                    
                    
//                    $('#thank-you').fadeOut(5000).done(function(){
//                        alert("Now all '.hotel_photo_select are hidden'");
//                    });
//                    $.when($('#thank-you').fadeOut(500))
//                               .done(function() {
//                        alert("Now all '.hotel_photo_select are hidden'");
//                $('.signup-form').show();
//                    });
//                    $('.signup-form').replaceWith($('.thank-you'));
  //                  $('#thank-you').show();
                    
//                    $('.signup-form').style.display='none';
                } if (data == 0) { 
                    $('.error-message').text('Invalid Email');
                    $('.error-message').show().delay(4000).fadeOut("slow");
                }                 
                 else if (data != 0 && data != 1) {
                    $('.error-message').show().delay(4000).fadeOut("slow");
                }     
            },
            error: function(data) { // if error occured
                  alert("Error occured.please try again");
                  alert(data);
             }
           });
}); 
});
</script>  
    </head>
    
    <body>
        


        <div class="modal fade bs-example-modal-lg" id="video-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="videowrapper">                    
                        <iframe id="yt-video" width="900" height="521" src="//www.youtube.com/embed/xKbUWWqp2PA" frameborder="0" allowfullscreen>
                        </iframe>
                    </div>
                </div>
            </div>
            </div>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#"><img src="<?php echo Yii::app()->theme->getBaseUrl(); ?>/img/logo.png" class="eatads-logo"></a>
        </div>
        <!--<ul class="nav navbar-nav navbar-right">
            <li><a href="#">Login</a></li>
        </ul>-->
      </div>
    </div>

    <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="container" id="big-banner">
        <div class="hero-lines">
            <h1>Your Media to the World</h1>
            <p>Welcome to your new online home. Proudly share your inventory with the buying community on a modern map interface. No more spreadsheets, PDFs and PPTs. Finally.</p>
        </div>
        <div class="landing-video">
            <img width="240px" height="160px" src="<?php echo Yii::app()->theme->getBaseUrl(); ?>/img/video-thumbnail.png">
        </div>
        <div class="signup-form">
            <div class="form-inline" >
                <div class="form-group">
                  <input type="email" placeholder="cheryl@company.com" class="form-control" id="email">
                </div>
                <div class="form-group">
                    <input type="text" placeholder="+91" class="form-control country-code" id="code" value="+91">
                </div>
                <div class="form-group">
                    <input type="tel" placeholder="optional" class="form-control" id="phonenumber" maxlength="10">
                </div>
                <button type="submit" class="btn btn-custom" id="btn-signup">Sign Up. It's Free.</button>
            </div>
        </div>
          <div class="thank-you" id="thank-you" style="display: none;">
            <span><img src="<?php echo Yii::app()->theme->getBaseUrl(); ?>/img/check.png">&nbsp; Thank you for signing-up with us. Please check your mailbox to activate your account.</span>
        </div>
        <div class="error-message">Email already exist with us.</div>
      </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row promises">
        <div class="col-md-3 col-sm-3 divider-vertical">
          <h2>Your Media Now on Map</h2>
          <img src="<?php echo Yii::app()->theme->getBaseUrl(); ?>/img/promise-icon-1.png">
          <p>Post unlimited Media Units for the first time on our unique map interface. Help buyers understand the placement of your media and justify your price. <span class="highligted-text">No more PPTs, XLSs and PDFs, phew.</span></p>
        </div>
        <div class="col-md-3 col-sm-3 divider-vertical">
          <h2>Generate More Leads</h2>
          <img src="<?php echo Yii::app()->theme->getBaseUrl(); ?>/img/promise-icon-2.png">
          <p>With our unique Map interface, buyers can easily browse your media units, create plans, and contact you directly with availability and pricing requests. <span class="highligted-text">Less communication, more sales.</span></p>
       </div>
        <div class="col-md-3 col-sm-3 divider-vertical">
          <h2>Your Online Presence</h2>
          <img src="<?php echo Yii::app()->theme->getBaseUrl(); ?>/img/promise-icon-3.png">
          <p>Your showcase page is your brand presence online. Just send them your showcase page link or use our automatic mailer to your contact list or our global contact list of buyers to expand your reach. <span class="highligted-text">Welcome online!</span></p>
        </div>
        <div class="col-md-3 col-sm-3">
          <h2>Gain Insight</h2>
          <img src="<?php echo Yii::app()->theme->getBaseUrl(); ?>/img/promise-icon-4.png">
          <p>Using our robust analytics engine, you're now able to see which buyers are opening your emails and looking at your media units on a daily basis. <span class="highligted-text">Sales just got a whole lot smarter.</span></p>
        </div>
      </div>
      <!--<div class="container">
        <div class="row">
           <div class="col-md-12 text-center"><span class="hand-written">Just last week <span style="color:#D0021B; font-weight:600;">75</span> vendors signed up with us and some of them say</span></div> 
        </div>  
        <div class="row">
            <div class="col-md-6 text-center"><span class="testimonial-text">“Testimonial 2 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco”</span><br><br><span style="font-weight:600">Person Name Lorem, </span><span>Position Lorem</span></div>
            <div class="col-md-6 text-center"><span class="testimonial-text">“ Presenting your inventory online for easy browsing and lead generation, anytime anywhere is a must for any OOH media vendor.”</span><br><br><span style="font-weight:600">Ron Graham, </span><span>Asia based OOH Advisor</span></div>   
        </div>
      </div>-->
      <div class="row landing-footer">
          <footer>
            <p>&copy; 2014 EatAds.com - All Rights Reserved Company 2014
                <span class="footer-links pull-right">
                    <a href="https://www.eatads.com/about-us" target="_blank">About</a>
                    <a href="https://www.eatads.com/contact-us" target="_blank">Contact</a>
                    <a href="https://www.facebook.com/EatAds?ref=br_tf" target="_blank"><img src="<?php echo Yii::app()->theme->getBaseUrl(); ?>/img/facebook.png"></a>
                    <a href="https://www.linkedin.com/company/eatads" target="_blank"><img src="<?php echo Yii::app()->theme->getBaseUrl(); ?>/img/linkedin.png"></a>
                </span>
            </p>
          </footer>
      </div>
    </div> 
    </body>

<?php $this->endCache();
}
?>
</html>
