<?php

Yii::import('application.models.base.BaseListingDraft');

class ListingDraft extends BaseListingDraft {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('mediatype, name, length, width, sizeunit, basecurrency, lighting, price, priceduration, audiencetag', 'required', 'on' => 'createListing'),
            array('basecurrency', 'length', 'max' => 3, 'tooLong' => 'Please enter valid base currency', 'on' => 'createListing'),
            array('sizeunit', 'length', 'max' => 10, 'on' => 'createListing'),
            array('priceduration', 'length', 'max' => 20, 'on' => 'createListing'),
            array('name', 'length', 'max' => 255, 'on' => 'createListing'),
            array('description', 'length', 'max' => 1200, 'on' => 'createListing'),
            array('otherdata, country, state, city, lighting, mediatype', 'length', 'max' => 50, 'on' => 'createListing'),
            array('reach', 'numerical', 'integerOnly' => true, 'min' => 1000, 'max' => 10000000, 'on' => 'createListing'),
            array('price', 'numerical', 'integerOnly'=>true, 'min' => 1, 'on' => 'createListing'),
            array('length, width', 'numerical', 'min' => 0.1, 'on' => 'createListing'),
            array('length, width', 'length', 'max' => 11, 'on' => 'createListing'),
            array('price', 'length', 'max' => 10, 'on' => 'createListing', 'message' => '{attribute} should be max 10 digits.'),
            array('length, width', 'match', 'pattern'=>'/^[0-9]{1,7}(\.[0-9]{1,3})?$/', 'on' => 'createListing', 'message'=>'{attribute} should be max 10 digit (7 before decimal and 3 after decimal).'),
            //array('price', 'match', 'pattern'=>'/^[0-9]{1,8}(\.[0-9]{1,2})?$/', 'on' => 'createListing', 'message'=>'{attribute} should be max 10 digits (8 before decimal and 2 after decimal).'),
            array('price', 'length', 'max' => 10, 'on' => 'createListing', 'message'=>'{attribute} should be max 10 digits.'),
            array('locality, audiencetag', 'length', 'max' => 100, 'on' => 'createListing'),
            
            array('geolat', 'length', 'allowEmpty' => true, 'max' => 10, 'on' => 'createListing', 'message' => 'Please provide a valid latitude.'),
            array('geolat, geolng', 'type', 'allowEmpty' => true, 'type' => 'float', 'on' => 'createListing'),
            array('geolat', 'compare', 'operator' => '<=', 'compareValue' => 90, 'message' => 'Please provide a valid latitude.', 'allowEmpty' => true, 'on' => 'createListing'),
            array('geolat', 'compare', 'operator' => '>=', 'compareValue' => -90, 'message' => 'Please provide a valid latitude.', 'allowEmpty' => true, 'on' => 'createListing'),
            array('geolat', 'match', 'pattern' => '/^[0-9.-]*$/', 'on' => 'createListing', 'message' => 'Please provide a valid latitude.'),
            array('geolng', 'length', 'allowEmpty' => true, 'max' => 11, 'on' => 'createListing', 'message' => 'Please provide a valid longitude.'),
            array('geolng', 'compare', 'operator' => '<=', 'compareValue' => 180, 'message' => 'Please provide a valid longitude.', 'allowEmpty' => true, 'on' => 'createListing'),
            array('geolng', 'compare', 'operator' => '>=', 'compareValue' => -180, 'message' => 'Please provide a valid longitude.', 'allowEmpty' => true, 'on' => 'createListing'),
            array('geolng', 'match', 'pattern' => '/^[0-9.-]*$/', 'on' => 'createListing', 'message' => 'Please provide a valid longitude.'),
            
            array('sizeunit', 'validateSizeUnit', 'on' => 'createListing'),
            array('lighting', 'validateLighting', 'on' => 'createListing'),
            array('priceduration', 'validatePriceDuration', 'on' => 'createListing'),
            array('mediatype', 'validateMediatype', 'on' => 'createListing'),
            array('basecurrency', 'validateBasecurrency', 'on' => 'createListing'),
            //array('country', 'validateCountry', 'on' => 'createListing'),
            array('audiencetag', 'validateAudiencetag', 'on' => 'createListing'),
            array('id, byuserid, foruserid, datecreated, datemodified, accurate_geoloc', 'safe', 'on' => 'createListing'),
//            array('price', 'length', 'max' => 45),
//            array('length, width', 'length', 'max' => 10),
//            array('description', 'safe'),
//            // The following rule is used by search().
//            // @todo Please remove those attributes that should not be searched.
//            array('id, userid, name, length, width, sizeunit, basecurrency, price, priceduration, otherdata, country, state, city, locality, geolat, geolng, lighting, mediatype, description, reach, audiencetag, datecreated, datemodified', 'safe', 'on' => 'search'),
        );
    }

    public function validateCountry($attribute, $params) {        
        if (!$countryId = Area::isCountryExist($this->country)) {
            $this->addError('country', 'Country does not exist');
        } elseif (!$stateId = Area::isStateExist($countryId, $this->state)) {
            $this->addError('state', 'State does not exist');
        } elseif (!$cityId = Area::isCityExist($stateId, $this->city)) {
            $this->addError('city', 'City does not exist');
        }
    }

    public function validateAudiencetag($attribute, $params) {
        if (!AudienceTag::isAudienceTagExist($this->audiencetag)) {
            $this->addError($attribute, 'Please enter valid audience tag');
        }
    }

    public function validateMediatype($attribute, $params) {
        if (!MediaType::isMediaTypeExist($this->mediatype)) {
            $this->addError($attribute, 'Please enter valid media type');
        }
    }

    public function validateBasecurrency($attribute, $params) {
        if (!LookupBaseCurrency::isBaseCurrencyExist($this->basecurrency)) {
            $this->addError($attribute, 'Please enter valid base currency');
        }
    }

    public function validatePriceDuration($attribute, $params) {
        $pricedurationArr = Listing::getPriceDuration();

        if (!in_array(ucfirst(strtolower($this->priceduration)), $pricedurationArr)) {
            $this->addError($attribute, 'Please enter valid price duration');
        }
    }

    public function validateLighting($attribute, $params) {
        $lightingArr = Listing::getLighting();

        if (!in_array(ucfirst(strtolower($this->lighting)), $lightingArr)) {
            $this->addError($attribute, 'Please enter valid lighting');
        }
    }

    public function validateSizeUnit($attribute, $params) {
        $sizeunitArr = Listing::getSizeUnit();

        if (!in_array(ucfirst(strtolower($this->sizeunit)), $sizeunitArr)) {
            $this->addError($attribute, 'Please enter valid size unit');
        }
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'byuserid' => 'By User id',
            'foruserid' => 'For User id',
            'name' => 'Name',
            'length' => 'Length',
            'width' => 'Width',
            'sizeunit' => 'Size unit',
            'basecurrency' => 'Base currency',
            'price' => 'Price',
            'priceduration' => 'Price duration',
            'otherdata' => 'Other data',
            'country' => 'Country',
            'state' => 'State',
            'city' => 'City',
            'locality' => 'Locality',
            'geolat' => 'Latitude',
            'geolng' => 'Longitude',
            'lighting' => 'Lighting',
            'mediatype' => 'Media type',
            'description' => 'Description',
            'reach' => 'Reach',
            'audiencetag' => 'Audience tag',
            'datecreated' => 'Date created',
            'datemodified' => 'Date modified',
        );
    }
    
    public static function getAllListingDraftByUserId($userId, $status = 1) {
        $criteria = new CDbCriteria();
        $criteria->select = '*';
        $criteria->condition = 'byuserid=:byuserid AND status = '.$status;
        $criteria->params = array(':byuserid' => $userId);

        $data = self::model()->findAll($criteria);
        return $data;
    }

}
