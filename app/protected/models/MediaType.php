<?php

Yii::import('application.models.base.BaseMediaType');

class MediaType extends BaseMediaType {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public static function isMediaTypeExist($mediaType) {
        $criteria = new CDbCriteria();
        $criteria->select = 'id';
        $criteria->condition = 'name=:name';
        $criteria->params = array(':name' => $mediaType);

        $data = self::model()->find($criteria);
        if($data) {
            return $data->id;
        } else {
            return false;
        }
    }
    /*
     * MediaType only with listings
     */
    public static function getPriorityMediaType($limit=0)
    {
        // media type with >1 listing count, only from solr, sorted by count
        $criteria = new CDbCriteria;
        $criteria->select   = 't.id, t.name, COUNT(tu.id) as cnt';
        $criteria->join     = ' LEFT JOIN `Listing` AS `tu` ON tu.mediatypeid = t.id';
        $criteria->group    = ' t.name ';
        $criteria->having   = ' COUNT(tu.id) > 0';
        $criteria->order    = ' COUNT(tu.id) DESC';
        $criteria->addCondition("tu.status=1 AND tu.solr=1");
        $mtArr = CHtml::listData(self::model()->findAll($criteria), 'id', 'name');
        return $mtArr;
    }
    
    /*
     * MediaType only with listings
     */
    public static function getPriorityMediaTypeForCompany($limit=0)
    {
        // media type with >1 listing count, only from solr, sorted by count
        $criteria = new CDbCriteria;
        $criteria->select   = 't.id, t.name, COUNT(tu.id) as cnt';
        $criteria->join     = ' LEFT JOIN `Listing` AS `tu` ON tu.mediatypeid = t.id ';
        $criteria->group    = ' t.name ';
        $criteria->having   = ' COUNT(tu.id) > 0';
        $criteria->order    = ' COUNT(tu.id) DESC';
        $criteria->addCondition("tu.status=1 AND tu.solr=1 ");
        //$mtArr = CHtml::listData(self::model()->findAll($criteria), 'id', 'name');
        return self::model()->findAll($criteria);
    }
}
