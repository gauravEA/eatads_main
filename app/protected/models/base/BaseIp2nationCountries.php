<?php

/**
 * This is the model class for table "ip2nationCountries".
 *
 * The followings are the available columns in table 'ip2nationCountries':
 * @property string $code
 * @property string $iso_code_2
 * @property string $iso_code_3
 * @property string $iso_country
 * @property string $country
 * @property double $lat
 * @property double $lon
 */
class BaseIp2nationCountries extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ip2nationCountries';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lat, lon', 'numerical'),
			array('code', 'length', 'max'=>4),
			array('iso_code_2', 'length', 'max'=>2),
			array('iso_code_3', 'length', 'max'=>3),
			array('iso_country, country', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('code, iso_code_2, iso_code_3, iso_country, country, lat, lon', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'code' => 'Code',
			'iso_code_2' => 'Iso Code 2',
			'iso_code_3' => 'Iso Code 3',
			'iso_country' => 'Iso Country',
			'country' => 'Country',
			'lat' => 'Lat',
			'lon' => 'Lon',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('code',$this->code,true);
		$criteria->compare('iso_code_2',$this->iso_code_2,true);
		$criteria->compare('iso_code_3',$this->iso_code_3,true);
		$criteria->compare('iso_country',$this->iso_country,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('lat',$this->lat);
		$criteria->compare('lon',$this->lon);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Ip2nationCountries the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
