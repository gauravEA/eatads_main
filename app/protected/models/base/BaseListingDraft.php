<?php

/**
 * This is the model class for table "ListingDraft".
 *
 * The followings are the available columns in table 'ListingDraft':
 * @property integer $id
 * @property integer $byuserid
 * @property integer $foruserid
 * @property string $name
 * @property string $length
 * @property string $width
 * @property string $sizeunit
 * @property string $basecurrency
 * @property string $price
 * @property string $priceduration
 * @property string $otherdata
 * @property string $country
 * @property string $state
 * @property string $city
 * @property string $locality
 * @property string $geolat
 * @property string $geolng
 * @property integer $accurate_geoloc
 * @property string $lighting
 * @property string $mediatype
 * @property string $description
 * @property integer $reach
 * @property string $audiencetag
 * @property integer $status
 * @property string $datecreated
 * @property string $datemodified
 *
 * The followings are the available model relations:
 * @property User $byuser
 * @property User $foruser
 */
class BaseListingDraft extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'ListingDraft';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('byuserid, foruserid, name, length, width, sizeunit, basecurrency, price, priceduration, country, state, city, locality, lighting, mediatype, audiencetag, datecreated, datemodified', 'required'),
            array('byuserid, foruserid, accurate_geoloc, reach, status', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>255),
            array('length, width, sizeunit, price', 'length', 'max'=>10),
            array('basecurrency', 'length', 'max'=>3),
            array('priceduration', 'length', 'max'=>20),
            array('otherdata, country, state, city, lighting, mediatype', 'length', 'max'=>50),
            array('locality, audiencetag', 'length', 'max'=>100),
            array('geolat, geolng', 'length', 'max'=>9),
            array('description', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, byuserid, foruserid, name, length, width, sizeunit, basecurrency, price, priceduration, otherdata, country, state, city, locality, geolat, geolng, accurate_geoloc, lighting, mediatype, description, reach, audiencetag, status, datecreated, datemodified', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'byuser' => array(self::BELONGS_TO, 'User', 'byuserid'),
            'foruser' => array(self::BELONGS_TO, 'User', 'foruserid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'byuserid' => 'Byuserid',
            'foruserid' => 'Foruserid',
            'name' => 'Name',
            'length' => 'Length',
            'width' => 'Width',
            'sizeunit' => 'Sizeunit',
            'basecurrency' => 'Basecurrency',
            'price' => 'Price',
            'priceduration' => 'Priceduration',
            'otherdata' => 'Otherdata',
            'country' => 'Country',
            'state' => 'State',
            'city' => 'City',
            'locality' => 'Locality',
            'geolat' => 'Geolat',
            'geolng' => 'Geolng',
            'accurate_geoloc' => 'Accurate Geoloc',
            'lighting' => 'Lighting',
            'mediatype' => 'Mediatype',
            'description' => 'Description',
            'reach' => 'Reach',
            'audiencetag' => 'Audiencetag',
            'status' => 'Status',
            'datecreated' => 'Datecreated',
            'datemodified' => 'Datemodified',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('byuserid',$this->byuserid);
        $criteria->compare('foruserid',$this->foruserid);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('length',$this->length,true);
        $criteria->compare('width',$this->width,true);
        $criteria->compare('sizeunit',$this->sizeunit,true);
        $criteria->compare('basecurrency',$this->basecurrency,true);
        $criteria->compare('price',$this->price,true);
        $criteria->compare('priceduration',$this->priceduration,true);
        $criteria->compare('otherdata',$this->otherdata,true);
        $criteria->compare('country',$this->country,true);
        $criteria->compare('state',$this->state,true);
        $criteria->compare('city',$this->city,true);
        $criteria->compare('locality',$this->locality,true);
        $criteria->compare('geolat',$this->geolat,true);
        $criteria->compare('geolng',$this->geolng,true);
        $criteria->compare('accurate_geoloc',$this->accurate_geoloc);
        $criteria->compare('lighting',$this->lighting,true);
        $criteria->compare('mediatype',$this->mediatype,true);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('reach',$this->reach);
        $criteria->compare('audiencetag',$this->audiencetag,true);
        $criteria->compare('status',$this->status);
        $criteria->compare('datecreated',$this->datecreated,true);
        $criteria->compare('datemodified',$this->datemodified,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ListingDraft the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}