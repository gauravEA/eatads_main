<?php

/**
 * This is the model class for table "LocalityGeoloc".
 *
 * The followings are the available columns in table 'LocalityGeoloc':
 * @property integer $id
 * @property string $sublocality1
 * @property string $sublocality2
 * @property string $sublocality
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $geolat
 * @property string $geolng
 *
 * The followings are the available model relations:
 * @property Listing[] $listings
 */
class BaseLocalityGeoloc extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'LocalityGeoloc';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sublocality, geolat, geolng', 'required'),
			array('sublocality1, sublocality2, sublocality, city, state, country', 'length', 'max'=>100),
			array('geolat, geolng', 'length', 'max'=>9),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sublocality1, sublocality2, sublocality, city, state, country, geolat, geolng', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'listings' => array(self::HAS_MANY, 'Listing', 'locality_geoloc_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sublocality1' => 'Sublocality1',
			'sublocality2' => 'Sublocality2',
			'sublocality' => 'Sublocality',
            'city' => 'City',
            'state' => 'State',
            'country' => 'Country',
			'geolat' => 'Geolat',
			'geolng' => 'Geolng',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sublocality1',$this->sublocality1,true);
		$criteria->compare('sublocality2',$this->sublocality2,true);
		$criteria->compare('sublocality',$this->sublocality,true);
        $criteria->compare('city',$this->city,true);
        $criteria->compare('state',$this->state,true);
        $criteria->compare('country',$this->country,true);
		$criteria->compare('geolat',$this->geolat,true);
		$criteria->compare('geolng',$this->geolng,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LocalityGeoloc the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
