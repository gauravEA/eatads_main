<?php

/**
 * This is the model class for table "Email".
 *
 * The followings are the available columns in table 'Email':
 * @property integer $id
 * @property integer $byuserid
 * @property integer $foruserid
 * @property integer $type
 * @property string $subject
 * @property string $message
 * @property integer $draft
 * @property integer $status
 * @property string $datecreated
 * @property string $datesent
 * @property string $dateread
 *
 * The followings are the available model relations:
 * @property User $byuser
 * @property User $foruser
 */
class BaseEmail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Email';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('message', 'required'),
			array('byuserid, foruserid, type, draft, status', 'numerical', 'integerOnly'=>true),
			array('subject', 'length', 'max'=>50),
			array('datesent, dateread', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, byuserid, foruserid, type, subject, message, draft, status, datecreated, datesent, dateread', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'byuser' => array(self::BELONGS_TO, 'User', 'byuserid'),
			'foruser' => array(self::BELONGS_TO, 'User', 'foruserid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'byuserid' => 'By user',
			'foruserid' => 'For user',
			'type' => 'Type',
			'subject' => 'Subject',
			'message' => 'Message',
			'draft' => 'Draft',
			'status' => 'Status',
			'datecreated' => 'Date Created',
			'datesent' => 'Date Sent',
			'dateread' => 'Date Read',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('byuserid',$this->byuserid);
		$criteria->compare('foruserid',$this->foruserid);
		$criteria->compare('type',$this->type);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('draft',$this->draft);
		$criteria->compare('status',$this->status);
		$criteria->compare('datecreated',$this->datecreated,true);
		$criteria->compare('datesent',$this->datesent,true);
		$criteria->compare('dateread',$this->dateread,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Email the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
