<?php

/**
 * This is the model class for table "UserCompany".
 *
 * The followings are the available columns in table 'UserCompany':
 * @property integer $id
 * @property integer $userid
 * @property string $name
 * @property string $alias
 * @property string $description
 * @property string $logo
 * @property string $websiteurl
 * @property string $phonenumber
 * @property integer $countryid
 * @property integer $stateid
 * @property integer $cityid
 * @property string $address1
 * @property string $address2
 * @property string $postalcode
 * @property string $facebookprofile
 * @property string $twitterhandle
 * @property string $linkedinprofile
 * @property string $googleplusprofile
 *
 * The followings are the available model relations:
 * @property Area $city
 * @property Area $country
 * @property Area $state
 * @property User $user
 */
class BaseUserCompany extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'UserCompany';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid, name, description, websiteurl, phonenumber, countryid, stateid, cityid, address1, address2', 'required'),
			array('userid, countryid, stateid, cityid', 'numerical', 'integerOnly'=>true),
			array('name, websiteurl, address1, address2', 'length', 'max'=>50),
			array('phonenumber, postalcode, twitterhandle', 'length', 'max'=>20),
			array('facebookprofile, linkedinprofile, googleplusprofile', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, userid, name, alias, description, websiteurl, phonenumber, countryid, stateid, cityid, address1, address2, postalcode, facebookprofile, twitterhandle, linkedinprofile, googleplusprofile, logo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'city' => array(self::BELONGS_TO, 'Area', 'cityid'),
			'country' => array(self::BELONGS_TO, 'Area', 'countryid'),
			'state' => array(self::BELONGS_TO, 'Area', 'stateid'),
			'user' => array(self::BELONGS_TO, 'User', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userid' => 'Userid',
			'name' => 'Name',
                        'alias' => 'Alias',
			'description' => 'Description',
                        'logo' => 'Logo',
			'websiteurl' => 'Websiteurl',
			'phonenumber' => 'Phonenumber',
			'countryid' => 'Countryid',
			'stateid' => 'Stateid',
			'cityid' => 'Cityid',
			'address1' => 'Address1',
			'address2' => 'Address2',
			'postalcode' => 'Postalcode',
			'facebookprofile' => 'Facebookprofile',
			'twitterhandle' => 'Twitterhandle',
			'linkedinprofile' => 'Linkedinprofile',
			'googleplusprofile' => 'Googleplusprofile',
                        'availability_auto_mail_trigger' => 'availability_auto_mail_trigger'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('name',$this->name,true);
                $criteria->compare('alias',$this->alias,true);
		$criteria->compare('description',$this->description,true);
                $criteria->compare('logo',$this->logo,true);
		$criteria->compare('websiteurl',$this->websiteurl,true);
		$criteria->compare('phonenumber',$this->phonenumber,true);
		$criteria->compare('countryid',$this->countryid);
		$criteria->compare('stateid',$this->stateid);
		$criteria->compare('cityid',$this->cityid);
		$criteria->compare('address1',$this->address1,true);
		$criteria->compare('address2',$this->address2,true);
		$criteria->compare('postalcode',$this->postalcode,true);
		$criteria->compare('facebookprofile',$this->facebookprofile,true);
		$criteria->compare('twitterhandle',$this->twitterhandle,true);
		$criteria->compare('linkedinprofile',$this->linkedinprofile,true);
		$criteria->compare('googleplusprofile',$this->googleplusprofile,true);
                $criteria->compare('availability_auto_mail_trigger',$this->availability_auto_mail_trigger,true);
                

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserCompany the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
