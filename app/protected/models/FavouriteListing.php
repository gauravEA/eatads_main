<?php

Yii::import('application.models.base.BaseFavouriteListing');

class FavouriteListing extends BaseFavouriteListing {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getFavListingByUserId($userId) {
        $criteria = new CDbCriteria();
        $criteria->select = 'id';
        $criteria->with = 'listing';
        $criteria->condition = "userid = :userId ";
        $criteria->params = array(':userId' => $userId);
        $result = self::model()->findAll($criteria);
        return $result;
    }
    
    public static function getFavListingCountByUserId($userId)
    {
        $criteria = new CDbCriteria();
        $criteria->select = 'id';
        $criteria->with = 'listing';
        $criteria->condition = "userid = :userId ";
        $criteria->params = array(':userId' => $userId);
        $result = self::model()->count($criteria);
        return $result;
    }

    // Admin Dashboard Function
    public static function getFavListingCountByDateRange($startDate, $endDate) {
        $criteria = new CDbCriteria();
        $criteria->select = 'listingid';
        $criteria->distinct = true;
        $criteria->group = 'listingid';
        $criteria->condition = "datecreated BETWEEN :startDate AND :endDate";
        $criteria->params = array(':startDate' => $startDate, ':endDate' => $endDate);
        return $result = self::model()->count($criteria);
    }

    public static function getFavListingChartData($startDate, $endDate, $duration){
        if($duration == "1") { // Weekly
            return FavouriteListing::getFavListingCountWeeklyByDateRange($startDate, $endDate);
        } elseif($duration == "2") { // Monthly
            return FavouriteListing::getFavListingCountMonthlyByDateRange($startDate, $endDate);
        } elseif($duration == "3") { // Yearly
            return FavouriteListing::getFavListingCountYearlyByDateRange($startDate, $endDate);
        }
    }
    
    // Admin Dashboard function Chart
    public static function getFavListingCountWeeklyByDateRange($startDate, $endDate) {
        $weeklyData = Yii::app()->db->createCommand("CALL getWeeklyFavListing ('$startDate', '$endDate')")->queryAll();
        
        $weekArr = JoyUtilities::getNoOfWeek($startDate, $endDate);
        $weeklyFavListingData = JoyUtilities::formatAdminGraphData($weeklyData, $weekArr);
                
        return $weeklyFavListingData;
    }    

    public static function getFavListingCountMonthlyByDateRange($startDate, $endDate) {
        $monthlyData = Yii::app()->db->createCommand("CALL getMonthlyFavListing ('$startDate', '$endDate')")->queryAll();

        $monthArr = JoyUtilities::getNoOfMonth($startDate, $endDate);
        $monthlyFavListingData = JoyUtilities::formatAdminGraphData($monthlyData, $monthArr);
                
        return $monthlyFavListingData;
    }    

    public static function getFavListingCountYearlyByDateRange($startDate, $endDate) {
        $yearlyData = Yii::app()->db->createCommand("CALL getYearlyFavListing ('$startDate', '$endDate')")->queryAll();
        
        $yearArr = JoyUtilities::getNoOfYear($startDate, $endDate);
        $yearlyFavListingData = JoyUtilities::formatAdminGraphData($yearlyData, $yearArr);
        
        return $yearlyFavListingData;
    }
    
    
}
