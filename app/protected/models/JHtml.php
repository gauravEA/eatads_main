<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of JHtml
 *
 * @author Amit Malakar
 */
class JHtml extends CHtml
{
    public static function enumItem($model,$attribute)
    {        
            $attr=$attribute;
            self::resolveName($model,$attr);
            preg_match('/\((.*)\)/',$model->tableSchema->columns[$attr]->dbType,$matches);
            foreach(explode(',', $matches[1]) as $value)
            {
                    $value=str_replace("'",null,$value);
                    $values[$value]=Yii::t('enumItem',$value);
            }

            return $values;
    }  

   public static function enumDropDownList($model, $attribute, $htmlOptions)
   {
      // http://www.yiiframework.com/forum/index.php/topic/10079-enum-db-type-in-yii/
      return CHtml::activeDropDownList( $model, $attribute,self::enumItem($model,  $attribute), $htmlOptions);
   }
   
   public static function enumRadioButtons($model, $attribute, $htmlOptions)
   {
      // http://www.yiiframework.com/forum/index.php/topic/10079-enum-db-type-in-yii/
      // $htmlOptions = array('separator'=>','); 
      return CHtml::radioButtonList( $attribute, '', self::enumItem($model,  $attribute), $htmlOptions);
   }
   
}

?>
