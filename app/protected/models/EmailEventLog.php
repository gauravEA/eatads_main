<?php

/**
 * This is the model class for table "EmailEventLog".
 *
 * The followings are the available columns in table 'EmailEventLog':
 * @property integer $id
 * @property integer $userid
 * @property integer $eventid
 * @property integer $openedFlag
 * @property integer $clickedFlag
 * @property string $createddate
 * @property string $modifieddate
 * @property integer $totalopens
 * @property integer $totalclicks
 *
 * The followings are the available model relations:
 * @property EmailEvent $event
 * @property User $user
 */
class EmailEventLog extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'EmailEventLog';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid, eventid, openedFlag, clickedFlag, createddate, modifieddate, totalopens, totalclicks', 'required'),
			array('userid, eventid, openedFlag, clickedFlag, totalopens, totalclicks', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, userid, eventid, openedFlag, clickedFlag, createddate, modifieddate, totalopens, totalclicks', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'event' => array(self::BELONGS_TO, 'EmailEvent', 'eventid'),
			'user' => array(self::BELONGS_TO, 'User', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userid' => 'Userid',
			'eventid' => 'Eventid',
			'openedFlag' => 'Opened Flag',
			'clickedFlag' => 'Clicked Flag',
			'createddate' => 'Createddate',
			'modifieddate' => 'Modifieddate',
			'totalopens' => 'Totalopens',
			'totalclicks' => 'Totalclicks',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('eventid',$this->eventid);
		$criteria->compare('openedFlag',$this->openedFlag);
		$criteria->compare('clickedFlag',$this->clickedFlag);
		$criteria->compare('createddate',$this->createddate,true);
		$criteria->compare('modifieddate',$this->modifieddate,true);
		$criteria->compare('totalopens',$this->totalopens);
		$criteria->compare('totalclicks',$this->totalclicks);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EmailEventLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
