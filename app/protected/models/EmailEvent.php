<?php

/**
 * This is the model class for table "EmailEvent".
 *
 * The followings are the available columns in table 'EmailEvent':
 * @property integer $id
 * @property string $name
 * @property string $emailtemplate
 * @property integer $emailtype
 * @property integer $createdby
 * @property string $createddate
 * @property string $companyid
 *
 * The followings are the available model relations:
 * @property EmailEventLog[] $emailEventLogs
 */
class EmailEvent extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'EmailEvent';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(' emailtemplate, emailtype, createddate', 'required'),
			array('emailtype, createdby', 'numerical', 'integerOnly'=>true),
			array('name, emailtemplate', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, emailtemplate, emailtype, createdby, createddate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'emailEventLogs' => array(self::HAS_MANY, 'EmailEventLog', 'eventid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'emailtemplate' => 'Emailtemplate',
			'emailtype' => 'Bulk=>0,Trans=>1',
			'createdby' => 'Createdby',
			'createddate' => 'Createddate',
                        'companyid' => 'CompanyId',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('emailtemplate',$this->emailtemplate,true);
		$criteria->compare('emailtype',$this->emailtype);
		$criteria->compare('createdby',$this->createdby);
		$criteria->compare('createddate',$this->createddate,true);
                $criteria->compare('companyid',$this->companyid,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EmailEvent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
