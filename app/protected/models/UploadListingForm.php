<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class UploadListingForm extends CFormModel
{
	public $file_upload;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
            array('file_upload', 'required'),
			// upload file type should be xls, xlsx, csv with max limit of 10 mb
			array('file_upload', 'file', 
                                'types' => 'xls,xlsx,csv', 
                                'maxSize' => 5*1024*1024, 
                                'wrongType'=>'Please upload a valid file. (Format - xls, xlsx, csv).', 
                                'tooLarge'=>'Please upload a valid file. (Size max 5mb).'),            
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'file_upload'=>'Upload file',            
		);
	}
}