<?php

Yii::import('application.models.base.BaseContact');

class Contact extends BaseContact {

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, email, message', 'required'),
            array('phone', 'required', 'on'=>'postRequirements'),
            array('phone', 'numerical', 'integerOnly'=>true),
            array('phone', 'length', 'max' => 10),
            // email has to be a valid email address
            array('email', 'match', 'pattern' => '/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/', 'message' => 'Email address is not valid.'),
            array('email', 'length', 'max' => 50),
            array('name', 'length', 'max' => 20),
            array('datecreated', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, email, message, phone, datecreated', 'safe', 'on' => 'search'),
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
