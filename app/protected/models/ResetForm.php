<?php

/**
 * ResetForm class.
 * ResetForm is the data structure for reseting
 * user password from db. It is used by the 'reset' action of 'AccountController'.
 */
class ResetForm extends CFormModel {

    public $password;
    public $confirmPassword;
    public $hiddenHash;
    private $_identity;

    const WEAK = 0;
    const STRONG = 1;

    /**
     * Declares the validation rules.
     * The rules state that password and confirmPassword are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            // password strength
            //array('password', 'ext.JOY.Validators.PasswordStrength', 'strength'=>self::STRONG),
            //array('password', 'passwordStrength', 'strength'=>self::STRONG),
            // required fields
            array('password, confirmPassword, hiddenHash', 'required'),
            // password & confirmPassword should match
            array('password, confirmPassword', 'length', 'min' => 6),
            array('password', 'match', 'pattern' => '/^([A-Za-z])+([0-9])+|([0-9])+([A-Za-z])+$/', 'message' => Yii::t("signup", "Password must be alphanumeric")),
            array('confirmPassword', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t("signup", "Passwords do not match")),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'password' => 'New Password',
            'confirmPassword' => 'Confirm Password',
        );
    }

    /**
     * check if the user password is strong enough
     * check the password against the pattern requested
     * by the strength parameter
     * This is the 'passwordStrength' validator as declared in rules().
     */
    public function passwordStrength($attribute, $params) {
        if ($params['strength'] === self::WEAK)
            $pattern = '/^(?=.*[a-zA-Z0-9]).{5,}$/';
        elseif ($params['strength'] === self::STRONG)
            $pattern = '/^(?=.*\d(?=.*\d))(?=.*[a-zA-Z](?=.*[a-zA-Z])).{5,}$/';

        if (!preg_match($pattern, $this->$attribute))
            $this->addError($attribute, 'your password is not strong enough!');
    }

}
