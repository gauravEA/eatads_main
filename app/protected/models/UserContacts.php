<?php

/**
 * This is the model class for table "UserContacts".
 *
 * The followings are the available columns in table 'UserContacts':
 * @property integer $id
 * @property string $email
 * @property string $name
 * @property string $company
 * @property integer $linkedUserId
 * @property string $createdDate
 * @property integer $status
 * @property integer $vendorid
 *
 * The followings are the available model relations:
 * @property UserCompany $vendor
 * @property User $linkedUser
 */
class UserContacts extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'UserContacts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, name, company, linkedUserId, createdDate, status, vendorid', 'required'),
			array('linkedUserId, status, vendorid', 'numerical', 'integerOnly'=>true),
			array('email, name, company', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, email, name, company, linkedUserId, createdDate, status, vendorid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'vendor' => array(self::BELONGS_TO, 'UserCompany', 'vendorid'),
			'linkedUser' => array(self::BELONGS_TO, 'User', 'linkedUserId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'name' => 'Name',
			'company' => 'Company',
			'linkedUserId' => 'Linked User',
			'createdDate' => 'Created Date',
			'status' => 'Status',
			'vendorid' => 'Vendorid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('company',$this->company,true);
		$criteria->compare('linkedUserId',$this->linkedUserId);
		$criteria->compare('createdDate',$this->createdDate,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('vendorid',$this->vendorid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserContacts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public static function getVendorContacts($companyId) {
        $sql = 'SELECT id, email, name, company, "false" as deleteFlag FROM UserContacts  WHERE status = 1 and vendorid = ' . $companyId;
        $command = Yii::app()->db->createCommand($sql);
        $data = $command->queryAll();
        return $data;
    }
}
