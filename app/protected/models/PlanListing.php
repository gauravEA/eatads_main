<?php

Yii::import('application.models.base.BasePlanListing');

class PlanListing extends BasePlanListing {

    public $title;
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public static function getPlanListingByPlanId($planId, $ownerId = ''){
        if($ownerId){
            $cond = 'planid=:planId AND l.foruserid =:ownerId ';
            $params = array(':planId'=> $planId, ':ownerId' => $ownerId);
        } else {
            $cond = 'planid=:planId';
            $params = array(':planId'=>$planId);
        }
        
        $planData = Yii::app()->db->createCommand()
            ->select('mt.name AS medianame, pl.id AS planlistingid, pl.listingid, l.name, price, sizeunitid, length, width, mediatypeid, pricedurationid, currency_code, filename, l.geolat as lat, l.geolng as lng, l.accurate_geoloc as ea')
            ->from('PlanListing AS pl')
            ->leftJoin('Listing AS l', 'pl.listingid = l.id')
            ->leftJoin('ListingImage AS li', 'l.id = li.listingid AND li.status = 1')
            ->leftJoin('LookupBaseCurrency AS lbc', 'lbc.id = l.basecurrencyid')                
            ->leftJoin('MediaType AS mt', 'mt.id = l.mediatypeid')
            ->where($cond, $params)
            ->group('pl.id')
            ->queryAll();
        return $planData;
    }
    
    public static function getPlanOwnerName($planId){
        $criteria = new CDbCriteria();
        $criteria->select = 'id, listingid';
        $criteria->with = array('listing' => array('select' => 'listing.name, listing.foruserid'), 'listing.foruser' => array('select' => 'fname, lname'));
        $criteria->condition = 'planid=:planId';
        $criteria->params = array(':planId' => $planId);
        $criteria->group = 'listing.foruserid';
        
        $data = self::model()->findAll($criteria);
        return $data;
    }

    public static function isPlanListingExist($planId, $listingId) {
        $criteria = new CDbCriteria();
        $criteria->select = 'id';
        $criteria->condition = 'planid=:planId AND listingid=:listingid';
        $criteria->params = array(':planId' => $planId, ':listingid' => $listingId);
        
        $data = self::model()->find($criteria);
        return count($data);

    }
    
//    public static function getPlanListingByOwnerId($planId, $ownerId) {
//        $criteria = new CDbCriteria();
//        $criteria->select = '*';
//        $criteria->with = array('listing' => array('select' => 'listing.name, listing.foruserid', 'condition' => 'listing.foruserid = '.$ownerId));
//        $criteria->condition = 'planid=:planId';
//        $criteria->params = array(':planId' => $planId);
//        
//        $data = self::model()->findAll($criteria);
//        return $data;
//    }
}
