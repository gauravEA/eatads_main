<?php

Yii::import('application.models.base.BasePlan');

class Plan extends BasePlan {

    public $listcount;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getPlanWithListCount($userId) {
        $criteria = new CDbCriteria();
        $criteria->select = 't.id, `name`, COUNT(listingid) AS listcount';
        $criteria->with = 'planListings';
        $criteria->condition = 'userid=:userId';
        $criteria->params = array(':userId' => $userId);
        $criteria->group = 't.id';
        $criteria->order = 'name';

        $data = self::model()->findAll($criteria);
        return $data;
    }

    public static function getPlanCountByUserId($userId) {
        $criteria = new CDbCriteria();
        $criteria->select = 'id, name';
        $criteria->condition = 'userid=:userId';
        $criteria->params = array(':userId' => $userId);
        $criteria->order = 'name';

        $data = self::model()->count($criteria);
        return $data;
    }     
    
    public static function getPlanByUserId($userId) {
        $criteria = new CDbCriteria();
        $criteria->select = 'id, name';
        $criteria->condition = 'userid=:userId';
        $criteria->params = array(':userId' => $userId);
        $criteria->order = 'name';

        $data = self::model()->findAll($criteria);
        return $data;
    }

    public static function getPlanById($planId) {
        $criteria = new CDbCriteria();
        $criteria->select = 'id, name';
        $criteria->condition = 'id=:planId';
        $criteria->params = array(':planId' => $planId);

        $data = self::model()->find($criteria);
        return $data;
    }

    // Admin Dashboard Function
    public static function getNewMediaPlanCountByDateRange($startDate, $endDate) {
        $criteria = new CDbCriteria();
        $criteria->select = 'id';
        $criteria->condition = "datecreated BETWEEN :startDate AND :endDate";
        $criteria->params = array(':startDate' => $startDate, ':endDate' => $endDate);
        return $result = self::model()->count($criteria);
    }
    
    public static function getMediaPlanChartData($startDate, $endDate, $duration){
        if($duration == "1") { // Weekly
            return Plan::getMediaPlanCountWeeklyByDateRange($startDate, $endDate);
        } elseif($duration == "2") { // Monthly
            return Plan::getMediaPlanCountMonthlyByDateRange($startDate, $endDate);
        } elseif($duration == "3") { // Yearly
            return Plan::getMediaPlanCountYearlyByDateRange($startDate, $endDate);
        }
    }
    
    // Admin Dashboard function Chart
    public static function getMediaPlanCountWeeklyByDateRange($startDate, $endDate) {
        $weeklyData = Yii::app()->db->createCommand("CALL getWeeklyMediaPlanCreate ('$startDate', '$endDate')")->queryAll();
        
        $weekArr = JoyUtilities::getNoOfWeek($startDate, $endDate);
        $weeklyMediaPlanData = JoyUtilities::formatAdminGraphData($weeklyData, $weekArr);
                
        return $weeklyMediaPlanData;
    }    

    public static function getMediaPlanCountMonthlyByDateRange($startDate, $endDate) {
        $monthlyData = Yii::app()->db->createCommand("CALL getMonthlyMediaPlanCreate ('$startDate', '$endDate')")->queryAll();

        $monthArr = JoyUtilities::getNoOfMonth($startDate, $endDate);
        $monthlyMediaPlanData = JoyUtilities::formatAdminGraphData($monthlyData, $monthArr);
                
        return $monthlyMediaPlanData;
    }    

    public static function getMediaPlanCountYearlyByDateRange($startDate, $endDate) {
        $yearlyData = Yii::app()->db->createCommand("CALL getYearlyMediaPlanCreate ('$startDate', '$endDate')")->queryAll();
        
        $yearArr = JoyUtilities::getNoOfYear($startDate, $endDate);
        $yearlyMediaPlanData = JoyUtilities::formatAdminGraphData($yearlyData, $yearArr);
        
        return $yearlyMediaPlanData;
    }     

    // Admin Dashboard Function
    public static function getMediaPlanModifyCountByDateRange($startDate, $endDate) {
        $criteria = new CDbCriteria();
        $criteria->select = 'id';
        $criteria->condition = "datemodified BETWEEN :startDate AND :endDate";
        $criteria->params = array(':startDate' => $startDate, ':endDate' => $endDate);
        return $result = self::model()->count($criteria);
    }
    
    public static function getMediaPlanModifyChartData($startDate, $endDate, $duration){
        if($duration == "1") { // Weekly
            return Plan::getMediaPlanModifyCountWeeklyByDateRange($startDate, $endDate);
        } elseif($duration == "2") { // Monthly
            return Plan::getMediaPlanModifyCountMonthlyByDateRange($startDate, $endDate);
        } elseif($duration == "3") { // Yearly
            return Plan::getMediaPlanModifyCountYearlyByDateRange($startDate, $endDate);
        }
    }
    
    // Admin Dashboard function Chart
    public static function getMediaPlanModifyCountWeeklyByDateRange($startDate, $endDate) {
        $weeklyData = Yii::app()->db->createCommand("CALL getWeeklyMediaPlanModify ('$startDate', '$endDate')")->queryAll();
        
        $weekArr = JoyUtilities::getNoOfWeek($startDate, $endDate);
        $weeklyMediaPlanModifyData = JoyUtilities::formatAdminGraphData($weeklyData, $weekArr);
                
        return $weeklyMediaPlanModifyData;
    }    

    public static function getMediaPlanModifyCountMonthlyByDateRange($startDate, $endDate) {
        $monthlyData = Yii::app()->db->createCommand("CALL getMonthlyMediaPlanModify ('$startDate', '$endDate')")->queryAll();

        $monthArr = JoyUtilities::getNoOfMonth($startDate, $endDate);
        $monthlyMediaPlanModifyData = JoyUtilities::formatAdminGraphData($monthlyData, $monthArr);
                
        return $monthlyMediaPlanModifyData;
    }    

    public static function getMediaPlanModifyCountYearlyByDateRange($startDate, $endDate) {
        $yearlyData = Yii::app()->db->createCommand("CALL getYearlyMediaPlanModify ('$startDate', '$endDate')")->queryAll();
        
        $yearArr = JoyUtilities::getNoOfYear($startDate, $endDate);
        $yearlyMediaPlanModifyData = JoyUtilities::formatAdminGraphData($yearlyData, $yearArr);
        
        return $yearlyMediaPlanModifyData;
    }
    
    public static function getPlansForUser($userid) {
        $plans = Yii::app()->db->createCommand("select id, name from Plan where status = 1 and userid =" .$userid)->queryAll();
        $result = array();
        foreach ($plans as $plan) {
            $planListings = PlanListing::getPlanListingByPlanId($plan['id']);
            $listings = array();
            $totalPrice = 0;
            $plan['TotalSites'] = count($planListings);
            foreach ($planListings as $list) {
                $temp= array();
                $temp['type'] = $list['medianame'];
                $temp['id'] = $list['listingid'];
                $temp['name'] = $list['name'];
                $temp['width'] = $list['width'];
                $temp['length'] = $list['length'];
                $temp['basecurrency'] = $list['currency_code'];
                $temp['thumbnail'] = JoyUtilities::getAwsFileUrl('small_'.$list['filename'], 'listing');
                $temp['price'] = $list['price'];
                $totalPrice += $list['price'];
                $temp['sizeunit'] = Listing::getSizeUnit($list['sizeunitid']);
                array_push($listings, $temp);
            }
            $plan['TotalPrice'] = $totalPrice;
            $plan['SiteListing'] = $listings;
            array_push($result, $plan);
        }
        return $result;
    }
    
    public static function getPlan($userid, $name) {
        $plans = Yii::app()->db->createCommand("select id, name from Plan where status = 1 and userid =" .$userid ." and name <> '" .$name . "'")->queryAll();
        $plan = Yii::app()->db->createCommand("select id, name from Plan where status = 1 and userid =" .$userid ." and name = '" .$name . "'")->queryRow();
        
        $result = array();
            $planListings = PlanListing::getPlanListingByPlanId($plan['id']);
            $listings = array();
            $markers = array();
            $totalPrice = 0;
            
            foreach ($planListings as $list) {
                $temp= array();
                $temp['type'] = $list['medianame'];
                $temp['id'] = $list['listingid'];
                $temp['name'] = $list['name'];
                $temp['width'] = $list['width'];
                $temp['length'] = $list['length'];
                $temp['basecurrency'] = $list['currency_code'];
                $temp['thumbnail'] = JoyUtilities::getAwsFileUrl('small_'.$list['filename'], 'listing');
                $temp['price'] = $list['price'];
                $totalPrice += $list['price'];
                $temp['sizeunit'] = Listing::getSizeUnit($list['sizeunitid']);
                array_push($listings, $temp);
                
                $temp1 = array();
                $temp1['id'] = $list['listingid'];
                $temp1['lat'] = $list['lat'];
                $temp1['lng'] = $list['lng'];
                $temp1['ea'] = $list['ea'];
                array_push($markers, $temp1);
            }
            $plan['TotalSites'] = count($planListings);
            $plan['TotalPrice'] = $totalPrice;
            $plan['SiteListing'] = $listings;
            $plan['Markerslist'] = $markers;
            $plan['PlanList'] = $plans;
        return $plan;
    }
}
