<?php

/**
 * This is the model class for table "floorPlan_listings".
 *
 * The followings are the available columns in table 'floorPlan_listings':
 * @property integer $id
 * @property integer $listings_floorPlan_mapping_id
 * @property integer $listing_id
 * @property string $dateCreated
 *
 * The followings are the available model relations:
 * @property Listing $listing
 * @property ParentListingFloorPlanMapping $listingsFloorPlanMapping
 */
class FloorPlanListings extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'floorPlan_listings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('listings_floorPlan_mapping_id, listing_id, dateCreated', 'required'),
			array('listings_floorPlan_mapping_id, listing_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, listings_floorPlan_mapping_id, listing_id, dateCreated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'listing' => array(self::BELONGS_TO, 'Listing', 'listing_id'),
			'listingsFloorPlanMapping' => array(self::BELONGS_TO, 'ParentListingFloorPlanMapping', 'listings_floorPlan_mapping_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'listings_floorPlan_mapping_id' => 'Listings Floor Plan Mapping',
			'listing_id' => 'Listing',
			'dateCreated' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('listings_floorPlan_mapping_id',$this->listings_floorPlan_mapping_id);
		$criteria->compare('listing_id',$this->listing_id);
		$criteria->compare('dateCreated',$this->dateCreated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FloorPlanListings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
