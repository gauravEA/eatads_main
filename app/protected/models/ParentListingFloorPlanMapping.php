<?php

/**
 * This is the model class for table "parentListing_floorPlan_mapping".
 *
 * The followings are the available columns in table 'parentListing_floorPlan_mapping':
 * @property integer $id
 * @property integer $parentListing_id
 * @property integer $floor_plan_id
 * @property string $dateCreated
 *
 * The followings are the available model relations:
 * @property FloorPlanListings[] $floorPlanListings
 * @property FloorPlans $floorPlan
 * @property Listing $parentListing
 */
class ParentListingFloorPlanMapping extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'parentListing_floorPlan_mapping';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('parentListing_id, floor_plan_id, dateCreated', 'required'),
			array('parentListing_id, floor_plan_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, parentListing_id, floor_plan_id, dateCreated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'floorPlanListings' => array(self::HAS_MANY, 'FloorPlanListings', 'listings_floorPlan_mapping_id'),
			'floorPlan' => array(self::BELONGS_TO, 'FloorPlans', 'floor_plan_id'),
			'parentListing' => array(self::BELONGS_TO, 'Listing', 'parentListing_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parentListing_id' => 'Parent Listing',
			'floor_plan_id' => 'Floor Plan',
			'dateCreated' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('parentListing_id',$this->parentListing_id);
		$criteria->compare('floor_plan_id',$this->floor_plan_id);
		$criteria->compare('dateCreated',$this->dateCreated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ParentListingFloorPlanMapping the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
