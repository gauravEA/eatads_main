<?php

Yii::import('application.models.base.BaseRfpLog');

class RfpLog extends BaseRfpLog {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    // Admin Dashboard Function
    public static function getSentRfpCountByDateRange($startDate, $endDate) {
        $criteria = new CDbCriteria();
        $criteria->select = 'id';
        $criteria->condition = "datecreated BETWEEN :startDate AND :endDate";
        $criteria->params = array(':startDate' => $startDate, ':endDate' => $endDate);
        return $result = self::model()->count($criteria);
    }
    
    // Buyer dashboard
    public static function getRfpCountByUserId($userId)
    {
        $criteria = new CDbCriteria();       
        $criteria->with = 'plan';
        $criteria->condition = 'userid=:userId';
        $criteria->params = array(':userId' => $userId);
        return $result = self::model()->count($criteria);
    }

    public static function getRfpChartData($startDate, $endDate, $duration){
        if($duration == "1") { // Weekly
            return RfpLog::getRfpCountWeeklyByDateRange($startDate, $endDate);
        } elseif($duration == "2") { // Monthly
            return RfpLog::getRfpCountMonthlyByDateRange($startDate, $endDate);
        } elseif($duration == "3") { // Yearly
            return RfpLog::getRfpCountYearlyByDateRange($startDate, $endDate);
        }
    }
    
    
    // Admin Dashboard function Chart
    public static function getRfpCountWeeklyByDateRange($startDate, $endDate) {
        $weeklyData = Yii::app()->db->createCommand("CALL getWeeklyRfp ('$startDate', '$endDate')")->queryAll();
        
        $weekArr = JoyUtilities::getNoOfWeek($startDate, $endDate);
        $weeklyRfpData = JoyUtilities::formatAdminGraphData($weeklyData, $weekArr);
                
        return $weeklyRfpData;
    }    

    public static function getRfpCountMonthlyByDateRange($startDate, $endDate) {
        $monthlyData = Yii::app()->db->createCommand("CALL getMonthlyRfp ('$startDate', '$endDate')")->queryAll();

        $monthArr = JoyUtilities::getNoOfMonth($startDate, $endDate);
        $monthlyRfpData = JoyUtilities::formatAdminGraphData($monthlyData, $monthArr);
                
        return $monthlyRfpData;
    }    

    public static function getRfpCountYearlyByDateRange($startDate, $endDate) {
        $yearlyData = Yii::app()->db->createCommand("CALL getYearlyRfp ('$startDate', '$endDate')")->queryAll();
        
        $yearArr = JoyUtilities::getNoOfYear($startDate, $endDate);
        $yearlyRfpData = JoyUtilities::formatAdminGraphData($yearlyData, $yearArr);
        
        return $yearlyRfpData;
    }        
 
}
