<?php

Yii::import('application.models.base.BaseUserCompany');

class UserCompany extends BaseUserCompany {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            array('name, description, phonenumber, countryid, stateid, cityid', 'required', 'on' => 'editProfile, createProfile'),
            array('address1', 'required', 'on' => 'editProfile, createProfile', 'message'=>'Address cannot be blank'), 
            array('name, address1, address2', 'length', 'max' => 50, 'min' => 1, 'on' => 'editProfile, createProfile'),
            array('twitterhandle', 'length', 'max' => 20, 'min' => 1, 'on' => 'editProfile, createProfile'),
            array('twitterhandle', 'match', 'pattern' => '/^[a-zA-Z0-9]+$/', 'message' => Yii::t("usercompany", "Twitter handle must be alphanumeric"), 'on' => 'editProfile, createProfile'),
            array('description', 'length', 'min' => 1, 'max' => 512, 'on' => 'editProfile, createProfile'),
            array('name', 'match', 'pattern' => '/^[a-zA-Z ]*$/', 'message' => Yii::t("usercompany", "Company Name must contain letters only"), 'on' => 'editProfile, createProfile'),
            //array('description', 'match', 'pattern'=>'/^[a-zA-Z. ]*$/', 'message' => Yii::t("usercompany", "A few lines about your company must contain letters only"), 'on' => 'editProfile, createProfile'),
            array('websiteurl', 'url', 'allowEmpty' => true, 'on' => 'editProfile, createProfile'),
            array('facebookprofile, linkedinprofile, googleplusprofile', 'url', 'on' => 'editProfile, createProfile'),
            array('phonenumber', 'numerical', 'integerOnly' => true, 'message' => Yii::t("usercompany", "Phone number must contain numbers only"), 'on' => 'editProfile, createProfile'),
            //array('address1, address2', 'match', 'pattern'=>'/^[a-zA-Z0-9 ]*$/', 'message' => Yii::t("usercompany", "Address must contain letters only"), 'on' => 'editProfile, createProfile'),
            array('postalcode', 'numerical', 'integerOnly' => true, 'message' => Yii::t("usercompany", "ZIP must contain numbers only"), 'on' => 'editProfile, createProfile'),
            array('id', 'safe', 'on' => 'editProfile'),
            array('logo', 'file', 'types' => 'jpg, jpeg, gif, png', 'allowEmpty' => true, 'on' => 'editProfile, createProfile',
                'maxSize' => 1024 * 1024 * 5,
                'wrongType' => 'Please upload a valid file. (Format - jpg, jpeg, gif, png).',
                'tooLarge' => 'Please upload a valid file. (Size max 5mb).'),
            //array('logo', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>true, 'on'=>'createProfile', 'message' => 'Only .jpg, .gif and .png file is allowed.'),                    
            //array('logo', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>true, 'on' => 'editProfile, createProfile', 'message' => 'Only .jpg, .gif and .png file is allowed.'),
            array('alias, logo', 'safe'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'userid' => 'Userid',
            'name' => 'Name',
            'description' => 'Description',
            'logo' => 'Logo',
            'websiteurl' => 'Website url',
            'phonenumber' => 'Phone number',
            'countryid' => 'Country',
            'stateid' => 'State',
            'cityid' => 'City',
            'address1' => 'Address1',
            'address2' => 'Address2',
            'postalcode' => 'Zip',
            'facebookprofile' => 'Facebook profile',
            'twitterhandle' => 'Twitter handle',
            'linkedinprofile' => 'Linkedin profile',
            'googleplusprofile' => 'Google+ profile',
        );
    }

    // Return User company data as array by user id
    public static function getCompanyDataByUserId($userId) {
        $criteria = new CDbCriteria();
        $criteria->select = '*';
        $criteria->condition = "userid = :userId ";
        $criteria->params = array(':userId' => $userId);
        $result = self::model()->find($criteria);
        return $result;
    }

    // Return User company data as array by company id
    public static function getCompanyDataById($companyId) {
//            $criteria = new CDbCriteria();
//            $criteria->select = '*';
//            $criteria->condition = "id = :id";
//            $criteria->params = array(':id' => $companyId);
        $result = self::model()->with(array('country', 'city', 'state'))->findByPk($companyId);
        return $result;
    }

    // return unique company name alias
    public static function companyNameAlias($alias, $companyId = 0, $counter = 2) {
        $criteria = new CDbCriteria();
        $criteria->select = 'count(1)';

        // In case of edit pass company id and ignore that company from condition
        if ($companyId) {
            $criteria->addCondition("id != :id");
            $criteria->params[':id'] = $companyId;
        }

        $criteria->addCondition("alias = :alias");
        $criteria->params[':alias'] = $alias;

        $result = self::model()->count($criteria);

        // If alias already exist
        if ($result) {

            // in case of same alias add counter at the end of alias
            $aliasArr = explode('-', $alias);
            $currentCounter = $aliasArr[count($aliasArr) - 1];
            if (is_numeric($currentCounter)) {
                array_pop($aliasArr);
                $aliasArr[] = $counter;
                $alias = implode("-", $aliasArr);
            } else {
                $alias = $alias . "-" . $counter;
            }
            $counter++;
            $alias = self::companyNameAlias($alias, $companyId, $counter);
        }
        return $alias;
    }

//    public static function getCompanyUrlData($companyId) {
//        $criteria = new CDbCriteria();
//        $criteria->select = 'id, alias, userid';
//        $criteria->condition = "id=:id";
//        $criteria->params = array(':id' => $companyId);
//
//        $data = self::model()->find($criteria);
//
//        $companyUrlData['id'] = $data['id'];
//        $companyUrlData['alias'] = $data['alias'];
//        $companyUrlData['userid'] = $data['userid'];
//        return $companyUrlData;
//    }
    
    public static function getCompanyUrlData($userId) {
        $criteria = new CDbCriteria();
        $criteria->select = 'id, alias, userid';
        $criteria->condition = "userid=:userid";
        $criteria->params = array(':userid' => $userId);

        $data = self::model()->find($criteria);

        $companyUrlData['id'] = $data['id'];
        $companyUrlData['alias'] = $data['alias'];
        $companyUrlData['userid'] = $data['userid'];
        return $companyUrlData;
    }

    public static function getCompanyDataFromAlias($alias) {
        $criteria = new CDbCriteria();
        $criteria->select = 'id, name, alias, userid, logo, phonenumber, availability_auto_mail_trigger';
        $criteria->condition = "alias=:alias";
        $criteria->params = array(':alias' => $alias);

        $data = self::model()->find($criteria);

        $uniUrl['id'] = $data->id;
        $uniUrl['name'] = $data->name;
        $uniUrl['alias'] = $data->alias;
        $uniUrl['userid'] = $data->userid;
        $contact = array();
        $contact['Phonenumber'] = $data->phonenumber;
        $uniUrl['Contact'] = $contact;
        //for new eatads 
        $uniUrl['Vendorname'] = $data->name;
        $uniUrl['autoAvailabilityMailerFlag'] = $data->availability_auto_mail_trigger;
        if (!empty($data->logo)) {
            $uniUrl['Companylogo'] = JoyUtilities::getAwsFileUrl($data->logo, 'companylogo');
        } else {
            $uniUrl['Companylogo'] = JoyUtilities::getAwsFileUrl('default_listing.png', 'companylogo');
        }
        
        return $uniUrl;
    }
    
    
    public static function isUserCompany($userId) {
        // check if listing is in solr and status is not deleted
        $criteria = new CDbCriteria();
        $criteria->select = 'id';
        $criteria->condition = "userid = :userid";
        $criteria->params = array('userid'=>$userId);
        $result = self::model()->find($criteria);
        if($result) {
            return $result->id;
        } else {
            return 0;
        }
    }
    
    public static function isUserAliasCompany($userId) {
        // check if listing is in solr and status is not deleted
        $criteria = new CDbCriteria();
        $criteria->select = 'id';
        $criteria->condition = "alias = :alias";
        $criteria->params = array('alias'=>$userId);
        $result = self::model()->find($criteria);
        if($result) {
            return $result->id;
        } else {
            return 0;
        }
    }

    public static function deleteCompanyLogoByIdAndFileName($cId, $fileName) {
        $result = self::model()->updateByPk($cId, array('logo' => NULL), 'id = ' . $cId . ' AND logo = "' . $fileName . '"');
        return $result;
    }

    /* This function will return all state of media owner Listing in comma separeted string */

    public static function getOwnerListingState($ownerId) {
        $sql = 'SELECT GROUP_CONCAT(DISTINCT(a.name) SEPARATOR ", ") as state_string FROM Listing AS l INNER JOIN `Area` AS a ON a.id=l.stateid WHERE foruserid = ' . $ownerId;
        $command = Yii::app()->db->createCommand($sql);
        $data = $command->queryRow();
        if($data) {
            return $data['state_string'];
        }
    }
    
    public static function getCompanyDataByUserEmail($email) {
        $sql = 'select id from UserCompany where userid = (select id from User where email = \'' . $email .'\')';
        $command = Yii::app()->db->createCommand($sql);
        $data = $command->queryRow();
//        if($data) {
//            return $data['state_string'];
//        }
        return $data;
    }
}
