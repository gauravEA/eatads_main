<?php

Yii::import('application.models.base.BaseIp2nationCountries');

class Ip2nationCountries extends BaseIp2nationCountries
{
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    public static function getCountryLatLng($countryName)
    {        
        $result = self::model()->findByAttributes(array('country'=>$countryName), array('select'=>'lat,lon'));
        if($result) {
            $latLng = $result['lat']. ',' .$result['lon'];
        } else {
            $latLng = Yii::app()->params['map_latlng'];              // India
        }        
        return $latLng;
    }
    
//    public static function getShortCodeByCountry($countryName)
//    {
//        $result = self::model()->findByAttributes(array('country'=>$countryName), array('select'=>'iso_code_2'));
//        $countryCode = null;
//        if($result) {
//            $countryCode = $result['iso_code_2'];
//        }
//        return $countryCode;
//    }
    
}
