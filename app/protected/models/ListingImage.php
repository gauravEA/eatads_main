<?php

Yii::import('application.models.base.BaseListingImage');

class ListingImage extends BaseListingImage
{
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public static function getListingImageName($listingId) {
            $criteria = new CDbCriteria();
            $criteria->select = '*';
            $criteria->condition = "listingid = :listingId";
            $criteria->params = array(':listingId' => $listingId);
            $result = self::model()->findAll($criteria);
            return $result;
        }

        public static function getListingImageCount($listingId) {
            $criteria = new CDbCriteria();
            $criteria->select = '*';
            $criteria->condition = "listingid = :listingId";
            $criteria->params = array(':listingId' => $listingId);
            $result = self::model()->count($criteria);
            return $result;
        }

        
        public static function deleteListingImage($imageId, $listingId, $imageName) {
            return self::model()->deleteByPk($imageId, "listingid = $listingId and filename = '$imageName'");
        }
        
        public static function isImageExist($imageName) {
            $criteria = new CDbCriteria();
            $criteria->select = '*';
            $criteria->condition = "filename = :filename and new_status = 0";
            $criteria->params = array(':filename' => $imageName);
            $result = self::model()->findAll($criteria);
            return $result;
        }
        
        
        public static function getListingImageNameOnly($listingId) {
            $criteria = new CDbCriteria();
            $criteria->select = 'filename';
            $criteria->condition = "listingid = :listingId";
            $criteria->params = array(':listingId' => $listingId);
            $result = self::model()->findAll($criteria);
            return $result;
        }
}
