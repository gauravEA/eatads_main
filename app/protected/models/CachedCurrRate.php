<?php

Yii::import('application.models.base.BaseCachedCurrRate');

class CachedCurrRate extends BaseCachedCurrRate
{	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    public static function updateRates($data)
    {
        Yii::app()->db->createCommand()->truncateTable(self::model()->tableName());        
        foreach($data as $key => $value) {
            //echo $key . ' => ' . $value . '<br />';
            $obj = new CachedCurrRate;
            $obj->currency = $key;
            $obj->rate = $value;
            $obj->datemodified = date("Y-m-d H:i:s");
            $obj->save();            
        }
    }
    public static function getRates()
    {
        $data = self::model()->findAll(array('select'=>'currency,rate'));        
        $res = array();
        foreach($data as $curr) {
            $res[$curr->currency] = (float)$curr->rate;
        }
        $result['rates'] = (object)$res;
        return (object)$result;
    }
}
