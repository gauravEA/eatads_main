<?php

Yii::import('application.models.base.BaseAudienceTag');

class AudienceTag extends BaseAudienceTag {

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required', 'except' => 'addlisting'),
            array('status', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 20),
            array('id', 'checkAudienceTag', 'on' => 'addlisting'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, status', 'safe', 'on' => 'search'),
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function checkAudienceTag($attribute, $params) {
        /* echo "<pre>";
          print_r(count($_POST['AudienceTag']['id']));
          print_r($_POST['AudienceTag']['id']);
          var_dump(isset($_POST['AudienceTag']['id'][0]));
          exit; */
        if (count($_POST['AudienceTag']['id']) > 3) {
            $this->addError($attribute, 'You can not select more than 3 audiecne tag.');
        } else if (!isset($_POST['AudienceTag']['id'][0])) {
            $this->addError($attribute, 'Please select at least one audience tag.');
        }
    }
    
    // MASS UPLOAD VALIDATION
    public static function getAudienceTag($audienceTag) {
        $tagArr = $audienceTagArr = array();
        if(strlen($audienceTag)) {
            $audienceTagArr = explode(",", $audienceTag);
            $audienceTagArr = array_map('trim', $audienceTagArr);
            
            $tagMax = 3;
            $counter = 1;
            foreach($audienceTagArr as $key => $value) {
                $tagArr[] = $value;
                if($counter == $tagMax) {
                    break;
                }
                $counter++;
            }
        }
        
        return $tagArr;
    }
    
    // MASS UPLOAD VALIDATION
    public static function isAudienceTagExist($audienceTag) {
        
        $tagArr = self::getAudienceTag($audienceTag); 
        $tagCount = count($tagArr);
               
        $criteria = new CDbCriteria();
        $criteria->select = 'id';
        $criteria->addInCondition('name', $tagArr);

        $data = self::model()->findAll($criteria);
        if($tagCount == count($data)) {
            return true;
        } else {
            return false;
        }
        
    }
    
    public static function getTagIdByName($tag) {
        $criteria = new CDbCriteria();
        $criteria->select = 'id';
        $criteria->addCondition('name = "'.$tag.'"');

        $data = self::model()->find($criteria);
        if($data) {
            return $data->id;
        } else {
            return false;
        }
        
    }
    
}
