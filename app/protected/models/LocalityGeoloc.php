<?php

Yii::import('application.models.base.BaseLocalityGeoloc');

class LocalityGeoloc extends BaseLocalityGeoloc
{	    
    public $locality;
    public $geoloc;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('sublocality, geolat, geolng', 'required'),
			array('sublocality1, sublocality2, sublocality', 'length', 'max'=>100),									
			array('id, sublocality1, sublocality2, sublocality, geolat, geolng', 'safe', 'on'=>'search'),
            array('geolat', 'length', 'allowEmpty' => true, 'max' => 10, 'on' => 'createListing', 'message' => 'Please provide a valid latitude.'),
            array('geolat, geolng', 'type', 'allowEmpty' => true, 'type' => 'float'),
            array('geolat', 'compare', 'operator' => '<=', 'compareValue' => 90, 'message' => 'Please provide a valid latitude.', 'allowEmpty' => true),
            array('geolat', 'compare', 'operator' => '>=', 'compareValue' => -90, 'message' => 'Please provide a valid latitude.', 'allowEmpty' => true),
            array('geolat', 'match', 'pattern' => '/^[0-9.-]*$/', 'on' => 'createListing', 'message' => 'Please provide a valid latitude.'),
            array('geolng', 'length', 'allowEmpty' => true, 'max' => 11, 'on' => 'createListing', 'message' => 'Please provide a valid longitude.'),
            array('geolng', 'compare', 'operator' => '<=', 'compareValue' => 180, 'message' => 'Please provide a valid longitude.', 'allowEmpty' => true),
            array('geolng', 'compare', 'operator' => '>=', 'compareValue' => -180, 'message' => 'Please provide a valid longitude.', 'allowEmpty' => true),
            array('geolng', 'match', 'pattern' => '/^[0-9.-]*$/', 'on' => 'createListing', 'message' => 'Please provide a valid longitude.'),            
        );
    }
}
