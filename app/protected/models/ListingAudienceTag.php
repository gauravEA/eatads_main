<?php

Yii::import('application.models.base.BaseListingAudienceTag');

class ListingAudienceTag extends BaseListingAudienceTag
{	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public static function getListingAudienceTag($listingId) {
            $audienceTag = self::model()->with('audiencetag')->findAll(array('condition' => 'listingid = '.$listingId));
            return $audienceTag;
        }
}
