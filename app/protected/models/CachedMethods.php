<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *  Cached vars
 *  rates
 *  curr_symbols
 *  min_max_listprice
 */
class CachedMethods {

    public static function getCurrencySymbols() {
        $curr_sym = LookupBaseCurrency::getBaseCurrencyList();
        $currSymArray = array();
        foreach ($curr_sym as $cs) {
            $currSymArray[$cs->currency_code] = $cs->currency_symbol;
        }
        Yii::app()->cache->set('curr_symbols', $currSymArray, 24 * 60 * 60);      // set rates for 24 hours
        return $currSymArray;
    }

    public static function getMinMaxListPrice() {

        $minMaxArray = Listing::getMinMaxPrice();
        // set rates for 24 hours
        // as discuessed changed to 1 hour from 24 hr
        Yii::app()->cache->set('min_max_listprice', $minMaxArray, 1 * 60 * 60);
        return $minMaxArray;
    }

}
