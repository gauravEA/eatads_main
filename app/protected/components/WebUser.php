<?php 
class WebUser extends CWebUser
{
    
    /**
     * Overrides a Yii method that is used for roles in controllers (accessRules).
     *
     * @param string $operation Name of the operation required (here, a role).
     * @param mixed $params (opt) Parameters for this operation, usually the object to access.
     * @return bool Permission granted?
     */
    public function checkAccess($operation, $params=array())
    {
        if (empty($this->id)) {
            // Not identified => no rights
            return false;
        }
        $role = $this->getState("roleId");
        if ($role === JoyUtilities::ROLE_ADMIN) {
            return true; // admin role has access to everything
        }
        // allow access if the operation request is the current user's role
        return ($operation === $role);
    }
    
    public static function allowActiveUser($message) {
        if(Yii::app()->user->active == 0) {
                throw new CHttpException(403, 'Activate your account - Your activation link has been sent on mail. Please check your mail.');
                return false;
        } else {
            if( Yii::app()->user->status == 0) {
                throw new CHttpException(403, 'Activate your account - Your activation link has been sent on mail. Please check your mail.');
                return false;
            } else {
                return true;
            }
        }
    }
}