<?php

class Uploadify extends CWidget {
    public $fileFieldId;
    public $imageType;
    public $uploadify_filenames;
    
    public function init() {
        
    }

    public function run() {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.uploadifive.min.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerCSSFile(Yii::app()->baseUrl . '/css/uploadifive.css');
        
        $timestamp = time();
//        echo "<style>.uploadifive-button {
//                        float: left;
//                        margin-right: 10px;
//                }
//                #queue {
//                        border: 1px solid #E5E5E5;
//                        height: 177px;
//                        overflow: auto;
//                        margin-bottom: 10px;
//                        padding: 0 3px 3px;
//                        width: 300px;
//                }</style>";
        
        // <a style="position: relative; top: 8px;" href="javascript:$('#file_upload').uploadifive('upload')">Upload Files</a>
        // hidden element inside <form>
        // comma saperated filenames will be appended when upload file is clicked
        //'uploadLimit'  : ". Yii::app()->params['fileUpload']['count'] .",
        
        
        echo "<script type='text/javascript'>
            $(document).ready(function() {
                $('.dropzone .rows').hide();
                $('#". $this->fileFieldId ."').uploadifive({
                    'auto': false,
                    //   'checkScript': 'check-exists.php',
                    'formData': {
                                    'timestamp' : '". $timestamp ."',
                                    'token'     : '". md5('unique_salt' . $timestamp) ."',
                                    'imageType' : '". $this->imageType ."'
                    },
                    'queueID': 'queue',
                    'fileType'     : 'image',
                    'fileSizeLimit' : ". Yii::app()->params['fileUpload']['size'] .",
                    'uploadScript' : '". Yii::app()->urlManager->createUrl('uploadify/uploadFive') ."',
                    'onUploadComplete': function(file, data) {
                        $('.dropzone .rows').show();
                        $('.dropzone .rows').prepend('<div class=\'type\' rel=\'' + data + '\' style=\'background:url() no-repeat top left; background-size:130px\'><img src=\''+ data +'\' height=\'90px\' width=\'90px\'><div class=\'delete\'></div></div>');
                        getCompletedFileListNew();
                        $('div.uploadifive-queue-item.progressbar.error').remove();
                        $('div.clear.error').remove();
                        $('#uploadify_filenames_em_').html('').hide();
                    },
                    'onFallback'   : function() { alert('Our file uploader is not compatible with your browser. Please upgrade to IE10 or Download Chrome.'); },
                });

                $('.imagelist .type .delete').live('click', function() {
                    that = $(this);
                    $.ajax({
                        type: 'POST',
                        url: '". Yii::app()->urlManager->createUrl('uploadify/deleteFile') ."',
                        data: {filename: $(this).parent().attr('rel')},
                        beforeSend: function() {
                            if ($('#saveImage').length) {
                                $('#saveImage').attr('disabled', true);
                            }
                        }
                    }) .done(function( msg ) {
                    
                        if(msg == 1) {
                            var uploadifyCount = parseInt($('#image_count_uplodify').val()) - 1;
                            $('#image_count_uplodify').val(uploadifyCount);
                        }
                        that.parent().fadeOut().remove();
                        if ($('.imagelist .type').length) {
                            
                        }
                        else {
                            $('.dropzone .rows').hide();
                        }

                        
                        // 1 for success 0 otherwise
                        
                        setTimeout(function() {
                            getCompletedFileListNew();
                            // 1000 for 1 seconds
                        }, 1000);
                        
                        if ($('#saveImage').length) {
                            $('#saveImage').attr('disabled', false);
                        }

                    });

                });
                
                // Add hidden field to keep currently uploaded file name
                $('.upload').after('<input type=\'hidden\' id=\'uploadify_filenames\' value=\'". $this->uploadify_filenames ."\' name=\'uploadify_filenames\' /><div style=\'display:none;\' id=\'uploadify_filenames_em_\' class=\'errormessage\'></div>');

                // Add upload file link
                $('.upload').after('<a class=\"btn btn-lg btn-success upload2 disabled\" href=\"javascript:$(\'#". $this->fileFieldId ."\').uploadifive(\'upload\')\">Upload images</a>');    


            });
            
            function getCompletedFileList() {                
                var fileArr = [];
                $( '#queue .uploadifive-queue-item' ).each(function( index ) {
                    fileArr.push( $.trim( $( this ).find('.filename').text() ) );
                });
                
                fileArr.join( ',' );
                $('#uploadify_filenames').val(fileArr);
            }

            function getCompletedFileListNew() {  
                var fileArr = [];
                $('.imagelist .type').each(function( index ) {
                    var relfilename = $(this).attr('rel');
                    var index = relfilename.lastIndexOf('/') + 1;
                    fileArr.push( relfilename.substr(index));
                });
                
                fileArr.join( ',' );
                $('#uploadify_filenames').val(fileArr);
            }

            </script>";
        
        
//        echo "<script type='text/javascript'>
//                $(function() {
//                    $('#". $this->fileFieldId ."').before('<div id=\"queue\"></div>');
//
//                    $('#". $this->fileFieldId ."').uploadifive({
//                        'auto'             : false,
//                        //'checkScript'      : '". Yii::app()->urlManager->createUrl('uploadify/checkExist') ."',
//                        'formData'         : {
//                                                'timestamp' : '". $timestamp ."',
//                                                'token'     : '". md5('unique_salt' . $timestamp) ."',
//                                                'imageType' : '". $this->imageType ."'
//                                                },
//                        'queueID'          : 'queue',
//                        'fileType'     : 'image',
//                        'uploadLimit'  : ". Yii::app()->params['fileUpload']['count'] .",
//                        'uploadScript' : '". Yii::app()->urlManager->createUrl('uploadify/uploadFive') ."',
//                        'onUploadComplete' : function(file, data) { getCompletedFileList(); },
//                        'onFallback'   : function() { alert('Our file uploader is not compatible with your browser. Please upgrade to IE10 or Download Chrome.'); },
//                        //'onInit' : function(options) { alert(options); }, 
//                        'onCancel'    : function() { 
//                                            $.ajax({
//                                                type: 'POST',
//                                                url: '". Yii::app()->urlManager->createUrl('uploadify/deleteFile') ."',
//                                                data: { filename: file.name }
//                                            }) .done(function( msg ) {
//                                                // 1 for success 0 otherwise
//                                                
//                                                setTimeout(function() {
//                                                    getCompletedFileList();
//                                                    // 1000 for 1 seconds
//                                                }, 1000)
//                                                
//                                            });
//                                        }
//                    });
//
//                    // Add hidden field to keep currently uploaded file name
//                    $('.uploadifive-button').after('<input type=\'hidden\' id=\'uploadify_filenames\' value=\'". $this->uploadify_filenames ."\' name=\'uploadify_filenames\' />');
//                    
//                    // Add upload file link
//                    $('.uploadifive-button').after('<a href=\"javascript:$(\'#". $this->fileFieldId ."\').uploadifive(\'upload\')\">Upload Files</a>');
//
//                    //var cancel_btn = '<a style=\'position: relative; top: 8px;\' href=\'javascript:$(\'#file_upload\').uploadifive(\'cancel\',$(\'.uploadifive-queue-item\').first().data(\'file\'));\'>Cancel Files</a>';
//                    
//                });
//
//                function getCompletedFileList() {
//                    var fileArr = [];
//                    $( '.uploadifive-queue-item' ).each(function( index ) {
//                        if( $( this ).find('.fileinfo').text() == ' - Completed' ) {
//                            fileArr.push( $( this ).find('.filename').text() );
//                        }
//                    });
//                    fileArr.join( ',' )
//                    $('#uploadify_filenames').val(fileArr);
//                }
//
//	</script>";
    }

}

?>