<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/main';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();
    public $ipCurrencyId = null;
    public $ipCurrencyCode = null;

    public function init() {

        // if currency cookie not set get currency from IP
        if (!isset($_COOKIE['IpCurrency'])) {
            $result = LookupBaseCurrency::getCurrencyIdByIp();            
            if (($result['id']==null || $result['id']=='') || ($result['currencycode']=='' || $result['currencycode']==null)) {                 
                $this->ipCurrencyId = 20;            // default USD 20
                $this->ipCurrencyCode = 'USD';      // default USD 20                
            } else {                
                $this->ipCurrencyId = $result['id'];
                $this->ipCurrencyCode = $result['currencycode'];                
            }
            $cookie = $this->ipCurrencyId .'#'.$this->ipCurrencyCode;
            setcookie('IpCurrency', $cookie, time() + 30* 24 * 60 * 60 , '/');            
        } else {
            $cookie = explode('#', $_COOKIE['IpCurrency']);
            if($cookie[0]==0 || $cookie[0]==null || $cookie[0]=='') {                
                // case when Select Country is selected
                $this->ipCurrencyId = 20;            // default USD 20
                $this->ipCurrencyCode = 'USD';      // default USD 20
                $cookie = $this->ipCurrencyId .'#'.$this->ipCurrencyCode;                
                setcookie('IpCurrency', $cookie, time() + 30 * 24 * 60 * 60, '/');                
            } else {                
                $this->ipCurrencyId = $cookie[0];            
                $this->ipCurrencyCode = $cookie[1];                
            }
        }

        Yii::app()->clientScript->registerMetaTag(Yii::app()->params['meta_keyword'], 'keywords', null, array('id' => 'meta_keywords'), 'meta_keywords');
        Yii::app()->clientScript->registerMetaTag(Yii::app()->params['meta_description'], 'description', null, array('id' => 'meta_description'), 'meta_description');
    }

}