<?php
/* @var $this ListingController */
/* @var $model Listing */
?>

<script>
    var address = "", city = "", state = "", zip = "", country = "", country_short="";
    function toUcWords(str)
    {
        str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        });
        return str;
    }
    function validateLocation() {
        var lat = $('#Listing_geolat').val();
        var lng = $('#Listing_geolng').val();
        if(lat && lng) {                            
                currLatLng = new google.maps.LatLng(lat, lng);                
                getLatLongDetail(currLatLng, false);    // false for no db update
                setTimeout(function(){ 
                    var country_dd = toUcWords( $('#Listing_countryid option:selected').text() );
                    var state_dd = toUcWords( $('#Listing_stateid option:selected').text() );
                    var city_dd = toUcWords( $('#Listing_cityid option:selected').text() );
                    
                    //console.log('country - ' + country_dd);
                    //console.log('state - ' + state_dd);
                    //console.log('city - ' + city_dd);
                    //console.log('lat - ' + lat);
                    //console.log('country='+country+' state='+state+' city='+city);
                    if(country === country_dd && state === state_dd && city === city_dd) {
                        //console.log('match');
                        return true;
                    } else {
                        //console.log('mis-match');
                        return false;                       
                    }
                }, 1000);                      
        } else {
            //console.log('else');
            return true;
        }
    }
    function getLatLongDetail(curLatLng, updateDb) {        
        var geocoder = new google.maps.Geocoder(); 
        geocoder.geocode({ 'latLng': curLatLng },
          function (results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                  if (results[0]) {
                      var address = "", city = "", state = "", zip = "", country = "", country_short="";
                      
                      for (var i = 0; i < results[0].address_components.length; i++) {
                          
                          var addr = results[0].address_components[i];
                          //console.log(addr);
                          // check if this entry in address_components has a type of country
                          if (addr.types[0] == 'country') {
                              country = addr.long_name;
                              country_short = addr.short_name;
                          } else if (addr.types[0] == 'street_address') { // address 1 
                              address = address + addr.long_name;
                          } else if (addr.types[0] == 'establishment') {
                              address = address + addr.long_name;
                          } else if (addr.types[0] == 'route') { // address 2
                              address = address + addr.long_name;
                          } else if (addr.types[0] == 'postal_code') {      // Zip
                              zip = addr.short_name;
                          } else if (addr.types[0] == 'administrative_area_level_1') {      // State
                              state = addr.long_name;
                          } else if (addr.types[0] == 'locality') {      // City
                              city = addr.long_name; 
                          } else if (addr.types[0] == 'administrative_area_level_2' && city.length===0) {      // City
                              city = addr.long_name; 
                          }
                      }
                                            
                       
                      if(updateDb) {
                        // country, state, city not empty 
                        if(state == '') {
                            state = country;
                        }
                        if(city == '') {
                            city = state;
                        }
                        //console.log('country='+country+' state='+state+' city='+city);
                        if(country && state && city) {
                            $.ajax({                
                                  type: "GET",
                                  url: "<?php echo Yii::app()->urlManager->createUrl('ajax/setlocation'); ?>", //url to call.
                                  cache: true,
                                  data: {                                
                                      country: country,
                                      country_short: country_short,
                                      state: state,
                                      city: city
                                  },
                                  success: function (data) {
                                      var obj = jQuery.parseJSON(data);
                                      // console.log(obj.c);
                                      // console.log(obj.s);
                                      // console.log(obj.ci);
                                      // See more at: http://www.jqfancyfields.com/examples-docs/#sthash.qwn8FgiY.R8UyJVmn.dpuf
                                      $('#Listing_countryid').fancyfields("unbind","onSelectChange");
                                      $('#Listing_stateid').fancyfields("unbind","onSelectChange");

                                      $('#Listing_countryid').setOptions([["Select country", ""], [obj.c[0], obj.c[1]]]);
                                      $('#Listing_stateid').setOptions([["Select state", ""], [obj.s[0], obj.s[1]]]);
                                      $('#Listing_cityid').setOptions([["Select city", ""], [obj.ci[0], obj.ci[1]]]);

                                      $('#Listing_countryid').setVal(obj.c[1]);
                                      $('#Listing_stateid').setVal(obj.s[1]);
                                      $('#Listing_cityid').setVal((obj.ci[1]));
                                  }
                            });    
                        } else {
                            alert('Adderss not valid.');
                        }
                      } else {
                          // country state cit var already set                          
                      }
                      
                      // console.log('City: '+ city + '\n' + 'State: '+ state + '\n' + 'Country: '+ country + '\n' + 'Address: '+ address + '\n' + 'Zip: '+ zip);
                      // call reverse geocode                        
                      // http://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&sensor=false
                      // get country state city
                      // match corresponding values in db
                      // update country/state/city select boxes
                  }
              }
          });
    }
    
    var lat_fld = "<?php echo $latitude_fieldname; ?>";
    var lng_fld = "<?php echo $longitude_fieldname; ?>";
    
    $(document).ready(function(){
        $('#'+lat_fld).blur(function(){
            var lat = parseFloat($('#'+lat_fld).val());
            var lng = parseFloat($('#'+lng_fld).val());
            if($.isNumeric(lat) && $.isNumeric(lng)) {                
                moveMarker(lat, lng); 
            }
        });
        $('#'+lng_fld).blur(function(){            
            var lat = parseFloat( $('#'+lat_fld).val());
            var lng = parseFloat( $('#'+lng_fld).val());
            if($.isNumeric(lat) && $.isNumeric(lng)) {
                moveMarker(lat, lng);
            } 
        });
    });
    // global marker
    var marker;
    var map;
    function moveMarker(lat,lng) {
        var newLatLng = new google.maps.LatLng(lat, lng);
        marker.setPosition(newLatLng);
        map.panTo(marker.position);
    } 
    function initialize() {
      // Enable the visual refresh
      google.maps.visualRefresh = true;
      var myLatLng;
      var lat = parseFloat($('#<?php echo $latitude_fieldname; ?>').val());
      var lng = parseFloat($('#<?php echo $longitude_fieldname; ?>').val());
      if($.isNumeric(lat) && $.isNumeric(lng)) {      
          myLatLng = new google.maps.LatLng(lat, lng);
      } else {
          myLatLng = new google.maps.LatLng(22.84, 78.75);
      }
      
      var mapOptions = {
        zoom: 3,
        center: myLatLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        streetViewControl: false,
        panControl: false,
        mapTypeControl: false
      };
      map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

      //var point=new GLatLng(49.461,-2.58);
      marker = new google.maps.Marker({
        //position: point,
        position: myLatLng,
        map: map,
        //title: 'Click to zoom',
        draggable: true
      });

      /*google.maps.event.addListener(map, 'center_changed', function() {
        // 3 seconds after the center of the map has changed, pan back to the marker.
        window.setTimeout(function() {
          map.panTo(marker.getPosition());
        }, 3000);
      });*/

      /*google.maps.event.addListener(marker, 'click', function() {
        map.setZoom(8);
        map.setCenter(marker.getPosition());
      });*/

      google.maps.event.addListener(marker, 'drag', function() {  // drag function
        var curLatLng = marker.getPosition();
        document.getElementById(lat_fld).value = curLatLng.lat().toFixed(6);
        document.getElementById(lng_fld).value = curLatLng.lng().toFixed(6);        
      });
      google.maps.event.addListener(marker, 'dragend', function() {  // or use dragend
        map.panTo(marker.getPosition());
        var curLatLng = marker.getPosition();
        <?php echo (strlen($callback_function_name)>1) ? $callback_function_name.'();': ''; ?>;
        $('#Listing_countryid').fancyfields("disable");
        $('#Listing_stateid').fancyfields("disable");
        $('#Listing_cityid').fancyfields("disable"); 
        $('#select_disable_flag').val('1');
        getLatLongDetail(curLatLng, true);         // update db
      });

    }      
    function loadScript()
    {
      var script = document.createElement("script");
      script.type = "text/javascript";
      script.src = "https://maps.googleapis.com/maps/api/js?key=<?php echo Yii::app()->params['gmapApiKey']; ?>&sensor=false&callback=initialize";
      // &language=ja for language change
      // &region=GB for regional settings
      document.body.appendChild(script);
    }
    window.onload = loadScript;
</script>