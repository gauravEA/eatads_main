<script>
    // global marker
    var marker;
    var map;
    function initialize() {
        
        <?php foreach($mapFieldData as $map) { ?>
            // Enable the visual refresh
            google.maps.visualRefresh = true;
            lat = <?php echo $map['lat']; ?>;
            lng = <?php echo $map['lng']; ?>;
            myLatLng = new google.maps.LatLng(lat, lng);

            var mapOptions = {
                zoom: <?php echo isset($map['zoomLevel']) ? $map['zoomLevel'] : 12; ?>,
                center: myLatLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                streetViewControl: false,
                panControl: false,
                mapTypeControl: false
            };    
                
            map = new google.maps.Map(document.getElementById("<?php echo $map['fieldid']; ?>"), mapOptions);
            if(<?php echo $map['showMarker'] ? $map['showMarker'] : '0'; ?>) {
                    marker = new google.maps.Marker({
                    //position: point,
                    position: myLatLng,
                    map: map,
                    //title: 'Click to zoom',
                    draggable: false
                });
            }
        <?php } ?>
    }      
    function loadScript()
    {
      var script = document.createElement("script");
      script.type = "text/javascript";
      script.src = "https://maps.googleapis.com/maps/api/js?key=<?php echo Yii::app()->params['gmapApiKey']; ?>&sensor=false&callback=initialize";
      // &language=ja for language change
      // &region=GB for regional settings
      document.body.appendChild(script);
    }
    window.onload = loadScript;
</script>