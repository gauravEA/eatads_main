<div class="left">
    <?php if ($header) { ?>
        <h1>Where do you want to <br/>advertise?</h1>            
    <?php } ?>
    <!-- Common fields -->  
    <?php /*
    <div id="common">        
        <div class="col-sm-6 clearfix">
            <?php echo CHtml::dropDownList('countryid', 'id', Area::model()->getPriorityCountryOptions(), array('empty' => 'Select country', 'tabindex' => 1)); ?>             
        </div>
        <div class="col-sm-6 clearfix" id="div_state">
            <?php echo CHtml::dropDownList('stateid', '', array(), array('empty' => 'Select state')); ?>
        </div>
        <div class="col-sm-6 clearfix">
            <?php echo CHtml::dropDownList('cityid', '', array(), array('empty' => 'Select city')); ?>
        </div>         
    </div> */ ?>
    <div class="col-sm-6 clearfix">
        <?php //echo CHtml::dropDownList('mediatypeid', 'id', CHtml::listData(MediaType::model()->findAll(), 'id', 'name'), array('empty' => 'Select media type')); ?>
        <?php //echo CHtml::checkBoxList('mediatypeid', 'id', CHtml::listData(MediaType::model()->findAll(), 'id', 'name'), array('class'=>'mediatype')); ?>
        <?php echo CHtml::dropDownList('mediatypeid', '', 
                                    CHtml::listData(MediaType::model()->findAll(), 'id', 'name'), 
                                    array('multiple'=>'multiple', 'data-title'=>'Media Type')); ?>
        
    </div>
    <div class="col-sm-6 clearfix">
        <?php echo CHtml::textField('locality', '', array('class'=>'form-control')); 
            echo CHtml::hiddenField('mapcenter', '', array('class'=>'form-control'));
        ?>
    </div>
    <div class="col-sm-6 clearfix errormessage" id="errorCountryMediaType" style="display:none;">Please select country and media type to search.</div>
    <!-- Normal search -->
    <div class="left normal">
        <div class="clear clearfix"></div>
        <div class="col-sm-6 clearfix">
            <?php echo CHtml::button('Search', array('class' => 'btn btn-lg btn-success big btn_search', 'name' => 'btn_search')); ?>
            <!--<a href="#" class="change">Advanced Search</a>-->
        </div>
        <div class="clear clearfix"></div>
    </div>
                    
    <!-- Advanced search -->
    <div class="left advanced">
        <div class="hr"></div>
        <div class="row">
            <div class="col-sm-6 clearfix">
                <p class="range">Length:</p><input type="text" id="length" name="length" tabindex="1" class="form-control" />
            </div>
            <div class="col-sm-6 ">
                <p class="range">Width:</p><input type="text" id="width" name="width" tabindex="1" class="form-control" />                            
            </div>
            <div class="col-sm-6 clearfix">
                <p class="range">Size Unit:</p>
                <?php foreach (Listing::getSizeUnit() as $key => $sizeUnit): ?>
                    <input type="radio" name="sizeunit" value="<?php echo $key; ?>" /><?php echo $sizeUnit; ?>&nbsp;
                <?php endforeach; ?>
            </div>
        </div>
        <div class="hr"></div>
        <div class="col-sm-12 clearfix" id="adv_search_slider">
            <p class="range">Price range:</p><input type="text" id="slider-range" />
        </div>
        <div class="hr"></div>
        <div class="col-sm-12 clearfix">
            <div class="col-sm-6 clearfix">
                <p class="range">Reach:</p><input type="text" id="reach" name="reach" tabindex="1" class="form-control" />
            </div>
            <div class=" clearfix">
                <p class="range">Lighting type:</p>
                <?php foreach (Listing::getLighting() as $key => $lightingType): ?>
                    <input type="radio" name="lighting" value="<?php echo $key; ?>" /><?php echo $lightingType; ?>&nbsp;
                <?php endforeach; ?>
            </div>                        
        </div>
        <div class="hr"></div>                    
        <div class="col-sm-6 clearfix">                        
            <?php echo CHtml::button('Search', array('class' => 'btn btn-lg btn-success big btn_search', 'name' => 'btn_search')); ?>
            <a class="change">Back to Basic Search</a>
        </div>
    </div>
    <div class="clear clearfix"></div>
</div>
<?php
    $minMaxListPriceUsd = Yii::app()->cache->get('min_max_listprice');
    if(!$minMaxListPriceUsd) {
        $cmObj = new CachedMethods(); 
        $minMaxListPriceUsd = $cmObj->getMinMaxListPrice();
    }

    $minMaxListPriceNew = array(
        'min' => round(Yii::app()->openexchanger->convertCurrency($minMaxListPriceUsd['min'], 'USD', $ipCurrencyCode)),
        'max' => round(Yii::app()->openexchanger->convertCurrency($minMaxListPriceUsd['max'], 'USD', $ipCurrencyCode))
    );    
?>


<script type="text/javascript">
    function __highlight(s, t) {
        var matcher = new RegExp("("+$.ui.autocomplete.escapeRegex(t)+")", "ig" );
        return s.replace(matcher, "<strong>$1</strong>");
    }
    $(function() {        
        $('#locality').autocomplete({
            source: "<?php echo Yii::app()->urlManager->createUrl('ajax/localitysearch'); ?>",
            minLength: 2,
            messages: {
                noResults: '',
                results: function() {}
            },
            success: function(data) {
                response($.map(data, function(item) {
                    return {label: __highlight(item.locality, request.term) + " (" + item.locality + ")",
                        value: item.locality    };

                })); 
            },
            select: function( event, ui ) {
                console.log( ui.item ?
                  "Selected: " + ui.item.value + " aka " + ui.item.id :
                  "Nothing selected, input was " + this.value );
          
                $("#locality").val( ui.item.locality);
                $("#mapcenter").val( ui.item.geoloc);
                return false;
            }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<li>" )
              .append( "<a>" + item.locality + "</a>" )
              .appendTo( ul );
        };
        
        $('#mediatypeid').multiselect({
                includeSelectAllOption: true,
                enableFiltering: false,
                maxHeight: 250
            });
        // price slider
        $("#slider-range").ionRangeSlider({
            min: <?php echo $minMaxListPriceNew['min']; ?>,
            max: <?php echo $minMaxListPriceNew['max']; ?>,
            from: <?php echo $minMaxListPriceNew['min']; ?>,
            to: <?php echo $minMaxListPriceNew['max']; ?>,
            type: "double",
            step: 1,
            prefix:  $('#selected_currency_sym').html() + " ",
            prettify: true,
            hasGrid: false
        });
        
        $('#stateid').fancyfields("disable");
        $('#cityid').fancyfields("disable");
        
        $('.btn_search').click(function(){
            var countryid = $('#countryid').val();
            //var mediatypeid = $('#mediatypeid').val();
            var mediatypeid = '';
            $(':checkbox:checked').each(function(i){
                 mediatypeid += $(this).val() +',';
            });
            mediatypeid = mediatypeid.replace(/^,|,$/g,'');            
            if(countryid=='' || mediatypeid=='') {
                $('#errorCountryMediaType').show();
            } else {                
                var stateid = ($('#stateid').val()>0) ? $('#stateid').val() : '';
                var cityid = ($('#cityid').val()>0) ? $('#cityid').val() : '';
                
                var price_val_array = $("#slider-range").val().split(";");
                var length = $("#length").val();
                var width = $("#width").val();
                var reach = $("#reach").val();
                var sizeunitid = ($( "input:radio[name=sizeunit]:checked" ).val()) ? $( "input:radio[name=sizeunit]:checked" ).val() : '';
                var lightingid = ($( "input:radio[name=lighting]:checked" ).val()) ? $( "input:radio[name=lighting]:checked" ).val() : '';
                if(price_val_array.length) {
                    var price_val = price_val_array[0] + "-" + price_val_array[1]; 
                }
                
                url = '<?php echo Yii::app()->urlManager->createUrl("/listing/index?"); ?>countryid='+countryid+'&stateid='+stateid+'&cityid='+cityid+'&length='+length+'&width='+width+'&reach='+reach+'&sizeunitid='+sizeunitid+'&lightingid='+lightingid+'&mediatypeid='+mediatypeid+'&priceslider='+price_val;
                //console.log(url);
                location.href = url;
            }
        });
        // AJAX city field
        $('#countryid').fancyfields("bind","onSelectChange", function (input,text,val){
            //alert("Current input - ID : "+input.attr("id") + " , selected text : " + text + " , value : " + val);
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->urlManager->createUrl('ajax/prioritystates'); ?>", //url to call.
                cache: true,
                data: {id: val},
                success: function (data) {
                    $('#stateid').fancyfields("enable");
                    $('#stateid').setOptions(eval(data));
                    $('#cityid').setOptions([["Select city", ""]]);
                    /*if($('#mediatypeid').val() >= 1) {
                        $('#errorCountryMediaType').hide();
                    }*/
                    var count_checked = $(".mediatype:checked").length;
                    if (count_checked >= 1){                    
                        $('#errorCountryMediaType').hide();
                    }
                }
            });                
        });
        
        /*$('#mediatypeid').fancyfields("bind","onSelectChange", function (input,text,val){
            //console.log(val);
            if(val >= 1) {
                
            }
        });*/
        $(".mediatype").click(function(){                
            var count_checked = $(".mediatype:checked").length;
            if (count_checked >= 1){                    
                $('#errorCountryMediaType').hide();
            }
        });
        
        // AJAX state field
        $('#stateid').fancyfields("bind","onSelectChange", function (input,text,val){
            //alert("Current input - ID : "+input.attr("id") + " , selected text : " + text + " , value : " + val);
            $.ajax({                
                type: "POST",
                url: "<?php echo Yii::app()->urlManager->createUrl('ajax/prioritycities'); ?>", //url to call.
                cache: true,
                data: {id: val},
                success: function (data) {
                    $('#cityid').fancyfields("enable");                        
                    $('#cityid').setOptions(eval(data));                    
                }
            });                
        });
        
        // normal search
        $('.home-bar .normal a.change').click(function(e) {
            e.preventDefault();
            $('.home-bar .normal ').hide();
            $('.home-bar .advanced ').show();
            $('#div_state').removeClass('clearfix');
            $('#common').wrap('<div class="row">');
            $('#dashboard .navbar-second.navbar-static-top.home-bar').attr('style','height:auto!important');
            $('.navbar-second.navbar-static-top.home-bar .container').css('background', 'none');
            $('.home-bar select, #dashboard select, #dashboard input[type="radio"]').fancyfields("reset");
            $('#stateid').fancyfields("disable");
            $('#cityid').fancyfields("disable");
        });
        
        // advance search
        $('.home-bar .advanced a.change').click(function(e) {
            e.preventDefault();
            $('.home-bar .advanced ').hide();   
            $('.home-bar .normal ').show();
            $('#dashboard .navbar-second.navbar-static-top.home-bar').attr('style','height:208px!important');
            $('.navbar-second.navbar-static-top.home-bar .container').not('#dashboard .navbar-second.navbar-static-top.home-bar .container').css('background', 'url(./images/site/home_bg.png) no-repeat bottom right');
            $('.home-bar select, #dashboard select, #dashboard input[type="radio"]').fancyfields("reset");  
            $('#stateid').fancyfields("disable");
            $('#cityid').fancyfields("disable");
        });
    });
</script>
