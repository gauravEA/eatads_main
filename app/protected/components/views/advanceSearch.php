<?php // Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/geocomplete.css');  ?>
<script src="<?php echo Yii::app()->params['protocol']; ?>maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.geocomplete.js'); ?>

<!-- <div class="left"> -->
<div class="left normal">
    <?php if ($header) { ?>
        <h1>Where do you want to advertise?</h1>            
    <?php } ?>
    <!-- Common fields -->  
    <?php /*
      <div id="common">
      <div class="col-sm-6 clearfix">
      <?php echo CHtml::dropDownList('countryid', 'id', Area::model()->getPriorityCountryOptions(), array('empty' => 'Select country', 'tabindex' => 1)); ?>
      </div>
      <div class="col-sm-6 clearfix" id="div_state">
      <?php echo CHtml::dropDownList('stateid', '', array(), array('empty' => 'Select state')); ?>
      </div>
      <div class="col-sm-6 clearfix">
      <?php echo CHtml::dropDownList('cityid', '', array(), array('empty' => 'Select city')); ?>
      </div>
      </div> */ ?>
    <!-- <div class="col-sm-6 clearfix"> -->
    <div class="col-sm-6 clearfix mtype">    
        <?php //echo CHtml::dropDownList('mediatypeid', 'id', CHtml::listData(MediaType::model()->findAll(), 'id', 'name'), array('empty' => 'Select media type')); ?>
        <?php //echo CHtml::checkBoxList('mediatypeid', 'id', CHtml::listData(MediaType::model()->findAll(), 'id', 'name'), array('class'=>'mediatype')); ?>
        <?php
        echo CHtml::dropDownList('mediatypeid', '', MediaType::getPriorityMediaType(), array('multiple' => 'multiple', 'data-title' => 'Media Type', 'class' => 'mediatype'));
        ?>

    </div>
    <!-- <div class="col-sm-6 clearfix"> -->
    <div class="col-sm-6 clearfix geo">
        <?php
        echo CHtml::textField('geocomplete', '', array('placeholder' => 'Enter a location', 'class' => 'form-control geoac'));
        echo CHtml::hiddenField('lat', '', array('class' => 'form-control geoac'));
        echo CHtml::hiddenField('lng', '', array('class' => 'form-control geoac'));
        echo CHtml::hiddenField('vp', '', array('class' => 'form-control geoac'));
        echo CHtml::hiddenField('geocount', '', array('class' => 'form-control geoac'));
        echo CHtml::hiddenField('proximity', '', array('class' => 'form-control geoac'));
        ?>
        <img src="<?php echo Yii::app()->getBaseUrl(); ?>/images/site/ajax-loader.gif" style="display:none;" id="ajax_loader"/>        
    </div>
    <div class="col-sm-6 clearfix errormessage" id="errorCountryMediaType" style="display:none;">Please select media type and locality to search.</div>
    <!-- Normal search -->
    <?php /*
      <div class="left normal">
      <div class="clear clearfix"></div>
      <div class="col-sm-6 clearfix">
      <?php echo CHtml::button('Search', array('class' => 'btn btn-lg btn-success big btn_search', 'name' => 'btn_search')); ?>
      <!--<a href="#" class="change">Advanced Search</a>-->
      </div>
      <div class="clear clearfix"></div>
      </div> */
    ?>
    <div class="col-sm-6 clearfix submit">
<?php echo CHtml::button('Search', array('class' => 'btn btn-lg btn-success', 'id' => 'btn_search_id', 'name' => 'btn_search')); ?> 
    </div>

    <?php /*
      <!-- Advanced search -->
      <div class="left advanced">
      <div class="hr"></div>
      <div class="row">
      <div class="col-sm-6 clearfix">
      <p class="range">Length:</p><input type="text" id="length" name="length" tabindex="1" class="form-control" />
      </div>
      <div class="col-sm-6 ">
      <p class="range">Width:</p><input type="text" id="width" name="width" tabindex="1" class="form-control" />
      </div>
      <div class="col-sm-6 clearfix">
      <p class="range">Size Unit:</p>
      <?php foreach (Listing::getSizeUnit() as $key => $sizeUnit): ?>
      <input type="radio" name="sizeunit" value="<?php echo $key; ?>" /><?php echo $sizeUnit; ?>&nbsp;
      <?php endforeach; ?>
      </div>
      </div>
      <div class="hr"></div>
      <div class="col-sm-12 clearfix" id="adv_search_slider">
      <p class="range">Price range:</p><input type="text" id="slider-range" />
      </div>
      <div class="hr"></div>
      <div class="col-sm-12 clearfix">
      <div class="col-sm-6 clearfix">
      <p class="range">Reach:</p><input type="text" id="reach" name="reach" tabindex="1" class="form-control" />
      </div>
      <div class=" clearfix">
      <p class="range">Lighting type:</p>
      <?php foreach (Listing::getLighting() as $key => $lightingType): ?>
      <input type="radio" name="lighting" value="<?php echo $key; ?>" /><?php echo $lightingType; ?>&nbsp;
      <?php endforeach; ?>
      </div>
      </div>
      <div class="hr"></div>
      <div class="col-sm-6 clearfix">
      <?php echo CHtml::button('Search', array('class' => 'btn btn-lg btn-success big btn_search', 'name' => 'btn_search')); ?>
      <a class="change">Back to Basic Search</a>
      </div>
      </div>
     */ ?>
    <div class="clear clearfix"></div>
</div>
<?php
/* $minMaxListPriceUsd = Yii::app()->cache->get('min_max_listprice');
  if(!$minMaxListPriceUsd) {
  $cmObj = new CachedMethods();
  $minMaxListPriceUsd = $cmObj->getMinMaxListPrice();
  }

  $minMaxListPriceNew = array(
  'min' => round(Yii::app()->openexchanger->convertCurrency($minMaxListPriceUsd['min'], 'USD', $ipCurrencyCode)),
  'max' => round(Yii::app()->openexchanger->convertCurrency($minMaxListPriceUsd['max'], 'USD', $ipCurrencyCode))
  ); */
?>

<script type="text/javascript">
    function calc_proximity()
    {
        // r = radius of the earth in statute miles
        var r = 3963.0;
        // Convert lat or lng from decimal degrees into radians (divide by 57.2958)
        var lat1 = parseFloat($('#lat').val()) / 57.2958;
        var lon1 = parseFloat($('#lng').val()) / 57.2958;
        var ne = $('#vp').val();
        ne = ne.split(",");
        var lat2 = ne[0] / 57.2958;
        var lon2 = ne[1] / 57.2958;
        // distance = circle radius from center to Northeast corner of bounds
        proximity = r * Math.acos(Math.sin(lat1) * Math.sin(lat2) +
                Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1));
        proximity = Math.round(proximity * 1000) / 1000;
        $('#proximity').val(proximity);
        //console.log('init proximity - ' + proximity); 
    }
    function check_listing_exists()
    {
        $('#ajax_loader').show();
        var lat = $('#lat').val();
        var lng = $('#lng').val();
        var proximity = $('#proximity').val();
        var mediatypeid = '';
        $(':checkbox:checked').each(function(i) {
            mediatypeid += $(this).val() + ',';
        });
        mediatypeid = mediatypeid.replace(/^,|,$/g, '');
        $.ajax({
            type: "GET",
            url: "<?php echo Yii::app()->urlManager->createUrl('ajax/getlistingcountlatlng'); ?>",
            async: false,
            data: {
                lat: lat,
                lng: lng,
                mediatypeid: mediatypeid,
                proximity: proximity
            },
            beforeSend: function() {
                if (mediatypeid == "") {
                    $('#errorCountryMediaType').html("Please select Media type.");
                    $('#errorCountryMediaType').show();
                    $('#ajax_loader').hide();
                    return false;
                }
            },
            success: function(data) {
                $('#ajax_loader').hide();
                $('#geocount').val(data);
                if (data == 0) {
                    $('#errorCountryMediaType').html("No result found.");
                    $('#errorCountryMediaType').show();
                } else {
                    $('#errorCountryMediaType').hide();
                }
                /*var mediatypeid = '';
                 $(':checkbox:checked').each(function(i){
                 mediatypeid += $(this).val() +',';
                 });
                 if(mediatypeid!='') {
                 $('#errorCountryMediaType').hide();
                 } */
            }
        });
    }
    $(function() {        
        $("#geocomplete").geocomplete()
                .bind("geocode:result", function(event, result) {
                    $('#geomsg').html("Result: " + result.formatted_address);
                    //console.log(result.geometry.location.k);
                    //console.log(result.geometry.location.A);                
                    $('#lat').val(parseFloat(result.geometry.location.lat()).toFixed(6));
                    $('#lng').val(parseFloat(result.geometry.location.lng()).toFixed(6));
                    var vp = result.geometry.viewport.getNorthEast();
                    vp = parseFloat(vp.lat()).toFixed(6) + "," + parseFloat(vp.lng()).toFixed(6);
                    $('#vp').val(vp);
                    calc_proximity();
                    check_listing_exists();
                })
                .bind("geocode:error", function(event, status) {
                    $('#geomsg').html("ERROR: " + status);
                })
                .bind("geocode:multiple", function(event, results) {
                    $('#geomsg').html("Multiple: " + results.length + " results found");
                });

        $('#mediatypeid').multiselect({
            includeSelectAllOption: true,
            enableFiltering: false,
            maxHeight: 250,
            onChange: function(element, checked) {
                //var count_checked = $(".mediatype:checked").length;
                var count_checked = $('#mediatypeid option:selected').length;
                $('.geoac').val('');
                if (count_checked >= 1) {
                    $('#errorCountryMediaType').hide();
                }
            }
        });

        $('#btn_search_id').click(function() {
            //$('#geocomplete').val('');
            //var mediatypeid = $('#mediatypeid').val();
            var mediatypeid = '';
            $(':checkbox:checked').each(function(i) {
                if ($(this).val() !== 'multiselect-all') {
                    mediatypeid += $(this).val() + ',';
                }
            });
            mediatypeid = mediatypeid.replace(/^,|,$/g, '');
            var geocount = $('#geocount').val();
            //console.log('check - ' + geocount + ' ' + mediatypeid);
            if (geocount == '' || geocount == '0' || mediatypeid == '') {
                var query = $('#geocomplete').val();
                if (geocount == '0' && mediatypeid != '') {
                    $('#errorCountryMediaType').html("No result found.");
                    $('#errorCountryMediaType').show();
                } else {
                    if (query.length && mediatypeid != '') {
                        $('#errorCountryMediaType').html("No result found.");
                        $('#errorCountryMediaType').show();
                    } else {
                        $('#errorCountryMediaType').html("Please select media type and locality to search.");
                        $('#errorCountryMediaType').show();
                    }
                }
            } else {
                //console.log(mediatypeid);                
                var geoloc = $('#lat').val() + ',' + $('#lng').val();
                var vp = $('#vp').val();
                var proximity = $('#proximity').val();
                var mt = $('#geocomplete').val();
                mt = mt.split(",");

                // console.log(geoloc);
                // console.log(result.geometry.viewport);
                // url = '<?php // echo Yii::app()->urlManager->createUrl("/map/index?");  ?>mediatypeid='+mediatypeid+'&geoloc='+geoloc+'&proximity='+proximity+'&mt='+mt[0];                
                url = '<?php echo Yii::app()->urlManager->createUrl("/s/mapview?"); ?>mediatypeid=' + mediatypeid + '&geoloc=' + geoloc + '&proximity=' + proximity + '&mt=' + mt[0];
                location.href = url;
            }
        });
        /*$('#locality').autocomplete({
         source: "<?php echo Yii::app()->urlManager->createUrl('ajax/localitysearch'); ?>",
         minLength: 2,
         messages: {
         noResults: '',
         results: function() {}
         },
         success: function(data) {
         response($.map(data, function(item) {
         return {label: __highlight(item.locality, request.term) + " (" + item.locality + ")",
         value: item.locality    };
         
         })); 
         },
         select: function( event, ui ) {
         console.log( ui.item ?
         "Selected: " + ui.item.value + " aka " + ui.item.id :
         "Nothing selected, input was " + this.value );
         
         $("#locality").val( ui.item.locality);
         $("#mapcenter").val( ui.item.geoloc);
         return false;
         }
         }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
         return $( "<li>" )
         .append( "<a>" + item.locality + "</a>" )
         .appendTo( ul );
         };*/


        // price slider
        /*$("#slider-range").ionRangeSlider({
         min: <?php //echo $minMaxListPriceNew['min'];  ?>,
         max: <?php //echo $minMaxListPriceNew['max'];  ?>,
         from: <?php //echo $minMaxListPriceNew['min'];  ?>,
         to: <?php //echo $minMaxListPriceNew['max'];  ?>,
         type: "double",
         step: 1,
         prefix:  $('#selected_currency_sym').html() + " ",
         prettify: true,
         hasGrid: false
         });*/

        /*$('#stateid').fancyfields("disable");
         $('#cityid').fancyfields("disable");*/

        /*$('.btn_search').click(function(){
         var countryid = $('#countryid').val();
         //var mediatypeid = $('#mediatypeid').val();
         var mediatypeid = '';
         $(':checkbox:checked').each(function(i){
         mediatypeid += $(this).val() +',';
         });
         mediatypeid = mediatypeid.replace(/^,|,$/g,'');            
         if(countryid=='' || mediatypeid=='') {
         $('#errorCountryMediaType').show();
         } else {                
         var stateid = ($('#stateid').val()>0) ? $('#stateid').val() : '';
         var cityid = ($('#cityid').val()>0) ? $('#cityid').val() : '';
         
         var price_val_array = $("#slider-range").val().split(";");
         var length = $("#length").val();
         var width = $("#width").val();
         var reach = $("#reach").val();
         var sizeunitid = ($( "input:radio[name=sizeunit]:checked" ).val()) ? $( "input:radio[name=sizeunit]:checked" ).val() : '';
         var lightingid = ($( "input:radio[name=lighting]:checked" ).val()) ? $( "input:radio[name=lighting]:checked" ).val() : '';
         if(price_val_array.length) {
         var price_val = price_val_array[0] + "-" + price_val_array[1]; 
         }
         
         url = '<?php echo Yii::app()->urlManager->createUrl("/listing/index?"); ?>countryid='+countryid+'&stateid='+stateid+'&cityid='+cityid+'&length='+length+'&width='+width+'&reach='+reach+'&sizeunitid='+sizeunitid+'&lightingid='+lightingid+'&mediatypeid='+mediatypeid+'&priceslider='+price_val;
         //console.log(url);
         location.href = url;
         }
         });*/
        // AJAX city field
        /*$('#countryid').fancyfields("bind","onSelectChange", function (input,text,val){
         //alert("Current input - ID : "+input.attr("id") + " , selected text : " + text + " , value : " + val);
         $.ajax({
         type: "POST",
         url: "<?php //echo Yii::app()->urlManager->createUrl('ajax/prioritystates');  ?>", //url to call.
         cache: true,
         data: {id: val},
         success: function (data) {
         $('#stateid').fancyfields("enable");
         $('#stateid').setOptions(eval(data));
         $('#cityid').setOptions([["Select city", ""]]);
         /*if($('#mediatypeid').val() >= 1) {
         $('#errorCountryMediaType').hide();
         }*/
        /*var count_checked = $(".mediatype:checked").length;
         if (count_checked >= 1){                    
         $('#errorCountryMediaType').hide();
         }
         }
         });                
         });*/

        /*$('#mediatypeid').fancyfields("bind","onSelectChange", function (input,text,val){
         //console.log(val);
         if(val >= 1) {
         
         }
         });*/


        // AJAX state field
        /*$('#stateid').fancyfields("bind","onSelectChange", function (input,text,val){
         //alert("Current input - ID : "+input.attr("id") + " , selected text : " + text + " , value : " + val);
         $.ajax({                
         type: "POST",
         url: "<?php //echo Yii::app()->urlManager->createUrl('ajax/prioritycities');  ?>", //url to call.
         cache: true,
         data: {id: val},
         success: function (data) {
         $('#cityid').fancyfields("enable");                        
         $('#cityid').setOptions(eval(data));                    
         }
         });                
         });*/

        // normal search
        /*$('.home-bar .normal a.change').click(function(e) {
         e.preventDefault();
         $('.home-bar .normal ').hide();
         $('.home-bar .advanced ').show();
         $('#div_state').removeClass('clearfix');
         $('#common').wrap('<div class="row">');
         $('#dashboard .navbar-second.navbar-static-top.home-bar').attr('style','height:auto!important');
         $('.navbar-second.navbar-static-top.home-bar .container').css('background', 'none');
         $('.home-bar select, #dashboard select, #dashboard input[type="radio"]').fancyfields("reset");
         $('#stateid').fancyfields("disable");
         $('#cityid').fancyfields("disable");
         });
         
         // advance search
         $('.home-bar .advanced a.change').click(function(e) {
         e.preventDefault();
         $('.home-bar .advanced ').hide();   
         $('.home-bar .normal ').show();
         $('#dashboard .navbar-second.navbar-static-top.home-bar').attr('style','height:208px!important');
         $('.navbar-second.navbar-static-top.home-bar .container').not('#dashboard .navbar-second.navbar-static-top.home-bar .container').css('background', 'url(./images/site/home_bg.png) no-repeat bottom right');
         $('.home-bar select, #dashboard select, #dashboard input[type="radio"]').fancyfields("reset");  
         $('#stateid').fancyfields("disable");
         $('#cityid').fancyfields("disable");
         });*/
    });
</script>

<div class="hr"></div>
<!-- POST YOUR REQUIREMENTS -->
<h3 class="postText">Alternatively, Submit your requirements and we'll get back to you</h3>
<?php echo CHtml::button('Post your requirements', array('class' => 'btn btn-success btn-cta', 'id' => 'btn_requirements_id', 'name' => 'btn_requirements')); ?>
<!-- POST YOUR REQUIREMENTS -->