<?php

// ref - http://docs.aws.amazon.com/aws-sdk-php/guide/latest/service-s3.html
require_once  'vendor/autoload.php';

use Aws\Sqs\SqsClient;

class EatadSqs {
public $accessKey;		// AWS Access key	
	public $secretKey;		// AWS Secret key
        public $region;
        public $queue;
	public $lastError = "";
        public $client;
        public $_errors;
        public $_lastRequestId;
        /**
	 * Instance the SQS object
	 */
	public function __construct($queue=null) {
            $this->client = SqsClient::factory(array('key' => Yii::app()->params['awssqs']['accessKey'], 'secret' => Yii::app()->params['awssqs']['secretKey']
                    , 'region'  => 'ap-southeast-1'));
            
            if ($this->queue == null) {
                $this->queue = Yii::app()->params['awssqs']['queue'];
            } else {
                $this->queue = $queue;
            }
            
	}
        
        
        public function addMessagesToQueue($messageArray) {
            
           $result = $this->client->sendMessageBatch(array('QueueUrl' => $this->queue,'Entries' => array($messageArray)));
        }
        
        
        
        public function generateQueueMessage($messageArray) {
            $r='true';
        foreach(array_chunk($messageArray,10) as $batch)
        {
            $messages=array();
            foreach($batch as $i=>$message)
            {
                $messages[]=array(
                    'Id'          => $i,
                    'MessageBody' => json_encode($message),
                );
            }
            $r =$this->client->sendMessageBatch(array('QueueUrl' => $this->queue,'Entries' => $messages));
        }
        return $r;

        }

        
        /*
         * function to call messages of a queue 
         */
        
        /*
         * function to remove message from the queue
         */
        
        
        

    /**
     * Parse response to get the last request id, check for errrors
     *
     * @param CFResponse response object to parse
     * @return array
     */
    private function parseResponse($response)
    {
        $this->_errors=array();
        $this->_lastRequestId = (string)$response->body->ResponseMetadata->RequestId;

        if($response->isOK()) {
            return $response;
        } else {
            $this->_errors = array(
                'id'      => (string)$response->body->RequestId,
                'code'    => (string)$response->body->Error->Code,
                'message' => (string)$response->body->Error->Message,
            );
            Yii::log(implode(' - ', array_values($this->_errors)),'error','ext.sqs');
        }
        return false;
    }
        
        
}
?>