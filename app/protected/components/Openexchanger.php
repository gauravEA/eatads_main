<?php 
 
class Openexchanger extends CApplicationComponent
{
    public $currency = null;
    public $appId;
    public $cache;
 
    /*public function getExchangeRates()
    {
        if($this->cache) {
            if(isset($_SESSION['rates'])) { // Return cached data 
                $this->currency = $_SESSION['rates'];
            } else { // Need cached data but no data in cache
                $this->currency = $_SESSION['rates'] =  $this->getOpenExchnagerRate();
            }
        } else { // Fetch fresh data save them and return
            $this->currency = $_SESSION['rates'] = $this->getOpenExchnagerRate();
        }
    }*/
    
    // if system cache is enabled
//    if(extension_loaded('memcache')) { 
//        if(!Yii::app()->cache->get('rates')) {
//            // if value is not cached
//            // regenerate $value because it is not found in cache
//            // and save it in cache for later use:
//            Yii::app()->cache->set('rates', $this->getOpenExchnagerRate(), 24*60*60);      // set rates for 24 hours
//            $this->currency = Yii::app()->cache->get('rates');
//
//        } else {
//            // if value is cached
//            $this->currency = Yii::app()->cache->get('rates');
//        }     
//    } else {
//        // if system cache is not enabled
//            $this->currency = $this->getOpenExchnagerRate();
//    }
//    return $this->currency;
    
    
    public function getExchangeRates()
    {
        $this->currency = $this->getOpenExchnagerRate();
        //Yii::app()->cache->set('rates', $this->currency, 24*60*60);      // set rates for 24 hours
    }    
    public function getOpenExchnagerRate() {
        
        $ratesArray = Yii::app()->cache->get('rates'); 
        $protocol = Yii::app()->params['protocol'];
        //print_r(count((array)$ratesArray));
        if(count((array)$ratesArray) <=1) {
            // not there
            // Requested file
            // Could also be e.g. 'currencies.json' or 'historical/2011-01-01.json'
            $file = 'latest.json';

            // Open CURL session:
            $ch = curl_init($protocol."openexchangerates.org/api/{$file}?app_id={$this->appId}");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            // Get the data:
            $json = curl_exec($ch);
            curl_close($ch);        
            $result = null;        
            if($json && $json!='null') {                
                // rates available
                // update db            
                $decodedJson = json_decode($json);                
                CachedCurrRate::updateRates($decodedJson->rates);
                $result = $decodedJson;
            } else {
                // rates unavailable
                // get it from db
                $result = CachedCurrRate::getRates();
            }

            // Decode JSON response:
            Yii::app()->cache->set('rates', $result, 24*60*60);      // set rates for 24 hours            
            return $result;
            //return $exchangeRates = $result;                        
        } else {
            return $ratesArray;
        }
        
    }
    
    public function getId()
    {
        //return $this->model->id;
    }
 
    public function getName()
    {
        //return $this->model->name;
    }
    
    public function convertCurrency($value, $nativeCurr, $newCurr) {        
        //echo '<pre>';
        //print_r(Yii::app()->openexchanger->currency->rates);
        // Currency From, rate
        $nativeCurrRate = isset(Yii::app()->openexchanger->currency->rates->{$nativeCurr}) ? Yii::app()->openexchanger->currency->rates->{$nativeCurr} : 1;
        //echo $nativeCurrRate . '<br />';
        // Currency To, rate
        $newCurrRate = isset(Yii::app()->openexchanger->currency->rates->{$newCurr}) ? Yii::app()->openexchanger->currency->rates->{$newCurr} : 1;        
        //echo $newCurrRate . '<br />';
        // Converted value
        $newValue = ((1 / $nativeCurrRate ) * $newCurrRate ) * $value;
        //echo $newValue . '<br />';        
        // Return rounded value to 2 digits        
        return $newValue;// ? round($newValue, 2) : 1;
    }
}