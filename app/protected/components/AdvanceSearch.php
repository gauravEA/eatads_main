<?php

class AdvanceSearch extends CWidget {

    public $renderBackground;
    public $header = true;
    public $ipCurrencyCode;
    
    public function init() {        
        
    }

    public function run() {
        Yii::app()->openexchanger->exchangeRates;  
        $this->render("advanceSearch", array('background'=>$this->renderBackground, 'header' => $this->header, 'ipCurrencyCode'=>$this->ipCurrencyCode));
    }

}

?>