<?php

class GoogleMap extends CWidget {

    public $latitude_fieldname;
    public $longitude_fieldname;
    public $latitude;
    public $longitude;
    public $callback_function_name;
    public $onlyMap;
    public $showMarker;
    public $mapFieldData;    
    public function init() {
        
    }

    public function run() {
        
        if($this->onlyMap) {            
            $this->render("onlyGoogleMap", array('mapFieldData' => $this->mapFieldData));
        } else {
            $this->render("googleMap", array(
                                    'latitude_fieldname'=>$this->latitude_fieldname, 
                                    'longitude_fieldname'=>$this->longitude_fieldname,
                                    'callback_function_name'=>$this->callback_function_name
                                ));
        }
        
    }

}

?>