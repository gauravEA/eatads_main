<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>
<?php
/* Input dialog with Javascript callback */
/*$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'mydialog2',
    'options' => array(
        'title' => 'Choose Account Type',
        'width'=>400,
        'height'=>150,
        'autoOpen' => !isset($userAccountTypeModel) ? true: false,
        'modal' => true,
        'buttons' => array(
            'Media Buyer' => 'js:function(){$(this).dialog("close"); $("#span_account_type").text("Media Buyer"); $("#SignupForm_hiddenAccountType").val("2"); $(".hint").show();}',
            'Media Owner' => 'js:function(){$(this).dialog("close"); $("#span_account_type").text("Media Owner"); $("#SignupForm_hiddenAccountType").val("3"); $(".hint").show();}',
            '3rd Party' => 'js:function(){$(this).dialog("close"); $("#span_account_type").text("3rd Party"); $("#SignupForm_hiddenAccountType").val("4"); $(".hint").show();}',
        ),
    ),
));
echo '<span style="color:#f00">'.$accountTypeErrorMsg.'</span>';
$this->endWidget('zii.widgets.jui.CJuiDialog'); */
?>
<!-- Navbar second -->
<div class="navbar navbar-second navbar-static-top ">
    <div class="container">
        <div class="left">
            <h1>Sign Up - <span id="selUserType"></span></h1>
        </div>
        <div class="right">
            <ul> 
                <li class="done"><span>1</span><br/>Account type</li>
                <li class="second active"><span>2</span><br/>Basic data</li>
                <li class="third"><span>3</span><br/>Company data</li>
            </ul>
        </div>
    </div>
</div>
<div class="container main-body">
    <h2>Let’s get you signed up</h2>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'user-signup-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // See class documentation of CActiveForm for details on this,
        // you need to use the performAjaxValidation()-method described there.
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    )); ?>

    <div class="row">
        <div class="col-sm-9">
            <?php //echo $form->errorSummary($model); ?>
            
            <?php /* <div class="col-sm-6 clearfix">
                <label class="top big">Username </label> 
                <?php echo $form->textField($model, 'username', array('class'=>'form-control', 'autocomplete'=>'off')); ?>
                <?php echo $form->error($model, 'username', array('class'=>'errormessage')); ?>
            </div> */ ?>
            <div class="clear clearfix"></div>
            <div class="col-sm-6 clearfix">
                <label class="top">Email address </label>
                <?php echo $form->textField($model, 'email', array('class'=>'form-control', 'autocomplete'=>'off')); ?>
                <?php echo $form->error($model, 'email', array('class'=>'errormessage')); ?>
            </div>
            <div class="col-sm-6">
                <label class="top">Phone number </label> 
                <?php echo $form->textField($model, 'phonenumber', array('class'=>'form-control', 'autocomplete'=>'off')); ?>
                <?php echo $form->error($model, 'phonenumber', array('class'=>'errormessage')); ?>
            </div>
            <div class="col-sm-6 clearfix">
                <label class="top">Password </label> 
                <?php echo $form->passwordField($model, 'password', array('class'=>'form-control', 'autocomplete'=>'off')); ?>
                <?php echo $form->error($model, 'password', array('class'=>'errormessage')); ?>
            </div>
            <div class="col-sm-6">
                <label class="top">Confirm Password</label>                 
                <?php echo $form->passwordField($model, 'confirmPassword', array('class'=>'form-control', 'autocomplete'=>'off')); ?>
                <?php echo $form->error($model, 'confirmPassword', array('class'=>'errormessage')); ?>
            </div>
            <div class="col-sm-6 clearfix">
                <label class="top">First name </label>
                <?php echo $form->textField($model, 'fname', array('class'=>'form-control', 'autocomplete'=>'off')); ?>
                <?php echo $form->error($model, 'fname', array('class'=>'errormessage')); ?>
            </div>
            <div class="col-sm-6">
                <label class="top">Last name </label>
                <?php echo $form->textField($model, 'lname', array('class'=>'form-control', 'autocomplete'=>'off')); ?>
                <?php echo $form->error($model, 'lname', array('class'=>'errormessage')); ?>
            </div>
            <div class="col-sm-12">
                <?php echo $form->checkBox($model, 'subscribe', array('value' => 1, 'checked'=>'checked')); ?>
                <label class="top"> I would like to receive regular updates from EatAds.com </label>
                
                <?php echo $form->error($model, 'subscribe'); ?>
                <?php echo $form->hiddenField($model,'hiddenAccountType', array('value'=>isset($hiddenAccountType)?$hiddenAccountType:'')); ?>
            </div>
            
            <p class="hint" style="display: none;">
                 By clicking next, you agree to EatAds's <a href="<?php echo Yii::app()->urlManager->createUrl('staticPages/termsAndCondition'); ?>" target="_blank">Terms & Conditions</a>
            </p>
            <?php echo CHtml::submitButton('Next', array('class'=>'btn btn-lg btn-success')); ?>
            
        </div>
    </div>
    <script type="text/javascript">        
            var account_type = parseInt($('#User_hiddenAccountType').val());            
            switch(account_type)
            {
                case 2: $("#selUserType").html("Media Buyer"); break;
                case 3: $("#selUserType").html("Media Vendor"); break;
                case 4: $("#selUserType").html("3rd Party"); break;
            }        
    </script>
    <?php $this->endWidget(); ?>
<!-- content container will end in layout -->
<!-- Account Type Modal -->
<!-- Success Email Modal -->
    <div class="modal fade left-aligned" id="RegisterModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <?php /*<span class="close"></span>*/ ?>
                <div class="modal-body">
                    <h4 class="modal-title">Thank you!</h4>
                    <p>Your account has been created and a confirmation link has been sent. Please check your email to complete the sign up process.</p>
                    <?php 
                        $dashboardUrl[0] = '';
                        if(!Yii::app()->user->isGuest){
                            JoyUtilities::redirectUser(Yii::app()->user->id);
                            $dashboardUrl = Yii::app()->user->returnUrl;                            
                        }
                    ?>
                    <button class="btn btn-lg btn-success" onclick="location.href='<?php echo Yii::app()->urlManager->createUrl($dashboardUrl[0]); ?>'">Dashboard</button> <b>OR</b>
                    <button class="btn btn-lg btn-success" onclick="location.href='<?php echo Yii::app()->urlManager->createUrl('account/usercompany'); ?>'">Company Profile</button>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>

    <div class="modal fade" id="AccountSelectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <h4 class="modal-title">What type of account do you need?</h4>

                <div class="modal-body">
                    <div class="span12 pagination-centered">
                        <div class="row">                            
                            <div class="left">
                                <h3>Media Vendor</h3>
                                <img src="<?php echo Yii::app()->baseUrl; ?>/images/site/owner.png" alt="owner" />
<!--                                <p>Do you sell media?
                                    <br/>This is for you.</p>-->
                                <button class="btn btn-lg btn-success" onclick="close_dialog(3);">Choose this</button>
                            </div>
                            <div class="left">
                                <h3>Media Buyer</h3>
                                <img src="<?php echo Yii::app()->baseUrl; ?>/images/site/buyer.png" alt="buyer" />
<!--                                <p>Do you plan or make media buys?
                                    <br/>This is for you.</p>-->
                                <button class="btn btn-lg btn-success" onclick="close_dialog(2);">Choose this</button>
                            </div>
                            <div class="left">
                                <span class="tool"><span class="tooltip">Do you supply services to the OOH industry? Creative agencies, printers, mobile ad technology, LED screens etc. this is for you. </span></span>
                                <h3>3rd Party</h3>
                                <img src="<?php echo Yii::app()->baseUrl; ?>/images/site/3rd.png" alt="third_party" />
<!--                                <p>Do you plan or make media buys?
                                    <br/>This is for you.</p>-->
                                <button class="btn btn-lg btn-success" onclick="close_dialog(4);" disabled="disabled">Coming Soon</button>
                            </div>
                        </div>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
    </div>
</div>
<script>
    $(function(){
       if( $('#User_email').val()!='' ) {
           $('#User_email').attr('disabled', 'disabled');
       }
    });
    </script>
<?php if($successModel) { ?>    
    <script type="text/javascript">
        $(function() {
            $('#RegisterModal').modal({ keyboard: false, backdrop : false });            
        });
    </script>
<?php } 
    if($userAccountTypeModel) { ?>
    
    <script type="text/javascript">
        $(function() {
            $('#AccountSelectModal').modal({ keyboard: false, backdrop : false });        
        });
        function close_dialog(account_type)
        {               
            switch(account_type)
            {
                case 2: $("#selUserType").html("Media Buyer"); break;
                case 3: $("#selUserType").html("Media Vendor"); break;
                case 4: $("#selUserType").html("3rd Party"); break;
            }        
            $("#User_hiddenAccountType").val(account_type); 
            $('#AccountSelectModal').modal('hide');
            $(".hint").show(); 
        }
    </script>
<?php } ?>
