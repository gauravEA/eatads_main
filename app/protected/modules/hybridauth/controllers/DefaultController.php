<?php

class DefaultController extends CController {

	public function actionIndex() {
		$this->render('index');
	}

	/**
	 * Public login action.  It swallows exceptions from Hybrid_Auth. Comment try..catch to bubble exceptions up. 
	 */
	public function actionLogin() {
		try {
			if (!isset(Yii::app()->session['hybridauth-ref'])) {
                // http://localhost/eatads/app/index.php/account/login
				Yii::app()->session['hybridauth-ref'] = Yii::app()->request->urlReferrer;                
			}
			$this->_doLogin();
		} catch (Exception $e) {
			Yii::app()->user->setFlash('hybridauth-error', "Something went wrong, did you cancel?");
			$this->redirect(Yii::app()->session['hybridauth-ref'], true);
		}
	}

	/**
	 * Main method to handle login attempts.  If the user passes authentication with their
	 * chosen provider then it displays a form for them to choose their username and email.
	 * The email address they choose is *not* verified.
	 * 
	 * If they are already logged in then it links the new provider to their account
	 * 
	 * @throws Exception if a provider isn't supplied, or it has non-alpha characters
	 */
	private function _doLogin() {
		if (!isset($_GET['provider']))
			throw new Exception("You haven't supplied a provider");

		if (!ctype_alpha($_GET['provider'])) {
			throw new Exception("Invalid characters in provider string");
		}
		
		$identity = new RemoteUserIdentity($_GET['provider'],$this->module->getHybridauth());
		     
		if ($identity->authenticate()) {
            //echo '1 ha authenticated and user exists but ';
            
			// They have authenticated AND we have a user record associated with that provider
			if (Yii::app()->user->isGuest) {
                // die('1.1 user identity (db) not there so do it'); 
                // check userid (db) and provide user identity login
                
				$this->_loginUser($identity);     // DONE  
                JoyUtilities::redirectUser(Yii::app()->user->id);
				$this->redirect(Yii::app()->user->returnUrl);
			} else {
                //die('1.2 user identity (db) there so redirect to home'); // they wont get here
				//they shouldn't get here because they are already logged in AND have a record for
				// that provider.  Just bounce them on
                    //die('return url');
                    JoyUtilities::redirectUser(Yii::app()->user->id);
				$this->redirect(Yii::app()->user->returnUrl);
			}
		} else if ($identity->errorCode == RemoteUserIdentity::ERROR_USERNAME_INVALID) {            
            //echo '2 ha authenticated but ';
            
			// They have authenticated to their provider but we don't have a matching HaLogin entry
			if (Yii::app()->user->isGuest) {
                //die('2.1 match email or token to user and do user identity login'); SIGN UP USING SOCIAL NOT NOW                
                // got ha login -> need to check ha login return email and associate
                // 2 case matching record found and not found
                // match -> case 2.1.1 find same email and get user id and associate
                // not matching -> case 2.1.2 same email not found -> sign up required
                //              -> case 2.1.3 provider did not return email -> sign up required
                
                // get identity email
                // $remoteEmail = $identity->userData->email;
                $remoteEmail = $identity->getAdapter()->getUserProfile()->email;
                // check if HO return email or not
                //echo '<pre>';var_dump($identity->getAdapter()->getUserProfile()); echo '</pre>'; die();
                
                // match identity email from user table, get user id
                if($remoteEmail!=null) {                    
                    $userRecord = User::model()->findByAttributes(array('email'=>$remoteEmail));                    
                    if($userRecord) {
                        // case 2.1.1
                        // link provide and get user logged in
                        //echo '<pre>';var_dump($userRecord);echo '</pre>';
                        $identity->id = $userRecord->id;
                        $identity->name = ucfirst(strtolower($userRecord->fname));
                        $this->_linkProvider($identity);
						$this->_loginUser($identity);
                        JoyUtilities::redirectUser(Yii::app()->user->id);
                        $this->redirect(Yii::app()->user->returnUrl);
                    } else {
                        // case 2.1.2
                        //echo "no {$remoteEmail} found";
                        //echo '2.1.2 ';//<pre>'; var_dump($identity->loginProvider); echo '</pre>';                         
                    }
                } else {
                    // case 2.1.3
                    // provide didn't return any email, 
                    // make entry halogins table with provider and token
                    // show sign up form, get user to register
                    //echo '2.1.3 ';//<pre>'; var_dump($identity); echo '</pre>'; die();
                }
                
                
 				// They aren't logged in => display a form to choose their username & email 
				// (we might not get it from the provider)
				if ($this->module->withYiiUser == true) {
					Yii::import('application.modules.user.models.*');
				} else {
					Yii::import('application.models.*');
				}
               
                $model = new User;
                $model->setScenario('createProfile');
                $model->setAttribute('email', $remoteEmail);
                $userAccountTypeModel = 1;
                $successModel = 0;
                $accountTypeErrorMsg = '';
                // collect user input data
                if (isset($_POST['User'])) {
                    
                    $model->attributes = $_POST['User']; 
                    if(empty($_POST['User']['hiddenAccountType'])) {
                        // User account type not selected
                        $userAccountTypeModel = 1;
                        $accountTypeErrorMsg = "Please select the account type to complete Sign up.";                
                    } else {
                        $userAccountTypeModel = 0;
                    }
                                        
                    if ($model->validate()) {
                        // create user model to save record
                        
                        $model->attributes = $_POST['User'];
                        // encrypt password
                        // $model->password = md5($model->password);
                        $ph = new PasswordHash(Yii::app()->params['phpass']['iteration_count_log2'], Yii::app()->params['phpass']['portable_hashes']);                
                        $model->password = $ph->HashPassword($model->password);
                        // as discussed on 25-02-0214 3:30 pm IST (Pooja Ghulam)
                        $model->active = 0;
                        $model->datecreated = date("Y-m-d H:i:s");   
                        // var_dump($model); die();
                        // save user registration
                        $model->save(false);
                        // var_dump($model->getErrors()); die;
                        $userId = $model->primaryKey;
                        // Save to User Role table
                        $userRoleModel = new UserRole;
                        $userRoleModel->attributes = array('userid'=>$userId, 'roleid'=> (int)$_POST['User']['hiddenAccountType']);
                        $userRoleModel->save();
                        $linkModel = new Link;
                        // generate the hash
                        $hash = sha1(uniqid());
                        $linkModel->attributes = array('userid'=>$userId, 'hash'=> $hash, 'type'=> 1, 'datecreated'=>date('Y-m-d H:i:s'));
                        // save to Link table
                        // $linkModel->save();
                        if($linkModel->save()) {
                            
                            $identity->id = $userId;
                            $identity->name = ucfirst(strtolower($model->fname));
                            $this->_linkProvider($identity);
                            $this->_loginUser($identity);                            
                            // send reset pwd link email to user                    

                            $to      = $model->email;                            
                            // send reset pwd link email to user                    
                            $resetLink = Yii::app()->createAbsoluteUrl('account/activateuser',array('code'=>$hash));
                            $mail = new EatadsMailer('sign-up', $model->email, array('resetLink'=>$resetLink));
                            $mail->eatadsSend();
                            $mail = new EatadsMailer('sign-up-admin', Yii::app()->params['adminEmail'], array('userType' => JoyUtilities::getUserRole($userId), 
                                                                                                                'fname' => $_POST['User']['fname'], 
                                                                                                                'lname' => $_POST['User']['lname']));
                            $mail->eatadsSend();
                            
                            // show success message
                            $userMessage =  'Please check your email to activate your account.';
                            $successModel = 1;
                            // sign up flow changed, login user 
                            $identity=new LoginForm();
                            $identity->loginWithoutPassword($model->email);

                            /*if(mail($to, $subject, $message, $headers)){ 
                                // show success message
                                $userMessage =  'Please check your email to activate your account.';
                                $successModel = 1;
                                // sign up flow changed, login user 
                                $identity=new LoginForm();
                                $identity->loginWithoutPassword($model->email);  
                            } else {
                                // show error message
                                $userMessage =  'Could not send email to user.';
                            } */                    
                        } else {                    
                            // show error message
                            $userMessage =  'Could not send email to user.';
                        }
                        // redirect to the previous page if valid
                        // flow not clear, need to throw to user company profile page
                        $model->unsetAttributes();
                    }
                }
                // display the login form
                $this->render('signup', array('model' => $model, 'userAccountTypeModel'=>$userAccountTypeModel, 'accountTypeErrorMsg'=>$accountTypeErrorMsg, 
                              'hiddenAccountType'=>isset($_POST['User']['hiddenAccountType'])?$_POST['User']['hiddenAccountType']:'', 'successModel'=>$successModel));
                
                /*
				$user = new User;
				if (isset($_POST['User'])) {
					//Save the form
					$user->attributes = $_POST['User'];

					if ($user->validate() && $user->save()) {
						if ($this->module->withYiiUser == true) {
							$profile = new Profile();
							$profile->first_name='firstname';
							$profile->last_name='lastname';
							$profile->user_id=$user->id;
							$profile->save();
						}
						
						$identity->id = $user->id;
						$identity->username = $user->username;
						$this->_linkProvider($identity);
						$this->_loginUser($identity);
					} // } else { do nothing } => the form will get redisplayed
				} else {
					//Display the form with some entries prefilled if we have the info.
					if (isset($identity->userData->email)) {
						$user->email = $identity->userData->email;
						$email = explode('@', $user->email);
						$user->username = $email[0];
					}
				}

				$this->render('createUser', array(
					'user' => $user,
				));*/
                
			} else {
                //die('2.2 user identity (db) there, check for token and user id match'); NO NEED TO CHANGE
                
				// They are already logged in, link their user account with new provider
				$identity->id = Yii::app()->user->id;
				$this->_linkProvider($identity);
				$this->redirect(Yii::app()->session['hybridauth-ref']);
				unset(Yii::app()->session['hybridauth-ref']);
			}
		}
	}
	
	private function _linkProvider($identity) {
		$haLogin = new HaLogin();
		$haLogin->loginProviderIdentifier = $identity->loginProviderIdentifier;
		$haLogin->loginProvider = $identity->loginProvider;
		$haLogin->userId = $identity->id;
		$haLogin->save();
	}
	
	private function _loginUser($identity) {
        
        // HERE LOGIN SHOULD BE SIMILAR TO USERIDENTITY FILE to get name
        // changed code $identity to have fname now, in RemoteUserIdentity
		Yii::app()->user->login($identity, 0);
        if(!Yii::app()->user->isGuest) {
            $roleId = JoyUtilities::getUserRoleId(Yii::app()->user->id);
            Yii::app()->user->setState('roleId', $roleId);
            
            $userData = User::model()->findByPk(Yii::app()->user->id);
            Yii::app()->user->setState('lname', ucfirst(strtolower($userData->lname)));
            Yii::app()->user->setState('active', $userData->active);
            Yii::app()->user->setState('status', $userData->status);
        }
        //echo '<pre>'; var_dump($identity); echo '</pre>'; 
        //echo 'after - '.Yii::app()->user->name;
        //echo '<pre>'; var_dump($identity); echo '</pre>'; die();        
	}

	/** 
	 * Action for URL that Hybrid_Auth redirects to when coming back from providers.
	 * Calls Hybrid_Auth to process login. 
	 */
	public function actionCallback() {
		require dirname(__FILE__) . '/../Hybrid/Endpoint.php';
		Hybrid_Endpoint::process();
	}
	
	public function actionUnlink() {
		$login = HaLogin::getLogin(Yii::app()->user->getid(),$_POST['hybridauth-unlinkprovider']);
		$login->delete();
		$this->redirect(Yii::app()->getRequest()->urlReferrer);
	}
}