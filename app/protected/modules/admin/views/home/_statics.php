
<div class="col-lg-3 col-sm-2">
    <section class="panel">
        <div class="symbol terques">
            <i class="fa fa-user"></i>
        </div>
        <div class="value">
            <h1 class="count1">
                0
            </h1>
            <p>of listings</p>
        </div>
    </section>
</div>
<div class="col-lg-3 col-sm-2">
    <section class="panel">
        <div class="symbol red">
            <i class="fa fa-tags"></i>
        </div>
        <div class="value">
            <h1 class="count2">
                0
            </h1>
            <p>of buyers</p>
        </div>
    </section>
</div>
<div class="col-lg-3 col-sm-2">
    <section class="panel">
        <div class="symbol yellow">
            <i class="fa fa-shopping-cart"></i>
        </div>
        <div class="value">
            <h1 class="count3">
                0
            </h1>
            <p>of sellers</p>
        </div>
    </section>
</div>
<div class="col-lg-3 col-sm-2">
    <section class="panel">
        <div class="symbol blue">
            <i class="fa fa-bar-chart-o"></i>
        </div>
        <div class="value">
            <h1 class="count4">
                0
            </h1>
            <p>of signups</p>
        </div>
    </section>
</div>
<div class="col-lg-3 col-sm-2">
    <section class="panel">
        <div class="symbol red">
            <i class="fa fa-bar-chart-o"></i>
        </div>
        <div class="value">
            <h1 class="count5">
                0
            </h1>
            <p>of rfps sent</p>
        </div>
    </section>
</div>


<div class="col-lg-3 col-sm-2">
    <section class="panel">
        <div class="symbol terques">
            <i class="fa fa-user"></i>
        </div>
        <div class="value">
            <h1 class="count6">
                0
            </h1>
            <p>of favorited listings</p>
        </div>
    </section>
</div>
<div class="col-lg-3 col-sm-2">
    <section class="panel">
        <div class="symbol red">
            <i class="fa fa-tags"></i>
        </div>
        <div class="value">
            <h1 class="count7">
                0
            </h1>
            <p>of new media plans created</p>
        </div>
    </section>
</div>
<div class="col-lg-3 col-sm-2">
    <section class="panel">
        <div class="symbol yellow">
            <i class="fa fa-shopping-cart"></i>
        </div>
        <div class="value">
            <h1 class="count8">
                0
            </h1>
            <p>of media plans modified</p>
        </div>
    </section>
</div>
<div class="col-lg-3 col-sm-2">
    <section class="panel">
        <div class="symbol blue">
            <i class="fa fa-bar-chart-o"></i>
        </div>
        <div class="value">
            <h1 class="count9">
                0
            </h1>
            <p>of active users (logged in the last <?php echo $days; ?> days)</p>
        </div>
    </section>
</div>
<div class="col-lg-3 col-sm-2">
    <section class="panel">
        <div class="symbol red">
            <i class="fa fa-bar-chart-o"></i>
        </div>
        <div class="value">
            <h1 class="count10">
                0
            </h1>
            <p>of inactive users (not logged in the last <?php echo $days; ?> days)</p>
        </div>
    </section>
</div>

<script>
    function countUp(count, countid)
    {
        var div_by = 100,
                speed = count / div_by,
                $display = $('.count' + countid),
                run_count = 1,
                int_speed = 24;

        var int = setInterval(function() {
            if (run_count < div_by) {
                $display.text(Math.round(speed * run_count));
                run_count++;
            } else if (parseInt($display.text()) < count) {
                var curr_count = parseInt($display.text()) + 1;
                $display.text(curr_count);
            } else {
                clearInterval(int);
            }
        }, int_speed);
    }
    function callCountUp() {
    <?php foreach ($dashboardCount as $key => $val) { ?>
            var myVar = setTimeout(function() {
                countUp(<?php echo $val; ?>, <?php echo $key; ?>);
            }, 100);
    <?php } ?>
    }
    <?php if($ajax == "ajax") { ?>
        callCountUp();
    <?php } else { ?>
        window.onload = callCountUp;
    <?php } ?>
</script>