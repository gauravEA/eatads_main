
                        <header class="panel-heading tab-bg-dark-navy-blue ">
                            <ul class="nav nav-tabs" id="dashboardTabs">
                                <li class="active">
                                    <a data-toggle="tab" href="#listings" onclick="getChart('listings', 1);">Listings</a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#buyers" onclick="getChart('buyers', 1);">Buyers</a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#sellers" onclick="getChart('sellers', 1);">Sellers</a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#Signups" onclick="getChart('Signups', 1);">Signups</a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#rfps" onclick="getChart('rfps', 1);">Rfps sent</a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#Favourited" onclick="getChart('Favourited', 1);">Favorited listings</a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#mediaplans" onclick="getChart('mediaplans', 1);">Media plans created</a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#mediaplansmod" onclick="getChart('mediaplansmod', 1);">Media plans modified</a>
                                </li>  
                                <li class="">
                                    <a data-toggle="tab" href="#active" onclick="getChart('active', 1);">Active users</a>
                                </li>
<!--                                <li class="">
                                    <a data-toggle="tab" href="#inactive" onclick="getChart('inactive', 1);">Inactive users</a>
                                </li>-->

                            </ul>
                        </header>
                        <div class="panel-body">
                            <div class="tab-content">

                                <div id="listings" class="active tab-pane">
                                    <section class="panel">
                                        <header class="panel-heading">
                                            Number of listings
                                        </header>
                                        <div class="panel-body text-center">
                                            <select id="listings_dd" onchange="getChartOnDurationChange();" class="form-control m-bot15">
                                                <option value="1">Weekly</option>
                                                <option value="2">Monthly</option>
                                                <option value="3">Yearly</option>
                                            </select>
                                            <canvas id="listingsChart" class="line" height="450" width="800"></canvas>
                                        </div>
                                    </section>
                                </div>
                                <div id="buyers" class="tab-pane">
                                    <section class="panel">
                                        <header class="panel-heading">
                                            Number of buyers
                                        </header>
                                        <div class="panel-body text-center">
                                            <select id="buyers_dd" onchange="getChartOnDurationChange();" class="form-control m-bot15">
                                                <option value="1">Weekly</option>
                                                <option value="2">Monthly</option>
                                                <option value="3">Yearly</option>

                                            </select>
                                            <canvas class="line" id="buyersChart" height="450" width="800"></canvas>
                                        </div>
                                    </section>
                                </div>
                                <div id="sellers" class="tab-pane">    
                                    <section class="panel">
                                        <header class="panel-heading">
                                            Number of sellers
                                        </header>
                                        <div class="panel-body text-center">
                                            <select id="sellers_dd" onchange="getChartOnDurationChange();"  class="form-control m-bot15">
                                                <option value="1">Weekly</option>
                                                <option value="2">Monthly</option>
                                                <option value="3">Yearly</option>

                                            </select>
                                            <canvas id="sellersChart" class="line" height="450" width="800"></canvas>  </div>
                                    </section>
                                </div>
                                <div id="Signups" class="tab-pane">    
                                    <section class="panel">
                                        <header class="panel-heading">
                                            Number of signups
                                        </header>
                                        <div class="panel-body text-center">
                                            <select id="Signups_dd" onchange="getChartOnDurationChange();" class="form-control m-bot15">
                                                <option value="1">Weekly</option>
                                                <option value="2">Monthly</option>
                                                <option value="3">Yearly</option>

                                            </select>
                                            <canvas id="SignupsChart" class="line" height="450" width="800"></canvas></div>
                                    </section>
                                </div>
                                <div id="rfps" class="tab-pane">  
                                    <section class="panel">
                                        <header class="panel-heading">
                                            Number of rfps sent
                                        </header>
                                        <div class="panel-body text-center">
                                            <select id="rfps_dd" onchange="getChartOnDurationChange();" class="form-control m-bot15">
                                                <option value="1">Weekly</option>
                                                <option value="2">Monthly</option>
                                                <option value="3">Yearly</option>

                                            </select>
                                            <canvas id="rfpsChart" class="line" height="450" width="800"></canvas></div>
                                    </section>
                                </div>
                                <div id="Favourited" class="tab-pane">
                                    <section class="panel">
                                        <header class="panel-heading">
                                            Number of favorited listings
                                        </header>
                                        <div class="panel-body text-center">
                                            <select id="Favourited_dd" onchange="getChartOnDurationChange();" class="form-control m-bot15">
                                                <option value="1">Weekly</option>
                                                <option value="2">Monthly</option>
                                                <option value="3">Yearly</option>

                                            </select>
                                            <canvas id="FavouritedChart" class="line" height="450" width="800"></canvas></div>
                                    </section>
                                </div>
                                <div id="mediaplans" class="tab-pane">    
                                    <section class="panel">
                                        <header class="panel-heading">
                                            Number of new media plans created
                                        </header>
                                        <div class="panel-body text-center"> 
                                            <select id="mediaplans_dd" onchange="getChartOnDurationChange();" class="form-control m-bot15">
                                                <option value="1">Weekly</option>
                                                <option value="2">Monthly</option>
                                                <option value="3">Yearly</option>
                                            </select>
                                            <canvas id="mediaplansChart" class="line" height="450" width="800"></canvas></div>
                                    </section>
                                </div>
                                <div id="mediaplansmod" class="tab-pane"> 
                                    <section class="panel">
                                        <header class="panel-heading">
                                            Number of media plans modified
                                        </header>
                                        <div class="panel-body text-center">   
                                            <select id="mediaplansmod_dd" onchange="getChartOnDurationChange();" class="form-control m-bot15">
                                                <option value="1">Weekly</option>
                                                <option value="2">Monthly</option>
                                                <option value="3">Yearly</option>

                                            </select>
                                            <canvas id="mediaplansmodChart" class="line" height="450" width="800"></canvas></div>
                                    </section>
                                </div>
                                <div id="active" class="tab-pane">
                                    <section class="panel">
                                        <header class="panel-heading">
                                            Number of active users
                                        </header>
                                        <div class="panel-body text-center">   
                                            <select id="active_dd" onchange="getChartOnDurationChange();" class="form-control m-bot15">
                                                <option value="1">Weekly</option>
                                                <option value="2">Monthly</option>
                                                <option value="3">Yearly</option>

                                            </select><canvas id="activeChart" class="line" height="450" width="800"></canvas></div>
                                    </section>
                                </div>
<!--                                <div id="inactive" class="tab-pane"> <section class="panel">
                                        <header class="panel-heading">
                                            of inactive users (not logged in the last 30 days)
                                        </header>
                                        <div class="panel-body text-center">       
                                            <select id="inactive_dd" onchange="getChartOnDurationChange();" class="form-control m-bot15">
                                                <option value="1">Weekly</option>
                                                <option value="2">Monthly</option>
                                                <option value="3">Yearly</option>
                                            </select>
                                            <canvas id="inactiveChart" class="line" height="450" width="800"></canvas></div>
                                    </section>
                                </div>-->

                            </div>
                        </div>


<?php

Yii::app()->clientScript->registerScript('chart1',
        
    '
    var lineChartData = {
        labels : '. $labelArr .',
        datasets : [
            {
                fillColor : "rgba(220,220,220,0.5)",
                strokeColor : "rgba(220,220,220,1)",
                pointColor : "rgba(220,220,220,1)",
                pointStrokeColor : "#fff",
              data : '. $dataPoint .'
            }
        ]

    };
    
    var l=new Array();
    var id="listingsChart";
    var max = '. $chartMax .';        

    if(max == 0){
        var setps = 0;
    } else {
        var steps = 10;
    }

    new Chart(document.getElementById(id).getContext("2d")).Line(lineChartData, {        
        scaleOverride : true,
        scaleSteps : steps,
        scaleStepWidth : max/steps,
        scaleStartValue : 0
    });

'
    , CClientScript::POS_END ); 
?>