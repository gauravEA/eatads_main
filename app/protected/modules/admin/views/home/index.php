            <section class="wrapper">     
                    <div class="row state-overview"><div class="form-group">
                            
                            <div class="col-md-4"> 
                                <div id="dateError" style="display:none; color:red;"></div>
                                <span class="help-block">Filter by date:</span>
                                <div class="input-group input-large" data-date="13/07/2013" data-date-format="mm/dd/yyyy">
                                    <input type="text" class="form-control dpd1" name="from" id="startDate">
                                    <span class="input-group-addon">To</span>
                                    <input type="text" class="form-control dpd2" name="to" id="endDate">
                                </div>
                                <br>
                                <input type="button" id="staticsFilterBtn" value="Filter"><span id="loadingFilter"></span>
                                <span class="help-block">&nbsp;</span>
                            </div>


                            <div class="col-md-4">




<!--                                <div class="input-group m-bot15">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown">Filter by Country <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Select all</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>


                                        </ul>
                                    </div>

                                </div>-->


                            </div>
                        </div>
                    </div>
                    <div id="dashboardBlock">
                        <div id="statics" class="row state-overview">
                            <?php $this->renderPartial('_statics', array('days' => $days, 'dashboardCount' => $dashboardCount, 'ajax' => ''));?>
                        </div>
                        <!--state overview end-->
                        <section id="staticsChart" class="panel">
                            <?php $this->renderPartial('_chart', array('labelArr' => $labelArr, 'dataPoint' => $dataPoint, 'chartMax' => $chartMax)); ?>
                        </section>
                    </div>
                </section>
<?php
    Yii::app()->clientScript->registerScript('filterbtn',
        '$("#staticsFilterBtn").click(function (){
            var startDate = $("#startDate").val();
            var endDate = $("#endDate").val();
            
            var validateStartDate = new Date(startDate);
            var validateEndDate = new Date(endDate);

            if (validateStartDate > validateEndDate){
                $("#dateError").html("From date should be less than To date.").show();
            } else if(!startDate && !endDate) {
                $("#dateError").html("Please select From and To date.").show();
            } else if(!startDate) {
                $("#dateError").html("Please select From date.").show();
            } else if(!endDate) {
                $("#dateError").html("Please select To date.").show();
            } else {
                $("#dateError").html("").hide();
                if(startDate && endDate) {
                    $.ajax({
                        type: "POST",
                        url:  "'.Yii::app()->urlManager->createUrl('admin/home/Updatestatics').'",
                        data: { startDate: startDate, endDate: endDate},
                        beforeSend: function() {
                            $("#loadingFilter").html("<img src=\''. Yii::app()->theme->getBaseUrl().'/img/input-spinner.gif' .'\' alt=\'spinner\' >");
                        }
                    }) .done(function( msg ) {
                        $("#statics").html(msg);
                        var str = $("#dashboardTabs").find("li.active a").attr("href");
                        str = str.substring(1, str.length);
                        var duration = $("#"+str+"_dd option:selected").val();
                        getChart(str, duration);
                        $("#loadingFilter").html("");
                    });
                }
            }
        });'
    , CClientScript::POS_END ); 
    
    Yii::app()->clientScript->registerScript('getChart',
        'function getChart(chartfor, duration) {
            var startDate = $("#startDate").val();
            var endDate = $("#endDate").val();
            
            $("#"+chartfor+"_dd").val(duration);
            //if(startDate && endDate) {
                $.ajax({
                    type: "POST",
                    url:  "'.Yii::app()->urlManager->createUrl('admin/home/getchart').'",
                    data: { startDate: startDate, endDate: endDate, chartfor: chartfor, duration: duration},
                    beforeSend: function() {
                        $("#loadingFilter").html("<img src=\''. Yii::app()->theme->getBaseUrl().'/img/input-spinner.gif' .'\' alt=\'spinner\' >");
                    }
                }) .done(function( msg ) {
                    msg = JSON.parse(msg);
                    //console.log(msg.labelArr);
                    //console.log(msg.dataPoint);
                        var lineChartData1 = {
                            labels : msg.labelArr,
                            datasets : [
                                {
                                    fillColor : "rgba(220,220,220,0.5)",
                                    strokeColor : "rgba(220,220,220,1)",
                                    pointColor : "rgba(220,220,220,1)",
                                    pointStrokeColor : "#fff",
                                    data : msg.dataPoint
                                }
                            ]

                        };
                        var l=new Array();
                        var id1 = chartfor+"Chart";
                        var max = 20;        
                        var max = msg.chartMax;        

                        if(max == 0){
                            var setps = 0;
                        } else {
                            var steps = 10;
                        }
                        new Chart(document.getElementById(id1).getContext("2d")).Line(lineChartData1, {
                            scaleOverride : true,
                            scaleSteps : steps,
                            scaleStepWidth : max/steps,
                            scaleStartValue : 0
                        });
                        $("#loadingFilter").html("");
                });
            //}
        };'
    , CClientScript::POS_END ); 
    
    Yii::app()->clientScript->registerScript('getChartOnSelectBoxChange',
        'function getChartOnDurationChange() {
            var str = $("#dashboardTabs").find("li.active a").attr("href");
            str = str.substring(1, str.length);
            var duration = $("#"+str+"_dd option:selected").val();
            getChart(str, duration);
        };'
    , CClientScript::POS_END ); 

?>

