<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/jquery.handsontable.full.css'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.handsontable.full.js'); ?>
<link rel="stylesheet" media="screen" href="http://handsontable.com/lib/jquery-ui/css/ui-bootstrap/jquery-ui.custom.css">
<script src="http://handsontable.com/lib/jquery-ui/js/jquery-ui.custom.min.js"></script>
<div class="container main-body">

    <div id="loading-image" style="display: none">
    <img style="display: block;margin-left: 400px;margin-top: 200px;" src='<?php echo Yii::app()->getBaseUrl(); ?>/images/site/override.gif' alt="Loading..." />
</div>
    
    <h2>Mass Upload</h2>
    <div class="row">
        <?php if($errorStr) { ?>
            <div style="color:red;">
            <?php echo $errorStr; ?> 
                <br /><br />
            </div>
        <?php } ?>
        
        <div class="col-sm-9">
                <h3><span class="ol">1</span> Select Media Vendor's Email Address</h3>
                <div class="rows">
                    <?php echo CHtml::dropDownList('userEmail', 'email', $userEmails, array('empty'=>'Select Email')); 
                        $emailErrorStyle = ($emailErrorFlag)==1 ? 'style="display:block"' : 'style="display:none"';
                    ?>
                    <div class="errormessage" id="UploadListingForm_user_email_em_" <?php echo $emailErrorStyle; ?> >Please select an email address.</div>
                </div>
            
<!--                <h3><span class="ol">2</span> Add listing</h3>
                <div class="rows">
                    <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Donec id elit non mi porta gravida at eget metus. Aenean lacinia bibendum nulla sed consectetur. Curabitur blandit tempus porttitor.</p>
                    <div class="download-box">
                        <a href="<?php echo  Yii::app()->baseUrl. '/MassUploadExcelTemplate.xlsx'; ?>" class="excel">Excel template</a>
                        <a href="<?php echo  Yii::app()->baseUrl. '/MassUploadGuide.pdf'; ?>" class="guide">Guide</a>
                    </div>
                </div>-->

                <h3><span class="ol">2</span> Add Listings</h3>
                <div class="rows">
                    <p>Add all the listings in the excel below</p>
                    
                        <div class="upload-holder" id="listings">  

                        </div>
                        
                    <button name="update"  data-dump="#listings" class="btn btn-lg btn-success big">Upload</button>
                </div>

        </div>
    </div>
</div>

<script>
    

        $('#listings').handsontable({
              //data: [['','','','']],
              minCols: 20,
              colHeaders: ['MEDIA TYPE','COUNTRY','STATE','CITY','LOCALITY','LATITUDE','LONGITUDE','NAME','LENGTH','WIDTH','SIZE UNIT','BASE CURRENCY','OTHER DATA','LIGHTING','DESCRIPTION','REACH','AUDIENCE TAG','PRICE','PRICE DURATION', 'AVAILABILITY DATE'],
              rowHeaders: true,
              //colWidths: [100, 100, 100],
              manualColumnResize: true,
              manualRowResize: true,
              minSpareRows: 50,
              columns: [{
                 //data : 'mediatype',
                type: 'text'
            }, {
                //data : 'country',
            type: 'text'
        }, {
            //data : 'state',
            type: 'text'
        }, {
            //data : 'city',
            type: 'text'
        }, {
            //data : 'locality',
            type: 'text'
        },{
            //data : 'lat',
            type: 'numeric'
        }, {
            //data : 'lng',
            type: 'numeric'
        }, {
            //data : 'name',
            type : 'text'
        }, {
            //data : 'len',
            type: 'numeric'
        }, {
            //data : 'wdh',
            type: 'numeric'
        },{
            //data : 'sizeunit',
            type: 'text'
        },{
            //data : 'basecurrency',
            type : 'text'
        },{
            //data : 'otherdata',
            type : 'text'
        },{
            //data : 'lighting',
            type : 'text'
        },{
            //data : 'description',
            type : 'text'
        },{
            //data : 'reach',
            type : 'text'
        },{
            //data : 'audiencetag',
            type : 'text'
        },{
            //data : 'price',
            type: 'numeric'
        },{
            //data : 'priceduration',
            type: 'text'
        },{
            //data : 'availabilitydate',
            dateFormat: 'mm/dd/yy'
        },{
            type : 'text'
        } ]
            });

function validateEmail(email) { 
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
            
            $('body').on('click', 'button[name=update]', function () {
            //jQuery('#loading-image').show(); 
              
               var dump = $(this).data('dump');
               var $container = $(dump);

               console.log('data of ' + dump, JSON.stringify($container.handsontable('getData')));
               var arr = $container.handsontable('getData');
               if (validateEmail($('.ffSelectButton').children().first().text())) {
                   $.ajax({
                    type: 'POST',
                    url: '<?php echo Yii::app()->urlManager->createUrl('ajax/MassUploadListingsForVendor'); ?>',
                    data:{
                          'useremail' : $('.ffSelectButton').children().first().text(),
                           'data' : JSON.stringify(arr) },
                        success:function(data){
                            //var json = JSON.parse(data);
                            console.log("adasdasdaa" + data);
                           },
                           error: function(data) { // if error occured
                                 alert("Error occured.please try again");
                                 alert(data);
                            }
                  });
             } else {
                alert("Please select Media Vendor's Email Address");
             }
               
               
            });   
            
    $('body').attr('id', "dashboard");
    function validateEmail()
    {
        if($('#userEmail').val() == '') {            
            $('#UploadListingForm_user_email_em_').show();
            return false;
        } else {            
            $('#UploadListingForm_user_email_em_').hide();
            return true;
        }
    }
</script>