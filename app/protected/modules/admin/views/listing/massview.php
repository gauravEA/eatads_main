<?php //$this->renderPartial('../dashboard/_ownerHeader', array('activeMenu' => 'massListing')); ?>
<script>
    $('body').removeAttr('id');
    $('body .wrapper').attr('id', 'dashboard');
    function grid_saveChanges()
    {
        /*if (!confirm("Are you sure you want to save all changes?")) {
            e.preventDefault();
        }*/
        //var grid = $("#grid").data("kendoGrid");
        //grid.bind("saveChanges", grid_saveChanges);
        
        /*var boolDirty = false;
        $.each($("#myGrid").data("kendoGrid").dataSource._data, function() {
            if (this.dirty) { boolDirty = true; console.log(boolDirty); }
        });*/
        var grid = $("#grid").data("kendoGrid");
        var result = grid.saveChanges();
        console.log(result + ' ');
        return false;
    }
</script>
<div class="container main-body">


    <h2>View Mass Upload</h2>
    <div id="error" style="color:red;">
        <div class="adderrors"></div>
    </div>
    <br />
    <div>
        <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'massupload_save',
                            'enableClientValidation' => true,
                       )); ?>
            <?php echo CHtml::submitButton('Save', array('name'=> 'save', 'class' => 'btn btn-lg btn-success')); ?>
        <br /><br />
        <?php $this->endWidget(); ?>
    </div>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/kendo.all.min.js'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/kendo.common.min.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/kendo.default.min.css'); ?>        

    <div id="grid"></div>

    <script>        
        function getUrlParameter(query_string, search_param)
        {
            
            var sURLVariables = query_string.split('&');
            for (var i = 0; i < sURLVariables.length; i++)
            {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == search_param)
                {                    
                    if(sParameterName[1]=='') {
                        return true;    // in case of &lt= 
                    } else {
                        return sParameterName[1];   // otherwise
                    }
                }
            }
            return false;   // if param donot exists
        }
        //$(function() {            
            $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: { 
                            url: '<?php echo Yii::app()->urlManager->createUrl('ajax/massview'); ?>'                            
                        },
                        create: { 
                            url: '<?php echo Yii::app()->urlManager->createUrl('ajax/massview'); ?>',
                            type: "PUT",
                            beforeSend: function(e) { $('.adderrors').html(''); },
                            complete: function(e) {
                                if(e.responseText == "success") {
                                    var div_id = 'sn' + getUrlParameter(this.data, 'sn');
                                    $('#'+div_id).remove();
                                    $("#grid").data("kendoGrid").dataSource.read();
                                    alert('Record successfully created.');
                                } else {                                    
                                    var div_id = 'sn' + getUrlParameter(this.data, 'sn');
                                    //$('#'+div_id).remove();
                                    //var html = '<div id="' + div_id+ '" >'+e.responseText+'</div>';
                                    //$('.adderrors').html('');
                                    var html = e.responseText;
                                    
                                    $(".adderrors").append(html);
                                }
                            }
                        },
                        update: {
                            url: '<?php echo Yii::app()->urlManager->createUrl('ajax/massview'); ?>',
                            type: "POST",
                            complete: function(e) {
                                if(e.responseText == "success") {
                                    var div_id = 'sn' + getUrlParameter(this.data, 'sn');
                                    $('#'+div_id).remove();
                                    $("#grid").data("kendoGrid").dataSource.read();
                                    alert('Record successfully updated.');
                                } else {                                    
                                    var div_id = 'sn' + getUrlParameter(this.data, 'sn');
                                    $('#'+div_id).remove();
                                    var html = '<div id="' + div_id+ '" >'+e.responseText+'</div>';
                                    $("#error").append(html);
                                }
                            }
                        },
                        destroy: {
                            url: '<?php echo Yii::app()->urlManager->createUrl('ajax/massview'); ?>',
                            type: "DELETE",
                            complete: function(e) {
                                if(e.responseText == "success") {
                                    var div_id = 'sn' + getUrlParameter(this.data, 'sn');
                                    $('#'+div_id).remove();                                    
                                    alert('Changes have been saved successfully.');
                                } else {
                                    var div_id = 'sn' + getUrlParameter(this.data, 'sn');
                                    $('#'+div_id).remove();
                                    var html = '<div id="' + div_id+ '" >'+e.responseText+'</div>';
                                    $("#error").append(html);
                                }
                            }
                        }
                    },
                    
                    pageSize: <?php echo Yii::app()->params['jrid_recorder_per_page']; ?>,
                    /*error: function(e) {
                        alert(e.responseText);
                    },*/
                    schema: {
                        data: "data",
                        total: function(response) {
                            return $(response.data).length;
                        },
                        model: {
                            id: "id",
                            fields: {
                                sn: { editable: false },
                                name: { required: true },
                                length: { type: 'number', required: true },
                                width: { type: 'number', required: true },
                                sizeunit: { required: true },
                                basecurrency: { required: true },
                                price: { required: true },
                                priceduration: {},
                                otherdata: { },
                                country: { required: true },
                                state: { required: true  },
                                city: { required: true },
                                locality: { },
                                geolat: { type: 'number' },
                                geolng: { type: 'number' },
                                lighting: { required: true },
                                mediatype: { required: true },
                                description: {},
                                reach: { },
                                audiencetag: { required: true }
                            }
                        }
                    }
                },
                columns: [
                        //{ command: { name: "destroy", text: "Delete"}, title: "&nbsp;", width: 110},
                        { field: "sn", title: "S.No.", width:70 },
                        { field: "name", title: "Listing Name", width:110},
                        { field: "length", title: "Length", width: 110, editor: decimal3Editor },
                        { field: "width", title: "Width", width: 110, editor: decimal3Editor },
                        { field: "sizeunit", title: "Size Unit", width:110 },
                        { field: "basecurrency", title: "Base Currency", width:110 },
                        { field: "price", title: "Price", width: 110 },
                        { field: "priceduration", title: "Price Duration", width:110 },
                        { field: "otherdata", title: "Other Data", width:110 },
                        { field: "country", title: "Country", width:110 },
                        { field: "state", title: "State", width:110 },
                        { field: "city", title: "City", width:110 },
                        { field: "locality", title: "Locality", width:110 },
                        { field: "geolat", title: "Latitude", width: 110, editor: decimal6Editor },
                        { field: "geolng", title: "Longitude", width:110, editor: decimal6Editor },
                        { field: "lighting", title: "Lighting", width:110 },
                        { field: "mediatype", title: "Media Type", width:110 },
                        { field: "description", title: "Description", width:110 },
                        { field: "reach", title: "Reach", width:110 },
                        { field: "audiencetag", title: "Audience Tag", width:110 }
                ],
//                batch: true,
//                sortable: true,                
//                editable: true,
//                navigable: true, // enables keyboard navigation                
//                pageable: true,
                /*{ refresh: true, pageSizes: true },*/
//                height: 430,
                //toolbar: ["save" , "cancel" ]   // adds save and cancel button                
                
                editable: "inline",
                pageable: true,
                height: 430
            });
        //});
        function decimal3Editor(container, options) {
            $('<input name="' + options.field + '"/>')
                    .appendTo(container)
                    .kendoNumericTextBox({
                        format: "{0:n3}",
                        decimals: 3,
                        step: 0.001
                    });
        }
        function decimal6Editor(container, options) {
            $('<input name="' + options.field + '"/>')
                    .appendTo(container)
                    .kendoNumericTextBox({
                        format: "{0:n6}",
                        decimals: 6,
                        step: 0.000001
                    });
        }
       $(".k-grid-save-changes").html('<span class="k-icon k-cancel"></span> Save as draft'); 
       $(".k-grid-cancel-changes").click(function(){
           $('#error').html('');
       });                
</script>