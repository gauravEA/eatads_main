<div class="navbar navbar-second navbar-static-top ">
    <div class="container">
        <div class="left">
            <h3><img src="<?php echo Yii::app()->baseUrl ; ?>/images/site/john_doe.png" alt="john_doe" >Welcome, <?php echo Yii::app()->user->name;?></h3>
        </div>
        <div class="right">
            <ul> 
                <li <?php if($activeMenu == "home") { ?>class="active" <?php  } ?> ><a href="<?php echo Yii::app()->urlManager->createUrl('admin/dashboard'); ?>">Home</a></li>
                <li <?php if($activeMenu == "addListing") { ?>class="active" <?php  } ?> ><a href="<?php echo Yii::app()->urlManager->createUrl('admin/listing/create'); ?>">Add Listing</a></li>
                <li <?php if($activeMenu == "massUpload") { ?>class="active" <?php  } ?> ><a href="<?php echo Yii::app()->urlManager->createUrl('admin/listing/mass'); ?>">Mass Upload</a></li>
                <li <?php if($activeMenu == "editProfile") { ?>class="active" <?php  } ?> ><a href="<?php echo Yii::app()->urlManager->createUrl('admin/profile/edit'); ?>">Edit Your Profile</a></li>                                
            </ul>
        </div>
    </div>
</div>