<script>
    $('#body').attr('id', 'dashboard');
</script>

<!-- Fixed navbar -->
<?php $this->renderPartial('/dashboard/_ownerHeader', array('activeMenu' => 'addListing')); ?>

<div class="container main-body">
                <h2><?php echo $listingHeading; ?></h2>
                <div class="row">
                    <div class="col-sm-12">
                        <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'listing-form',
                            // Please note: When you enable ajax validation, make sure the corresponding
                            // controller action is handling ajax validation correctly.
                            // There is a call to performAjaxValidation() commented in generated controller code.
                            // See class documentation of CActiveForm for details on this.
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),                           
                            'htmlOptions' => array('enctype'=>'multipart/form-data', 'class' => 'form-signup'),
                       )); ?>
                            <h3><span class="ol">1</span> Choose the listing type</h3>
                            <div class="rows">
                                <?php // echo $form->labelEx($model,'mediatypeid'); ?>
                                <?php echo $form->radioButtonList($model,'mediatypeid', $mediaTypeList, array('separator'=>' ')); ?>
                                <?php // echo $form->error($model,'mediatypeid'); ?>
                                <?php echo $form->error($model, 'mediatypeid', array('class'=>'errormessage')); ?>
                            </div>
                            <div class="rows">
                                <div class="left">
                                    <h3><span class="ol">2</span> Location</h3>
                                    <div class="col-sm-6 clearfix">
                                        <?php // echo $form->labelEx($model,'countryid'); ?>
                                        <?php echo $form->dropDownList($model, 'countryid', $countryList, array('empty'=>'Select country')); ?>
                                        <?php echo $form->error($model, 'countryid', array('class'=>'errormessage')); ?>
                                    </div>
                                    <div class="col-sm-6 clearfix" >
                                        <?php // echo $form->labelEx($model,'stateid'); ?>                                        
                                        <?php echo $form->dropDownList($model, 'stateid', $stateList, array('empty'=>'Select state')); ?>
                                        <?php echo $form->error($model, 'stateid', array('class'=>'errormessage')); ?>

                                    </div>
                                    <div class="col-sm-6 clearfix">
                                        <?php // echo $form->labelEx($model,'cityid'); ?>
                                        <?php echo $form->dropDownList($model, 'cityid', $cityList, array('empty'=>'Select city')); ?>                                        
                                        <?php echo $form->error($model, 'cityid', array('class'=>'errormessage')); ?>

                                    </div>
                                    <div class="gps">
                                        <div class="col-sm-6 clearfix">
                                            <?php echo $form->labelEx($model,'street', array('class'=>'top')); ?>
                                           <?php echo $form->textField($model,'street',array('size'=>60,'maxlength'=>100, 'class'=>'form-control')); ?>
                                           <?php // echo $form->error($model,'street'); ?>
                                           <?php echo $form->error($model, 'street', array('class'=>'errormessage')); ?> 
                                        </div>
                                        <div class="col-sm-6 ">                                           
                                        </div>
                                    </div>
                                    <div class="gps">
                                        <div class="col-sm-6 clearfix">
                                            <h4>Or enter GPS coordinates</h4>
                                        </div>
                                        <div class="col-sm-6 ">
                                           <?php // echo $form->labelEx($model,'geolat', array('class'=>'top')); ?>
                                           <?php echo $form->textField($model,'geolat',array('class'=>'form-control')); ?>
                                           <?php // echo $form->error($model,'geolat'); ?>
                                           <?php echo $form->error($model, 'geolat', array('class'=>'errormessage')); ?>
                                        </div><br clear="all">
                                        <div class="col-sm-6 ">
                                            <h4>&nbsp;</h4>

                                        </div>
                                        <div class="col-sm-6 ">
                                            <?php // echo $form->labelEx($model,'geolng', array('class'=>'top')); ?>
                                            <?php echo $form->textField($model,'geolng',array('class'=>'form-control')); ?>
                                            <?php // echo $form->error($model,'geolng'); ?>
                                            <?php echo $form->error($model, 'geolng', array('class'=>'errormessage')); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="right">
                                    <h4>Drag the pin to the listings location</h4>
                                    <div id="map-canvas" style="height: 300px; width:484px;" ></div>
                                </div>
                            </div>
                            <div class="rows">

                                <h3><span class="ol">3</span> Listing Specific Data</h3>
                                <div class="col-sm-6 ">
                                    <?php echo $form->labelEx($model,'name', array('class'=>'top')); ?>
                                    <?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50, 'class'=>'form-control')); ?>
                                    <?php // echo $form->error($model,'name'); ?>
                                    <?php echo $form->error($model, 'name', array('class'=>'errormessage')); ?>
                                </div>
                                <div class="col-sm-6 ">
                                    <?php echo $form->labelEx($model,'length', array('class'=>'top')); ?>
                                    <?php echo $form->textField($model,'length',array('size'=>10,'maxlength'=>10, 'class'=>'form-control')); ?>
                                    <?php // echo $form->error($model,'length'); ?>
                                    <?php echo $form->error($model, 'length', array('class'=>'errormessage')); ?>
                                </div>
                                <div class="col-sm-6 ">
                                    <?php echo $form->labelEx($model,'width', array('class'=>'top')); ?>
                                    <?php echo $form->textField($model,'width',array('size'=>10,'maxlength'=>10, 'class'=>'form-control')); ?>
                                    <?php // echo $form->error($model,'width'); ?>
                                    <?php echo $form->error($model, 'width', array('class'=>'errormessage')); ?>
                                </div>
                                <div class="col-sm-6 ">
                                    <?php echo $form->labelEx($model,'sizeunitid', array('class'=>'top')); ?>
                                    <?php echo $form->radioButtonList($model,'sizeunitid', $sizeUnitList, array('separator'=>'&nbsp;&nbsp;', 'class'=>'form-control')); ?>
                                    <?php // echo $form->error($model,'sizeunitid'); ?>
                                    <?php echo $form->error($model, 'sizeunitid', array('class'=>'errormessage')); ?>
                                </div>
                                <div class="col-sm-6 ">
                                    <?php echo $form->labelEx($model,'basecurrencyid', array('class'=>'top')); ?>
                                    <?php echo $form->dropDownList($model, 'basecurrencyid', $baseCurrencyList, array('empty'=>'Select base currency')); ?>
                                    <?php // echo $form->error($model,'basecurrency'); ?>
                                    <?php echo $form->error($model, 'basecurrencyid', array('class'=>'errormessage')); ?>
                                </div>
                                
                                <div class="col-sm-6 ">
                                    <?php echo $form->labelEx($model,'otherdata', array('class'=>'top')); ?>
                                    <?php echo $form->textField($model,'otherdata',array('size'=>50,'maxlength'=>50, 'class'=>'form-control')); ?>
                                    <?php // echo $form->error($model,'otherdata'); ?>
                                    <?php echo $form->error($model, 'otherdata', array('class'=>'errormessage')); ?>
                                </div>                                
                            </div>
                            <div class="rows">
                                <div class="col-sm-6 ">
                                    <?php echo $form->labelEx($model,'lightingid', array('class'=>'top')); ?>
                                    <?php echo $form->dropDownList($model,'lightingid', $lightingList); ?>
                                    <?php // echo $form->error($model,'lightingid'); ?>
                                    <?php echo $form->error($model, 'lightingid', array('class'=>'errormessage')); ?>
                                </div>
                                <div class="col-sm-6 ">
                                    <?php echo $form->labelEx($model,'description', array('class'=>'top')); ?>
                                    <?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>
                                    <?php // echo $form->error($model,'description'); ?>
                                    <?php echo $form->error($model, 'description', array('class'=>'errormessage')); ?>
                                </div>
                                <div class="col-sm-6 ">
                                    <?php echo $form->labelEx($model,'reach', array('class'=>'top')); ?>
                                    <?php echo $form->textField($model,'reach', array('size'=>50,'maxlength'=>50, 'class'=>'form-control')); ?>                                    
                                    <?php echo $form->error($model, 'reach', array('class'=>'errormessage')); ?>
                                </div>
                            </div>
                            <div class="rows">                                
                                <div class=" ">
                                    <?php echo $form->labelEx($audTagModel,'audience tag', array('class'=>'top')); ?>
                                    <?php echo $form->checkboxlist($audTagModel, 'id', $audienceTagCheckboxList, array('class'=>'audiencetag', 'separator'=>'&nbsp;&nbsp;')); ?>
                                    <?php echo $form->error($audTagModel, 'id', array('class'=>'errormessage')); ?>
                                </div>
                            </div>
                            <div class="rows">

                                <h3><span class="ol">4</span> Pricing</h3>
                                <div class="col-sm-6 ">
                                    <?php echo $form->labelEx($model,'price', array('class'=>'top')); ?>
                                    <?php echo $form->textField($model,'price',array('size'=>10,'maxlength'=>10, 'class'=>'form-control')); ?>
                                    <?php // echo $form->error($model,'price'); ?>
                                    <?php echo $form->error($model, 'price', array('class'=>'errormessage')); ?>
                                </div>
                                <div class="col-sm-7 ">
                                    <?php echo $form->radioButtonList($model,'pricedurationid', $priceDurationList, array('separator'=>'&nbsp;&nbsp;')); ?>
                                    <?php echo $form->error($model, 'pricedurationid', array('class'=>'errormessage')); ?>
                                </div>
                            </div>
                            
                            
                            
                            <div class="rows">
                               <h3><span class="ol">5</span> Upload Image</h3>
                               <div class="col-sm-6 ">
                                   <input id="listingimages" name="listingimages" type="file" multiple="true">
                                   <?php 
//                                   $this->widget('CMultiFileUpload', array(
//							'name'=>'listingimages',
//							'accept'=>'jpeg|jpg|png',
//							'duplicate'=>'Duplicate File!',
//							'denied'=>'Invalid file type',
//                                                        'max'=>Yii::app()->params['fileUpload']['count'],                            
//				   ));
                                   ?>
                               </div>
                            </div>
                            
                            <?php if(isset($listingImages) && count($listingImages)){ ?>
                                <div class="rows">
                                   <h3><span class="ol">6</span> Uploaded Image</h3>
                                   <div class="col-sm-6 ">
                                        <?php
                                            foreach($listingImages as $image) {
                                        ?>
                                            <div style="float:left; padding-right:5px;">
                                                <img src="<?php echo JoyUtilities::getAwsFileUrl("tiny_".$image->filename); ?>" alt="listing_image" />
                                                <div class="deletePermanentImage" id="<?php echo $image->id; ?>" rel="<?php echo $image->filename; ?>" style="cursor:pointer;">X</div>
                                            </div>
                                        <?php } ?>
                                   </div>
                                </div>
                            <?php } ?>
                            
                            
                            <?php if(isset($alreadyUploadedArr) && count($alreadyUploadedArr)){ ?>
                                <div class="rows">
                                   <h3><span class="ol">6</span> Pushed to Our Server </h3>
                                   <div class="col-sm-6 ">
                                        <?php
                                            foreach($alreadyUploadedArr as $image) {
                                        ?>
                                            <div style="float:left; padding-right:5px;">
                                                <img width="100" height="100" src="<?php echo Yii::app()->getBaseUrl() . '/uploads/listing/'.$image; ?>" alt="listing_image" />
                                                <div class="deleteImage" rel="<?php echo $image; ?>" style="cursor:pointer;">X</div>
                                            </div>
                                        <?php } ?>
                                   </div>
                                </div>
                            <?php } ?>
                            
                            <div class="col-sm-12">
                                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-lg btn-success big')); ?>
                            </div>

                        <?php $this->endWidget(); ?>
                    </div>
                </div>
            </div>
<?php $this->widget('Uploadify', array('fileFieldId' => 'listingimages', 'imageType' => 'listing') ); ?>
<script type="text/javascript">
		$(function(){
            
            // length width only 3 digits after decimal
            $('#Listing_length,#Listing_width').keyup(function(){
                if($(this).val().indexOf('.')!=-1){         
                    if($(this).val().split(".")[1].length > 3){
                        if( isNaN( parseFloat( this.value ) ) ) return;
                        this.value = parseFloat(this.value).toFixed(3);
                    }  
                 }            
                 return this; //for chaining
            });
            
            // Add hidden field to keep uploaded file name but after validation error keep here
            $('.uploadifive-button').after('<input type=\'hidden\' id=\'uploaded_filenames\' value=\'<?php echo $uploaded_filenames; ?>\' name=\'uploaded_filenames\' />');
            
            // audience tag
            $(".audiencetag").click(function(){                
                count_checked = $(".audiencetag:checked").length;
                if (count_checked >= 3){                    
                    $(".audiencetag").not(":checked").attr("disabled", true);
                } else {                                       
                    $(".audiencetag").not(":checked").attr("disabled", false);
                }
            });
        
			//$('#stateid').fancyfields("disable");
            //$('#cityid').fancyfields("disable");            
            // AJAX city field
            $('#Listing_countryid').fancyfields("bind","onSelectChange", function (input,text,val){
                //alert("Current input - ID : "+input.attr("id") + " , selected text : " + text + " , value : " + val);
                $.ajax({                
                    type: "POST",
                    url: "<?php echo Yii::app()->urlManager->createUrl('ajax/dynamicstates'); ?>", //url to call.
                    cache: true,
                    data: {id: val},
                    success: function (data) {
                        $('#Listing_stateid').fancyfields("enable");
                        $('#Listing_stateid').setOptions(eval(data));
                        $('#Listing_cityid').setOptions([["Select city", ""]]);
                    },
                });                
            });
            // AJAX state field
            $('#Listing_stateid').fancyfields("bind","onSelectChange", function (input,text,val){
                //alert("Current input - ID : "+input.attr("id") + " , selected text : " + text + " , value : " + val);
                $.ajax({                
                    type: "POST",
                    url: "<?php echo Yii::app()->urlManager->createUrl('ajax/dynamiccities'); ?>", //url to call.
                    cache: true,
                    data: {id: val},
                    success: function (data) {
                        $('#Listing_cityid').fancyfields("enable");                        
                        $('#Listing_cityid').setOptions(eval(data));                    
                    },
                });                
            });            
		});	
            
            <?php if($model->id) { ?>
            // This will come only in edit case
            $( '.deletePermanentImage' ).click(function(){
                var fileName = $( this ).attr('rel');
                var imageId = $( this ).attr('id');
                var currItem = $( this );
                
                 $.ajax({
                    type: 'POST',
                    url:  '<?php echo Yii::app()->urlManager->createUrl('uploadify/deletePermanentFile')?>',
                    data: { filename: fileName, listingid: <?php echo $model->id; ?>, imageid: imageId}
                }) .done(function( msg ) {
                    currItem.parent().html('');
                    // 1 for success 0 otherwise
                });
            });
            <?php } ?>
            
            $( '.deleteImage' ).click(function(){
                var fileName = $( this ).attr('rel');
                var currItem = $( this );
                
                 $.ajax({
                    type: 'POST',
                    url:  '<?php echo Yii::app()->urlManager->createUrl('uploadify/deleteFile')?>',
                    data: { filename: fileName }
                }) .done(function( msg ) {
                    
                    currItem.parent().html('');
                    // 1 for success 0 otherwise
                    setTimeout(function() {
                        getUploadedRemoveFileList();
                        // 1000 for 1 seconds
                    }, 1000)

                });
            });
            
            function getUploadedRemoveFileList() {
                var fileArr = [];
                $( '.deleteImage' ).each(function( index ) {
                    fileArr.push( $( this ).attr('rel') );
                });
                fileArr.join( ',' )
                $('#uploaded_filenames').val(fileArr);
            }
</script>