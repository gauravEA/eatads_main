<?php
/* @var $this AudiencetagController */
/* @var $model AudienceTag */

$this->breadcrumbs=array(
	'Audience Tags'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List AudienceTag', 'url'=>array('index')),
	array('label'=>'Create AudienceTag', 'url'=>array('create')),
	array('label'=>'Update AudienceTag', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AudienceTag', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AudienceTag', 'url'=>array('admin')),
);
?>

<h1>View AudienceTag #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'status',
	),
)); ?>
