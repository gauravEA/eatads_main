<?php
/* @var $this AudiencetagController */
/* @var $model AudienceTag */

$this->breadcrumbs=array(
	'Audience Tags'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AudienceTag', 'url'=>array('index')),
	array('label'=>'Manage AudienceTag', 'url'=>array('admin')),
);
?>

<h1>Create AudienceTag</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>