<?php
/* @var $this AudiencetagController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Audience Tags',
);

$this->menu=array(
	array('label'=>'Create AudienceTag', 'url'=>array('create')),
	array('label'=>'Manage AudienceTag', 'url'=>array('admin')),
);
?>

<h1>Audience Tags</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
