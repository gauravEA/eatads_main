<?php
/* @var $this AudiencetagController */
/* @var $model AudienceTag */

$this->breadcrumbs=array(
	'Audience Tags'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AudienceTag', 'url'=>array('index')),
	array('label'=>'Create AudienceTag', 'url'=>array('create')),
	array('label'=>'View AudienceTag', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AudienceTag', 'url'=>array('admin')),
);
?>

<h1>Update AudienceTag <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>