<?php
/* @var $this EmailController */
/* @var $model EmailTemplate */

$this->breadcrumbs=array(
	'Email Templates'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EmailTemplate', 'url'=>array('index')),
	array('label'=>'Manage EmailTemplate', 'url'=>array('admin')),
);
?>

<h1>Create EmailTemplate</h1>

<?php $this->renderPartial('_my_form', array('model'=>$model)); ?>