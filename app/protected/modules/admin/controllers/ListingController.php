<?php

class ListingController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform actions
                'actions' => array('admin', 'create', 'massview', 'massupload', 'delete', 'index', 'solr', 'update', 'view', 'signup','massuploadnew'),
                'users' => array('@'),
                'roles' => array(JoyUtilities::ROLE_ADMIN),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $countryList = array();
        $stateList = array();
        $cityList = array();
        $model = new Listing;
        $model->setScenario('createListing');
        $audTagModel = new AudienceTag;
        $audTagModel->setScenario('addlisting');
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Listing'])) {
            $_POST['Listing'] = JoyUtilities::cleanInput($_POST['Listing']);
            $model->attributes = $_POST['Listing'];
            $audTagModel->attributes = $_POST['AudienceTag'];
            $hasError = false;

            if (!$audTagModel->validate()) {
                $hasError = true;
            }
            if (!$model->validate()) {
                $hasError = true;
            }

            if (!$hasError) {
                // Multiple images upload
                $images = CUploadedFile::getInstancesByName('listingimages');
                $model->byuserid = Yii::app()->user->id;
                $model->foruserid = Yii::app()->user->id;
                $model->weeklyprice = Listing::calculateWeeklyPrice($_POST['Listing']['price'], $_POST['Listing']['pricedurationid']);
                $model->datecreated = date("Y-m-d H:i:s");
                $model->length = number_format((float) $model->length, 3, '.', '');
                $model->width = number_format((float) $model->width, 3, '.', '');
                if ($model->save()) {
                    // get the listing id                    
                    $listingId = $model->getPrimaryKey();
                    // save audience tag for the listing

                    foreach ($audTagModel->id as $audIds) {
                        $listAudTagModel = new ListingAudienceTag;
                        $listAudTagModel->listingid = $listingId;
                        $listAudTagModel->audiencetagid = $audIds;
                        $listAudTagModel->save();
                    }
                    // save the listing images with listing id
                    $listingImageModel = new ListingImage;
                    $listingImageModel->listingid = $listingId;

                    // proceed if the images have been set
                    if (isset($images) && count($images) > 0) {
                        // go through each images
                        $i = 1;
                        foreach ($images as $image) {
                            if ($image->size > 0 && $image->size < Yii::app()->params['fileUpload']['size']) {
                                $uploadedImagePath = Yii::getPathOfAlias('webroot') . Yii::app()->params['fileUpload']['path'];
                                $uploadedImageName = time() . '.' . $image->getExtensionName();
                                if ($image->saveAs($uploadedImagePath . $uploadedImageName)) {
                                    $imageThumb = new EasyImage($uploadedImagePath . $uploadedImageName);

                                    $imageThumb->resize(1280, 1024);
                                    $newImgThumbName = $uploadedImagePath . $uploadedImageName;
                                    $imageThumb->save($newImgThumbName);

                                    $imageThumb->resize(487, 310);
                                    $newImgThumbName = $uploadedImagePath . 'big_' . $uploadedImageName;
                                    $imageThumb->save($newImgThumbName);

                                    $imageThumb->resize(212, 160);
                                    $newImgThumbName = $uploadedImagePath . 'small_' . $uploadedImageName;
                                    $imageThumb->save($newImgThumbName);

                                    $imageThumb->resize(102, 74);
                                    $newImgThumbName = $uploadedImagePath . 'tiny_' . $uploadedImageName;
                                    $imageThumb->save($newImgThumbName);
                                    $listingImageModel->filename = $uploadedImageName;
                                    if (!$listingImageModel->save())
                                        echo "error uploading image {$i}.<br />";
                                }
                            }
                        }
                    }
                    Yii::app()->user->setFlash('success', "Listing successfully added.");
                    $this->redirect(array('view', 'id' => $model->id));
                }
            }
        }

        $countryId = $model->countryid;
        $countryList = Area::model()->getCountryOptions();

        $stateId = $model->stateid;
        if ($stateId) {
            $stateList = Area::model()->getStateOptions($countryId);
        }
        $cityId = $model->cityid;
        if ($cityId) {
            $cityList = Area::model()->getCityOptions($stateId);
        }

        $baseCurrencyList = CHtml::listData(LookupBaseCurrency::model()->findAll(array('select' => 'id,currency_code')), 'id', 'currency_code');
        $audienceTagCheckboxList = CHtml::listData(AudienceTag::model()->findAll(array('select' => 'id,name')), 'id', 'name');
        $mediaTypeList = CHtml::listData(MediaType::model()->findAll(array('select' => 'id,name')), 'id', 'name');
        $sizeUnitList = Listing::getSizeUnit();
        $lightingList = Listing::getLighting();
        $priceDurationList = Listing::getPriceDuration();
        $this->render('create', array(
            'model' => $model,
            'audTagModel' => $audTagModel,
            'baseCurrencyList' => $baseCurrencyList,
            'audienceTagCheckboxList' => $audienceTagCheckboxList,
            'mediaTypeList' => $mediaTypeList,
            'sizeUnitList' => $sizeUnitList,
            'lightingList' => $lightingList,
            'priceDurationList' => $priceDurationList,
            'countryList' => $countryList,
            'stateList' => $stateList,
            'cityList' => $cityList,
            'listingHeading' => 'Add new listing',
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $countryList = array();
        $stateList = array();
        $cityList = array();
        $model = new Listing;
        $audTagModel = new AudienceTag;
        $audTagModel->setScenario('addlisting');
        // Multiple images upload
        $images = CUploadedFile::getInstancesByName('listingimages');
        $model = $this->loadModel($id);
        $model->setScenario('updateListing');
        $selectedAudTagIds = ListingAudienceTag::model()->findAllByAttributes(array('listingid' => $id), array('select' => 'audiencetagid'));
        $audTagIds['id'] = array();
        foreach ($selectedAudTagIds as $selAudTagIds) {
            array_push($audTagIds['id'], $selAudTagIds->audiencetagid);
        }
        $audTagModel->attributes = $audTagIds;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Listing'])) {
            $_POST['Listing'] = JoyUtilities::cleanInput($_POST['Listing']);
            $audTagModel->attributes = $_POST['AudienceTag'];
            $model->attributes = $_POST['Listing'];

            $hasError = false;

            if (!$audTagModel->validate()) {
                $hasError = true;
            }
            if (!$model->validate()) {
                $hasError = true;
            }

            if (!$hasError) {

                $model->byuser = Yii::app()->user->id;
                $model->foruser = Yii::app()->user->id;
                $model->weeklyprice = Listing::calculateWeeklyPrice($_POST['Listing']['price'], $_POST['Listing']['pricedurationid']);
                $model->solr = 0;
                $model->datemodified = date("Y-m-d H:i:s");
                $model->length = number_format((float) $model->length, 3, '.', '');
                $model->width = number_format((float) $model->width, 3, '.', '');
                if ($model->save()) {
                    // get the listing id
                    $listingId = $id;

                    // delete all prev aud tags and update new tags
                    $listAudTagModel = new ListingAudienceTag;
                    $listAudTagModel->deleteAllByAttributes(array('listingid' => $listingId));
                    foreach ($audTagModel->id as $audIds) {
                        $listAudTagModel = new ListingAudienceTag;
                        $listAudTagModel->listingid = $listingId;
                        $listAudTagModel->audiencetagid = $audIds;
                        $listAudTagModel->save();
                    }

                    // save the listing images with listing id
                    $listingImageModel = new ListingImage;
                    $listingImageModel->listingid = $listingId;

                    // proceed if the images have been set
                    if (isset($images) && count($images) > 0) {
                        // go through each images
                        $i = 1;
                        foreach ($images as $image) {
                            if ($image->size > 0 && $image->size < Yii::app()->params['fileUpload']['size']) {
                                $uploadedImagePath = Yii::getPathOfAlias('webroot') . Yii::app()->params['fileUpload']['path'];
                                $uploadedImageName = time() . '.' . $image->getExtensionName();
                                if ($image->saveAs($uploadedImagePath . $uploadedImageName)) {
                                    $imageThumb = new EasyImage($uploadedImagePath . $uploadedImageName);

                                    $imageThumb->resize(1280, 1024);
                                    $newImgThumbName = $uploadedImagePath . $uploadedImageName;
                                    $imageThumb->save($newImgThumbName);

                                    $imageThumb->resize(487, 310);
                                    $newImgThumbName = $uploadedImagePath . 'big_' . $uploadedImageName;
                                    $imageThumb->save($newImgThumbName);

                                    $imageThumb->resize(212, 160);
                                    $newImgThumbName = $uploadedImagePath . 'small_' . $uploadedImageName;
                                    $imageThumb->save($newImgThumbName);

                                    $imageThumb->resize(102, 74);
                                    $newImgThumbName = $uploadedImagePath . 'tiny_' . $uploadedImageName;
                                    $imageThumb->save($newImgThumbName);
                                    $listingImageModel->filename = $uploadedImageName;
                                    if (!$listingImageModel->save())
                                        echo "error uploading image {$i}.<br />";
                                }
                            }
                        }
                    }
                    Yii::app()->user->setFlash('success', "Listing successfully updated.");
                    $this->redirect(array('view', 'id' => $model->id));
                }
            }
        }

        $countryId = $model->countryid;
        $countryList = Area::model()->getCountryOptions();

        $stateId = $model->stateid;
        if ($stateId) {
            $stateList = Area::model()->getStateOptions($countryId);
        }
        $cityId = $model->cityid;
        if ($cityId) {
            $cityList = Area::model()->getCityOptions($stateId);
        }

        $baseCurrencyList = CHtml::listData(LookupBaseCurrency::model()->findAll(array('select' => 'id,currency_code')), 'id', 'currency_code');
        $audienceTagCheckboxList = CHtml::listData(AudienceTag::model()->findAll(array('select' => 'id,name')), 'id', 'name');
        $mediaTypeList = CHtml::listData(MediaType::model()->findAll(array('select' => 'id,name')), 'id', 'name');
        $sizeUnitList = Listing::getSizeUnit();
        $lightingList = Listing::getLighting();
        $priceDurationList = Listing::getPriceDuration();
        $this->render('update', array(
            'model' => $model,
            'audTagModel' => $audTagModel,
            'baseCurrencyList' => $baseCurrencyList,
            'audienceTagCheckboxList' => $audienceTagCheckboxList,
            'mediaTypeList' => $mediaTypeList,
            'sizeUnitList' => $sizeUnitList,
            'lightingList' => $lightingList,
            'priceDurationList' => $priceDurationList,
            'countryList' => $countryList,
            'stateList' => $stateList,
            'cityList' => $cityList,
            'listingHeading' => 'Update Listing',
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Listing');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Solr Index
     */
    public function actionSolr() {
        // get all the listing with solr status 0 (not indexed)
        // call the stored procedure
        $list = Yii::app()->db->createCommand('CALL getListing(0);')->queryAll();
        //echo '<pre>';var_dump($list); die();echo '</pre>';
        $i = 0;
        foreach ($list as $lt) {
            $lt['sizeunit'] = Listing::getSizeUnit($lt['sizeunitid']);
            $lt['priceduration'] = Listing::getPriceDuration($lt['pricedurationid']);
            unset($lt['pricedurationid']);
            $lt['datemodified'] = date('Y-m-d', strtotime($lt['datemodified'])) . 'T' . date('H:i:s', strtotime($lt['datemodified'])) . 'Z';
            $lt['reach'] = empty($lt['reach']) ? 0 : $lt['reach'];
            /*
              // solr score field name is pscore (score is reserver word)
              $lt['pscore'] = $lt['score'];
              unset($lt['score']);
             */
            // add to solr index
            /*echo '<pre>';
            print_r($lt);
            die();*/
            Yii::app()->listingSearch->updateOne($lt);

            // update listing solr field to 1
            $listRow = Listing::model()->findByPk($lt['id']);
            $listRow->solr = 1;
            $listRow->save();       // save solr status to indexed

            $i++;
            //die();
        }
        //echo '<pre>'; print_r($lt); echo '</pre>'; die();

        /*
          // To search in your index
          $result= Yii::app()->listingSearch->get('*:*',0,20);
          if(isset($result->response->numFound) && $result->response->numFound) {
          echo "Results number is ".$result->response->numFound.'<br />';
          echo '<pre>';
          foreach($result->response->docs as $doc){
          print_r($doc);
          }
          echo '</pre>';
          } */

        $this->render('solr', array('docs' => $i));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Listing('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Listing']))
            $model->attributes = $_GET['Listing'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Listing the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Listing::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Listing $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'listing-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /*
     * Mass Upload using PHPExcel
     */
    public function actionMassupload()
    {
        $model = new UploadListingForm;
        $errorStr = '';
        $emailErrorFlag = 0;
        // get owner emails
        $userEmails = User::getOwnerEmail();
        
        if(isset($_POST['UploadListingForm']))
        {                        
            $model->file_upload = CUploadedFile::getInstance($model,'file_upload');
            // echo "<pre>"; print_r($model->file_upload);             
            if($model->validate())
            {
                $fileExtension = pathinfo($model->file_upload->name, PATHINFO_EXTENSION); 
                $objPHPExcel = Yii::app()->excel;
                if($fileExtension == 'csv') {           // case CSV
                    $objReader = $objPHPExcel->readCsv();                    
                } else if($fileExtension == 'xls') {    // case XLS
                    $objReader = $objPHPExcel->readExcel9703();                    
                } else {                                // default case XLSX
                    $objReader = $objPHPExcel->readExcel2007();                    
                }
                
                $objPHPExcel = $objReader->load($model->file_upload->tempName);
                $highestRowNumber = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow()."<br />";
                
                //echo $highestColNumber = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn()."<br />";
                //exit;
                
                $hasError = 0;
                if($highestRowNumber == 1) {
                    $hasError = 1;
                    $errorStr = 'Please enter at least one record for listing.';
                } elseif($highestRowNumber > 1) { // if more than one row is there but not in proper order
                    $headerRow = 1;
                    foreach(range("A", "S") as $col) {
                        $excelheader = $this->getExcelHeaderRow();
                        $cellNumber = $col.$headerRow;
                        $cellObj = $objPHPExcel->getActiveSheet()->getCell($cellNumber);
                        $cellValue = $cellObj->getValue();
                        if(strtolower($cellValue) != strtolower($excelheader[$col])) {
                            $hasError = 1;
                            $errorStr = 'Please upload valid template file';
                            break;
                        }
                    }
                }
             
                // loop through eatch cell element
                // ROW STARTS HERE
                
                // initialize excel heading column
                $this->setMassExcelFields();
                //echo '<pre>';print_r($_POST);
                
                if(!$hasError) { // if template has more than one row and template header is valid then check other validation
                    $errorCounter = 1;
                    
                    for($rowNumber=2; $rowNumber<=$highestRowNumber; $rowNumber++) {
                        $accurateGeoloc = 0;
                        $country = $state = $city = $lat = $lng = '';
                        $objMassUpload = new ListingDraft();                        
                        $objMassUpload->setScenario('createListing');

                        foreach(range("A", "S") as $col) {
                            
                            $attribute = $this->massExcelFields[$col]; 
                            $cellNumber = $col.$rowNumber;

                            if($col == 'G' || $col == 'F'){                                
                                $cellObj = $objPHPExcel->getActiveSheet()->getCell($cellNumber);                                
                                if ( is_float($cellObj->getValue()) || is_int($cellObj->getValue())) {
                                    $cellValue = number_format($cellObj->getValue(), 6);
                                } else {
                                    $cellValue = $cellObj->getValue();
                                }                                
                            } else {
                                $cellObj = $objPHPExcel->getActiveSheet()->getCell($cellNumber);
                                $cellValue = $cellObj->getValue();
                            }
                            //echo $cellNumber . ' = ' . $cellValue . '<br />';
                            $objMassUpload->$attribute = $cellValue;
                        }
                        
                        
                        if(!$objMassUpload->validate()) {
                            $hasError = 1;
                        } 
                        // match lat lng and country/state/city                        
                        $checkWithLocalityCity = false;
                        if(!$hasError) {
                            
                            if(!empty($objMassUpload->geolat) && !empty($objMassUpload->geolng)) {                                
                                // lat lng provided, so just get the country state city
                                // and update/add in db                                 

                                $locationInfoArr = JoyUtilities::reverseGeocode($objMassUpload->geolat, $objMassUpload->geolng);
                                if(!isset($locationInfoArr['country']) || empty($locationInfoArr['country']) 
                                    || !isset($locationInfoArr['state']) || empty($locationInfoArr['state'])
                                    || !isset($locationInfoArr['city']) || empty($locationInfoArr['city'])) {
                                    // possibly border case, try with location + city
                                    $checkWithLocalityCity = true;
                                } else {
                                    Area::model()->setLocationArray($locationInfoArr);   // to update/add location in db
                                    $locationArr[$rowNumber]['country'] = $locationInfoArr['country'];
                                    $locationArr[$rowNumber]['state'] = $locationInfoArr['state'];
                                    $locationArr[$rowNumber]['city'] = $locationInfoArr['city'];
                                    $locationArr[$rowNumber]['lat'] = number_format($objMassUpload->geolat, 6);
                                    $locationArr[$rowNumber]['lng'] = number_format($objMassUpload->geolng, 6);
                                    $locationArr[$rowNumber]['accurate_geoloc'] = 1;
                                    //$country = $locationInfoArr['country'];
                                    //$state = $locationInfoArr['state'];
                                    //$city = $locationInfoArr['city'];
                                    //$accurateGeoloc = 1;
                                    $checkWithLocalityCity = false;
                                }
                            } else {
                                
                                $checkWithLocalityCity = true;
                            }
                            
                            if($checkWithLocalityCity) {
                                if(!empty($objMassUpload->city)) {
                                    // reverse geocode to get lat long, from locality and city
                                    $locationArr[$rowNumber]['accurate_geoloc'] = 0;
                                    //$accurateGeoloc = 0;
                                    // first try with locality + city
                                    $address = $objMassUpload->city;
                                    $address .= empty($objMassUpload->locality) ? '' : ','.$objMassUpload->locality;
                                    
                                    $locationInfoArr = JoyUtilities::geocode($address);
                                    if(!isset($locationInfoArr['country']) || empty($locationInfoArr['country']) 
                                        || !isset($locationInfoArr['state']) || empty($locationInfoArr['state'])
                                        || !isset($locationInfoArr['city']) || empty($locationInfoArr['city'])
                                        || !isset($locationInfoArr['lat']) || empty($locationInfoArr['lat'])
                                        || !isset($locationInfoArr['lng']) || empty($locationInfoArr['lng'])) {
                                        // need to check it with city + country
                                        $objMassUpload->addError('city', 'Please enter valid city.');
                                        $hasError = 1;                                                                                  
                                    } else {
                                        Area::model()->setLocationArray($locationInfoArr);   // to update/add location in db
                                        $locationArr[$rowNumber]['country'] = $locationInfoArr['country'];
                                        $locationArr[$rowNumber]['state'] = $locationInfoArr['state'];
                                        $locationArr[$rowNumber]['city'] = $locationInfoArr['city'];
                                        $locationArr[$rowNumber]['lat'] = number_format($locationInfoArr['lat'], 6);
                                        $locationArr[$rowNumber]['lng'] = number_format($locationInfoArr['lng'], 6);

                                        //$country = $locationInfoArr['country'];
                                        //$state = $locationInfoArr['state'];
                                        //$city = $locationInfoArr['city'];
                                        //$lat = $locationInfoArr['lat'];
                                        //$lng = $locationInfoArr['lng'];                                        
                                    }                                    
                                } else {                                    
                                    /*if(empty($objMassUpload->locality)) {
                                        $objMassUpload->addError('locality', 'Locality cannot be blank.');
                                        $hasError = 1;                                        
                                    }*/
                                    if(empty($objMassUpload->city)) {
                                        $objMassUpload->addError('city', 'City cannot be blank.');
                                        $hasError = 1;                                        
                                    }                                    
                                }
                            }
                        }
                        //echo '<pre>';print_r($objMassUpload->attributes); die();
                        // Validate Each Row 
                        if($hasError) {
                            $hasError = 1;
                            $modelArr = $objMassUpload->getErrors();
                            foreach($modelArr as $key => $val) {                                
                                $errorCell = array_search($key, $this->massExcelFields);
                                $errorStr .= $errorCounter." - Cell No. ". $errorCell . $rowNumber ." - ".$val[0]."<br />";
                                $errorCounter++;
                            }

                        }
                        //print_r($objMassUpload->getErrors());
                        usleep(250000);
                    }
                }
                
                if($_POST['userEmail']=='') {
                    $emailErrorFlag = 1;
                    $hasError = 1;
                }
                // data is validated and save in database
                
                
                if(!$hasError) {
                    // before that truncate table
                    Yii::app()->db->createCommand()->truncateTable(ListingDraft::model()->tableName());
                    for($rowNumber=2; $rowNumber<=$highestRowNumber; $rowNumber++) {
                        $objMassUpload = new ListingDraft();
                        $objMassUpload->setScenario('createListing');

                        foreach(range("A", "S") as $col) {
                            $attribute = $this->massExcelFields[$col]; 
                            $cellNumber = $col.$rowNumber;
                            
                            if($col == 'G' || $col == 'F'){
                                $cellObj = $objPHPExcel->getActiveSheet()->getCell($cellNumber);
                                // Lat Lng is not mandatory if lat lan is there then format_number otherwise not
                                if( $cellObj->getValue() != '' ) {
                                    $cellValue = number_format($cellObj->getValue(), 6);
                                } else {
                                    $cellValue = (NULL);
                                }
                            } else {
                                $cellObj = $objPHPExcel->getActiveSheet()->getCell($cellNumber);
                                $cellValue = $cellObj->getValue();
                            }

                            //echo $cellNumber . ' = ' . $cellValue . '<br />';
                            $objMassUpload->$attribute = $cellValue;
                        }
                        
                        $objMassUpload->audiencetag = implode(",", AudienceTag::getAudienceTag($objMassUpload->audiencetag));
                        $objMassUpload->byuserid = Yii::app()->user->id;
                        $objMassUpload->foruserid = $_POST['userEmail'];
                        $objMassUpload->country = $locationArr[$rowNumber]['country'];
                        $objMassUpload->state= $locationArr[$rowNumber]['state'];
                        $objMassUpload->city = $locationArr[$rowNumber]['city'];
                        $objMassUpload->accurate_geoloc = $locationArr[$rowNumber]['accurate_geoloc'];
                        $objMassUpload->geolat = $locationArr[$rowNumber]['lat'];
                        $objMassUpload->geolng = $locationArr[$rowNumber]['lng'];
                        $objMassUpload->datecreated = date("Y-m-d H:i:s");
                        $objMassUpload->datemodified = date("Y-m-d H:i:s");
                        //echo '<pre>'; print_r($objMassUpload->attributes); die();
                        $objMassUpload->save();                        
                    }
                    Yii::app()->user->setFlash('success', "Listing template has been uploaded successfully");
                    $this->redirect(Yii::app()->urlManager->createUrl('admin/listing/massview'));
                }
            }
        }
        
        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            'Dashboard' => JoyUtilities::getDashboardUrl(),
            //'Mass View' => Yii::app()->urlManager->createUrl('admin/listing/massview'),
            'Mass Upload',
        );

        
        $this->render('massupload', array('model'=>$model, 'errorStr' => $errorStr, 'userEmails'=>$userEmails, 'emailErrorFlag' => $emailErrorFlag));
    }
    
    public function getExcelHeaderRow() {
        $excelheader = array (
                            'A' => 'MEDIA TYPE',
                            'B' => 'COUNTRY',
                            'C' => 'STATE',
                            'D' => 'CITY',
                            'E' => 'LOCALITY',
                            'F' => 'LATITUDE',
                            'G' => 'LONGITUDE',
                            'H' => 'NAME',
                            'I' => 'LENGTH',
                            'J' => 'WIDTH',
                            'K' => 'SIZE UNIT',
                            'L' => 'BASE CURRENCY',
                            'M' => 'OTHER DATA',
                            'N' => 'LIGHTING',
                            'O' => 'DESCRIPTION',
                            'P' => 'REACH',
                            'Q' => 'AUDIENCE TAG',
                            'R' => 'PRICE',
                            'S' => 'PRICE DURATION',
                        );
        return $excelheader;
    }
    
    public function setMassExcelFields() {
        $this->massExcelFields = array (
                            'H' => 'name',
                            'I' => 'length',
                            'J' => 'width',
                            'K' => 'sizeunit',
                            'L' => 'basecurrency',
                            'R' => 'price',
                            'S' => 'priceduration',
                            'M' => 'otherdata',
                            'B' => 'country',
                            'C' => 'state',
                            'D' => 'city',
                            'E' => 'locality',
                            'F' => 'geolat',
                            'G' => 'geolng',
                            'N' => 'lighting',
                            'A' => 'mediatype',
                            'O' => 'description',
                            'P' => 'reach',
                            'Q' => 'audiencetag',
                        );
    }
    
    public function actionMassview()
    {        
        if(isset($_SERVER['HTTP_REFERER'])) {
            $pathParts = pathinfo($_SERVER['HTTP_REFERER']);            
            if($pathParts['basename'] == 'massupload' || $pathParts['basename'] == 'massview') {
                // from massupload, dont do anything
            } else {
                // truncate draft table
                Yii::app()->db->createCommand()->truncateTable(ListingDraft::model()->tableName());
            }
        } else {
            // truncate draft table
            Yii::app()->db->createCommand()->truncateTable(ListingDraft::model()->tableName());
        }
        
        if(isset($_POST['save'])) {            
            $listingData = ListingDraft::getAllListingDraftByUserId(Yii::app()->user->id);

            // if listing exist then Save
            if(count($listingData)) {
                $lightingArr = Listing::getLighting();
                $sizeunitArr = Listing::getSizeUnit();
                $pricedurationArr = Listing::getPriceDuration();
                
                foreach($listingData as $data) {
                    $listingObj = new Listing();
                    $listingObj->byuserid = $data->byuserid;
                    $listingObj->foruserid = $data->foruserid;
                    $listingObj->name = $data->name;
                    $listingObj->length = $data->length;
                    $listingObj->width = $data->width;
                    $listingObj->price = $data->price;
                    $listingObj->reach = $data->reach;
                    $listingObj->description = $data->description;
                    $listingObj->otherdata = $data->otherdata;
                    $listingObj->geolat = $data->geolat;
                    $listingObj->geolng = $data->geolng;
                    $listingObj->accurate_geoloc = $data->accurate_geoloc;
                    $listingObj->locality = $data->locality;
                    
                    //Fetch id of country, state and city
                    $countryId = Area::isCountryExist($data->country);
                    $stateId = Area::isStateExist($countryId, $data->state);
                    $cityId = Area::isCityExist($stateId, $data->city);
                    
                    $listingObj->countryid = $countryId;
                    $listingObj->stateid = $stateId;
                    $listingObj->cityid = $cityId;

                    // Set Fixed array element
                    $lightingId = array_search(ucfirst(strtolower($data->lighting)), $lightingArr);
                    $sizeunitId = array_search(ucfirst(strtolower($data->sizeunit)), $sizeunitArr);
                    $pricedurationId = array_search(ucfirst(strtolower($data->priceduration)), $pricedurationArr);
                    $listingObj->lightingid = $lightingId;
                    $listingObj->sizeunitid = $sizeunitId;
                    $listingObj->pricedurationid = $pricedurationId;
                    
                    
                    // Fetch id of media type and base currency
                    $baseCurrencyId = LookupBaseCurrency::isBaseCurrencyExist($data->basecurrency);
                    $mediaTypeId = MediaType::isMediaTypeExist($data->mediatype);
                    
                    $listingObj->basecurrencyid = $baseCurrencyId;
                    $listingObj->weeklyprice = Listing::calculateWeeklyPrice($data->price, $pricedurationId, $baseCurrencyId);
                    $listingObj->mediatypeid = $mediaTypeId;

                    $listingObj->datecreated = $data->datecreated;
                    $listingObj->datemodified = $data->datemodified;
                    
                    $listingObj->save();
                    $listingId = $listingObj->getPrimaryKey();
                    
                    // Insert Audiance tag
                    $tagArr = explode(",", $data->audiencetag);
                    if(count($tagArr)) {
                        foreach ($tagArr as $tag) {
                            $tagId = AudienceTag::getTagIdByName($tag);
                            $listAudTagModel = new ListingAudienceTag;
                            $listAudTagModel->listingid = $listingId;
                            $listAudTagModel->audiencetagid = $tagId;
                            $listAudTagModel->save();
                        }
                    }
                    
                    $rs = Yii::app()->db->createCommand("DELETE FROM `ListingDraft` WHERE id = {$data->id}")->query();
                    // add Listing default image
                    $listingImageModel = new ListingImage;
                    $listingImageModel->listingid = $listingId;
                    $listingImageModel->filename = 'default_listing.png';
                    //$listingImageModel->filename = '1395226617_1283896814.png';
                    $listingImageModel->save();
                    
                    // add to solr
                    Listing::updateSolr($listingId);                    
//                    $listingDraftModel = ListingDraft::model()->findByPk($data->id); 
//                    $listingDraftModel->status = 0;
//                    ListingDraft::model()->updateByPk($data->id, $listingDraftModel->attributes);
                }
                
                
                Yii::app()->user->setFlash('success', "Listing has been saved successfully");
            } else {
                Yii::app()->user->setFlash('success', "No listing to save");
            }
        }
        
        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            'Dashboard' => JoyUtilities::getDashboardUrl(),
            'Mass View',
        );
        
        $this->render('massview');
    }
    
   
    
        public function actionSignup() {        
        //check if logged in redirect to home
//        if(!Yii::app()->user->isGuest) {
//            $this->redirect(Yii::app()->user->returnUrl);
//        }
        
        $model = new User;
        $model->setScenario('createProfile');
        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-signup-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        $userAccountTypeModel = 1;
        $successModel = 0;
        $accountTypeErrorMsg = '';
        $hiddenAccountType = '';
        // collect user input data
        if (isset($_POST['User'])) {
            $_POST['User'] = JoyUtilities::cleanInput($_POST['User']);
            $model->attributes = $_POST['User'];            
            if(empty($_POST['User']['hiddenAccountType'])) {
                // User account type not selected
                $userAccountTypeModel = 1;
                $accountTypeErrorMsg = "Please select the account type to complete Sign up.";                
            } else {
                $userAccountTypeModel = 0;
            }
            
            if (!$userAccountTypeModel && $model->validate()) {                
                // create user model to save record
                //$userModel = new User;
                $model->attributes = $_POST['User'];
                    
                // encrypt password                
                $ph = new PasswordHash(Yii::app()->params['phpass']['iteration_count_log2'], Yii::app()->params['phpass']['portable_hashes']);
                
                $model->password = $ph->HashPassword($model->password);
                // as discussed on 25-02-0214 3:30 pm IST (Pooja Ghulam)
                $model->active = 1;
                $model->datecreated = date("Y-m-d H:i:s");                                
                // save user registration
                $model->save(false);    // false for not validating again                
                $userId = $model->primaryKey;
                // Save to User Role table
                $userRoleModel = new UserRole;
                $userRoleModel->attributes = array('userid'=>$userId, 'roleid'=> (int)$_POST['User']['hiddenAccountType']);
                $userRoleModel->save();
                $linkModel = new Link;
                // generate the hash
                $hash = sha1(uniqid());
                $linkModel->attributes = array('userid'=>$userId, 'hash'=> $hash, 'type'=> 1, 'datecreated'=>date('Y-m-d H:i:s'));                
                if($linkModel->save()) {
                    
                    // subscribe to newsletter
                    $MailChimp = new MailChimp(Yii::app()->params['mailChimp']['api_key']);
                    if($model->subscribe == 1) {
                        $result = $MailChimp->call('lists/subscribe', array(
                                                'id' => Yii::app()->params['mailChimp']['id'],
                                                'email' => array('email'=>$model->email)                                             
                                            ));
                    } else {
                        $result = $MailChimp->call('lists/unsubscribe', array(
                                                'id' => Yii::app()->params['mailChimp']['id'],
                                                'email' => array('email'=>$model->email)                                                
                                            ));
                    }
                    
                     // show success message
                    $userMessage =  'Please check your email to activate your account.';
                    $successModel = 1;
                    // sign up flow changed, login user 
                    $identity=new LoginForm();
                    $identity->loginWithoutPassword($model->email);

                    // send reset pwd link email to user
                    $resetLink = Yii::app()->createAbsoluteUrl('account/activateuser',array('code'=>$hash));
                    $mail = new EatadsMailer('sign-up', $model->email, array('resetLink'=>$resetLink));
                    $mail->eatadsSend();
                    $mail = new EatadsMailer('sign-up-admin', Yii::app()->params['adminEmail'], array('userType' => JoyUtilities::getUserRole($userId), 
                                                                                                        'fname' => $_POST['User']['fname'], 
                                                                                                        'lname' => $_POST['User']['lname']));
                    $mail->eatadsSend();
                   
                    
                    /*if(mail($to, $subject, $message, $headers)){ 
                        // show success message
                        $userMessage =  'Please check your email to activate your account.';
                        $successModel = 1;
                        // sign up flow changed, login user 
                        $identity=new LoginForm();
                        $identity->loginWithoutPassword($model->email);
                        
                    } else {
                        // show error message
                        $userMessage =  'Could not send email to user.';
                    } */                   
                } else {                    
                    // show error message
                    $userMessage =  'Could not send email to user.';
                }
                // redirect to the previous page if valid
                // $this->redirect(Yii::app()->user->returnUrl);
                $model->unsetAttributes();
            }
        } else {            
            $accountType = Yii::app()->request->getQuery('type');
            if(strtolower($accountType) == 'vendor') {
                $userAccountTypeModel = 0; 
                $hiddenAccountType = 3;
            } else if(strtolower($accountType) == 'buyer') {
                $userAccountTypeModel = 0;
                $hiddenAccountType = 2;
            }
        }        
        // display the signup form        
//        $this->render('signup', array('model' => $model, 'userAccountTypeModel'=>$userAccountTypeModel, 'accountTypeErrorMsg'=>$accountTypeErrorMsg, 
//            'hiddenAccountType'=>isset($_POST['User']['hiddenAccountType'])?$_POST['User']['hiddenAccountType']:'', 'successModel'=>$successModel));
        $this->render('signup', array('model' => $model, 'userAccountTypeModel'=>$userAccountTypeModel, 'accountTypeErrorMsg'=>$accountTypeErrorMsg, 
            'hiddenAccountType'=>isset($_POST['User']['hiddenAccountType'])?$_POST['User']['hiddenAccountType']:$hiddenAccountType, 'successModel'=>$successModel));
        
       // $this->render('signup1');
    }
    
    public function actionMassUploadNew() {
        //print_r("test");die();
        $errorStr = '';
        $userEmails = User::getOwnerEmail();
        $emailErrorFlag = 0;
        $this->render('massuploadnew', array('errorStr' => $errorStr, 'userEmails'=>$userEmails,'emailErrorFlag' => $emailErrorFlag));
    }
    
}
