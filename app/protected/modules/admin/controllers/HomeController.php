<?php

class HomeController extends Controller {

    //public $weeklyArr = array("Mon" => 0, "Tue" => 0, "Wed" => 0, "Thu" => 0, "Fri" => 0, "Sat" => 0, "Sun" => 0);
    
    public function init() {
        Yii::app()->theme = 'admin';
        $this->layout = "//layouts/admin_page";
    }
    
    
    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform actions
                'actions' => array('index', 'updatestatics', 'getchart'),
                'users' => array('@'),
                'roles' => array(JoyUtilities::ROLE_ADMIN),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionIndex() {

        //$yearArr = JoyUtilities::getNoOfYear('2012-05-01', '2014-02-20');
        
        $endDate = date("Y-m-d 23:59:59");
        $startDate = date("Y-m-d 00:00:00" , strtotime(date("Y-m-d 00:00:00")) - 6*24*60*60); // data of before 7 days
                
        $endDateUser = date("Y-m-d 23:59:59");
        $startDateUser = date("Y-m-d 00:00:00" , strtotime(date("Y-m-d 00:00:00")) - 30*24*60*60); // data of before 30 days
        $days = 30;
        
        $listingCount = Listing::getListCountByDateRange($startDate, $endDate);
        $buyerCount = User::getRoleIdBasedCountByDateRange($startDate, $endDate, 2);
        $mediaOwnerCount = User::getRoleIdBasedCountByDateRange($startDate, $endDate, 3);
        $signCount = User::getSignUpCountByDateRange($startDate, $endDate);
        $rfpSentCount = RfpLog::getSentRfpCountByDateRange($startDate, $endDate);
        $favListingCount = FavouriteListing::getFavListingCountByDateRange($startDate, $endDate);
        $newMediaPlanCount = Plan::getNewMediaPlanCountByDateRange($startDate, $endDate);
        $mediaPlanModifyCount = Plan::getMediaPlanModifyCountByDateRange($startDate, $endDate);
        $activeUserCount = User::getActiveUserCountByDateRange($startDateUser, $endDateUser);
        $inactiveUserCount = User::getInactiveUserCountByDefault($startDateUser, $endDateUser);
        
        
        $dashboardCount[1] = $listingCount;
        $dashboardCount[2] = $buyerCount;
        $dashboardCount[3] = $mediaOwnerCount;
        $dashboardCount[4] = $signCount;
        $dashboardCount[5] = $rfpSentCount;
        $dashboardCount[6] = $favListingCount;
        $dashboardCount[7] = $newMediaPlanCount;
        $dashboardCount[8] = $mediaPlanModifyCount;
        $dashboardCount[9] = $activeUserCount;
        $dashboardCount[10] = $inactiveUserCount;

        
        // for first time chart laod show listing weekly data for current year
        $graphEndDate = date("Y-m-d 23:59:59");
        $graphStartDate = date("Y-m-d 00:00:00" , strtotime(date(date("Y")."-01-01 00:00:00"))); // 

        $weeklyListingData = Listing::getListCountWeeklyByDateRange($graphStartDate, $graphEndDate);
        $labelArr = json_encode($weeklyListingData[0]);
        $chartMax = max($weeklyListingData[1]);
        $dataPoint = json_encode($weeklyListingData[1]);
        
        $this->pageTitle = "Eatads.com - Admin's Dashboard";
        $this->render('index', array(
            'dashboardCount' => $dashboardCount,
            'days' => $days,
            'labelArr' => $labelArr,
            'dataPoint' => $dataPoint,
            'chartMax' => $chartMax
            
        ));
    }
    
    public function actionUpdatestatics() {

        $startDate = Yii::app()->request->getParam('startDate');
        $endDate = Yii::app()->request->getParam('endDate');
        
        $endDate = date("Y-m-d 23:59:59", strtotime($endDate));
        $startDate = date("Y-m-d 00:00:00", strtotime($startDate));
       
        $endDay = strtotime($endDate); // or your date as well
        $startDay = strtotime($startDate);
        $datediff = $endDay - $startDay;
        $days = ceil($datediff/(60*60*24));
        
        $listingCount = Listing::getListCountByDateRange($startDate, $endDate);
        $buyerCount = User::getRoleIdBasedCountByDateRange($startDate, $endDate, 2);
        $mediaOwnerCount = User::getRoleIdBasedCountByDateRange($startDate, $endDate, 3);
        $signCount = User::getSignUpCountByDateRange($startDate, $endDate);
        $rfpSentCount = RfpLog::getSentRfpCountByDateRange($startDate, $endDate);
        $favListingCount = FavouriteListing::getFavListingCountByDateRange($startDate, $endDate);
        $newMediaPlanCount = Plan::getNewMediaPlanCountByDateRange($startDate, $endDate);
        $mediaPlanModifyCount = Plan::getMediaPlanModifyCountByDateRange($startDate, $endDate);
        $activeUserCount = User::getActiveUserCountByDateRange($startDate, $endDate);
        $inactiveUserCount = User::getInactiveUserCountByDateRange($startDate, $endDate);
        
        
        $dashboardCount[1] = $listingCount;
        $dashboardCount[2] = $buyerCount;
        $dashboardCount[3] = $mediaOwnerCount;
        $dashboardCount[4] = $signCount;
        $dashboardCount[5] = $rfpSentCount;
        $dashboardCount[6] = $favListingCount;
        $dashboardCount[7] = $newMediaPlanCount;
        $dashboardCount[8] = $mediaPlanModifyCount;
        $dashboardCount[9] = $activeUserCount;
        $dashboardCount[10] = $inactiveUserCount;
        
        echo $this->renderPartial('_statics', array('days' => $days, 'dashboardCount' => $dashboardCount, 'ajax' => 'ajax'));
    }

    public function actionGetchart() {

        $startDate = Yii::app()->request->getParam('startDate');
        $endDate = Yii::app()->request->getParam('endDate');
        $chartfor = Yii::app()->request->getParam('chartfor');
        $duration = Yii::app()->request->getParam('duration');
        
        // format date range if given
        if($endDate && $startDate) {
            $endDate = date("Y-m-d 23:59:59", strtotime($endDate));
            $startDate = date("Y-m-d 00:00:00", strtotime($startDate));
        } else { // if start or end date is missing then show weekly data
            if($duration == "1" || $duration == "2") { // Weekly - current year all week, Monthly - current year all month
                $endDate = date("Y-m-d 23:59:59");
                $startDate = date("Y-m-d 00:00:00" , strtotime(date(date("Y")."-01-01 00:00:00")));
            } elseif($duration == "3") { // Yearly - curren year and previous year
                $endDate = date("Y-m-d 23:59:59");
                $lastYear = date("Y")-1;
                $startDate = date("Y-m-d 00:00:00" , strtotime(date($lastYear."-01-01 00:00:00")));
            }
        }

        switch($chartfor) {
            case 'listings': 
                    $listingChartData = Listing::getListingChartData($startDate, $endDate, $duration);
                    $labelArr = $listingChartData[0];
                    $chartMax = max($listingChartData[1]);
                    $dataPoint = $listingChartData[1];
                break;
            case 'buyers': 
                    $buyerChartData = User::getRolebasedChartData($startDate, $endDate, $duration, 2);
                    $labelArr = $buyerChartData[0];
                    $chartMax = max($buyerChartData[1]);
                    $dataPoint = $buyerChartData[1];
                break;
            case 'sellers': 
                    $sellerChartData = User::getRolebasedChartData($startDate, $endDate, $duration, 3);
                    $labelArr = $sellerChartData[0];
                    $chartMax = max($sellerChartData[1]);                    
                    $dataPoint = $sellerChartData[1];
                break;
            case 'Signups': 
                    $signupChartData = User::getSignupChartData($startDate, $endDate, $duration);
                    $labelArr = $signupChartData[0];
                    $chartMax = max($signupChartData[1]); 
                    $dataPoint = $signupChartData[1];
                break;
            case 'rfps': 
                    $rfpChartData = RfpLog::getRfpChartData($startDate, $endDate, $duration);
                    $labelArr = $rfpChartData[0];
                    $chartMax = max($rfpChartData[1]);
                    $dataPoint = $rfpChartData[1];
                break;
            case 'Favourited': 
                    $favChartData = FavouriteListing::getFavListingChartData($startDate, $endDate, $duration);
                    $labelArr = $favChartData[0];
                    $chartMax = max($favChartData[1]);
                    $dataPoint = $favChartData[1];
                break;
            case 'mediaplans': 
                    $mediaPlanChartData = Plan::getMediaPlanChartData($startDate, $endDate, $duration);
                    $labelArr = $mediaPlanChartData[0];
                    $chartMax = max($mediaPlanChartData[1]);
                    $dataPoint = $mediaPlanChartData[1];
                break;
            case 'mediaplansmod': 
                    $modifyMediaPlanChartData = Plan::getMediaPlanModifyChartData($startDate, $endDate, $duration);
                    $labelArr = $modifyMediaPlanChartData[0];
                    $chartMax = max($modifyMediaPlanChartData[1]);
                    $dataPoint = $modifyMediaPlanChartData[1];
                break;
            case 'active': 
                    $loggedinUserChartData = User::getLoggedinUserChartData($startDate, $endDate, $duration);
                    $labelArr = $loggedinUserChartData[0];
                    $chartMax = max($loggedinUserChartData[1]);
                    $dataPoint = $loggedinUserChartData[1];
                break;
            case 'inactive': 
                    if($duration == "1") { // Weekly
                        
                    } elseif($duration == "2") { // Monthly
                        
                    } elseif($duration == "3") { // Yearly
                        
                    }                
                break;
        }
        
        echo json_encode(array('labelArr' => $labelArr, 'dataPoint' => $dataPoint, 'chartMax' => $chartMax));
    }    
    
}
