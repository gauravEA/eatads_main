<?php

class ProfileController extends Controller {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }
    
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform actions
                'actions' => array('edit', 'deleteLogo'),
                'users' => array('@'),
                'roles'=>array(JoyUtilities::ROLE_MEDIA_BUYER, JoyUtilities::ROLE_MEDIA_OWNER, JoyUtilities::ROLE_THIRD_PARTY),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    public function actionEdit() {
        $userId = Yii::app()->user->id;
        if(Yii::app()->user->isGuest) {
            $this->redirect(Yii::app()->urlManager->createUrl('account/login'));
        }

        $userModel = new User();
        $userModel = User::model()->findByPk($userId); 
        $userModel->setScenario('editProfile');
        
        $userCompanyModel = new UserCompany('editProfile');
        $userCompanyData = UserCompany::getCompanyDataByUserId($userId); // Get user company details
        
        $previousLogo = null;
        if($userCompanyData) {
            $previousLogo = $userCompanyData->logo;
        }
        
        // check user company reocrd already exist or not
        if($userCompanyData) {
            $userCompanyModel = $userCompanyData;
        }
        $userCompanyModel->setScenario('editProfile');
        
        if(isset($_POST['User'])){
            $_POST['User'] = JoyUtilities::cleanInput($_POST['User']);
            $userModel->fname = $_POST['User']['fname'];
            $userModel->lname = $_POST['User']['lname'];
            $userModel->phonenumber = $_POST['User']['phonenumber'];
            $userModel->datemodified = date('Y-m-d H:i:s');
            
            if ($userModel->validate()) {
                // Only update password when password is give
                if($_POST['User']['password'] != '' && $_POST['User']['confirmPassword'] != '' && ($_POST['User']['password'] == $_POST['User']['confirmPassword']) ){
                    $passHash = new PasswordHash(Yii::app()->params['phpass']['iteration_count_log2'], Yii::app()->params['phpass']['portable_hashes']);
                    $userModel->password = $passHash->HashPassword($_POST['User']['password']);
                }
                
                User::model()->updateByPk($userId, $userModel->attributes);
                Yii::app()->user->setFlash('success', "Account Details has been saved successfully.");
                
                // Update name and lname in state variable
                Yii::app()->user->setState('name', ucfirst(strtolower($_POST['User']['fname']))); 
                Yii::app()->user->setState('lname', ucfirst(strtolower($_POST['User']['lname'])));
                // $this->redirect(Yii::app()->urlManager->createUrl('user/profile/edit'));                
                $this->redirect(JoyUtilities::getDashboardUrl().'/profile');
            }

        }
        
        if(isset($_POST['UserCompany'])) {
           $_POST['UserCompany'] = JoyUtilities::cleanInput($_POST['UserCompany']);
           $userCompanyModel->attributes = $_POST['UserCompany'];
           $userCompanyModel->logo = CUploadedFile::getInstance($userCompanyModel, 'logo');
           
           $userCompanyModel->validate();           
           $hasError = count($userCompanyModel->getErrors());
           //print_r($userCompanyModel->getErrors());
           //die($hasError);
           if($userCompanyModel->logo && !$hasError) {               
               list($width, $height) = getimagesize($userCompanyModel->logo->tempName);
               if( $width < 100 || $height < 100 ){
                   $userCompanyModel->addError('logo', 'Logo must be minimum of 100x100 and maximum of 500x500');
                   $hasError = 1;
               }
               if( $width > 500 || $height > 500 ){
                   $userCompanyModel->addError('logo', 'Logo must be minimum of 100x100 and maximum of 500x500');
                   $hasError = 1;
               }
           }
           
            // validate user input 
            if ( !$hasError) {                
                //$userCompanyModel = new UserCompany;
                $userCompanyModel->attributes = $_POST['UserCompany'];
                $userCompanyModel->userid = $userId;  
                
                
                if($userCompanyModel->logo) { // when user upload his logo
                    $ext = pathinfo($userCompanyModel->logo->name, PATHINFO_EXTENSION);
                    $newFileName = time()."_".$userId.'.' . $ext;
                    $uploadFilePath = Yii::app()->params['fileUpload']['path'] . 'companylogo/'.$newFileName;
                    $userCompanyModel->logo->saveAs($uploadFilePath);

                    // Create Thumb
                    $imageThumb = new EasyImage($uploadFilePath);
                    $imageThumb->resize(100, 100);
                    $imageThumb->save($uploadFilePath);

                    // Upload on Amazon S3
                    $s3Obj = new EatadsS3();
                    $s3Obj->uploadFile($uploadFilePath, 'companylogo/' . $newFileName);
                    @unlink($uploadFilePath);
                    
                    JoyUtilities::deleteAwsFile($previousLogo, 'companylogo');
                    
                    $userCompanyModel->logo = $newFileName;
                } else { // if user is not uploading new image then keep previous logo image
                    $userCompanyModel->logo = $previousLogo;
                }
                
                if($userCompanyModel->id) {
                    $userCompanyModel->alias = UserCompany::companyNameAlias(JoyUtilities::createAlias($_POST['UserCompany']['name']), $userCompanyModel->id);
                    UserCompany::model()->updateByPk($userCompanyModel->id, $userCompanyModel->attributes, 'userid = '.$userId);
                } else {
                    $userCompanyModel->alias = UserCompany::companyNameAlias(JoyUtilities::createAlias($_POST['UserCompany']['name']));
                    $userCompanyModel->save();
                }
                Yii::app()->user->setFlash('success', "Company Profile has been saved successfully.");

                // Reload existing page
                // $this->redirect(Yii::app()->urlManager->createUrl('user/profile/edit'));
                $this->redirect(JoyUtilities::getDashboardUrl().'/profile');
            } 
        }
        
               
        $countryList = $stateList = $cityList = array();
        $countryList = Area::model()->getCountryOptions();
        
        $countryId = $userCompanyModel->countryid;
        if($countryId) {
            $stateList = Area::model()->getStateOptions($countryId);
        }
        
        $stateId = $userCompanyModel->stateid;
        if($stateId) {
            $cityList = Area::model()->getCityOptions($stateId);
        }
        
        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            'Dashboard' => JoyUtilities::getDashboardUrl(),
            'Edit Your Profile',
        );

        $this->render('index', array(
            'userModel' => $userModel,
            'userCompanyModel' => $userCompanyModel,
            'countryList' => $countryList,
            'stateList' => $stateList,
            'cityList' => $cityList,
            'previousLogo' => $previousLogo,
        ));
    }
    
    public function actionDeleteLogo() {
        $id = Yii::app()->request->getPost('cid');
        $fileName = Yii::app()->request->getPost('filename');
        $result = UserCompany::deleteCompanyLogoByIdAndFileName($id, $fileName);
        if($result == 1) {
            JoyUtilities::deleteAwsFile($fileName, 'companylogo');
            echo "1";
        } else {
            echo "0";
        }
    }

}