<?php

class FavouritesController extends Controller {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform actions
                'actions' => array('index', 'removeFav'),
                'users' => array('@'),
                'roles'=>array(JoyUtilities::ROLE_MEDIA_BUYER, JoyUtilities::ROLE_THIRD_PARTY),
                'expression' => array('WebUser', 'allowActiveUser'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    public function actionIndex() {
        $this->pageTitle= Yii::app()->name . ' - Favorites';
        Yii::app()->openexchanger->exchangeRates;
        $userId = Yii::app()->user->id;
        if (Yii::app()->user->isGuest) {
            $this->redirect(Yii::app()->urlManager->createUrl('account/login'));
        }
        $listType = trim(Yii::app()->request->getQuery('lt'));
        
        $searchText = Yii::app()->request->getQuery('textsearch');
        
        if($listType=='m') {
            $renderView = '_mosaicView';
        } else { 
            $renderView = '_listView';
        }
        
        $criteria = new CDbCriteria;
        
        $criteria->with = array('listing', 'listing.mediatype', 'listing.basecurrency', 'listing.listingImages');
        $criteria->compare('listing.name', $searchText, true, 'OR');
        $criteria->compare('listing.description', $searchText, true, 'OR');
        $criteria->compare('mediatype.name', $searchText, true, 'OR');
        $criteria->addCondition('userid = '.$userId);
        $criteria->order ='t.datecreated DESC';
        
        $dataProvider = new CActiveDataProvider('FavouriteListing', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->params['records_per_page'],
            ),            
        ));

        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            'Dashboard' => JoyUtilities::getDashboardUrl(),
            'Favorites',
        );

        
        if(Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('_favGrid', array('dataProvider' => $dataProvider, 'listType' => $listType, 'renderView' => $renderView) );
        } else {
            $this->render('index', array(
                    'dataProvider' => $dataProvider,
                    'listType' => $listType,
                    'renderView' => $renderView,
            ));
        }
    }
    
    public function actionRemoveFav(){
        $id = Yii::app()->request->getPost('id');
        $userId = Yii::app()->user->id;
        $count = FavouriteListing::model()->deleteByPk($id, 'userid = '.$userId);
        echo $count;
        Yii::app()->end();
    }

}