<?php

class DashboardController extends Controller {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform actions
                'actions' => array('index'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }    
    
    public function actionIndex() {
        $userId = Yii::app()->user->id;
        if (Yii::app()->user->isGuest) {
            $this->redirect(Yii::app()->urlManager->createUrl('account/login'));
        }

        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            'Dashboard'
        );
        
        
        $roleId = Yii::app()->user->roleId;
        if ($roleId == 2) { //Media Buyer
            $this->pageTitle =  "EatAds.com - Media Buyer's Dashboard";
            //$countryArr = Area::getPriorityCountryList();
            
            // get rfp count
            $rfpCount = RfpLog::getRfpCountByUserId($userId);            
            
            // get media plan count
            $mediaPlanCount = Plan::getPlanCountByUserId($userId);            
            
            // get favorite listing count
            $favListingCount = FavouriteListing::getFavListingCountByUserId($userId);
            
            // Render Buyer View
            $this->render('buyer', array('rfpCount'=>$rfpCount, 'mediaPlanCount'=>$mediaPlanCount, 'favListingCount'=>$favListingCount));
            
        } elseif ($roleId == 3) { //Media Owner 
            // get the list of media types
            $mediaTypeList = MediaType::model()->findAll(array('select'=>'id,name'));

            $this->pageTitle =  "EatAds.com - Media Owner's Dashboard";            
            // get active listing count
            $listingCount = Listing::getListingCountByUserId($userId, 'active');

            // Render Owner View
            $this->render('owner', array('listingCount'=>$listingCount, 'mediaTypeList'=>$mediaTypeList));
        } elseif ($roleId == 4) { //Third Party
            $this->pageTitle =  "EatAds.com - Service Provider's Dashboard";
            
            // Render Owner View
            $this->render('serviceProvider');

        }
    }

}