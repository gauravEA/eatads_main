<?php

class MediaplansController extends Controller {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'addplan', 'removeMediaPlan', 'sendrfp', 'addInExistingPlan'),
                'users' => array('@'),
                'roles' => array(JoyUtilities::ROLE_MEDIA_BUYER),
                'expression' => array('WebUser', 'allowActiveUser'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $userId = Yii::app()->user->id;
        Yii::app()->openexchanger->exchangeRates;

        $planDetail = Plan::getPlanByUserId($userId);
        
        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            'Dashboard' => JoyUtilities::getDashboardUrl(),
            'Media Plans',
        );

        
        $this->render('index', array(
            'planData' => $planDetail,
        ));
    }

    public function actionSendrfp() {
        $this->pageTitle= Yii::app()->name . ' - Send RFP Mediaplans';
        $userId = Yii::app()->user->id;
        // $planId = Yii::app()->request->getQuery('id');
        $planId = Yii::app()->request->getQuery('planid');
        
        if (isset($_POST['sendRfp'])) {
            $buyerDetail = User::getUserAttributeById($userId, 'fname, lname, email, phonenumber');

            $everyOneComment = Yii::app()->request->getPost('everyonecomment');
            $comments = Yii::app()->request->getPost('comment');
            $ownerComment = $comments['owner'];

            //if noting is entered in comment section we track with isRfpSend variable
            $isRfpSend = false;
            foreach($ownerComment as $ownerId => $commentForOwner) {
                $hasComment = false;
                //if($everyOneComment || $commentForOwner ) {
                
                if($everyOneComment || $commentForOwner ) {
                    $hasComment = true;
                }
                $listingDetails = '';

                $ownerDetail = User::getUserAttributeById($ownerId, 'fname, lname, email');

                $listingsData = PlanListing::getPlanListingByPlanId($_GET['id'], $ownerId);
                foreach($listingsData as $listing) {
                    $listingComment = $comments[$ownerId]['listing'][$listing['listingid']];
                    if(strlen($listingComment)) {
                        $hasComment = true;
                    }
                    $listingComment = strlen($listingComment) > 0 ? nl2br($listingComment)."<br><br>" : '';
                    $listingDetails .= "<a href='".Yii::app()->createAbsoluteUrl('listing/view', array('id'=>$listing['listingid']))."'>".$listing['name']."</a><br>".$listingComment;
                }
                
                if($hasComment) {
                    $isRfpSend = true;
                    $mailData = array(
                                'ownerFname' => ucfirst(strtolower($ownerDetail->fname)),
                                'buyerFname' => ucfirst(strtolower($buyerDetail->fname)),
                                'generalComment' => strlen($everyOneComment) > 0 ? nl2br($everyOneComment)."<br><br>" : '',
                                'mediaOwnerComment' => strlen($commentForOwner) > 0 ? nl2br($commentForOwner)."<br><br>" : '',
                                'buyerFname' => ucfirst(strtolower($buyerDetail->fname)),
                                'buyerLname' => ucfirst(strtolower($buyerDetail->lname)),
                                'buyerPhone' => $buyerDetail->phonenumber,
                                'buyerEmail' => "<a href='mailto:". $buyerDetail->email ."'>".$buyerDetail->email."</a>",
                                'listingDetailComment' => $listingDetails,
                    );

                    $mail = new EatadsMailer('send-rfp', $ownerDetail->email, $mailData, Yii::app()->params['bccEmail']);
                    $mail->eatadsSend();
                    Yii::app()->user->setFlash('success', "RFP has been sent successfully.");
                }
                //}
            }
            if(!$isRfpSend) {
                Yii::app()->user->setFlash('success', "Please enter any comment in order to send RFP.");
            } else { // log send rfp
                $rfpLogModel = new RfpLog();
                $rfpLogModel->planid = $planId;
                $rfpLogModel->datecreated = date('Y-m-d H:i:s');
                $rfpLogModel->save();
            }
        }
        
        Yii::app()->openexchanger->exchangeRates;
        
        $planDetail = Plan::getPlanById($planId);
        //print_r($planDetail->name);
        
        $ownerNames = PlanListing::getPlanOwnerName($planId);
        
        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            'Dashboard' => JoyUtilities::getDashboardUrl(),
            'Media Plans' => Yii::app()->UrlManager->createUrl('user/mediaplans'),
            'Send RFP',
        );

        
        $this->render('rfp', array(
            'planData' => $planDetail,
            'ownerNames' => $ownerNames,
        ));
    }
    
    public function actionAddplan() {

        $userId = Yii::app()->user->id;
        $name = Yii::app()->request->getPost('planName');
        $listingId = Yii::app()->request->getPost('listingId');
        
        if($userId && $name && $listingId) {
            $planModel = new Plan();
            $planModel->userid = $userId;
            $planModel->name = $name;
            $planModel->status = 1;
            $planModel->datecreated = date('Y-m-d H:i:s');
            //$planModel->datemodified = date('Y-m-d H:i:s');
            $planModel->save();
            echo $planId = $planModel->getPrimaryKey();


            $planListingModel = new PlanListing();
            $planListingModel->planid = $planId;
            $planListingModel->listingid = $listingId;
            $planListingModel->status = 1;
            $planListingModel->datecreated = date('Y-m-d H:i:s');
            $planListingModel->datemodified = date('Y-m-d H:i:s');
            $planListingModel->save();
        }
        
    }
    
    public function actionAddInExistingPlan() {
        $userId = Yii::app()->user->id;
        $planId = Yii::app()->request->getPost('planId');
        $listingId = Yii::app()->request->getPost('listingId');

        $response = PlanListing::isPlanListingExist($planId, $listingId);
        if($response) {
            echo "error";
            exit;
        }
        
        if(!$response && $userId && $planId && $listingId) {
            $planListingModel = new PlanListing();
            $planListingModel->planid = $planId;
            $planListingModel->listingid = $listingId;
            $planListingModel->status = 1;
            $planListingModel->datecreated = date('Y-m-d H:i:s');
            $planListingModel->datemodified = date('Y-m-d H:i:s');
            $planListingModel->save();
            
            // Show report in admin updating plan modify date
            $planModel = Plan::model()->findByPk($planId);
            $planModel->datemodified = date('Y-m-d H:i:s');
            $planModel->save();
        }
    }
    
    public function actionRemoveMediaPlan() {
        $listingId = Yii::app()->request->getPost('lid');
        $planId = Yii::app()->request->getPost('pid');
        $planListingId = Yii::app()->request->getPost('plid');
        
        $userId = Yii::app()->user->id;
        
        $count = PlanListing::model()->deleteByPk($planListingId, 'planid = ' . $planId.' AND listingid = '.$listingId);
        Yii::app()->user->setFlash('success', "Listing from plan has been removed successfully");
        echo $count;
        Yii::app()->end();
    }
}