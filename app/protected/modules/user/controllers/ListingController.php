<?php

class ListingController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $massExcelFields;
    
    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform actions
                //'actions' => array('massview', 'massupload', 'create', 'view', 'update', 'delete', 'index', 'manage', 'uploadedImage', 'modelSaveImage'),
                'actions' => array('create', 'view', 'update', 'delete', 'index', 'manage', 'uploadedImage', 'modelSaveImage'),
                'users' => array('@'),
                'roles'=>array(JoyUtilities::ROLE_MEDIA_OWNER),
                'expression' => array('WebUser', 'allowActiveUser'),
            ),
            
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {        
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
    * Creates a new model.
    * If creation is successful, the browser will be redirected to the 'view' page.
    */
    public function actionCreate() 
    {
        $this->pageTitle= Yii::app()->name . ' - Add Listing';
        $uploaded_filenames = '';
        $selectDisableFlag = $accurateGeoloc = 0;
        $alreadyUploadedArr = array();
        $countryList = array();
        $stateList = array();
        $cityList = array();
        $model = new Listing;
        $model->setScenario('createListing');
        $audTagModel = new AudienceTag;
        $audTagModel->setScenario('addlisting');
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        
        if (isset($_POST['Listing'])) {
            //echo '<pre>'; var_dump($_REQUEST); die();
            $_POST['Listing'] = JoyUtilities::cleanInput($_POST['Listing']);
            $selectDisableFlag = $_POST['select_disable_flag'];
            $model->attributes = $_POST['Listing'];
            $audTagModel->attributes = $_POST['AudienceTag'];
            $hasError = false;

            if (!$audTagModel->validate()) {
                $hasError = true;
            }
            if (!$model->validate()) {
                $hasError = true;
            }
            
            // match lat lng and country/state/city
            /*if(!empty($model->geolat) && !empty($model->geolng)) {
                $accurateGeoloc = 1;
                $locationInfoArr = JoyUtilities::reverseGeocode($model->geolat, $model->geolng);
                $revGeoArr = array('country' => strtolower($locationInfoArr['country']),
                                    'state' => strtolower($locationInfoArr['state']),
                                    'city' => strtolower($locationInfoArr['city']));
                $dropdownLocationArr = array(
                    'country' => strtolower(Area::model()->getAreaName($model->countryid)),
                    'state' => strtolower(Area::model()->getAreaName($model->stateid)),
                    'city' => strtolower(Area::model()->getAreaName($model->cityid))
                );                
                if(count(array_diff($revGeoArr, $dropdownLocationArr))) {
                    $model->addError('geolng', 'Selected locations and the Latitude Longitude provided are different. Please enter valid location details.');
                    $hasError = true;
                }
            }*/
            
            $locationArr = array();
            $checkWithLocalityCity = false;
            // if lat long available
            if(!empty($model->geolat) && !empty($model->geolng)) {
                echo 'lat lng provided<br />';
                $accurateGeoloc = 1;
                // get the country state city and save in db
                $locationInfoArr = JoyUtilities::reverseGeocode($model->geolat, $model->geolng);
                if(!isset($locationInfoArr['country']) || empty($locationInfoArr['country']) 
                    || !isset($locationInfoArr['state']) || empty($locationInfoArr['state'])
                    || !isset($locationInfoArr['city']) || empty($locationInfoArr['city'])) {
                    // possibly border case, try with location + city
                    $checkWithLocalityCity = true;
                    echo 'loc from lat lng failed<br />';
                } else {
                    Area::model()->setLocationArray($locationInfoArr);   // to update/add location in db
                    $locationArr['countryid'] = Area::checkAreaExists($locationInfoArr['country'], 'c');
                    $locationArr['stateid'] = Area::checkAreaExists($locationInfoArr['state'], 's');
                    $locationArr['cityid'] = Area::checkAreaExists($locationInfoArr['city'], 'ci');
                    $locationArr['geolat'] = number_format($model->geolat, 6);
                    $locationArr['geolng'] = number_format($model->geolng, 6);
                    $locationArr['accurate_geoloc'] = 1;
                    //$country = $locationInfoArr['country'];
                    //$state = $locationInfoArr['state'];
                    //$city = $locationInfoArr['city'];
                    //$accurateGeoloc = 1;
                    $checkWithLocalityCity = false;
                    echo 'loc from lat lng fetched<br />';
                }
            } else {
                echo 'lat lng not provided';
                $checkWithLocalityCity = true;
            }
            if($checkWithLocalityCity) {
                $city = Area::model()->getAreaName($model->cityid);
                if(!empty($city)) {
                    // reverse geocode to get lat long, from locality and city
                    $locationArr['accurate_geoloc'] = 0;
                    //$accurateGeoloc = 0;
                    // first try with locality + city
                    $address = $city;
                    $address .= empty($model->locality) ? '' : ','.$model->locality;

                    $locationInfoArr = JoyUtilities::geocode($address);
                    
                    if(!isset($locationInfoArr['country']) || empty($locationInfoArr['country']) 
                        || !isset($locationInfoArr['state']) || empty($locationInfoArr['state'])
                        || !isset($locationInfoArr['city']) || empty($locationInfoArr['city'])
                        || !isset($locationInfoArr['lat']) || empty($locationInfoArr['lat'])
                        || !isset($locationInfoArr['lng']) || empty($locationInfoArr['lng'])) {
                        // need to check it with city + country
                        $model->addError('cityid', 'Please select valid city.');
                        $hasError = 1;
                        echo 'city locality failed<br />';
                    } else {
                        Area::model()->setLocationArray($locationInfoArr);   // to update/add location in db
                        $locationArr['countryid'] = Area::checkAreaExists($locationInfoArr['country'], 'c');
                        $locationArr['stateid'] = Area::checkAreaExists($locationInfoArr['state'], 's');
                        $locationArr['cityid'] = Area::checkAreaExists($locationInfoArr['city'], 'ci');
                        $locationArr['geolat'] = number_format((double)$locationInfoArr['lat'], 6);
                        $locationArr['geolng'] = number_format((double)$locationInfoArr['lng'], 6);
                        echo 'city locality worked<br />';
                    }                                    
                } else {                                    
                    /*if(empty($objMassUpload->locality)) {
                        $objMassUpload->addError('locality', 'Locality cannot be blank.');
                        $hasError = 1;                                        
                    }*/
                    if(empty($city)) {
                        $model->addError('cityid', 'City cannot be blank.');
                        $hasError = 1;
                        echo 'city blank <br />';
                    }                                    
                }
            }
            
            // sending uplaoded image name by uploadify in case of validataion error.
            if (isset($_POST['uploadify_filenames']) && strlen($_POST['uploadify_filenames'])) {
                $alreadyUploadedArr = explode(",", $_POST['uploadify_filenames']);
            }
            if (isset($_POST['uploaded_filenames']) && strlen($_POST['uploaded_filenames'])) {
                //$alreadyUploadedArr = array_unique(array_merge($alreadyUploadedArr, explode(",", $_POST['uploaded_filenames'])));
                $alreadyUploadedArr = array_merge($alreadyUploadedArr, explode(",", $_POST['uploaded_filenames']));
            }
            $uploaded_filenames = implode(",", $alreadyUploadedArr);
            //echo "<pre>";
            //print_r($uploaded_filenames);
            //echo "</pre>";
            if (!$hasError) {
                // Multiple images upload
                //$images = CUploadedFile::getInstancesByName('listingimages');                
                $model->byuserid = Yii::app()->user->id;
                $model->foruserid = Yii::app()->user->id;
                $model->weeklyprice = Listing::calculateWeeklyPrice($model->price, $model->pricedurationid, $model->basecurrencyid);
                $model->countryid = $locationArr['countryid'];
                $model->stateid = $locationArr['stateid'];
                $model->cityid = $locationArr['cityid'];
                $model->geolat = $locationArr['geolat'];
                $model->geolng = $locationArr['geolng'];                
                $model->datecreated = date("Y-m-d H:i:s");
                $model->datemodified = date("Y-m-d H:i:s");
                $model->accurate_geoloc = $accurateGeoloc;
                $model->length = number_format((float)$model->length, 3, '.', '');
                $model->width = number_format((float)$model->width, 3, '.', '');                
                if ($model->save()) {
                    // get the listing id                    
                    $listingId = $model->getPrimaryKey();
                    // save audience tag for the listing                    
                    foreach ($audTagModel->id as $audIds) {
                        $listAudTagModel = new ListingAudienceTag;
                        $listAudTagModel->listingid = $listingId;
                        $listAudTagModel->audiencetagid = $audIds;
                        $listAudTagModel->save();
                    }
                    
                    // pass array with file name do you want to upload on aws
                    $this->uploadFileOnAWS($alreadyUploadedArr, $listingId);
                    
                    // send email to owner on success
                    $userInfo = User::getUserAttributeById(Yii::app()->user->id, 'fname, lname, email');
                    $mail = new EatadsMailer('list-add-owner', $userInfo->email, array('listName'=>$_POST['Listing']['name']));
                    $mail->eatadsSend();
                    // send email to admin on success
                    $mail = new EatadsMailer('list-add-admin', Yii::app()->params['bccEmail'], 
                                    array('listPath'=>Yii::app()->createAbsoluteUrl("listing/{$listingId}"), 'listName'=>$_POST['Listing']['name'], 'fname'=>$userInfo->fname, 'lname'=>$userInfo->lname));
                    $mail->eatadsSend();
                    
                    // add to solr
                    Listing::updateSolr($listingId);
                    Yii::app()->user->setFlash('success', "Listing successfully added.");
                    // $this->redirect(array('manage'));                    
                    $this->redirect(JoyUtilities::getDashboardUrl().'/manage-media');
                }                
            }
        }

        $baseCurrencyList = CHtml::listData(LookupBaseCurrency::model()->findAll(array('select'=>'id,currency_code')), 'id', 'currency_code');
        $countryId = $model->countryid;
        $countryList = Area::model()->getCountryOptions();
        
        $stateId = $model->stateid;
        if ($stateId) {
            $stateList = Area::model()->getStateOptions($countryId);
        }
        $cityId = $model->cityid;
        if ($cityId) {
            $cityList = Area::model()->getCityOptions($stateId);
        }

        $audienceTagCheckboxList = CHtml::listData(AudienceTag::model()->findAll(array('select' => 'id,name')), 'id', 'name');
        //$mediaTypeList = CHtml::listData(MediaType::model()->findAll(array('select' => 'id,name')), 'id', 'name');
        $mediaTypeList = MediaType::model()->findAll();
        $sizeUnitList = Listing::getSizeUnit();
        $lightingList = array('' => 'Select lighting') + Listing::getLighting();
        
        $priceDurationList = Listing::getPriceDuration();

        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            'Dashboard' => JoyUtilities::getDashboardUrl(),
            'Add Listing',
        );
        $listingImages = array();
        $this->render('create', array(
            'model' => $model,
            'audTagModel' => $audTagModel,
            'baseCurrencyList'=>$baseCurrencyList,
            'audienceTagCheckboxList' => $audienceTagCheckboxList,
            'mediaTypeList' => $mediaTypeList,
            'sizeUnitList' => $sizeUnitList,
            'lightingList' => $lightingList,
            'priceDurationList' => $priceDurationList,
            'countryList' => $countryList,
            'stateList' => $stateList,
            'cityList' => $cityList,
            'listingHeading' => 'Add Listing',
            'uploaded_filenames' => $uploaded_filenames,
            'alreadyUploadedArr' => $alreadyUploadedArr,
            'listingImages' => $listingImages, // using for aws uplaoded file count. Blank array is required
            'selectDisableFlag'=>$selectDisableFlag
        ));
    }

    public function uploadFileOnAWS($alreadyUploadedArr, $listingId){
        if (count($alreadyUploadedArr)) {
            foreach ($alreadyUploadedArr as $uploadedFileName) {
                // save the listing images with listing id
                $listingImageModel = new ListingImage;
                $listingImageModel->listingid = $listingId;

                // send them to aws s3
                $s3Obj = new EatadsS3();
                $ext = pathinfo($uploadedFileName, PATHINFO_EXTENSION);
                $newFileName = time().'_'. mt_rand() . '.' . $ext;
                $uploadFilePath = Yii::app()->params['fileUpload']['path'] . 'listing/';
                $originalFileWithPath = $uploadFilePath . $uploadedFileName;

//                $imageThumb = new EasyImage($originalFileWithPath);
//
//                $imageThumb->resize(1280, 1024);
//                $newFileThumbName = $uploadFilePath . $newFileName;
//                $imageThumb->save($newFileThumbName);
//                $s3Obj->uploadFile($newFileThumbName, 'listing/' . $newFileName);
//                @unlink($newFileThumbName);

                $imageThumb = new EasyImage($originalFileWithPath);

                $newFileThumbName = $uploadFilePath . $newFileName;

                copy($originalFileWithPath, $newFileThumbName);
                $s3Obj->uploadFile($newFileThumbName, 'listing/' . $newFileName);
                @unlink($newFileThumbName);                

                $imageThumb->resize(487, 310);
                $newFileThumbName = $uploadFilePath . 'big_' . $newFileName;
                $imageThumb->save($newFileThumbName);
                $s3Obj->uploadFile($newFileThumbName, 'listing/big_' . $newFileName);
                @unlink($newFileThumbName);

                $imageThumb->resize(212, 160);
                $newFileThumbName = $uploadFilePath . 'small_' . $newFileName;
                $imageThumb->save($newFileThumbName);
                $s3Obj->uploadFile($newFileThumbName, 'listing/small_' . $newFileName);
                @unlink($newFileThumbName);

                $imageThumb->resize(102, 74);
                $newFileThumbName = $uploadFilePath . 'tiny_' . $newFileName;
                $imageThumb->save($newFileThumbName);
                $s3Obj->uploadFile($newFileThumbName, 'listing/tiny_' . $newFileName);
                @unlink($newFileThumbName);
                //@unlink($originalFileWithPath);

                $listingImageModel->filename = $newFileName;
                $listingImageModel->save();
            }

            // unlink original file once after uploading all
            array_unique($alreadyUploadedArr);
            foreach ($alreadyUploadedArr as $uploadedFileName) {
                $ext = pathinfo($uploadedFileName, PATHINFO_EXTENSION);
                //$uploadFilePath = Yii::app()->params['fileUpload']['path'] . 'listing/';
                $originalFileWithPath = $uploadFilePath . $uploadedFileName;
                @unlink($originalFileWithPath);
            }

        }
    }
    

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $userId = Yii::app()->user->id;
        // check if it's user's listing
        $result = Listing::hasListing($userId, $id);
        if($result==0) {
            $this->redirect(array('manage'));
        }
        
        $uploaded_filenames = '';
        $alreadyUploadedArr = array();
        $selectDisableFlag = 0;
        $countryList = array();
        $stateList = array();
        $cityList = array();
        $model = new Listing;
        $audTagModel = new AudienceTag;
        $audTagModel->setScenario('addlisting');
        
        // Multiple images upload
        //$images = CUploadedFile::getInstancesByName('listingimages');

        $model = $this->loadModel($id);
        $model->setScenario('updateListing');

        $selectedAudTagIds = ListingAudienceTag::model()->findAllByAttributes(array('listingid' => $id), array('select' => 'audiencetagid'));
        $audTagIds['id'] = array();
        foreach ($selectedAudTagIds as $selAudTagIds) {
            array_push($audTagIds['id'], $selAudTagIds->audiencetagid);
        }
        $audTagModel->attributes = $audTagIds;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Listing'])) {
            $selectDisableFlag = $_POST['select_disable_flag'];
            $_POST['Listing'] = JoyUtilities::cleanInput($_POST['Listing']);
            $audTagModel->attributes = $_POST['AudienceTag'];
            $model->attributes = $_POST['Listing'];
            $hasError = false;

            if (!$audTagModel->validate()) {
                $hasError = true;
            }
            if (!$model->validate()) {
                $hasError = true;
            }

            // match lat lng and country/state/city

            /*if(!empty($model->geolat) && !empty($model->geolng)) {
                $locationInfoArr = JoyUtilities::reverseGeocode($model->geolat, $model->geolng);
                $revGeoArr = array('country' => strtolower($locationInfoArr['country']),
                                    'state' => strtolower($locationInfoArr['state']),
                                    'city' => strtolower($locationInfoArr['city']));
                $dropdownLocationArr = array(
                    'country' => strtolower(Area::model()->getAreaName($model->countryid)),
                    'state' => strtolower(Area::model()->getAreaName($model->stateid)),
                    'city' => strtolower(Area::model()->getAreaName($model->cityid))
                ); 
                if(count(array_diff($revGeoArr, $dropdownLocationArr))) {
                    $model->addError('geolng', 'Selected locations and the Latitude Longitude provided are different. Please enter valid location details.');
                    $hasError = true;
                }
            }*/
            
            $locationArr = array();
            $checkWithLocalityCity = false;
            // if lat long available
            if(!empty($model->geolat) && !empty($model->geolng)) {
                $accurateGeoloc = 1;
                // get the country state city and save in db
                $locationInfoArr = JoyUtilities::reverseGeocode($model->geolat, $model->geolng);
                if(!isset($locationInfoArr['country']) || empty($locationInfoArr['country']) 
                    || !isset($locationInfoArr['state']) || empty($locationInfoArr['state'])
                    || !isset($locationInfoArr['city']) || empty($locationInfoArr['city'])) {
                    // possibly border case, try with location + city
                    $checkWithLocalityCity = true;
                } else {
                    Area::model()->setLocationArray($locationInfoArr);   // to update/add location in db
                    $locationArr['countryid'] = Area::checkAreaExists($locationInfoArr['country'], 'c');
                    $locationArr['stateid'] = Area::checkAreaExists($locationInfoArr['state'], 's');
                    $locationArr['cityid'] = Area::checkAreaExists($locationInfoArr['city'], 'ci');
                    $locationArr['geolat'] = number_format($model->geolat, 6);
                    $locationArr['geolng'] = number_format($model->geolng, 6);
                    $locationArr['accurate_geoloc'] = 1;
                    //$country = $locationInfoArr['country'];
                    //$state = $locationInfoArr['state'];
                    //$city = $locationInfoArr['city'];
                    //$accurateGeoloc = 1;
                    $checkWithLocalityCity = false;
                }
            } else {
                $checkWithLocalityCity = true;
            }
            if($checkWithLocalityCity) {
                $city = Area::model()->getAreaName($model->cityid);
                if(!empty($city)) {
                    // reverse geocode to get lat long, from locality and city
                    $locationArr['accurate_geoloc'] = 0;
                    //$accurateGeoloc = 0;
                    // first try with locality + city
                    $address = $city;
                    $address .= empty($model->locality) ? '' : ','.$model->locality;

                    $locationInfoArr = JoyUtilities::geocode($address);
                    
                    if(!isset($locationInfoArr['country']) || empty($locationInfoArr['country']) 
                        || !isset($locationInfoArr['state']) || empty($locationInfoArr['state'])
                        || !isset($locationInfoArr['city']) || empty($locationInfoArr['city'])
                        || !isset($locationInfoArr['lat']) || empty($locationInfoArr['lat'])
                        || !isset($locationInfoArr['lng']) || empty($locationInfoArr['lng'])) {
                        // need to check it with city + country
                        $model->addError('cityid', 'Please select valid city.');
                        $hasError = 1;
                    } else {
                        Area::model()->setLocationArray($locationInfoArr);   // to update/add location in db
                        $locationArr['countryid'] = Area::checkAreaExists($locationInfoArr['country'], 'c');
                        $locationArr['stateid'] = Area::checkAreaExists($locationInfoArr['state'], 's');
                        $locationArr['cityid'] = Area::checkAreaExists($locationInfoArr['city'], 'ci');
                        $locationArr['geolat'] = number_format((double)$locationInfoArr['lat'], 6);
                        $locationArr['geolng'] = number_format((double)$locationInfoArr['lng'], 6);
                    }
                } else {
                    /*if(empty($objMassUpload->locality)) {
                        $objMassUpload->addError('locality', 'Locality cannot be blank.');
                        $hasError = 1;
                    }*/
                    if(empty($city)) {
                        $model->addError('cityid', 'City cannot be blank.');
                        $hasError = 1;
                    }
                }
            }            
            
            // sending uplaoded image name by uploadify in case of validataion error.
            if (isset($_POST['uploadify_filenames']) && strlen($_POST['uploadify_filenames'])) {
                $alreadyUploadedArr = explode(",", $_POST['uploadify_filenames']);
            }
            if (isset($_POST['uploaded_filenames']) && strlen($_POST['uploaded_filenames'])) {
                //$alreadyUploadedArr = array_unique(array_merge($alreadyUploadedArr, explode(",", $_POST['uploaded_filenames'])));
                $alreadyUploadedArr = array_merge($alreadyUploadedArr, explode(",", $_POST['uploaded_filenames']));
            }
            $uploaded_filenames = implode(",", $alreadyUploadedArr);

            if (!$hasError) {

                $model->byuserid = $userId;
                $model->foruserid = $userId;
                
                // save weeklyprice in USD, conversion from basecurrency to USD
            
                $model->weeklyprice = Listing::calculateWeeklyPrice($model->price, $model->pricedurationid, $model->basecurrencyid);
                $model->solr = 0;
                $model->geolat = empty($model->geolat) ? NULL : $model->geolat;
                $model->geolng = empty($model->geolng) ? NULL : $model->geolng;
                $model->datemodified = date("Y-m-d H:i:s");
                $model->length = number_format((float)$model->length, 3, '.', '');
                $model->width = number_format((float)$model->width, 3, '.', '');
                
                if ($model->save()) {
                    // get the listing id
                    $listingId = $id;

                    // delete all prev aud tags and update new tags
                    $listAudTagModel = new ListingAudienceTag;
                    $listAudTagModel->deleteAllByAttributes(array('listingid' => $listingId));
                    foreach ($audTagModel->id as $audIds) {
                        $listAudTagModel = new ListingAudienceTag;
                        $listAudTagModel->listingid = $listingId;
                        $listAudTagModel->audiencetagid = $audIds;
                        $listAudTagModel->save();
                    }
                    
                    // save the listing images with listing id
                    $listingImageModel = new ListingImage;
                    $listingImageModel->listingid = $listingId;

                    if (count($alreadyUploadedArr)) {

                        foreach ($alreadyUploadedArr as $uploadedFileName) {

                            // save the listing images with listing id
                            $listingImageModel = new ListingImage;
                            $listingImageModel->listingid = $listingId;


                            // send them to aws s3
                            $s3Obj = new EatadsS3();
                            $ext = pathinfo($uploadedFileName, PATHINFO_EXTENSION);
                            $newFileName = time() . '.' . $ext;
                            $uploadFilePath = Yii::app()->params['fileUpload']['path'] . 'listing/';
                            $originalFileWithPath = $uploadFilePath . $uploadedFileName;

                            $imageThumb = new EasyImage($originalFileWithPath);

                            $imageThumb->resize(1280, 1024);
                            $newFileThumbName = $uploadFilePath . $newFileName;
                            $imageThumb->save($newFileThumbName);
                            $s3Obj->uploadFile($newFileThumbName, 'listing/' . $newFileName);
                            @unlink($newFileThumbName);

                            $imageThumb->resize(487, 310);
                            $newFileThumbName = $uploadFilePath . 'big_' . $newFileName;
                            $imageThumb->save($newFileThumbName);
                            $s3Obj->uploadFile($newFileThumbName, 'listing/big_' . $newFileName);
                            @unlink($newFileThumbName);

                            $imageThumb->resize(212, 160);
                            $newFileThumbName = $uploadFilePath . 'small_' . $newFileName;
                            $imageThumb->save($newFileThumbName);
                            $s3Obj->uploadFile($newFileThumbName, 'listing/small_' . $newFileName);
                            @unlink($newFileThumbName);

                            $imageThumb->resize(102, 74);
                            $newFileThumbName = $uploadFilePath . 'tiny_' . $newFileName;
                            $imageThumb->save($newFileThumbName);
                            $s3Obj->uploadFile($newFileThumbName, 'listing/tiny_' . $newFileName);
                            @unlink($newFileThumbName);
                            //@unlink($originalFileWithPath);

                            $listingImageModel->filename = $newFileName;
                            $listingImageModel->save();
                        }
                        
                        // unlink original file once after uploading all
                        array_unique($alreadyUploadedArr);
                        foreach ($alreadyUploadedArr as $uploadedFileName) {
                            $ext = pathinfo($uploadedFileName, PATHINFO_EXTENSION);
                            //$uploadFilePath = Yii::app()->params['fileUpload']['path'] . 'listing/';
                            $originalFileWithPath = $uploadFilePath . $uploadedFileName;
                            @unlink($originalFileWithPath);
                        }
                        
                    }
                    // add to solr
                    Listing::updateSolr($listingId);
                    Yii::app()->user->setFlash('success', "Listing successfully updated.");
                    $this->redirect(array('manage'));
                    //$this->redirect(array('view', 'id' => $model->id));
                }
            }
        }

        $countryId = $model->countryid;
        $countryList = Area::model()->getCountryOptions();

        $stateId = $model->stateid;
        if ($stateId) {
            $stateList = Area::model()->getStateOptions($countryId);
        }
        $cityId = $model->cityid;
        if ($cityId) {
            $cityList = Area::model()->getCityOptions($stateId);
        }
        
        $baseCurrencyList = CHtml::listData(LookupBaseCurrency::model()->findAll(array('select'=>'id,currency_code')), 'id', 'currency_code');
        $audienceTagCheckboxList = CHtml::listData(AudienceTag::model()->findAll(array('select' => 'id,name')), 'id', 'name');
        //$mediaTypeList = CHtml::listData(MediaType::model()->findAll(array('select' => 'id,name')), 'id', 'name');
        $mediaTypeList = MediaType::model()->findAll();
        $sizeUnitList = Listing::getSizeUnit();
        $lightingList = array('' => 'Select lighting') + Listing::getLighting();
        $priceDurationList = Listing::getPriceDuration();
        $listingImages = ListingImage::getListingImageName($id);
        
        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            'Dashboard' => JoyUtilities::getDashboardUrl(),
            'Edit Listing',
        );
        
        $this->render('update', array(
            'model' => $model,
            'audTagModel' => $audTagModel,
            'baseCurrencyList'=>$baseCurrencyList,
            'audienceTagCheckboxList' => $audienceTagCheckboxList,
            'mediaTypeList' => $mediaTypeList,
            'sizeUnitList' => $sizeUnitList,
            'lightingList' => $lightingList,
            'priceDurationList' => $priceDurationList,
            'countryList' => $countryList,
            'stateList' => $stateList,
            'cityList' => $cityList,
            'listingHeading' => 'Edit Listing',
            'uploaded_filenames' => $uploaded_filenames,
            'alreadyUploadedArr' => $alreadyUploadedArr,
            'listingImages' => $listingImages,
            'selectDisableFlag'=>$selectDisableFlag
        ));
    }

    /*
     * Mass Upload using PHPExcel
     */
    public function actionMassupload()
    {
        $model = new UploadListingForm;
        $errorStr = '';
        
        if(isset($_POST['UploadListingForm']))
        {                        
            $model->file_upload = CUploadedFile::getInstance($model,'file_upload');
            // echo "<pre>"; print_r($model->file_upload); 
            
            
            if($model->validate())
            {
                $fileExtension = pathinfo($model->file_upload->name, PATHINFO_EXTENSION); 
                $objPHPExcel = Yii::app()->excel;
                if($fileExtension == 'csv') {           // case CSV
                    $objReader = $objPHPExcel->readCsv();                    
                } else if($fileExtension == 'xls') {    // case XLS
                    $objReader = $objPHPExcel->readExcel9703();                    
                } else {                                // default case XLSX
                    $objReader = $objPHPExcel->readExcel2007();                    
                }
                
                $objPHPExcel = $objReader->load($model->file_upload->tempName);
                $highestRowNumber = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow()."<br />";
                
                //echo $highestColNumber = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn()."<br />";
                //exit;
                
                $hasError = 0;
                if($highestRowNumber == 1) {
                    $hasError = 1;
                    $errorStr = 'Please enter at least one record for listing.';
                } elseif($highestRowNumber > 1) { // if more than one row is there but not in proper order
                    $headerRow = 1;
                    foreach(range("A", "S") as $col) {
                        $excelheader = $this->getExcelHeaderRow();
                        $cellNumber = $col.$headerRow;
                        $cellObj = $objPHPExcel->getActiveSheet()->getCell($cellNumber);
                        $cellValue = $cellObj->getValue();
                        if(strtolower($cellValue) != strtolower($excelheader[$col])) {
                            $hasError = 1;
                            $errorStr = 'Please upload valid template file';
                            break;
                        }
                    }
                }
                
                // loop through eatch cell element
                // ROW STARTS HERE
                
                // initialize excel heading column
                $this->setMassExcelFields();
                
                if(!$hasError) { // if template has more than one row and template header is valid then check other validation
                    $errorCounter = 1;
                    for($rowNumber=2; $rowNumber<=$highestRowNumber; $rowNumber++) {
                        $objMassUpload = new ListingDraft();
                        $objMassUpload->setScenario('createListing');

                        foreach(range("A", "S") as $col) {
                            $attribute = $this->massExcelFields[$col]; 
                            $cellNumber = $col.$rowNumber;

                            if($col == 'G' || $col == 'F'){
                                
                                $cellObj = $objPHPExcel->getActiveSheet()->getCell($cellNumber);
                                
                                if ( is_float($cellObj->getValue()) || is_int($cellObj->getValue())) {
                                    $cellValue = number_format($cellObj->getValue(), 6);
                                } else {
                                    $cellValue = $cellObj->getValue();
                                }
                                
                            } else {
                                $cellObj = $objPHPExcel->getActiveSheet()->getCell($cellNumber);
                                $cellValue = $cellObj->getValue();
                            }
                            //echo $cellNumber . ' = ' . $cellValue . '<br />';
                            $objMassUpload->$attribute = $cellValue;
                        }

                        
                        
                        if(!$objMassUpload->validate()) {
                            $hasError = 1;
                        } 
                        // match lat lng and country/state/city                        
                        if(!$hasError && !empty($objMassUpload->geolat) && !empty($objMassUpload->geolng)) {
                            $locationInfoArr = JoyUtilities::reverseGeocode($objMassUpload->geolat, $objMassUpload->geolng);
                            unset($locationInfoArr['countryCode']);
                            $dropdownLocationArr = array(
                                'country' => strtolower($objMassUpload->country),
                                'state' => strtolower($objMassUpload->state),
                                'city' => strtolower($objMassUpload->city)
                            );
                            $locationInfoArr = array_map('strtolower', $locationInfoArr);
                            $misMatchCount = count(array_diff($locationInfoArr, $dropdownLocationArr));                            
                            if($misMatchCount) {
                                $objMassUpload->addError('geolng', 'Selected locations and the Latitude Longitude provided are different. Please enter valid location details.');
                                $hasError = 1;
                            }
                        }                                                                        
                        
                        // Validate Each Row 
                        if($hasError) {
                            $hasError = 1;
                            $modelArr = $objMassUpload->getErrors();
                            foreach($modelArr as $key => $val) {
                                //echo $key; 
                                //print_r($val[0]);
                                $errorCell = array_search($key, $this->massExcelFields);
                                $errorStr .= $errorCounter." - Cell No. ". $errorCell . $rowNumber ." - ".$val[0]."<br />";
                                $errorCounter++;
                            }
                        }
                        //print_r($objMassUpload->getErrors());
                    }
                }
                // data is validated and save in database
                if(!$hasError) {
                    for($rowNumber=2; $rowNumber<=$highestRowNumber; $rowNumber++) {
                        $objMassUpload = new ListingDraft();
                        $objMassUpload->setScenario('createListing');

                        foreach(range("A", "S") as $col) {
                            $attribute = $this->massExcelFields[$col]; 
                            $cellNumber = $col.$rowNumber;
                            
                            if($col == 'G' || $col == 'F'){
                                $cellObj = $objPHPExcel->getActiveSheet()->getCell($cellNumber);
                                // Lat Lng is not mandatory if lat lan is there then format_number otherwise not
                                if( $cellObj->getValue() != '' ) {
                                    $cellValue = number_format($cellObj->getValue(), 6);
                                } else {
                                    $cellValue = (NULL);
                                }
                            } else {
                                $cellObj = $objPHPExcel->getActiveSheet()->getCell($cellNumber);
                                $cellValue = $cellObj->getValue();
                            }

                            //echo $cellNumber . ' = ' . $cellValue . '<br />';
                            $objMassUpload->$attribute = $cellValue;
                        }
                        
                        $objMassUpload->audiencetag = implode(",", AudienceTag::getAudienceTag($objMassUpload->audiencetag));
                        $objMassUpload->userid = Yii::app()->user->id;
                        $objMassUpload->datecreated = date("Y-m-d H:i:s");
                        $objMassUpload->datemodified = date("Y-m-d H:i:s");
                        $objMassUpload->save();
                        
                    }
                    Yii::app()->user->setFlash('success', "Listing template has been uploaded successfully");
                    $this->redirect(Yii::app()->urlManager->createUrl('user/listing/massview'));
                }
            }
        }
        
        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            'Dashboard' => JoyUtilities::getDashboardUrl(),
            'Mass View' => Yii::app()->urlManager->createUrl('user/listing/massview'),
            'Mass Upload',
        );

        
        $this->render('massupload', array('model'=>$model, 'errorStr' => $errorStr));
    }

    public function getExcelHeaderRow() {
        $excelheader = array (
                            'A' => 'MEDIA TYPE',
                            'B' => 'COUNTRY',
                            'C' => 'STATE',
                            'D' => 'CITY',
                            'E' => 'LOCALITY',
                            'F' => 'LATITUDE',
                            'G' => 'LONGITUDE',
                            'H' => 'NAME',
                            'I' => 'LENGTH',
                            'J' => 'WIDTH',
                            'K' => 'SIZE UNIT',
                            'L' => 'BASE CURRENCY',
                            'M' => 'OTHER DATA',
                            'N' => 'LIGHTING',
                            'O' => 'DESCRIPTION',
                            'P' => 'REACH',
                            'Q' => 'AUDIENCE TAG',
                            'R' => 'PRICE',
                            'S' => 'PRICE DURATION',
                        );
        return $excelheader;
    }
    
    public function setMassExcelFields() {
        $this->massExcelFields = array (
                            'H' => 'name',
                            'I' => 'length',
                            'J' => 'width',
                            'K' => 'sizeunit',
                            'L' => 'basecurrency',
                            'R' => 'price',
                            'S' => 'priceduration',
                            'M' => 'otherdata',
                            'B' => 'country',
                            'C' => 'state',
                            'D' => 'city',
                            'E' => 'locality',
                            'F' => 'geolat',
                            'G' => 'geolng',
                            'N' => 'lighting',
                            'A' => 'mediatype',
                            'O' => 'description',
                            'P' => 'reach',
                            'Q' => 'audiencetag',
                        );
    }
    
    public function actionMassview()
    {        
        if(isset($_POST['save'])) {            
            $listingData = ListingDraft::getAllListingDraftByUserId(Yii::app()->user->id);

            // if listing exist then Save
            if(count($listingData)) {
                $lightingArr = Listing::getLighting();
                $sizeunitArr = Listing::getSizeUnit();
                $pricedurationArr = Listing::getPriceDuration();
                
                foreach($listingData as $data) {
                    $listingObj = new Listing();
                    $listingObj->byuserid = $data->userid;
                    $listingObj->foruserid = $data->userid;
                    $listingObj->name = $data->name;
                    $listingObj->length = $data->length;
                    $listingObj->width = $data->width;
                    $listingObj->price = $data->price;
                    $listingObj->reach = $data->reach;
                    $listingObj->description = $data->description;
                    $listingObj->otherdata = $data->otherdata;
                    $listingObj->geolat = $data->geolat;
                    $listingObj->geolng = $data->geolng;
                    $listingObj->locality = $data->locality;
                    
                    //Fetch id of country, state and city
                    $countryId = Area::isCountryExist($data->country);
                    $stateId = Area::isStateExist($countryId, $data->state);
                    $cityId = Area::isCityExist($stateId, $data->city);
                    
                    $listingObj->countryid = $countryId;
                    $listingObj->stateid = $stateId;
                    $listingObj->cityid = $cityId;

                    // Set Fixed array element
                    $lightingId = array_search(ucfirst(strtolower($data->lighting)), $lightingArr);
                    $sizeunitId = array_search(ucfirst(strtolower($data->sizeunit)), $sizeunitArr);
                    $pricedurationId = array_search(ucfirst(strtolower($data->priceduration)), $pricedurationArr);
                    $listingObj->lightingid = $lightingId;
                    $listingObj->sizeunitid = $sizeunitId;
                    $listingObj->pricedurationid = $pricedurationId;
                    
                    
                    // Fetch id of media type and base currency
                    $baseCurrencyId = LookupBaseCurrency::isBaseCurrencyExist($data->basecurrency);
                    $mediaTypeId = MediaType::isMediaTypeExist($data->mediatype);
                    
                    $listingObj->basecurrencyid = $baseCurrencyId;
                    $listingObj->weeklyprice = Listing::calculateWeeklyPrice($data->price, $pricedurationId, $baseCurrencyId);
                    $listingObj->mediatypeid = $mediaTypeId;

                    $listingObj->datecreated = $data->datecreated;
                    $listingObj->datemodified = $data->datemodified;
                    
                    $listingObj->save();
                    $listingId = $listingObj->getPrimaryKey();
                    
                    // Insert Audiance tag
                    $tagArr = explode(",", $data->audiencetag);
                    if(count($tagArr)) {
                        foreach ($tagArr as $tag) {
                            $tagId = AudienceTag::getTagIdByName($tag);
                            $listAudTagModel = new ListingAudienceTag;
                            $listAudTagModel->listingid = $listingId;
                            $listAudTagModel->audiencetagid = $tagId;
                            $listAudTagModel->save();
                        }
                    }
                    
                    $rs = Yii::app()->db->createCommand("DELETE FROM `ListingDraft` WHERE id = {$data->id}")->query();
//                    $listingDraftModel = ListingDraft::model()->findByPk($data->id); 
//                    $listingDraftModel->status = 0;
//                    ListingDraft::model()->updateByPk($data->id, $listingDraftModel->attributes);
                }
                
                
                Yii::app()->user->setFlash('success', "Listing has been save successfully");
            } else {
                Yii::app()->user->setFlash('success', "No listing to save");
            }
        }
        
        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            'Dashboard' => JoyUtilities::getDashboardUrl(),
            'Mass View',
        );
        
        $this->render('massview');
    }
        
    public function actionMassdelete()
    { 
    
    }
    /*
     * Mass view / Manage listing
     * generates json view to create dropdown
     */
    private function massViewJsonFormat($array=array())
    {       
        $formatedArray = array();
        foreach($array as $key => $value) {            
            $temp = array('text' => $value, 'value'=>$key);
            array_push($formatedArray, $temp);
        }        
        return json_encode($formatedArray);
    }
    
    public function actionManage()
    {        
        $sizeUnitList = $this->massViewJsonFormat(Listing::getSizeUnit());        
        $priceDurationList = $this->massViewJsonFormat(Listing::getPriceDuration());        
        $lightingList =  $this->massViewJsonFormat(Listing::getLighting());
        
        $baseCurrencyList = CHtml::listData(LookupBaseCurrency::model()->findAll(array('select'=>'id,currency_code')), 'id', 'currency_code');
        $baseCurrencyList = $this->massViewJsonFormat($baseCurrencyList);
        
        $mediaTypeList = CHtml::listData(MediaType::model()->findAll(array('select' => 'id,name')), 'id', 'name');                        
        $mediaTypeList = $this->massViewJsonFormat($mediaTypeList);
          
        $audienceTagList = CHtml::listData(AudienceTag::model()->findAll(array('select'=>'id,name')), 'id', 'name');
        $audienceTagList = $this->massViewJsonFormat($audienceTagList);
        //print_r($audienceTagList); die();
        
        $countryList = Area::getCountriesInManage();
        $stateList = Area::getStatesInManage();
        $cityList = Area::getCitiesInManage();
        /* print_r($countryList); die();
        
        $stateId = $model->stateid;
        if ($stateId) {
            $stateList = Area::model()->getStateOptions($countryId);
        }
        $cityId = $model->cityid;
        if ($cityId) {
            $cityList = Area::model()->getCityOptions($stateId);
        } */
        $this->breadcrumbs = array(
            'Home' => JoyUtilities::getHomeUrl(),
            'Dashboard' => JoyUtilities::getDashboardUrl(),
            'Listings Manager',
        );
        
        $this->render('manage',
                array(
                    'sizeUnitList' => $sizeUnitList,
                    'lightingList' => $lightingList,
                    'priceDurationList' => $priceDurationList,
                    'baseCurrencyList' => $baseCurrencyList,
                    'mediaTypeList'=> $mediaTypeList,
                    'countryList' => $countryList,
                    'stateList' => $stateList,
                    'cityList' => $cityList,
                    'audienceTagList' => $audienceTagList
                ));
    }
    
    public function actionUploadedImage() {
        $listingId = Yii::app()->request->getParam('id');
        $listingImages = ListingImage::getListingImageName($listingId);
        $count = count($listingImages);
        $str = '';
        foreach($listingImages as $image) {
        $str .= '<div style="float:left; padding-right:5px;">
                <img src="'.JoyUtilities::getAwsFileUrl("tiny_".$image->filename) .'" alt="'.$image->filename.'" />
                <div class="deletePermanentImage" id="'. $image->id .'" rel="'.$image->filename.'" style="cursor:pointer;">X</div>
                </div>';
        }
        echo json_encode(array($str, $count));
    }
    
    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Listing');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }
    
    /**
     * Manages all models.
     */
//    public function actionAdmin() {
//        $model = new Listing('search');
//        $model->unsetAttributes();  // clear any default values
//        if (isset($_GET['Listing']))
//            $model->attributes = $_GET['Listing'];
//
//        $this->render('admin', array(
//            'model' => $model,
//        ));
//    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Listing the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Listing::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Listing $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'listing-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    public function actionModelSaveImage() {
        $fileNames = Yii::app()->request->getPost('filename');
        $listingId = Yii::app()->request->getPost('listingid');
        
        if($fileNames) {
            $alreadyUploadedArr = explode(",", $fileNames);
            $this->uploadFileOnAWS($alreadyUploadedArr, $listingId);
            if( count($alreadyUploadedArr) ) {
                Listing::updateSolrStatusByListingId($listingId);                
                Listing::updateSolr($listingId);
                echo "1";
            }
        }
    }
}
