<?php

class UserModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'user.models.*',
			'user.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here

			// code will change on RBAC implementation
			if(!Yii::app()->user->isGuest) { // if user logged in 
				// if admin redirect to home, else let the user continue
				if(Yii::app()->user->roleId==1)
					Yii::app()->request->redirect(Yii::app()->homeUrl);
			} else {
					Yii::app()->request->redirect(Yii::app()->homeUrl);
			}
			return true;
		}
		else
			return false;
	}
}
