<?php 
$roleId = Yii::app()->user->roleId;
if($roleId == 3) {
    $this->renderPartial('/dashboard/_ownerHeader', array('activeMenu' => 'editProfile')); 
} elseif($roleId == 2) {
    $this->renderPartial('/dashboard/_buyerHeader', array('activeMenu' => 'editProfile')); 
} elseif($roleId == 4) {
    $this->renderPartial('/dashboard/_serviceProviderHeader', array('activeMenu' => 'editProfile')); 
}
?>
<script>
    $('body').removeAttr('id');
    $('body .wrapper').attr('id', 'dashboard');    
</script>
<div class="container main-body">
    <h2>Account Details</h2>
    
    <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'user_basic_data',
                    'enableAjaxValidation'=>false,
                        'enableClientValidation'=>true,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>true,
                    ),
                )); 
         echo $form->hiddenField($userModel, 'id', array('class'=>'form-control')); 
    ?>
    <div class="row">
        <div class="col-sm-9">
            <form class="form-signup">
                <?php /* <div class="col-sm-6 clearfix">
                    <label class="top big">Username </label>
                    <?php echo $form->textField($userModel, 'username', array('class'=>'form-control', 'readonly'=> true)); ?>
                </div> */ ?>
                <div class="clear clearfix"></div>
                <div class="col-sm-6 clearfix">
                    <label class="top">Email address </label>
                    <?php echo $form->textField($userModel, 'email', array('class'=>'form-control', 'readonly'=> true)); ?>
                </div>
                <div class="col-sm-6">
                    <label class="top">Phone number </label> 
                    <?php echo $form->textField($userModel, 'phonenumber', array('class'=>'form-control')); ?>
                    <?php echo $form->error($userModel, 'phonenumber', array('class'=>'errormessage')); ?>
                </div>
                <div class="col-sm-6 clearfix">
                    <label class="top">Password </label> 
                    <?php echo $form->passwordField($userModel, 'password', array('class'=>'form-control', 'value' => '')); ?>
                    <?php echo $form->error($userModel, 'password', array('class'=>'errormessage')); ?>
                </div>
                <div class="col-sm-6">
                    <label class="top">Confirm Password </label> 
                    <?php echo $form->passwordField($userModel, 'confirmPassword', array('class'=>'form-control')); ?>
                    <?php echo $form->error($userModel, 'confirmPassword', array('class'=>'errormessage')); ?>
                </div>
                <div class="col-sm-6 clearfix">
                    <label class="top">First name </label> 
                    <?php echo $form->textField($userModel, 'fname', array('class'=>'form-control')); ?>
                    <?php echo $form->error($userModel, 'fname', array('class'=>'errormessage')); ?>
                </div>
                <div class="col-sm-6">
                    <label class="top">Last name </label> 
                    <?php echo $form->textField($userModel, 'lname', array('class'=>'form-control')); ?>
                    <?php echo $form->error($userModel, 'lname', array('class'=>'errormessage')); ?>
                </div>
                <div class="col-sm-12">

                </div>
                <input type="submit" id="_submit" class="btn btn-lg btn-success" value="Save"/>
            </form>
        </div>
    </div>
    <?php $this->endWidget(); ?>
    
    
    <?php 
                    $form = $this->beginWidget('CActiveForm', array(
                                                            'id' => 'user_company_form',
                                                            'enableAjaxValidation' => false,
                                                            'enableClientValidation' => true,
                                                            'clientOptions' => array(
                                                                'validateOnSubmit' => true,
                                                            ),
                                                            'htmlOptions' => array('enctype'=>'multipart/form-data'),
                        ));
                echo $form->hiddenField($userCompanyModel, 'id', array('class'=>'form-control')); 
    ?>
    <h2>Your Company Profile</h2>
    <div class="row">
        <div class="col-sm-9">
            <form class="form-signup">
                <div class="col-sm-6 clearfix">
                    <label class="top">Company name</label>
                    <?php echo $form->textField($userCompanyModel, 'name', array('class'=>'form-control')); ?>
                    <?php echo $form->error($userCompanyModel, 'name', array('class'=>'errormessage')); ?>
                </div><br clear="all">
                <div class="col-sm-12 clearfix">
                    <label class="top">A few lines about your company</label> 
                    <?php echo $form->textArea($userCompanyModel, 'description', array('class'=>'form-control col-sm-8')); ?>
                    <?php echo $form->error($userCompanyModel, 'description', array('class'=>'errormessage')); ?>
                </div>
                <div class="col-sm-6 clearfix">
                    <label class="top">Website url </label> 
                    <?php 
                        //if(!$userCompanyModel->websiteurl) { $userCompanyModel->websiteurl = 'http://'; }
                        echo $form->textField($userCompanyModel, 'websiteurl', array('class'=>'form-control', 'placeholder' => "http://www.example.com")); 
                    ?>
                    <?php echo $form->error($userCompanyModel, 'websiteurl', array('class'=>'errormessage')); ?>
                </div>
                <div class="col-sm-6">
                    <label class="top">Phone number </label> 
                    <?php echo $form->textField($userCompanyModel, 'phonenumber', array('class'=>'form-control')); ?>
                    <?php echo $form->error($userCompanyModel, 'phonenumber', array('class'=>'errormessage')); ?>
                </div>
                <div class="hr clearfix"></div>
                <div class="col-sm-6 clearfix">
                    <label class="top">Country </label> 
                    <?php echo $form->dropDownList($userCompanyModel, 'countryid', $countryList, array('empty' => 'Select country')); ?>
                    <?php echo $form->error($userCompanyModel, 'countryid', array('class'=>'errormessage')); ?>
                </div>
                <div class="col-sm-6">
                    <label class="top">State </label> 
                    <?php echo $form->dropDownList($userCompanyModel, 'stateid', $stateList, array('empty' => 'Select state') ); ?>
                    <?php echo $form->error($userCompanyModel, 'stateid', array('class'=>'errormessage')); ?>
                </div><br clear="all">
                <div class="col-sm-6 clearfix">
                    <label class="top">City </label> 
                    <?php echo $form->dropDownList($userCompanyModel, 'cityid', $cityList, array('empty' => 'Select city') ); ?>
                    <?php echo $form->error($userCompanyModel, 'cityid', array('class'=>'errormessage')); ?>    
                </div><br clear="all">
                <div class="col-sm-6">
                    <label class="top">Address </label> 
                    <?php echo $form->textField($userCompanyModel, 'address1', array('class'=>'form-control')); ?>
                    <?php echo $form->error($userCompanyModel, 'address1', array('class'=>'errormessage')); ?>
                </div><br clear="all">
                <div class="col-sm-6">
                    <label class="top"> </label> 
                    <?php echo $form->textField($userCompanyModel, 'address2', array('class'=>'form-control')); ?>
                    <?php echo $form->error($userCompanyModel, 'address2', array('class'=>'errormessage')); ?>
                </div><br clear="all">
                <div class="col-sm-6">
                    <label class="top">Zip </label> 
                    <?php echo $form->textField($userCompanyModel, 'postalcode', array('class'=>'form-control')); ?>
                    <?php echo $form->error($userCompanyModel, 'postalcode', array('class'=>'errormessage')); ?>
                </div><br clear="all">
                <div class="hr clearfix"></div>

                <div class="col-sm-6 clearfix">
                    <label class="top">Facebook profile</label> 
                    <?php echo $form->textField($userCompanyModel, 'facebookprofile', array('class'=>'form-control', 'placeholder' => "http://www.facebook.com")); ?>
                    <?php echo $form->error($userCompanyModel, 'facebookprofile', array('class'=>'errormessage')); ?>
                </div>
                <div class="col-sm-6 clearfix">
                    <label class="top">Twitter handle</label> 
                    <?php echo $form->textField($userCompanyModel, 'twitterhandle', array('class'=>'form-control')); ?>
                    <?php echo $form->error($userCompanyModel, 'twitterhandle', array('class'=>'errormessage')); ?>
                </div>
                <div class="col-sm-6 clearfix">
                    <label class="top">Linkedin profile</label> 
                    <?php echo $form->textField($userCompanyModel, 'linkedinprofile', array('class'=>'form-control', 'placeholder' => "http://www.linkedin.com")); ?>
                    <?php echo $form->error($userCompanyModel, 'linkedinprofile', array('class'=>'errormessage')); ?>
                </div>
                <div class="col-sm-6 clearfix">
                    <label class="top">Google+ profile</label> 
                    <?php echo $form->textField($userCompanyModel, 'googleplusprofile', array('class'=>'form-control', 'placeholder' => "http://plus.google.com")); ?>
                    <?php echo $form->error($userCompanyModel, 'googleplusprofile', array('class'=>'errormessage')); ?>
                </div>
                <div class="hr clearfix"></div>
                <div class="col-sm-6 clearfix">
                    <label class="top">Logo</label> 
                    <?php echo $form->fileField($userCompanyModel, 'logo', array('class'=>'')); ?>
                    <?php echo $form->error($userCompanyModel, 'logo', array('class'=>'errormessage')); ?>
                </div>
                
                <div class="col-sm-6 ">
                    <?php if($previousLogo == '') { ?> 
                        <div style="float:left; padding-right:5px;">
                            No logo uploaded.
                        </div>
                    <?php } else { ?>
                        <div style="float:left; padding-right:5px;">
                            <?php 
                                $logoPath = JoyUtilities::getAwsFileUrl($previousLogo, 'companylogo'); 
                                if(strlen($logoPath)) {
                            ?>
                                <img src="<?php echo $logoPath; ?>" alt="companylogo" />
                                <div class="deleteLogo" id="<?php echo $userCompanyModel->id; ?>" rel="<?php echo $previousLogo; ?>" style="cursor:pointer;">X</div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                
                <div class="col-sm-12">

                    <input type="submit" id="_submit" class="btn btn-lg btn-success" value="Save"> 



                </div>

            </form>

        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>

<script>
    
    
    $( '.deleteLogo' ).click(function(){
        var fileName = $( this ).attr('rel');
        var cId = $( this ).attr('id');
        var currItem = $( this );

        $.ajax({
            type: 'POST',
            url:  '<?php echo Yii::app()->urlManager->createUrl('user/profile/deleteLogo')?>',
            data: { filename: fileName, cid: cId}
        }) .done(function( msg ) {
            if(msg == 1) {
                currItem.parent().html('No logo uploaded.');
            }
            // 1 for success 0 otherwise
        });
    });
    
    $(function(){
        $('#UserCompany_countryid').fancyfields("bind","onSelectChange", function (input,text,val){
            //alert("Current input - ID : "+input.attr("id") + " , selected text : " + text + " , value : " + val);
            $.ajax({                
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl('ajax/dynamicstates'); ?>", //url to call.
                cache: true,
                data: {id: val},
                success: function (data) {                                        
                    $('#UserCompany_stateid').setOptions(eval(data));   
                    $('#UserCompany_cityid').setOptions([["Select city", ""]]); 
                    
                }
            });                
        }); //- See more at: http://www.jqfancyfields.com/examples-docs/#sthash.qwn8FgiY.dpuf
        $('#UserCompany_stateid').fancyfields("bind","onSelectChange", function (input,text,val){
            //alert("Current input - ID : "+input.attr("id") + " , selected text : " + text + " , value : " + val);
            $.ajax({                
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl('ajax/dynamiccities'); ?>", //url to call.
                cache: true,
                data: {id: val},
                success: function (data) {                                        
                    $('#UserCompany_cityid').setOptions(eval(data));   
                }
            });                
        });
    });
</script>
