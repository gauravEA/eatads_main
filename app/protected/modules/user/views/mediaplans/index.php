<?php
$this->renderPartial('/dashboard/_buyerHeader', array('activeMenu' => 'mediaPlan'));
?>
<div class="container main-body">
    
    <?php 
    if(count($planData)) {
        foreach($planData as $plan) { $counter = 1; ?>
    <div class="content_header"><h2><?php echo $plan['name']; ?></h2></div>

    <div class="row mosaic left">
        <div class="col-sm-12 left">
            <div class="row left">
                <div class="clear"></div>
                
                <?php 
                $planListings = PlanListing::getPlanListingByPlanId($plan['id']);
                if(count($planListings) > 0) {
                    foreach($planListings as $key => $listing) { 
                ?>
                
                <?php 
                    if(!empty($listing['filename'])) {
                        $imagePath = JoyUtilities::getAwsFileUrl('small_'.$listing['filename'], 'listing');
                        if(empty($imagePath)) {
                            $imagePath = Yii::app()->baseUrl. '/images/site/img.png';
                        }
                    } else {
                        $imagePath = Yii::app()->baseUrl. '/images/site/img.png';
                    }  
                ?>
                <div id="<?php echo $listing['listingid']."_".$plan['id']."_".$listing['planlistingid']; ?>" class="left mosaicview" style="background: url(<?php echo $imagePath;?>) no-repeat top left;">

                    <h3>
                        <a href="<?php echo Yii::app()->urlManager->createUrl('listing/view', array('id'=>$listing['listingid'])); ?>"><?php echo $listing['name']; ?></a>
                        <br/><span>
                            <?php echo $listing['medianame']; ?><br/>
                            <?php echo JoyUtilities::formatNumber($listing['length'], 2); ?> x <?php echo JoyUtilities::formatNumber($listing['width'], 2); ?> <?php echo Listing::getSizeUnit($listing['sizeunitid']); ?><sup>2</sup>
                        </span>
                    </h3>

                    <div class="foot-info">
                        <h4>
                            <?php echo '<span class="dyn_ccode">'.CHtml::encode($this->ipCurrencyCode).'</span> <span class="dyn_price">'. CHtml::encode(JoyUtilities::formatNumber(Yii::app()->openexchanger->convertCurrency($listing['price'], $listing['currency_code'], $this->ipCurrencyCode))).'</span> / '.Listing::getPriceDuration($listing['pricedurationid']); ?>
                            <br/>
                            <span>
                                <?php echo '<span class="base_ccode">'.CHtml::encode($listing['currency_code']).'</span> <span class="base_price">'. CHtml::encode(JoyUtilities::formatNumber($listing['price'])).'</span> / '. Listing::getPriceDuration($listing['pricedurationid']); ?>
                            </span>
                        </h4>
                        <span class="btn btn-primary counter"><?php echo $counter++; ?></span>
                    </div>
                    <a href="javascript:void(0);" onclick="removeMediaPlan(<?php echo $listing['listingid']; ?>, <?php echo $plan['id']; ?>, <?php echo $listing['planlistingid']; ?>);" class="remove">Remove from plan</a>
                </div>
                
                <?php  }  ?>
                
            </div>
            <div class="col-sm-12">
                <button rel="<?php echo $plan['id']; ?>" class="btn btn-success btn-lg sendrfp">Create RFP</button>
            </div>
            <?php } else {?>
            <div>No listing in this plan.</div>
            <?php } ?>
        </div>
    </div>
    <?php } ?>
    
    <?php } else { ?>
    <div class="content_header" align="center">No Media plan found.</div>
    <?php } ?>
    
</div>




<script>
    $('body').addClass("views favourites");
    $('body').attr('id', "dashboard");
    
    function editMediaPlan(mediaPlanId) {
        $('#mediaplanid').val(mediaPlanId);
        $('#edit_media_plan').removeAttr("disabled");
        $('#mediaplanname').val($.trim($('#'+mediaPlanId+' .mediaplanname').text()));
        $('#mediaplannotes').val($.trim($('#'+mediaPlanId+' .mediaplannotes').text()));
        $('#EditToPlanModal').modal('show');
    }  
    
    
    $(function(){            
        $('#edit_media_plan').click(function(){
            //console.log('email submit') ;
            
            if($('#mediaplanname').val()) {
                $('#plan_unique_error').html('Plan name cannot be blank.');
                $('#plan_unique_error').show();
                return;
            }
            
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->urlManager->createUrl('user/mediaplan/edit'); ?>",
                data: {
                    id: $('#mediaplanid').val(),
                    name: $('#mediaplanname').val(),
                    notes: $('#mediaplannotes').val()
                },
                success: function(data) {
                    if(data == 2) {
                        $('#plan_unique_error').html('Please enter unique plan name.');
                        $('#plan_unique_error').show();
                        return;
                    } 
                    if(data == 3) {
                        $('#plan_unique_error').html('Plan name cannot be blank.');
                        $('#plan_unique_error').show();
                        return;
                    }
                    
                    $('#EditToPlanModal').modal('hide');
                    if(data == 1) {
                        msg = '<h4 class="modal-title">Edit Media Plan!</h4><p>Media plan has been edited successfully.</p>';
                    } else {
                        msg = '<h4 class="modal-title">Oops!</h4><p></p>Failure, Please try again!';
                    }
                    $('#ThanksModalBody').html(msg);
                    $('#ThanksModal').modal('show');
                    $.fn.yiiListView.update("listViewId");
                }
            }); 
        });
        
        $('#mediaplanname').click(function(){
            $('#plan_unique_error').hide();
        });
        
        $("#mediaplanname").bind("keyup", function(event, ui) {            
            if( !$.trim($(this).val()) ) {
                $('#edit_media_plan').attr("disabled", "disabled");
            } else {                
                $('#edit_media_plan').removeAttr("disabled");
            }
        });
        
    });

    function sendRFP(mediaPlanId) {
        $('#rfpplanid').val(mediaPlanId);
        $('#rfpprice').val('');
        $('#rfpnotes').val('');
        $('#send_rfp').removeAttr("disabled");
        $('#sendRFP').modal('show');
    }  
    
    $(function(){            
        $('#send_rfp_btn').click(function(){
            
            if(isNaN($('#rfpprice').val()) || $('#rfpprice').val() <= 0  ) { // if price is not numeric is blank
                $('#rfp_price_error').html('Price cannot be blank or zero.');
                $('#rfp_price_error').show();
                return;
            }
            
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->urlManager->createUrl('user/mediaplan/sendRFP'); ?>",
                data: {
                    planid: $('#rfpplanid').val(),
                    price: $('#rfpprice').val(),
                    notes: $('#rfpnotes').val()
                },
                success: function(data) {
                    if(data == 3) { // if price is blank
                        $('#rfp_price_error').html('Price cannot be blank or zero.');
                        $('#rfp_price_error').show();
                        return;
                    }
                    
                    $('#sendRFP').modal('hide');
                    if(data == 1) {
                        msg = '<h4 class="modal-title">Send RFP!</h4><p>RFP has been sent successfully.</p>';
                    } else {
                        msg = '<h4 class="modal-title">Oops!</h4><p></p>Failure, Please try again!';
                    }
                    $('#ThanksModalBody').html(msg);
                    $('#ThanksModal').modal('show');
                    $.fn.yiiListView.update("listViewId");
                }
            }); 
        });
        
        $('#rfpprice').click(function(){
            $('#rfp_price_error').hide();
        });
        
        $("#rfpprice").bind("keyup", function(event, ui) {            
            if( !$.trim($(this).val()) ) {
                $('#send_rfp_btn').attr("disabled", "disabled");
            } else {                
                $('#send_rfp_btn').removeAttr("disabled");
            }
        });
        
    });

    function removeMediaPlan(lid, pid, plid) {
        
        var r=confirm("Do you want to remove this listing from plan?");
        if (r == true)
        {
            $.ajax({
            url: '<?php echo Yii::app()->createUrl("user/mediaplans/removeMediaplan"); ?>',
            type: "POST",
            data: { lid: lid, pid: pid, plid: plid}
            }).done(function(data) {
                $('#'+lid+'_'+pid+'_'+plid).remove();
                window.location.reload();
            }); 
        }
    }
    
    // add sendrfp class on button
    $('.sendrfp').click(function(){
        var planid = $(this).attr('rel');
        // window.location.href = '<?php // echo Yii::app()->urlManager->createUrl('user/mediaplans/sendrfp', array('id' => '')); ?>'+'/'+planid;
        window.location.href = '<?php echo JoyUtilities::getDashboardUrl(); ?>'+'/'+planid+'/rfp-create';
    });
</script>