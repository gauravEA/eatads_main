<?php
$this->renderPartial('/dashboard/_buyerHeader', array('activeMenu' => 'mediaPlan'));
?>
<div class="container main-body views favourites rfp">
    
    <form method="post">
    <div class="content_header"><h2>Sending RFP | <?php echo $planData->name?></h2></div>

    

    <div class="col-sm-12 clearfix comment ">
        <label class="top">Add comment for everyone</label><div class="clear clearfix"></div> 
        <textarea name="everyonecomment" class="form-control col-sm-8"></textarea>
    </div>
    
    <?php foreach($ownerNames as $ownerName) {
        $counter = 1;
        $listingsData = PlanListing::getPlanListingByPlanId($planData->id, $ownerName->listing->foruserid);
        $ownerFullName = ucfirst(strtolower($ownerName->listing->foruser->fname)). " " .ucfirst(strtolower($ownerName->listing->foruser->lname));
    ?>
    <h3 class="sub"><?php echo $ownerFullName; ?>'s listing</h3>
    <div class="row mosaic left">
        <div class="col-sm-12 left">


            <div class="row left">
                <div class="clear"></div>
                
                <?php foreach($listingsData as $listing) {
                    
                    if(!empty($listing['filename'])) {
                        $imagePath = JoyUtilities::getAwsFileUrl('small_'.$listing['filename'], 'listing');
                        if(empty($imagePath)) {
                            $imagePath = Yii::app()->baseUrl. '/images/site/img.png';
                        }
                    } else {
                        $imagePath = Yii::app()->baseUrl. '/images/site/img.png';
                    }  
                ?>
                <div class="left mosaicview" style="background: url(<?php echo $imagePath;?>) no-repeat top left;">

                    <h3>
                        <a href="<?php echo Yii::app()->urlManager->createUrl('listing/view', array('id'=>$listing['listingid'])); ?>"><?php echo $listing['name']; ?></a><br/>
                        <span>
                            <?php echo $listing['medianame']; ?><br/>
                            <?php echo JoyUtilities::formatNumber($listing['length'], 2); ?> x <?php echo JoyUtilities::formatNumber($listing['width'], 2); ?> <?php echo Listing::getSizeUnit($listing['sizeunitid']); ?><sup>2</sup>
                        </span>
                    </h3>

                    <div class="foot-info">
                        <h4>
                            <?php echo '<span class="dyn_ccode">'.CHtml::encode($this->ipCurrencyCode).'</span> <span class="dyn_price">'. CHtml::encode(JoyUtilities::formatNumber(Yii::app()->openexchanger->convertCurrency($listing['price'], $listing['currency_code'], $this->ipCurrencyCode))).'</span> / '. Listing::getPriceDuration($listing['pricedurationid']); ?><br/>
                            <span>
                                <?php echo '<span class="base_ccode">'.CHtml::encode($listing['currency_code']).'</span> <span class="base_price">'. CHtml::encode(JoyUtilities::formatNumber($listing['price'])).'</span> / '. Listing::getPriceDuration($listing['pricedurationid']); ?>
                            </span>
                        </h4>
                        <span class="btn btn-primary counter"><?php echo $counter++;?></span>
                    </div>
                    <?php
                        $ownerListingId = $ownerName->listing->foruserid."_".$listing['listingid'];
                    ?>
                    <a href="javascript:void(0);" 
                       id="popover_<?php echo $ownerListingId; ?>" 
                       class="remove addcomment" 
                       data-container="body" 
                       data-html="true" 
                       data-placement="bottom"  
                       data-class="rfp" 
                       data-content="<textarea id='listingcomment_<?php echo $ownerListingId; ?>' name='listingcomment_<?php echo $ownerListingId; ?>' class='form-control'></textarea><button onclick='saveComment(this.id)' id='<?php echo $ownerListingId; ?>' class='btn btn-primary'>Save</button><p onclick='cancelComment(this.id)' id='<?php echo $ownerListingId; ?>'>Cancel</p>" 
                       data-original-title="">
                        Make a comment
                    </a>
                    <input type="hidden" id="listing_<?php echo $ownerListingId; ?>" name="comment[<?php echo $ownerName->listing->foruserid; ?>][listing][<?php echo $listing['listingid']; ?>]" />
                </div>
                <?php } ?>

            </div>

        </div>
    </div>
    <div class="col-sm-12 clearfix comment secondary closed">
        <label class="top">Add comment for <?php echo $ownerFullName; ?></label><div class="clear clearfix"></div> 
        <textarea name="comment[owner][<?php echo $ownerName->listing->foruserid; ?>]" class="form-control col-sm-8"></textarea>
    </div>
    <?php } ?>
    
    
    
    <div class="col-sm-12">
        <button type="submit" name="sendRfp" class="btn btn-lg btn-success big">Send RFP</button>
    </div>
    
    </form>
</div>
<script>
    $('body').attr('class', 'rfp');
    $('body').attr('id', '');
    $('body .wrapper').attr('id', 'dashboard');
    
   function saveComment(listingCommentId) {
       $('#listing_'+listingCommentId).val($('#listingcomment_'+listingCommentId).val());
       $('#popover_'+listingCommentId).popover("hide");
   }
   function cancelComment(listingCommentId) {
       $('#listing_'+listingCommentId).val('');
   }
   
    $('.addcomment').click(function(){
        var popoverId = this.id;
        var commentId = popoverId.substring(8,popoverId.length);
        setTimeout(function() {
            $('#listingcomment_'+commentId).val($('#listing_'+commentId).val());
            // 1000 for 1 seconds
        }, 100);
        //
    });
</script>