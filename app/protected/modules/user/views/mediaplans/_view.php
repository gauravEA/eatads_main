<div id="<?php echo $data->id; ?>" class="left list-row">
    <div class="col-sm-3">
        <a href='<?php echo Yii::app()->urlManager->createUrl('listing/view', array('id'=>$data->listing->id)); ?>'><?php echo $data->listing->name; ?></a><br/>
    </div>
    <div class="col-sm-3 mediaplanname">
        <?php echo $data->name; ?>
    </div>
    <div class="col-sm-3 mediaplannotes">
        <?php echo $data->notes; ?>
    </div>

    <div class="col-sm-3">
        <a href="javascript:void(0);" onclick="removeMediaPlan(<?php echo $data->id; ?>);" class="remove">Remove from media plan</a>
        <br>
        <a href="javascript:void(0);" onclick="editMediaPlan(<?php echo $data->id; ?>);" class="remove">Edit media plan</a>
        <br>
        <a href="javascript:void(0);" onclick="sendRFP(<?php echo $data->id; ?>);" class="remove">Send RFP</a>
    </div>
</div>
<div style="clear:both;"></div>
<br>