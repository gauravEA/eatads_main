<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view', // refers to the partial view named '_post'
    //lets tell the pager to use our own css file
    'pager' => array('cssFile' => Yii::app()->baseUrl . '/css/listViewStyle/listViewPager.css'),
    // 'pagerCssClass' => 'paging',
    'summaryText' => 'Showing {start} - {end} of {count}',
    'pager' => Array(
        // 'cssFile' => Yii::app()->baseUrl . '/css/listViewStyle/listViewPager.css',
        'header' => '',
        'prevPageLabel' => 'Previous',
        'nextPageLabel' => 'Next',
        'firstPageLabel' => 'First',
        'lastPageLabel' => 'Last Page'
    ),
    'template' => '{items}{pager}{summary}',
    'id' => 'listViewId',
    'sortableAttributes' => array(
    //'name',
    ),
    'emptyText' => '<div align="center">No media plan found.</div>',
    //'enablePagination'=> false,
    'afterAjaxUpdate' => "function(id, data) {               
                }",
));
?>
