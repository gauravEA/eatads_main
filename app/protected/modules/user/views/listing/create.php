<?php
/* @var $this ListingController */
/* @var $model Listing */

?>

<?php $this->renderPartial('_my_form', array(
                                        'model'=>$model, 
                                        'audTagModel'=>$audTagModel,
                                        'baseCurrencyList'=>$baseCurrencyList,
                                        'mediaTypeList'=>$mediaTypeList,
                                        'sizeUnitList'=>$sizeUnitList,
                                        'lightingList'=>$lightingList,
                                        'priceDurationList'=>$priceDurationList,
                                        'countryList'=>$countryList,
                                        'stateList'=>$stateList,
                                        'cityList'=>$cityList,
                                        'audienceTagCheckboxList'=>$audienceTagCheckboxList,
                                        'listingHeading'=>$listingHeading,
                                        'uploaded_filenames' => $uploaded_filenames,
                                        'alreadyUploadedArr' => $alreadyUploadedArr,
                                        'listingImages' => $listingImages,
                                        'selectDisableFlag'=>$selectDisableFlag,
                        )); ?>

<?php $this->widget('GoogleMap', array('latitude_fieldname'=>'Listing_geolat', 'longitude_fieldname'=>'Listing_geolng')); ?>