<?php $this->renderPartial('../dashboard/_ownerHeader', array('activeMenu' => 'manageListing')); ?>
<div class="container main-body">


    
    <div class="navbar navbar-second navbar-static-top dashboard ">
        <div class="container">

            <div class="left">
                <h2>
                    Listings Manager
                </h2>
            </div>
            <div class="right">
                <a href="<?php // echo Yii::app()->urlManager->createUrl('user/listing/create'); ?><?php echo JoyUtilities::getDashboardUrl().'/add'; ?>" class="btn btn-primary">Add Listing</a>
            </div>
        </div>
    </div>
    
    
    <div id="error" style="color:red;"></div>
    
    <br />

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/kendo.all.min.js'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/kendo.common.min.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/kendo.default.min.css'); ?>        

    <div id="grid"></div>

    <script>  
        var all_countries = <?php echo $countryList; ?>;
        var all_states = <?php echo $stateList; ?>;
        var all_cities = <?php echo $cityList; ?>;
        
        function getUrlParameter(query_string, search_param)
        {

            var sURLVariables = query_string.split('&');
            for (var i = 0; i < sURLVariables.length; i++)
            {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == search_param)
                {
                    if (sParameterName[1] == '') {
                        return true;    // in case of &lt= 
                    } else {
                        return sParameterName[1];   // otherwise
                    }
                }
            }
            return false;   // if param donot exists
        }
        //$(function() {            
        $("#grid").kendoGrid({
            dataSource: {
                transport: {
                    read: {
                        url: '<?php echo Yii::app()->urlManager->createUrl('ajax/managelist'); ?>'
                    },
                    create: {
                        url: '<?php echo Yii::app()->urlManager->createUrl('ajax/managelist'); ?>',
                        type: "PUT",
                        complete: function(e) {
                            if (e.responseText == "success") {
                                var div_id = 'sn' + getUrlParameter(this.data, 'sn');
                                $('#' + div_id).remove();
                                $("#grid").data("kendoGrid").dataSource.read();
                                alert('Record successfully created.');
                            } else {
                                var div_id = 'sn' + getUrlParameter(this.data, 'sn');
                                $('#' + div_id).remove();
                                var html = '<div id="' + div_id + '" >' + e.responseText + '</div>';
                                $("#error").append(html);
                            }
                        }
                    },
                    update: {
                        url: '<?php echo Yii::app()->urlManager->createUrl('ajax/managelist'); ?>',
                        type: "POST",
                        complete: function(e) {
                            if (e.responseText == "success") {
                                var div_id = 'sn' + getUrlParameter(this.data, 'sn');
                                $('#' + div_id).remove();
                                $("#grid").data("kendoGrid").dataSource.read();
                                alert('Record successfully updated.');
                            } else {
                                var div_id = 'sn' + getUrlParameter(this.data, 'sn');
                                $('#' + div_id).remove();
                                var html = '<div id="' + div_id + '" >' + e.responseText + '</div>';
                                $("#error").append(html);
                            }
                        }
                    }
                },
                pageSize: <?php echo Yii::app()->params['jrid_recorder_per_page']; ?>,
                /*error: function(e) {
                 alert(e.responseText);
                 },*/
                schema: {
                    data: "data",
                    total: function(response) {
                        return $(response.data).length;
                    },
                    model: {
                        id: "id",
                        fields: {
                            sn: {type: 'number', editable: false},
                            name: {required: true},
                            length: {type: 'number', required: true},
                            width: {type: 'number', required: true},
                            sizeunitid: {required: true},
                            basecurrencyid: {required: true},
                            price: {required: true},
                            pricedurationid: {},
                            otherdata: {},
                            countryid: {required: true },
                            stateid: {required: true },
                            cityid: {required: true },
                            locality: {},
                            geolat: {type: 'number'},
                            geolng: {type: 'number'},
                            lightingid: {required: true},
                            mediatypeid: {required: true},
                            description: {},
                            reach: {},
                            audiencetag: {required: true}
                        }
                    }
                }
            },   
            editable: "inline", // use inline mode so both dropdownlists are visible (required for cascading)
            //editor: sizeUnitDropDownEditor, template: "#=sizeunitid.sizeunit#"},
            columns: [
                { command: ["edit"], title: "&nbsp;", width: "80px" },
                { field: "uploadimage", title: "&nbsp;", width: 110, template: "#= uploadImage(id) #" },
                { field: "viewlisting", title: "&nbsp;", width: 110, template: "#= viewListing(id) #" },
                { field: "sn", title: "S.No.", width: 70 },
                { field: "name", title: "Listing Name", width: 110},
                { field: "length", title: "Length", width: 110, editor: decimal3Editor },
                { field: "width", title: "Width", width: 110, editor: decimal3Editor },
                { field: "sizeunitid", title: "Size Unit", width: 110, values: <?php echo $sizeUnitList; ?> },
                { field: "basecurrencyid", title: "Base Currency", width: 110, values: <?php echo $baseCurrencyList; ?> },
                { field: "price", title: "Price", width: 110 },
                { field: "pricedurationid", title: "Price Duration", width: 110, values: <?php echo $priceDurationList; ?> },
                { field: "otherdata", title: "Other Data", width: 110},
                { field: "countryid", title: "Country", width: 110, template: "#= countryName(countryid) #", 
                    editor: function(container) { // use a dropdownlist as an editor
                        // create an input element with id and name set as the bound field (brandId)
                        // console.log('country changed');
                        var input = $('<input id="countryid" name="countryid">');
                        // append to the editor container 
                        input.appendTo(container);
                        // initialize a dropdownlist
                        input.kendoDropDownList({
                            dataTextField: "name",
                            dataValueField: "countryid",
                            dataSource: all_countries // bind it to the brands array
                        }).appendTo(container);
                    } 
                },
                { field: "stateid", title: "State", width: 110, template: "#= stateName(stateid) #", 
                    editor: function(container) { // use a dropdownlist as an editor
                        //console.log('state changed');
                        var input = $('<input id="stateid" name="stateid">');
                        input.appendTo(container);
                        input.kendoDropDownList({
                            dataTextField: "name",
                            dataValueField: "stateid",
                            cascadeFrom: "countryid", // cascade from the brands dropdownlist
                            dataSource: all_states // bind it to the models array
                        }).appendTo(container);
                    } 
                },
                { field: "cityid", title: "City", width: 110, template: "#= cityName(cityid) #", 
                    editor: function(container) { // use a dropdownlist as an editor
                        //console.log('city changed');
                        var input = $('<input id="cityid" name="cityid">');
                        input.appendTo(container);
                        input.kendoDropDownList({
                            dataTextField: "name",
                            dataValueField: "cityid",
                            cascadeFrom: "stateid", // cascade from the brands dropdownlist
                            dataSource: all_cities // bind it to the models array
                        }).appendTo(container);
                    } 
                },
                { field: "locality", title: "Locality", width: 110},
                { field: "geolat", title: "Latitude", width: 110, editor: decimal6Editor },
                { field: "geolng", title: "Longitude", width: 110, editor: decimal6Editor },
                { field: "lightingid", title: "Lighting", width: 110, values: <?php echo $lightingList; ?> },
                { field: "mediatypeid", title: "Media Type", width: 110, values: <?php echo $mediaTypeList; ?> },
                { field: "description", title: "Description", width: 110},
                { field: "reach", title: "Reach", width: 110},
                { field: "audiencetag", title: "Audience Tag", width: 110, template: audTagDisplay, editor: audTagEditor }                
            ],
            //batch: true,
            sortable: true,
            //editable: true,
            
            navigable: true, // enables keyboard navigation            
            pageable: true,
            /*{ refresh: true, pageSizes: true },*/
            height: 430,
            //toolbar: ["save", "cancel"]   // adds save and cancel button                
        });
        function countryName(countryid) {
            for (var i = 0; i < all_countries.length; i++) {
                if (all_countries[i].countryid == countryid) {
                    return all_countries[i].name;
                }
            }
        }
        function countryEditor(container)
        {
            // use a dropdownlist as an editor
            // create an input element with id and name set as the bound field (countryid)
            var input = $('<input id="countryid" name="countryid">');
            // append to the editor container 
            input.appendTo(container);

            // initialize a dropdownlist
            input.kendoDropDownList({
                dataTextField: "name",
                dataValueField: "countryid",
                dataSource: all_countries // bind it to the brands array
            }).appendTo(container);
        }
        function stateName(stateid) {
            for (var i = 0; i < all_states.length; i++) {
                if (all_states[i].stateid == stateid) {
                    return all_states[i].name;
                }
            }
        }
        function stateEditor(container)
        {
            var input = $('<input id="stateid" name="stateid">');
            input.appendTo(container);
            input.kendoDropDownList({
                dataTextField: "name",
                dataValueField: "stateid",
                cascadeFrom: "countryid", // cascade from the brands dropdownlist
                dataSource: all_states // bind it to the models array
            }).appendTo(container);
        }
        function cityName(cityid) {
            for (var i = 0; i < all_cities.length; i++) {
                if (all_cities[i].cityid == cityid) {
                    return all_cities[i].name;
                }
            }
        }
        function cityEditor(container)
        {
            var input = $('<input id="cityid" name="cityid">');
            input.appendTo(container);
            input.kendoDropDownList({
                dataTextField: "name",
                dataValueField: "cityid",
                cascadeFrom: "stateid", // cascade from the brands dropdownlist
                dataSource: all_cities // bind it to the models array
            }).appendTo(container);
        }        
        function decimal3Editor(container, options) {
            $('<input name="' + options.field + '"/>')
                    .appendTo(container)
                    .kendoNumericTextBox({
                        format: "{0:n3}",
                        decimals: 3,
                        step: 0.001
                    });
        }
        function decimal6Editor(container, options) {
            $('<input name="' + options.field + '"/>')
                    .appendTo(container)
                    .kendoNumericTextBox({
                        format: "{0:n6}",
                        decimals: 6,
                        step: 0.000001
                    });
        }
        var AudTagDS = <?php echo $audienceTagList; ?>;
        function audTagDisplay(data) {
            var res = [];
            $.each(data.audiencetag, function(idx, elem) {
                //console.log(idx + " " +JSON.stringify(elem));
                //console.log(elem.text);
                res.push(elem.text);
                //res.push(AudTagDS.get(elem).text);
            });
            return res.join(", ");
        }
        function audTagEditor(container, options) {
            $("<select multiple='multiple' data-bind='value : audiencetag'>")
                    .appendTo(container)
                    .kendoMultiSelect({
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: AudTagDS
                    });
        }                
//        function sizeUnitDropDownEditor(container, options) {
//        $('<input required data-text-field="sizeunit" data-value-field="sizeunitid" data-bind="value:' + options.field + '"/>')
//                .appendTo(container)
//                .kendoDropDownList({
//                    autoBind: false,
//                    dataSource: {
//                        data: <?php echo $sizeUnitList; ?>
//                    }
//                });
//        }
        $(".k-grid-save-changes").html('<span class="k-icon k-cancel"></span> Save as draft');
        $(".k-grid-cancel-changes").click(function() {
            $('#error').html('');
        });        
        $('body').attr('id', "");
        $('body .wrapper').attr('id', 'dashboard');
        $(function(){
            $("#error").html();            
        });

        // Uplaod image
        function uploadImage(id){
            return '<a href="javascript:void(0);" onclick="modelUploadImage('+id+')">Upload Image</a>';
        }
        
        // View Listing
        function viewListing(id){
            return '<a href="<?php echo Yii::app()->urlManager->createUrl('listing'); ?>/'+id+'" target="_blank">View Listing</a>';
        }

        function modelUploadImage(id){
            $('#PreviewModal1').modal({ keyboard: false, backdrop : false });
            $('#saveImage').attr('disabled',false);
            $("#listingId").val(id);
            $.ajax({
                type: "POST",
                url:  "<?php echo Yii::app()->urlManager->createUrl('user/listing/uploadedImage'); ?>",
                data: { id: id},
                beforeSend: function() {
                    $("input:file").attr('disabled', true);
                }
            }) .done(function( msg ) {
                var obj = jQuery.parseJSON(msg);
                $("#uploadedImages").html(obj[0]);
                $("#image_count").val(obj[1]);
                $("input:file").attr('disabled', false);
            });
        }
        $(window).load(function() {
            $('#PreviewModal1 .btn-primary').click(function(){
                $('#PreviewModal1').modal('hide');
                $("#uploadedImageError, #uploadify_filenames_em_, #loadingFilter, #dropzoneQueue, #uploadedImages, #queue").html("");
                $("#uploadify_filenames").val('');
                $("#image_count").val('');
                $("#image_count_uplodify").val('0');
                $("#uploadify_filenames_em_").hide();
            });
            
            $( '.deletePermanentImage' ).live( "click", function(){
                var fileName = $( this ).attr('rel');
                var imageId = $( this ).attr('id');
                var currItem = $( this );
                var listingId = $("#listingId").val();
                var response = confirm("Do you want to delete this image?");
                if(response === true) {
                    $.ajax({
                        type: 'POST',
                        url:  '<?php echo Yii::app()->urlManager->createUrl('uploadify/deletePermanentFile')?>',
                        data: { filename: fileName, listingid:listingId, imageid: imageId},
                        beforeSend: function() {
                            $("#loadingFilter").html("<img src='<?php echo Yii::app()->getBaseUrl()?>/images/site/ajax_loader.gif' alt='ajax_loader' >");
                        }
                    }) .done(function( msg ) {
                        if(msg == "error") {
                            $("#uploadedImageError").html('At least one image is required.');
                        } else {
                            var uploadCount = parseInt($('#image_count').val()) - 1;
                            $('#image_count').val(uploadCount);
                            currItem.parent().html('');
                            // 1 for success 0 otherwise
                        }
                        $("#loadingFilter").html("");
                    });
                } else {
                    $("#uploadedImageError").html('');
                }
            });
            
            $( '#saveImage' ).live( "click", function(){
                var fileNames = $("#uploadify_filenames").val();
                var listingId = $("#listingId").val();
                if(fileNames != '') {
                    $.ajax({
                        type: 'POST',
                        url:  '<?php echo Yii::app()->urlManager->createUrl('user/listing/modelSaveImage')?>',
                        data: { filename: fileNames, listingid:listingId},
                        beforeSend: function() {
                            $('#saveImage').attr('disabled',true);
                            $("#loadingFilter").html("<img src='<?php echo Yii::app()->getBaseUrl()?>/images/site/ajax_loader.gif'>");
                        }
                    }) .done(function( msg ) {
                        $('#PreviewModal1').modal('hide');
                        $("#uploadedImageError, #uploadify_filenames_em_, #loadingFilter, #dropzoneQueue, #uploadedImages, #queue").html("");
                        $("#uploadify_filenames").val('');
                        $("#image_count").val('');
                        $("#image_count_uplodify").val('0');
                        
                        $("#uploadify_filenames_em_").hide();
                        $('#saveImage').attr('disabled',false);
                    });
                } else {
                    $("#uploadify_filenames_em_").html('Please upload atleast one image.').show();
                } // end if
            });
            
        });


    </script>
</div>

    <div class="modal fade imageUpload" id="PreviewModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div id="uploadedImageError" style="color:red;"></div>
                <div id="uploadedImages">
                    <img src='<?php echo Yii::app()->getBaseUrl()?>/images/site/ajax_loader.gif' alt="ajax_loader" >
                </div>
                <div class="col-sm-12 top-gap">
                    <div class="dropzone imagelist"> 
                        <div class="rows" id="dropzoneQueue"> 
                            
                        </div>
                        <div class="clear"></div>
                        <div id="queue" style="margin-top:20px;"></div>
                        <input id="listingId" name="listingId" type="hidden" multiple="true"/>
                        <input id="listingimages" name="listingimages" type="file" multiple="true"/>
                                            
                        <?php $this->widget('Uploadify', array('fileFieldId' => 'listingimages', 'imageType' => 'listing')); ?>
                    </div>
                    
                    <!--<div class="dropzone imagelist"> <div class="rows"> <div class="clear"></div> <p>Choose a default image</p></div><div class="clear"></div>
                        <div id="queue" style="margin-top:20px;"></div>
                        <input id="file_upload" name="file_upload" type="file" multiple="true"/>
                        <a  href="javascript:$('#file_upload').uploadifive('upload')" class="btn btn-lg btn-success upload2 disabled">Upload images</a>
                    </div>-->
                    
                </div>
                <div class="col-sm-9 clearfix container"><button class="btn btn-lg btn-success" id="saveImage">Save</button> <button type="button" class="btn btn-primary">Back</button><span id="loadingFilter"></span></div>
            </div><!-- /.modal-dialog -->
        </div>
    </div>