<?php
/* @var $this ListingController */
/* @var $data Listing */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('byuserid')); ?>:</b>
	<?php echo CHtml::encode($data->byuserid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('foruserid')); ?>:</b>
	<?php echo CHtml::encode($data->foruserid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('length')); ?>:</b>
	<?php echo CHtml::encode($data->length); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('width')); ?>:</b>
	<?php echo CHtml::encode($data->width); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sizeunitid')); ?>:</b>
	<?php echo CHtml::encode($data->sizeunitid); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('basecurrency')); ?>:</b>
	<?php echo CHtml::encode($data->basecurrency); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('priceduration')); ?>:</b>
	<?php echo CHtml::encode($data->priceduration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('otherdata')); ?>:</b>
	<?php echo CHtml::encode($data->otherdata); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('countryid')); ?>:</b>
	<?php echo CHtml::encode($data->countryid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stateid')); ?>:</b>
	<?php echo CHtml::encode($data->stateid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cityid')); ?>:</b>
	<?php echo CHtml::encode($data->cityid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('street')); ?>:</b>
	<?php echo CHtml::encode($data->street); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('geolat')); ?>:</b>
	<?php echo CHtml::encode($data->geolat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('geolng')); ?>:</b>
	<?php echo CHtml::encode($data->geolng); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('zoomlevel')); ?>:</b>
	<?php echo CHtml::encode($data->zoomlevel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lighting')); ?>:</b>
	<?php echo CHtml::encode($data->lighting); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mediatypeid')); ?>:</b>
	<?php echo CHtml::encode($data->mediatypeid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datecreated')); ?>:</b>
	<?php echo CHtml::encode($data->datecreated); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datemodified')); ?>:</b>
	<?php echo CHtml::encode($data->datemodified); ?>
	<br />

	*/ ?>

</div>