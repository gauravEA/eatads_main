<script>
    $('div.wrapper').attr('id', 'dashboard');
</script>

<!-- Fixed navbar -->
<?php $this->renderPartial('/dashboard/_ownerHeader', array('activeMenu' => 'addListing')); ?>

<div class="container main-body">
    <h2><?php echo $listingHeading; ?></h2>
    <div class="row">
        <div class="col-sm-12">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'listing-form',
                // Please note: When you enable ajax validation, make sure the corresponding
                // controller action is handling ajax validation correctly.
                // There is a call to performAjaxValidation() commented in generated controller code.
                // See class documentation of CActiveForm for details on this.
                'enableAjaxValidation' => false,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'afterValidate' => "js: function(form, data, hasError) {
                                    if(!validateAudienceTag()) {                                        
                                        hasError = true;
                                    }
                                    
                                    // Uploadify Image Upload Validtion
                                    if($('#uploadify_filenames').val() == '' && $('#uploaded_filenames').val() == '' && $('#awsuploaded_count').val() < 1) {
                                        $('#uploadify_filenames_em_').html('Please upload atleast one image.').show();
                                        return false;
                                    } else {
                                        $('#uploadify_filenames_em_').html('').hide();
                                    }
                                    
                                    if(!hasError) { // no errors
                                        $('#Listing_countryid').fancyfields('enable');
                                        $('#Listing_stateid').fancyfields('enable');
                                        $('#Listing_cityid').fancyfields('enable');
                                        return true;
                                    }
                                }"
                ),
                'htmlOptions' => array('enctype' => 'multipart/form-data', 'class' => 'listingupload form-signup'),
            ));
            ?>
            <h3><span class="ol">1</span>Media Type</h3>
            <div class="rows">
<?php // echo $form->labelEx($model,'mediatypeid');  ?>
<?php foreach ($mediaTypeList as $mediaType) { ?>
                    <div class="type">
                        <img src="<?php echo Yii::app()->getBaseUrl() . '/images/site/' . $mediaType->logopath; ?>" id="mt_<?php echo $mediaType->id; ?>" alt="<?php echo $mediaType->name; ?>" />
                        <p><?php echo $mediaType->name; ?></p>
                    </div>                                                                  
                <?php } ?>
                <?php
                echo $form->hiddenField($model, 'mediatypeid', array('class' => 'form-control'));
                // code added in script.js
                /* var item_id = $(this).attr('id');
                  item_id = item_id.replace("mt_", "");
                  $('#Listing_mediatypeid').val(item_id);

                  $('#Listing_mediatypeid').val('');
                 */
                ?>
<?php //echo $form->radioButtonList($model,'mediatypeid', $mediaTypeList, array('separator'=>' '));  ?>
<?php // echo $form->error($model,'mediatypeid');  ?>
<?php echo $form->error($model, 'mediatypeid', array('class' => 'errormessage')); ?>
            </div>
            <div class="rows">
                <div class="left">
                    <h3><span class="ol">2</span> Location</h3>
                    <div class="col-sm-6 clearfix">
<?php // echo $form->labelEx($model,'countryid');  ?>
                        <?php echo $form->dropDownList($model, 'countryid', $countryList, array('empty' => 'Select country')); ?>
                        <?php echo $form->error($model, 'countryid', array('class' => 'errormessage')); ?>
                    </div>
                    <div class="col-sm-6 clearfix" >
<?php // echo $form->labelEx($model,'stateid');  ?>                                        
<?php echo $form->dropDownList($model, 'stateid', $stateList, array('empty' => 'Select state')); ?>
                        <?php echo $form->error($model, 'stateid', array('class' => 'errormessage')); ?>

                    </div>
                    <div class="col-sm-6 clearfix">
<?php // echo $form->labelEx($model,'cityid');  ?>
<?php echo $form->dropDownList($model, 'cityid', $cityList, array('empty' => 'Select city')); ?>                                        
<?php echo $form->error($model, 'cityid', array('class' => 'errormessage')); ?>

                    </div>
                    <div class="gps">
                        <div class="col-sm-6 clearfix">
                            <?php echo $form->labelEx($model, 'locality', array('class' => 'top')); ?>
<?php echo $form->textField($model, 'locality', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
<?php // echo $form->error($model,'street');  ?>
<?php echo $form->error($model, 'locality', array('class' => 'errormessage')); ?> 
                        </div>
                        <div class="col-sm-6 ">                                           
                        </div>
                    </div>
                    <div class="gps">
                        <div class="col-sm-6 clearfix">
                            <h4>Enter GPS coordinates</h4>                                            
                        </div>
                            <?php // hidden field added to retain select box disabled state ?>
                        <input type="hidden" id="select_disable_flag" name="select_disable_flag" value="<?php echo $selectDisableFlag; ?>" />
                        <div class="col-sm-6 ">
                            <?php // echo $form->labelEx($model,'geolat', array('class'=>'top')); ?>
                            <?php echo $form->textField($model, 'geolat', array('class' => 'form-control', 'placeholder' => 'Latitude')); ?>
                            <?php // echo $form->error($model,'geolat');  ?>
                            <?php echo $form->error($model, 'geolat', array('class' => 'errormessage')); ?>
                        </div><br clear="all">
                        <div class="col-sm-6 ">
                            <h4>&nbsp;</h4>

                        </div>
                        <div class="col-sm-6 ">
                            <?php // echo $form->labelEx($model,'geolng', array('class'=>'top')); ?>
                            <?php echo $form->textField($model, 'geolng', array('class' => 'form-control', 'placeholder' => 'Longitude')); ?>
                            <?php // echo $form->error($model,'geolng');  ?>
                            <?php echo $form->error($model, 'geolng', array('class' => 'errormessage')); ?>
                        </div>
                        <div class="col-sm-6 ">
<?php echo CHtml::button('Reset', array('class' => 'btn btn-lg btn-success', 'id' => 'reset_geo')); ?>
                        </div>
                    </div>
                </div>
                <div class="right">
                    <h4>Drag the pin to the listings location</h4>
                    <div id="map-canvas" style="height: 300px; width:484px;" ></div>
                </div>
            </div>
            <div class="rows">

                <h3><span class="ol">3</span> Details of specific media</h3>
                <div class="col-sm-6 ">
                    <?php echo $form->labelEx($model, 'name', array('class' => 'top')); ?>
<?php echo $form->textField($model, 'name', array('class' => 'form-control')); ?>
                    <?php // echo $form->error($model,'name');  ?>
                    <?php echo $form->error($model, 'name', array('class' => 'errormessage')); ?>
                </div>
                <div class="col-sm-6 ">
                    <?php echo $form->labelEx($model, 'length', array('class' => 'top')); ?>
<?php echo $form->textField($model, 'length', array('class' => 'form-control')); ?>
                    <?php // echo $form->error($model,'length');  ?>
                    <?php echo $form->error($model, 'length', array('class' => 'errormessage')); ?>
                </div>
                <div class="col-sm-6 ">
                    <?php echo $form->labelEx($model, 'width', array('class' => 'top')); ?>
<?php echo $form->textField($model, 'width', array('class' => 'form-control')); ?>
                    <?php // echo $form->error($model,'width');  ?>
                    <?php echo $form->error($model, 'width', array('class' => 'errormessage')); ?>
                </div>
                <div class="col-sm-6 ">
                    <?php echo $form->labelEx($model, 'sizeunitid', array('class' => 'top')); ?>
<?php echo $form->radioButtonList($model, 'sizeunitid', $sizeUnitList, array('separator' => '&nbsp;&nbsp;', 'class' => 'form-control')); ?>
                    <?php // echo $form->error($model,'sizeunitid');  ?>
                    <?php echo $form->error($model, 'sizeunitid', array('class' => 'errormessage')); ?>
                </div>
                <div class="col-sm-6 ">
                    <?php echo $form->labelEx($model, 'reach', array('class' => 'top', 'label' => $model->getAttributeLabel('reach').' <span class="tool"><span class="tooltip">Approx number of people viewing this site in a week </span></span>')); ?>
                    <?php echo $form->textField($model, 'reach', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>                                    
                    <?php echo $form->error($model, 'reach', array('class' => 'errormessage')); ?>
                </div>
                <div class="col-sm-6 ">
                    <?php echo $form->labelEx($model, 'otherdata', array('class' => 'top')); ?>
<?php echo $form->textField($model, 'otherdata', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
<?php // echo $form->error($model,'otherdata');  ?>
<?php echo $form->error($model, 'otherdata', array('class' => 'errormessage')); ?>
                </div>                                
            </div>
            <div class="rows">
                <div class="col-sm-6 ">
                    <?php echo $form->labelEx($model, 'lightingid', array('class' => 'top')); ?>
<?php echo $form->dropDownList($model, 'lightingid', $lightingList); ?>
                    <?php // echo $form->error($model,'lightingid');  ?>
                    <?php echo $form->error($model, 'lightingid', array('class' => 'errormessage')); ?>
                </div>
                <div class="col-sm-6 ">
                    <?php echo $form->labelEx($model, 'description', array('class' => 'top')); ?>
<?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' => 50, 'class' => 'form-control')); ?>
<?php // echo $form->error($model,'description');  ?>
<?php echo $form->error($model, 'description', array('class' => 'errormessage')); ?>
                </div>
            </div>
            <div class="rows"> 
                <h3><span class="ol">4</span> Audience details (Choose up to 3)</h3>
                <div class="">
<?php //echo $form->labelEx($audTagModel, 'audience tag *', array('class'=>'top'));  ?>
<?php echo $form->checkboxlist($audTagModel, 'id', $audienceTagCheckboxList, array('class' => 'audiencetag', 'template' => '<div class="col-sm-3">{input}{label}</div>', 'separator' => '')); ?>
<?php echo $form->error($audTagModel, 'id', array('class' => 'errormessage')); ?>
                </div>
            </div>
            <div class="rows">
                <h3><span class="ol">5</span> Pricing</h3>
                <div class="col-sm-6 ">
                    <?php echo $form->labelEx($model, 'price', array('class' => 'top')); ?>
<?php echo $form->textField($model, 'price', array('class' => 'form-control')); ?>
                    <?php // echo $form->error($model,'price');  ?>
                    <?php echo $form->error($model, 'price', array('class' => 'errormessage')); ?>
                </div>
                <div class="col-sm-6  clearfix">
                    <?php echo $form->labelEx($model, 'basecurrencyid', array('class' => 'top')); ?>
<?php echo $form->dropDownList($model, 'basecurrencyid', $baseCurrencyList, array('empty' => 'Select base currency')); ?>
<?php // echo $form->error($model,'basecurrency');  ?>
<?php echo $form->error($model, 'basecurrencyid', array('class' => 'errormessage')); ?>
                </div>
            </div>
            <div class="rows">
                <div class="col-sm-7">
<?php echo $form->radioButtonList($model, 'pricedurationid', $priceDurationList, array('separator' => '&nbsp;&nbsp;')); ?>
<?php echo $form->error($model, 'pricedurationid', array('class' => 'errormessage')); ?>
                </div>
            </div>



            <!--                            
                                        <div class="rows">
                                           <h3><span class="ol">5</span> Upload Image</h3>
                                           <div class="col-sm-6 ">
            <?php
            // check browser compitability and remvoe uplodify for IE 9 
//                                        $browserData = JoyUtilities::getBrowser();
//                                        if($browserData['name'] == 'Internet Explorer' && ( (int)$browserData['version'] <= 9  ) ) {
//                                            echo "Our file uploader is not compatible with your browser. Please upgrade to IE10 or Download Chrome.";
//                                        } else { 
            ?>
                                                    <input id="listingimages" name="listingimages" type="file" multiple="true">
<?php //$this->widget('Uploadify', array('fileFieldId' => 'listingimages', 'imageType' => 'listing') );  ?>
<?php //  }  ?>
                                                    
                                           </div>
                                        </div>
            -->

            <div class="rows">

                <h3><span class="ol">6</span>Images</h3>
                <div class="col-sm-12 top-gap">

                    <?php
                    // check browser compitability and remvoe uplodify for IE 9 
                    $browserData = JoyUtilities::getBrowser();
                    if ($browserData['name'] == 'Internet Explorer' && ( (int) $browserData['version'] <= 9 )) {
                        echo "Our file uploader is not compatible with your browser. Please upgrade to IE10 or Download Chrome.";
                    } else {
                        ?>
                        <div class="dropzone imagelist"> <div class="rows"> <div class="clear"></div> <p></p></div><div class="clear"></div>
                            <div id="queue" style="margin-top:20px;"></div>
                            <input id="listingimages" name="listingimages" type="file" multiple="true"/>

    <?php $this->widget('Uploadify', array('fileFieldId' => 'listingimages', 'imageType' => 'listing')); ?>
                        </div>
<?php } ?>

                </div>
            </div>

                    <?php if (isset($listingImages) && count($listingImages)) { ?>
                <div class="rows">
                    <h3><span class="ol">7</span> Uploaded Image</h3>
                    <div class="col-sm-6 ">
    <?php
    foreach ($listingImages as $image) {
        ?>
                            <div style="float:left; padding-right:5px;">
                                <img src="<?php echo JoyUtilities::getAwsFileUrl("tiny_" . $image->filename); ?>" alt="<?php echo $image->filename; ?>" />
                                <div class="deletePermanentImage" id="<?php echo $image->id; ?>" rel="<?php echo $image->filename; ?>" style="cursor:pointer;">X</div>
                            </div>
    <?php } ?>
                        <div style="clear:both"></div>
                        <div id="uploadedImageError" style="color:red;"></div>
                    </div>
                </div>
            <?php } ?>


                    <?php if (isset($alreadyUploadedArr) && count($alreadyUploadedArr)) { ?>
                <div class="rows">
                    <h3><span class="ol">8</span> Pushed to Our Server </h3>
                    <div class="col-sm-6 ">
                <?php
                    foreach ($alreadyUploadedArr as $image) { ?>
                            <div style="float:left; padding-right:5px;">
                                <img width="100" height="100" src="<?php echo Yii::app()->getBaseUrl() . '/uploads/listing/' . $image; ?>" alt="<?php echo $image; ?>" />
                                <div class="deleteImage" rel="<?php echo $image; ?>" style="cursor:pointer;">X</div>
                            </div>
                <?php } ?>
                    </div>
                </div>
                <?php } ?>

            <div class="col-sm-12">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-lg btn-success big')); ?>
            </div>

<?php $this->endWidget(); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    function bindSelectBox()
    {
        // AJAX country field
        $('#Listing_countryid').fancyfields("bind", "onSelectChange", function(input, text, val) {
            //alert("Current input - ID : "+input.attr("id") + " , selected text : " + text + " , value : " + val);
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->urlManager->createUrl('ajax/dynamicstates'); ?>", //url to call.
                cache: true,
                data: {id: val},
                success: function(data) {
                    $('#Listing_stateid').setOptions(eval(data));
                    $('#Listing_cityid').setOptions([["Select city", ""]]);
                }
            });
        });

        // AJAX state field
        $('#Listing_stateid').fancyfields("bind", "onSelectChange", function(input, text, val) {
            //alert("Current input - ID : "+input.attr("id") + " , selected text : " + text + " , value : " + val);
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->urlManager->createUrl('ajax/dynamiccities'); ?>", //url to call.
                cache: true,
                data: {id: val},
                success: function(data) {
                    $('#Listing_cityid').fancyfields("enable");
                    $('#Listing_cityid').setOptions(eval(data));
                }
            });
        });
    }
    $(function() {
        $('.col-sm-3 input[type="checkbox"]').fancyfields();

        // in case of update listing, selected media type
        var media_type_id = $('#Listing_mediatypeid').val();        
        if(media_type_id>0) {
            $('.listingupload .rows .type img').removeClass('selected').css('opacity', '0.5');
            $('#mt_'+media_type_id).addClass('selected').css('opacity', '1');            
        }

        // audience tag            
        count_checked = $(".audiencetag:checked").length;
        if (count_checked >= 3) {
            $(".audiencetag").not(":checked").attr("disabled", true);
        } else {
            $(".audiencetag").not(":checked").attr("disabled", false);
        }

        var select_flag = $('#select_disable_flag').val();
        if (select_flag == 1) {
            $('#Listing_countryid').fancyfields("disable");
            $('#Listing_stateid').fancyfields("disable");
            $('#Listing_cityid').fancyfields("disable");
        } else {
            $('#Listing_countryid').fancyfields("enable");
            $('#Listing_stateid').fancyfields("enable");
            $('#Listing_cityid').fancyfields("enable");
        }

        bindSelectBox();
        // reset geo fields
        $('#reset_geo').click(function() {
            $('#Listing_geolat').val('');
            $('#Listing_geolng').val('');
            $('#Listing_locality').val('');
            $.ajax({
                type: "GET",
                url: "<?php echo Yii::app()->urlManager->createUrl('ajax/dynamiccountries'); ?>", //url to call.
                cache: true,
                success: function(data) {
                    //;
                    //$('#Listing_stateid').fancyfields("bind","onSelectChange");
                    $('#Listing_countryid').fancyfields("enable");
                    $('#Listing_stateid').fancyfields("enable");
                    $('#Listing_cityid').fancyfields("enable");
                    $('#Listing_countryid').setOptions(eval(data));
                    $('#Listing_stateid').setOptions([["Select state", ""]]);
                    $('#Listing_cityid').setOptions([["Select city", ""]]);
                    bindSelectBox();
                    $('#select_disable_flag').val('0');
                }
            });
        });

        // length width only 3 digits after decimal
        $('#Listing_length,#Listing_width').keyup(function() {
            if ($(this).val().indexOf('.') != -1) {
                if ($(this).val().split(".")[1].length > 3) {
                    if (isNaN(parseFloat(this.value)))
                        return;
                    this.value = parseFloat(this.value).toFixed(3);
                }
            }
            return this; //for chaining
        });

        // length width only 3 digits after decimal
        /*$('#Listing_price').keyup(function(){
         if($(this).val().indexOf('.')!=-1){         
         if($(this).val().split(".")[1].length > 2){
         if( isNaN( parseFloat( this.value ) ) ) return;
         this.value = parseFloat(this.value).toFixed(2);
         }  
         }            
         return this; //for chaining
         });*/

        // Add hidden field to keep uploaded file name but after validation error keep here
        $('.upload').after('<input type=\'hidden\' id=\'uploaded_filenames\' value=\'<?php echo $uploaded_filenames; ?>\' name=\'uploaded_filenames\' />');

        // Add hidden field to keep aws uploaded file name but after validation error keep here
        $('.upload').after('<input type=\'hidden\' id=\'awsuploaded_count\' value=\'<?php echo count($listingImages); ?>\' name=\'awsuploaded_count\' />');

        // audience tag
        //$(".audiencetag").click(function(){                
        $(".audiencetag").fancyfields("bind", "onCheckboxChange", function(input, isChecked) {
            var count_checked = $(".audiencetag:checked").length;
            //console.log("count - " + count_checked);
            if (count_checked >= 3) {
                //$(".audiencetag").not(":checked").attr("disabled", true);
                $(".audiencetag").not(":checked").fancyfields("disable");
            } else {
                // $(".audiencetag").not(":checked").attr("disabled", false);
                $(".audiencetag").not(":checked").fancyfields("enable");
            }
        });

        //$('#stateid').fancyfields("disable");
        //$('#cityid').fancyfields("disable");            
        // AJAX city field
        $('#Listing_countryid').fancyfields("bind", "onSelectChange", function(input, text, val) {
            //alert("Current input - ID : "+input.attr("id") + " , selected text : " + text + " , value : " + val);                
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->urlManager->createUrl('ajax/dynamicstates'); ?>", //url to call.
                cache: true,
                data: {id: val},
                success: function(data) {
                    $('#Listing_stateid').fancyfields("enable");
                    $('#Listing_stateid').setOptions(eval(data));
                    $('#Listing_cityid').setOptions([["Select city", ""]]);
                }
            });
        });
        // AJAX state field
        $('#Listing_stateid').fancyfields("bind", "onSelectChange", function(input, text, val) {
            //alert("Current input - ID : "+input.attr("id") + " , selected text : " + text + " , value : " + val);
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->urlManager->createUrl('ajax/dynamiccities'); ?>", //url to call.
                cache: true,
                data: {id: val},
                success: function(data) {
                    $('#Listing_cityid').fancyfields("enable");
                    $('#Listing_cityid').setOptions(eval(data));
                }
            });
        });
    });

<?php if ($model->id) { ?>
        // This will come only in edit case
        $('.deletePermanentImage').click(function() {
            var fileName = $(this).attr('rel');
            var imageId = $(this).attr('id');
            var currItem = $(this);

            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->urlManager->createUrl('uploadify/deletePermanentFile') ?>',
                data: {filename: fileName, listingid: <?php echo $model->id; ?>, imageid: imageId}
            }).done(function(msg) {
                if (msg == "error") {
                    $("#uploadedImageError").html('At least one image is required.');
                } else {
                    $('#awsuploaded_count').val($('#awsuploaded_count').val() - 1);
                    currItem.parent().html('');
                }
                // 1 for success 0 otherwise
            });
        });
<?php } ?>

    $('.deleteImage').click(function() {
        var fileName = $(this).attr('rel');
        var currItem = $(this);

        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->urlManager->createUrl('uploadify/deleteFile') ?>',
            data: {filename: fileName}
        }).done(function(msg) {

            currItem.parent().html('');
            // 1 for success 0 otherwise
            setTimeout(function() {
                getUploadedRemoveFileList();
                // 1000 for 1 seconds
            }, 1000);

        });
    });

    function getUploadedRemoveFileList() {
        var fileArr = [];
        $('.deleteImage').each(function(index) {
            fileArr.push($(this).attr('rel'));
        });
        fileArr.join(',');
        $('#uploaded_filenames').val(fileArr);
    }
    function validateAudienceTag()
    {
        count_checked = $(".audiencetag:checked").length;
        //console.log(count_checked);
        //return false;
        if (count_checked > 3) {
            $("div#AudienceTag_id_em_").show();
            $("div#AudienceTag_id_em_").html('You can not select more than 3 audiecne tag.');
            return false;
        } else if (count_checked < 1) {
            $("div#AudienceTag_id_em_").show();
            $("div#AudienceTag_id_em_").html('Please select at least one audience tag.');
            return false;
        }
        return true;
    }
</script>

<script>
//    $(document).ready(function() {
//        $('.dropzone .rows').hide();
//        $('#file_upload').uploadifive({
//            'auto': false,
//            //   'checkScript': 'check-exists.php',
//            'formData': {
//                'timestamp': '1389950856',
//                'token': 'dfdd0d4b49880ec7d317089ce3ac9514'
//            },
//            'queueID': 'queue',
//            'uploadScript': '<?php //echo Yii::app()->urlManager->createUrl('uploadify/uploadFive'); ?>',
//            'onUploadComplete': function(file, data) {
//                $('.dropzone .rows').show();
//                $('.dropzone .rows').prepend('<div class="type" rel="' + data + '"style="background:url(' + data + ') no-repeat top left; background-size:130px"><div class="delete"></div></div>');
//            }
//        });
//
//        $('.imagelist .type .delete').live("click", function() {
//
//            $that = $(this);
//            $.ajax({
//                type: "POST",
//                url: '<?php echo Yii::app()->urlManager->createUrl('uploadify/deleteFile'); ?>',
//                data: {name: $(this).parent().attr('rel')},
//                success: function(result) {
//                    $that.parent().fadeOut().remove();
//                    if ($('.imagelist .type').length) {
//
//                    }
//                    else {
//                        $('.dropzone .rows').hide();
//                    }
//
//                }
//            });
//
//        })
//
//    });
</script>
