<?php
/* @var $this ListingController */
/* @var $model Listing */

$this->breadcrumbs=array(
	'Listings'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Listing', 'url'=>array('index')),
	array('label'=>'Create Listing', 'url'=>array('create')),
	array('label'=>'Update Listing', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Listing', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Listing', 'url'=>array('admin')),
);
?>

<h1>View Listing #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'byuserid',
		'foruserid',
		'name',
		'length',
		'width',
		'sizeunitid',
		'basecurrencyid',
		'price',
		'pricedurationid',
		'otherdata',
		'countryid',
		'stateid',
		'cityid',
		'street',
		'geolat',
		'geolng',
		'zoomlevel',
		'lightingid',
		'mediatypeid',
		'description',
		'status',
		'datecreated',
		'datemodified',
	),
)); ?>
