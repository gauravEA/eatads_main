<?php $this->renderPartial('../dashboard/_ownerHeader', array('activeMenu' => 'massListing')); ?>
<div class="container main-body">


    <h2>Mass Upload</h2>
    <div class="row">
        <?php if($errorStr) { ?>
            <div style="color:red;">
            <?php echo $errorStr; ?> 
                <br /><br />
            </div>
        <?php } ?>
        
        <div class="col-sm-9">            
                <h3><span class="ol">1</span> Download the template</h3>
                <div class="rows">
                    <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Donec id elit non mi porta gravida at eget metus. Aenean lacinia bibendum nulla sed consectetur. Curabitur blandit tempus porttitor.</p>
                    <div class="download-box">
                        <a href="<?php echo  Yii::app()->baseUrl. '/MassUploadExcelTemplate.xlsx'; ?>" class="excel">Excel template</a>
                        <a target="_blank" href="<?php echo  Yii::app()->baseUrl. '/MassUploadGuide.pdf'; ?>" class="guide">Guide</a>
                    </div>
                </div>

                <h3><span class="ol">2</span> Upload the template</h3>
                <div class="rows">
                    <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Donec id elit non mi porta gravida at eget metus. Aenean lacinia bibendum nulla sed consectetur. Curabitur blandit tempus porttitor.</p>
                    <?php $form = $this->beginWidget(
                                'CActiveForm',
                                array(
                                    'id' => 'mass-upload-form',
                                    'enableAjaxValidation' => false,
                                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                                )
                            );
                    ?>
                        <div class="upload-holder">  
                            
<!--                            
                            <div class="customfile">
                                <span aria-hidden="true" class="customfile-button btn btn-primary big">Browse</span>
                                <span aria-hidden="true" class="customfile-feedback">You may only upload .csv files</span>
                                <input type="file" id="mass-uploader" class="customfile-input" style="left: 210px; top: -2px;" title="" data-original-title="">
                            </div>
-->
                            <?php echo $form->fileField($model,'file_upload', array('' => '')); // 'id'=>'mass-uploader'?>                            
                            <?php echo $form->error($model,'file_upload', array('class'=>'errormessage')); ?>
                            <br />
                        </div>
                    
                    
                        <?php echo CHtml::submitButton('Upload', array('class' => 'btn btn-lg btn-success big')); ?>
                    
                    <?php $this->endWidget(); ?>
                    
                </div>
            
        </div>
    </div>
</div>

<script>
    $('body').attr('id', "dashboard");
</script>