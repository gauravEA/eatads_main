<?php $this->renderPartial('_buyerHeader', array('activeMenu' => 'home')); ?>
<style>
    .optionWidth { width:148px; margin-right: 17px; font-size: 16px; float:left; }
</style>
<script>
    $('body').addClass('views favourites');
    $('body').removeAttr('id');
    $('body .wrapper').attr('id', 'dashboard');
</script> 
<div class="container main-body dashboard">
    <div class="row">
        <div class="circle red">
            <h6><?php echo $rfpCount; ?></h6>
            <p>RFP Sent</p>
        </div>        
        <div class="circle yellow last-child ">
            <h6><?php echo $mediaPlanCount; ?></h6>
            <p>Media Plans</p>
        </div>
        <div class="circle blue">
            <h6><?php echo $favListingCount; ?></h6>
            <p>Favorite Listings</p>
        </div>                
    </div>
</div>


<div class="container main-body">



    <div class="row">
        <?php /*
        <div class="row col-sm-12 left country_listing">
            <h2>Browse Countries</h2>
            <div>
                <?php 
                    foreach($countryArr as $key => $countryData) {
                        //print_r($countryData->attributes);
                        echo "<div class='optionWidth'><a href='". Yii::app()->urlManager->createUrl('listing/area', array('countryid' => $countryData->id) ) ."'>". $countryData->name ."</a></div>";
                    }
                ?>
            </div>
            <div style="clear:both"></div>
        </div>
        */ ?>
    </div>
</div>
