<div class="navbar navbar-second navbar-static-top ">
    <div class="container">
        <div class="left">
            <h3>
<!--                <img src="<?php echo Yii::app()->baseUrl ; ?>/images/site/john_doe.png">-->
                Welcome, <?php echo Yii::app()->user->name . ' '. Yii::app()->user->lname; ?></h3>
        </div>
        <div class="right">
            <ul> 
                <li <?php if($activeMenu == "home") { ?>class="active" <?php  } ?> ><a href="<?php echo JoyUtilities::getDashboardUrl(); ?>">Dashboard</a></li>
                <li <?php if($activeMenu == "mediaPlan") { ?>class="active" <?php  } ?> ><a href="<?php // echo Yii::app()->urlManager->createUrl('user/mediaplans'); ?><?php echo JoyUtilities::getDashboardUrl().'/media-plans'; ?>">Media Plans</a></li>                
                <li <?php if($activeMenu == "fav") { ?>class="active" <?php  } ?> ><a href="<?php // echo Yii::app()->urlManager->createUrl('user/favourites'); ?><?php echo JoyUtilities::getDashboardUrl().'/favourites'; ?>">Favorites</a></li>
                <li <?php if($activeMenu == "editProfile") { ?>class="active" <?php  } ?> ><a href="<?php // echo Yii::app()->urlManager->createUrl('user/profile/edit'); ?><?php echo JoyUtilities::getDashboardUrl().'/profile'; ?>">Edit Profile</a></li>
            </ul>
        </div>
    </div>
</div>