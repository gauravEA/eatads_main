<?php $this->renderPartial('_ownerHeader', array('activeMenu' => 'home')); ?>

<script>
    $('body').addClass('views');
    $('body').removeAttr('id');
    $('body .wrapper').attr('id', 'dashboard');
</script>

<div class="navbar navbar-second navbar-static-top dashboard ">
    <div class="container">

        <div class="left">
            <h3>
                You have <?php echo $listingCount; ?> active <?php echo $listingCount > 1 ? 'listings' : 'listing' ?>  <?php if($listingCount) { ?><a href="<?php // echo Yii::app()->urlManager->createUrl('user/listing/manage'); ?><?php echo JoyUtilities::getDashboardUrl().'/manage-media'; ?>">Listings Manager</a> <?php } ?>
            </h3>
        </div>
        <div class="right">
            <a href="<?php // echo Yii::app()->urlManager->createUrl('user/listing/create'); ?><?php echo JoyUtilities::getDashboardUrl().'/add'; ?>" class="btn btn-primary">Add Listing</a>
<!--            <a href="<?php echo Yii::app()->urlManager->createUrl('user/listing/massupload'); ?>" class="btn btn-primary">Mass Upload</a>-->
        </div>
    </div>
</div>
<div class="container main-body dashboard">
    <div class="row">
        <?php   
            $loggedInUserId = Yii::app()->user->id;
            $circleClass = array('red', 'green', 'blue', 'yellow last-child ');
            $i = 0;
            $listingCount = 0;
            foreach($mediaTypeList as $mediaType) {
                //echo $mediaType->id.' - '.$mediaType->name;
                $listingForMediaTypeCount = Listing::getListingCount($mediaType->id, $loggedInUserId);
                if($listingForMediaTypeCount>0) {
                    echo "<div class=\"circle {$circleClass[$i]}\"><h6>{$listingForMediaTypeCount}</h6><p>{$mediaType->name}</p></div>";
                    if($i==3) 
                        $i=0;
                    else 
                        $i++;
                    $listingCount++;
                }
            }            
            if($listingCount==0) {
                // echo "You don't have any listings added on eatads. <a href='".Yii::app()->urlManager->createUrl('user/listing/create')."'>Click here</a> to add your first listing.";             
                echo "You don't have any listings added on eatads. <a href='".JoyUtilities::getDashboardUrl().'/add'."'>Click here</a> to add your first listing.";
            }
            //echo '<pre>';
            //print_r($mediaTypeList);
        ?>
        <!--
        <div class="circle red">
            <h6>20</h6>
            <p>something</p>
        </div>
        
        <div class="circle green">
            <h6>20</h6>
            <p>something</p>
        </div>
        <div class="circle blue">
            <h6>20</h6>
            <p>something</p>
        </div>
        <div class="circle yellow last-child ">
            <h6>20</h6>
            <p>something</p>
        </div>
        -->
    </div>
</div>

<!-- <div class="navbar navbar-second navbar-static-top home-bar ">
    <div class="container ">
        <?php //$this->widget('AdvanceSearch', array('renderBackground'=>true, 'header' => false)); ?>
    </div>
</div> -->
<div class="container main-body">
</div>
