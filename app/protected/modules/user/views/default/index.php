<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	$this->module->id,
);
?>
<h1><?php echo 'Media Owner Dashboard'; ?></h1>

<?php echo CHtml::link('Listing',array('listing/index'));?><br />
<?php echo CHtml::link('Add Listing',array('listing/create'));?><br />
<?php echo CHtml::link('Manage Listing',array('listing/admin'));?><br />

<p>
This is the view content for action "<?php echo $this->action->id; ?>".
The action belongs to the controller "<?php echo get_class($this); ?>"
in the "<?php echo $this->module->id; ?>" module.
</p>
<p>
You may customize this page by editing <tt><?php echo __FILE__; ?></tt>
</p>