<?php
$currentListing = ($widget->dataProvider->getPagination()->pageSize * $widget->dataProvider->getPagination()->getCurrentPage()+1) + $index++;
$curPage = $widget->dataProvider->getPagination()->getCurrentPage()+1;


if(!empty($data->listing->listingImages[0]->filename)) {
    $fileName = $data->listing->listingImages[0]->filename;
    $imagePath = JoyUtilities::getAwsFileUrl('tiny_'.$fileName, 'listing');
    if(empty($imagePath)) {
        $imagePath = Yii::app()->baseUrl. '/images/site/img.png';
    }
} else {
    $imagePath = Yii::app()->baseUrl. '/images/site/img.png';
}     
?>
<div id="<?php echo $data->id; ?>" class="left list-row">
    <div class="col-sm-7">
        <img src="<?php echo $imagePath; ?>" alt="listing_image" >
        <h3>
            <a href='<?php echo Yii::app()->urlManager->createUrl('listing/view', array('id' => $data->listing->id)); ?>'><?php echo $data->listing->name; ?></a><br/>
            <span><?php echo $data->listing->mediatype->name; ?><br/><?php echo JoyUtilities::formatNumber($data->listing->length, 2) . ' x ' . JoyUtilities::formatNumber($data->listing->width,2) . ' ' . Listing::getSizeUnit($data->listing->sizeunitid); ?><sup>2</sup></span>
        </h3>
    </div>
    <div class="col-sm-3">
<?php $priceDuration = Listing::getPriceDuration($data->listing->pricedurationid); ?>            
        <h4><?php echo '<span class="dyn_ccode">'.$this->ipCurrencyCode .'</span> <span class="dyn_price">'. CHtml::encode(JoyUtilities::formatNumber(Yii::app()->openexchanger->convertCurrency($data->listing->price, $data->listing->basecurrency->currency_code, $this->ipCurrencyCode))) .'</span> / '. $priceDuration; ?>
            <br /><span><?php echo '<span class="base_ccode">'.$data->listing->basecurrency->currency_code .'</span> <span class="base_price">'. CHtml::encode(JoyUtilities::formatNumber($data->listing->price)) .'</span> / '. $priceDuration; ?></span>
        </h4>        
    </div>

    <div class="col-sm-3 right">
        <!--        <a href="#" class="add2fav"></a>-->       
        <span class="btn btn-primary counter"><?php echo $currentListing; ?></span><br/><a href="javascript:void(0);" onclick="removeFav(<?php echo $data->id; ?>, '', <?php echo $curPage; ?>);" class="remove">Remove from favorites</a>
    </div>
</div>
<div style="clear:both;"></div>
<div  class="hr"></div>
