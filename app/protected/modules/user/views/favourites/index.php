<?php
$roleId = Yii::app()->user->roleId;
if ($roleId == 3) {
    $this->renderPartial('/dashboard/_ownerHeader', array('activeMenu' => 'fav'));
} elseif ($roleId == 2) {
    $this->renderPartial('/dashboard/_buyerHeader', array('activeMenu' => 'fav'));
}
?>

<?php
$mosaicTabClass = $listTabClass = '';
if ($listType == 'm') {
    $mosaicTabClass = 'active';
    $mosaicContainerCss = 'mosaic left';
} else {
    $listTabClass = 'active';
}
?>

<div class="container main-body">
    <div class="viewNavigation">
        <ul>
            <li class="<?php echo $listTabClass; ?>"><a id="list" class="list" href="javascript:void(0);">List view</a></li>
<!--            <li class="<?php echo $listTabClass; ?>"><a id="list" class="list" href="<?php echo Yii::app()->urlManager->createUrl('user/favourites/index'); ?>">List view</a></li>            -->
            <li class="<?php echo $mosaicTabClass; ?>"><a href="javascript:void(0);" id="mosaic" class="mosaic">Mosaic view</a></li>
<!--            <li class="<?php echo $mosaicTabClass; ?>"><a href="<?php echo Yii::app()->urlManager->createUrl('user/favourites/index', array('lt' => 'm')); ?>" class="mosaic">Mosaic view</a></li>-->
        </ul>
        <div class="right"><div class="text-search">
                <input type="text" id="searchText" placeholder="search in the results">
                <button id="search" class="btn btn-search" />
            </div>
        </div>
    </div>
    <div class="hr"></div>

    <?php if ($listType == 'm') { ?>
        <div class="row mosaic left">
            <div class="col-sm-12 left">
                <div class="row left">
                <?php } else { ?>                            
                    <div class="row">
                    <?php } ?>

                    <?php $this->renderPartial('_favGrid', array('dataProvider' => $dataProvider, 'listType' => $listType, 'renderView' => $renderView)); ?>  

                    <?php if ($listType == 'm') { ?>
                    </div>

                </div>
            </div>
        <?php } else { ?>                            
        </div>
    <?php } ?>

</div>

<script>
    $('body').addClass("views favourites");
    $('body').attr('id', "dashboard");
    
    function removeFav(id, listtype, curpage) {        
        $.ajax({
            url: '<?php echo Yii::app()->createUrl("user/favourites/removeFav"); ?>',
            type: "POST",
            data: { id: id }
        }).done(function(data) {
            var textsearch = $("#searchText").val();            
            $.fn.yiiListView.update("listViewId", {
                    url: "?&lt="+listtype+"&textsearch="+textsearch+"&FavouriteListing_page="+curpage
                }
            );
        }); 
    }
</script>

<?php
$btnFilter = '$("#search, #mosaic, #list").click(function(){
                            searchData($(this));
                        });';
Yii::app()->clientScript->registerScript('listFilter', $btnFilter, CClientScript::POS_END);
?>

<script>
    
    $('#searchText').keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
         {
           searchData($(this));
         }
    });   
    
    function searchData(search) {
        listType = "";    
        if( search.attr("id") == "list" ) { // clicked on list view
            search.parent().addClass("active");
            $("#mosaic").parent().removeClass("active");
        } else if(search.attr("id") == "mosaic") { // clicked on mosaic view
            search.parent().addClass("active");
            $("#list").parent().removeClass("active");
            listType = "m";
        } else { // clicked on search button
            if( $("#mosaic").parent().attr("class") == "active" ) {
                listType = "m";
            }
        }

        textsearch = $("#searchText").val();
        $.fn.yiiListView.update("listViewId", {
                url: "?&lt="+listType+"&textsearch="+textsearch,
            }
        );
        return false;
    }
</script>