<!--HEADER-->
<?php include_once "header.php"; ?>
<!--HEADER-->

<!--main content start-->
    <section id="main-content">
        <!--CONTENT-->
        <?php echo $content; ?>
        <!--CONTENT_END-->
    </section>
<!--main content end-->

<!--FOOTER-->
    <?php // include_once "footer.php"; ?>
<!--FOOTER_END-->