<!DOCTYPE html5>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Graphisads</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
         <!-- Bootstrap core CSS -->
         <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <?php
            $theme = Yii::app()->theme;
            $cs = Yii::app()->clientScript;
            $cs->registerCssFile( $theme->getBaseUrl() . '/css/bootstrap.min.css' );
            $cs->registerCssFile( $theme->getBaseUrl() . '/css/bootstrap-theme.min.css' );
            $cs->registerCssFile( $theme->getBaseUrl() . '/css/main.css' );
            $cs->registerCssFile( $theme->getBaseUrl() . '/css/datepicker.css' );
        $cs->registerCssFile( $theme->getBaseUrl() . '/css/slider.css' );
             
        
          $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/jquery-1.11.0.min.js');
          $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/bootstrap.js');
        $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js');
        
        $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/dust/dust-full-2.2.0.js');
        $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/dust/dust-helpers-1.1.1.js');
        
         
        ?>
       <?php Yii::app()->clientScript->registerScriptFile("https://maps.googleapis.com/maps/api/js", CClientScript::POS_BEGIN); ?>
        <?php
            $theme = Yii::app()->theme;
            $cs = Yii::app()->clientScript;
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/Custom/map.js', CClientScript::POS_BEGIN); 
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/Custom/main.js', CClientScript::POS_BEGIN);
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/Custom/bootstrap-datepicker.js', CClientScript::POS_BEGIN);
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/Custom/bootstrap-slider.js', CClientScript::POS_BEGIN);
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/Custom/template/site_detail_view.tl', CClientScript::POS_BEGIN);
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/Custom/template/listings.tl', CClientScript::POS_BEGIN);
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/Custom/template/myplans.tl', CClientScript::POS_BEGIN);
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/Custom/template/popover_plans.tl', CClientScript::POS_BEGIN);
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/jquery.infinitescroll.js', CClientScript::POS_BEGIN);
        ?>
<script src="<?php echo Yii::app()->params['protocol']; ?>maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.geocomplete.js', CClientScript::POS_BEGIN); ?>

<!--        <script src="twitter-bootstrap-v2/docs/assets/js/bootstrap-transition.js"></script>-->
    
<script>
     dataLayer = [];
</script>
    </head>

    <body>

        
        
        <nav class="navbar navbar-default navbar-static-top" role="navigation" id="top-header">
  <div class="container-fluid">
    <div class="row">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
 
        <div class="dropdown plan-header-dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="selectedplan">Louis Vuitton Outdoor Delhi<b class="caret"></b></a>
          <ul class="dropdown-menu" id="myplans_id">
            <li><a href="#">Plan 2 Lorem Ipsum</a></li>
            <li><a href="#">Plan 3 Lorem</a></li>
            <li><a href="#">Plan 4 Lorem</a> </li>
          </ul>
        </div>
    </div>
          
      <ul class="nav navbar-nav navbar-left plan-details">  
        <li>
            <h4><span>No. of Sites</span><span class="semi-bold" id="totalsites"> 49</span></h4>
        </li>
        <li>
            <h4><span>Total Pricing</span><span class="semi-bold" id="totalPrice"> INR 960,000</span></h4>
        </li>     
      </ul>


    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          
      <ul class="nav navbar-nav navbar-right plan-action-items">  
        <li>
            <button class="btn btn-primary-custom1">Send RFP</button>  
        </li>
        <li>
          <a href="#" class="glyphicon glyphicon-share glyphicon-medium"></a>
        </li>
        <li>
          <a href="#" class="glyphicon glyphicon-download glyphicon-medium"></a>
        </li>      
      </ul>
    </div><!-- /.navbar-collapse -->
    </div>      
  </div><!-- /.container-fluid -->
</nav>
