<!--footer start-->
            <footer class="site-footer">
                <div class="text-center">
                    <?php echo date("Y");?> &copy; EatAds.com - All Rights Reserved.
                    <a href="#" class="go-top">
                        <i class="fa fa-angle-up"></i>
                    </a>
                </div>
            </footer>
            <!--footer end-->
        </section>

        <?php
            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/jquery.js', CClientScript::POS_END );
            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/bootstrap.min.js', CClientScript::POS_END );
            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/jquery.scrollTo.min.js', CClientScript::POS_END );
            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/jquery.nicescroll.js', CClientScript::POS_END );
            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/jquery-ui-1.9.2.custom.min.js', CClientScript::POS_END );
            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/jquery.dcjqaccordion.2.7.js', CClientScript::POS_END );
        ?>

        <!--custom switch-->
        <?php $cs->registerScriptFile( $theme->getBaseUrl() . '/js/bootstrap-switch.js', CClientScript::POS_END ); ?>
        
        <!--custom tagsinput-->
        <?php $cs->registerScriptFile( $theme->getBaseUrl() . '/js/jquery.tagsinput.js', CClientScript::POS_END ); ?>

        <!--custom checkbox & radio-->
        <?php 
            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/ga.js', CClientScript::POS_END ); 
            $cs->registerScriptFile( $theme->getBaseUrl() . '/assets/bootstrap-datepicker/js/bootstrap-datepicker.js', CClientScript::POS_END ); 
            $cs->registerScriptFile( $theme->getBaseUrl() . '/assets/fuelux/js/spinner.min.js', CClientScript::POS_END );             
            $cs->registerScriptFile( $theme->getBaseUrl() . '/assets/bootstrap-fileupload/bootstrap-fileupload.js', CClientScript::POS_END ); 
            $cs->registerScriptFile( $theme->getBaseUrl() . '/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js', CClientScript::POS_END ); 
            $cs->registerScriptFile( $theme->getBaseUrl() . '/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js', CClientScript::POS_END );             

            $cs->registerScriptFile( $theme->getBaseUrl() . '/assets/bootstrap-datepicker/js/bootstrap-datepicker.js', CClientScript::POS_END ); 
            $cs->registerScriptFile( $theme->getBaseUrl() . '/assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', CClientScript::POS_END ); 
            $cs->registerScriptFile( $theme->getBaseUrl() . '/assets/bootstrap-daterangepicker/moment.min.js', CClientScript::POS_END );             
            $cs->registerScriptFile( $theme->getBaseUrl() . '/assets/bootstrap-daterangepicker/daterangepicker.js', CClientScript::POS_END ); 
            $cs->registerScriptFile( $theme->getBaseUrl() . '/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js', CClientScript::POS_END ); 
            $cs->registerScriptFile( $theme->getBaseUrl() . '/assets/bootstrap-timepicker/js/bootstrap-timepicker.js', CClientScript::POS_END );             
            $cs->registerScriptFile( $theme->getBaseUrl() . '/assets/jquery-multi-select/js/jquery.multi-select.js', CClientScript::POS_END ); 
            $cs->registerScriptFile( $theme->getBaseUrl() . '/assets/jquery-multi-select/js/jquery.quicksearch.js', CClientScript::POS_END ); 
            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/advanced-form-components.js', CClientScript::POS_END );             
            
            $cs->registerScriptFile( $theme->getBaseUrl() . '/assets/bootstrap-inputmask/bootstrap-inputmask.min.js', CClientScript::POS_END );             
            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/jquery.dcjqaccordion.2.7.js', CClientScript::POS_END );
            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/jquery.scrollTo.min.js', CClientScript::POS_END );
            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/jquery.nicescroll.js', CClientScript::POS_END );
            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/jquery.sparkline.js', CClientScript::POS_END );
            $cs->registerScriptFile( $theme->getBaseUrl() . '/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js', CClientScript::POS_END );
            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/owl.carousel.js', CClientScript::POS_END );
            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/jquery.customSelect.min.js', CClientScript::POS_END );
            $cs->registerScriptFile( $theme->getBaseUrl() . '/js/respond.min.js', CClientScript::POS_END );

        ?>


        <!--common script for all pages-->
        <?php $cs->registerScriptFile( $theme->getBaseUrl() . '/js/common-scripts.js', CClientScript::POS_END ); ?>

        <!--script for this page-->
        <?php $cs->registerScriptFile( $theme->getBaseUrl() . '/assets/chart-master/Chart.js', CClientScript::POS_END ); ?>

        <!-- script for this page only-->
        <?php $cs->registerScriptFile( $theme->getBaseUrl() . '/js/count.js', CClientScript::POS_END ); ?>

        <script>

            //owl carousel

            $(document).ready(function() {

                $("#owl-demo").owlCarousel({
                    navigation: true,
                    slideSpeed: 300,
                    paginationSpeed: 400,
                    singleItem: true,
                    autoPlay: true

                });
            });

            //custom select box

            $(function() {
                $('select.styled').customSelect();
            });

        </script>
        <?php $cs->registerScriptFile( $theme->getBaseUrl() . '/js/all-chartjs.js', CClientScript::POS_END ); ?>

        <!-- Google Tag Manager -->
<!-- <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MD367B"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MD367B');-->
<!-- End Google Tag Manager -->

    </body>
    
    
        

   </script>   
</html>
