$(document).ready(function(){
 
    //hide detailed-view
    $("#detailed-view").hide();
    
    //popover functionality
//$(".plus").popover({
//        html : true, 
//        content: function() {
//          return $(".popover-content-custom").html();
//        },
//        title: function() {
//          return $(".popover-title-custom").html();
//        }
//    });
    
    //show detailed view on clicking on list item barring buttons
//    $(".site-list-item").click(function(e) {
//        if(e.target.nodeName !== "BUTTON")
//            {
//              $("#detailed-view").show();  
//            }
//        
//    });

    $("#sort .btn").click(function(e) {
        if(!$(this).hasClass("active")){ 
            $("#sort").find(".active").removeClass("active");
            $(this).addClass("active");
            }
        else {
        $(this).removeClass("active");
        }
    fetchListings();
    });
    
//to keep filter dropdowns open
    $('.dropdown-menu').click( function(e) {
    e.stopPropagation();
    });
    

//datepicker in availability filter    
    $(".input-group.date").datepicker({ autoclose: true, todayHighlight: true });
    
    
//range slider in pricing filter
    $('#price-slider').slider();
    
//keeping the filter labes color dark when mouse is in outside area
    
});
