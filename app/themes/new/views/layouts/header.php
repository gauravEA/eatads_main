<!DOCTYPE html5>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>EatAds</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
         <!-- Bootstrap core CSS -->
         <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <?php
            $theme = Yii::app()->theme;
            $cs = Yii::app()->clientScript;
            $cs->registerCssFile( $theme->getBaseUrl() . '/css/application.css' );
            
          $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/jquery-1.11.0.min.js');
          $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/bootstrap.js');
        $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js');
        ?>
    
<script>
     dataLayer = [];
</script>
<style>
    #loading-image {
    background-color: #333;
    width: 100%;
    height: 100%;
    position: fixed;
    top: 0px;
    right: 0px;
    z-index: 9999;
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;
    border-radius: 10px; /* future proofing */
    -khtml-border-radius: 10px;
        opacity: 0.7;
}    
</style>
    
    
    </head>

    <body>
    <!-- header -->
        
    <nav class="navbar navbar-default" role="navigation">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">
            <h6 class="disclaimer"><span>Powered by </span><span style="color: #C70B27">Eat</span><span style="color:#17568A">Ads</span></h6>
            <div id="image" class="vendor-logo"></div>  
          </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav tabs">
              <li id="mediaunitlink" class="navbartoggle"><a href="#" >Media Units</a></li>
            <li class="navbartoggle" id="availabilitymanagerlink"><a href="<?php echo Yii::app()->urlManager->createUrl('vendor/' . $this->myvar . '/availability' ) ?>">Availability Manager</a></li>
            <li id="contactslink" class="navbartoggle"><a href="<?php echo Yii::app()->urlManager->createUrl('vendor/' . $this->myvar . '/contacts' ) ?>">Contacts</a></li>
            <li id="insightslink" class="navbartoggle"><a href="<?php echo Yii::app()->urlManager->createUrl('vendor/' . $this->myvar . '/insights' ) ?>" >Insights</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
<!--            <li>
                <form class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default">Search</button>
                </form>
            </li>-->
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Stephanie<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings</a></li>
                <li><a href="#">Logout</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
<?php if (Yii::app()->user->hasFlash('success')) { ?>
<div class="row" id="errormessageid" >
    <div class="col-md-12">
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    </div>
</div>
        <?php } ?>

<div id="loading-image" style="display:none;">
            <img style="display: block;margin-left: 400px;margin-top: 200px;" src='img/override.gif' alt="Loading..." />
        </div>    