function initialize() {
    var markersArray = [];
    var trafficLayer;
    var markers = [
    ['Malad', 19.164174,72.850647],
    ['Andheri', 19.136282,72.836914],
    ['Andheri', 19.13990,72.837894],
    ['Andheri', 19.156282,72.878914],
    ['Andheri', 19.148282,72.859914],
    ['Andheri', 19.147282,72.888914],
    ['Andheri', 19.139282,72.821914],
    ['Andheri', 19.146282,72.856914]
    ];
    
    var bounds = new google.maps.LatLngBounds();
	
  var mapOptions = {
      center: new google.maps.LatLng(19.5403, 75.5463),
      zoom: 1,
      panControl: false,
      zoomControl: false,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      overviewMapControl: false,
      zoomControl: true,
      zoomControlOptions: {
        style: google.maps.ZoomControlStyle.SMALL,
        position: google.maps.ControlPosition.TOP_LEFT
      },
      mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById('map_canvas'),
      mapOptions);

 var PolygonOptions = {map:map,clickable:false,
        strokeColor: 'steelblue',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: 'steelblue',
        fillOpacity: 0.35
    }
      
  
      
      
  google.maps.event.addDomListener(map.getDiv(),'mousedown',function(e){
    //do it with the right mouse-button only
    if(e.button!=2)return;
    //the polygon
    poly=new google.maps.Polyline({options:PolygonOptions});
    //move-listener
    var move=google.maps.event.addListener(map,'mousemove',function(e){
          poly.getPath().push(e.latLng);
        });
    //mouseup-listener
    google.maps.event.addListenerOnce(map,'mouseup',function(e){
          google.maps.event.removeListener(move);
          if('Polygon'=='Polygon'){
            var path=poly.getPath();
            poly.setMap(null);
            poly=new google.maps.Polygon({map:map,path:path,options:PolygonOptions});
          }
          var coordinate = new google.maps.LatLng(40, -90);                                                                                                                                                                                                       
//          var polygon = new google.maps.Polygon([], "#000000", 1, 1, "#336699", 0.3);
          for( i = 0; i < markersArray.length; i++ ) {
              // var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
               if(!google.maps.geometry.poly.containsLocation(markersArray[i].position, poly)) 
               {
                   console.log(markersArray[i].position);
                   markersArray[i].setVisible(false);
               }
              
          }
          $("#clearoption").show('slide', {direction: 'left'}, 5000);
          
    });

  });


  // Multiple Markers
  
  // Info Window Content
  var infoWindowContent = [
      ['<div class="info_content">' +
      '<h3>Digital Screen</h3>' +
      '<p> Located in Bandra</p>' + '<p> Price</p>' + '<p> 5000 INR</p>' +   '</div>'],
      ['<div class="info_content">' +
      '<h3>Price</h3>' +
      '<p> 5000 INR</p>' +
      '</div>']
  ];
  
  // Display multiple markers on a map
  var infoWindow = new google.maps.InfoWindow(), marker, i;

  // Loop through our array of markers & place each one on the map  
  for( i = 0; i < markers.length; i++ ) {
      var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
      bounds.extend(position);
      marker = new google.maps.Marker({
          position: position,
          map: map,
          icon: 'images/vendorProfileDemo/marker.png',
          title: markers[i][0]
      });
 
      // Allow each marker to have an info window    
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
              infoWindow.setContent(infoWindowContent[i][0]);
              infoWindow.open(map, marker);
          }
      })(marker, i));

      // Automatically center the map fitting all markers on the screen
      map.fitBounds(bounds);
      markersArray.push(marker);
  
  }

  // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
  var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
      this.setZoom(14);
      google.maps.event.removeListener(boundsListener);
  });

  $( ".entypo-audience").click(function() {
      if (trafficLayer == null )
      {
       trafficLayer = new google.maps.TrafficLayer();
      trafficLayer.setMap(map);
      }
      else{
          trafficLayer.setMap(null);
          trafficLayer = null;
      }
  });


}

google.maps.event.addDomListener(window, 'load', initialize);
