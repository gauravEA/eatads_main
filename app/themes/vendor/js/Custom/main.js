$(document).ready(function(){
 
    //hide detailed-view
    $("#detailed-view").hide();

    $("#sort .btn").click(function(e) {
        if(!$(this).hasClass("active")){ 
            $("#sort").find(".active").removeClass("active");
            $(this).addClass("active");
            }
        else {
        $(this).removeClass("active");
        }
    fetchListings();
    });
    
//to keep filter dropdowns open
    $('.dropdown-menu').click( function(e) {
    e.stopPropagation();
    });
    

//datepicker in availability filter    
    $(".input-group.date").datepicker({ autoclose: true, todayHighlight: true });
    
    
//range slider in pricing filter
    $('#price-slider').slider();
    
//keeping the filter labes color dark when mouse is in outside area
    
});
