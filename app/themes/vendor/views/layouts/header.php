<!DOCTYPE html5>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>EatAds</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
         <!-- Bootstrap core CSS -->
         <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <?php
            $theme = Yii::app()->theme;
            $cs = Yii::app()->clientScript;
            $cs->registerCssFile( $theme->getBaseUrl() . '/css/bootstrap.min.css' );
            $cs->registerCssFile( $theme->getBaseUrl() . '/css/bootstrap-theme.min.css' );
            $cs->registerCssFile( $theme->getBaseUrl() . '/css/main.css' );
            $cs->registerCssFile( $theme->getBaseUrl() . '/css/datepicker.css' );
        $cs->registerCssFile( $theme->getBaseUrl() . '/css/slider.css' );
             
        
          $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/jquery-1.11.0.min.js');
          $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/bootstrap.js');
        $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js');
        
        $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/dust/dust-full-2.2.0.js');
        $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/dust/dust-helpers-1.1.1.js');
        
         
        ?>
       <?php Yii::app()->clientScript->registerScriptFile("https://maps.googleapis.com/maps/api/js", CClientScript::POS_BEGIN); ?>
        <?php
            $theme = Yii::app()->theme;
            $cs = Yii::app()->clientScript;
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/Custom/map.js', CClientScript::POS_BEGIN); 
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/Custom/main.js', CClientScript::POS_BEGIN);
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/Custom/bootstrap-datepicker.js', CClientScript::POS_BEGIN);
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/Custom/bootstrap-slider.js', CClientScript::POS_BEGIN);
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/Custom/template/site_detail_view.tl', CClientScript::POS_BEGIN);
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/Custom/template/listings.tl', CClientScript::POS_BEGIN);
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/Custom/template/myplans.tl', CClientScript::POS_BEGIN);
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/Custom/template/popover_plans.tl', CClientScript::POS_BEGIN);
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/Custom/template/mediatypes.tl', CClientScript::POS_BEGIN);
            $cs->registerScriptFile($theme->getBaseUrl() . '/js/vendor/jquery.infinitescroll.js', CClientScript::POS_BEGIN);
            
        ?>
<script src="<?php echo Yii::app()->params['protocol']; ?>maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.geocomplete.js', CClientScript::POS_BEGIN); ?>

<!--        <script src="twitter-bootstrap-v2/docs/assets/js/bootstrap-transition.js"></script>-->
    
<script>
     dataLayer = [];
</script>
    </head>

    <body>

           <!-- header -->
        
    
   <nav class="navbar navbar-default navbar-static-top" role="navigation" id="top-header">
  <div class="container-fluid">
    <div class="row">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
 
      <a class="navbar-brand" href="#">
        <h6><span>Powered by </span><span style="color: #C70B27">Eat</span><span style="color:#17568A">Ads</span></h6>
        <div id="image"><img class="vendor-logo" src="/images/vendorProfileDemo/vendor-logo.png"></div>
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active-custom"><a href="#">Inventory</a></li>
        <!--<li><a href="#">About</a></li>-->
        <li><button class="btn btn-primary-custom1" data-toggle="modal" data-target="#myModal">Contact</button></li>
            <li class="divider"></li>
          </ul>
        </li>
      </ul>
          
      <ul class="nav navbar-nav navbar-right ">
        <li>
           <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
            <input type="text" class="form-control" id="geocomplete" placeholder="Search">
        </div>
            </form> 
        </li>
          
        <!-- /My Plans -->  
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Plans<b class="caret"></b></a>
          <ul class="dropdown-menu" id="myplans_id">
            
          </ul>
        </li>
          
        <!-- User Account --> 
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gaurav<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#">Logout</a> </li>
          </ul>
        </li>
        <li><img src="/images/vendorProfileDemo/user.png" width="45px" height="50px" margin-top="10px"></li>
          
      </ul>
    </div><!-- /.navbar-collapse -->
    </div>      
  </div><!-- /.container-fluid -->
</nav>

<!-- filter bar -->
      <nav class="navbar navbar-default" role="navigation" id="filter-bar">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
          
        <!-- Availability -->  
        <li class="dropdown">
          <a href="#" class="dropdown-toggle dropdown-custom1" data-toggle="dropdown">Availability<b class="caret"></b></a>
          <ul class="dropdown-menu dropdown-menu-custom">
            <li>
                <div class="dropdown-menu-container">
                    <h4>Select a Date:</h4>
                    <br>
                    <div class="form-group row" id="datepicker">
                        <div class="col-xs-8">
                            <div class="input-group date" id="dp3" data-date="21-06-2014" data-date-format="dd-mm-yyyy">
                            <input class="form-control" type="text" value="21-06-2014">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                                                                  </div>
            </li>
          </ul>
        </li>
          
        <!-- Media Types -->  
        <li class="dropdown keep-open">
          <a href="#" class="dropdown-toggle dropdown-custom1" data-toggle="dropdown">Media Types<b class="caret"></b></a>
          <ul class="dropdown-menu dropdown-menu-custom" id="mediatypes">
            
               <?php
                       foreach (MediaType::getPriorityMediaTypeForCompany() as $mt) {
               ?>
              <li>
                <a href="#">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="checkboxMedia" name="type" value="<?php echo $mt->id ?>"><?php echo $mt->name ?>
                        </label>
                    </div>
                </a>
            </li>
            <?php
                       }
            ?>
          </ul>
        </li>
          
        <!-- Point of Interest -->  
        <li class="dropdown">
          <a href="#" class="dropdown-toggle dropdown-custom1" data-toggle="dropdown">Point of Interest<b class="caret"></b></a>
          <ul class="dropdown-menu dropdown-menu-custom">
            <li>
                <a href="#">
                    <div class="checkbox">
                        <label>
      <input type="checkbox">School
                        </label>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="checkbox">
                        <label>
      <input type="checkbox">Cafe
                        </label>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="checkbox">
                        <label>
      <input type="checkbox">Hospital
                        </label>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="checkbox">
                        <label>
      <input type="checkbox">Cinema
                        </label>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="checkbox">
                        <label>
      <input type="checkbox">IT Parks
                        </label>
                    </div>
                </a>
            </li>
          </ul>
        </li>
          
        <!-- Pricing -->  
        <li class="dropdown">
          <a href="#" class="dropdown-toggle dropdown-custom1" data-toggle="dropdown">Pricing<b class="caret"></b></a>
          <ul class="dropdown-menu dropdown-menu-custom">
            <li>
                <div class="dropdown-menu-container">
                    <div class="slider-container">
                        INR 5000&nbsp;<input id="price-slider" type="text" class="span2" value="" data-slider-min="5000" data-slider-max="3000000" data-slider-step="5000" data-slider-value="[5000,100000]"/>&nbsp;INR 500000
                    </div>
                </div>
            </li>
          </ul>
        </li>
          
        <!-- Lighting -->  
        <li class="dropdown">
          <a href="#" class="dropdown-toggle dropdown-custom1" data-toggle="dropdown">Lighting<b class="caret"></b></a>
          <ul class="dropdown-menu dropdown-menu-custom">
            <li>
                <a href="#">
                    <div class="checkbox" >
                        <label>
                            <input type="checkbox" class="checkboxMedia" name="lighting" value="1">Non-lit
                        </label>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="checkboxMedia" name="lighting" value="2">Front-lit
                        </label>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="checkboxMedia" name="lighting" value="3">Back-lit
                        </label>
                    </div>
                </a>
            </li>
          </ul>
        </li>
          
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>