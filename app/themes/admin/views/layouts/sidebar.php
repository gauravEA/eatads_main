<!--sidebar start-->
            <aside>
                <div id="sidebar"  class="nav-collapse ">
                    <!-- sidebar menu start-->
                    <ul class="sidebar-menu" id="nav-accordion">
                        <li>
                            <a class="active" href="<?php echo JoyUtilities::getDashboardUrl(); ?>">
                                <i class="fa fa-dashboard"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a class="" href="<?php echo Yii::app()->urlManager->createUrl('admin/listing/massupload'); ?>">
                                <i class="fa fa-dashboard"></i>
                                <span>Massupload</span>
                            </a>
                        </li>
                         <li>
                            <a class="" href="<?php echo Yii::app()->urlManager->createUrl('admin/listing/signup'); ?>">
                                <i class="fa fa-dashboard"></i>
                                <span>Signup</span>
                            </a>
                        </li>
                        <li>
                            <a class="" href="<?php echo Yii::app()->urlManager->createUrl('admin/listing/massuploadnew'); ?>">
                                <i class="fa fa-dashboard"></i>
                                <span>New Mass Upload</span>
                            </a>
                        </li>
                        <?php /*<li>
                            <a class="" href="<?php echo Yii::app()->urlManager->createUrl('admin/audiencetag'); ?>">
                                <i class="fa fa-dashboard"></i>
                                <span>Audience Tag</span>
                            </a>
                        </li> */ ?>

                    </ul>
                    <!-- sidebar menu end-->
                </div>
            </aside>
            <!--sidebar end-->